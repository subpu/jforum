<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set value="${pageContext.request.contextPath}" var="BASE"/>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>快速回复</title>
  </head>
  <body>
      <fmt:parseNumber var="mid" integerOnly="true" type="number" value="${member.id}" />
      <h5 class="hl45 pl-4">快速回复<span class="float-right"><a href="javascript:;" class="drawer-close-btn embed_drawer_close ibw-anchor text-center" role="button"><i class="ico mdi mdi-close"></i></a></span></h5>
      <div style="padding:5px 15px">
        <p class="alert alert-danger text-center" role="alert">${errors}</p>
        <c:if test="${mid<=0}">
        <p><a class="d-inline-block w-100 p-2 text-center" role="button" href="${BASE}/member/login">登录</a></p>
        <p><a class="d-inline-block w-100 p-2 text-center" role="button" href="${BASE}/member/register">注册</a></p>
        </c:if>
      </div>
  </body>
</html>