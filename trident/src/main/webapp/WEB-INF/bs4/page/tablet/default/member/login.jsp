<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>小伙伴正在等您开车呢</title>
    <style>#embed_page_form{padding:0 15px;padding-top: 20px;padding-bottom: 5px;}</style>
  </head>
  <body>
      <header class="container-fluid main-color" id="embed_page_header">
          <div class="row">
              <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="historyBack"><i class="ico mdi mdi-chevron-left"></i></a></div>
              <div class="col-xs-10 col-sm-10 embed_header_title">会员登录</div>
              <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;"><i class="ico mdi mdi-search"></i></a></div>
          </div>
      </header>
      <div id="embed_page">
          <div class="container">
              <form action="${BASE}/member/login" method="post" id="login" data-submit="once" class="theme-form">
              <div id="embed_page_form" class="main-color">
                  <c:if test="${not empty errors}">
                  <div class="alert alert-danger" role="alert">
                      <strong>oOps!</strong> ${errors}
                  </div>
                  </c:if>
                  <div class="form-group">
                      <label for="pswdConfirm">帐号:</label>
                      <input type="text" tabindex="1" name="names" size="25" value="${form.names}" class="form-control" required="required" placeholder="登录帐号" />
                  </div>
                  <div class="form-group">
                      <label for="pswdConfirm">密码:</label>
                      <input type="password" tabindex="2" name="pswd" size="25" class="form-control" autocomplete="off" required="required" placeholder="登录密码" />
                      <p class="clearCache control-label">记不清密码了?请点击&nbsp;<a href="${BASE}/lostPswd">找回密码</a></p>
                  </div>
                  <div class="form-group">
                      <input type="hidden" name="token" value="${form.token}"/>
                      <input type="hidden" name="redirect" value="${form.redirect}"/>
                      <input type="hidden" name="tries" value="${form.tries}"/>
                      <input type="hidden" name="trace" value="msa"/>
                      <input type="submit" class="btn btn-primary btn-block" value="马上登陆"/>
                  </div>
              </div>
              <p class="main-color control-label" style="padding-bottom: 15px;text-align: center;">没有帐号? 还不快来<a href="${BASE}/member/register">免费注册</a></p>
              </form>
          </div>
      </div>
  </body>
</html>