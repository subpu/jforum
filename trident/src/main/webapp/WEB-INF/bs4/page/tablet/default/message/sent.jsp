<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>消息中心</title>
  </head>
  <body>
    <header class="container-fluid main-color" id="embed_page_header">
        <div class="row">
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="historyBack"><i class="ico mdi mdi-chevron-left"></i></a></div>
            <div class="col-xs-10 col-sm-10">
                <ul class="list-inline header_menu">
                  <li class="list-inline-item"><a href="${BASE}/message/">收件箱</a></li>
                  <li class="list-inline-item focus"><a href="javascript:;">发件箱</a></li>
                </ul>
            </div>
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;"><i class="ico mdi mdi-search"></i></a></div>
        </div>
    </header>
    <div id="embed_page">
        <div class="container">
            <h6 class="home-record-header default-txt-color"><span class="float-right"><small>${total}条记录</small></span></h6>
            <!-- 列表 -->
            <div id="member-message-records" class="main-color page_scroll_pagination" data-handler="${BASE}/message/outbox/rawdata" data-function="memberTabletOutboxTemplate"></div>
            <!-- /列表-->
        </div>
        <!-- 分页开始 -->
        <!-- 分页结束 -->
    </div>
    <footer class="container-fluid main-color" id="embed_page_footer">
        <div class="row">
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/home" role="button"><i class="ico-sm mdi mdi-home"></i>&nbsp;首页</a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/board/home" role="button"><i class="ico-sm mdi mdi-layers"></i>&nbsp;版块</a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/message/" role="button" class="btn btn-primary" id="primary-panel-notice"><i class="ico-sm mdi mdi-notifications"></i>&nbsp;消息&nbsp;<sup class="device-notice-ele"></sup></a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/member/home/" role="button"><i class="ico-sm mdi mdi-account"></i>&nbsp;我</a></div>
        </div>
    </footer>
  </body>
</html>