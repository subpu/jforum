<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>消息中心</title>
  </head>
  <body>
    <header class="container-fluid main-color" id="embed_page_header">
        <div class="row">
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="historyBack"><i class="ico mdi mdi-chevron-left"></i></a></div>
            <div class="col-xs-10 col-sm-10">
                <ul class="list-inline header_menu">
                  <li class="list-inline-item focus"><a href="javascript:;">收件箱</a></li>
                  <li class="list-inline-item"><a href="${BASE}/message/sent">发件箱</a></li>
                </ul>
            </div>
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="drawer" data-drawer="#top-page-drawer"><i class="ico mdi mdi-more"></i></a></div>
        </div>
    </header>
    <div id="embed_page">
        <div class="container">
            <h6 class="home-record-header default-txt-color"><span class="float-right"><small>${unreadSize}未读消息</small></span></h6>
            <!-- 列表 -->
            <div id="member-message-records" class="main-color page_scroll_pagination" data-handler="${BASE}/message/inbox/rawdata" data-function="memberTabletInboxTemplate" data-deferred="unreadInboxMessageSize"></div>
            <!-- /列表-->
        </div>
        <!-- 分页开始 -->
        <!-- 分页结束 -->
    </div>
    <footer class="container-fluid main-color" id="embed_page_footer">
        <div class="row">
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/home" role="button"><i class="ico-sm mdi mdi-home"></i>&nbsp;首页</a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/board/home" role="button"><i class="ico-sm mdi mdi-layers"></i>&nbsp;版块</a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/message/" role="button" class="btn btn-primary" id="primary-panel-notice"><i class="ico-sm mdi mdi-notifications"></i>&nbsp;消息&nbsp;<sup class="device-notice-ele"></sup></a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/member/home/" role="button"><i class="ico-sm mdi mdi-account"></i>&nbsp;我</a></div>
        </div>
    </footer>
    <!-- 顶部菜单开始-->
    <div class="embed_drawer_menu main-color" id="top-page-drawer">
        <h5 class="hl45 pl-4">消息管理菜单</h5>
        <!-- list-unstyled -->
        <ul class="list-unstyled">
            <li class="txt-anchor"><a href="javascript:;" class="action-ddm ibw-anchor" data-handler="${BASE}/message/read/all?member=-1&direction=2"><i class="ico mdi mdi-assignment-check"></i><span>全部标记为已读</span></a></li>
            <li class="txt-anchor"><a href="javascript:;" class="drawer-close-btn ibw-anchor"><i class="ico mdi mdi-close"></i><span>关闭</span></a></li>
        </ul>
    </div>
    <!-- 顶部菜单结束-->
  </body>
</html>