<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>${member.nickname}的个人主页</title>
  </head>
  <body>
      <div class="container-fluid main-color" id="embed_page_header">
          <div class="row">
              <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="historyBack"><i class="ico mdi mdi-chevron-left"></i></a></div>
              <div class="col-xs-10 col-sm-10 embed_header_title">${member.nickname}的个人主页</div>
              <div class="col-xs-1 col-sm-1 text-center">
                  <c:if test="${mine}">
                  <a href="${BASE}/member/home/profile" role="button">
                      <i class="ico mdi mdi-settings" aria-hidden="true"></i>
                  </a>
                  </c:if>
                  <c:if test="${not mine}">
                  <a href="javascript:;" title="发送消息" role="button" class="drawer" data-drawer="#message-form-drawer">
                      <i class="ico mdi mdi-email" aria-hidden="true"></i>
                  </a>
                  </c:if>
              </div>
          </div>
      </div>
      <div id="embed_page">
          <div class="container">
              <div class="row">
                  <div class="col-xs-9 col-sm-10">
                      <h4 class="body_header">${member.nickname}<br/><small>${member.signature}</small></h4>
                      <ul class="list-inline" id="board-stats-section">
                          <li class="list-inline-item"><i class="ico-sm mdi mdi-looks"></i> 话题 <span><c:out value="${member.threads}" default="0"/></span></li>
                          <li class="list-inline-item"><i class="ico-sm mdi mdi-comment"></i> 回复 <span><c:out value="${member.replies}" default="0"/></span></li>
                          <li class="list-inline-item"><i class="ico-sm mdi mdi-time"></i> 活跃 <span>${member.activeDateTime}</span></li>
                      </ul>
                  </div>
                  <div class="col-xs-3 col-sm-2">
                      <span class="float-right"><img class="img-circle" src="${BASE}/member/avatar/${member.id}.png" width="64px" height="64px"/></span>
                  </div>
              </div>
              <div class="row">
                  <div class="col-xs-12 col-sm-12 w-100" style="margin-bottom:30px;">
                      <h5 class="default-txt-color">版块收藏</h5>
                      <div id="favorite-board-collect" data-result="loader.json" data-empty-show="false" data-handler="${BASE}/member/home/board/active/json" data-query="member:${member.id}" data-template-function="buildMemberStarBoard">
                          <p class="no-record-tip"><i class="fa fa-spinner" aria-hidden="true"></i> 看成败人生豪迈, 只不过是从头再来</p>
                      </div>
                  </div>
              </div>
              <!-- foreach -->
              <div class="topic_box">
                  <div style="margin-bottom:40px;">
                      <h5 class="default-txt-color" style="margin-left:15px">发布的话题<c:if test="${mine}">&nbsp;<small class="float-right" style="padding-right: 15px;"><a href="${BASE}/topic/member" role="button">更多</a></small></c:if></h5>
                      <div data-result="loader.json" data-empty-show="false" data-template-function="drawDynamicTabletTopic" data-query="member:${member.id}" data-handler="${BASE}/member/home/topic/publish/json">
                          <p class="no-record-tip"><i class="fa fa-spinner" aria-hidden="true"></i> 看成败人生豪迈, 只不过是从头再来</p>
                      </div>
                  </div>
                  
                  <div>
                      <h5 class="default-txt-color" style="margin-left:15px">回复的话题<c:if test="${mine}">&nbsp;<small class="float-right" style="padding-right: 15px;"><a href="${BASE}/posts/member" role="button">更多</a></small></c:if></h5>
                      <div data-result="loader.json" data-empty-show="false" data-template-function="drawDynamicTabletTopic" data-query="member:${member.id}" data-handler="${BASE}/member/home/topic/reply/json">
                          <p class="no-record-tip"><i class="fa fa-spinner" aria-hidden="true"></i> 看成败人生豪迈, 只不过是从头再来</p>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <footer class="container-fluid main-color" id="embed_page_footer">
        <div class="row">
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/home" role="button"><i class="ico-sm mdi mdi-home"></i>&nbsp;首页</a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/board/home" role="button"><i class="ico-sm mdi mdi-layers"></i>&nbsp;版块</a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/message/" role="button" id="primary-panel-notice"><i class="ico-sm mdi mdi-notifications"></i>&nbsp;消息&nbsp;<sup class="device-notice-ele"></sup></a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/member/home/" role="button"><i class="ico-sm mdi mdi-account"></i>&nbsp;我</a></div>
        </div>
      </footer>
      <!-- 消息发布开始-->
      <div class="embed_drawer_menu main-color" id="message-form-drawer" style="bottom:-260px">
        <form id="send-message-form">
        <h5 class="hl45 pl-4">给${member.nickname}发送消息<span class="float-right pr-3"><a href="javascript:;" role="button" class="send-message-action btn btn-sm btn-primary" data-handler="${BASE}/message/transmit" data-query="receiver:${member.id},names:${member.nickname}" data-function="tabletMemberMessageFun">提交</a></span></h5>
        <div style="padding:5px 15px">
            <input type="hidden" name="token" value="${token}" />
            <textarea name="content" rows="2" cols="60" class="drawer_menu_text form-control" placeholder="在此输入消息内容" style="border-style:none;border-radius:0"></textarea>
        </div>
        </form>
        <p class="hl60"><a href="javascript:;" class="drawer-close-btn ibw-anchor text-center">关闭</a></p>
      </div>
      <!-- 消息发布结束-->
      <!-- 侧边栏工具条开始 -->
      <section id="sideTool" data-tools="theme"></section>
      <!-- 侧边栏工具条结束 -->
  </body>
</html>