<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>服务器找不到请求的资源 - 404</title>
    <style>
      html,body{margin:0;padding:0;width:100%;height:100%;font-size: 1.2rem;font-weight: 400;line-height: 1.5;font-family:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace}
      .badge {display: inline-block;padding: .25em .4em;font-size: 75%;font-weight: 400;line-height: 1;text-align: center;white-space: nowrap;vertical-align: baseline;border-radius: .25rem;}
      .badge-dark {color: #fff;background-color: #343a40;}
      #embed_page{margin: auto 0;width:100%;height:100%}
          .container{width:inherit;height:inherit;justify-content: center;align-items: center;display:flex}
             .embed-err-box{height:auto;display:inline-block;background-color: #fff;background-repeat: no-repeat;background-size: auto;-webkit-background-size: auto;-o-background-size: auto;background-position: 50% 0;max-width: 502px;}
                 .embed-err-info{padding-top:210px;padding-left:30px}
                     .embed-err-info h4{color:#f00;margin-bottom:0}
                     .embed-err-info p{margin-top:10px}
                          .embed-err-info span{display:inline-block;margin:0 10px}
                              .embed-err-info span a{text-decoration: none;color:#000}.embed-err-info span a:hover{color:#007bff}
    </style>
  </head>
  <body>
      <div id="embed_page">
          <div class="container">
              <div class="embed-err-box" style="background-image:url('/static/img/tablet-oops.png');min-height:230px;min-width:300px">
                  <div class="embed-err-info">
                      <h4><small class="badge badge-dark">404</small>&nbsp;服务器找不到请求的资源</h4>
                      <p>您可以: 
                          <span><a href="${BASE}">首页</a></span>
                          <span><a href="${BASE}/board/home">版块</a></span> 
                          <span><a href="${BASE}/member/home/">个人中心</a></span>
                      </p>
                  </div>
              </div>
          </div>
      </div>
  </body></html>