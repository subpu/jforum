<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>创建话题</title>
    <style>.bootstrap-select{width:auto!important}</style>
  </head>
  <body>
      <div class="container-fluid main-color" id="embed_page_header">
          <div class="row">
              <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="historyBack"><i class="ico mdi mdi-chevron-left"></i></a></div>
              <div class="col-xs-10 col-sm-10 embed_header_title">创建话题</div>
              <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;"><i class="ico mdi mdi-search"></i></a></div>
          </div>
      </div>
      <div id="embed_page">
          <div class="container">
              <div class="main-color p-3">
                  <c:if test="${not empty errors}">
                  <div class="alert alert-danger" role="alert">
                      <strong>oOps!</strong> ${errors}
                  </div>
                  </c:if>
                  <form method="post" action="${BASE}/topic/create" class="theme-form">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-xs-3 col-sm-3">
                          <select class="asyn-loadate-select" name="category" data-handler="${BASE}/loader/board/category.json?volumes=${form.volumes}&board=${form.board}" data-current="${form.category}" required="required" tabindex="1">
                              <option value="">选择话题类型</option>
                          </select>
                      </div>
                      <div class="col-xs-9 col-sm-9">
                          <input type="text" name="title" class="form-control" value="${form.title}" placeholder="主题在此输入" required="required" tabindex="2"/>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                      <div>
                          <textarea id="content" name="content" class="richeditor" data-query="height:450,width:100%,upload:/upload/ckeditor" required="required" tabindex="3">${form.content}</textarea>
                      </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-xs-9 col-sm-9">
                        <div class="d-inline-block mr-2">
                          <select name="volumes" data-current="${form.volumes}" class="asyn-loadate-select" id="asyn-volumes-select" data-handler="${BASE}/loader/volumes/usable.json" required="required" tabindex="4">
                            <option value="">请选择版块组</option>
                          </select>
                        </div>
                        <div class="d-inline-block">
                          <select name="board" data-current="${form.board}" class="selectpicker" id="asyn-board-select" data-handler="${BASE}/loader/volumes/board.json" required="required" tabindex="5">
                            <option value="">请选择版块</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-xs-3 col-sm-3 text-right">
                          <input type="hidden" name="token" value="${form.token}"/>
                          <input type="submit" name="publish" tabindex="4" value="发布话题" class="btn btn-primary" />
                      </div>
                    </div>
                  </div>
                  </form>
              </div>
          </div>
      </div>
  </body>
</html>