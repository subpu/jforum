<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>随便看看</title>
  </head>
  <body>
    <header class="container-fluid main-color" id="embed_page_header">
        <div class="row">
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" role="button"><i class="ico mdi mdi-menu"></i></a></div>
            <div class="col-xs-10 col-sm-10 embed_header_title">随便看看</div>
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;"><i class="ico mdi mdi-search"></i></a></div>
        </div>
    </header>
    <div id="embed_page" data-result="batch.jsonp" data-handler="${BASE}/loader/album.jsonp" data-function="packAlbumHoldSection" data-param="album" data-record="div[data-album]" data-query="size:4">
        <div class="container">
            <c:forEach items="${rs}" var="topic">
            <div class="embed_topic_item main-color" id="topic-${topic.id}">
                <div class="embed_topic_item_head">
                    <div class="topic_item_avatar">
                        <a href="${BASE}/member/${topic.memberId}.xhtml" title="${topic.memberNickname}">
                            <img class="img-circle" src="${BASE}/member/avatar/${topic.memberId}.png" style="width: 64px; height: 64px;">
                        </a>
                    </div>
                    <div class="topic_item_author">
                        <h6><a href="${BASE}/topic/${topic.connect}.xhtml" class="topic_item_title">${topic.title}</a></h6>
                        <p><a href="${BASE}/member/${topic.memberId}.xhtml" title="会员主页">${topic.memberNickname}</a>&nbsp;&#x40;&nbsp;<forum:format value="${topic.entryDateTime}"/>&nbsp;&nbsp;&#187;&nbsp;&nbsp;<a href="${BASE}/board/${topic.board.connect}.xhtml">${topic.board.title}</a></p>
                    </div>
                    <div class="topic_item_tools">
                        <p><a href="javascript:;" role="button" class="btn drawer" data-drawer="#topic-report-drawer" data-drawer-init="topicDrawerMenuInit" data-drawerclose-fun="drawerReportMenuClose"><i class="ico-sm mdi mdi-more-vert" aria-hidden="true"></i></a></p>
                    </div>
                </div>
                <div class="embed_topic_item_body" data-album="${topic.albumId}">
                    <p class="topic_item_summary">${topic.summary}</p>
                </div>
                <div class="embed_topic_item_foot">
                    <ul class="list-inline topic-panel">
                        <li class="list-inline-item"><a href="${BASE}/topic/${topic.connect}.xhtml"><i class="ico-sm mdi mdi-comment" aria-hidden="true"></i>&nbsp;&nbsp;${topic.stats.postses}</a></li>
                        <li class="list-inline-item"><i class="ico-sm ico-color-teal mdi mdi-thumb-up" aria-hidden="true"></i>&nbsp;&nbsp;${topic.stats.likes}</li>
                        <li class="list-inline-item"><i class="ico-sm ico-color-red mdi mdi-favorite" aria-hidden="true"></i>&nbsp;&nbsp;${topic.stats.favorites}</li>
                    </ul>
                    <span class="float-right footer_board_section">
                        <i class="ico-sm mdi mdi-time"></i>&nbsp;<a href="${BASE}/member/${topic.stats.recentPostsMemberId}.xhtml" class="username-coloured">${topic.stats.recentPostsMemberNickname}</a>&nbsp;&nbsp;<forum:format value="${topic.stats.recentPostsDate}"/>
                    </span>
                </div>
            </div>
            </c:forEach>
        </div>
    </div>
    <footer class="container-fluid main-color" id="embed_page_footer">
        <div class="row">
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/home" role="button" class="btn btn-primary"><i class="ico-sm mdi mdi-home"></i>&nbsp;首页</a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/board/home" role="button"><i class="ico-sm mdi mdi-layers"></i>&nbsp;版块</a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/message/" role="button" id="primary-panel-notice"><i class="ico-sm mdi mdi-notifications"></i>&nbsp;消息&nbsp;<sup class="device-notice-ele"></sup></a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/member/home/" role="button"><i class="ico-sm mdi mdi-account"></i>&nbsp;我</a></div>
        </div>
    </footer>
    <!-- 话题举报菜单开始-->
    <div class="embed_drawer_menu main-color" id="topic-report-drawer" style="bottom:-260px">
        <form action="${BASE}/topic/report" method="post">
        <h5 class="hl45 pl-4">举报话题<span class="float-right pr-3"><a href="javascript:;" role="button" class="btn btn-sm btn-primary drawer-report-action">提交</a></span></h5>
        <div style="height:60px">
           <ul class="list-inline report-type focus-item-parent">
               <li class="list-inline-item"><a href="javascript:;" class="focus-item" data-type="1">广告</a></li>
               <li class="list-inline-item"><a href="javascript:;" class="focus-item" data-type="2">色情</a></li>
               <li class="list-inline-item"><a href="javascript:;" class="focus-item" data-type="3">政治</a></li>
               <li class="list-inline-item"><a href="javascript:;" class="focus-item" data-type="4">暴力</a></li>
               <li class="list-inline-item"><a href="javascript:;" class="focus-item" data-type="5">语言暴力</a></li>
               <li class="list-inline-item"><a href="javascript:;" class="focus-item focus-current" data-type="6">其它</a></li>
           </ul>
        </div>
        <div style="padding:5px 15px">
            <input type="hidden" name="token" value="${token}" />
            <input type="hidden" name="id" value="" />
            <textarea name="reason" rows="5" cols="60" class="drawer_menu_text" placeholder="在此输入描述内容"></textarea>
        </div>
        <p class="hl60"><a href="javascript:;" class="drawer-close-btn ibw-anchor text-center">关闭</a></p>
        </form>
    </div>
    <!-- 话题菜单结束-->
    <!-- 侧边栏工具条开始 -->
    <section id="sideTool" data-tools="theme"></section>
    <!-- 侧边栏工具条结束 -->
  </body>
</html>