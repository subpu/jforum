<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>密码更新成功</title>
    <meta http-equiv="refresh" content="5; url=${DOMAIN}/member/home/" />
    <style>html,body{margin:0;padding:0;width:100%;height:100%;}#embed_page{margin: auto 0;width:100%;height:100%}.container{width:inherit;height:inherit;justify-content: center;align-items: center;display:flex}.embed-tip-box{height:auto;display:inline-block;}</style>
  </head>
  <body>
    <header class="container-fluid main-color" id="embed_page_header">
        <div class="row">
            <div class="col-xs-1 col-sm-1 text-center"></div>
            <div class="col-xs-10 col-sm-10">
                <ul class="list-inline header_menu">
                  <li class="list-inline-item focus"><a href="javascript:;">活动记录</a></li>
                  <li class="list-inline-item"><a href="${BASE}/member/home/profile">帐户设置</a></li>
                  <li class="list-inline-item"><a href="${BASE}/member/home/passport">密码设置</a></li>
                  <li class="list-inline-item"><a href="${BASE}/member/home/avatar">选择头像</a></li>
                </ul>
            </div>
            <div class="col-xs-1 col-sm-1 text-center"></div>
        </div>
    </header>
    <div id="embed_page">
        <div class="container">
              <div class="embed-tip-box">
                  <p class="ico-big text-center"><i class="ico-color-green mdi mdi-check-circle"></i></p>
                  <p class="embed-tip">登录密码更新成功;系统将自动跳转至<a href="${BASE}/member/home/">个人中心</a>页面</p>
              </div>
        </div>
    </div>
  </body></html>