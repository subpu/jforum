<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>密码设置</title>
  </head>
  <body>
    <header class="container-fluid main-color" id="embed_page_header">
        <div class="row">
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="historyBack"><i class="ico mdi mdi-chevron-left"></i></a></div>
            <div class="col-xs-10 col-sm-10">
                <ul class="list-inline header_menu">
                  <li class="list-inline-item"><a href="${BASE}/member/home/profile">个性签名</a></li>
                  <li class="list-inline-item"><a href="${BASE}/member/home/avatar">选择头像</a></li>
                  <li class="list-inline-item focus"><a href="javascript:;">修改密码</a></li>
                  <li class="list-inline-item"><a href="${BASE}/member/home/history">历史记录</a></li>
                </ul>
            </div>
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;"><i class="ico mdi mdi-search"></i></a></div>
        </div>
    </header>
    <div id="embed_page">
        <div class="container">
            <div class="main-color p-3">
                <c:if test="${not empty errors}">
                <div class="alert alert-danger" role="alert">
                    <strong>oOps!</strong> ${errors}
                </div>
                </c:if>
                <form  method="post" action="${BASE}/member/home/passport" id="member_passport_form" data-submit="once" class="theme-form parsley-form" data-parsley-validate>
                <div class="form-group">
                    <label class="control-label" for="oldpswd">原始密码:</label>
                    <input type="password" name="oldpswd" class="inputbox no-radius form-control w600" value="" data-parsley-required tabindex="1"/>
                </div>
                <div class="form-group">
                    <label class="control-label" for="newpswd">新密码:</label>
                    <small class="form-text text-muted">必须介于6个字符和100个字符之间</small>
                    <input type="password" name="newpswd" id="newpswd" class="inputbox no-radius form-control w600" value="" data-parsley-required data-parsley-length="[6, 100]" tabindex="2"/>
                </div>
                <div class="form-group">
                    <label class="control-label" for="confimpswd">确认密码:</label>
                    <small class="form-text text-muted">必须介于6个字符和100个字符之间</small>
                    <input type="password" name="confimpswd" class="inputbox no-radius form-control w600" value="" data-parsley-required data-parsley-equalto="#newpswd" tabindex="3"/>
                </div>
                <div class="form-group">
                    <input type="hidden" name="token" value="${form.token}"/>
                    <input type="submit" name="send" value="下一步" class="btn btn-primary btn-block" tabindex="4"/>
                </div>
                </form>
            </div>
        </div>
    </div>
    <footer class="container-fluid main-color" id="embed_page_footer">
        <div class="row">
            <div class="col-xs-3 col-sm-3 text-center"><a href="javascript:;" role="button" class="btn btn-primary"><i class="ico-sm mdi mdi-settings"></i>&nbsp;设置</a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/member/home/topic" role="button"><i class="ico-sm mdi mdi-looks"></i>&nbsp;话题</a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/message/" role="button" id="primary-panel-notice"><i class="ico-sm mdi mdi-notifications"></i>&nbsp;消息&nbsp;<sup class="device-notice-ele"></sup></a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/member/home/" role="button"><i class="ico-sm mdi mdi-account"></i>&nbsp;我</a></div>
        </div>
    </footer>
  </body>
</html>