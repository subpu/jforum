<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>收藏列表</title>
  </head>
  <body>
    <header class="container-fluid main-color" id="embed_page_header">
        <div class="row">
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="historyBack"><i class="ico mdi mdi-chevron-left"></i></a></div>
            <div class="col-xs-10 col-sm-10">
                <ul class="list-inline header_menu">
                  <li class="list-inline-item"><a href="${BASE}/member/home/topic">话题列表</a></li>
                  <li class="list-inline-item"><a href="${BASE}/member/home/posts">回复列表</a></li>
                  <li class="list-inline-item focus"><a href="javascript:;">收藏列表</a></li>
                  <li class="list-inline-item"><a href="${BASE}/member/home/topic/like">点赞列表</a></li>
                </ul>
            </div>
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;"><i class="ico mdi mdi-search"></i></a></div>
        </div>
    </header>
    <div id="embed_page">
        <div class="container">
            <h6 class="home-record-header default-txt-color">我收藏的话题<span class="float-right"><small>${total}条记录</small></span></h6>
            <!-- 列表-->
            <div id="member-home-actions" class="main-color page_scroll_pagination" data-handler="${BASE}/member/home/topic/favorite/rawdata" data-function="drawTabletMHomeAction"></div>
            <!-- /列表-->
        </div>
        <!-- 分页开始 -->
        <!-- 分页结束 -->
    </div>
    <footer class="container-fluid main-color" id="embed_page_footer">
        <div class="row">
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/member/home/profile" role="button"><i class="ico-sm mdi mdi-settings"></i>&nbsp;设置</a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="javascript:;" role="button" class="btn btn-primary"><i class="ico-sm mdi mdi-looks"></i>&nbsp;话题</a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/message/" role="button" id="primary-panel-notice"><i class="ico-sm mdi mdi-notifications"></i>&nbsp;消息&nbsp;<sup class="device-notice-ele"></sup></a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/member/home/" role="button"><i class="ico-sm mdi mdi-account"></i>&nbsp;我</a></div>
        </div>
    </footer>
    <!-- 侧边栏工具条开始 -->
    <section id="sideTool" data-tools="theme"></section>
    <!-- 侧边栏工具条结束 -->
  </body>
</html>