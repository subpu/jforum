<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set value="${pageContext.request.contextPath}" var="BASE"/>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>快速回复</title>
  </head>
  <body>
      <form action="${BASE}/posts/reply" style="padding-bottom: 15px;">
        <h5 class="hl45 pl-4">快速回复<span class="float-right"><a href="javascript:;" class="drawer-close-btn embed_drawer_close ibw-anchor text-center" role="button"><i class="ico mdi mdi-close"></i></a></span></h5>
        <div style="padding:5px 15px" class="h5Editor" data-emoji-json="${BASE}/static/js/data.json">
          <input type="hidden" name="token" value="${token}" />
          <input type="hidden" name="quote" value="0" />
          <input type="hidden" name="scale" value="" />
          <textarea name="content" id="content" rows="5" cols="60" class="drawer_menu_text emoji-textarea" placeholder="在此输入回复内容"></textarea>
        </div>
        <div id="emoji-picker" class="OwO" style="margin:0 15px"></div>
        <p style="position:absolute;right:15px;top:108px"><a href="javascript:;" class="btn btn-sm btn-primary embed-quickreply-btn" role="button">回复</a></p>
      </form>
  </body>
</html>