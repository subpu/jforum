<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>${topic.title}</title>
  </head>
  <body>
    <header class="container-fluid main-color" id="embed_page_header">
        <div class="row">
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="historyBack"><i class="ico mdi mdi-chevron-left"></i></a></div>
            <div class="col-xs-10 col-sm-10"><a href="${BASE}/board/${topic.board.connect}.xhtml">${topic.board.title}</a></div>
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="drawer" data-drawer="#top-page-drawer"><i class="ico mdi mdi-more"></i></a></div>
        </div>
    </header>
    <div id="embed_page">
        <div class="container" id="topic_posts_collect">
            <div class="row" id="embed_page_title">
                <h5 class="body_header col-xs-12 col-sm-12 default-txt-color">${topic.title}</h5>
                <div class="col-xs-12 col-sm-12">
                    <ul class="list-inline" id="topic-stats-section">
                        <li class="list-inline-item"><i class="ico-sm mdi mdi-eye"></i> 查看 <span><c:out value="${topic.stats.displaies}" default="0"/></span></li>
                        <li class="list-inline-item"><i class="ico-sm mdi mdi-comment"></i> 回复 <span><c:out value="${topic.stats.postses}" default="0"/></span></li>
                        <li class="list-inline-item"><i class="ico-sm mdi mdi-time"></i> 活跃 <span><forum:format value="${topic.rankingDateTime}"/></span></li>
                        <c:if test="${topic.tops}"><li class="list-inline-item"><span>&#x1F4CC&nbsp;置顶</span></li></c:if> 
                        <c:if test="${topic.goods}"><li class="list-inline-item"><span>&#x1F48E&nbsp;精华</span></c:if>
                        <c:if test="${topic.hots}"><li class="list-inline-item"><span>&#x1F525&nbsp;火贴</span></c:if> 
                    </ul>
                </div>
            </div>
            <!-- 话题内容开始 -->
            <div class="embed_topic_item main-color">
                <div class="embed_topic_item_head">
                    <div class="topic_item_avatar">
                        <a href="${BASE}/member/${topic.memberId}.xhtml" title="${topic.memberNickname}">
                            <img class="img-circle" src="${BASE}/member/avatar/${topic.memberId}.png" style="width: 64px; height: 64px;">
                        </a>
                    </div>
                    <div class="topic_item_author">
                        <h6>
                            <a href="${BASE}/member/${topic.memberId}.xhtml" title="会员主页">${topic.memberNickname}</a><small class="badge member-bg-${authorStyle} mgs">${topic.content.member.mgroup.title}</small>
                        </h6>
                        <p>楼主&nbsp;&nbsp;&#187;&nbsp;&nbsp;<forum:format value="${topic.entryDateTime}"/></p>
                    </div>
                    <div class="topic_item_tools">
                        <p><a href="javascript:;" role="button" name="#posts-${topic.content.id}">1<sup>#</sup></a><a href="javascript:;" role="button" class="btn drawer" data-drawer="#top-page-drawer"><i class="ico-sm mdi mdi-more-vert" aria-hidden="true"></i></a></p>
                    </div>
                </div>
                <div class="embed_topic_item_body">
                    <forum:decorate posts = "${topic.content}" scale="640x360">
                        <forum:blockTip><p class="alert alert-danger" role="alert"><strong>提示:</strong> 作者被禁止发言或内容自动屏蔽</p></forum:blockTip>
                        <forum:editTip><p class="alert alert-warning" role="alert">回复最近由 ${modifyer} 于 ${modifyDate} 编辑</p></forum:editTip>
                    </forum:decorate>
                    <!-- 标签列表  开始-->
                    <!-- 标签列表  结束-->
                </div>
                <!-- 个性签名开始 -->
                <div class="embed_topic_item_foot"></div>
                <!-- 个性签名结束 -->
            </div>
            <!-- 话题内容结束 -->
            <!-- 话题回复开始-->
            <div id="topic_replier_collect" class="page_scroll_pagination" data-handler="${BASE}/topic/replier.json" data-query="author:${topic.memberId},filter:${filterAuthor},scale:640x360" data-function="drawTabletReplier" data-deferred="gatherTabletMood">
            </div>
            <!-- 话题回复结束-->
        </div>
        <!-- 分页开始 -->
        <!-- 分页结束 -->
    </div>
    <footer class="container-fluid main-color" id="embed_page_footer">
        <div class="row">
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/topic/poster" role="button"><i class="ico-sm mdi mdi-link" aria-hidden="true"></i>&nbsp;分享</a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="javascript:;" role="button" data-acctip="false" class="action-cmd action-check like-topic" data-handler="${BASE}/topic/like" data-function="updateLikeCounter" data-check-handler="${BASE}/topic/like/check"><i class="ico-sm mdi mdi-thumb-up" aria-hidden="true"></i>&nbsp;赞</a><sup class="actionCounter likeCounter">${topic.stats.likes}</sup></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="javascript:;" role="button" data-acctip="false" class="action-cmd action-check favorite-topic" data-handler="${BASE}/topic/favorite" data-function="updateFavoriteCounter" data-check-handler="${BASE}/topic/favorite/check"><i class="ico-sm mdi mdi-favorite" aria-hidden="true"></i>&nbsp;收藏</a><sup class="actionCounter favoriteCounter">${topic.stats.favorites}</sup></div>
            <div class="col-xs-3 col-sm-3 text-center">
                <c:choose>
                <c:when test="${topic.status == 'LOCKED'}">
                <a href="javascript:;" role="button" class="disabled"><i class="ico-sm mdi mdi-lock"></i>&nbsp;锁定</a>
                </c:when>
                <c:when test="${topic.normal}">
                <a href="javascript:;" role="button" class="drawer" data-drawer="#quick-reply-drawer"><i class="ico-sm mdi mdi-mail-reply"></i>&nbsp;回复</a>
                </c:when>
                <c:otherwise>
                <a href="javascript:;" role="button" class="disabled"><i class="ico-sm mdi mdi-block"></i>&nbsp;回复</a>
                </c:otherwise>
                </c:choose>
            </div>
        </div>
    </footer>
    <!-- 顶部菜单开始-->
    <div class="embed_drawer_menu main-color" id="top-page-drawer" style="bottom:-300px">
        <h5 class="hl45 pl-4">话题管理菜单<span class="float-right"><a href="javascript:;" class="drawer-close-btn embed_drawer_close ibw-anchor text-center" role="button"><i class="ico mdi mdi-close"></i></a></span></h5>
        <!-- list-unstyled -->
        <ul class="list-inline">
            <!-- 
            <li class="ico-anchor">
                <a href="javascript:;" class="action-ddm" data-handler="${BASE}/topic/favorite">
                  <i class="fa fa-heart"></i>
                  <span>收藏</span>
                </a>
            </li>-->
            <li class="list-inline-item ico-anchor">
                <a href="${BASE}/rss/topic.xml?id=${topic.id}">
                  <i class="ico mdi mdi-rss"></i>
                  <span>订阅</span>
                </a>
            </li>
            <li class="list-inline-item ico-anchor">
                <a href="javascript:;" class="action-ddm" data-handler="${BASE}/topic/delete">
                  <i class="ico mdi mdi-delete"></i>
                  <span>删除</span>
                </a>
            </li>
            <li class="list-inline-item ico-anchor">
                <a href="javascript:;" class="action-ddm" data-handler="${BASE}/topic/lock">
                  <i class="ico mdi mdi-lock"></i>
                  <span>锁定</span>
                </a>
            </li>
            <li class="list-inline-item ico-anchor">
                <a href="javascript:;" class="action-ddm" data-handler="${BASE}/topic/best">
                  <i class="ico mdi mdi-star"></i>
                  <span>精华</span>
                </a>
           </li>
           <li class="list-inline-item ico-anchor">
               <a href="javascript:;" class="action-ddm" data-handler="${BASE}/topic/top">
                 <i class="ico mdi mdi-triangle-up"></i>
                 <span>置顶</span>
               </a>
           </li>
        </ul>
    </div>
    <!-- 顶部菜单结束-->
    <!-- 回复菜单开始-->
    <div class="embed_drawer_menu main-color" id="page-reply-drawer" style="bottom:-260px">
        <h5 class="hl45 pl-4">回复管理菜单</h5>
        <!-- list-unstyled -->
        <ul class="list-unstyled">
            <li class="txt-anchor"><a href="${BASE}/topic/${topic.connect}.xhtml?author=${topic.memberId}" class="ibw-anchor"><i class="ico mdi mdi-account" aria-hidden="true"></i><span>只看楼主</span></a></li>
            <li class="txt-anchor"><a href="javascript:;" class="ibw-anchor replier-report-anchor drawer" data-drawer="#replier-report-drawer" data-drawer-init="replierDrawerMenuInit" data-drawerclose-fun="drawerReportMenuClose"><i class="ico mdi mdi-flag"></i><span>举报</span></a></li>
            <li class="txt-anchor"><a href="javascript:;" class="action-ddm post-action-remove ibw-anchor" data-handler="${BASE}/posts/delete" data-function="cmdDrawerMenuClose"><i class="ico mdi mdi-delete"></i><span>删除</span></a></li>
            <li class="txt-anchor"><a href="javascript:;" class="drawer-close-btn ibw-anchor"><i class="ico mdi mdi-close"></i><span>关闭</span></a></li>
        </ul>
    </div>
    <!-- 回复菜单结束-->
    <!-- 快速回复开始 -->
    <div id="quick-reply-drawer" class="embed_drawer_menu main-color load_content_section" style="bottom:-221px" data-handler="${BASE}/posts/reply/form" data-function="buildTabletRichEditor"></div>
    <!-- 快速回复结束 -->
    <!-- 回复举报菜单开始 -->
    <div id="replier-report-drawer" class="embed_drawer_menu main-color" style="bottom:-260px">
        <form action="${BASE}/posts/report" method="post">
        <h5 class="hl45 pl-4">举报回复<span class="float-right pr-3"><a href="javascript:;" role="button" class="btn btn-sm btn-primary drawer-report-action">提交</a></span></h5>
        <div style="height:60px">
           <ul class="list-inline report-type focus-item-parent">
               <li class="list-inline-item"><a href="javascript:;" class="focus-item" data-type="1">广告</a></li>
               <li class="list-inline-item"><a href="javascript:;" class="focus-item" data-type="2">色情</a></li>
               <li class="list-inline-item"><a href="javascript:;" class="focus-item" data-type="3">政治</a></li>
               <li class="list-inline-item"><a href="javascript:;" class="focus-item" data-type="4">暴力</a></li>
               <li class="list-inline-item"><a href="javascript:;" class="focus-item" data-type="5">语言暴力</a></li>
               <li class="list-inline-item"><a href="javascript:;" class="focus-item focus-current" data-type="6">其它</a></li>
           </ul>
        </div>
        <div style="padding:5px 15px">
            <input type="hidden" name="token" value="${token}" />
            <input type="hidden" name="id" value="" />
            <textarea name="reason" rows="5" cols="60" class="drawer_menu_text" placeholder="在此输入描述内容"></textarea>
        </div>
        <p class="hl60"><a href="javascript:;" class="drawer-close-btn ibw-anchor text-center">关闭</a></p>
        </form>
    </div>
    <!-- 回复举报菜单结束 -->
    <!-- 侧边栏工具条开始 -->
    <section id="sideTool" data-tools="theme"></section>
    <section id="sideToolPlug" data-tool-plug="publish"></section>
    <!-- 侧边栏工具条结束 -->
  </body>
</html>