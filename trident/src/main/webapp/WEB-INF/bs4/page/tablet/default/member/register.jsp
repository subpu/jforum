<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>一起来分享身边的大事件吧</title>
    <style>#embed_page_form{padding:0 15px;padding-top: 20px;padding-bottom: 15px;}</style>
  </head>
  <body>
      <header class="container-fluid main-color" id="embed_page_header">
          <div class="row">
              <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="historyBack"><i class="ico mdi mdi-chevron-left"></i></a></div>
              <div class="col-xs-10 col-sm-10 embed_header_title">注册会员</div>
              <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;"><i class="ico mdi mdi-search"></i></a></div>
          </div>
      </header>
      <div id="embed_page">
          <div class="container">
              <form id="register" method="post" action="${BASE}/member/register" data-submit="once" class="theme-form parsley-form" data-parsley-validate>
              <div id="embed_page_form" class="main-color">
                  <c:if test="${not empty errors}">
                  <div class="alert alert-danger" role="alert">
                      <strong>oOps!</strong> ${errors}
                  </div>
                  </c:if>
                  <div class="form-group">
                      <label for="names">帐号:</label>
                      <small class="form-text text-muted">名称长度必须介于5个字符和20个字符之间，并且只能使用字母数字字符</small>
                      <input type="text" tabindex="1" name="names" size="25" value="${form.names}" class="form-control" placeholder="会员的登录帐号名称" data-parsley-required data-parsley-type="alphanum" data-parsley-length="[5, 20]" data-parsley-remote="${BASE}/member/detection/unique" data-parsley-remote-message="用户名已经存在"/>
                  </div>
                  <div class="form-group">
                      <label for="nickname">昵称:</label>
                      <input type="text" tabindex="2" name="nickname" size="25" maxlength="100" value="${form.nickname}" class="form-control" placeholder="会员的个性昵称" autocomplete="off" data-parsley-required />
                  </div>
                  <div class="form-group" id="pwdbox">
                      <label for="newPswd">密码:</label>
                      <small class="form-text text-muted">必须介于6个字符和100个字符之间</small>
                      <input type="password" tabindex="3" name="newPswd" id="newPswd" size="25" value="${form.newPswd}" class="form-control" placeholder="会员的登录密码" autocomplete="off" data-parsley-required data-parsley-length="[6, 100]" />
                  </div>
                  <div class="form-group">
                      <label for="pswdConfirm">确认密码:</label>
                      <input type="password" tabindex="4" name="pswdConfirm" size="25" value="${form.pswdConfirm}" class="form-control" placeholder="重新输入会员的登录密码" autocomplete="off" data-parsley-required data-parsley-equalto="#newPswd" />
                  </div>
                  <c:if test="${activeInvite}">
                  <div class="form-group">
                      <label for="pswdConfirm">邀请码:</label>
                      <input type="text" name="inviteCode" size="25" value="${form.inviteCode}" class="form-control" placeholder="输入会员的注册邀请码" required="required" />
                  </div>
                  </c:if>
                  <div class="form-group">
                      <div>
                          <input type="checkbox" name="agreed" checked="checked" class="magic-checkbox" />
                          <label>我已阅读并完全同意 <a href="${BASE}/member/register/licenses.xhtml">用户协议及条款内容</a></label>
                      </div>
                  </div>
                  <input type="hidden" name="token" value="${form.token}"/>
                  <button type="submit" data-submit-disabled="请稍后" class="btn btn-primary btn-block" tabindex="6">免费注册</button>
              </div>
              <p class="main-color control-label" style="padding-bottom: 15px;text-align: center;">已有帐号! 马上<a href="${BASE}/member/login">登录</a>进入系统</p>
              </form>
          </div>
      </div>
  </body>
</html>