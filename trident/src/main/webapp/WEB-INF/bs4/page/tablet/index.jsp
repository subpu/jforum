<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>找回帐号密码:第一步</title>
    <style>#embed_page_form{padding:0 15px;padding-top: 20px;padding-bottom: 20px;}</style>
  </head>
  <body>
      <div class="container-fluid main-color" id="embed_page_header">
          <div class="row">
              <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="historyBack"><i class="fa fa-chevron-left"></i></a></div>
              <div class="col-xs-10 col-sm-10 embed_header_title">找回帐号密码:第一步</div>
              <div class="col-xs-1 col-sm-1 text-center"></div>
          </div>
      </div>
      <div id="embed_page">
          <div class="container">
              <form action="${BASE}/lostPswd" method="post" data-submit="once" class="theme-form">
              <div id="embed_page_form" class="main-color">
                  <c:if test="${not empty errors}">
                  <div class="alert alert-danger" role="alert">
                      <strong>oOps!</strong> ${errors}
                  </div>
                  </c:if>
                  
                  <div class="form-group">
                      <label for="email">邮箱</label>
                      <input type="email" class="form-control" name="email" placeholder="请输入帐号绑定的邮箱地址" value="${form.email}" required="required" tabindex="1"/>
                  </div>
                  <div class="form-group">
                      <label for="names">帐号</label>
                      <input type="text" class="form-control" name="names" placeholder="请输入登录用的帐号" value="${form.names}" required="required" tabindex="2"/>
                  </div>
                  <input type="hidden" name="token" value="${form.token}" />
                  <button type="submit" class="btn btn-primary btn-block" tabindex="3">下一步</button>
              </div>
              </form>
          </div>
      </div>
  </body>
</html>