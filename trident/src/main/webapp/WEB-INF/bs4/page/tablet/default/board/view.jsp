<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>${board.title}</title>
  </head>
  <body>
      <header class="container-fluid main-color" id="embed_page_header">
          <div class="row">
              <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="historyBack"><i class="ico mdi mdi-chevron-left"></i></a></div>
              <div class="col-xs-10 col-sm-10"><a href="${BASE}/board/volumes/${board.volumes.connect}.xhtml">${board.volumes.title}</a></div>
              <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="drawer" data-drawer="#top-page-drawer"><i class="ico mdi mdi-more"></i></a></div>
          </div>
      </header>
      <div id="embed_page">
          <div class="container">
              <div class="row" id="embed_page_title">
                  <h5 class="body_header col-xs-12 col-sm-12 default-txt-color">
                      ${board.title}
                      <span class="float-right">
                          <small>
                          <c:choose>
                            <c:when test="${board.status == 'LOCKED'}">
                            <a href="javascript:;" class="btn btn-sm btn-default disabled" role="button"><i class="ico-sm mdi mdi-lock" aria-hidden="true"></i> 锁定</a>
                            </c:when>
                            <c:when test="${board.normal }">
                            <a href="${BASE}/topic/create" class="btn btn-sm btn-primary" role="button"><i class="ico-sm mdi mdi-edit"></i> 新话题</a>
                            </c:when>
                            <c:otherwise>
                            <a href="javascript:;" class="btn btn-sm btn-default disabled" role="button"><i class="ico-sm mdi mdi-block"></i> 新话题</a>
                            </c:otherwise>
                          </c:choose>
                          </small>
                      </span>
                  </h5>
                  <div class="col-xs-12 col-sm-12">
                      <ul class="list-inline" id="board-stats-section">
                          <li class="list-inline-item"><i class="ico-sm mdi mdi-looks"></i> 话题 <span><c:out value="${board.stats.topices}" default="0"/></span></li>
                          <li class="list-inline-item"><i class="ico-sm mdi mdi-comment"></i> 回复 <span><c:out value="${board.stats.postses}" default="0"/></span></li>
                          <li class="list-inline-item"><i class="ico-sm mdi mdi-account"></i> 版主 
                              <ul id="board-moderator" data-params="{&quot;type&quot; : &quot;json&quot;, &quot;id&quot; : &quot;${board.id}&quot;, &quot;target&quot; : &quot;${BASE}/board/moderator/json&quot;}">
                              </ul>
                          </li>
                      </ul>
                  </div>
              </div>
              <!-- 置顶话题开始 -->
              <div class="section">
                  <ul class="board_list_header" data-result="loader.json" data-empty-show="false" data-handler="${BASE}/loader/board/top.json" data-query="board:${board.id}" data-template-function="drawBoardTopSectionTabletTopic">
                      <li class="no-record-tip">正在玩命的搬运中...</li>
                  </ul>
              </div>
              <!-- 置顶话题结束 -->
              <div role="separator" class="divider" style="height:1px;">&nbsp;</div>
              <!-- 话题列表开始 -->
              <ul class="board_list_header mb20 page_scroll_pagination" data-board="${board.id}" id="board_topic_collect" data-handler="${BASE}/board/threads.json" data-query="category:${category}" data-function="drawTabletThreads"></ul>
              <!-- 话题列表结束 -->
          </div>
          <!-- 分页开始 -->
          <!-- 分页结束 -->
      </div>
      <footer class="container-fluid main-color" id="embed_page_footer">
          <div class="row">
              <div class="col-xs-4 col-sm-4 text-center"><a href="javascript:;" role="button"><i class="ico-sm mdi mdi-link" aria-hidden="true"></i>&nbsp;分享</a></div>
              <div class="col-xs-4 col-sm-4 text-center"><a href="javascript:;" role="button" data-acctip="false" class="action-cmd action-check star-board" data-handler="${BASE}/board/star" data-function="updateFavoriteCounter" data-check-handler="${BASE}/board/star/check"><i class="ico-sm mdi mdi-favorite" aria-hidden="true"></i>&nbsp;收藏</a><sup class="actionCounter favoriteCounter">${board.stats.favorites}</sup></div>
              <div class="col-xs-4 col-sm-4 text-center">
                  <c:choose>
                  <c:when test="${board.status == 'LOCKED'}">
                  <a href="javascript:;" class="disabled"><i class="ico-sm mdi mdi-lock" aria-hidden="true"></i>&nbsp;锁定</a>
                  </c:when>
                  <c:when test="${board.normal }">
                  <a href="${BASE}/topic/create" role="button"><i class="ico-sm mdi mdi-edit"></i>&nbsp;新话题</a>
                  </c:when>
                  <c:otherwise>
                  <a href="javascript:;" class="disabled"><i class="ico-sm mdi mdi-block" aria-hidden="true"></i>&nbsp;新话题</a>
                  </c:otherwise>
                  </c:choose>
              </div>
          </div>
      </footer>
      <!-- 顶部菜单开始-->
      <div class="embed_drawer_menu main-color" id="top-page-drawer">
        <h5 class="hl45 pl-4">版块管理菜单<span class="float-right"><a href="javascript:;" class="drawer-close-btn embed_drawer_close ibw-anchor text-center" role="button"><i class="ico mdi mdi-close"></i></a></span></h5>
        <!-- list-unstyled -->
        <ul class="list-inline">
            <!-- 
            <li class="ico-anchor">
                <a href="javascript:;" class="action-ddm" data-handler="${BASE}/board/star">
                  <i class="fa fa-heart"></i>
                  <span>收藏</span>
                </a>
            </li>-->
            <li class="list-inline-item ico-anchor">
                <a href="${BASE}/rss/board.xml?id=${board.id}&volumes=${board.volumesId}">
                  <i class="ico mdi mdi-rss"></i>
                  <span>订阅</span>
                </a>
            </li>
            <li class="list-inline-item ico-anchor">
                <a href="javascript:;" class="action-ddm" data-handler="${BASE}/board/lock">
                  <i class="ico mdi mdi-lock-outline"></i>
                  <span>锁定</span>
                </a>
            </li>
            <li class="list-inline-item ico-anchor">
                <a href="javascript:;" class="action-ddm" data-handler="${BASE}/board/unlock">
                  <i class="ico mdi mdi-lock-open"></i>
                  <span>解锁</span>
                </a>
            </li>
        </ul>
      </div>
      <!-- 顶部菜单结束-->
      <!-- 侧边栏工具条开始 -->
      <section id="sideTool" data-tools="theme"></section>
      <section id="sideToolPlug" data-tool-plug="publish"></section>
      <!-- 侧边栏工具条结束 -->
  </body>
</html>