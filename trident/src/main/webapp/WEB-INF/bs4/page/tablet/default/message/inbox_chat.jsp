<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>消息内容</title>
<style>
html,body{margin:0;padding:0;width:100%;height:100%;}
#embed_page{margin: auto 0;width:100%;height:100%}
.letter-embed{position:relative}
.letter-item-collect{overflow-y:scroll;height: inherit;border-style:none}
.media{position:relative}
.media{position:relative;width:60%;clear:both;margin-top:1rem}
.media .media-body{margin-left: 10px;}
.media-master{float:right}
.media-master .media-avatar{position: absolute;right: 0;top: 0;}
.media-master .media-heading{text-align:right;}
.media-master .media-body:before{right:80px;left:auto;}
.media-heading small{color:#333}
.media-body:before{
    content: "";
    position: absolute;
    width: 10px;
    height: 10px;
    background-color: #f1f1f1;
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg);
    top: 15.5px;
    left: 70px;
}
.media-bubble{
    width: auto;
    background-color:#f1f1f1;
    padding: 10px;
    float: left;
    border-radius: 5px;
    color:#333;
}
.media-master .media-bubble{float:right;padding-right:10px;margin-right: 85px;}
.snowhite .media-bubble a{color:#007bff}
.snowhite .media-master .media-bubble{background-color:#007bff;color:#fff;}
.snowhite .media-master .media-body:before{background-color:#007bff}
.snowhite .letter-item-collect .media-master .media-heading small{color:#fff}
.jetblack .media-bubble a{color:#575f69}
.jetblack .media-master .media-bubble{background-color:#575f69;color:#ccc;}
.jetblack .media-master .media-body:before{background-color:#575f69}
.jetblack .letter-item-collect .media-master .media-heading small{color:#ccc}
</style>
  </head>
  <body>
    <header class="container-fluid main-color" id="embed_page_header">
        <div class="row">
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="historyBack"><i class="ico mdi mdi-chevron-left"></i></a></div>
            <div class="col-xs-10 col-sm-10 embed_header_title">与${senderNickname}往来的消息</div>
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="drawer" data-drawer="#top-page-drawer"><i class="ico mdi mdi-more"></i></a></div>
        </div>
    </header>
    <div id="embed_page">
        <div class="container" style="height:100%">
            <!-- 消息记录正区开始 -->
            <div class="letter-item-collect" id="message-${sender}-box" data-page="1" data-handler="${BASE}/message/view/more?sender=${sender}">
                <p class="text-center<c:if test="${not hasMore}"> hide</c:if>"><a class="txt-sm p-2 letter-more-action"  href="javascript:;">更多历史消息</a></p>
                <c:forEach items="${rs}" var="forumLetter">
                <div class="media letter-embed<c:if test="${forumLetter.author == master.id}"> media-master</c:if>" id="letter-${forumLetter.id}">
                    <div class="media-avatar media-left">
                        <a href="javascript:;">
                            <img class="media-object img-circle" src="${BASE}/member/avatar/${forumLetter.author}.png" alt="${forumLetter.nickname}" style="height:64px;width:64px;">
                        </a>
                    </div>
                    <div class="media-body">
                        <div class="media-bubble">
                            <h6 class="media-heading"><small><forum:format value="${forumLetter.entryDateTime}"/></small></h6>
                            <c:if test="${forumLetter.author == 0}">
                            <strong>${forumLetter.title}</strong>
                            </c:if>
                            <p>${forumLetter.content}</p>
                        </div>
                    </div>
                </div>
                </c:forEach>
            </div>
        </div>
    </div>
    <!-- 消息记录正区结束 -->
    <c:if test="${sender > 0}">
    <footer class="container-fluid main-color" id="embed_page_footer">
        <div class="input-group pt-2">
            <input type="text" class="form-control drawer" placeholder="点击回复消息" data-drawer="#message-reply-drawer" readonly="readonly"/>
            <span class="input-group-append"><button class="btn input-group-text"><i class="ico mdi mdi-mail-send"></i></button></span>
        </div>
    </footer>
    </c:if>
    <!-- 顶部菜单开始-->
    <div class="embed_drawer_menu main-color" id="top-page-drawer">
        <h5 class="hl45 pl-4">消息管理菜单</h5>
        <!-- list-unstyled -->
        <ul class="list-unstyled">
            <li class="txt-anchor"><a href="javascript:;" class="action-ddm ibw-anchor" data-handler="${BASE}/message/read/all?member=${sender}&direction=1"><i class="ico mdi mdi-assignment-check"></i><span>全部标记为已读</span></a></li>
            <li class="txt-anchor"><a href="javascript:;" class="drawer-close-btn ibw-anchor"><i class="ico mdi mdi-close"></i><span>关闭</span></a></li>
        </ul>
    </div>
    <!-- 顶部菜单结束-->
    <!-- 消息回复表单开始 -->
    <div class="embed_drawer_menu main-color" id="message-reply-drawer">
        <form id="send-message-form" data-submit="once">
        <h5 class="hl45 pl-4">回复消息<span class="float-right pr-3"><a tabindex="2" role="button" class="btn btn-sm btn-primary drawer-message-send" href="javascript:;" data-handler="${BASE}/message/transmit" data-query="receiver:${sender},names:${senderNickname}">发送</a></span></h5>
        <div style="padding:5px 15px">
            <input type="hidden" name="token" value="${token}" />
            <textarea name="content" rows="2" cols="60" class="drawer_menu_text form-control" placeholder="在此输入消息内容" style="border-style:none;border-radius:0"></textarea>
        </div>
        </form>
        <p class="hl60"><a href="javascript:;" class="drawer-close-btn ibw-anchor text-center">关闭</a></p>
    </div>
    <!-- 消息回复表单结束 -->
  </body>
</html>