<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>帐户设置</title>
  </head>
  <body>
    <header class="container-fluid main-color" id="embed_page_header">
        <div class="row">
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="historyBack"><i class="ico mdi mdi-chevron-left"></i></a></div>
            <div class="col-xs-10 col-sm-10">
                <ul class="list-inline header_menu">
                  <li class="list-inline-item focus"><a href="javascript:;">个性签名</a></li>
                  <li class="list-inline-item"><a href="${BASE}/member/home/avatar">选择头像</a></li>
                  <li class="list-inline-item"><a href="${BASE}/member/home/passport">修改密码</a></li>
                  <li class="list-inline-item"><a href="${BASE}/member/home/history">历史记录</a></li>
                </ul>
            </div>
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;"><i class="ico mdi mdi-search"></i></a></div>
        </div>
    </header>
    <div id="embed_page">
        <div class="container">
            <div class="main-color p-3">
                <c:if test="${not empty errors}">
                <div class="alert alert-danger" role="alert">
                    <strong>oOps!</strong> ${errors}
                </div>
                </c:if>
                <form method="post" action="${BASE}/member/home/profile" id="member_profile_form" data-submit="once" class="theme-form">
                <div class="form-group">
                    <label class="control-label" for="nickname">昵称:</label>
                    <input type="text" name="nickname" class="inputbox no-radius form-control w600" value="${form.nickname}" readonly="readonly" required="required" tabindex="1"/>
                </div>
                <div class="form-group">
                    <label class="control-label" for="signature">个性签名:</label>
                    <textarea name="signature" class="form-control h150" style="resize:none;" tabindex="2">${form.signature}</textarea>
                    <small class="form-text text-muted">&#x1F4A1 &nbsp;只允许纯文字;不可以包含连接,图片,UBB代码和脚本代码</small>
                </div>
                <div class="form-group">
                    <input type="hidden" name="token" value="${form.token}"/>
                    <input type="hidden" name="record" value="${form.record}"/>
                    <input type="submit" name="send" value="提交更新" class="btn btn-primary btn-block" tabindex="3"/>
                </div>
                </form>
            </div>
        </div>
    </div>
    <footer class="container-fluid main-color" id="embed_page_footer">
        <div class="row">
            <div class="col-xs-3 col-sm-3 text-center"><a href="javascript:;" role="button" class="btn btn-primary"><i class="ico-sm mdi mdi-settings"></i>&nbsp;设置</a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/member/home/topic" role="button"><i class="ico-sm mdi mdi-looks"></i>&nbsp;话题</a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/message/" role="button" id="primary-panel-notice"><i class="ico-sm mdi mdi-notifications"></i>&nbsp;消息&nbsp;<sup class="device-notice-ele"></sup></a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/member/home/" role="button"><i class="ico-sm mdi mdi-account"></i>&nbsp;我</a></div>
        </div>
    </footer>
  </body>
</html>