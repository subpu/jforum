<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
                           <ul class="pager">
                               <forum:paginate size="${param.showSize}" total="${param.records}" url="${param.url}">
                               <li><a href="${prevPageLink}"><span class="ico mdi mdi-chevron-left" aria-hidden="true"></span></a></li>
                               <li class="pager_number_range">${currentPage} / ${totalPageSize}</li>
                               <li><a href="${nextPageLink}"><span class="ico mdi mdi-chevron-right" aria-hidden="true"></span></a></li>
                               </forum:paginate>
                            </ul>