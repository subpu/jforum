<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>个人中心</title>
    <style>.rht-link{float:right;display:inline-block;padding:0 1rem}</style>
  </head>
  <body>
    <header class="container-fluid main-color" id="embed_page_header">
        <div class="row">
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="historyBack"><i class="ico mdi mdi-chevron-left"></i></a></div>
            <div class="col-xs-10 col-sm-10 embed_header_title">我</div>
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;"><i class="ico mdi mdi-search"></i></a></div>
        </div>
    </header>
    <div id="embed_page">
        <div class="container">
            <div class="member-bg-${member.style}" id="member-header">
              <div class="row">
                <div class="col-xs-2 col-sm-2">
                    <span id="member-header-avatar">
                        <img class="rounded-circle" src="${BASE}/member/avatar/${member.id}.png" width="64px" height="64px"/>
                    </span>
                </div>
                <div class="col-xs-10 col-sm-10">
                    <div id="member-header-body">
                        <h5>${member.names} @u${member.id}</h5>
                        <ul class="list-inline" id="member-info">
                            <li class="list-inline-item">组: ${member.groupNames}</li>
                            <li class="list-inline-item">角色: ${member.roleNames}</li>
                            <li class="list-inline-item">注册日期: ${member.logonDateTime}</li>
                            <li class="list-inline-item">上次登录: ${member.prevLoginDateTime}</li>
                        </ul>
                        <p>${member.signature}</p>
                    </div>
                </div>
              </div>
            </div>
            <div class="main-color" id="member-stats-set">
              <div class="row">
                <div class="col-xs-3 col-sm-3"><dl><dt>话题</dt><dd>${member.threads}</dd></dl></div>
                <div class="col-xs-3 col-sm-3"><dl><dt>回复</dt><dd>${member.replies}</dd></dl></div>
                <div class="col-xs-3 col-sm-3"><dl><dt>积分</dt><dd>${member.score}</dd></dl></div>
                <div class="col-xs-3 col-sm-3"><dl><dt>等级</dt><dd>${member.levelNo} / ${member.level}</dd></dl></div>
              </div>
            </div>
            <div id="member_set_section">
                <div class="embed_nr">
                    <h5 class="embed_nr_title topic_normal_color default-txt-color">设置</h5>
                    <ul class="list-unstyled embed_nr_childs main-color">
                        <li><a class="d-block" role="button" href="${BASE}/member/home/profile">个性签名<span class="rht-link"><i class="ico mdi mdi-chevron-right"></i></span></a></li>
                        <li><a class="d-block" role="button" href="${BASE}/member/home/avatar">选择头像<span class="rht-link"><i class="ico mdi mdi-chevron-right"></i></span></a></li>
                        <li><a class="d-block" role="button" href="${BASE}/member/home/passport">修改密码<span class="rht-link"><i class="ico mdi mdi-chevron-right"></i></span></a></li>
                        <li><a class="d-block" role="button" href="${BASE}/member/home/history">历史记录<span class="rht-link"><i class="ico mdi mdi-chevron-right"></i></span></a></li>
                    </ul>
                </div>
            </div>
            <div id="member_topic_section">
                <div class="embed_nr">
                    <h5 class="embed_nr_title topic_normal_color default-txt-color">话题</h5>
                    <ul class="list-unstyled embed_nr_childs main-color">
                     <li><a class="d-block" role="button" href="${BASE}/member/home/topic">发布的话题<span class="rht-link"><i class="ico mdi mdi-chevron-right"></i></span></a></li>
                     <li><a class="d-block" role="button" href="${BASE}/member/home/posts">回复的话题<span class="rht-link"><i class="ico mdi mdi-chevron-right"></i></span></a></li>
                     <li><a class="d-block" role="button" href="${BASE}/member/home/topic/favorite">收藏的话题<span class="rht-link"><i class="ico mdi mdi-chevron-right"></i></span></a></li>
                     <li><a class="d-block" role="button" href="${BASE}/member/home/topic/like">点赞的话题<span class="rht-link"><i class="ico mdi mdi-chevron-right"></i></span></a></li>
                    </ul>
                </div>
            </div>
            <p class="bg-default hl60 text-center"><a href="${BASE}/member/offline.xhtml?trace=msa">注销</a></p>
        </div>
    </div>
    <footer class="container-fluid main-color" id="embed_page_footer">
        <div class="row">
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/home" role="button"><i class="ico-sm mdi mdi-home"></i>&nbsp;首页</a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/board/home" role="button"><i class="ico-sm mdi mdi-layers"></i>&nbsp;版块</a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/message/" role="button" id="primary-panel-notice"><i class="ico-sm mdi mdi-notifications"></i>&nbsp;消息&nbsp;<sup class="device-notice-ele"></sup></a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/member/home/" role="button" class="btn btn-primary"><i class="ico-sm mdi mdi-account"></i>&nbsp;我</a></div>
        </div>
    </footer>
</body>
</html>