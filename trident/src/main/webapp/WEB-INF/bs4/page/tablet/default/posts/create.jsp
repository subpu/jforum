<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>回复话题</title>
  </head>
  <body>
    <header class="container-fluid main-color" id="embed_page_header">
        <div class="row">
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="historyBack"><i class="ico mdi mdi-chevron-left"></i></a></div>
            <div class="col-xs-10 col-sm-10 embed_header_title">回复: <a href="${BASE}/topic/${topic.connect}.xhtml">${topic.title}</a></div>
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;"><i class="ico mdi mdi-search"></i></a></div>
        </div>
    </header>
    <div id="embed_page">
        <div class="container">
            <div class="main-color p-3">
                <c:if test="${not empty errors}">
                <div class="alert alert-danger" role="alert">
                    <strong>oOps!</strong> ${errors}
                </div>
                </c:if>
                <c:if test="${not empty infoTip}"> 
                <div class="alert alert-info alert-dismissible fade show" role="alert" style="margin-left:15px;margin-right:15px">
                    <strong>提示!</strong> ${infoTip}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                </c:if>
                <form method="post" action="${BASE}/posts/create" data-submit="once" class="theme-form">
                <div class="form-group row">
                    <div class="col-sm-12">
                        <textarea id="content" name="content" class="richeditor" data-query="height:450,width:100%,upload:/upload/ckeditor" required="required" tabindex="1">${form.content}</textarea>
                    </div>
                </div>
                <div class="form-group row justify-content-end">
                    <div class="col-sm-4 text-right">
                        <input type="hidden" name="token" value="${form.token}"/>
                        <input type="hidden" name="topic" value="${form.topic}" />
                        <input type="hidden" name="board" value="${form.board}" />
                        <input type="hidden" name="volumes" value="${form.volumes}" />
                        <input type="hidden" name="quoteId" value="${form.quoteId}" />
                        <input type="hidden" name="quoteFloor" value="${form.quoteFloor}" />
                        <input type="hidden" name="quoteScale" value="${form.quoteScale}" />
                        <input type="submit" name="reply" value="提交回复" class="btn btn-primary" tabindex="2"/>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
  </body>
</html>