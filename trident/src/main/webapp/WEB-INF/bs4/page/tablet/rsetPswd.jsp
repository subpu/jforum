<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>找回帐号密码:第二步</title>
    <style>#embed_page_form{padding:0 15px;padding-top: 20px;padding-bottom: 20px;}</style>
  </head>
  <body>
      <div class="container-fluid main-color" id="embed_page_header">
          <div class="row">
              <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="historyBack"><i class="fa fa-chevron-left"></i></a></div>
              <div class="col-xs-10 col-sm-10 embed_header_title">找回帐号密码:第二步</div>
              <div class="col-xs-1 col-sm-1 text-center"></div>
          </div>
      </div>
      <div id="embed_page">
          <div class="container">
              <form action="${BASE}/rsetPswd" method="post" data-submit="once" class="theme-form">
              <div id="embed_page_form" class="main-color">
                  <div class="alert alert-warning alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <strong>Warning!</strong> 请在3分钟内完成操作,超时找回密码将以失败告终
                  </div>
                  <c:if test="${not empty errors}">
                  <div class="alert alert-danger" role="alert">
                      <strong>oOps!</strong> ${errors}
                  </div>
                  </c:if>
                  
                  <div class="form-group">
                      <label for="newpswd">新密码</label>
                      <span class="help-block with-errors">必须介于6个字符和100个字符之间</span>
                      <input type="password" class="form-control" name="newpswd" placeholder="请输入新的登录密码" required="required" tabindex="1"/>
                  </div>
                  <div class="form-group">
                      <label for="confirmpswd">确认密码</label>
                      <span class="help-block with-errors">必须介于6个字符和100个字符之间</span>
                      <input type="password" class="form-control" name="confirmpswd" placeholder="请重新确认新的密码" required="required" tabindex="2"/>
                  </div>
                  <input type="hidden" name="token" value="${form.token}" />
                  <input type="hidden" name="record" value="${form.record}" />
                  <input type="hidden" name="disi" value="${form.disi}" />
                  <button type="submit" class="btn btn-primary btn-block" tabindex="3">下一步</button>
              </div>
              </form>
          </div>
      </div>
  </body>
</html>