<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>${boardGroup.title}</title>
  </head>
  <body>
    <header class="container-fluid main-color" id="embed_page_header">
        <div class="row">
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="historyBack"><i class="ico mdi mdi-chevron-left"></i></a></div>
            <div class="col-xs-10 col-sm-10 embed_header_title">${boardGroup.title}</div>
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;"><i class="ico mdi mdi-search"></i></a></div>
        </div>
    </header>
    <div id="embed_page">
        <div class="container">
            <div data-result="batch.jsonp" data-handler="${BASE}/loader/board/stats.jsonp" data-function="loadBoardStatsTabletResult" data-param="ids" data-record="li[data-board]">
                <div class="embed_nr w-100">
                    <!-- 版块列表开始 -->
                    <ul class="list-unstyled embed_nr_childs main-color">
                        <c:forEach items="${boardes}" var="board">
                        <li data-board="${board.id}" class="icon forum_${board.icoStatus}">
                            <div class="embed_board_item">
                                <h6><a href="${BASE}/board/${board.connect}.xhtml">${board.title}</a>&nbsp;<small class="float-right badge bubble today-counter mr-1">${board.stats.todayTopices}</small></h6>
                                <p class="txt-sm">${board.description}</p>
                            </div>
                        </li>
                        </c:forEach>
                    </ul>
                    <!-- 版块列表结束 -->
                </div>
            </div>
            <!-- 话题列表开始 -->
            <ul class="board_list_header" id="board_topic_collect" data-result="loader.json" data-handler="${BASE}/loader/volumes/topic/recent.json" data-query="id:${boardGroup.id}" data-template-function="drawTabletThreads">
            </ul>
            <!-- 话题列表结束 -->
        </div>
    </div>
    <footer class="container-fluid main-color" id="embed_page_footer">
        <div class="row">
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/home" role="button"><i class="ico-sm mdi mdi-home"></i>&nbsp;首页</a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/board/home" role="button"><i class="ico-sm mdi mdi-layers"></i>&nbsp;版块</a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/message/" role="button"><i class="ico-sm mdi mdi-notifications"></i>&nbsp;消息&nbsp;<sup class="device-notice-ele"></sup></a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/member/home/" role="button"><i class="ico-sm mdi mdi-account"></i>&nbsp;我</a></div>
        </div>
    </footer>
    <!-- 侧边栏工具条开始 -->
    <section id="sideTool" data-tools="theme"></section>
    <!-- 侧边栏工具条结束 -->
  </body>
</html>