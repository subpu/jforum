<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>选择头像</title>
  </head>
  <body>
    <header class="container-fluid main-color" id="embed_page_header">
        <div class="row">
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="historyBack"><i class="ico mdi mdi-chevron-left"></i></a></div>
            <div class="col-xs-10 col-sm-10">
                <ul class="list-inline header_menu">
                  <li class="list-inline-item"><a href="${BASE}/member/home/profile">个性签名</a></li>
                  <li class="list-inline-item focus"><a href="javascript:;">选择头像</a></li>
                  <li class="list-inline-item"><a href="${BASE}/member/home/passport">修改密码</a></li>
                  <li class="list-inline-item"><a href="${BASE}/member/home/history">历史记录</a></li>
                </ul>
            </div>
            <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;"><i class="ico mdi mdi-search"></i></a></div>
        </div>
    </header>
    <div id="embed_page">
        <div class="container">
            <div class="main-color">
                <div class="alert alert-info" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>&#x1F4A1 &nbsp;点击图片变更头像. 当前选中的头像为绿色边框
                </div>
                <div class="row mt20" id="avatarBox">
                    <!-- foreach -->
                    <c:forEach items="${rs}" var="defAvtar">
                    <div class="col-xs-4 col-sm-3 text-center">
                        <a href="javascript:;" class="thumbnail trans-bg member-avatar-section">
                            <c:set var="curAvtar" value="${fn:substringAfter(defAvtar, 'avatar')}"></c:set>
                            <img src="${defAvtar}" class="rounded-circle member_default_avtar<c:if test="${curAvtar eq active }"> active</c:if>" data-current="${active}" data-path="${curAvtar}" style="width:100px;height:100px"/>
                        </a>
                    </div>
                    </c:forEach>
                    <!-- foreach -->
                </div>
            </div>
        </div>
    </div>
    <footer class="container-fluid main-color" id="embed_page_footer">
        <div class="row">
            <div class="col-xs-3 col-sm-3 text-center"><a href="javascript:;" role="button" class="btn btn-primary"><i class="ico-sm mdi mdi-settings"></i>&nbsp;设置</a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/member/home/topic" role="button"><i class="ico-sm mdi mdi-looks"></i>&nbsp;话题</a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/message/" role="button" id="primary-panel-notice"><i class="ico-sm mdi mdi-notifications"></i>&nbsp;消息&nbsp;<sup class="device-notice-ele"></sup></a></div>
            <div class="col-xs-3 col-sm-3 text-center"><a href="${BASE}/member/home/" role="button"><i class="ico-sm mdi mdi-account"></i>&nbsp;我</a></div>
        </div>
    </footer>
  </body>
</html>