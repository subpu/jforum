<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>话题海报</title>
    <style>
    #poster-head{height:400px;background-repeat:no-repeat;background-position:center top;background-color:#fff}
    #poster-body{padding:20px;background-color: #fff;}#poster-body article h3{line-height: 2em;height: 1.5em;overflow: hidden;}#body-detail{padding:20px 0;line-height:1.6em;color:#666}
    #poster-footer{border-top:1px dashed #ddd;height:200px;padding-top:40px}.table-cell-w300{display:table-cell;width:300px;}.table-cell-auto{display:table-cell;width:auto}
    .pl10{padding-left:10px;}.pl160{padding-left:160px}
    </style>
  </head>
  <body>
      <div class="container-fluid main-color" id="embed_page_header">
          <div class="row">
              <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;" class="historyBack"><i class="ico mdi mdi-chevron-left"></i></a></div>
              <div class="col-xs-10 col-sm-10 embed_header_title">话题海报</div>
              <div class="col-xs-1 col-sm-1 text-center"><a href="javascript:;"><i class="ico mdi mdi-search"></i></a></div>
          </div>
      </div>
      <div id="embed_page">
          <div class="container" id="poster-page">
            <div id="poster">
                <div id="poster-head" style="background-image:url(${cover});"></div>
                <div id="poster-body">
                    <article>
                        <h5>${topic.title}</h5>
                        <div id="body-detail">${topic.summary}</div>
                    </article>
                    <div id="poster-footer">
                        <div class="table-cell-w300">
                            <div class="pl10">
                                <div class="table-cell-auto">
                                  <img src="${BASE}/member/avatar/${topic.memberId}.png" class="img-circle" width="100px" height="100px"/>
                                </div>
                                <div class="table-cell-auto pl10" style="vertical-align: middle">
                                    <ul class="list-unstyled">
                                        <li><a href="${BASE}/member/${topic.memberId}.xhtml">${topic.memberNickname}</a></li>
                                        <li>${profile.level}</li>
                                        <li>${profile.levelNo}级</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="table-cell-w300 pl160"><img src="${BASE}/topic/qrcode/${topic.id}.png"/></div>
                    </div>
                </div>
            </div>
          </div>
      </div>
    <!-- 侧边栏工具条开始 -->
    <section id="sideTool">
        <ul>
            <li><a id="poster-back-action" href="${BASE}/topic/${topic.connect}.xhtml" title="返回话题"><i class="ico mdi mdi-arrow-left"></i></a></li>
            <li><a id="poster-share-action" href="javascript:;" title="分享海报"><i class="ico mdi mdi-share"></i></a></li>
            <li><a id="poster-down-action" href="javascript:;" data-handler="${BASE}/topic/poster/down" data-query="topic:${topic.id}" title="下载海报"><i class="ico mdi mdi-download"></i></a></li>
        </ul>
    </section>
    <!-- 侧边栏工具条结束 -->
  </body>
</html>