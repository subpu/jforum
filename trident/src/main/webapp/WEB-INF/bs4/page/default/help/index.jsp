<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>${page.title}</title>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                    <span class="crumb">
                        ${page.title}
                    </span>
                </li>
            </ul>
            <%@ include file = "../include/page_search.jsp" %>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container main-color">
            <div class="row" style="width: 100%;clear: both;padding: 0 15px;">
                <div class="w-100" style="clear:both">${page.body}</div>
                <c:forEach items="${page.entries}" var="entry">
                <div class="w-100" style="clear:both;margin:15px 0">
                    <h5 class="text-left"><a href="${entry.link}">${entry.title}</a></h5>
                    <p>${entry.summary}</p>
                </div>
                </c:forEach>
            </div>
        </div>
    </div>
    <%@ include file = "../include/footer.jsp" %>
  </body></html>