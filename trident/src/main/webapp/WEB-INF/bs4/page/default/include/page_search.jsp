<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
             <div id="global_search_form">
                <form action="${DOMAIN}/search/" method="get">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="" name="word" style="height:auto"/>
                    <span class="input-group-append"><button class="btn input-group-text" type="sumbit"><i class="ico mdi mdi-search"></i></button></span>
                </div>
                </form>
            </div>