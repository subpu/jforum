<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>创建话题</title>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                    <span class="crumb">创建话题</span>
                </li>
            </ul>
            <%@ include file = "../include/page_search.jsp" %>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container main-color" style="padding-top:20px;margin-top:50px;">
            <c:if test="${not empty errors}">
            <div class="alert alert-danger" role="alert">
                <strong>oOps!</strong> ${errors}
            </div>
            </c:if>
            <form method="post" action="${BASE}/topic/create" data-submit="once" class="theme-form">
            <div class="form-group row">
                <div class="col-md-12 row">
                    <div class="col-md-2">
                        <select class="asyn-loadate-select form-control" name="category" data-handler="${BASE}/loader/board/category.json?volumes=${form.volumes}&board=${form.board}" data-current="${form.category}" required="required" tabindex="1">
                            <option value="">选择话题类型</option>
                        </select>
                    </div>
                    <div class="col-md-10">
                        <input type="text" name="title" class="form-control" value="${form.title}" placeholder="主题在此输入" required="required" tabindex="2"/>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <textarea id="content" name="content" data-richEditor="CKEditor" data-query="height:450,width:100%,upload:/upload/ckeditor" required="required" tabindex="3">${form.content}</textarea>
                </div>
            </div>
            <div class="form-inline justify-content-end pb-3">
                <div class="form-group mx-sm-2">
                    <label class="form_label_title mx-sm-2">选择话题发布至的版块</label>
                    <select name="volumes" data-current="${form.volumes}" class="asyn-loadate-select" id="asyn-volumes-select" data-handler="${BASE}/loader/volumes/usable.json" required="required" tabindex="4">
                        <option value="">请选择版块组</option>
                     </select>
                </div>
               <div class="form-group mx-sm-2">
                    <select name="board" data-current="${form.board}" class="selectpicker" id="asyn-board-select" data-handler="${BASE}/loader/volumes/board.json" required="required" tabindex="5">
                        <option value="">请选择版块</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="hidden" name="token" value="${form.token}"/>
                    <input type="submit" name="publish" value="发布话题" class="btn btn-primary" tabindex="6"/>
                </div>
            </div>
            </form>
        </div>
    </div>
    <%@ include file = "../include/footer.jsp" %>
  </body></html>