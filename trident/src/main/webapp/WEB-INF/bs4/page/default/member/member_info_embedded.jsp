<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set value="${pageContext.request.contextPath}" var="BASE"/>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
      <div style="padding-top:30px;">
          <div class="text-center" style="height:auto;">
              <a href="${BASE}/member/${member.id}.xhtml" title="会员主页"><img class="img-circle" src="${BASE}/member/avatar/${member.id}.png" style="width:100px;height:100px" /></a>
          </div>
          <div class="text-center member_embed_info">
              <h5><a href="${BASE}/member/home/" title="会员中心" class="member-${member.style}">${member.nickname}</a></h5>
              <div class="row">
                  <dl class="col-xs-6 col-sm-4"><dt>${member.status}</dt><dd>状态</dd></dl>
                  <dl class="col-xs-6 col-sm-4"><dt>${member.groupNames}</dt><dd>组</dd></dl>
                  <dl class="col-xs-6 col-sm-4"><dt>${member.roleNames}</dt><dd>角色</dd></dl>
              </div>
          </div>
      </div>
  </body>
</html>