<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>${board.title}</title>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                    <span class="crumb">
                        <a href="${BASE}/board/volumes/${board.volumes.connect}.xhtml">${board.volumes.title}</a>
                    </span>
                </li>
            </ul>
            <%@ include file = "../include/page_search.jsp" %>
        </div>
    </div>
    <div class="container">
        <div style="clear:both">
            <h5 class="default-txt-color">${board.title}</h5>
            <ul class="list-inline" id="board-stats-section">
                <li class="list-inline-item"><i class="ico mdi mdi-looks"></i> 话题 <span id="stats-threads"><c:out value="${board.stats.topices}" default="0"/></span></li>
                <li class="list-inline-item"><i class="ico mdi mdi-comment"></i> 回复 <span id="stats-replies"><c:out value="${board.stats.postses}" default="0"/></span></li>
                <li class="list-inline-item section">
                    <i class="ico mdi mdi-account"></i> 版主 
                    <div class="lb" data-result="loader.json" data-empty-show="false" data-handler="${BASE}/loader/board/moderator.json" data-query="id:${board.id}" data-template-function="drawBoardModeratorList">
                        <p class="no-record-tip" style="padding:0;background-color: transparent;">loading...</p>
                    </div>
                </li>
            </ul>
        </div>
        <!-- 版块统计结束 -->
        <!-- 轮播图开始-->
        <div class="row" data-carousel="bootstrap" data-handler="${BASE}/topic/carousel/${board.volumes.id}-${board.id}.json"></div>
        <!-- 轮播图结束-->
        <!-- 顶部区功能开始 -->
        <div class="row">
            <div class="col-md-6">
                <c:choose>
                <c:when test="${board.status == 'LOCKED'}">
                <a href="javascript:;" class="btn btn-secondary disabled" role="button"><i class="ico-sm mdi mdi-lock" aria-hidden="true"></i> 锁定</a>
                </c:when>
                <c:when test="${board.normal }">
                <a href="${BASE}/topic/create" class="btn btn-primary" role="button"><i class="ico-sm mdi mdi-edit"></i> 新话题</a>
                </c:when>
                <c:otherwise>
                <a href="javascript:;" class="btn btn-secondary disabled" role="button"><i class="ico-sm mdi mdi-block"></i> 新话题</a>
                </c:otherwise>
                </c:choose>
                <!-- 版块工具开始 -->
                <div class="btn-group" data-result="click.jsonp" data-handler="${BASE}/board/tool.jsonp" data-function="buildMenuList" data-selector=".board-menu-list">
                    <button type="button" class="btn btn-secondary"><i class="ico-sm mdi mdi-wrench"></i> &nbsp;工具</button>
                    <button type="button" class="btn btn-secondary dropdown-toggle-split caret-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu board-menu-list"></div>
                </div>
                <!-- 收藏 -->
                <div class="btn-group" role="group" aria-label="喜欢都收藏呗">
                    <a role="button" href="javascript:;" class="btn btn-danger action-cmd action-check star-board" data-acctip="false" data-handler="${BASE}/board/star" data-function="updateBoardFavoriteCounter" data-check-handler="${BASE}/board/star/check" data-check-function="checkBoardFavoriteStatus"><i class="ico-sm mdi mdi-favorite" aria-hidden="true"></i> 收藏</a>
                    <button type="button" class="btn btn-light favoriteCounter" disabled="disabled">${board.stats.favorites}</button>
                </div>
                <!-- 版块工具结束 -->
            </div>
            <div class="col-md-6">
                <!-- 分页开始 -->
                <div id="page_another_navigation" class="frinner"></div>
                <!-- 分页结束 -->
            </div>
        </div>
        <!-- 顶部区功能区结束 -->
        <div class="mt-3">
            <div class="board_list">
                <ul class="board_list_header subject-color">
                    <li class="hl60">
                        <dl class="icon">
                            <dt>
                                <div class="ti20">版块主题</div>
                            </dt>
                            <dd class="wp10">回复数</dd>
                            <dd class="wp10">查看数</dd>
                            <dd class="latest">最后回复</dd>
                        </dl>
                    </li>
                </ul>
                <!-- 置顶话题开始 -->
                <div class="section">
                    <ul class="board_list_header" data-result="loader.json" data-empty-show="false" data-handler="${BASE}/loader/board/top.json" data-query="board:${board.id}" data-template-function="drawBoardTopSectionTopic">
                       <li class="no-record-tip">正在玩命的搬运中...</li>
                    </ul>
                </div>
                <!-- 置顶话题结束 -->
                <div role="separator" class="divider" style="height:1px;">&nbsp;</div>
                <!-- 话题列表开始 -->
                <ul class="board_list_header mb20" data-board="${board.id}" id="board_topic_collect" data-socket-uri="${BASE}/sock/updated"></ul>
                <!-- 话题列表结束 -->
            </div>
        </div>
        <!-- 底部区功能区开始 -->
        <div class="row">
            <div class="col-md-6">
                <c:choose>
                <c:when test="${board.status == 'LOCKED'}">
                <a href="javascript:;" class="btn btn-secondary disabled" role="button"><i class="ico-sm mdi mdi-lock" aria-hidden="true"></i> 锁定</a>
                </c:when>
                <c:when test="${board.normal }">
                <a href="${BASE}/topic/create" class="btn btn-primary" role="button"><i class="ico-sm mdi mdi-edit"></i> 新话题</a>
                </c:when>
                <c:otherwise>
                <a href="javascript:;" class="btn btn-secondary disabled" role="button"><i class="ico-sm mdi mdi-block"></i> 新话题</a>
                </c:otherwise>
                </c:choose>
                <!-- 版块工具开始 -->
                <div class="btn-group" data-result="click.jsonp" data-handler="${BASE}/board/tool.jsonp" data-function="buildMenuList" data-selector=".board-menu-list">
                    <button type="button" class="btn btn-secondary"><i class="ico-sm mdi mdi-wrench"></i> &nbsp;工具</button>
                    <button type="button" class="btn btn-secondary dropdown-toggle-split caret-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu board-menu-list"></div>
                </div>
                <!-- 收藏 -->
                <div class="btn-group" role="group" aria-label="喜欢都收藏呗">
                    <a role="button" href="javascript:;" data-acctip="false" class="btn btn-danger action-cmd star-board" data-handler="${BASE}/board/star" data-function="updateBoardFavoriteCounter"><i class="ico-sm mdi mdi-favorite" aria-hidden="true"></i> 收藏</a>
                    <button type="button" class="btn btn-light favoriteCounter" disabled="disabled">${board.stats.favorites}</button>
                </div>
                <!-- 版块工具结束 -->
            </div>
            <div class="col-md-6">
                <!-- 分页开始 -->
                <div class="frinner page_navigation" data-handler="${BASE}/board/threads.json" data-query="category:${category}" data-function="drawThreads"></div>
                <!-- 分页结束 -->
            </div>
        </div>
        <!-- 底部区功能区结束 -->
        <!-- 底部导航开始 -->
        <div class="row mb-2 mt-3">
            <p class="col-md-10 jumpbox-return"><a href="${BASE}/board/home" accesskey="r"><i class="ico mdi mdi-arrow-left" aria-hidden="true"></i> 返回版块列表</a></p>
            <!-- board navigate start-->
            <div class="col-md-2">
                <div class="board-navigator-box" data-handler="${BASE}/loader/board/select.jsonp" data-function="buildBoardNavigateSelect">
                    <p class="div-select board-navigator-select" role="button">选择跳转的版块</p>
                </div>
            </div>
            <!-- board navigate end -->
        </div>
        <!-- 底部导航结束 -->
    </div>
    <%@ include file = "../include/footer.jsp" %>
    <section id="sideToolPlug" data-tool-plug="publish"></section>
  </body></html>