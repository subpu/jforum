<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>帐户设置:联系方式</title>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                    <span class="crumb">
                        <a href="<c:url value="/member/home/"/>" title="会员中心">会员中心</a>
                    </span>
                    <span class="crumb">帐户设置</span>
                    <span class="crumb">联系方式</span>
                </li>
            </ul>
            <%@ include file = "../include/page_search.jsp" %>
        </div>
    </div>
    <div class="container-fluid no-padding">
        <div class="container">
            <div class="row w-100">
                <!-- 左侧开始-->
                <div class="col-md-10 main-color mb-3" role="main">
                    <div style="position:relative">
                        <ul class="list-inline nav-section">
                            <li class="list-inline-item"><a href="${BASE}/member/home/profile">基础信息</a></li>
                            <li class="list-inline-item"><a href="${BASE}/member/home/social">社交信息</a></li>
                            <li class="list-inline-item"><a href="${BASE}/member/home/realauth">实名认证</a></li>
                            <li class="list-inline-item active"><a href="#">联系方式</a></li>
                        </ul>
                    </div>
                    <c:if test="${not empty errors}">
                    <div class="alert alert-danger" role="alert">
                        <strong>oOps!</strong> ${errors}
                    </div>
                    </c:if>
                    <form class="theme-form" method="post" action="${BASE}/member/home/contact" id="member_contact_form" data-submit="once">
                        <br/>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label text-right" for="province">省份:</label>
                            <div class="col-sm-10">
                                <input type="text" name="province" class="inputbox no-radius form-control w600" value="${form.province}" tabindex="1"/>
                                <small class="form-text text-muted">可选项,可以不输入</small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label text-right" for="city">城市:</label>
                            <div class="col-sm-10">
                                <input type="text" name="city" class="inputbox no-radius form-control w600" value="${form.city}" tabindex="2"/>
                                <small class="form-text text-muted">可选项,可以不输入</small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label text-right" for="region">地区:</label>
                            <div class="col-sm-10">
                                <input type="text" name="region" class="inputbox no-radius form-control w600" value="${form.region}" tabindex="3"/>
                                <small class="form-text text-muted">可选项,可以不输入</small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label text-right" for="street">街道:</label>
                            <div class="col-sm-10">
                                <input type="text" name="street" class="inputbox no-radius form-control w600" value="${form.street}" tabindex="4"/>
                                <small class="form-text text-muted">可选项,可以不输入</small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label text-right" for="postcode">邮编:</label>
                            <div class="col-sm-10">
                                <input type="number" name="postcode" class="inputbox no-radius form-control w600" value="${form.postcode}" tabindex="5"/>
                                <small class="form-text text-muted">可选项,可以不输入</small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label text-right" for="mobile">手机号:</label>
                            <div class="col-sm-10">
                                <input type="tel" name="mobile" class="inputbox no-radius form-control w600" value="${form.mobile}" required="required" tabindex="6"/>
                                <small class="form-text text-muted">必填项,必需输入有效的手机号</small>
                            </div>
                        </div>
                        <div class="form-group row justify-content-end">
                            <div class="col-sm-10">
                                <input type="hidden" name="token" value="${form.token}"/>
                                <input type="hidden" name="record" value="${form.record}"/>
                                <input type="submit" name="send" value="提交更新" class="btn btn-primary" tabindex="7"/>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- 左侧结束-->
                <!-- 右侧开始-->
                <div class="col-md-2 load_content_section" role="menu" id="member_menu_panel" data-query="active:profile" data-handler="${BASE}/member/home/panel">
                    <!-- 管理菜单 -->
                    <div class="ld ld-ring ld-spin"></div>
                </div>
                <!-- 右侧结束-->
            </div>
        </div>
    </div>
    <%@ include file = "../include/footer.jsp" %>
  </body></html>
