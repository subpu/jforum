<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>论坛首页</title>
  </head>
  <body>
    <div class="container-fluid" style="background-color: rgb(14, 16, 17); background-image: url('${BASE}/static/img/top_banner.jpg'); background-repeat: no-repeat; background-position: 50% 0; zoom: 1;height:450px">
        &nbsp;
    </div>
    <div class="container-fluid">
        <div class="container">
            <!-- 右侧开始 -->
            <div id="sidebar">
                <div class="side-block side-login load_content_section" style="height:220px;" data-handler="${BASE}/member/login/form" id="right_member_info">
                    <!-- 登录表单 -->
                    <div class="ld ld-ring ld-spin"></div>
                </div>
                <div class="side-block section">
                    <h6 class="side-block-head hl60 ti20">社区标兵</h6>
                    <div class="side-block-body p-2" data-result="loader.json"  data-handler="${BASE}/member/model/json" data-template-function="buildModelMember">
                        <p class="no-record-tip"><i class="ico mdi mdi-spinner" aria-hidden="true"></i> 看成败人生豪迈, 只不过是从头再来</p>
                    </div>
                </div>
                <div class="side-block section">
                    <h6 class="side-block-head hl60 ti20">热点话题</h6>
                    <div class="side-block-body p-2" data-result="loader.json" data-handler="${BASE}/loader/hot.json" data-template-function="buildRightSectionTopic">
                        <p class="no-record-tip"><i class="ico mdi mdi-spinner" aria-hidden="true"></i> 看成败人生豪迈, 只不过是从头再来</p>
                    </div>
                </div>
            </div>
            <!-- 右侧结束 -->
            <!-- 左侧开始 -->
            <div id="forumlist">
                <div>
                    <!-- 遍历版块组开始 -->
                    <div data-result="batch.jsonp" data-handler="${BASE}/loader/board/stats.jsonp" data-function="loadBoardStatsResult" data-param="ids" data-record="li[data-board]">
                        <c:forEach items="${rs}" var="boardGroup">
                        <div class="board_list">
                            <ul class="board_list_header subject-color">
                                <li class="hl60">
                                    <dl class="icon">
                                        <dt>
                                            <div class="ti20">
                                                <a href="${BASE}/board/volumes/${boardGroup.connect}.xhtml">${boardGroup.title}</a>
                                            </div>
                                        </dt>
                                        <dd class="wp10">话题数</dd>
                                        <dd class="wp10">回复数</dd>
                                        <dd class="latest">最近主题</dd>
                                    </dl>
                                </li>
                            </ul>
                            <!-- 遍历版块组的子版块开始 -->
                            <ul class="board_list_header">
                                <c:forEach items="${boardGroup.boardes}" var="board">
                                <li data-board="${board.id}" class="board_item">
                                    <dl class="icon forum_${board.icoStatus}">
                                        <dt>
                                            <div class="board_item_title">
                                                <a href="${BASE}/board/${board.connect}.xhtml" class="forum-article-header">${board.title} &nbsp;<small class="today-counter">(0)</small></a>
                                                <div class="txt-sm">${board.description}</div>
                                            </div>
                                        </dt>
                                        <dd class="threads wp10 stats-item">0<dfn>Threads</dfn></dd>
                                        <dd class="replies wp10 stats-item">0<dfn>Replies</dfn></dd>
                                        <dd class="latest"></dd>
                                    </dl>
                                </li>
                                </c:forEach>
                            </ul>
                            <!-- 遍历版块组的子版块结束 -->
                        </div>
                        </c:forEach>
                    </div>
                    <!-- 遍历版块组结束 -->
                    <!-- 论坛统计区开始 -->
                    <div class="stat-block online-list mb-3 mt-3">
                        <h6 class="hl60 ti20">统计信息</h6>
                        <p>总在线人数 <strong id="stats-onlines">0</strong> 注册会员 <span id="stats-onlines-member">0</span>, 游客 <span id="stats-onlines-guest">0</span> (基于过去5分钟内活跃的成员)
                        <!-- <br>活跃最高记录是 <strong>0</strong> on 11 Apr 2017, 10:11 -->
                        <br>身份颜色说明: 
                        <span class="member-admin">系统管理员</span>, <span class="member-manager">社区经理</span>, <span class="member-moderator">版主</span>, 
                        <span class="member-robot">机器人</span>, <span class="member-spider">网络蜘蛛</span>, 
                        <span class="member-guest">游客</span>, <span class="member-general">普通会员</span>, <span class="member-vip">VIP会员</span>
                        </p>
                        <p id="online-list-stats" data-board="${BASE}/loader/stats/collect.json" data-member="${BASE}/member/stats/collect">
                                           回复合计 <strong id="stats-replies">0</strong> 
                                           话题合计 <strong id="stats-threads">0</strong> 
                                           注册会员合计 <strong id="stats-member">0</strong> 
                                           最近注册的成员 <strong id="stats-recent-member">loading</strong>
                        </p>
                    </div>
                    <!-- 论坛统计区结束 -->
                </div>
            </div>
            <!-- 左侧结束 -->
        </div>
    </div>
    <%@ include file = "../include/footer.jsp" %>
  </body></html>