<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>回复话题</title>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                    <span class="crumb">
                        <a href="${BASE}/board/${topic.board.connect}.xhtml">${topic.board.title}</a>
                    </span>
                    <span class="crumb mw600">
                        <a href="${BASE}/topic/${topic.connect}.xhtml">${topic.title}</a>
                    </span>
                    <span class="crumb">回复话题</span>
                </li>
            </ul>
            <%@ include file = "../include/page_search.jsp" %>
        </div>
    </div>
    <div class="container-fluid no-padding">
        <div class="container main-color" style="padding-top:20px;margin-top:50px;">
            <c:if test="${not empty errors}">
            <div class="alert alert-danger" role="alert">
                <strong>oOps!</strong> ${errors}
            </div>
            </c:if>
            <c:if test="${not empty infoTip}">
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                <strong>提示!</strong> ${infoTip}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            </c:if>
            <form method="post" action="${BASE}/posts/create" data-submit="once" class="theme-form">
            <div class="form-group row">
                <div class="col-sm-12">
                    <textarea id="content" name="content" data-richEditor="CKEditor" data-query="height:450,width:100%,upload:/upload/ckeditor" required="required" tabindex="1">${form.content}</textarea>
                </div>
            </div>
            <div class="form-group text-right h60">
                  <input type="hidden" name="token" value="${form.token}"/>
                  <input type="hidden" name="topic" value="${form.topic}" />
                  <input type="hidden" name="board" value="${form.board}" />
                  <input type="hidden" name="volumes" value="${form.volumes}" />
                  <input type="hidden" name="quoteId" value="${form.quoteId}" />
                  <input type="hidden" name="quoteFloor" value="${form.quoteFloor}" />
                  <input type="hidden" name="quoteScale" value="${form.quoteScale}" />
                  <input type="submit" name="reply" value="提交回复" class="btn btn-primary" tabindex="2"/>
            </div>
            </form>
        </div>
    </div>
    <%@ include file = "../include/footer.jsp" %>
  </body></html>