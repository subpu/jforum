<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>帐户设置:基础信息</title>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                    <span class="crumb">
                        <a href="<c:url value="/member/home/"/>" title="会员中心">会员中心</a>
                    </span>
                    <span class="crumb">帐户设置</span>
                    <span class="crumb">基础信息</span>
                </li>
            </ul>
            <%@ include file = "../include/page_search.jsp" %>
        </div>
    </div>
    <div class="container-fluid no-padding">
        <div class="container">
            <div class="row w-100">
                <!-- 左侧开始-->
                <div class="col-md-10 main-color mb-3" role="main">
                    <div style="position:relative">
                        <ul class="list-inline nav-section">
                            <li class="list-inline-item active"><a href="#">基础信息</a></li>
                            <li class="list-inline-item"><a href="${BASE}/member/home/social">社交信息</a></li>
                            <li class="list-inline-item"><a href="${BASE}/member/home/realauth">实名认证</a></li>
                            <li class="list-inline-item"><a href="${BASE}/member/home/contact">联系方式</a></li>
                        </ul>
                    </div>
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <i class="ico-sm mdi mdi-alert-triangle" aria-hidden="true"></i> &nbsp;未填写邮箱的情况下无法完成密码找回
                    </div>
                    <c:if test="${not empty errors}">
                    <div class="alert alert-danger" role="alert">
                        <strong>oOps!</strong> ${errors}
                    </div>
                    </c:if>
                    <form class="theme-form" method="post" action="${BASE}/member/home/profile" id="member_profile_form" data-submit="once">
                        <br/>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label text-right" for="nickname">昵称:</label>
                            <div class="col-sm-10">
                                <input type="text" name="nickname" class="inputbox no-radius form-control w600" value="${form.nickname}" readonly="readonly" required="required" tabindex="1"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label text-right" for="signature">个性签名:</label>
                            <div class="col-sm-10">
                                <textarea name="signature" class="form-control w600 h150" style="resize:none;" tabindex="2">${form.signature}</textarea>
                                <small class="form-text text-muted">&#x1F4A1 &nbsp;只允许纯文字;不可以包含连接,图片,UBB代码和脚本代码</small>
                            </div>
                        </div>
                        <div class="form-group row justify-content-end">
                            <div class="col-sm-10">
                                <input type="hidden" name="token" value="${form.token}"/>
                                <input type="hidden" name="record" value="${form.record}"/>
                                <input type="submit" name="send" value="提交更新" class="btn btn-primary" tabindex="3"/>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- 左侧结束-->
                <!-- 右侧开始-->
                <div class="col-md-2 load_content_section" role="menu" id="member_menu_panel" data-query="active:profile" data-handler="${BASE}/member/home/panel">
                    <!-- 管理菜单 -->
                    <div class="ld ld-ring ld-spin"></div>
                </div>
                <!-- 右侧结束-->
            </div>
        </div>
    </div>
    <%@ include file = "../include/footer.jsp" %>
  </body></html>
