<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>小伙伴正在等您开车呢</title>
  </head>
  <body>
    <!-- form begin-->
    <div class="container-fluid no-padding">
        <div class="container w480 well main-color px-4 py-3 mt-5">
            <c:if test="${not empty errors}">
            <div class="alert alert-danger" role="alert">
                <strong>oOps!</strong> ${errors}
            </div>
            </c:if>
            <form action="${BASE}/member/login" method="post" id="login" data-submit="once" class="theme-form">
                <div class="form-group">
                    <label for="names">帐号:</label>
                    <input type="text" name="names" size="25" value="${form.names}" class="form-control" required="required" tabindex="1"/>
                </div>
                <div class="form-group">
                    <label for="password">密码:</label>
                    <input type="password" name="pswd" size="25" class="form-control" autocomplete="off" data-minlength="6" required="required" tabindex="2"/>
                    <p class="clearCache control-label txt-sm">记不清密码了?请点击&nbsp;<a href="${BASE}/lostPswd">找回密码</a></p>
                </div>
                <!-- 从哪来的 -->
                <input type="hidden" name="token" value="${form.token}"/>
                <input type="hidden" name="redirect" value="${form.redirect}"/>
                <input type="hidden" name="tries" value="${form.tries}"/>
                <input type="hidden" name="trace" value="msa"/>
                <input type="submit" name="login" value="马上登陆" data-submit-disabled="正在登录" class="btn btn-primary" tabindex="3"/>
            </form>
            <div class="mt-3">
                <div class="inner">
                    <div class="member-registe-content">
                        <h3 class="default-txt-color">没有帐号?</h3>
                        <p>还不快来注册! 一群热心的小伙伴正在等待您的到来,在这里不光可以看到热门的资讯,还有优质的资源等待您查看,同时还可以将您宝贵的知识分享给大家, 快点下面的免费注册按钮吧</p>
                        <p><a class="btn btn-secondary" role="button" href="${BASE}/member/register" class="button2">免费注册</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </body></html>