<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>帐户设置:实名认证</title>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                    <span class="crumb">
                        <a href="<c:url value="/member/home/"/>" title="会员中心">会员中心</a>
                    </span>
                    <span class="crumb">帐户设置</span>
                    <span class="crumb">实名认证</span>
                </li>
            </ul>
            <%@ include file = "../include/page_search.jsp" %>
        </div>
    </div>
    <div class="container-fluid no-padding">
        <div class="container">
            <div class="row w-100">
                <!-- 左侧开始-->
                <div class="col-md-10 main-color mb-3" role="main">
                    <div style="position:relative">
                        <ul class="list-inline nav-section">
                            <li class="list-inline-item"><a href="${BASE}/member/home/profile">基础信息</a></li>
                            <li class="list-inline-item"><a href="${BASE}/member/home/social">社交信息</a></li>
                            <li class="list-inline-item active"><a href="#">实名认证</a></li>
                            <li class="list-inline-item"><a href="${BASE}/member/home/contact">联系方式</a></li>
                        </ul>
                    </div>
                    <c:if test="${not empty errors}">
                    <div class="alert alert-danger" role="alert">
                        <strong>oOps!</strong> ${errors}
                    </div>
                    </c:if>
                    <form class="theme-form" method="post" action="${BASE}/member/home/realauth" id="member_realauth_form" data-submit="once">
                        <br/>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label text-right" for="realname">真实姓名:</label>
                            <div class="col-sm-10">
                                <input type="text" name="realname" class="inputbox no-radius form-control w600" value="${form.realname}" required="required" tabindex="1"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label text-right" for="birthYear">年:</label>
                            <div class="col-sm-10">
                                <input type="number" name="birthYear" class="inputbox no-radius form-control w600" value="${form.birthYear}" required="required" tabindex="2"/>
                                <small class="form-text text-muted">出生日期中的年,例:1980</small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label text-right" for="birthMonth">月:</label>
                            <div class="col-sm-10">
                                <input type="number" name="birthMonth" class="inputbox no-radius form-control w600" value="${form.birthMonth}" required="required" tabindex="3"/>
                                <small class="form-text text-muted">出生日期中的月,例:08,不足两位补0</small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label text-right" for="birthDay">日:</label>
                            <div class="col-sm-10">
                                <input type="number" name="birthDay" class="inputbox no-radius form-control w600" value="${form.birthDay}" required="required" tabindex="4"/>
                                <small class="form-text text-muted">出生日期中的日,例:08,不足两位补0</small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label text-right" for="identityCard">身份证:</label>
                            <div class="col-sm-10">
                                <input type="text" name="identityCard" class="inputbox no-radius form-control w600" value="${form.identityCard}" required="required" tabindex="5"/>
                            </div>
                        </div>
                        <div class="form-group row justify-content-end">
                            <div class="col-sm-10">
                                <input type="hidden" name="token" value="${form.token}"/>
                                <input type="hidden" name="record" value="${form.record}"/>
                                <c:if test="${not isAuthed}">
                                <input type="submit" name="send" value="开始认证" class="btn btn-primary" tabindex="6"/>
                                </c:if>
                                <c:if test="${isAuthed}">
                                <input type="button" name="send" tabindex="6" value="认证通过" class="btn btn-default" disabled="disabled"/>
                                </c:if>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- 左侧结束-->
                <!-- 右侧开始-->
                <div class="col-md-2 load_content_section" role="menu" id="member_menu_panel" data-query="active:profile" data-handler="${BASE}/member/home/panel">
                    <!-- 管理菜单 -->
                    <div class="ld ld-ring ld-spin"></div>
                </div>
                <!-- 右侧结束-->
            </div>
        </div>
    </div>
    <%@ include file = "../include/footer.jsp" %>
  </body></html>
