<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>${pageTitle}</title>
    <style>nav.page-navigation ul{text-align:center;justify-content: center;}</style>
  </head>
  <body>
    <!-- 搜索表单开始 -->
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <div class="search_form row justify-content-center">
                <form action="${DOMAIN}/search/" method="get">
                    <div id="search_form_ele">
                        <p class="ele_group">
                            <input type="text" name="word" value="${param.word}" />
                            <input type="submit" value="搜索"/>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- 搜索表单结束 -->
    <div id="container-fluid">
        <div class="container">
            <!-- 右侧开始 -->
            <div id="sidebar">
                <div class="side-block section">
                    <h6 class="side-block-head hl60 ti20">相关搜索</h6>
                    <div class="side-block-body p-2" data-result="loader.json" data-empty-show="false" data-handler="${BASE}/topic/tag/relate.json" data-template-function="buildRightSectionTag">
                        <p class="no-record-tip"><i class="ico mdi mdi-spinner" aria-hidden="true"></i> 看成败人生豪迈, 只不过是从头再来</p>
                    </div>
                </div>
                <div class="side-block section">
                    <h6 class="side-block-head hl60 ti20">热点标签</h6>
                    <div class="side-block-body p-2" data-result="loader.json" data-empty-show="false" data-handler="${BASE}/topic/tag/hot.json" data-template-function="buildRightSectionTag">
                        <p class="no-record-tip"><i class="ico mdi mdi-spinner" aria-hidden="true"></i> 看成败人生豪迈, 只不过是从头再来</p>
                    </div>
                </div>
                <!-- 右侧广告开始 -->
                <div class="side-block">
                    <a href="javascript:;"><img src="${BASE}/ad/side_ad_1.png"/></a>
                </div>
                <!-- 右侧广告结束 -->
            </div>
            <!-- 右侧结束 -->
            <!-- 左侧开始 -->
            <div id="forumlist">
                <div id="search_item_collection" class="main-color" data-result="batch.jsonp" data-handler="${BASE}/loader/album.jsonp" data-function="packAlbumHoldSection" data-param="album" data-record="div[data-album]" data-query="size:4">
                    <p style="line-height:50px;text-indent:20px;margin-bottom:0;font-size:12px;">通过标签为您找到相关结果{${pageData.records}}条</p>
                    <ul>
                        <c:forEach items="${rs}" var="topic">
                        <li class="search_item">
                            <h6><a href="${BASE}/topic/${topic.connect}.xhtml">${topic.title}</a></h6>
                            <p><a href="${BASE}/member/${topic.memberId}.xhtml" title="会员主页">${topic.memberNickname}</a> &#187; <forum:format value="${topic.entryDateTime}"/></p>
                            <div class="search_item_body">${topic.summary}</div>
                            <br/>
                            <div class="mc-topic-body-txt" data-album="${topic.albumId}"></div>
                        </li>
                        </c:forEach>
                    </ul>
                </div>
                <div style="padding:20px 0;" class="home-page-section">
                    <!-- 分页 -->
                    <nav aria-label="Page navigation" class="page-navigation">
                        <!-- page tool section-->
                        <jsp:include page="../include/page_embedded.jsp">
                            <jsp:param name="url" value="${pageData.pageURL}"></jsp:param>
                            <jsp:param name="records" value="${pageData.records}"></jsp:param>
                            <jsp:param name="showSize" value="${pageData.pageSize}"></jsp:param>
                            <jsp:param name="page" value="${pageData.page}"></jsp:param>
                        </jsp:include>
                    </nav>
                </div>
            </div>
            <!-- 左侧结束 -->
        </div>
    </div>
    <%@ include file = "../include/footer.jsp" %>
  </body></html>