<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>轮播图</title>
    <link href="${BASE}/static/lib/bootstrap4/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
  </head>
  <body>
    <div class="container-fluid no-padding">
        <div class="container" style="margin-top:10px;">
            <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel" style="width:749px">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <c:forEach var="item" begin="0" end="${fn:length(rs.slides)-1}">
                    <li data-target="#carouselExampleCaptions" data-slide-to="${item}"<c:if test="${item == 0}"> class="active"</c:if>></li>
                    </c:forEach>
                </ol>
                
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <c:forEach items="${rs.slides}" var="slide" varStatus="step">
                    <div class="carousel-item<c:if test="${step.index == 0}"> active</c:if>">
                        <img src="${slide.imageAddr}" alt="${slide.title}">
                        <div class="carousel-caption d-none d-md-block">
                        <a href="${slide.link}">${slide.title}</a>
                        </div>
                    </div>
                    </c:forEach>
                </div>
                
                <!-- Controls -->
                <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
    <script src="${BASE}/static/lib/jquery/jquery.min.js"></script>
    <script src="${BASE}/static/lib/bootstrap4/js/bootstrap.min.js"></script>
  </body>
</html>