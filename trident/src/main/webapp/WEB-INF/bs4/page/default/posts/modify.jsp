<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>编辑回复</title>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                    <span class="crumb">
                        <a href="${BASE}/board/${topic.board.connect}.xhtml">${topic.board.title}</a>
                    </span>
                    <span class="crumb mw600">
                        <a href="${BASE}/topic/${topic.connect}.xhtml">${topic.title}</a>
                    </span>
                    <span class="crumb">编辑回复</span>
                </li>
            </ul>
            <%@ include file = "../include/page_search.jsp" %>
        </div>
    </div>
    <div class="container-fluid no-padding">
        <div class="container" style="padding-top:20px;background-color:#fff;margin-top:50px;">
            <c:if test="${not empty errors}">
            <div class="alert alert-danger" role="alert">
                <strong>oOps!</strong> ${errors}
            </div>
            </c:if>
            <form class="form-horizontal" method="post" action="${BASE}/posts/edit" data-submit="once" class="theme-form">
            <div class="form-group">
                <label class="col-sm-2 control-label" for="content">内容:</label>
                <div class="col-sm-10 row">
                    <div class="col-md-8">
                        <textarea id="content" name="content" tabindex="1" class="richeditor" data-query="height:450,upload:/upload/ckeditor">${form.content}</textarea>
                    </div>
                    
                    <jsp:include page="${THEME}common/smiley_embedded.jsp"></jsp:include>
                    
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="hidden" name="record" value="${form.record}"/>
                    <input type="hidden" name="topic" value="${form.topic}"/>
                    <input type="hidden" name="board" value="${form.board}"/>
                    <input type="hidden" name="volumes" value="${form.volumes}"/>
                    <input type="hidden" name="token" value="${form.token}"/>
                    <input type="submit" name="更新内容" tabindex="3" value="修改回复内容" class="btn btn-primary" />
                </div>
            </div>
            </form>
        </div>
    </div>
    <%@ include file = "../include/footer.jsp" %>
  </body></html>
