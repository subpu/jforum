<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>随便看看</title>
  </head>
  <body>
    <div class="container-fluid" style="background-color: rgb(14, 16, 17); background-image: url('${BASE}/static/img/top_banner.jpg'); background-repeat: no-repeat; background-position: 50% 0px; zoom: 1;height:450px">
        &nbsp;
    </div>
    <div class="container-fluid" style="background-color: #fff;">
        <div class="container">
            <div>
                <!-- 论坛统计区开始 -->
                <div class="h150">
                    <ul class="list-inline row counter-tabs" id="online-list-stats" data-board="${BASE}/loader/stats/collect.json" data-member="${BASE}/member/stats/collect">
                        <li class="col-md-3"><h4>主题数量</h4><span class="counter-number" id="stats-threads">0</span></li>
                        <li class="col-md-3"><h4>回复数量</h4><span class="counter-number" id="stats-replies">0</span></li>
                        <li class="col-md-3"><h4>注册会员</h4><span class="counter-number" id="stats-member">0</span></li>
                        <li class="col-md-3"><h4>最近注册的会员</h4><span id="stats-recent-member">loading</span></li>
                    </ul>
                </div>
                <!-- 论坛统计区结束 -->
                <div class="hl60">
                    <ul class="list-inline" id="native-tabs">
                        <li class="list-inline-item active"><a href="javascript:;">随便看看</a></li>
                        <li class="list-inline-item"><a href="<c:url value="/board/home"/>">版块列表</a></li>
                        <li class="list-inline-item"><a target="_blank" href="${BASE}/rss/recent.xml">RSS订阅</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="container-fluid"  style="padding-top:20px;">
        <div class="container">
          <div class="row">
            <!-- 最近的话题开始 -->
            <c:forEach items="${rs}" var="albumObj">
            <div class="col-sm-6 col-md-3">
                <div class=" thumb-item thumb-item-trans">
                    <a class="thumb-item-anchor lazyload" style="background-image: url(data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=)" data-original="<forum:thumb path="${albumObj.coverLink}" />" href="${BASE}/topic/${albumObj.topicId}-${albumObj.boardId}-${albumObj.volumesId}.xhtml">&nbsp;</a>
                    
                    <h6 class="thumb-item-title"><a href="${BASE}/topic/${albumObj.topic.connect}.xhtml">${albumObj.title}</a></h6>
                    <p class="thumb-item-body">
                        <a href="${BASE}/member/${albumObj.memberId}.xhtml" title="会员主页">${albumObj.memberNickname}</a> &#187; <forum:format value="${albumObj.entryDateTime}"/>
                        <a href="${BASE}/board/${albumObj.board.connect}.xhtml" class="float-right">${albumObj.board.title}</a>
                    </p>
                </div>
            </div>
            </c:forEach>
            <!-- 最近的话题结束 -->
          </div>
        </div>
    </div>
    <%@ include file = "include/footer.jsp" %>
  </body></html>