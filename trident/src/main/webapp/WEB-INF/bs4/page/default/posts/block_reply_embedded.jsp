<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set value="${pageContext.request.contextPath}" var="BASE"/>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>快速回复</title>
  </head>
  <body>
    <fmt:parseNumber var="mid" integerOnly="true" type="number" value="${member.id}" />
    <div class="topic_posts_item main-color">
        <dl class="topic_posts_item_left">
            <dt style="padding-top: 20px;padding-left: 20px;width:180px">
                <div class="posts-member-avatar">
                    <div class="avatar">
                        <img class="avatar" src="${BASE}/member/avatar/${mid}.png" alt="User avatar" />
                    </div>
                </div>
                <c:if test="${mid>0}">
                <a href="${BASE}/member/${mid}.xhtml" class="member-info"><span class="member-${m.id}-label member-${member.style}"> ${member.names}</span></a>
                </c:if>
            </dt>
        </dl>
        <div class="topic_posts_item_right h260">
            <div class="posts-body">
                <h5 class="hl45 default-txt-color">快速回复</h5>
                <div id="block-paper">
                    <p>${errors}<c:if test="${mid<=0}"> &nbsp; <a href="${BASE}/member/login">登录</a>&nbsp; | &nbsp;<a href="${BASE}/member/register">注册</a></c:if></p>
                </div>
            </div>
        </div>
    </div>
  </body></html>