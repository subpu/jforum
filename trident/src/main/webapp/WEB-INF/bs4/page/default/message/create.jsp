<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>新建消息</title>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                    <span class="crumb">消息</span>
                    <span class="crumb">新建</span>
                </li>
            </ul>
            <%@ include file = "../include/page_search.jsp" %>
        </div>
    </div>
    <div class="container-fluid no-padding">
        <div class="container">
            <div class="row w-100">
                <div class="col-md-10 main-color mb-3" role="main">
                    <div style="position:relative">
                        <ul class="list-inline nav-section">
                            <li class="list-inline-item"><a href="${BASE}/message/">收件箱</a></li>
                            <li class="list-inline-item"><a href="${BASE}/message/sent">发件箱</a></li>
                            <li class="list-inline-item active"><a href="#">新消息</a></li>
                        </ul>
                    </div>
                    <c:if test="${not empty errors}">
                    <div class="alert alert-danger" role="alert">
                        <strong>oOps!</strong> ${errors}
                    </div>
                    </c:if>
                    <form class="theme-form" method="post" action="${BASE}/message/create" id="message_form" data-submit="once">
                    <br/>
                    <div class="form-group row">
                        <label class="col-sm-2 control-label text-right">收件人/UID:</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <input type="text" name="uid" id="suggest-uid" class="inputbox no-radius form-control w600" value="${form.uid}" data-handler="${BASE}/member/detection/uid" required="required" tabindex="1"/>
                                <div class="input-group-append"><button class="btn btn-outline-primary" type="button" id="suggest-btn">查询</button></div>
                            </div>
                            <small class="form-text text-muted">以u开头后跟会员的ID,称之为uid; 例如:u1001</small>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 control-label text-right">会员:</label>
                        <div class="col-sm-10">
                            <input type="text" name="snames" class="inputbox no-radius form-control w600" value="" readonly="readonly" placeholder="输入收件人并点击查询后会自动填充" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 control-label text-right" for="title">主题:</label>
                        <div class="col-sm-10">
                            <input type="text" name="title" class="inputbox no-radius form-control w600" value="${form.title}" required="required" tabindex="2" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 control-label text-right" for="content">内容:</label>
                        <div class="col-sm-10">
                            <textarea name="content" class="form-control w600 h150" required="required" tabindex="3" style="resize:none;">${form.content}</textarea>
                            <small class="form-text text-muted">&#x1F4A1 只支持纯文本内容.html标签会被清除</small>
                        </div>
                    </div>
                    <div class="form-group row justify-content-end">
                        <div class="col-sm-10">
                            <input type="hidden" name="token" value="${form.token}"/>
                            <input type="submit" name="send" tabindex="4" value="发送消息" class="btn btn-primary" />
                        </div>
                    </div>
                    </form>
                </div>
                <div class="col-md-2 load_content_section" role="menu" id="member_menu_panel" data-query="active:mesg_create" data-handler="${BASE}/member/home/panel">
                    <!-- 管理菜单 -->
                </div>
            </div>
        </div>
    </div>
    <%@ include file = "../include/footer.jsp" %>
  </body></html>
