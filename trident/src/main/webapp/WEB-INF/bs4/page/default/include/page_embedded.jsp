<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
                            <ul class="pagination">
                                    <forum:pagination size="${param.showSize}" total="${param.records}" url="${param.url}">
                                    <c:set var="currentPageNumber">${param.page}</c:set>
                                    <c:choose>
                                    <c:when test="${currentPageNumber eq anchor}">
                                    <li class="page-item active"><a class="page-link" href="javascript:;">${anchor}<span class="sr-only">(current)</span></a></li>
                                    </c:when>
                                    <c:otherwise>
                                    <li class="page-item"><a class="page-link" href="${uri}">${anchor}</a></li>
                                    </c:otherwise>
                                    </c:choose>
                                    </forum:pagination>
                            </ul>
