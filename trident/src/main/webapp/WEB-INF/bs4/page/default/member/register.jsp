<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>一起来分享身边的大事件吧</title>
  </head>
  <body>
    <!-- form begin-->
    <div class="container-fluid no-padding">
        <div class="container w480 well main-color px-4 py-3 mt-5">
            <c:if test="${not empty errors}">
            <div class="alert alert-danger" role="alert">
                <strong>oOps!</strong> ${errors}
            </div>
            </c:if>
            <form id="register" method="post" action="${BASE}/member/register" data-submit="once" class="theme-form parsley-form" data-parsley-validate>
                <div class="form-group">
                    <label for="names">帐号:</label>
                    <small class="form-text text-muted">名称长度必须介于5个字符和20个字符之间，并且只能使用字母数字字符</small>
                    <input type="text" name="names" size="25" value="${form.names}" class="form-control" placeholder="会员的登录帐号名称" data-parsley-required data-parsley-type="alphanum" data-parsley-length="[5, 20]" data-parsley-remote="${BASE}/member/detection/unique" data-parsley-remote-message="用户名已经存在" tabindex="1"/>
                </div>
                <div class="form-group">
                    <label for="nickname">昵称:</label>
                    <small class="form-text text-muted">若昵称小于5个字符(中文占2个)将使用Member#4位随机数字</small>
                    <input type="text" name="nickname" size="25" maxlength="100" value="${form.nickname}" class="form-control" placeholder="会员的个性昵称" autocomplete="off" data-parsley-required tabindex="2"/>
                </div>
                <div class="form-group" id="pwdbox">
                    <label for="newPswd">密码:</label>
                    <small class="form-text text-muted">必须介于6个字符和100个字符之间</small>
                    <input type="password" name="newPswd" id="newPswd" size="25" value="${form.newPswd}" class="form-control" placeholder="会员的登录密码" autocomplete="off" data-parsley-required data-parsley-length="[6, 100]" tabindex="3"/>
                </div>
                <div class="form-group">
                    <label for="pswdConfirm">确认密码:</label>
                    <input type="password" name="pswdConfirm" size="25" value="${form.pswdConfirm}" class="form-control" placeholder="重新输入会员的登录密码" autocomplete="off" data-parsley-required data-parsley-equalto="#newPswd" tabindex="4"/>
                </div>
                <c:if test="${ICInputActive}">
                <div class="form-group">
                    <label for="pswdConfirm">邀请码:</label>
                    <input type="text" name="${ICInputName}" size="25" value="${param.invite}" class="form-control" placeholder="输入会员的注册邀请码" data-parsley-required tabindex="5"/>
                </div>
                </c:if>
                <div class="checkbox">
                    <div>
                        <input type="checkbox" name="agreed" checked="checked" class="magic-checkbox" tabindex="6"/>
                        <label class="control-label">我已阅读并完全同意 <a href="${BASE}/member/register/licenses.xhtml" target="_blank">会员注册协议及条款内容</a></label>
                    </div>
                    
                </div>
                <br/>
                <input type="hidden" name="token" value="${form.token}"/>
                <button type="submit" data-submit-disabled="请稍后" class="btn btn-primary" tabindex="7">免费注册</button>
            </form>
        </div>
    </div>
  </body></html>