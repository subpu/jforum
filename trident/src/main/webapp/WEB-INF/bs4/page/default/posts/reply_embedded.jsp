<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set value="${pageContext.request.contextPath}" var="BASE"/>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>快速回复</title>
  </head>
  <body>
    <div class="topic_posts_item main-color">
        <dl class="topic_posts_item_left">
            <dt style="padding-top: 20px;padding-left: 20px;width:180px">
                <div class="posts-member-avatar">
                    <a href="${BASE}/member/${member.id}.xhtml" class="avatar">
                        <img class="avatar" src="${BASE}/member/avatar/${member.id}.png" alt="User avatar" />
                    </a>
                </div>
                <a href="${BASE}/member/${member.id}.xhtml" class="member-info"><span class="member-${member.id}-label member-${member.style}"> ${member.nickname}</span></a>
            </dt>
        </dl>
        <div class="topic_posts_item_right">
            <div class="posts-body">
                <h5 class="hl45 default-txt-color">快速回复</h5>
                <form method="POST" action="${BASE}/posts/reply" id="quick_reply_form" data-submit="once">
                    <div class="form-group">
                        <textarea name="content" id="content" rows="3" cols="40" data-richEditor="CKEditor" data-query="width:880,height:158,upload:/upload/ckeditor" required="required"></textarea>
                    </div>
                    <div class="form-group row pr-4">
                        <div class="col-md-6"><span class="text-info" id="reply-tip-message"></span></div>
                        <div class="col-md-6 text-right">
                            <input type="hidden" name="token" value="${token}" />
                            <input type="hidden" name="quote" value="0" />
                            <input type="hidden" name="scale" value="" />
                            <input type="submit" name="quickreply" tabindex="2" value="提交回复" class="btn btn-primary" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
  </body>
</html>