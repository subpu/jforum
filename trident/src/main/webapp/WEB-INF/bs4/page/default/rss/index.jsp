<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>RSS订阅中心</title>
  </head>
  <body>
    <div class="container-fluid no-padding" id="sitebanner" style="background-color:#eea236">
        <div class="container rss-header">
            <h4>RSS订阅中心</h4>
            <p>本站采用RSS2.0规范输出XML内容.关于RSS2.0的规范的说明文档请参考: <a target="_blank" href="https://cyber.harvard.edu/rss/rss.html">RSS 2.0 Specification</a></p>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <c:forEach items="${rs}" var="boardGroup">
            <div class="rss-subject">
                <h5 class="default-txt-color">${boardGroup.title}</h5>
            </div>
            <div class="row mb-3">
                <c:forEach items="${boardGroup.boardes}" var="board">
                <div class="col-md-3 mt-3">
                    <div class="thumbnail main-color px-4 py-3">
                        <div style="padding:10px; ">
                            <img src="${BASE}/board/ico/${board.id}.png" alt="${board.title}" class="mx-auto d-block img-fluid"/>
                        </div>
                        <div class="caption">
                            <h6 class="default-txt-color">${board.title}</h6>
                            <p>${board.description}</p>
                            <p>
                                <a href="${BASE}/board/${board.connect}.xhtml" class="btn btn-secondary" role="button"> 话题</a>
                                <a href="${BASE}/rss/board.xml?id=${board.id}&volumes=${board.volumesId}" class="btn btn-warning" role="button"><i class="ico-sm mdi mdi-rss"></i> 订阅</a>
                            </p>
                        </div>
                    </div>
                </div>
                </c:forEach>
            </div>
            </c:forEach>
        </div>
    </div>
    <%@ include file = "../include/footer.jsp" %>
  </body></html>