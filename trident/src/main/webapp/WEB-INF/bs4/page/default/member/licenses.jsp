<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>会员注册协议及条款内容</title>
    <style>
    .list-unstyled > li{padding:12px 0;}
    .list-unstyled > li ul{margin-top:10px;}
    .list-unstyled > li ul li{padding:5px 0;margin-left:30px;}
    </style>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                    <span class="crumb">会员注册协议</span>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid" style="clear:both">
        <div class="container main-color" id="member-licenses-content">
            <p class="lead">当您申请会员时，表示您已经同意遵守本规章。 欢迎您加入本站参加交流和讨论，本站为公共论坛，为维护网上公共秩序和社会稳定，请您自觉遵守以下条款：</p>
            <ul class="list-unstyled">
                <li>
                    (1) &nbsp;不得利用本站危害国家安全、泄露国家秘密，不得侵犯国家社会集体的和公民的合法权益，不得利用本站制作、复制和传播下列信息：
                    <ul>
                        <li>煽动抗拒、破坏宪法和法律、行政法规实施的</li>
                        <li>煽动颠覆国家政权，推翻社会主义制度的</li>
                        <li>煽动分裂国家、破坏国家统一的</li>
                        <li>煽动民族仇恨、民族歧视，破坏民族团结的</li>
                        <li>捏造或者歪曲事实，散布谣言，扰乱社会秩序的</li>
                        <li>宣扬封建迷信、淫秽、色情、赌博、暴力、凶杀、恐怖、教唆犯罪的</li>
                        <li>公然侮辱他人或者捏造事实诽谤他人的，或者进行其他恶意攻击的</li>
                        <li>损害国家机关信誉的</li>
                        <li>其他违反宪法和法律行政法规的</li>
                        <li>进行商业广告行为的</li>
                    </ul>
                </li>
                <li>(2) &nbsp;互相尊重，对自己的言论和行为负责。</li>
                <li>(3) &nbsp;禁止在申请用户时使用相关本站的词汇，或是带有侮辱、毁谤、造谣类的或是有其含义的各种语言进行注册用户，否则我们会将其删除。</li>
                <li>(4) &nbsp;禁止以任何方式对本站进行各种破坏行为。</li>
                <li>(5) &nbsp;如果您有违反国家相关法律法规的行为，本站概不负责，您的登录论坛信息均被记录无疑，必要时，我们会向相关的国家管理部门提供此类信息。</li>
            </ul>
        </div>
    </div>
    <%@ include file = "../include/footer.jsp" %>
  </body></html>