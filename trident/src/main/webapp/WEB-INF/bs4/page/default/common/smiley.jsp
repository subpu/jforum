<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set value="${pageContext.request.contextPath}" var="BASE"/>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="${BASE}/static/lib/bootstrap3/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <title>表情列表 - ${GLTitle}</title>
    <style type="text/css">
    body{margin:0;padding:0;width:100%;background-color:#fff}
    .bs-glyphicons {
       margin: 0 -10px 20px;
       overflow: hidden;
    }
    .bs-glyphicons-list {
       padding-left: 0;
       list-style: none;
    }
    .bs-glyphicons li {
       float: left;
       width: 25%;
       height: auto;
       padding: 10px;
       font-size: 10px;
       line-height: 1.4;
       text-align: center;
       border: 1px solid #fff;
       cursor:pointer;
    }
     .bs-glyphicons li:hover{
     	 background-color:#f1f1f1;
    }
    </style>
  </head>
  <body>
    <div id="smiley-collection" data-handler="${BASE}/smiley/pic">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist" id="smiley-theme-set">
        <c:forEach items="${smileyThemesMap}" var="smileyThemeMap">
        <li role="presentation"<c:if test="${activeTheme eq smileyThemeMap.key}"> class="active"</c:if>><a href="#${smileyThemeMap.key}" aria-controls="${smileyThemeMap.key}" role="tab" data-toggle="tab">${smileyThemeMap.value}</a></li>
        </c:forEach>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content" id="smiley-pic-collection">
        <c:forEach items="${smileyThemesMap}" var="smileyTM">
        <div role="tabpanel" class="tab-pane bs-glyphicons<c:if test="${activeTheme eq smileyTM.key}"> active</c:if>" id="${smileyTM.key}">
            <img src="${BASE}/static/img/loading.gif" class="load-image"/>
        </div>
        </c:forEach>
      </div>
    </div>
  </body>
</html>