<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>密码设置</title>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                    <span class="crumb">
                        <a href="<c:url value="/member/home/"/>" title="会员中心">会员中心</a>
                    </span>
                    <span class="crumb">密码设置</span>
                </li>
            </ul>
            <%@ include file = "../include/page_search.jsp" %>
        </div>
    </div>
    <div class="container-fluid no-padding">
        <div class="container">
            <div class="row w-100">
                <!-- 左侧开始-->
                <div class="col-md-10 main-color mb-3" role="main">
                    <c:if test="${not empty errors}">
                    <div class="alert alert-danger" role="alert">
                        <strong>oOps!</strong> ${errors}
                    </div>
                    </c:if>
                    <form class="theme-form mt-5 parsley-form" method="post" action="${BASE}/member/home/passport" id="member_passport_form" data-submit="once" data-parsley-validate>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label text-right" for="oldpswd">原始密码:</label>
                            <div class="col-sm-10">
                                <input type="password" name="oldpswd" class="inputbox no-radius form-control w600" value="" data-parsley-required tabindex="1"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label text-right" for="newpswd">新密码:</label>
                            <div class="col-sm-10">
                                <input type="password" name="newpswd" id="newpswd" class="inputbox no-radius form-control w600" value="" data-parsley-required data-parsley-length="[6, 100]" tabindex="2"/>
                                <small class="form-text text-muted">必须介于6个字符和100个字符之间</small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 control-label text-right" for="confimpswd">确认密码:</label>
                            <div class="col-sm-10">
                                <input type="password" name="confimpswd" class="inputbox no-radius form-control w600" value="" data-parsley-required data-parsley-equalto="#newpswd" tabindex="3"/>
                                <small class="form-text text-muted">必须介于6个字符和100个字符之间</small>
                            </div>
                        </div>
                        <div class="form-group row justify-content-end">
                            <div class="col-sm-10">
                                <input type="hidden" name="token" value="${form.token}"/>
                                <input type="submit" name="send" value="下一步" class="btn btn-primary" tabindex="4"/>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- 左侧结束-->
                <!-- 右侧开始-->
                <div class="col-md-2 load_content_section" role="menu" id="member_menu_panel" data-query="active:passport" data-handler="${BASE}/member/home/panel">
                    <!-- 管理菜单 -->
                    <div class="ld ld-ring ld-spin"></div>
                </div>
                <!-- 右侧结束-->
            </div>
        </div>
    </div>
    <%@ include file = "../include/footer.jsp" %>
  </body></html>