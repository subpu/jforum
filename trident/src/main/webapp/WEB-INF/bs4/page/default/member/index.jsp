<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>${member.nickname}的个人主页</title>
    <style>
.section{clear:left}
    #board-active > li{ margin-bottom: 20px;}
    .topic-list > li{height:2em;line-height:2em;display:block;text-decoration: none;}
    .avatar_box{display: inline-block;height: 100px;float: left;margin-right: 10px;margin-top:15px}
    .info_box{display: inline-block;line-height:20px;padding-left: 0.5em;padding-top: 20px;}
    .info_box>small{font-size:1em;color:#666}
    .f14{font-size:14px;padding-top:10px}
    .wrap{background:hsla(0,0%,100%,.25) border-box;}
    .member_collect{margin-top: 10px;height:90px;float: left;width: 100%;}
    .member_collect dl{width:50%;display:inline-block;float: left;}
    .member_collect dl dt, .member_collect dl dd{height:45px;text-align:center;font-size: 14px;}
    .member_collect dl dt{line-height: 60px;font-weight:500;letter-spacing:3px}
    .member_collect dl dd{font-weight:600}
    #member_operate li{padding: 8px 0;}.badge{border-radius:0}
    .sectionbox{background-image: url(${BASE}/static/img/empty.png);background-repeat: no-repeat;background-position: center 81px;}
    #forumlist{padding: 15px;min-height:492px;}.member-favlik-collect{margin-bottom:10px}
    </style>
  </head>
  <body>
    <div class="container-fluid" style="background-color: rgb(130, 210, 197); background-image: url('${BASE}/static/img/member_banner.png'); background-repeat: no-repeat; background-position: 50% 0px; zoom: 1;height:196px">
        <div class="container" style="padding:0">
          <div style="padding-top:85px">
            <div class="wrap" style="height:110px;padding:15px">
                <span class="avatar_box"><a href="#"><img class="img-circle" src="${BASE}/member/avatar/${member.id}.png" width="64px" height="64px" alt="User avatar" /></a></span>
                <div class="info_box">
                    <h5 title="会员昵称" class="mt0 member-${member.style}">
                        ${member.nickname}&nbsp;
                        <c:if test="${not empty member.roleNames}">
                        <small><span class="badge badge-warning" title="角色">${member.roleNames}</span></small>
                        </c:if>
                    </h5>
                    <small><forum:out value="${member.signature}" defValue="尚未填写个性签名"/></small>
                </div>
                <div class="float-right" style="line-height:75px">
                    <c:if test="${mine}">
                    <a href="${BASE}/member/home/profile" role="button" class="btn btn-info btn-sm"><i class="ico-sm mdi mdi-settings" aria-hidden="true"></i> 设置</a> &nbsp;&nbsp; 
                    </c:if>
                    <c:if test="${not mine}">
                    <a href="javascript:;" title="发送消息" data-handler="${BASE}/message/transmit?receiver=${member.id}&names=${member.nickname}" role="button" class="btn btn-success btn-sm message-transmit"><i class="ico-sm mdi mdi-email" aria-hidden="true"></i> 短消息</a>
                    </c:if>
                </div>
            </div>
          </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container" style="padding:0">
            <div id="sidebar">
                <div class="member_collect main-color">
                    <dl><dt>话题</dt><dd>${member.threads}</dd></dl>
                    <dl><dt>回复</dt><dd>${member.replies}</dd></dl>
                </div>
                <div class="side-block member-favlik-collect">
                    <div class="side-block-body" style="padding:10px">
                        <ul class="list-unstyled" id="member_operate">
                            <li>收藏<span class="float-right"><forum:out value="${member['threads-favorites']}" defValue="0"/></span></li>
                            <li>点赞<span class="float-right"><forum:out value="${member['threads-likes']}" defValue="0"/></span></li>
                        </ul>
                    </div>
                </div>
                <!-- 右侧广告开始 -->
                <div class="side-block">
                    <a href="javascript:;"><img src="${BASE}/ad/side_ad_1.png"/></a>
                </div>
                <!-- 右侧广告结束 -->
            </div>
            <div id="forumlist" class="sectionbox main-color mb-3">
                <div class="section main-color">
                    <h5 class="default-txt-color">活跃版块</h5>
                    <div data-result="loader.json" data-empty-show="false" data-handler="${BASE}/member/home/board/active/json" data-query="member:${member.id}" data-template-function="buildMemberStarBoard">
                        <p class="no-record-tip"><i class="ico mdi mdi-spinner" aria-hidden="true"></i> 看成败人生豪迈, 只不过是从头再来</p>
                    </div>
                </div>
                
                <div class="section main-color" style="padding-bottom:40px;">
                    <h5 class="default-txt-color">发布的话题<c:if test="${mine}">&nbsp;<small class="float-right f14"><a href="${BASE}/member/home/topic" role="button">更多</a></small></c:if></h5>
                    <div role="separator" class="divider" style="height:1px;">&nbsp;</div>
                    <div class="mt20" data-result="loader.json" data-empty-show="false" data-template-function="drawDynamicTopic" data-query="member:${member.id}" data-handler="${BASE}/member/home/topic/publish/json">
                        <p class="no-record-tip"><i class="ico mdi mdi-spinner" aria-hidden="true"></i> 看成败人生豪迈, 只不过是从头再来</p>
                    </div>
                </div>
                
                <div class="section main-color" style="padding-bottom:40px;">
                    <h5 class="default-txt-color">回复的话题<c:if test="${mine}">&nbsp;<small class="float-right f14"><a href="${BASE}/member/home/posts" role="button">更多</a></small></c:if></h5>
                    <div role="separator" class="divider" style="height:1px;">&nbsp;</div>
                    <div class="mt20" data-result="loader.json" data-empty-show="false" data-template-function="drawDynamicTopic" data-query="member:${member.id}" data-handler="${BASE}/member/home/topic/reply/json">
                        <p class="no-record-tip"><i class="ico mdi mdi-spinner" aria-hidden="true"></i> 看成败人生豪迈, 只不过是从头再来</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%@ include file = "../include/footer.jsp" %>
  </body></html>