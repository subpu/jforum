<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>随便看看</title>
  </head>
  <body>
    <div class="container-fluid" style="background-color: rgb(14, 16, 17); background-image: url('${BASE}/static/img/top_banner.jpg'); background-repeat: no-repeat; background-position: 50% 0; zoom: 1;height:450px">
        &nbsp;
    </div>
    <div id="container-fluid">
        <div class="container">
            <!-- 右侧开始 -->
            <div id="sidebar">
                <div class="side-block side-login load_content_section h220" data-handler="${BASE}/member/login/form" id="right_member_info">
                    <!-- 登录表单 -->
                    <div class="ld ld-ring ld-spin"></div>
                </div>
                <div class="side-block section">
                    <h6 class="side-block-head hl60 ti20">版块统计</h6>
                    <div class="side-block-body p-2" data-result="loader.json" data-handler="${BASE}/loader/stats/board.json" data-template-function="buildIndexSideBoardStats">
                        <p class="no-record-tip"><i class="ico mdi mdi-spinner" aria-hidden="true"></i> 看成败人生豪迈, 只不过是从头再来</p>
                    </div>
                </div>
            </div>
            <!-- 右侧结束 -->
            <!-- 左侧开始 -->
            <div id="forumlist">
                <div class="main-color">
                    <!-- board start-->
                    <div data-result="batch.jsonp" data-handler="${BASE}/loader/album.jsonp" data-function="packAlbumHoldSection" data-param="album" data-record="div[data-album]" data-query="size:4">
                        <h6 class="forumlist-head ti20 mt0 fs1">随便看看 <span class="float-right"><a class="rss-btn" target="_blank" href="${BASE}/rss/recent.xml"><i class="ico-sm mdi mdi-rss"></i></a></span></h6>
                        <!-- newed style -->
                        <c:forEach items="${rs}" var="topic">
                        <div class="media mc-topic">
                            <div class="media-left">
                                <a href="${BASE}/member/${topic.memberId}.xhtml" title="${topic.memberNickname}">
                                    <img class="media-object img-circle" src="${BASE}/member/avatar/${topic.memberId}.png" style="width: 64px; height: 64px;">
                                </a>
                            </div>
                            <div class="media-body mc-topic-body">
                                <article class="forum-article-header">
                                    <c:if test="${topic.tops}"><span class="label" title="置顶话题">&#x1F4CC</span> </c:if>
                                    <c:if test="${topic.goods}"><span class="label" title="精华话题">&#x1F48E</span> </c:if>
                                    <c:if test="${topic.albumId > 0}"><span class="label" title="图片话题">&#x1F304</span> </c:if>
                                    <c:if test="${topic.hots}"><span class="label" title="很火噢">&#x1F525</span> </c:if>
                                    <a href="${BASE}/topic/${topic.connect}.xhtml" class="forumtitle">${topic.title}</a>
                                </article>
                                <p>
                                   <a href="${BASE}/member/${topic.memberId}.xhtml" title="会员主页">${topic.memberNickname}</a> &#187; <forum:format value="${topic.entryDateTime}"/>
                                   <span class="float-right">
                                       <a href="${BASE}/board/${topic.board.connect}.xhtml">${topic.board.title}</a>
                                   </span>
                                </p>
                                <div class="mc-topic-body-txt" data-album="${topic.albumId}">
                                    <p>${topic.summary}</p>
                                </div>
                                <div class="topic_item_body_footer">
                                    <ul class="list-inline mc-topic-panel">
                                        <li class="list-inline-item">
                                            <a href="${BASE}/topic/${topic.connect}.xhtml"><i class="ico-sm mdi mdi-comment" aria-hidden="true"></i>&nbsp;&nbsp;${topic.stats.postses}</a>
                                        </li>
                                        <li class="list-inline-item">
                                            <span><i class="ico-sm ico-color-teal mdi mdi-thumb-up" aria-hidden="true"></i>&nbsp;&nbsp;${topic.stats.likes}</span>
                                        </li>
                                        <li class="list-inline-item">
                                            <span><i class="ico-sm ico-color-red mdi mdi-favorite" aria-hidden="true"></i>&nbsp;&nbsp;${topic.stats.favorites}</span>
                                        </li>
                                    </ul>
                                    <c:if test="${topic.stats.postses > 0 }">
                                    <span class="float-right footer_board_section">
                                        <i class="ico-sm mdi mdi-time"></i>&nbsp;<a href="${BASE}/member/${topic.stats.recentPostsMemberId}.xhtml" class="username-coloured">${topic.stats.recentPostsMemberNickname}</a> &#187; <forum:format value="${topic.stats.recentPostsDate}"/>
                                    </span>
                                    </c:if>
                                </div>
                            </div>
                        </div>
                        </c:forEach>
                    </div>
                    <p class="bg-default hl60 text-center"><a href="${BASE}/board/home">去版块查看更多</a>&nbsp; <i class="ico-sm mdi mdi-arrow-right" aria-hidden="true"></i></p>
                </div>
            </div>
            <!-- 左侧结束 -->
        </div>
    </div>
    <%@ include file = "include/footer.jsp" %>
  </body></html>