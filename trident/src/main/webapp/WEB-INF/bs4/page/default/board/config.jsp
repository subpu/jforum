<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>编辑版块下的话题策略配置</title>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                    <span class="crumb">
                        <a href="${BASE}/board/${board.connect}.xhtml">${board.title}</a>
                    </span>
                    <span class="crumb">编辑版块下的话题策略配置</span>
                </li>
            </ul>
            <%@ include file = "../include/page_search.jsp" %>
        </div>
    </div>
    <div class="container-fluid no-padding">
        <div class="container jumbotron w680 mt-3 main-color">
            <c:if test="${not empty errors}">
            <div class="alert alert-danger" role="alert">
                <strong>oOps!</strong> ${errors}
            </div>
            </c:if>
            <form method="post" action="${BASE}/board/config/edit" class="theme-form" data-submit="once">
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="readWrite">读写模式:</label>
                <div class="col-md-9 radio">
                    <label><input type="radio" name="readWrite" value="1"<c:if test="${'1' eq form.readWrite}"> checked="checked"</c:if>>读写</label> &nbsp;&nbsp;&nbsp;&nbsp;
                    <label><input type="radio" name="readWrite" value="0"<c:if test="${'0' eq form.readWrite}"> checked="checked"</c:if>>只读</label>
                    <small class="form-text text-muted">选择不同的模式,可以进行积分,会员组,会员等级限制</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="ipFilter">IP地址过滤:</label>
                <div class="col-md-9 radio">
                    <label><input type="radio" name="ipFilter" value="1"<c:if test="${'1' eq form.ipFilter}"> checked="checked"</c:if>>启用</label> &nbsp;&nbsp;&nbsp;&nbsp;
                    <label><input type="radio" name="ipFilter" value="0"<c:if test="${'0' eq form.ipFilter}"> checked="checked"</c:if>>关闭</label>
                    <p class="help-block"></p>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="readMinScore">只读积分:</label>
                <div class="col-md-9">
                    <input type="number" class="form-control w260" name="readMinScore" placeholder="最小积分要求" value="${form.readMinScore}" />
                    <small class="form-text text-muted">只读模式下会员积分的最低要求,单位:积分数</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="readLowMemberGroup">只读会员组:</label>
                <div class="col-md-9">
                    <select class="asyn-loadate-select" data-current="${form.readLowMemberGroup}" data-handler="${BASE}/member/group/json" name="readLowMemberGroup">
                        <option value="">请选择只读会员组</option>
                    </select>
                    <small class="form-text text-muted">只读模式下会员组的最低要求,单位:会员组枚举</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="readLowMemberRole">只读会员角色:</label>
                <div class="col-md-9">
                    <select class="asyn-loadate-select" data-current="${form.readLowMemberRole}" data-handler="${BASE}/member/role/json" name="readLowMemberRole">
                        <option value="">请选择只读会员角色</option>
                    </select>
                    <small class="form-text text-muted">只读模式下会员角色的最低要求,单位:会员角色枚举</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="readLowMemberLevel">只读等级:</label>
                <div class="col-md-9">
                    <input type="number" class="form-control w260" name="readLowMemberLevel" placeholder="最低等级要求" value="${form.readLowMemberLevel}" />
                    <small class="form-text text-muted">只读模式下会员等级的最低要求,单位:等级数</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="writeMinScore">读写积分:</label>
                <div class="col-md-9">
                    <input type="number" class="form-control w260" name="writeMinScore" placeholder="最小积分要求" value="${form.writeMinScore}" />
                    <small class="form-text text-muted">读写模式下会员积分的最低要求,单位:积分数</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="writeLowMemberGroup">读写会员组:</label>
                <div class="col-md-9">
                    <select class="asyn-loadate-select" data-current="${form.writeLowMemberGroup}" data-handler="${BASE}/member/group/json" name="writeLowMemberGroup">
                        <option value="">请选择读写会员组</option>
                    </select>
                    <small class="form-text text-muted">读写模式下会员组的最低要求,单位:会员组枚举</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="writeLowMemberRole">读写会员角色:</label>
                <div class="col-md-9">
                    <select class="asyn-loadate-select" data-current="${form.writeLowMemberRole}" data-handler="${BASE}/member/role/json" name="writeLowMemberRole">
                        <option value="">请选择读写会员角色</option>
                    </select>
                    <small class="form-text text-muted">读写模式下会员角色的最低要求,单位:会员角色枚举</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="writeLowMemberLevel">读写等级:</label>
                <div class="col-md-9">
                    <input type="number" class="form-control w260" name="writeLowMemberLevel" placeholder="最低等级要求" value="${form.writeLowMemberLevel}" />
                    <small class="form-text text-muted">读写模式下会员等级的最低要求,单位:等级数</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="writeMinInterrupt">连续操作次数:</label>
                <div class="col-md-9">
                    <div class="input-group w300">
                        <input type="number" class="form-control w260 " name="writeMinInterrupt" placeholder="连续操作次数" value="${form.writeMinInterrupt}" />
                        <div class="input-group-append">
                            <div class="input-group-text">次</div>
                        </div>
                    </div>
                    <small class="form-text text-muted">读写模式下同一会员连续发贴的次数限制.例:3,同一会员只能连续3次发布主题.防止恶意注水</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="editMinute">编辑时长:</label>
                <div class="col-md-9">
                    <div class="input-group w300">
                        <input type="number" class="form-control w260" name="editMinute" placeholder="可编辑的时长" value="${form.editMinute}" min="1" max="9"/>
                        <div class="input-group-append">
                            <div class="input-group-text">分钟</div>
                        </div>
                    </div>
                    <small class="form-text text-muted">读写模式下原作者可编辑的时长,值应取1-9之间;单位:分钟; 例:3,在提交后可在3分钟内进行编辑;-1表示不限制</small>
                </div>
                
            </div>
            <div class="form-group row justify-content-end">
                <div class="col-md-9">
                    <input type="hidden" name="record" value="${form.record}" />
                    <input type="hidden" name="token" value="${form.token}" />
                    <input type="hidden" name="boardId" value="${form.boardId}" />
                    <input type="hidden" name="volumesId" value="${form.volumesId}" />
                    <input type="submit" name="btn" value="更新配置文件" class="btn btn-primary" />
                </div>
            </div>
            </form>
        </div>
    </div>
    </body></html>