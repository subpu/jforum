<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>回复列表</title>
    <style>nav.page-navigation ul{text-align:center;justify-content: center;}</style>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                    <span class="crumb">
                        <a href="<c:url value="/member/home/"/>" title="会员中心">会员中心</a>
                    </span>
                    <span class="crumb">回复列表</span>
                </li>
            </ul>
            <%@ include file = "../include/page_search.jsp" %>
        </div>
    </div>
    <div class="container-fluid no-padding">
        <div class="container">
            <div class="row w-100">
                <!-- 左侧开始-->
                <div class="col-md-10 main-color mb-3" role="main">
                    <h5 class="home-record-header default-txt-color">我回复的话题<span class="float-right"><small>${total}条记录</small></span></h5>
                    <!-- 列表 -->
                    <div id="member-home-records"></div>
                    <!-- /列表 -->
                    <!-- 分页开始 -->
                    <div style="padding:20px 0;" class="home-page-section page_navigation" data-handler="${BASE}/member/home/posts/rawdata" data-function="drawMHomeRecord"></div>
                    <!-- 分页结束 -->
                </div>
                <!-- 左侧结束-->
                <!-- 右侧开始-->
                <div class="col-md-2 load_content_section" role="menu" id="member_menu_panel" data-query="active:posts" data-handler="${BASE}/member/home/panel">
                    <!-- 管理菜单 -->
                    <div class="ld ld-ring ld-spin"></div>
                </div>
                <!-- 右侧结束-->
            </div>
        </div>
    </div>
    <%@ include file = "../include/footer.jsp" %>
  </body></html>
