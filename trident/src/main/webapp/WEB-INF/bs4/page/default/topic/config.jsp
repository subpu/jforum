<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>编辑话题策略配置</title>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                    <span class="crumb">
                        <a href="${BASE}/board/${topic.board.connect}.xhtml">${topic.board.title}</a>
                    </span>
                    <span class="crumb mw600">
                        <a href="${BASE}/topic/${topic.connect}.xhtml">${topic.title}</a>
                    </span>
                    <span class="crumb">编辑话题策略配置</span>
                </li>
            </ul>
            <%@ include file = "../include/page_search.jsp" %>
        </div>
    </div>
    <div class="container-fluid no-padding">
        <div class="container jumbotron w680 mt-3 main-color">
            <c:if test="${not empty errors}">
            <div class="alert alert-danger" role="alert">
                <strong>oOps!</strong> ${errors}
            </div>
            </c:if>
            <form method="post" action="${BASE}/topic/config/edit" class="theme-form" data-submit="once">
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="privacy">话题公开:</label>
                <div class="col-md-9 radio">
                    <label><input type="radio" name="privacy" value="1"<c:if test="${'1' eq form.privacy}"> checked="checked"</c:if>>否</label> &nbsp;&nbsp;&nbsp;&nbsp;
                    <label><input type="radio" name="privacy" value="0"<c:if test="${'0' eq form.privacy}"> checked="checked"</c:if>>是</label>
                    <small class="form-text text-muted">所有人可见表示为公开(是),仅自已或朋友可见表示不公开(否)</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="reply">回复:</label>
                <div class="col-md-9 radio">
                    <label><input type="radio" name="reply" value="1"<c:if test="${'1' eq form.reply}"> checked="checked"</c:if>>开启</label> &nbsp;&nbsp;&nbsp;&nbsp;
                    <label><input type="radio" name="reply" value="0"<c:if test="${'0' eq form.reply}"> checked="checked"</c:if>>关闭</label>
                    <small class="form-text text-muted">选择关闭后,任何人都不可以回复</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="notify">通知:</label>
                <div class="col-md-9 radio">
                    <label><input type="radio" name="notify" value="1"<c:if test="${'1' eq form.notify}"> checked="checked"</c:if>>开启</label> &nbsp;&nbsp;&nbsp;&nbsp;
                    <label><input type="radio" name="notify" value="0"<c:if test="${'0' eq form.notify}"> checked="checked"</c:if>>关闭</label>
                    <small class="form-text text-muted">如果回复选择:开启,通知选择:开启;当有人回复时会收到回复通知消息</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="readMinScore">只读积分:</label>
                <div class="col-md-9">
                    <input type="number" class="form-control w260" name="readMinScore" placeholder="最小积分要求" value="${form.readMinScore}" />
                    <small class="form-text text-muted">只读模式下会员积分的最低要求,单位:积分数</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="readLowMemberGroup">只读会员组:</label>
                <div class="col-md-9">
                    <select class="asyn-loadate-select" data-current="${form.readLowMemberGroup}" data-handler="${BASE}/member/group/json" name="readLowMemberGroup" >
                        <option value="">请选择只读会员组</option>
                    </select>
                    <small class="form-text text-muted">只读模式下会员组的最低要求,单位:会员组枚举</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="readLowMemberRole">只读会员角色:</label>
                <div class="col-md-9">
                    <select class="asyn-loadate-select" data-current="${form.readLowMemberRole}" data-handler="${BASE}/member/role/json" name="readLowMemberRole" >
                        <option value="">请选择只读会员角色</option>
                    </select>
                    <small class="form-text text-muted">只读模式下会员角色的最低要求,单位:会员角色枚举</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="readLowMemberLevel">只读等级:</label>
                <div class="col-md-9">
                    <input type="number" class="form-control w260" name="readLowMemberLevel" placeholder="最低等级要求" value="${form.readLowMemberLevel}" />
                    <small class="form-text text-muted">只读模式下会员等级的最低要求,单位:等级数</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="writeMinScore">读写积分:</label>
                <div class="col-md-9">
                    <input type="number" class="form-control w260" name="writeMinScore" placeholder="最小积分要求" value="${form.writeMinScore}" />
                    <small class="form-text text-muted">读写模式下会员积分的最低要求,单位:积分数</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="writeLowMemberGroup">读写会员组:</label>
                <div class="col-md-9">
                    <select class="asyn-loadate-select" data-current="${form.writeLowMemberGroup}" data-handler="${BASE}/member/group/json" name="writeLowMemberGroup">
                        <option value="">请选择读写会员组</option>
                    </select>
                    <small class="form-text text-muted">读写模式下会员组的最低要求,单位:会员组枚举</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="writeLowMemberRole">读写会员角色:</label>
                <div class="col-md-9">
                    <select class="asyn-loadate-select" data-current="${form.writeLowMemberRole}" data-handler="${BASE}/member/role/json" name="writeLowMemberRole">
                        <option value="">请选择读写会员角色</option>
                    </select>
                    <small class="form-text text-muted">读写模式下会员角色的最低要求,单位:会员角色枚举</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="writeLowMemberLevel">读写等级:</label>
                <div class="col-md-9">
                    <input type="number" class="form-control w260" name="writeLowMemberLevel" placeholder="最低等级要求" value="${form.writeLowMemberLevel}" />
                    <small class="form-text text-muted">读写模式下会员等级的最低要求,单位:等级数</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="writeMinInterrupt">连续操作次数:</label>
                <div class="col-md-9">
                    <div class="input-group w300">
                        <input type="number" class="form-control w260 " name="writeMinInterrupt" placeholder="连续操作次数" value="${form.writeMinInterrupt}" />
                        <div class="input-group-append">
                            <div class="input-group-text">次</div>
                        </div>
                    </div>
                    <small class="form-text text-muted">读写模式下同一会员连续回贴的次数限制.例:3,同一会员只能连续进行3次回复.防止恶意注水</small>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="atomPoster">原子回复:</label>
                <div class="col-md-9 radio">
                    <label><input type="radio" name="atomPoster" value="1"<c:if test="${'1' eq form.atomPoster}"> checked="checked"</c:if>>开启</label> &nbsp;&nbsp;&nbsp;&nbsp;
                    <label><input type="radio" name="atomPoster" value="0"<c:if test="${'0' eq form.atomPoster}"> checked="checked"</c:if>>关闭</label>
                    <small class="form-text text-muted">原子回复:开启,每一位会员只允许回复一次,不可以重复回复,楼主可以重复回复</small>
                </div>
            </div>
            <div class="form-group row justify-content-end">
                <div class="col-md-9">
                    <input type="hidden" name="record" value="${form.record}" />
                    <input type="hidden" name="token" value="${form.token}"/>
                    <input type="hidden" name="topicId" value="${form.topicId}"/>
                    <input type="hidden" name="boardId" value="${form.boardId}"/>
                    <input type="hidden" name="volumesId" value="${form.volumesId}" />
                    <input type="submit" name="btn" value="更新配置文件" class="btn btn-primary" />
                </div>
            </div>
            </form>
        </div>
    </div>
    </body></html>