<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>${page.title}</title>
    <style>#toc{padding: 20px 0;}ol>li{margin-left:15px;padding:5px 0}</style>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                    <span class="crumb">
                        <a href="${section.link}">${section.title}</a>
                    </span>
                    <span class="crumb">
                        ${page.title}
                    </span>
                </li>
            </ul>
            <%@ include file = "../include/page_search.jsp" %>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container main-color">
            <div class="row w-100">
                <div class="col-md-3" data-handler="${BASE}/article/menu?bd=${page.category}&id=${page.id}" id="spa-page-menu">loading</div>
                <div class="col-md-9" style="padding:20px 0">
                    <h5>${page.title}</h5>
                    <div>${page.body}</div>
                    <div>
                        <nav aria-label="...">
                            <ul class="pagination w-100" id="page_navigate" data-handler="${BASE}/article/navigate" data-query="id:${page.id},bd:${page.category}">
                                <li class="page-item previous w-50 disabled"><a href="#"><span aria-hidden="true">&larr;</span> 前一条</a></li>
                                <li class="page-item next w-50 text-right disabled"><a href="#">下一条 <span aria-hidden="true">&rarr;</span></a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%@ include file = "../include/footer.jsp" %>
  </body></html>