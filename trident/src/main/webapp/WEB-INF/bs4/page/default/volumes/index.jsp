<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>${boardGroup.title}</title>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                </li>
            </ul>
            <%@ include file = "../include/page_search.jsp" %>
        </div>
    </div>
    
    <div class="container-fluid"  style="clear:both">
        <div class="container">
            <!-- 右侧开始 -->
            <div id="sidebar">
                <div class="side-block main-color" style="height:280px;">
                    <div style="padding-top:30px;">
                        <div class="text-center" style="height:auto;"><img class="rounded-circle" src="${BASE}/board/volumes/ico/${boardGroup.id}.png" style="width:100px;height:100px"></div>
                        <div class="text-center">
                            <h5 class="default-txt-color">${boardGroup.title}</h5>
                            <p class="mid_parallel_lines">&#x1F482&nbsp;大版主</p>
                            <div class="section" data-result="loader.json" data-empty-show="false" data-handler="${BASE}/loader/volumes/moderator.json" data-query="id:${boardGroup.id}" data-template-function="drawBoardGroupModeratorList">
                                <p class="no-record-tip" style="padding:0;background-color: transparent;">loading...</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="side-block section">
                    <h6 class="side-block-head hl60 ti20">精华话题</h6>
                    <div class="side-block-body p-2" data-result="loader.json" data-template-function="buildRightSectionTopic" data-query="id:${boardGroup.id}" data-handler="${BASE}/loader/volumes/topic/best.json">
                        <p class="no-record-tip"><i class="ico mdi mdi-spinner" aria-hidden="true"></i> 看成败人生豪迈, 只不过是从头再来</p>
                    </div>
                </div>
                <div class="side-block section">
                    <h6 class="side-block-head hl60 ti20">热点话题</h6>
                    <div class="side-block-body p-2" data-result="loader.json" data-template-function="buildRightSectionTopic" data-query="id:${boardGroup.id}" data-handler="${BASE}/loader/volumes/topic/hot.json">
                        <p class="no-record-tip"><i class="ico mdi mdi-spinner" aria-hidden="true"></i> 看成败人生豪迈, 只不过是从头再来</p>
                    </div>
                </div>
            </div>
            <!-- 右侧结束 -->
            <!-- 左侧开始 -->
            <div id="forumlist">
                <!-- 轮播图开始 -->
                <div id="carousel-top" data-handler="${BASE}/topic/carousel/${boardGroup.id}-0.json" data-carousel="bootstrap" style="height:290px">
                    <div class="ld ld-ring ld-spin"></div>
                </div>
                <!-- 轮播图结束 -->
                <div>
                    <!-- 子版块开始 -->
                    <div>
                        <div class="board_list">
                            <ul class="board_list_header subject-color">
                                <li class="hl60">
                                    <dl class="icon">
                                        <dt>
                                            <div class="ti20">版块名称</div>
                                        </dt>
                                        <dd class="wp10">话题数</dd>
                                        <dd class="wp10">回复数</dd>
                                        <dd class="latest">最近主题</dd>
                                    </dl>
                                </li>
                            </ul>
                            <ul class="board_list_header" data-result="batch.jsonp" data-handler="${BASE}/loader/board/stats.jsonp" data-function="loadBoardStatsResult" data-param="ids" data-record="li[data-board]">
                                <c:forEach items="${boardes}" var="board">
                                <li class="board_item" data-board="${board.id}">
                                    <dl class="icon forum_${board.icoStatus}">
                                        <dt>
                                            <div class="board_item_title">
                                                <a href="${BASE}/board/${board.connect}.xhtml" class="forum-article-header">${board.title} &nbsp;<small class="today-counter">(0)</small></a>
                                                <div class="txt-sm">${board.description}</div>
                                            </div>
                                        </dt>
                                        <dd class="threads wp10 stats-item">0<dfn>Threads</dfn></dd>
                                        <dd class="replies wp10 stats-item">0<dfn>Replies</dfn></dd>
                                        <dd class="latest"></dd>
                                    </dl>
                                </li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                    <!-- 子版块结束 -->
                    <!-- 版块组最近话题开始 -->
                    <div class="mt-2 mb-2">
                        <div class="board_list">
                            <ul class="board_list_header subject-color">
                                <li class="hl60">
                                    <dl class="icon">
                                        <dt>
                                            <div class="ti20">最近主题</div>
                                        </dt>
                                        <dd class="wp10">回复数</dd>
                                        <dd class="wp10">查看数</dd>
                                        <dd class="latest">最后回复</dd>
                                    </dl>
                                </li>
                            </ul>
                            <ul class="board_list_header" id="board_topic_collect" data-result="loader.json" data-handler="${BASE}/loader/volumes/topic/recent.json" data-query="id:${boardGroup.id}" data-template-function="drawThreads">
                            </ul>
                        </div>
                    </div>
                    <!-- 版块组最近话题结束 -->
                </div>
            </div>
            <!-- 左侧结束 -->
        </div>
    </div>
    <%@ include file = "../include/footer.jsp" %>
  </body></html>