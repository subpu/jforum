<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>移动话题</title>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                    <span class="crumb">移动话题</span>
                </li>
            </ul>
            <%@ include file = "../include/page_search.jsp" %>
        </div>
    </div>
    <div class="container-fluid no-padding">
        <div class="container jumbotron w680 mt-3 main-color">
            <c:if test="${not empty errors}">
            <div class="alert alert-danger" role="alert">
                <strong>oOps!</strong> ${errors}
            </div>
            </c:if>
            <form method="post" action="${BASE}/topic/move" class="theme-form" data-submit="once">
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="title">话题:</label>
                <div class="col-md-9">
                    <input type="text" class="inputbox no-radius form-control w600" value="${form.topicTitle}" name="topicTitle"  readonly="readonly" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="names">当前版块:</label>
                <div class="col-md-9">
                    <input type="text" class="inputbox no-radius form-control w600" value="${form.boardTitle}"  name="boardTitle"  readonly="readonly" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-3 control-label text-right" for="board">移至版块:</label>
                <div class="col-md-9" 
                     data-result="loader.jsonp" 
                     data-handler="${BASE}/loader/board/except.jsonp" 
                     data-function="buildBoardFormTemp" 
                     data-query="board:${form.boardId}" 
                     data-container="resultBox_20191115">
                    <select name="targetBoardId" tabindex="1" required="required" class="form-control" id="resultBox_20191115">
                    </select>
                </div>
            </div>
            <div class="form-group row  justify-content-end">
                <div class="col-md-9">
                    <input type="hidden" name="topicId" value="${form.topicId}"/>
                    <input type="hidden" name="boardId" value="${form.boardId}"/>
                    <input type="hidden" name="volumesId" value="${form.volumesId}"/>
                    <input type="hidden" name="token" value="${form.token}"/>
                    <input type="submit" name="edit" tabindex="3" value="开始移动" class="btn btn-primary" />
                </div>
            </div>
            </form>
        </div>
    </div>
  </body></html>