<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>发件箱的消息列表</title>
    <style>.media{padding:10px 0;}.media:last-child{border-bottom-style:none}.media-heading{margin-top:5px}nav.page-navigation ul{text-align:center;justify-content: center;}</style>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                    <span class="crumb">消息</span>
                    <span class="crumb">发件箱</span>
                </li>
            </ul>
            <%@ include file = "../include/page_search.jsp" %>
        </div>
    </div>
    <div class="container-fluid no-padding">
        <div class="container">
            <div class="row w-100">
                <!-- 左侧开始-->
                <div class="col-md-10 main-color mb-3" role="main">
                    <div style="position:relative">
                        <ul class="list-inline nav-section">
                            <li class="list-inline-item"><a href="${BASE}/message/">收件箱</a></li>
                            <li class="list-inline-item active"><a href="#">发件箱</a></li>
                            <li class="list-inline-item"><a href="${BASE}/message/create">新消息</a></li>
                        </ul>
                    </div>
                    
                    <p class="bg-ault hl60"><span class="float-right"><small>${total}条记录</small></span></p>
                    <!-- 列表 -->
                    <div id="member-message-records"></div>
                    <!-- /列表-->
                    <!-- 分页开始 -->
                    <div style="padding:20px 0;" class="home-page-section page_navigation" data-handler="${BASE}/message/outbox/rawdata" data-function="memberOutboxTemplate"></div>
                    <!-- 分页结束 -->
                </div>
                <!-- 左侧结束-->
                <!-- 右侧开始-->
                <div class="col-md-2 load_content_section" role="menu" id="member_menu_panel" data-query="active:mesg_sent" data-handler="${BASE}/member/home/panel">
                    <!-- 管理菜单 -->
                </div>
                <!-- 右侧结束-->
            </div>
        </div>
    </div>
    <%@ include file = "../include/footer.jsp" %>
  </body></html>