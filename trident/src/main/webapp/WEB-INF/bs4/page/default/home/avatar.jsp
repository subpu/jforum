<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>选择头像</title>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                    <span class="crumb">
                        <a href="<c:url value="/member/home/"/>" title="会员中心">会员中心</a>
                    </span>
                    <span class="crumb">选择头像</span>
                </li>
            </ul>
            <%@ include file = "../include/page_search.jsp" %>
        </div>
    </div>
    <div class="container-fluid no-padding">
        <div class="container">
            <div class="row w-100">
                <!-- 左侧开始-->
                <div class="col-md-10 main-color mb-3 pt-2 pb-2" role="main">
                    <div class="alert alert-info" role="alert">
                       <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>&#x1F4A1 &nbsp;点击图片变更头像. 当前选中的头像为绿色边框
                    </div>
                    <div class="row" id="avatarBox">
                        <!-- foreach -->
                        <c:forEach items="${rs}" var="defAvtar">
                        <div class="col-md-2 col-xs-3">
                            <a href="javascript:;" class="thumbnail trans-bg member-avatar-section">
                                <c:set var="curAvtar" value="${fn:substringAfter(defAvtar, 'avatar')}"></c:set>
                                <img src="${defAvtar}" class="rounded-circle member_default_avtar<c:if test="${curAvtar eq active }"> active</c:if>" data-path="${curAvtar}" data-current="${active}" style="width:90%"/>
                            </a>
                        </div>
                        </c:forEach>
                        <!-- foreach -->
                    </div>
                </div>
                <!-- 左侧结束-->
                <!-- 右侧开始-->
                <div class="col-md-2 load_content_section" role="menu" id="member_menu_panel" data-query="active:avatar" data-handler="${BASE}/member/home/panel">
                    <!-- 管理菜单 -->
                    <div class="ld ld-ring ld-spin"></div>
                </div>
                <!-- 右侧结束-->
            </div>
        </div>
    </div>
    <%@ include file = "../include/footer.jsp" %>
  </body></html>