<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set value="${pageContext.request.contextPath}" var="BASE" />
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<!-- 右侧菜单开始 -->
	<div class="home_menu main-color">
		<ul class="list-unstyled profile_menu">
			<li <c:if test="${param.active eq 'home'}"> class="focus"</c:if>><a href="${BASE}/member/home/">个人中心</a></li>
			<li <c:if test="${param.active eq 'profile'}"> class="focus"</c:if>><a href="${BASE}/member/home/profile">个性签名</a></li>
			<li <c:if test="${param.active eq 'avatar'}"> class="focus"</c:if>><a href="${BASE}/member/home/avatar">选择头像</a></li>
			<li <c:if test="${param.active eq 'passport'}"> class="focus"</c:if>><a href="${BASE}/member/home/passport">密码设置</a></li>
			<li <c:if test="${param.active eq 'history'}"> class="focus"</c:if>><a href="${BASE}/member/home/history">历史记录</a></li>
			<li role="separator" class="divider"></li>
			<li <c:if test="${param.active eq 'mesg_inbox'}"> class="focus"</c:if>><a href="${BASE}/message/">消息</a></li>
			<li <c:if test="${param.active eq 'mesg_sent'}"> class="focus"</c:if>><a href="${BASE}/message/sent">发件箱</a></li>
			<li <c:if test="${param.active eq 'mesg_create'}"> class="focus"</c:if>><a href="${BASE}/message/create">新建消息</a></li>
			<li role="separator" class="divider"></li>
			<li <c:if test="${param.active eq 'topic'}"> class="focus"</c:if>><a href="${BASE}/member/home/topic">话题列表</a></li>
			<li <c:if test="${param.active eq 'posts'}"> class="focus"</c:if>><a href="${BASE}/member/home/posts">回复列表</a></li>
			<li <c:if test="${param.active eq 'topic_favorite'}"> class="focus"</c:if>><a href="${BASE}/member/home/topic/favorite">收藏列表</a></li>
			<li <c:if test="${param.active eq 'topic_like'}"> class="focus"</c:if>><a href="${BASE}/member/home/topic/like">点赞列表</a></li>
		</ul>
	</div>
	<!-- 右侧菜单结束 -->
</body>
</html>