<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <!-- 顶部导航条开始 -->
    <header class="container-fluid hl80" id="sitehead">
        <div class="container">
            <div id="leftmenu" class="col-md-9 text-left" style="padding-left:0">
                <div id="menu"></div>
                <div id="logo"><a href="<c:url value="/"/>"><img src="${DOMAIN}/${LOGO}" style="width:23%"/></a></div>
            </div>
            <div id="topmenu" class="col-md-3 text-right">
                <div class="btn-group" role="group" id="header_member_panel" data-handler="${DOMAIN}/member/panel">
                    <a role="button" class="btn btn-default forum_member_defat_link" href="${DOMAIN}/member/register">注册</a>
                    <a role="button" class="btn btn-primary forum_member_defat_link" href="${DOMAIN}/member/login">登陆</a>
                </div>
            </div>
        </div>
    </header>
    <!-- 顶部导航条结束 -->