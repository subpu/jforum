<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>消息内容</title>
    <style>
.media{position:relative}
.media{position:relative;width:60%;clear:both;margin-top:1rem}
.media .media-body{margin-left: 10px;}
.media-master{float:right}
.media-master .media-avatar{position: absolute;right: 0;top: 0;}
.media-master .media-heading{text-align:right;}
.media-master .media-body:before{right:80px;left:auto;background-color: #007bff}
.media-heading small{color:#333}
.media-body:before{
    content: "";
    position: absolute;
    width: 10px;
    height: 10px;
    background-color: #f1f1f1;
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg);
    top: 15.5px;
    left: 70px;
}
.media-bubble{
    width: auto;
    background-color:#f1f1f1;
    padding: 10px;
    float: left;
    border-radius: 5px;
    color:#333;
}
.media-master .media-bubble{float:right;padding-right:10px;margin-right: 85px;}
.snowhite .media-bubble a{color:#007bff}
.snowhite .media-master .media-bubble{background-color:#007bff;color:#fff;}
.snowhite .media-master .media-body:before{background-color:#007bff}
.snowhite .letter-item-collect .media-master .media-heading small{color:#fff}
.jetblack .media-bubble a{color:#575f69}
.jetblack .media-master .media-bubble{background-color:#575f69;color:#ccc;}
.jetblack .media-master .media-body:before{background-color:#575f69}
.jetblack .letter-item-collect .media-master .media-heading small{color:#ccc}
    </style>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                    <span class="crumb">消息</span>
                    <span class="crumb">内容</span>
                </li>
            </ul>
            <%@ include file = "../include/page_search.jsp" %>
        </div>
    </div>
    <div class="container-fluid no-padding">
        <div class="container">
            <div class="row w-100">
                <div class="col-md-10 mb-3" role="main">
                    <!-- 聊天界面的头部开始 -->
                    <div class="letter-collect-header">与${senderNickname}往来的消息
                        <span class="float-right" style="margin-right:10px;">
                            <small>
                                <a href="javascript:;" class="readall-message-action" data-handler="${BASE}/message/read/all?member=${sender}&direction=1">全部标记为已读</a>
                            </small>
                        </span>
                    </div>
                    <!-- 聊天界面的头部结束 -->
                    <!-- 消息记录正区开始 -->
                    <div class="letter-item-collect" id="message-${sender}-box" data-page="1" data-handler="${BASE}/message/view/more?sender=${sender}">
                        <p class="text-center<c:if test="${not hasMore}"> d-none</c:if>"><a class="txt-sm p-2 letter-more-action"  href="javascript:;">更多历史消息</a></p>
                        <c:forEach items="${rs}" var="forumLetter">
                        <div class="media <c:if test="${forumLetter.author == master.id}">media-master</c:if>" id="letter-${forumLetter.id}">
                            <div class="media-avatar media-left">
                                <a href="javascript:;">
                                    <img class="media-object img-circle" src="${BASE}/member/avatar/${forumLetter.author}.png" alt="${forumLetter.nickname}" style="height:64px;width:64px;">
                                </a>
                            </div>
                            <div class="media-body">
                                <div class="media-bubble">
                                    <h6 class="media-heading"><small><forum:format value="${forumLetter.entryDateTime}"/></small></h6>
                                    <c:if test="${forumLetter.author == 0}">
                                    <strong>${forumLetter.title}</strong>
                                    </c:if>
                                    <p>${forumLetter.content}</p>
                                </div>
                            </div>
                        </div>
                        </c:forEach>
                    </div>
                    <!-- 消息记录正区结束 -->
                    <c:if test="${sender > 0}">
                    <div class="letter-publish-form main-color">
                        <form id="send-message-form" data-submit="once">
                        <div class="form-group">
                            <textarea class="form-control" name="content" placeholder="输入发送的消息内容" required="required" tabindex="1" style="border-style:none;border-width:0;background-color: #f1f1f1;resize:none;"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="text-muted">&#x1F4A1 只支持纯文本内容.html标签会被清除</label>
                            <span class="float-right">
                                <a tabindex="2" role="button" class="btn btn-primary send-message-action" href="javascript:;" data-handler="${BASE}/message/transmit" data-query="receiver:${sender},names:${senderNickname}"><span class="ico-sm mdi mdi-mail-send" aria-hidden="true"></span>&nbsp;发送</a>
                            </span>
                        </div>
                        </form>
                    </div>
                    <!-- 消息互动界表单结束-->
                    </c:if>
                </div>
                <div class="col-md-2 load_content_section" role="menu" id="member_menu_panel" data-query="active:mesg_inbox" data-handler="${BASE}/member/home/panel">
                    <!-- 管理菜单 -->
                </div>
            </div>
        </div>
    </div>
    <%@ include file = "../include/footer.jsp" %>
  </body></html>