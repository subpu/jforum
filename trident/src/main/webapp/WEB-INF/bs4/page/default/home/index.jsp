<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>个人中心</title>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                    <span class="crumb">会员中心</span>
                </li>
            </ul>
            <%@ include file = "../include/page_search.jsp" %>
        </div>
    </div>
    <div class="container-fluid no-padding">
        <div class="container">
            <div class="row w-100">
                <!-- 左侧开始-->
                <div class="col-md-10" role="main">
                    <div class="row member-bg-${member.style}">
                        <div class="col-md-8 member-profile">
                            <div class="member-profile-avatar"><span><img class="rounded-circle" src="${BASE}/member/avatar/${member.id}.png" width="100px" height="100px"/></span></div>
                            <div class="member-profile-info">
                                <h4 style="margin-bottom:0">${member.names} @u${member.id}</h4>
                                <p>上次登录: ${member.prevLoginDateTime}</p>
                            </div>
                        </div>
                        <div class="col-md-4 member-profile-ext">
                            <dl class="col-dl-3p"><dt>组</dt><dd>${member.groupNames}</dd></dl>
                            <dl class="col-dl-3p"><dt>角色</dt><dd>${member.roleNames}</dd></dl>
                            <dl class="col-dl-3p"><dt>注册日期</dt><dd>${member.logonDateTime}</dd></dl>
                        </div>
                    </div>
                    <div class="row main-color member-profile-stats">
                        <div class="col-sm-3"><dl class="col-dl-4p"><dt>话题</dt><dd>${member.threads}</dd></dl></div>
                        <div class="col-sm-3"><dl class="col-dl-4p"><dt>回复</dt><dd>${member.replies}</dd></dl></div>
                        <div class="col-sm-3"><dl class="col-dl-4p"><dt>积分</dt><dd>${member.score}</dd></dl></div>
                        <div class="col-sm-3"><dl class="col-dl-4p"><dt>等级</dt><dd>${member.levelNo} / ${member.level}</dd></dl></div>
                    </div>
                    <div class="row main-color member-collect-item mt-3">
                        <h5 class="hl60 w-100 ti20 default-txt-color">收藏的版块<span class="float-right"><a class="btn btn-sm" href="javascript:;" role="button" id="remove-favorite-btn" data-handler="${BASE}/board/star/removes"><i class="mdi mdi-delete"></i>&nbsp;删除&nbsp;<label id="fav-focus-counter">0</label></a></span></h5>
                        <div class="col-md-12 member-profile-collect" id="member-favorite-board" data-result="loader.json" data-handler="${BASE}/member/home/board/active/json" data-query="member:${member.id}" data-template-function="buildMemberFavoriteBoard">
                            <p class="text-center align-middle no-record-tip" style="line-height:180px;background-color:transparent">去<a href="${BASE}/board/home">版块列表</a>逛一逛吧</p>
                        </div>
                    </div>
                    <div class="row main-color member-collect-item mt-3 mb-3">
                        <h5 class="hl60  w-100 ti20 default-txt-color">最受欢迎的话题<span class="float-right"><a class="btn btn-sm" href="${BASE}/member/home/topic">&#187;&nbsp;更多</a></span></h5>
                        <div class="col-md-12 member-profile-collect" style="padding-top:30px" id="member-publish-topic" data-result="loader.json" data-handler="${BASE}/member/home/topic/popular/json" data-template-function="basicThreadTemplate">
                            <p class="text-center align-middle no-record-tip" style="line-height:120px;background-color:transparent">快来分享您的发现吧</p>
                        </div>
                    </div>
                </div>
                <!-- 左侧结束-->
                <!-- 右侧开始-->
                <div class="col-md-2 load_content_section" role="menu" id="member_menu_panel" data-query="active:home" data-handler="${BASE}/member/home/panel">
                    <!-- 管理菜单 -->
                    <div class="ld ld-ring ld-spin"></div>
                </div>
                <!-- 右侧结束-->
            </div>
        </div>
    </div>
    <%@ include file = "../include/footer.jsp" %>
  </body></html>