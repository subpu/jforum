<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <footer id="footer">
        <section class="container">
            <div class="row pt-3">
                <div class="col-md-9">
                    <div id="copyright" class="pt10">
                        <p>Orion jForum: Java Free And Open Source Forum © 2020. All Rights Reserved</p>
                    </div><!--/#copyright-->
                </div>
                <div class="col-md-3">
                    <ul class="list-inline pull-right">
                        <li class="list-inline-item">
                            <a rel="nofollow" 
                                   class="social-tooltip"  
                                   href="javascript:;" 
                                   target="_blank" ><i class="ico mdi mdi-windows"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a rel="nofollow" 
                                   class="social-tooltip"  
                                   href="javascript:;" 
                                   target="_blank" ><i class="ico mdi mdi-android"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a rel="nofollow" 
                                   class="social-tooltip"  
                                   href="javascript:;" 
                                   target="_blank" ><i class="ico mdi mdi-apple"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a rel="nofollow" 
                                   class="social-tooltip"  
                                   title="Follow us on Rss" 
                                   aria-label="Follow us on Rss" 
                                   href="${DOMAIN}/rss/" 
                                   target="_blank" ><i class="ico mdi mdi-rss"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div><!--/.container-inner-->
        </section><!--/.container-->
    </footer><!--/#footer-->
    <!-- 侧边栏工具条开始 -->
    <section id="sideTool" data-tools="feedback,theme"></section>
    <!-- 侧边栏工具条结束 -->