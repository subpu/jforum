<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set value="${pageContext.request.contextPath}" var="BASE"/>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>会员登录表单</title>
  </head>
  <body>
      <form action="${BASE}/member/login/quick" method="post" id="quick_login_form">
          <h6 class="side-block-head hl60 ti20"><a href="${BASE}/member/login">会员登陆</a>&nbsp; | &nbsp;<a href="${BASE}/member/register">免费注册</a></h6>
          <div class="side-block-body p20">
              <fieldset>
                  <input type="text" name="names" id="names" size="10" class="inputbox" placeholder="会员帐号" style="width:100%" required="required" tabindex="1"/>
                  <input type="password" name="password" id="password" size="10" class="inputbox" placeholder="会员密码" autocomplete="off" required="required" tabindex="2"/>
                  <input type="hidden" name="token" value="${token}"/>
                  <input type="submit" name="login" value="马上登录" class="btn btn-primary txt-mid" tabindex="3"/> &nbsp; &nbsp;<a href="${BASE}/lostPswd" role="button" class="btn btn-link pull-right txt-sm" tabindex="4">找回密码</a>
              </fieldset>
          </div>
      </form>
  </body>
</html>