<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>${topic.title}</title>
    <style>
    /*--*/
    .mood-section{text-align:right;height:30px;position: absolute;right: 10px;bottom: 45px;}.mood-section span{padding-left:5px}.mood-section a:hover{color:#2e6da4}
    </style>
  </head>
  <body>
    <div class="container-fluid no-padding topic_normal_color" id="sitebanner">
        <div class="container">
            <ul id="breadcrumbs">
                <li class="breadcrumbs">
                    <span class="crumb">
                        <a href="<c:url value="/board/home"/>" title="论坛首页">论坛首页</a>
                    </span>
                    <span class="crumb">
                        <a href="${BASE}/board/volumes/${topic.board.volumes.connect}.xhtml">${topic.board.volumes.title}</a>
                    </span>
                    <span class="crumb">
                        <a href="${BASE}/board/${topic.board.connect}.xhtml" title="loading">${topic.board.title}</a>
                    </span>
                </li>
            </ul>
            <%@ include file = "../include/page_search.jsp" %>
        </div>
    </div>
    <div class="container" data-result="loader.json" data-handler="${BASE}/topic/browse">
        <div style="clear:both">
            <h5 class="default-txt-color">${topic.title}</h5>
            <ul class="list-inline" id="topic-stats-section">
                <li class="list-inline-item"><i class="ico mdi mdi-bookmark"></i> <c:out value="${topic.topicCategoryName}" default="普通"/></li>
                <li class="list-inline-item"><i class="ico mdi mdi-eye"></i> 查看 <span id="stats-views"><c:out value="${topic.stats.displaies}" default="0"/></span></li>
                <li class="list-inline-item"><i class="ico mdi mdi-comment"></i> 回复 <span id="stats-replies"><c:out value="${topic.stats.postses}" default="0"/></span></li>
                <li class="list-inline-item"><i class="ico mdi mdi-time"></i> 活跃 <span><forum:format value="${topic.rankingDateTime}"/></span></li>
            </ul>
        </div>
        <!-- 话题统计结束 -->
        <!-- 顶部功能区开始 -->
        <div class="row">
            <div class="col-md-6">
                <c:choose>
                <c:when test="${topic.status == 'LOCKED'}">
                <a href="javascript:;" class="btn btn-secondary disabled" role="button"><i class="ico-sm mdi mdi-lock" aria-hidden="true"></i> 锁定</a>
                </c:when>
                <c:when test="${topic.normal}">
                <a href="${BASE}/posts/create" class="btn btn-primary" role="button">回复 <i class="ico-sm mdi mdi-mail-reply"></i></a>
                </c:when>
                <c:otherwise>
                <a href="javascript:;" class="btn btn-secondary disabled" role="button"><i class="ico-sm mdi mdi-block"></i> 回复</a>
                </c:otherwise>
                </c:choose>
                <!-- 话题工具开始 -->
                <div class="btn-group" data-result="click.jsonp" data-handler="${BASE}/topic/tool.jsonp" data-function="buildMenuList" data-selector=".topic-menu-list">
                    <button type="button" class="btn  btn-secondary"><i class="ico-sm mdi mdi-wrench"></i> &nbsp;工具</button>
                    <button type="button" class="btn  btn-secondary dropdown-toggle-split caret-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu topic-menu-list"></div>
                </div>
                <!-- 分享 -->
                <a href="${BASE}/topic/poster" class="btn btn-success bds_more" data-cmd="more" role="button"><i class="ico-sm mdi mdi-link" aria-hidden="true"></i> 分享</a>
                <!-- 话题工具结束 -->
            </div>
            <div class="col-md-6">
                <!-- 分页开始 -->
                <div id="page_another_navigation" class="frinner"></div>
                <!-- 分页结束 -->
            </div>
        </div>
        <!-- 顶部功能区结束 -->
        <!-- 回复区段开始 -->
        <div id="topic_posts_collect" class="mb20">
        <div class="topic_posts">
            <div class="topic_posts_item">
                <dl class="topic_posts_item_left" data-handler="${BASE}/member/profile.json?id=${topic.memberId}">
                    <dt style="padding-top: 20px;padding-left: 20px;width:180px">
                        <div class="posts-member-avatar">
                            <a href="javascript:;" class="avatar poper-member-menu">
                                <img class="avatar" src="${BASE}/member/avatar/${topic.memberId}.png" alt="User avatar" />
                            </a>
                        </div>
                        <a href="${BASE}/member/${topic.memberId}.xhtml" class="member-info"> <span class="member-${topic.memberId}-label member-${authorStyle}">${topic.memberNickname}</span></a>
                    </dt>
                    <dd>${topic.content.member.mgroup.title}</dd>
                    <dd><small class="badge badge-dark">楼主</small></dd>
                </dl>
                <div class="topic_posts_item_right">
                    <div id="posts-${topic.content.id}" class="posts-body">
                        <ul class="posts-buttons">
                            <!-- 仅查看楼主回复 -->
                            <li><a href="${BASE}/topic/${topic.connect}.xhtml?author=${topic.memberId}"><i class="ico-sm mdi mdi-account" aria-hidden="true"></i><span>只看楼主</span></a></li>
                            <!-- 快速发贴:引用回复 -->
                            <li><a href="javascript:;" class="post-action-quote" data-handler="${BASE}/posts/quote/data.json" data-query="id:${topic.content.id}"><i class="ico-sm mdi mdi-quote"></i><span>回复</span></a></li>
                            <!-- 举报 -->
                            <li><a href="javascript:;" class="post-action-report" data-handler="${BASE}/posts/report" data-query="id:${topic.content.id}"><i class="ico-sm mdi mdi-flag"></i><span>举报</span></a></li>
                            <!-- 楼层 -->
                            <li><a name="#posts-${topic.content.id}">1<sup>#</sup></a></li>
                        </ul>
                        <p class="posts-member">
                            <a href="${BASE}/member/${topic.memberId}.xhtml" title="Topic's Original Poster">${topic.memberNickname} @u${topic.memberId}</a>&nbsp;&nbsp;&#187;&nbsp;&nbsp;<i class="ico-sm mdi mdi-time" aria-hidden="true"></i> <forum:format value="${topic.entryDateTime}"/>
                            <c:if test="${topic.tops}">&nbsp;&nbsp;|&nbsp;&nbsp;<span>&#x1F4CC&nbsp;置顶</span></c:if> 
                            <c:if test="${topic.goods}">&nbsp;&nbsp;|&nbsp;&nbsp;<span>&#x1F48E&nbsp;精华</span></c:if>
                            <c:if test="${topic.hots}">&nbsp;&nbsp;|&nbsp;&nbsp;<span>&#x1F525&nbsp;火贴</span></c:if> 
                        </p>
                        <div class="posts-body-content">
                            <forum:decorate posts = "${topic.content}">
                                <forum:blockTip><p class="alert alert-danger"><strong>提示:</strong> 作者被禁止发言或内容自动屏蔽</p></forum:blockTip>
                                <forum:editTip><p class="alert alert-warning">回复最近由 ${modifyer} 于 ${modifyDate} 编辑</p></forum:editTip>
                            </forum:decorate>
                            <br/>
                            <!-- 标签列表  开始-->
                            <c:if test="${not empty tages}">
                            <div id="topic_tages">
                                <label>标签</label>
                                <c:forEach items="${tages}" var="tag">
                                <a href="javascript:;" class="action-cmd tag-item-anchor" data-rates="${tag.rates}" data-handler="${BASE}/topic/tag/delete" data-query="id:${tag.id}" data-function="delTagHandler">${tag.names}</a>
                                </c:forEach>
                                <a href="javascript:;" id="tag-action-add" data-handler="${BASE}/topic/tag/add" data-toggle="tooltip" data-placement="right" title="添加新标签"><i class="ico-sm mdi mdi-plus"></i></a>
                            </div>
                            </c:if>
                            <!-- 标签列表  结束-->
                            <br/>
                            <div class="btn-group" role="group" aria-label="点个赞呗">
                                <a role="button" class="btn btn-info action-cmd action-check like-topic" href="javascript:;" data-acctip="false" data-handler="${BASE}/topic/like" data-function="updateLikeCounter" data-check-handler="${BASE}/topic/like/check">
                                    <i class="ico-sm mdi mdi-thumb-up" aria-hidden="true"></i> 赞
                                </a>
                                <button type="button" id="stats-likes" class="btn btn-light likeCounter" disabled="disabled">${topic.stats.likes}</button>
                            </div>
                            &nbsp;&nbsp;
                            <div class="btn-group" role="group" aria-label="喜欢都收藏呗">
                                <a role="button" class="btn btn-danger action-cmd action-check favorite-topic" href="javascript:;" data-acctip="false" data-handler="${BASE}/topic/favorite" data-function="updateFavoriteCounter" data-check-handler="${BASE}/topic/favorite/check">
                                    <i class="ico-sm mdi mdi-favorite" aria-hidden="true"></i> 收藏
                                </a>
                                <button type="button" id="stats-favorites" class="btn btn-light favoriteCounter" disabled="disabled">${topic.stats.favorites}</button>
                            </div>
                        </div>
                        <!-- 会员签名 -->
                        <div class="posts-member-signature member-${topic.memberId}-signature"><forum:out value="${topic.content.member.signature}" defValue="尚未填写个性签名"/></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- 相关话题 开始-->
        <div class="topic_posts">
            <div class="relate_topic" data-result="loader.json" data-empty-show="false" data-template-function="buildRelateSectionTopic" data-handler="${BASE}/topic/relate.json">
               <p class="no-record-tip"><i class="ico mdi mdi-spinner" aria-hidden="true"></i> 看成败人生豪迈, 只不过是从头再来</p>
            </div>
        </div>
        <!-- 相关话题 结束-->
        </div>
        <!-- 回复区段结束 -->
        <!-- 底部区功能区开始 -->
        <div class="row">
            <div class="col-md-6">
                <c:choose>
                <c:when test="${topic.status == 'LOCKED'}">
                <a href="javascript:;" class="btn btn-secondary disabled" role="button"><i class="ico-sm mdi mdi-lock" aria-hidden="true"></i> 锁定</a>
                </c:when>
                <c:when test="${topic.normal}">
                <a href="${BASE}/posts/create" class="btn btn-primary" role="button">回复 <i class="ico-sm mdi mdi-mail-reply"></i></a>
                </c:when>
                <c:otherwise>
                <a href="javascript:;" class="btn btn-secondary disabled" role="button"><i class="ico-sm mdi mdi-block"></i> 回复</a>
                </c:otherwise>
                </c:choose>
                <!-- 话题工具开始 -->
                <div class="btn-group" data-result="click.jsonp" data-handler="${BASE}/topic/tool.jsonp" data-function="buildMenuList" data-selector=".topic-menu-list">
                    <button type="button" class="btn  btn-secondary"><i class="ico-sm mdi mdi-wrench"></i> &nbsp;工具</button>
                    <button type="button" class="btn  btn-secondary dropdown-toggle-split caret-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu topic-menu-list"></div>
                </div>
                <!-- 分享 -->
                <a href="${BASE}/topic/poster" class="btn btn-success bds_more" data-cmd="more" role="button"><i class="ico-sm mdi mdi-link" aria-hidden="true"></i> 分享</a>
                <!-- 话题工具结束 -->
            </div>
            <div class="col-md-6">
                <!-- 分页开始 -->
                <div class="frinner page_navigation" data-handler="${BASE}/topic/replier.json" data-query="author:${topic.memberId},filter:${filterAuthor}" data-function="drawReplier" data-deferred="gatherMood"></div>
                <!-- 分页结束 -->
            </div>
        </div>
        <!-- 底部区功能区结束 -->
        <!-- 底部导航开始 -->
        <div class="row mb-2 mt-3">
            <p class="col-md-10 jumpbox-return"><a href="${BASE}/board/${topic.board.connect}.xhtml" accesskey="r"><i class="ico mdi mdi-arrow-left" aria-hidden="true"></i> 返回话题列表</a></p>
            <!-- board navigate start-->
            <div class="col-md-2">
                <div class="board-navigator-box" data-handler="${BASE}/loader/board/select.jsonp" data-function="buildBoardNavigateSelect">
                    <p class="div-select board-navigator-select" role="button">选择跳转的版块</p>
                </div>
            </div>
            <!-- board navigate end -->
        </div>
        <!-- 底部导航结束 -->
        <c:if test="${topic.normal}">
        <!-- 快速回复开始 -->
        <div class="post bg2 load_content_section" data-handler="${BASE}/posts/reply/form" data-function="buildRichEditor" data-error-function="buildEditorMessage" id="quick_reply_posts">
            <div class="ld ld-ring ld-spin"></div>
        </div>
        <!-- 快速回复结束 -->
        </c:if>
    </div>
    <%@ include file = "../include/footer.jsp" %>
    <section id="sideToolPlug" data-tool-plug="publish,poster"></section>
  </body></html>