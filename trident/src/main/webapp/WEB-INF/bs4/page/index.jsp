<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>找回帐号密码:第一步</title>
  </head>
  <body>
    <!-- form begin-->
    <div class="container-fluid no-padding">
        <div class="container w480 well main-color px-4 py-3 mt-5">
            <c:if test="${not empty errors}">
            <div class="alert alert-danger" role="alert">
                <strong>oOps!</strong> ${errors}
            </div>
            </c:if>
            <form action="${BASE}/lostPswd" method="post" class="theme-form" data-submit="once">
                <div class="form-group">
                    <label for="email">邮箱</label>
                    <input type="email" class="form-control" name="email" placeholder="请输入帐号绑定的邮箱地址" value="${form.email}" required="required" tabindex="1"/>
                </div>
                <div class="form-group">
                    <label for="names">帐号</label>
                    <input type="text" class="form-control" name="names" placeholder="请输入登录用的帐号" value="${form.names}" required="required" tabindex="2"/>
                </div>
                <input type="hidden" name="token" value="${form.token}" />
                <button type="submit" class="btn btn-primary" tabindex="3">下一步</button>
            </form>
        </div>
    </div>
  </body>
</html>