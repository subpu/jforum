<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>找回帐号密码:第二步</title>
  </head>
  <body>
    <!-- form begin-->
    <div class="container-fluid no-padding">
        <div class="container w480 well main-color px-4 py-3 mt-5">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Warning!</strong> 请在3分钟内完成操作,超时找回密码将以失败告终
            </div>
            <c:if test="${not empty errors}">
            <div class="alert alert-danger" role="alert">
                <strong>oOps!</strong> ${errors}
            </div>
            </c:if>
            <form action="${BASE}/rsetPswd" method="post" data-submit="once">
                <div class="form-group">
                    <label for="newpswd">新密码</label>
                    <input type="password" class="form-control" name="newpswd" placeholder="请输入新的登录密码" required="required" tabindex="1"/>
                </div>
                <div class="form-group">
                    <label for="confirmpswd">确认密码</label>
                    <input type="password" class="form-control" name="confirmpswd" placeholder="请重新确认新的密码" required="required" tabindex="2"/>
                </div>
                <input type="hidden" name="token" value="${form.token}" />
                <input type="hidden" name="record" value="${form.record}" />
                <input type="hidden" name="disi" value="${form.disi}" />
                <button type="submit" class="btn btn-primary" tabindex="3">下一步</button>
            </form>
        </div>
    </div>
  </body>
</html>