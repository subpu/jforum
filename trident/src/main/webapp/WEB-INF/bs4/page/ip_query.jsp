<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>IPv4查询</title>
  </head>
  <body>
    <!-- form begin-->
    <div class="container-fluid no-padding">
        <div class="container w480 jumbotron" style="margin-top:50px;background-color:#fff;">
            <form action="${BASE}/ip" method="get">
                <div class="form-group">
                    <label for="newpswd">IPv4地址</label>
                    <input type="text" class="form-control" name="addr" placeholder="请输入IPv4格式的地址" required="required" value="${param.addr}"/>
                    <span class="help-block">例: 27.194.66.247; 127开头的会返回星号(*)</span>
                </div>
                <div class="form-group">
                    <label for="confirmpswd">结果</label>
                    <textarea class="form-control" rows="3">${result}</textarea>
                </div>
                <button type="submit" class="btn btn-primary">查询</button>
                <small  class="text-muted float-right d-inline">查询使用开源的库: <a href="https://gitee.com/lionsoul/ip2region" target="_blank">ip2region</a></small>
            </form>
        </div>
    </div>
  </body>
</html>