<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="refresh" content="5; url=${DOMAIN}/member/login" />
    <title>找回帐号密码:第三步</title>
    <style type="text/css">
    body{margin:0;padding:0;font: 13px/20px 'Microsoft YaHei', 微软雅黑, helvetica, arial, verdana, tahoma, sans-serif;}
    p{text-align:center;margin-top:15%;height:150px;line-height:150px;font-size:2.3em;font-weight:900;background-color:#dff0d8; color:#3c763d;}
    </style>
  </head>
  <body>
      <p>密码重置成功;系统将自动跳转至<a href="${DOMAIN}/member/login">登录</a>页面</p>
  </body>
</html>