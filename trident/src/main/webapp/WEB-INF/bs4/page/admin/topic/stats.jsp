<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>话题统计</title>
    <style>
    .user-timeline-compact::before {
        background-color: #4285f4;
    }
    .user-timeline-compact > li::before {
        border: 1px solid #4285f4;
    }
    .user-timeline:after {
        background-color: #4285f4;
    }
    </style>
  </head>
  <body>
      <div class="row">
          <div class="col-md-12">
              <div class="card">
                  <div class="card-header pb-3">最近七日发布的话题统计</div>
                  <div class="card-body" data-raw='${rs}' id="sevenDayChart" style="height: 350px;">
                  </div>
              </div>
          </div>
      </div>
      <!-- 版块占比|状态占比|分类占比 -->
      <div class="row">
          <div class="col-12 col-lg-4">
              <div class="card">
                  <div class="card-header pb-3">版块话题占比</div>
                  <div class="card-body statsDonutChart" style="height: 250px;text-align:center" data-handler="${ADMIN}/topic/stats/donut/board">
                   
                  </div>
              </div>
          </div>
          <div class="col-12 col-lg-4">
              <div class="card">
                  <div class="card-header pb-3">话题状态占比</div>
                  <div class="card-body statsDonutChart" style="height: 250px;text-align:center" data-handler="${ADMIN}/topic/stats/donut/status">
                  </div>
              </div>
          </div>
          <div class="col-12 col-lg-4">
              <div class="card">
                  <div class="card-header pb-3">话题分类占比</div>
                  <div class="card-body statsDonutChart" style="height: 250px;text-align:center" data-handler="${ADMIN}/topic/stats/donut/category">
                  </div>
              </div>
          </div>
      </div>
      <!-- 最近话题|最近回复 -->
      <div class="row">
          <div class="col-12 col-lg-6">
              <div class="card">
                  <div class="card-header">
                      <div class="title">最近发布的话题</div>
                  </div>
                  <div class="card-body stats-data-load" data-handler="${ADMIN}/topic/stats/recent/json" data-function="drawTopicResult">
                  </div>
              </div>
          </div>
          <div class="col-12 col-lg-6">
              <div class="card">
                  <div class="card-header">
                      <div class="title">最近回复的话题</div>
                  </div>
                  <div class="card-body stats-data-load" data-handler="${ADMIN}/topic/stats/reply/json" data-function="drawTopicResult">
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>