<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>最近发布的话题</title>
    <style>.card-header .tools{font-size:0.8em}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">最近发布的话题</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!--Responsive table-->
              <div class="col-sm-12">
                  <div class="card card-table">
                      <div class="card-header">最近发布的话题列表
                          <div class="tools">
                              <a href="${ADMIN}/topic/publish"><i class="icon mdi mdi-plus"></i>新增</a>
                          </div>
                      </div>
                      <div class="card-body">
                          <div class="table-responsive noSwipe">
                              <table class="table table-striped table-hover">
                                  <thead>
                                      <tr>
                                          <th style="width:5%;">
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
                                              </label>
                                          </th>
                                          <th style="width:5%;">类别</th>
                                          <th style="width:10%;">版块</th>
                                          <th style="width:30%;">话题主题</th>
                                          <th style="width:10%;">作者</th>
                                          <th style="width:10%;">创建日期</th>
                                          <th style="width:10%;">状态</th>
                                          <th class="actions"></th>
                                      </tr>
                                  </thead>
                                  <tbody class="load-board-box" data-board-source="${ADMIN}/board/list.json">
                                      <!-- foreach tr -->
                                      <c:forEach items="${rs}" var="topic">
                                      <tr>
                                          <td>
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" name="id" class="custom-control-input" value="${topic.id}"><span class="custom-control-label"></span>
                                              </label>
                                          </td>
                                          <td><forum:outlet value="${topic.topicCategoryName}" optional="普通"/></td>
                                          <td class="loadBoard board-${topic.boardId}" data-board="${topic.boardId}">loading</td>
                                          <td><a href="${ADMIN}/topic/${topic.id}.xhtml" target="_blank">${topic.title}</a></td>
                                          <td><a href="${ADMIN}/member/${topic.memberId}.xhtml">${topic.memberNickname}</a></td>
                                          <td><forum:date value="${topic.entryDateTime}"/></td>
                                          <td>${topic.status.title}</td>
                                          <td class="actions">
                                              <a class="btn btn-primary" role="button" href="${ADMIN}/topic/edit?id=${topic.id}">编辑</a>
                                              <c:if test="${'LOCKED' eq  topic.status}">
                                              <a class="btn btn-secondary action-cmd" role="button" href="javascript:;" data-handler="${ADMIN}/topic/lock/remove" data-query="id:${topic.id}">解锁</a>
                                              </c:if>
                                              <div class="btn-group">
                                                  <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle">操作 <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                                                  <div role="menu" class="dropdown-menu">
                                                      <a href="javascript:;" class="dropdown-item action-cmd" data-handler="${ADMIN}/topic/lock" data-query="id:${topic.id}">锁定</a>
                                                      <a href="javascript:;" class="dropdown-item action-cmd" data-handler="${ADMIN}/topic/remove" data-query="id:${topic.id}">删除</a>
                                                      <a href="javascript:;" class="dropdown-item action-cmd" data-handler="${ADMIN}/topic/top" data-query="id:${topic.id}">置顶</a>
                                                      <div class="dropdown-divider"></div>
                                                      <a href="${ADMIN}/topic/config?id=${topic.id}" class="dropdown-item">配置</a>
                                                      <a href="${ADMIN}/topic/posts/?topic=${topic.id}" class="dropdown-item">回复</a>
                                                      <a href="${ADMIN}/topic/album/picture?topic=${topic.id}" class="dropdown-item">像册</a>
                                                      <a href="${ADMIN}/topic/tag/collection?topic=${topic.id}" class="dropdown-item">标签</a>
                                                      <a href="${ADMIN}/topic/action/?id=${topic.id}" class="dropdown-item">动作</a>
                                                  </div>
                                              </div>
                                          </td>
                                      </tr>
                                      </c:forEach>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>