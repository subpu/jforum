<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>轮播图</title>
    <style>.h186{height:186px;background-color:#868e96;margin:0}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">轮播图列表
              <small class="tools float-right">
                  <a href="${ADMIN}/topic/carousel/edit?id=${paramId}"><i class="icon mdi mdi-plus"></i>新增</a>
              </small>
          </h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!-- foreach card -->
              <c:forEach items="${rs}" var="topicCarousel">
              <div class="col-md-3 col-sm-6">
                  <div class="card">
                      <div class="card-header h186">&nbsp;</div>
                      <div class="card-body">
                          <h5 class="card-title">${topicCarousel.title}</h5>
                          <!-- <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6> -->
                          <p class="card-text">${topicCarousel.summary}</p>
                          <a class="btn btn-primary" role="button" href="${ADMIN}/topic/carousel/edit?id=${topicCarousel.id}">编辑</a>
                          <div class="btn-group">
                              <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle">操作 <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                              <div role="menu" class="dropdown-menu">
                                  <a href="javascript:;" class="dropdown-item action-cmd" data-handler="${ADMIN}/topic/carousel/remove" data-query="id:${topicCarousel.id}">删除</a>
                                  <a href="${ADMIN}/topic/carousel/slide?carousel=${topicCarousel.id}" class="dropdown-item">幻灯片</a>
                                  <a href="${ADMIN}/topic/carousel/bind/result?carousel=${topicCarousel.id}" class="dropdown-item">版块关联记录</a>
                                  <div class="dropdown-divider"></div>
                                  <a href="${ADMIN}/topic/carousel/slide/edit?carousel=${topicCarousel.id}" class="dropdown-item">添加幻灯片</a>
                                  <a href="${ADMIN}/topic/carousel/bind?carousel=${topicCarousel.id}" class="dropdown-item">关联版块</a>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              </c:forEach>
          </div>
      </div>
  </body>
</html>