<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>话题类型</title>
    <style>.card-header .tools{font-size:0.8em}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">话题类型</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!--Responsive table-->
              <div class="col-sm-12">
                  <div class="card card-table">
                      <div class="card-header">所有话题类型列表
                          <div class="tools">
                              <a href="${ADMIN}/topic/category/edit"><i class="icon mdi mdi-plus"></i>新增</a>
                          </div>
                      </div>
                      <div class="card-body">
                          <div class="table-responsive noSwipe">
                              <table class="table table-striped table-hover">
                                  <thead>
                                      <tr>
                                          <th style="width:5%;">
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
                                              </label>
                                          </th>
                                          <th style="width:20%;">名称</th>
                                          <th style="width:20%;">参数值</th>
                                          <th style="width:10%;">状态</th>
                                          <th class="actions"></th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <!-- foreach tr -->
                                      <c:forEach items="${rs}" var="topicCategory">
                                      <tr>
                                          <td>
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" name="id" class="custom-control-input" value="${topicCategory.id}"><span class="custom-control-label"></span>
                                              </label>
                                          </td>
                                          <td>${topicCategory.names}</td>
                                          <td>${topicCategory.value}</td>
                                          <td><forum:print value="${topicCategory.status}" trueTitle="可用" falseTitle="删除"/></td>
                                          <td>
                                              <a class="btn btn-primary" role="button" href="${ADMIN}/topic/category/edit?id=${topicCategory.id}" class="icon">编辑</a>
                                              <a class="btn btn-secondary action-cmd" role="button" href="javascript:;" data-handler="${ADMIN}/topic/category/remove" data-query="id:${topicCategory.id}">删除</a>
                                          </td>
                                      </tr>
                                      </c:forEach>
                                  </tbody>
                              </table>
                          </div>
                          <div class="row be-datatable-footer">
                              <div class="col-sm-5">
                                  <forum:pagination_collect total="${pageData.records}" page="${pageData.page}" pageSize="${pageData.pageSize}">
                                  <div class="dataTables_info" id="table1_info" role="status" aria-live="polite">Showing ${tagPageMinId} to ${tagPageMaxId} of ${tagPageTotal} entries</div>
                                  </forum:pagination_collect>
                              </div>
                              <div class="col-sm-7">
                                  <div class="dataTables_paginate paging_simple_numbers" id="table1_paginate">
                                      <!-- 分页代码 -->
                                      <nav>
                                          <ul class="pagination">
                                          <forum:pagination size="${pageData.pageSize}" total="${pageData.records}" url="${pageData.pageURL}">
                                          <c:set var="currentPageNumber">${pageData.page}</c:set>
                                              <c:choose>
                                              <c:when test="${currentPageNumber eq anchor}">
                                                  <li class="page-item active"><a href="javascript:;" class="page-link">${anchor}</a></li>
                                              </c:when>
                                              <c:otherwise>
                                                  <li class="page-item"><a href="${uri}" class="page-link">${anchor}</a></li>
                                              </c:otherwise>
                                              </c:choose>
                                          </forum:pagination>
                                          </ul>
                                     </nav>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>