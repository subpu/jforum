<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>最近注册的会员</title>
    <style>.card-header .tools{font-size:0.8em}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">最近注册的会员</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!--Responsive table-->
              <div class="col-sm-12">
                  <div class="card card-table">
                      <div class="card-header">会员列表
                          <div class="tools">
                              <a href="${ADMIN}/member/create" title="新增社区经理" data-toggle="tooltip" data-placement="left"><i class="icon mdi mdi-plus"></i>新增</a>
                          </div>
                      </div>
                      <div class="card-body">
                          <div class="table-responsive noSwipe">
                              <table class="table table-striped table-hover">
                                  <thead>
                                      <tr>
                                          <th style="width:5%;">
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
                                              </label>
                                          </th>
                                          <th style="width:10%;">登录帐号</th>
                                          <th style="width:10%;">昵称</th>
                                          <th style="width:10%;">邀请码</th>
                                          
                                          <th style="width:10%;">组</th>
                                          <th style="width:10%;">角色</th>
                                          <th style="width:5%;">第三方</th>
                                          <th style="width:10%;">状态</th>
                                          <th style="width:10%">注册日期</th>
                                          <th class="actions"></th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <!-- foreach item -->
                                      <c:forEach items="${rs}" var="member">
                                      <tr>
                                          <td>
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" name="id" class="custom-control-input" value="${member.id}"><span class="custom-control-label"></span>
                                              </label>
                                          </td>
                                          <td>${member.names}</td>
                                          <td><a href="${ADMIN}/member/${member.id}.xhtml">${member.nickname}</a></td>
                                          <td><forum:outlet value="${member.inviteCode}" optional="-"/></td>
                                          
                                          <td>${member.mgroup.title}</td>
                                          <td>${member.mrole.title}</td>
                                          <td><forum:outlet value="${member.tdparty}" optional="-"/></td>
                                          
                                          <td>${member.status.title}</td>
                                          <td><forum:date value="${member.registeDateTime}"/></td>
                                          <td class="actions">
                                              <a class="btn btn-primary" role="button" href="${ADMIN}/member/passport?id=${member.id}">重置密码</a>
                                              <div class="btn-group">
                                                  <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle">操作 <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                                                  <div role="menu" class="dropdown-menu">
                                                      <a href="${ADMIN}/member/contact?member=${member.id}" class="dropdown-item">联系方式</a>
                                                      <a href="${ADMIN}/member/realauth?member=${member.id}" class="dropdown-item">实名认证</a>
                                                      <a href="${ADMIN}/member/social?member=${member.id}" class="dropdown-item">社交信息</a>
                                                      <div class="dropdown-divider"></div>
                                                      <a href="${ADMIN}/member/action/?id=${member.id}" class="dropdown-item">动作</a>
                                                  </div>
                                              </div>
                                          </td>
                                      </tr>
                                      </c:forEach>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>