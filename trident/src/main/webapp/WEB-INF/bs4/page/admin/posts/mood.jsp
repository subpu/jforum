<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>话题回复喜好记录</title>
    <style>.h186{height:186px;background-color:#868e96;margin:0}.border-3{border-style:solid;border-width: 3px;}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">话题回复喜好记录</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/topic/${posts.topicId}.xhtml">话题</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/topic/posts/?topic=${posts.topicId}">话题回复</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!-- card -->
              <c:forEach items="${rs}" var="postsMoodRecords" varStatus="loop">
              <div class="col-md-3 col-sm-6">
                  <div class="card mb-3">
                      <div class="row no-gutters">
                          <div class="col-md-4 h186" style="text-align:center">
                              <img src="${FRONT}/member/avatar/${postsMoodRecords.memberId}.png" class="rounded-circle mt-2 member-avatar"/>
                          </div>
                          <div class="col-md-8">
                              <div class="card-body">
                                  <h5 class="card-title">${postsMoodRecords.memberNickname}</h5>
                                  <p>
                                     <c:if test="${postsMoodRecords.liked}"><span class="mdi mdi-thumb-up"></span> 支持</c:if>
                                     <c:if test="${not postsMoodRecords.liked}"><span class="mdi mdi-thumb-down"></span> 不赞同</c:if>
                                  </p>
                              </div>
                              <div class="card-footer">
                                  <small class="text-muted">操作日期: <forum:date value="${postsMoodRecords.entryDateTime}"/></small>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              </c:forEach>
          </div>
          <div class="row be-datatable-footer">
              <div class="col-sm-5">
                  <forum:pagination_collect total="${pageData.records}" page="${pageData.page}" pageSize="${pageData.pageSize}">
                  <div class="dataTables_info" id="table1_info" role="status" aria-live="polite">Showing ${tagPageMinId} to ${tagPageMaxId} of ${tagPageTotal} entries</div>
                  </forum:pagination_collect>
              </div>
              <div class="col-sm-7">
                  <div class="dataTables_paginate paging_simple_numbers" id="table1_paginate">
                      <!-- 分页代码 -->
                      <nav>
                          <ul class="pagination">
                              <forum:pagination size="${pageData.pageSize}" total="${pageData.records}" url="${pageData.pageURL}">
                                <c:set var="currentPageNumber">${pageData.page}</c:set>
                                <c:choose>
                                  <c:when test="${currentPageNumber eq anchor}">
                                  <li class="page-item active"><a href="javascript:;" class="page-link">${anchor}</a></li>
                                  </c:when>
                                  <c:otherwise>
                                  <li class="page-item"><a href="${uri}" class="page-link">${anchor}</a></li>
                                  </c:otherwise>
                                </c:choose>
                              </forum:pagination>
                          </ul>
                      </nav>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>