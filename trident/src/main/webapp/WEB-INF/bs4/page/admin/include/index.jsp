<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
            <ul class="nav navbar-nav float-right be-user-nav">
              <li class="nav-item dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><img src="${FRONT}/member/avatar/${fxost.mid}.png" alt="Avatar"><span class="user-name">${fxost.names}</span></a>
                <c:if test="${fxost.online}">
                <div role="menu" class="dropdown-menu">
                  <div class="user-info">
                    <div class="user-name">${fxost.names}</div>
                    <div class="user-position online">在线</div>
                  </div>
                  <a href="pages-profile.html" class="dropdown-item"><span class="icon mdi mdi-face"></span> 帐户设置</a>
                  <a href="#" class="dropdown-item"><span class="icon mdi mdi-settings"></span> 安全设置</a>
                  <a href="${ADMIN}/offline.xhtml?trace=msa" class="dropdown-item"><span class="icon mdi mdi-power"></span> 注销登录</a>
                </div>
                </c:if>
              </li>
            </ul>
            <ul class="nav navbar-nav float-right be-icons-nav">
              <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-expanded="false"><span class="icon mdi mdi-notifications"></span><span class="indicator"></span></a>
                <ul class="dropdown-menu be-notifications">
                  <li>
                    <div class="title">Notifications<span class="badge badge-pill">3</span></div>
                    <div class="list">
                      <div class="be-scroller-notifications ps">
                        <div class="content">
                          <ul>
                            <!-- foreach it
                            <li class="notification notification-unread"><a href="#">
                                <div class="image"><img src="assets/img/avatar2.png" alt="Avatar"></div>
                                <div class="notification-info">
                                  <div class="text"><span class="user-name">Jessica Caruso</span> accepted your invitation to join the team.</div><span class="date">2 min ago</span>
                                </div></a></li>-->
                          </ul>
                        </div>
                      <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
                    </div>
                    <div class="footer"> <a href="#">View all notifications</a></div>
                  </li>
                </ul>
              </li>
            </ul>