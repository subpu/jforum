<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>${form.actionTitle}会员等级</title>
    <style>.fileinput-upload-button,.btn-file{line-height:45px;}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">${form.actionTitle}会员等级</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/member/">会员</a></li>
              <li class="breadcrumb-item active">表单</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <div class="col-md-12">
                  <div class="card card-border-color card-border-color-primary">
                      <div class="card-header card-header-divider">${form.actionTitle}会员等级</div>
                      <div class="card-body">
                          <c:if test="${not empty errors}">
                          <div role="alert" class="alert alert-danger alert-dismissible">
                              <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
                              <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                              <div class="message"><strong>oOps!</strong> ${errors} </div>
                          </div>
                          </c:if>
                          <form method="post" action="${ADMIN}/member/level/edit" enctype="multipart/form-data">
                          <div class="form-group row">
                              <label for="names" class="col-12 col-sm-3 col-form-label text-sm-right">名称</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="names" type="text" class="form-control" value="${form.names}" required="required" tabindex="1"/>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="imageAddr" class="col-12 col-sm-3 col-form-label text-sm-right">图标</label>
                              <div class="col-12 col-sm-8 col-lg-6" id="upload-section">
                                  <input name="file" type="file" class="bootCoverImage" accept=".gif,.jpeg,.png,.jpg" data-show-preview="true" data-upfilepathURL="/upload/fileinput" data-bind="<forum:image path='${form.imageAddr}'/>"/>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="minScore" class="col-12 col-sm-3 col-form-label text-sm-right">下限积分</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="minScore" type="number" class="form-control" value="${form.minScore}" required="required" tabindex="3"/>
                                  <span class="form-text text-muted">达到该等级需要的最小积分</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="score" class="col-12 col-sm-3 col-form-label text-sm-right">上限积分</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="score" type="number" class="form-control" value="${form.score}" required="required" tabindex="4"/>
                                  <span class="form-text text-muted">该等级需要的最大积分,下一级应是该等级上限积分+1</span>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="status" class="col-12 col-sm-3 col-form-label text-sm-right">状态</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="status"<c:if test="${'1' eq form.status}"> checked="checked"</c:if> class="custom-control-input" value="1"><span class="custom-control-label">可用</span>
                                  </label>
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="status"<c:if test="${'0' eq form.status}"> checked="checked"</c:if> class="custom-control-input" value="0"><span class="custom-control-label">禁用</span>
                                  </label>
                              </div>
                          </div>
                          <div class="form-group row text-right">
                              <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
                                  <input type="hidden" name="record" value="${form.record}" />
                                  <input type="hidden" name="token" value="${form.token}" />
                                  <input type="hidden" name="fileurl" value="" />
                                  <button type="submit" class="btn btn-space btn-primary" tabindex="5">提交</button>
                                  <button class="btn btn-space btn-secondary">取消</button>
                              </div>
                          </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>