<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>${form.actionTitle}积分规则</title>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">${form.actionTitle}积分规则</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/score/role/">积分规则</a></li>
              <li class="breadcrumb-item active">表单</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <c:if test="${not form.update && empty actionSet}">
          <!-- 新增时 && 可选动作为空 -->
          <div role="alert" class="alert alert-warning alert-dismissible">
              <div class="icon"><span class="mdi mdi-alert-triangle"></span></div>
              <div class="message">
                  <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><strong>警告:</strong> 所有内置允许的动作都完成了积分定义.
              </div>
          </div>
          </c:if>
          <div class="row">
              <div class="col-md-12">
                  <div class="card card-border-color card-border-color-primary">
                      <div class="card-header card-header-divider">${form.actionTitle}积分规则</div>
                      <div class="card-body">
                          <c:if test="${not empty errors}">
                          <div role="alert" class="alert alert-danger alert-dismissible">
                              <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
                              <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                              <div class="message"><strong>oOps!</strong> ${errors} </div>
                          </div>
                          </c:if>
                          <form method="post" action="${ADMIN}/score/role/edit">
                          <div class="form-group row">
                              <label for="action" class="col-12 col-sm-3 col-form-label text-sm-right">操作</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <select class="select2 form-control" name="action" tabindex="1">
                                      <c:forEach items="${actionSet}" var="as">
                                      <option value="${as.key}"<c:if test="${form.action eq as.key}"> selected="selected"</c:if>>${as.value}</option>
                                      </c:forEach>
                                      <c:if test="${empty actionSet}">
                                      <option value="-1">暂未有动作定义</option>
                                      </c:if>
                                  </select>
                                  <span class="form-text text-muted">现行只有6类操作纳入积分计算范围</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="degree" class="col-12 col-sm-3 col-form-label text-sm-right">频次</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="degree" type="number" class="form-control" value="${form.degree}" required="required" tabindex="2"/>
                                  <span class="form-text text-muted">多少次算作一个积分单元</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="level" class="col-12 col-sm-3 col-form-label text-sm-right">等级</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="level" type="number" class="form-control" value="${form.level}" required="required" tabindex="3"/>
                                  <span class="form-text text-muted">暂时不起任何作用,填写的值对积分计算起作用</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="score" class="col-12 col-sm-3 col-form-label text-sm-right">分值</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="score" type="number" class="form-control" value="${form.score}" required="required" tabindex="4"/>
                                  <span class="form-text text-muted">现行计算公式为: 操作总次数 / 频次 * 分值; 分值允许负值,小数位后保留两位</span>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="status" class="col-12 col-sm-3 col-form-label text-sm-right">状态</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="status"<c:if test="${'1' eq form.status}"> checked="checked"</c:if> class="custom-control-input" value="1"><span class="custom-control-label">可用</span>
                                  </label>
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="status"<c:if test="${'0' eq form.status}"> checked="checked"</c:if> class="custom-control-input" value="0"><span class="custom-control-label">禁用</span>
                                  </label>
                              </div>
                          </div>
                          <div class="form-group row text-right">
                              <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
                                  <input type="hidden" name="record" value="${form.record}" />
                                  <input type="hidden" name="token" value="${form.token}" />
                                  <button type="submit" class="btn btn-space btn-primary" tabindex="5">提交</button>
                                  <button class="btn btn-space btn-secondary">取消</button>
                              </div>
                          </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>