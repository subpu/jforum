<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>版块绑定轮播图</title>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">版块绑定轮播图</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/topic/carousel/">轮播图</a></li>
              <li class="breadcrumb-item active">表单</li>
              
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <div class="col-md-12">
                  <div class="card card-border-color card-border-color-primary">
                      <div class="card-header card-header-divider">版块绑定轮播图</div>
                      <div class="card-body">
                          <c:if test="${not empty errors}">
                          <div role="alert" class="alert alert-danger alert-dismissible">
                              <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
                              <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                              <div class="message"><strong>oOps!</strong> ${errors} </div>
                          </div>
                          </c:if>
                          <form method="post" action="${ADMIN}/topic/carousel/bind">
                          <div class="form-group row">
                              <label for="volumesId" class="col-12 col-sm-3 col-form-label text-sm-right">版块组(卷)</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <select class="apo_select apo_cascade_parent_select form-control" name="volumesId" tabindex="1" required="required" data-lazy="board_lazy_select" data-active="${form.volumesId}" data-handler="${ADMIN}/board/group/json">
                                      <option value="">选择版块组(卷)</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="boardId" class="col-12 col-sm-3 col-form-label text-sm-right">版块</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <select id="board_lazy_select" class="apo_lazy_select form-control" name="boardId" tabindex="2" data-parent="${form.volumesId}" data-active="${form.boardId}" data-handler="${ADMIN}/board/lazy.json">
                                      <option value="">选择版块</option>
                                  </select>
                                  <span class="form-text text-muted">可选项,不选默认绑定到版块组(卷)</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label class="col-12 col-sm-3 col-form-label text-sm-right">轮播图</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input type="text" class="form-control" value="${carousel.title}" readonly="readonly" />
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="limit" class="col-12 col-sm-3 col-form-label text-sm-right">有效期</label>
                              <div class="col-sm-4 col-lg-3 mb-3 mb-sm-0">
                                  <input name="limit" type="text" class="form-control" value="${form.limit}" required="required" tabindex="3"/>
                              </div>
                              <div class="col-sm-4 col-lg-3">
                                  <select class="apo_select" name="unit" tabindex="4" data-active="3" data-handler="${ADMIN}/topic/carousel/bind/unit/json" data-active="${form.unit}" style="width:300px;">
                                      <option value="0">选择日期单位</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group row text-right">
                              <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
                                  <input type="hidden" name="carouselId" value="${form.carouselId}" />
                                  <button type="submit" class="btn btn-space btn-primary" tabindex="4">提交</button>
                                  <button class="btn btn-space btn-secondary">取消</button>
                              </div>
                          </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>