<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>会员惩罚记录</title>
    <style>.card-header .tools{font-size:0.8em}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">会员惩罚记录</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!--Responsive table-->
              <div class="col-sm-12">
                  <div class="card card-table">
                      <div class="card-header">会员惩罚记录
                          <div class="tools">
                              <a href="${ADMIN}/member/penalize/create"><i class="icon mdi mdi-plus"></i>新增</a>
                          </div>
                      </div>
                      <div class="card-body">
                          <div class="table-responsive noSwipe">
                              <table class="table table-striped table-hover">
                                  <thead>
                                      <tr>
                                          <th style="width:5%;">
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
                                              </label>
                                          </th>
                                          <th style="width:25%;">会员</th>
                                          <th style="width:10%;">惩罚</th>
                                          <th style="width:10%;">开始日期</th>
                                          
                                          <th style="width:10%;">时长</th>
                                          <th style="width:10%;">结束日期</th>
                                          <th style="width:10%;">状态</th>
                                          <th style="width:10%">创建者</th>
                                          <th class="actions"></th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <!-- foreach tr -->
                                      <c:forEach items="${rs}" var="penalize">
                                      <tr>
                                          <td>
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" name="id" class="custom-control-input" value="${penalize.id}"><span class="custom-control-label"></span>
                                              </label>
                                          </td>
                                          <td><a href="${ADMIN}/member/${penalize.memberId}.xhtml">${penalize.memberNickname}</a></td>
                                          <td>${penalize.arrive.title}</td>
                                          <td><forum:date value="${penalize.rebornDateTime}"/></td>
                                          
                                          <td>${penalize.rebornumber} ${penalize.rebornUnit.title}</td>
                                          <td><forum:date value="${penalize.rebirthDateTime}"/></td>
                                          <td><forum:print value="${penalize.status}" trueTitle="进行中" falseTitle="结束"/></td>
                                          <td>${penalize.judgeNickname}</td>
                                          <td class="actions">
                                              <a class="btn btn-primary action-cmd" role="button" href="javascript:;" data-handler="${ADMIN}/member/penalize/expire" data-query="id:${penalize.id}">手动结束</a>
                                          </td>
                                      </tr>
                                      </c:forEach>
                                  </tbody>
                              </table>
                          </div>
                          <div class="row be-datatable-footer">
                              <div class="col-sm-5">
                                  <forum:pagination_collect total="${pageData.records}" page="${pageData.page}" pageSize="${pageData.pageSize}">
                                  <div class="dataTables_info" id="table1_info" role="status" aria-live="polite">Showing ${tagPageMinId} to ${tagPageMaxId} of ${tagPageTotal} entries</div>
                                  </forum:pagination_collect>
                              </div>
                              <div class="col-sm-7">
                                  <div class="dataTables_paginate paging_simple_numbers" id="table1_paginate">
                                      <!-- 分页代码 -->
                                      <nav>
                                          <ul class="pagination">
                                          <forum:pagination size="${pageData.pageSize}" total="${pageData.records}" url="${pageData.pageURL}">
                                          <c:set var="currentPageNumber">${pageData.page}</c:set>
                                              <c:choose>
                                              <c:when test="${currentPageNumber eq anchor}">
                                                  <li class="page-item active"><a href="javascript:;" class="page-link">${anchor}</a></li>
                                              </c:when>
                                              <c:otherwise>
                                                  <li class="page-item"><a href="${uri}" class="page-link">${anchor}</a></li>
                                              </c:otherwise>
                                              </c:choose>
                                          </forum:pagination>
                                          </ul>
                                     </nav>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>