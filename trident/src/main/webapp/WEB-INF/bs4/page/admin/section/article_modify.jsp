<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>编辑子栏目文章</title>
    <style>
    .label-info {background-color: #4285f4;}
    .label {
        display: inline;
        padding: .2em .6em .3em;
        line-height: 1;
        color: #fff;
        text-align: center;
        white-space: nowrap;
        vertical-align: baseline;
        border-radius: .25em;
    }
    .nav-tabs{border-style:none}
    #default li{width:50px;height:50px;text-align:center;line-height:50px;}
    .tools {position: absolute;right: 1px;top: 1px;width: 25px;line-height: 25px;text-align: center;background-color: #f1f1f1;}
    .list-inline-item:hover{background-color:#666;cursor:pointer}
    </style>
  </head>
  <body>
      <div class="email-head">
          <c:if test="${not empty errors}">
          <div class="email-head-title">${errors}<span class="icon mdi mdi-alert-polygon"></span></div>
          </c:if>
          <c:if test="${empty errors}">
          <div class="email-head-title">编辑文章<span class="icon mdi mdi-edit"></span></div>
          </c:if>
      </div>
      <form action="${ADMIN}/section/article/modify" method="post">
      <div class="email-compose-fields">
          <div class="to cc">
              <div class="form-group row pt-2">
                  <label class="col-md-1 control-label">栏目</label>
                  <div class="col-md-11 row">
                      <div class="col-md-3" style="padding-left:0">
                          <select class="apo_select apo_cascade_parent_select form-control" name="volumes" data-lazy="board_lazy_select" data-active="${form.volumes}" data-handler="${ADMIN}/section/list/json" required="required" tabindex="1">
                              <option value="">选择栏目</option>
                          </select>
                      </div>
                      <div class="col-md-9">
                          <select id="board_lazy_select" class="apo_lazy_select form-control" name="board" data-parent="${form.volumes}" data-active="${form.board}" data-handler="${ADMIN}/section/term/all/json" required="required" tabindex="2">
                              <option value="">选择子栏目</option>
                          </select>
                      </div>
                  </div>
              </div>
          </div>
          <div class="subject">
              <div class="form-group row pt-2">
                  <label class="col-md-1 control-label">主题</label>
                  <div class="col-md-11 row">
                      <input name="title" type="text" class="form-control" value="${form.title}" required="required" tabindex="3" />
                  </div>
              </div>
          </div>
      </div>
      <div class="email editor">
            <div id="content-editor"><textarea id="content" name="content" rows="5" class="richeditor" data-query="height:450px,width:100%,upload:/upload/ckeditor"  required="required" tabindex="4">${form.content}</textarea></div>
            <div class="row">
                <div class="col-md-8 col-sm-9" style="margin-top: 15px;">&nbsp;</div>
                <div class="form-group col-md-4 col-sm-3">
                    <input type="hidden" name="record" value="${form.record}" />
                    <input type="hidden" name="token" value="${form.token}" />
                    <button type="submit" class="btn btn-primary btn-space" tabindex="5"><i class="icon s7-mail"></i> 提交</button>
                    <button type="button" class="btn btn-secondary btn-space"><i class="icon s7-close"></i> 取消</button>
                </div>
            </div>
      </div>
      </form>
  </body>
</html>