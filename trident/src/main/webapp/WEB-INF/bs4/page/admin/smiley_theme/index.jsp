<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>表情风格</title>
    <style>.h186{height:186px;background-color:#868e96;margin:0}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">表情风格列表
              <small class="tools float-right">
                  <a href="${ADMIN}/smiley/theme/edit"><i class="icon mdi mdi-plus"></i>新增</a>
              </small>
          </h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!-- foreach card -->
              <c:forEach items="${rs}" var="smileyTheme">
              <div class="col-md-3 col-sm-6">
                  <div class="card">
                      <div class="card-header h186">&nbsp;</div>
                      <div class="card-body">
                          <h5 class="card-title">${smileyTheme.title} / ${smileyTheme.label}</h5>
                          <h6 class="card-subtitle mb-2 text-muted"><forum:print value="${smileyTheme.status}" trueTitle="可用" falseTitle="禁用"/></h6> 
                          <p class="card-text">目录: ${smileyTheme.directNames}</p>
                          <a class="btn btn-primary" role="button" href="${ADMIN}/smiley/theme/edit?id=${smileyTheme.id}">编辑</a>
                          <a class="btn btn-secondary" role="button" href="${ADMIN}/smiley/pic/?theme=${smileyTheme.id}">图片</a>
                      </div>
                  </div>
              </div>
              </c:forEach>
          </div>
      </div>
  </body>
</html>