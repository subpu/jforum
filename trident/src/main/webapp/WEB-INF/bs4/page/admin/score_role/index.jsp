<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>积分规则</title>
    <style>.card-header .tools{font-size:0.8em}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">积分规则</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!--Responsive table-->
              <div class="col-sm-12">
                  <div class="card card-table">
                      <div class="card-header">积分规则列表
                          <div class="tools">
                              <a href="${ADMIN}/score/role/edit"><i class="icon mdi mdi-plus"></i>新增</a>
                          </div>
                      </div>
                      <div class="card-body">
                          <div class="table-responsive noSwipe">
                              <table class="table table-striped table-hover">
                                  <thead>
                                      <tr>
                                          <th style="width:5%;">
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
                                              </label>
                                          </th>
                                          <th style="width:20%;">动作名称</th>
                                          <th style="width:18%;">频次</th>
                                          <th style="width:20%;">等级</th>
                                          <th style="width:20%;">积分</th>
                                          <th style="width:10%;">状态</th>
                                          <th style="width:12%;"></th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <c:forEach items="${rs}" var="scoreRole">
                                      <tr>
                                          <td>
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" name="id" class="custom-control-input" value="${scoreRole.id}"><span class="custom-control-label"></span>
                                              </label>
                                          </td>
                                          <td>${scoreRole.action.title}</td>
                                          <td>${scoreRole.degree}</td>
                                          <td>${scoreRole.level}</td>
                                          <td>${scoreRole.score}</td>
                                          <td><forum:print value="${scoreRole.status}" trueTitle="可用" falseTitle="禁用"/></td>
                                          <td class="actions">
                                              <a class="btn btn-primary" role="button" href="${ADMIN}/score/role/edit?id=${scoreRole.id}">编辑</a>
                                          </td>
                                      </tr>
                                      </c:forEach>
                                  </tbody>
                              </table>
                          </div>
                          <!-- 分页区 -->
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>