<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>${form.actionTitle}版块设置</title>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">${form.actionTitle}版块设置</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/board/">版块</a></li>
              <li class="breadcrumb-item active">表单</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <div class="col-md-12">
                  <div class="card card-border-color card-border-color-primary">
                      <div class="card-header card-header-divider">${form.actionTitle}版块设置</div>
                      <div class="card-body">
                          <c:if test="${not empty errors}">
                          <div role="alert" class="alert alert-danger alert-dismissible">
                              <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
                              <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                              <div class="message"><strong>oOps!</strong> ${errors} </div>
                          </div>
                          </c:if>
                          <form method="post" action="${ADMIN}/board/config">
                          <div class="form-group row">
                              <label for="readWrite" class="col-12 col-sm-3 col-form-label text-sm-right">读写模式</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="readWrite"<c:if test="${'1' eq form.readWrite}"> checked="checked"</c:if> class="custom-control-input" value="1"><span class="custom-control-label">读写</span>
                                  </label>
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="readWrite"<c:if test="${'0' eq form.readWrite}"> checked="checked"</c:if> class="custom-control-input" value="0"><span class="custom-control-label">只读</span>
                                  </label>
                                  <span class="form-text text-muted">选择不同的模式,可以进行积分,会员组,会员等级限制</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="ipFilter" class="col-12 col-sm-3 col-form-label text-sm-right">IP地址过滤</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="ipFilter"<c:if test="${'1' eq form.ipFilter}"> checked="checked"</c:if> class="custom-control-input" value="1"><span class="custom-control-label">开启</span>
                                  </label>
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="ipFilter"<c:if test="${'0' eq form.ipFilter}"> checked="checked"</c:if> class="custom-control-input" value="0"><span class="custom-control-label">关闭</span>
                                  </label>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="readMinScore" class="col-12 col-sm-3 col-form-label text-sm-right">只读积分需求</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="readMinScore" type="number" class="form-control" value="${form.readMinScore}" required="required" />
                                  <span class="form-text text-muted">只读模式下会员积分的最低要求,单位:积分数</span>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="readLowMemberGroup" class="col-12 col-sm-3 col-form-label text-sm-right">只读会员组需求</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <select class="select2" name="readLowMemberGroup" style="width:300px;">
                                      <c:forEach items="${memberGroupData}" var="memberGroup">
                                      <option value="${memberGroup.key}"<c:if test="${form.readLowMemberGroup eq memberGroup.key }"> selected="selected"</c:if>>${memberGroup.value }</option>
                                      </c:forEach>
                                  </select>
                                  <span class="form-text text-muted">只读模式下会员组的最低要求,单位:会员组枚举</span>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="readLowMemberRole" class="col-12 col-sm-3 col-form-label text-sm-right">只读会员角色需求</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <select class="select2" name="readLowMemberRole" style="width:300px;">
                                      <c:forEach items="${memberRoleData}" var="memberRole">
                                      <option value="${memberRole.key}"<c:if test="${form.readLowMemberRole eq memberRole.key }"> selected="selected"</c:if>>${memberRole.value }</option>
                                      </c:forEach>
                                  </select>
                                  <span class="form-text text-muted">只读模式下会员角色的最低要求,单位:会员角色枚举</span>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="readLowMemberLevel" class="col-12 col-sm-3 col-form-label text-sm-right">只读等级需求</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="readLowMemberLevel" type="number" class="form-control" value="${form.readLowMemberLevel}" required="required" />
                                  <span class="form-text text-muted">只读模式下会员等级的最低要求,单位:等级数</span>
                              </div>
                          </div>
                          
                          <div class="form-group row pt-1 pb-1">
                              <label for="writeMinScore" class="col-12 col-sm-3 col-form-label text-sm-right">读写积分需求</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="writeMinScore" type="number" class="form-control" value="${form.writeMinScore}" required="required" />
                                  <span class="form-text text-muted">读写模式下会员积分的最低要求,单位:积分数</span>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="writeLowMemberGroup" class="col-12 col-sm-3 col-form-label text-sm-right">读写会员组需求</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <select class="select2" name="writeLowMemberGroup" style="width:300px;">
                                      <c:forEach items="${memberGroupData}" var="memberGroup">
                                      <option value="${memberGroup.key}"<c:if test="${form.writeLowMemberGroup eq memberGroup.key }"> selected="selected"</c:if>>${memberGroup.value }</option>
                                      </c:forEach>
                                  </select>
                                  <span class="form-text text-muted">读写模式下会员组的最低要求,单位:会员组枚举</span>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="writeLowMemberRole" class="col-12 col-sm-3 col-form-label text-sm-right">读写会员角色需求</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <select class="select2" name="writeLowMemberRole" style="width:300px;">
                                      <c:forEach items="${memberRoleData}" var="memberRole">
                                      <option value="${memberRole.key}"<c:if test="${form.writeLowMemberRole eq memberRole.key }"> selected="selected"</c:if>>${memberRole.value }</option>
                                      </c:forEach>
                                  </select>
                                  <span class="form-text text-muted">读写模式下会员角色的最低要求,单位:会员角色枚举</span>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="writeLowMemberLevel" class="col-12 col-sm-3 col-form-label text-sm-right">读写等级需求</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="writeLowMemberLevel" type="number" class="form-control" value="${form.writeLowMemberLevel}" required="required"/>
                                  <span class="form-text text-muted">读写模式下会员等级的最低要求,单位:等级数</span>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="writeMinInterrupt" class="col-12 col-sm-3 col-form-label text-sm-right">连续操作次数</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="writeMinInterrupt" type="number" class="form-control" value="${form.writeMinInterrupt}" required="required"/>
                                  <span class="form-text text-muted">读写模式下同一会员连续发贴的次数限制.例:3,同一会员只能连续3次发布主题.防止恶意注水</span>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="editMinute" class="col-12 col-sm-3 col-form-label text-sm-right">可编辑时长</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="editMinute" type="number" class="form-control" value="${form.editMinute}" required="required" max="9" min="1"/>
                                  <span class="form-text text-muted">读写模式下原作者可编辑的时长,值应取1-9之间;单位:分钟; 例:3,在提交后可在3分钟内进行编辑;-1表示不限制</span>
                              </div>
                          </div>
                          <div class="form-group row text-right">
                              <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
                                  <input type="hidden" name="record" value="${form.record}" />
                                  <input type="hidden" name="token" value="${form.token}" />
                                  <input type="hidden" name="boardId" value="${form.boardId}" />
                                  <input type="hidden" name="volumesId" value="${form.volumesId}" />
                                  <button type="submit" class="btn btn-space btn-primary">提交</button>
                                  <button class="btn btn-space btn-secondary">取消</button>
                              </div>
                          </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>