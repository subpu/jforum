<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>发件箱</title>
    <style>.card-header .tools{font-size:0.8em}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">发件箱</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!--Responsive table-->
              <div class="col-sm-12">
                  <div class="card card-table">
                      <div class="card-header">发件箱消息列表
                          <div class="tools">
                              <a href="${ADMIN}/message/create"><i class="icon mdi mdi-plus"></i>新增</a>
                          </div>
                      </div>
                      <div class="card-body">
                          <div class="table-responsive noSwipe">
                              <table class="table table-striped table-hover">
                                  <thead>
                                      <tr>
                                          <th style="width:5%;">
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
                                              </label>
                                          </th>
                                          <th style="width:10%">类别</th>
                                          <th style="width:25%;">主题</th>
                                          <th style="width:10%;">收件人</th>
                                          <th style="width:10%;">创建日期</th>
                                          <th style="width:20%;">状态</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <!-- foreach tr -->
                                      <c:forEach items="${rs}" var="forumLetter">
                                      <tr>
                                          <td>
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" name="id" class="custom-control-input" value="${forumLetter.id}"><span class="custom-control-label"></span>
                                              </label>
                                          </td>
                                          <td>${forumLetter.typed.title}</td>
                                          <td><a href="${ADMIN}/message/view?id=${forumLetter.id}" title="查看消息">${forumLetter.title}</a></td>
                                          <td><forum:datalist value="${forumLetter.targetReceiverNicknames}"/></td>
                                          <td><forum:date value="${forumLetter.entryDateTime}"/></td>
                                          <td>可用</td>
                                      </tr>
                                      </c:forEach>
                                  </tbody>
                              </table>
                          </div>
                          <div class="row be-datatable-footer">
                              <div class="col-sm-5">
                                  <forum:pagination_collect total="${pageData.records}" page="${pageData.page}" pageSize="${pageData.pageSize}">
                                  <div class="dataTables_info" id="table1_info" role="status" aria-live="polite">Showing ${tagPageMinId} to ${tagPageMaxId} of ${tagPageTotal} entries</div>
                                  </forum:pagination_collect>
                              </div>
                              <div class="col-sm-7">
                                  <div class="dataTables_paginate paging_simple_numbers" id="table1_paginate">
                                      <!-- 分页代码 -->
                                      <nav>
                                          <ul class="pagination">
                                          <forum:pagination size="${pageData.pageSize}" total="${pageData.records}" url="${pageData.pageURL}">
                                          <c:set var="currentPageNumber">${pageData.page}</c:set>
                                              <c:choose>
                                              <c:when test="${currentPageNumber eq anchor}">
                                                  <li class="page-item active"><a href="javascript:;" class="page-link">${anchor}</a></li>
                                              </c:when>
                                              <c:otherwise>
                                                  <li class="page-item"><a href="${uri}" class="page-link">${anchor}</a></li>
                                              </c:otherwise>
                                              </c:choose>
                                          </forum:pagination>
                                          </ul>
                                     </nav>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>