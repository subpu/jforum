<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="BASE" scope="application" value="${pageContext.request.contextPath}" />
<c:set var="ADMIN" scope="application"><spring:eval expression="@environment.getProperty('site.admin.path')"/></c:set>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Apobates Studio for JForum</title>
    <link rel="stylesheet" type="text/css" href="${BASE}/static/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="${BASE}/static/lib/material-design-icons/css/material-design-iconic-font.min.css"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="${BASE}/static/css/app.css" type="text/css"/>
  </head>
  <body class="be-splash-screen">
    <div class="be-wrapper be-login">
      <div class="be-content">
        <div class="main-content container-fluid">
          <div class="splash-container">
            <div class="card card-border-color card-border-color-primary">
              <div class="card-header">
                  <img src="${BASE}/static/img/logo.svg" alt="logo" class="logo-img" style="width:75%;height:75%">
                  <span class="splash-description">Please enter your user information.</span>
              </div>
              <div class="card-body">
                <c:if test="${not empty errors}">
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button>
                    <div class="icon"><span class="mdi mdi-alert-triangle"></span></div>
                    <div class="message"><strong>oOps!</strong> ${errors}.</div>
                </div>
                </c:if>
                <form method="post" action="${ADMIN}/" id="${form.lexical}">
                  <div class="form-group">
                    <input id="names" name="names" type="text" placeholder="输入登录帐号" autocomplete="off" class="form-control">
                  </div>
                  <div class="form-group">
                    <input id="pswd" name="pswd" type="password" placeholder="输入登录密码" class="form-control">
                  </div>
                  <div class="form-group row login-tools">
                    <div class="col-6 login-remember">
                      <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input"><span class="custom-control-label">记住我</span>
                      </label>
                    </div>
                    <div class="col-6 login-forgot-password"><a href="pages-forgot-password.html">找回密码?</a></div>
                  </div>
                  <div class="form-group login-submit">
                    <input type="hidden" name="redirect" value="${form.redirect}" />
                    <input type="hidden" name="token" value="${form.token}" />
                    <input type="hidden" name="trace" value="msa"/>
                    <button data-dismiss="modal" type="submit" class="btn btn-primary btn-xl">登录系统</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="${BASE}/static/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="${BASE}/static/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="${BASE}/static/lib/bootstrap4/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="${BASE}/static/js/app.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(document).ready(function(){
      	//initialize the javascript
      	App.init();
      });
      
    </script>
  </body>
</html>