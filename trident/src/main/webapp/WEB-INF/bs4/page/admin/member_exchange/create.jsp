<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>创建VIP交易记录</title>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">创建VIP交易记录</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/member/exchange/">VIP会员</a></li>
              <li class="breadcrumb-item active">表单</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <div class="col-md-12">
                  <div class="card card-border-color card-border-color-primary">
                      <div class="card-header card-header-divider">创建VIP交易记录</div>
                      <div class="card-body">
                          <c:if test="${not empty errors}">
                          <div role="alert" class="alert alert-danger alert-dismissible">
                              <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
                              <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                              <div class="message"><strong>oOps!</strong> ${errors} </div>
                          </div>
                          </c:if>
                          <form method="post" action="${ADMIN}/member/exchange/create">
                          <div class="form-group row">
                              <label for="names" class="col-12 col-sm-3 col-form-label text-sm-right">会员</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="names" type="text" class="form-control" value="${form.names}" required="required" tabindex="1" id="apo_member_lookup" placeholder="输入会员的登录帐号" />
                                  <span class="form-text text-muted">输入会员的登录帐号,支持字行的模糊提示</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="limit" class="col-12 col-sm-3 col-form-label text-sm-right">时长</label>
                              <div class="col-sm-4 col-lg-3 mb-3 mb-sm-0">
                                  <input name="limit" type="text" class="form-control" value="${form.limit}" required="required" tabindex="2"/>
                              </div>
                              <div class="col-sm-4 col-lg-3">
                                  <select class="select2" name="unit" tabindex="3" style="width:300px;">
                                      <option value="0">选择日期单位</option>
                                      <option value="5">月</option>
                                      <option value="6">年</option>
                                  </select>
                              </div>
                              <form:errors path="form.limit" delimiter="," element="div" cssClass="invalid-feedback d-inline"/>
                          </div>
                          <div class="form-group row">
                              <label for="serial" class="col-12 col-sm-3 col-form-label text-sm-right">交易码</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="serial" type="text" class="form-control" value="${form.serial}" tabindex="4"/>
                              </div>
                          </div>
                          <div class="form-group row text-right">
                              <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
                                  <input type="hidden" name="record" value="${form.record}" />
                                  <input type="hidden" name="token" value="${form.token}" />
                                  <input type="hidden" name="memberId" id="memberId" value="${form.memberId}" />
                                  <button type="submit" class="btn btn-space btn-primary" tabindex="5">提交</button>
                                  <button class="btn btn-space btn-secondary">取消</button>
                              </div>
                          </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>
