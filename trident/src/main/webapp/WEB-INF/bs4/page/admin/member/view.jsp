<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>会员主页</title>
    <style>
    #member-header{padding:20px 0;background-color: #232732;color: #fff;}
    #member-avatar{vertical-align: top;text-align:center}
    #member-collect{margin-top: 30px;}
    </style>
  </head>
  <body>
      <div class="row">
          <div class="col-lg-12" style="background-color:#fff">
              <div id="member-header" class="row">
                  <div class="col-xl-1 col-lg-2 col-md-3 col-sm-12" id="member-avatar"><img src="${FRONT}/member/avatar/${member.id}.png" class="rounded-circle mr-2 mb-2 member-avatar"/></div>
                  <div class="col-xl-11 col-lg-10 col-md-9 col-sm-12" id="member-info">
                      <h3 title="会员昵称" class="member-${profile.style}">${member.nickname}&nbsp;<span class="badge badge-primary" title="会员组">${profile.groupNames}</span>&nbsp;<span class="badge badge-warning" title="角色">${profile.roleNames}</span><br/><small class="msg"><forum:out value="${member.signature}" defValue="尚未填写个性签名"/></small></h3>
                      <div id="member-stats" class="row">
                          <div class="col-xl-11 col-lg-10 col-md-9 col-sm-12">
                              <ul class="list-inline">
                                  <li style="display:inline;padding-right:10px;">话题&nbsp;<span>${profile.threads}</span></li>
                                  <li style="display:inline;padding-right:10px;">回复&nbsp;<span>${profile.replies}</span></li>
                                  <li style="display:inline;padding-right:10px;">积分&nbsp;<span>${profile.score}</span></li>
                                  <li style="display:inline;padding-right:10px;">等级&nbsp;<span>${profile.levelNo} / ${profile.level}</span></li>
                                  <li style="display:inline;padding-right:10px;">状态&nbsp;<span>${member.status.title}</span></li>
                                  <li style="display:inline;padding-right:10px;">注册日期&nbsp;<span><forum:date value="${member.registeDateTime}"/></span></li>
                              </ul>
                          </div>
                          <div class="col-xl-1 col-lg-2 col-md-3" style="float:right"><span class="ico mdi mdi-hourglass-alt"></span> ${profile.activeDateTime}</div>
                      </div>
                  </div>
              </div>
              <div id="member-collect">
                  <div class="tab-container">
                      <ul role="tablist" class="nav nav-tabs">
                          <li class="nav-item"><a href="#member-topic" data-toggle="tab" role="tab" class="nav-link active">最近话题</a></li>
                          <li class="nav-item"><a href="#member-posts" data-toggle="tab" role="tab" class="nav-link">最新回复</a></li>
                          <li class="nav-item"><a href="#member-events" data-toggle="tab" role="tab" class="nav-link">日志</a></li>
                      </ul>
                      <div class="tab-content">
                          <div id="member-topic" class="tab-pane active cont">
                              <table class="table">
                                  <thead>
                                      <tr>
                                          <th>版块</th>
                                          <th>话题主题</th>
                                          <th>作者</th>
                                          <th>创建日期</th>
                                      </tr>
                                  </thead>
                                  <tbody class="topic-list" data-handler="${FRONT}/member/home/topic/publish/json" data-query="id:${member.id}">
                                  </tbody>
                              </table>
                          </div>
                          <div id="member-posts" class="tab-pane cont">
                              <table class="table">
                                  <thead>
                                      <tr>
                                          <th>版块</th>
                                          <th>话题主题</th>
                                          <th>作者</th>
                                          <th>回复日期</th>
                                      </tr>
                                  </thead>
                                  <tbody class="topic-list" data-handler="${FRONT}/member/home/topic/reply/json" data-query="id:${member.id}">
                                  </tbody>
                              </table>
                          </div>
                          <div id="member-events" class="tab-pane">
                              <table class="table">
                                  <thead>
                                      <tr>
                                          <th>动作</th>
                                          <th>操作日期</th>
                                          <th>Ip地址</th>
                                          <th>结果</th>
                                      </tr>
                                  </thead>
                                  <tbody class="operate-list" data-handler="${ADMIN}/member/action/json" data-query="id:${member.id},names:${member.names}">
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>