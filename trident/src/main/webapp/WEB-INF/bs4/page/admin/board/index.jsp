<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>版块</title>
    <style>.h186{height:186px;background-color:#868e96;margin:0}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">版块列表
              <small class="tools float-right">
                  <a href="${ADMIN}/board/edit?volume=${paramVolumes}"><i class="icon mdi mdi-plus"></i>新增</a>
              </small>
          </h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!-- foreach card -->
              <c:forEach items="${rs}" var="board">
              <div class="col-md-3 col-sm-6">
                  <div class="card">
                      <div class="card-header h186 text-center"><img src="<forum:image path="${board.imageAddr}"/>" class="rounded-circle" style="width:140px;height:140px"/></div>
                      <div class="card-body">
                          <h5 class="card-title">${board.title}</h5>
                          <!-- <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6> -->
                          <p class="card-text">${board.description}</p>
                          <a class="btn btn-primary" role="button" href="${ADMIN}/board/edit?id=${board.id}">编辑</a>
                          <div class="btn-group">
                              <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle">操作 <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                              <div role="menu" class="dropdown-menu">
                                  <a href="javascript:;" class="dropdown-item action-cmd" data-handler="${ADMIN}/board/lock" data-query="id:${board.id},volumes:${board.volumesId}">锁定版块</a>
                                  <a href="javascript:;" class="dropdown-item action-cmd" data-handler="${ADMIN}/board/remove" data-query="id:${board.id},volumes:${board.volumesId}">删除</a>
                                  <a href="javascript:;" class="dropdown-item action-cmd" data-handler="${ADMIN}/board/lock/remove" data-query="id:${board.id},volumes:${board.volumesId}">解除锁定</a>
                                  <div class="dropdown-divider"></div>
                                  <a href="${ADMIN}/board/moderator/?board=${board.id}&volume=${board.volumesId}" class="dropdown-item">版主</a>
                                  <a href="${ADMIN}/board/category/?board=${board.id}" class="dropdown-item">话题类型</a>
                                  <a href="${ADMIN}/topic/list/${board.id}.xhtml" class="dropdown-item">话题</a>
                                  <a href="${ADMIN}/board/config?id=${board.id}&volumes=${board.volumesId}" class="dropdown-item">配置</a>
                                  <a href="${ADMIN}/board/action/star?id=${board.id}" class="dropdown-item">收藏者</a>
                                  <a href="${ADMIN}/board/action/?id=${board.id}" class="dropdown-item">动作</a>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              </c:forEach>
          </div>
      </div>
  </body>
</html>