<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>新增惩罚记录</title>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">新增惩罚记录</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item active">表单</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <div class="col-md-12">
                  <div class="card card-border-color card-border-color-primary">
                      <div class="card-header card-header-divider">新增惩罚记录</div>
                      <div class="card-body">
                          <form method="post" action="${ADMIN}/member/penalize/create">
                          <div class="form-group row">
                              <label for="names" class="col-12 col-sm-3 col-form-label text-sm-right">会员</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="names" type="text" class="form-control" value="${form.names}" required="required" tabindex="1" id="apo_member_lookup" placeholder="输入会员的登录帐号" />
                                  <span class="form-text text-muted">输入会员的登录帐号,支持字行的模糊提示</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="arrive" class="col-12 col-sm-3 col-form-label text-sm-right">惩罚</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <select class="apo_select" name="arrive" tabindex="2" data-active="${form.arrive}" data-handler="${ADMIN}/member/penalize/status/json" style="width:300px;">
                                      <option value="0">选择进行何种惩罚</option>
                                  </select>
                                  <span class="form-text text-muted">[A]禁足: 能登录,但话题相关操作禁止; [B]禁言: 能登录, 但话题相关操作只读; [C]附言: 能登录, 但话题相关操作只可以回复.不能发布话题</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="limit" class="col-12 col-sm-3 col-form-label text-sm-right">惩罚时长</label>
                              <div class="col-sm-4 col-lg-3 mb-3 mb-sm-0">
                                  <input name="limit" type="text" class="form-control" value="${form.limit}" required="required" tabindex="5"/>
                              </div>
                              <div class="col-sm-4 col-lg-3">
                                  <select class="apo_select" name="unit" tabindex="4" data-active="${form.unit}" data-handler="${ADMIN}/member/penalize/unit/json" style="width:300px;">
                                      <option value="0">选择日期单位</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="reason" class="col-12 col-sm-3 col-form-label text-sm-right">理由</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <textarea id="reason" name="reason" rows="5" class="form-control" required="required" tabindex="6">${form.reason}</textarea>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="evidences" class="col-12 col-sm-3 col-form-label text-sm-right">证据列表</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <textarea id="evidences" name="evidences" rows="5" class="form-control" required="required" tabindex="7">${form.evidences}</textarea>
                                  <span class="form-text text-muted">输入举报的话题或回复连接地址</span>
                              </div>
                          </div>
                          <div class="form-group row text-right">
                              <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
                                  <input type="hidden" name="memberId" id="memberId" value="${form.memberId}" />
                                  <input type="hidden" name="token" value="${form.token}" />
                                  <button type="submit" class="btn btn-space btn-primary" tabindex="8">提交</button>
                                  <button class="btn btn-space btn-secondary">取消</button>
                              </div>
                          </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>