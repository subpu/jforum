<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>话题回复</title>
    <style>.card-header .tools{font-size:0.8em}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">话题回复</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!--Responsive table-->
              <div class="col-sm-12">
                  <div class="card card-table">
                      <div class="card-header">
                          <h4 id="page-title" data-handler="${ADMIN}/topic/title" data-query="id:${param.topic}" data-format="#%T%#的回复列表">loading</h4>
                          <div class="tools">
                          </div>
                      </div>
                      <div class="card-body">
                          <div class="table-responsive noSwipe">
                              <table class="table table-striped table-hover">
                                  <thead>
                                      <tr>
                                          <th style="width:5%;">
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
                                              </label>
                                          </th>
                                          <th style="width:20%;">回复者</th>
                                          <th style="width:17%;">楼主</th>
                                          <th style="width:15%;">状态</th>
                                          <th style="width:10%;">楼层</th>
                                          <th style="width:10%;">创建日期</th>
                                          <th style="width:10%;">Ip地址</th>
                                          <th class="actions"></th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <!-- foreach tr -->
                                      <c:forEach items="${rs}" var="posts">
                                      <tr>
                                          <td>
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" name="id" class="custom-control-input" value="${posts.id}"><span class="custom-control-label"></span>
                                              </label>
                                          </td>
                                          <td><a href="${ADMIN}/member/${posts.memberId}.xhtml">${posts.memberNickname}</a></td>
                                          <td><forum:print value="${posts.reply}" trueTitle="否" falseTitle="是"/></td>
                                          <td><forum:print value="${posts.status}" trueTitle="正常" falseTitle="删除"/></td>
                                          <td>${posts.floorNumber}楼</td>
                                          <td><forum:date value="${posts.entryDateTime}"/></td>
                                          <td>${posts.ipAddr}</td>
                                          <td class="actions">
                                              <a class="btn btn-primary" role="button" href="${ADMIN}/topic/posts/view?id=${posts.id}">查看</a>
                                              <a class="btn btn-secondary" role="button" href="${ADMIN}/topic/posts/mood?posts=${posts.id}">喜好</a>
                                              <a class="btn btn-secondary action-cmd" role="button" href="javascript:;" data-handler="${ADMIN}/topic/posts/remove" data-query="id:${posts.id}">删除</a>
                                          </td>
                                      </tr>
                                      </c:forEach>
                                  </tbody>
                              </table>
                          </div>
                          <div class="row be-datatable-footer">
                              <div class="col-sm-5">
                                  <forum:pagination_collect total="${pageData.records}" page="${pageData.page}" pageSize="${pageData.pageSize}">
                                  <div class="dataTables_info" id="table1_info" role="status" aria-live="polite">Showing ${tagPageMinId} to ${tagPageMaxId} of ${tagPageTotal} entries</div>
                                  </forum:pagination_collect>
                              </div>
                              <div class="col-sm-7">
                                  <div class="dataTables_paginate paging_simple_numbers" id="table1_paginate">
                                      <!-- 分页代码 -->
                                      <nav>
                                          <ul class="pagination">
                                          <forum:pagination size="${pageData.pageSize}" total="${pageData.records}" url="${pageData.pageURL}">
                                          <c:set var="currentPageNumber">${pageData.page}</c:set>
                                              <c:choose>
                                              <c:when test="${currentPageNumber eq anchor}">
                                                  <li class="page-item active"><a href="javascript:;" class="page-link">${anchor}</a></li>
                                              </c:when>
                                              <c:otherwise>
                                                  <li class="page-item"><a href="${uri}" class="page-link">${anchor}</a></li>
                                              </c:otherwise>
                                              </c:choose>
                                          </forum:pagination>
                                          </ul>
                                     </nav>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>