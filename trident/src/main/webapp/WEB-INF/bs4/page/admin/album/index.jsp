<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>话题像册列表</title>
    <style>.card-header .tools{font-size:0.8em}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">话题像册列表</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!--Responsive table-->
              <div class="col-sm-12">
                  <div class="card">
                      <div class="card-header">
                          <h4 id="page-title">话题像册列表</h4>
                      </div>
                      <div class="card-body">
                          <div class="gallery-container">
                              <c:forEach items="${rs}" var="topicAlbum">
                              <div class="item">
                                  <div class="photo">
                                      <div class="img">
                                          <img src="<forum:thumb path="${topicAlbum.coverLink}"/>" alt="Gallery Image" />
                                          <div class="over">
                                              <div class="info-wrapper">
                                                  <div class="info">
                                                      <div class="title">${topicAlbum.title}</div>
                                                      <div class="date">${topicAlbum.entryDateTime}</div>
                                                      <div class="description"></div>
                                                      <div class="func"><a href="${ADMIN}/topic/album/picture?topic=${topicAlbum.topicId}"><i class="icon mdi mdi-link"></i></a></div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              </c:forEach>
                          </div>
                          <div class="row be-datatable-footer">
                              <div class="col-sm-5">
                                  <forum:pagination_collect total="${pageData.records}" page="${pageData.page}" pageSize="${pageData.pageSize}">
                                  <div class="dataTables_info" id="table1_info" role="status" aria-live="polite">Showing ${tagPageMinId} to ${tagPageMaxId} of ${tagPageTotal} entries</div>
                                  </forum:pagination_collect>
                              </div>
                              <div class="col-sm-7">
                                 <div class="dataTables_paginate paging_simple_numbers" id="table1_paginate">
                                    <!-- 分页代码 -->
                                    <nav>
                                        <ul class="pagination">
                                            <forum:pagination size="${pageData.pageSize}" total="${pageData.records}" url="${pageData.pageURL}">
                                            <c:set var="currentPageNumber">${pageData.page}</c:set>
                                            <c:choose>
                                                <c:when test="${currentPageNumber eq anchor}">
                                                <li class="page-item active"><a href="javascript:;" class="page-link">${anchor}</a></li>
                                                </c:when>
                                                <c:otherwise>
                                                <li class="page-item"><a href="${uri}" class="page-link">${anchor}</a></li>
                                                </c:otherwise>
                                            </c:choose>
                                            </forum:pagination>
                                        </ul>
                                    </nav>
                                 </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>