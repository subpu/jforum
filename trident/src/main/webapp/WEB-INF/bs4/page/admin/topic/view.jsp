<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>${topic.title}</title>
  </head>
  <body>
      <div class="email-head">
          <div class="email-head-subject">
              <div class="title">
                <a href="javascript:;" class="active">
                    <span class="icon mdi <forum:status value="${topic.status.symbol}"/>"></span>
                </a> 
                <span>${topic.title}</span> 
                <div class="icons tools dropdown">
                    <a href="${ADMIN}/topic/edit?id=${topic.id}" title="编辑"><span class="icon mdi mdi-edit"></span></a>
                    <c:if test="${'LOCKED' eq  topic.status}">
                    <a class="action-cmd" role="button" href="javascript:;" data-handler="${ADMIN}/topic/lock/remove" data-query="id:${topic.id}" title="解锁"><span class="icon mdi mdi-lock-open"></span></a>
                    </c:if>
                    <a href="#" role="button" data-toggle="dropdown" class="dropdown-toggle"><span class="icon mdi mdi-more-vert"></span></a>
                    <div role="menu" class="dropdown-menu">
                        <a href="javascript:;" class="dropdown-item action-cmd" data-handler="${ADMIN}/topic/lock" data-query="id:${topic.id}">锁定</a>
                        <a href="javascript:;" class="dropdown-item action-cmd" data-handler="${ADMIN}/topic/remove" data-query="id:${topic.id}">删除</a>
                        <a href="javascript:;" class="dropdown-item action-cmd" data-handler="${ADMIN}/topic/top" data-query="id:${topic.id}">置顶</a>
                        <div class="dropdown-divider"></div>
                        <a href="${ADMIN}/topic/config?id=${topic.id}" class="dropdown-item">配置</a>
                        <a href="${ADMIN}/topic/album/picture?topic=${topic.id}" class="dropdown-item">图片</a>
                        <a href="${ADMIN}/topic/posts/?topic=${topic.id}" class="dropdown-item">回复</a>
                    </div>
                </div>
              </div>
          </div>
          <div class="email-head-sender">
              <div class="date">阅读 &nbsp; ${topic.stats.displaies} &nbsp; &nbsp;赞 &nbsp; ${topic.stats.likes}</div>
              <div class="sender">
                  <a id="page-title" href="${ADMIN}/topic/list/${topic.boardId}.xhtml" title="版块话题列表" data-handler="${ADMIN}/board/title" data-query="id:${topic.boardId}" data-format="%T%">loading</a> &nbsp; | &nbsp;
                  <forum:outlet value="${topic.topicCategoryName}" optional="普通"/> &nbsp; | &nbsp;
                  <a href="${ADMIN}/member/${topic.memberId}.xhtml">${topic.memberNickname}</a> &nbsp; <forum:date value="${topic.entryDateTime}"/>
              </div>
          </div>
      </div>
      <div class="email-body" style="min-height:760px">${topic.content.content}</div>
      <div class="email-attachments">
            <div class="title">标签</div>
            <ul>
              <c:forEach items="${topic.tages}" var="tag">
              <li style="display:inline"><a href="javascript:;"><span class="badge badge-primary">${tag.names}</span></a></li>
              </c:forEach>
            </ul>
      </div>
  </body>
</html>