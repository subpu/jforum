<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>${form.actionTitle}会员帐户保护</title>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">${form.actionTitle}会员帐户保护</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item active">表单</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <div class="col-md-12">
                  <div class="card card-border-color card-border-color-primary">
                      <div class="card-header card-header-divider">${form.actionTitle}会员帐户保护</div>
                      <div class="card-body">
                          <c:if test="${not empty errors}">
                          <div role="alert" class="alert alert-danger alert-dismissible">
                              <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
                              <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                              <div class="message"><strong>oOps!</strong> ${errors} </div>
                          </div>
                          </c:if>
                          <form method="post" action="${ADMIN}/member/protect/edit">
                          <div class="form-group row">
                              <label for="names" class="col-12 col-sm-3 col-form-label text-sm-right">帐户</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="names" type="text" class="form-control" value="${form.names}" required="required" tabindex="1"/>
                                  <span class="form-text text-muted">内置的保护帐号: ${nativeNames}; 不用创建</span>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="status" class="col-12 col-sm-3 col-form-label text-sm-right">状态</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="status"<c:if test="${'1' eq form.status}"> checked="checked"</c:if> class="custom-control-input" value="1"><span class="custom-control-label">可用</span>
                                  </label>
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="status"<c:if test="${'0' eq form.status}"> checked="checked"</c:if> class="custom-control-input" value="0"><span class="custom-control-label">禁用</span>
                                  </label>
                              </div>
                          </div>
                          <div class="form-group row text-right">
                              <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
                                  <input type="hidden" name="record" value="${form.record}" />
                                  <input type="hidden" name="token" value="${form.token}" />
                                  <button type="submit" class="btn btn-space btn-primary" tabindex="6">提交</button>
                                  <button class="btn btn-space btn-secondary">取消</button>
                              </div>
                          </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>