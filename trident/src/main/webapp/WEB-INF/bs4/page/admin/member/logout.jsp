<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="refresh" content="5; url=${ADMIN}/" />
    <title>正在进行注销操作...</title>
    <style type="text/css">
    body{margin:0;padding:0;font: 13px/20px 'Microsoft YaHei', 微软雅黑, helvetica, arial, verdana, tahoma, sans-serif;}
    p{text-align:center;margin-top:23%;height:150px;line-height:150px;background-color:#ddd;font-size:2.3em;font-weight:900}
    </style>
  </head>
  <body>
      <p class="clearCache">稍候将跳转到站点首页</p>
  </body>
</html>