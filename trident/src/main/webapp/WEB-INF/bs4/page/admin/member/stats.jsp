<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>会员统计</title>
    <style></style>
  </head>
  <body>
      <div class="row">
          <div class="col-md-12">
              <div class="card">
                  <div class="card-header pb-3">最近七日注册的会员统计</div>
                  <div class="card-body" data-raw='${rs}' id="sevenDayChart" style="height: 350px;">
                  </div>
              </div>
          </div>
      </div>
      <!-- 角色占比|组占比|状态占比 -->
      <div class="row">
          <div class="col-12 col-lg-4">
              <div class="card">
                  <div class="card-header pb-3">会员组占比</div>
                  <div class="card-body statsDonutChart" style="height: 250px;text-align:center" data-handler="${ADMIN}/member/stats/donut/group">
                   
                  </div>
              </div>
          </div>
          <div class="col-12 col-lg-4">
              <div class="card">
                  <div class="card-header pb-3">会员状态占比</div>
                  <div class="card-body statsDonutChart" style="height: 250px;text-align:center" data-handler="${ADMIN}/member/stats/donut/status">
                  </div>
              </div>
          </div>
          <div class="col-12 col-lg-4">
              <div class="card">
                  <div class="card-header pb-3">会员角色占比</div>
                  <div class="card-body statsDonutChart" style="height: 250px;text-align:center" data-handler="${ADMIN}/member/stats/donut/role">
                  </div>
              </div>
          </div>
      </div>
      <!-- 最近会员|地域|ip区段 -->
      <div class="row">
          <div class="col-12 col-lg-6">
              <div class="card">
                  <div class="card-header">
                      <div class="title">最近注册的会员</div>
                  </div>
                  <div class="card-body stats-data-load" data-handler="${ADMIN}/member/stats/recent/json" data-function="drawMemberResult">
                  </div>
              </div>
          </div>
          <div class="col-12 col-lg-6">
              <div class="card">
                  <div class="card-header">
                      <div class="title">当前在线的会员</div>
                  </div>
                  <div class="card-body stats-data-load" data-handler="${ADMIN}/member/stats/online/json" data-function="drawMemberResult">
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>