<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>${form.actionTitle}版主权限</title>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">${form.actionTitle}版主权限</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/board/">版块</a></li>
              <li class="breadcrumb-item active">General</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <div class="col-md-12">
                  <div class="card card-border-color card-border-color-primary">
                      <div class="card-header card-header-divider">${form.actionTitle}版主权限</div>
                      <div class="card-body">
                          <c:if test="${not empty errors}">
                          <div role="alert" class="alert alert-danger alert-dismissible">
                              <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
                              <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                              <div class="message"><strong>oOps!</strong> ${errors} </div>
                          </div>
                          </c:if>
                          <c:set var="tmpArray" value="${form.actions}"></c:set>
                          <form method="post" action="${ADMIN}/board/moderator/permission">
                          <div class="form-group row">
                              <label for="" class="col-12 col-sm-3 col-form-label text-sm-right">全部选中</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" class="custom-control-input" name="checked" value="1" id="mop_check_all"/><span class="custom-control-label">是</span>
                                  </label>
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" class="custom-control-input" name="checked" value="0" checked="checked" /><span class="custom-control-label">否</span>
                                  </label>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="volumesId" class="col-12 col-sm-3 col-form-label text-sm-right">版块权限</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <c:forEach items="${rs['board']}" var="bp">
                                  <label class="custom-control custom-checkbox custom-control-inline">
                                      <input type="checkbox" class="custom-control-input" name="actions" value="${bp.id}"<c:if test="${forum:contains(tmpArray, bp.id)}"> checked="checked"</c:if>/><span class="custom-control-label">${bp.title}</span>
                                  </label>
                                  </c:forEach>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="boardId" class="col-12 col-sm-3 col-form-label text-sm-right">话题权限</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <c:forEach items="${rs['topic']}" var="tp">
                                  <label class="custom-control custom-checkbox custom-control-inline">
                                      <input type="checkbox" class="custom-control-input" name="actions" value="${tp.id}"<c:if test="${forum:contains(tmpArray, tp.id)}"> checked="checked"</c:if>/><span class="custom-control-label">${tp.title}</span>
                                  </label>
                                  </c:forEach>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="memberNames" class="col-12 col-sm-3 col-form-label text-sm-right">回复权限</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <c:forEach items="${rs['posts']}" var="pp">
                                  <label class="custom-control custom-checkbox custom-control-inline">
                                      <input type="checkbox" class="custom-control-input" name="actions" value="${pp.id}"<c:if test="${forum:contains(tmpArray, pp.id)}"> checked="checked"</c:if>/><span class="custom-control-label">${pp.title}</span>
                                  </label>
                                  </c:forEach>
                              </div>
                          </div>
                          <div class="form-group row text-right">
                              <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
                                  <input type="hidden" name="record" value="${form.record}" />
                                  <input type="hidden" name="token" value="${form.token}" />
                                  <input type="hidden" name="moderatorId" value="${form.moderatorId}" />
                                  <button type="submit" class="btn btn-space btn-primary" tabindex="4">提交</button>
                                  <button class="btn btn-space btn-secondary">取消</button>
                              </div>
                          </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>