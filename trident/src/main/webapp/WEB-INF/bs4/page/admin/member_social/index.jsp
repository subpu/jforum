<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>会员社交信息</title>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">会员社交信息</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/member/${social.memberId}.xhtml">会员</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!--Responsive table-->
              <div class="col-sm-12">
                  <div class="card card-table">
                      <div class="card-header">会员社交信息
                          <div class="tools">
                          </div>
                      </div>
                      <div class="card-body">
                          <div class="table-responsive noSwipe">
                              <table class="table table-striped table-hover">
                                  <thead>
                                      <tr>
                                          <th style="width:5%;">
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
                                              </label>
                                          </th>
                                          <th style="width:20%;">电子邮箱</th>
                                          <th style="width:20%;">腾讯微信</th>
                                          <th style="width:15%;">新浪微博</th>
                                          <th style="width:15%;">阿里旺旺</th>
                                          <th style="width:10%;">QQ</th>
                                          <th class="actions"></th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td>
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" name="id" class="custom-control-input" value="${social.id}"><span class="custom-control-label"></span>
                                              </label>
                                          </td>
                                          <td>${social.email}</td>
                                          <td>${social.weixin}</td>
                                          <td>${social.weibo}</td>
                                          <td>${social.alibaba}</td>
                                          <td>${social.qq}</td>
                                          <td class="actions">
                                              
                                          </td>
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>