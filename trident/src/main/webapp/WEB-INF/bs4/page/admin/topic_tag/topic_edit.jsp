<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>增加话题标签</title>
    <style>.fileinput-upload-button,.btn-file{line-height:45px;}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">话题标签</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item active"><a href="${ADMIN}/topic/tag/collection?topic=${form.topic}">列表</a></li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <div class="col-md-12">
                  <div class="card card-border-color card-border-color-primary">
                      <div class="card-header card-header-divider">增加话题标签</div>
                      <div class="card-body">
                          <c:if test="${not empty errors}">
                          <div role="alert" class="alert alert-danger alert-dismissible">
                              <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
                              <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                              <div class="message"><strong>oOps!</strong> ${errors} </div>
                          </div>
                          </c:if>
                          <form method="post" action="${ADMIN}/topic/tag/create">
                          <div class="form-group row">
                              <label for="topic" class="col-12 col-sm-3 col-form-label text-sm-right">话题</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input type="text" class="form-control" value="${form.title}" readonly="readonly" />
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="names" class="col-12 col-sm-3 col-form-label text-sm-right">标签</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="names" type="text" class="form-control" value="${form.names}" required="required" tabindex="1"/>
                              </div>
                          </div>
                          <div class="form-group row text-right">
                              <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
                                  <input type="hidden" name="token" value="${form.token}" />
                                  <input type="hidden" name="topic" value="${form.topic}" />
                                  <button type="submit" class="btn btn-space btn-primary" tabindex="6">提交</button>
                                  <button class="btn btn-space btn-secondary">取消</button>
                              </div>
                          </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>