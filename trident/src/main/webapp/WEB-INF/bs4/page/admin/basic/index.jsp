<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>站点信息</title>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">站点信息</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Tables</a></li>
              <li class="breadcrumb-item active">General</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
      </div>
  </body>
</html>