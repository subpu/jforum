<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>消息内容</title>
  </head>
  <body>
      <div class="email-head">
          <div class="email-head-subject">
              <div class="title">
                <a href="javascript:;" class="active">
                    <span class="icon mdi mdi-star"></span>
                </a> 
                <span>${letter.title}</span> 
                <div class="icons">
                    <c:if test="${isInbox}">
                    <a href="${ADMIN}/message/create?receiver=${senderMember}&names=${sender}&label=${letter.typed.symbol}" title="回复"><i class="icon mdi mdi-mail-reply"></i></a>
                    <a class="action-cmd" href="javascript:;" title="标记为已读" data-handler="${ADMIN}/message/read" data-query="id:${letter.id}"><i class="icon mdi mdi-email-open"></i></a>
                    <a class="action-cmd" href="javascript:;" title="删除" data-handler="${ADMIN}/message/delete" data-query="id:${letter.id}"><i class="icon mdi mdi-delete"></i></a>
                    </c:if>
                </div>
              </div>
          </div>
          <div class="email-head-sender">
              <div class="date">${letter.entryDateTime}</div>
              <div class="sender">${sender} to ${receiver}</div>
          </div>
      </div>
      <div class="email-body" style="min-height:760px">${letter.content}</div>
  </body>
</html>