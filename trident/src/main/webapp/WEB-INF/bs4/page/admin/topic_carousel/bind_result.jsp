<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>轮播图关联记录</title>
    <style>.h186{height:186px;background-color:#868e96;margin:0}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">轮播图关联记录
              <small class="tools float-right">
                  <a href="${ADMIN}/topic/carousel/bind?carousel=${param.carousel}"><i class="icon mdi mdi-plus"></i>新增</a>
              </small>
          </h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/topic/carousel/">轮播图</a></li>
              <li class="breadcrumb-item active">关联记录</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!-- foreach card -->
              <c:forEach items="${rs}" var="boardCarousel">
              <div class="col-md-3 col-sm-6">
                  <div class="card">
                      <div class="card-header h186">&nbsp;</div>
                      <div class="card-body">
                          <h5 class="card-title">版块组: <c:out value="${boardCarousel.volumes.title}" default="-"/></h5>
                          <c:if test="${boardCarousel.board!=null}">
                          <h6 class="card-subtitle mb-2 text-muted">版块: ${boardCarousel.board.title}</h6>
                          </c:if>
                          <p class="card-text">开始日期:<forum:format value="${boardCarousel.entryDateTime}"/>, 结束日期: <forum:format value="${boardCarousel.expireDateTime}"/></p>
                          <a class="btn btn-primary action-cmd" 
                             role="button" 
                             href="javascript:;" 
                             data-handler="${ADMIN}/topic/carousel/bind/remove" data-query="carousel:${boardCarousel.carouselId},volumes:${boardCarousel.volumesId},board:${boardCarousel.boardId}">删除</a>
                      </div>
                  </div>
              </div>
              </c:forEach>
          </div>
      </div>
  </body>
</html>