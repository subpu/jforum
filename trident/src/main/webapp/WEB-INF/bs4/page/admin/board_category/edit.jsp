<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>版块关联话题类型</title>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">版块关联话题类型</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/board/">版块</a></li>
              <li class="breadcrumb-item active">General</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <div class="col-md-12">
                  <div class="card card-border-color card-border-color-primary">
                      <div class="card-header card-header-divider">版块关联话题类型</div>
                      <div class="card-body">
                          <c:if test="${not empty errors}">
                          <div role="alert" class="alert alert-danger alert-dismissible">
                              <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
                              <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                              <div class="message"><strong>oOps!</strong> ${errors} </div>
                          </div>
                          </c:if>
                          <c:set var="tmpArray" value="${form.category}"></c:set>
                          <form method="post" action="${ADMIN}/board/category/relative">
                          <div class="form-group row">
                              <label for="volumes" class="col-12 col-sm-3 col-form-label text-sm-right">版块组(卷)</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <select class="apo_select apo_cascade_parent_select" name="volumes" tabindex="1" data-lazy="board_lazy_select" data-active="${form.volumes}" data-handler="${ADMIN}/board/group/json" style="width:300px;">
                                      <option value="">选择版块组(卷)</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="board" class="col-12 col-sm-3 col-form-label text-sm-right">版块</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <select id="board_lazy_select" class="apo_lazy_select" name="board" tabindex="2" required="required" data-parent="${form.volumes}" data-active="${form.board}" data-handler="${ADMIN}/board/lazy.json" style="width:300px;">
                                      <option value="">选择版块</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="category" class="col-12 col-sm-3 col-form-label text-sm-right">话题类型</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <select class="apo_select" multiple="multiple" name="category" tabindex="3" data-active="${form.category}" data-handler="${ADMIN}/board/category/rawdata.json" style="width:300px;" required="required">
                                      <option value="">选择话题类型</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group row text-right">
                              <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
                                  <input type="hidden" name="record" value="${form.record}" />
                                  <input type="hidden" name="token" value="${form.token}" />
                                  <button type="submit" class="btn btn-space btn-primary" tabindex="4">提交</button>
                                  <button class="btn btn-space btn-secondary">取消</button>
                              </div>
                          </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>