<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>版主列表</title>
    <style>.card-header .tools{font-size:0.8em}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">版主</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/board/">版块</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!--Responsive table-->
              <div class="col-sm-12">
                  <div class="card card-table">
                      <div class="card-header">版主列表
                          <div class="tools">
                              <a href="${ADMIN}/board/moderator/edit?board=${paramBoard}&volume=${paramVolumes}"><i class="icon mdi mdi-plus"></i>新增</a>
                          </div>
                      </div>
                      <div class="card-body">
                          <div class="table-responsive noSwipe">
                              <table class="table table-striped table-hover">
                                  <thead>
                                      <tr>
                                          <th style="width:5%;">
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
                                              </label>
                                          </th>
                                          <th style="width:25%;">会员</th>
                                          <th style="width:10%;">版块组</th>
                                          <th style="width:10%;">版块</th>
                                          
                                          <th style="width:10%;">等级</th>
                                          <th style="width:10%;">大版主</th>
                                          <th style="width:10%;">状态</th>
                                          <th style="width:10%">任命日期</th>
                                          <th class="actions"></th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <!-- foreach item -->
                                      <c:forEach items="${rs}" var="moderator">
                                      <tr>
                                          <td>
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" name="id" class="custom-control-input" value="${moderator.id}"><span class="custom-control-label"></span>
                                              </label>
                                          </td>
                                          <td><a href="${ADMIN}/member/${moderator.memberId}.xhtml">${moderator.memberNickname}</a></td>
                                          <td><forum:outlet value="${moderator.volumes.title}" optional="-"/></td>
                                          <td><forum:outlet value="${moderator.board.title}" optional="-"/></td>
                                          
                                          <td>${moderator.level.title}</td>
                                          <td><forum:print value="${moderator.volumesMaster}" trueTitle="是" falseTitle="否"/></td>
                                          <td><forum:print value="${moderator.status}" trueTitle="可用" falseTitle="禁用"/></td>
                                          <td><forum:date value="${moderator.takeDateTime}"/></td>
                                          <td class="actions">
                                              <a class="btn btn-primary" role="button" href="${ADMIN}/board/moderator/edit?id=${moderator.id}">编辑</a>
                                              <div class="btn-group">
                                                  <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle">操作 <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                                                  <div role="menu" class="dropdown-menu">
                                                      <a href="${ADMIN}/board/moderator/permission?id=${moderator.id}" class="dropdown-item">权限</a>
                                                      <a href="${ADMIN}/board/moderator/roles?member=${moderator.memberId}" class="dropdown-item">角色历史</a>
                                                      <div class="dropdown-divider"></div>
                                                      <a href="javascript:;" class="dropdown-item action-cmd" data-handler="${ADMIN}/board/moderator/quit" data-query="member:${moderator.memberId},board:${moderator.boardId},volume:${moderator.volumesId}">卸任</a>
                                                  </div>
                                              </div>
                                          </td>
                                      </tr>
                                      </c:forEach>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>