<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>${moderator.names}角色变更历史</title>
    <style>.card-header .tools{font-size:0.8em}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">角色变更历史</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/board/">版块</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!--Responsive table-->
              <div class="col-sm-12">
                  <div class="card card-table">
                      <div class="card-header">${moderator.names}角色变更历史
                          <div class="tools">
                              &nbsp;
                          </div>
                      </div>
                      <div class="card-body">
                          <div class="table-responsive noSwipe">
                              <table class="table table-striped table-hover">
                                  <thead>
                                      <tr>
                                          <th style="width:5%;">
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
                                              </label>
                                          </th>
                                          <th style="width:10%;">版块组</th>
                                          <th style="width:10%;">版块</th>
                                          
                                          <th style="width:10%;">变更前</th>
                                          <th style="width:10%;">变更后</th>
                                          <th style="width:10%;">创建日期</th>
                                          <th style="width:10%;">状态</th>
                                      </tr>
                                  </thead>
                                  <tbody class="load-volumes-box load-board-box" data-volumes-source="${ADMIN}/board/group/list.json" data-board-source="${ADMIN}/board/list.json">
                                      <!-- foreach tr -->
                                      <c:forEach items="${rs}" var="roleHistory">
                                      <tr>
                                          <td>
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" name="id" class="custom-control-input" value="${roleHistory.id}"><span class="custom-control-label"></span>
                                              </label>
                                          </td>
                                          <td class="loadVolumes volumes-${roleHistory.volumesId}" data-volumes="${roleHistory.volumesId}">loading</td>
                                          <td class="loadBoard board-${roleHistory.boardId}" data-board="${roleHistory.boardId}">loading</td>
                                          
                                          <td>${roleHistory.originalRole.title}</td>
                                          <td>${roleHistory.investRole.title}</td>
                                          <td>${roleHistory.entryDateTime}</td>
                                          <td><forum:print value="${roleHistory.status}" trueTitle="可用" falseTitle="禁用"/></td>
                                      </tr>
                                      </c:forEach>
                                  </tbody>
                              </table>
                          </div>
                          <!-- 分页区 -->
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>