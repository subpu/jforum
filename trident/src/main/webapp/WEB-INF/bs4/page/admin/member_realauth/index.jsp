<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>会员实名认证</title>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">会员实名认证</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/member/${realauth.memberId}.xhtml">会员</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!--Responsive table-->
              <div class="col-sm-12">
                  <div class="card card-table">
                      <div class="card-header">会员实名认证
                          <div class="tools">
                          </div>
                      </div>
                      <div class="card-body">
                          <div class="table-responsive noSwipe">
                              <table class="table table-striped table-hover">
                                  <thead>
                                      <tr>
                                          <th style="width:5%;">
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
                                              </label>
                                          </th>
                                          <th style="width:20%;">真实姓名</th>
                                          <th style="width:17%;">出生年份</th>
                                          <th style="width:15%;">出生月份</th>
                                          <th style="width:10%;">出生日子</th>
                                          <th style="width:10%;">验证状态</th>
                                          <th style="width:10%;"></th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td>
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" name="id" class="custom-control-input" value="${realauth.id}"><span class="custom-control-label"></span>
                                              </label>
                                          </td>
                                          <td>${realauth.realname}</td>
                                          <td>${realauth.birthYear}</td>
                                          <td>${realauth.birthMonth}</td>
                                          <td>${realauth.birthDay}</td>
                                          <td><forum:print value="${realauth.passed}" trueTitle="通过" falseTitle="进行"/></td>
                                          <td class="actions">
                                              
                                          </td>
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>