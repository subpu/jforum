<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>${form.actionTitle}表情风格</title>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">${form.actionTitle}表情风格</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/smiley/theme/">表情风格</a></li>
              <li class="breadcrumb-item active">表单</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <div class="col-md-12">
                  <div class="card card-border-color card-border-color-primary">
                      <div class="card-header card-header-divider">${form.actionTitle}表情风格</div>
                      <div class="card-body">
                          <c:if test="${not empty errors}">
                          <div role="alert" class="alert alert-danger alert-dismissible">
                              <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
                              <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                              <div class="message"><strong>oOps!</strong> ${errors} </div>
                          </div>
                          </c:if>
                          <form method="post" action="${ADMIN}/smiley/theme/edit">
                          <div class="form-group row">
                              <label for="directNames" class="col-12 col-sm-3 col-form-label text-sm-right">目录名称</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <select class="apo_jsonp_select" name="directNames" tabindex="1" required="required" data-active="${form.directNames}" data-handler="${ADMIN}/smiley/theme/direct.jsonp" data-function="smileyCallFun" style="width:300px;">
                                      <option value="">选择表情所在的目录</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="label" class="col-12 col-sm-3 col-form-label text-sm-right">英文名称</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="label" type="text" class="form-control" value="${form.label}" required="required" tabindex="2"/>
                                  <span class="form-text text-muted">可选项,不填写将使用默认值</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="title" class="col-12 col-sm-3 col-form-label text-sm-right">中文名称</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="title" type="text" class="form-control" value="${form.title}" required="required" tabindex="3"/>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="ranking" class="col-12 col-sm-3 col-form-label text-sm-right">显示排序</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="ranking" type="number" class="form-control" value="${form.ranking}" required="required" tabindex="4"/>
                                  <span class="form-text text-muted">结果按升序排列.最小的先显示,最大的最后显示</span>
                              </div>
                          </div>

                          <div class="form-group row pt-1 pb-1">
                              <label for="status" class="col-12 col-sm-3 col-form-label text-sm-right">状态</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="status"<c:if test="${'1' eq form.status}"> checked="checked"</c:if> class="custom-control-input" value="1" tabindex="5"><span class="custom-control-label">可用</span>
                                  </label>
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="status"<c:if test="${'0' eq form.status}"> checked="checked"</c:if> class="custom-control-input" value="0" tabindex="6"><span class="custom-control-label">禁用</span>
                                  </label>
                              </div>
                          </div>
                          <div class="form-group row text-right">
                              <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
                                  <input type="hidden" name="record" value="${form.record}" />
                                  <input type="hidden" name="token" value="${form.token}" />
                                  <button type="submit" class="btn btn-space btn-primary" tabindex="7">提交</button>
                                  <button class="btn btn-space btn-secondary">取消</button>
                              </div>
                          </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>