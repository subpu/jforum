<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>${form.actionTitle}版主</title>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">${form.actionTitle}版主</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/board/">版块</a></li>
              <c:if test="${form.record > 0 }">
              <li class="breadcrumb-item"><a href="${ADMIN}/board/moderator/permission?id=${form.record}">版主权限</a></li>
              </c:if>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <div class="col-md-12">
                  <div class="card card-border-color card-border-color-primary">
                      <div class="card-header card-header-divider">${form.actionTitle}版主</div>
                      <div class="card-body">
                          <c:if test="${not empty errors}">
                          <div role="alert" class="alert alert-danger alert-dismissible">
                              <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
                              <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                              <div class="message"><strong>oOps!</strong> ${errors} </div>
                          </div>
                          </c:if>
                          <form method="post" action="${ADMIN}/board/moderator/edit">
                          <div class="form-group row">
                              <label for="volumesId" class="col-12 col-sm-3 col-form-label text-sm-right">版块组(卷)</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <select class="apo_select apo_cascade_parent_select form-control" name="volumesId" tabindex="1" required="required" data-lazy="board_lazy_select" data-active="${form.volumesId}" data-handler="${ADMIN}/board/group/json">
                                      <option value="">选择版块组(卷)</option>
                                  </select>
                                  <span class="form-text text-muted">可选项,不选默认为默认版块组(卷)</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="boardId" class="col-12 col-sm-3 col-form-label text-sm-right">版块</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <select id="board_lazy_select" class="apo_lazy_select form-control" name="boardId" tabindex="2" data-parent="${form.volumesId}" data-active="${form.boardId}" data-handler="${ADMIN}/board/lazy.json">
                                      <option value="">选择版块</option>
                                  </select>
                                  <span class="form-text text-muted">可选项,不选默认为选中版块组(卷)的版主</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="memberNames" class="col-12 col-sm-3 col-form-label text-sm-right">会员</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="memberNames" type="text" class="form-control" value="${form.memberNames}" required="required" tabindex="3" id="apo_member_lookup" placeholder="输入会员的登录帐号" />
                                  <span class="form-text text-muted">输入会员的登录帐号,支持字行的模糊提示</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="level" class="col-12 col-sm-3 col-form-label text-sm-right">等级</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <c:forEach items="${levelData}" var="level">
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="level"<c:if test="${level.key eq form.level}"> checked="checked"</c:if> class="custom-control-input" value="${level.key}"><span class="custom-control-label">${level.value}</span>
                                  </label>
                                  </c:forEach>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="status" class="col-12 col-sm-3 col-form-label text-sm-right">状态</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="status"<c:if test="${'1' eq form.status}"> checked="checked"</c:if> class="custom-control-input" value="1"><span class="custom-control-label">可用</span>
                                  </label>
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="status"<c:if test="${'0' eq form.status}"> checked="checked"</c:if> class="custom-control-input" value="0"><span class="custom-control-label">禁用</span>
                                  </label>
                              </div>
                          </div>
                          <div class="form-group row text-right">
                              <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
                                  <input type="hidden" name="record" value="${form.record}" />
                                  <input type="hidden" name="token" value="${form.token}" />
                                  <input type="hidden" name="memberId" id="memberId" value="${form.memberId}" />
                                  <button type="submit" class="btn btn-space btn-primary" tabindex="4">提交</button>
                                  <button class="btn btn-space btn-secondary">取消</button>
                              </div>
                          </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>