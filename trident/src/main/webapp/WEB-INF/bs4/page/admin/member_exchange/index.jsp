<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>最近的VIP会员</title>
    <style>.card-header .tools{font-size:0.8em}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">最近的VIP会员</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!--Responsive table-->
              <div class="col-sm-12">
                  <div class="card card-table">
                      <div class="card-header">最近的VIP会员
                          <div class="tools">
                              <a href="${ADMIN}/member/exchange/create" title="新增VIP会员" data-toggle="tooltip" data-placement="left"><i class="icon mdi mdi-plus"></i>新增</a>
                          </div>
                      </div>
                      <div class="row table-filters-container">
                          <div class="col-12 col-lg-12 col-xl-12">
                              <div class="row">
                                  <div class="col-12 col-lg-6 table-filters pb-0 pb-xl-4"><span class="table-filter-title">会员查询</span>
                                      <div class="filter-container">
                                          <form method="get" action="${ADMIN}/member/exchange/query">
                                              <label class="control-label"><span id="slider-value">UID</span></label>
                                              <input type="text" name="uid" class="form-control-sm form-control" placeholder="输入值后按回车开始查询">
                                          </form>
                                      </div>
                                  </div>
                                  <div class="col-12 col-lg-6 table-filters pb-0 pb-xl-4"><span class="table-filter-title">日期查询</span>
                                      <div class="filter-container">
                                          <form method="get" action="${ADMIN}/member/exchange/range">
                                              <div class="row">
                                                  <div class="col-5">
                                                      <label class="control-label">开始日期:</label>
                                                      <input type="text" name="start" class="form-control form-control-sm datetimepicker">
                                                  </div>
                                                  <div class="col-5">
                                                      <label class="control-label">结束日期:</label>
                                                      <input type="text" name="end" class="form-control form-control-sm datetimepicker">
                                                  </div>
                                                  <div class="col-2"><label class="control-label d-block">&nbsp;</label><button type="submit" class="btn btn-space btn-primary btn-lg">查询</button></div>
                                              </div>
                                          </form>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="card-body">
                          <div class="table-responsive noSwipe">
                              <table class="table table-striped table-hover">
                                  <thead>
                                      <tr>
                                          <th style="width:5%;">
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
                                              </label>
                                          </th>
                                          <th style="width:20%;">昵称</th>
                                          <th style="width:20%;">交易码</th>
                                          
                                          <th style="width:20%;">激活日期</th>
                                          <th style="width:10%;">有效日期</th>
                                          <th style="width:20%;">到期日期</th>
                                          <th style="width:10%;">状态</th>
                                          <th class="actions"></th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <!-- foreach item -->
                                      <c:forEach items="${rs}" var="exchange">
                                      <tr>
                                          <td>
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" name="id" class="custom-control-input" value="${exchange.id}"><span class="custom-control-label"></span>
                                              </label>
                                          </td>
                                          <td><a href="${ADMIN}/member/${exchange.memberId}.xhtml">${exchange.memberNickname}</a></td>
                                          <td><forum:outlet value="${exchange.serial}" optional="-"/></td>
                                          
                                          <td><forum:date value="${exchange.activeDateTime}"/></td>
                                          <td>${exchange.duration}(${exchange.durationUnit.title})</td>
                                          <td><forum:date value="${exchange.lapseDateTime}"/></td>
                                          
                                          <td><forum:print value="${exchange.status}" trueTitle="使用" falseTitle="结束"/></td>
                                          <td>
                                              <a class="btn btn-primary action-cmd" role="button" href="javascript:;" data-handler="${ADMIN}/member/exchange/expire" data-query="id:${exchange.id}">手动作废</a>
                                          </td>
                                      </tr>
                                      </c:forEach>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>