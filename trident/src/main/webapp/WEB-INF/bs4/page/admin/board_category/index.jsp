<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>${board.title}支持的话题类型</title>
    <style>.card-header .tools{font-size:0.8em}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">${board.title}支持的话题类型</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!--Responsive table-->
              <div class="col-sm-12">
                  <div class="card card-table">
                      <div class="card-header">
                          <h4 id="page-title">${board.title}的话题类型列表</h4>
                          <div class="tools">
                              <a href="${ADMIN}/board/category/relative?board=${param.board}&volume=${board.volumesId}"><i class="icon mdi mdi-link"></i>关联</a>
                          </div>
                      </div>
                      <div class="card-body">
                          <div role="alert" class="alert alert-primary alert-dismissible">
                              <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
                              <div class="icon"><span class="mdi mdi-info-outline"></span></div>
                              <div class="message"><strong>提示!</strong>举报和意见反馈等内置话题类型不会显示.</div>
                          </div>
                          <div class="table-responsive noSwipe">
                              <table class="table table-striped table-hover">
                                  <thead>
                                      <tr>
                                          <th style="width:5%;">
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
                                              </label>
                                          </th>
                                          <th style="width:20%;">名称</th>
                                          <th style="width:20%;">参数值</th>
                                          <th style="width:10%;">状态</th>
                                          <th class="actions"></th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <!-- foreach tr -->
                                      <c:forEach items="${rs}" var="topicCategory">
                                      <tr>
                                          <td>
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" name="id" class="custom-control-input" value="${topicCategory.id}"><span class="custom-control-label"></span>
                                              </label>
                                          </td>
                                          <td>${topicCategory.names}</td>
                                          <td>${topicCategory.value}</td>
                                          <td><forum:print value="${topicCategory.status}" trueTitle="可用" falseTitle="删除"/></td>
                                          <td class="actions">
                                              <a class="btn btn-primary action-cmd" role="button" href="javascript:;" data-handler="${ADMIN}/board/category/remove" data-query="category:${topicCategory.id},board:${param.board}">删除</a>
                                          </td>
                                      </tr>
                                      </c:forEach>
                                  </tbody>
                              </table>
                          </div>
                          <!-- 分页区 -->
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>