<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>会员等级</title>
    <style>.h186{height:186px;background-color:#868e96;margin:0}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">会员等级列表
              <small class="tools float-right">
                  <a href="${ADMIN}/member/level/edit"><i class="icon mdi mdi-plus"></i>新增</a>
              </small>
          </h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/member/">会员</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!-- foreach card -->
              <c:forEach items="${rs}" var="level">
              <div class="col-md-3 col-sm-6">
                  <div class="card">
                      <div class="card-header h186 text-center"><img src="<forum:image path="${level.imageAddr}"/>" class="rounded-circle" style="width:140px;height:140px"/></div>
                      <div class="card-body">
                          <h5 class="card-title">${level.names}</h5>
                          <h6 class="card-subtitle mb-2 text-muted"><forum:print value="${level.status}" trueTitle="可用" falseTitle="禁用"/></h6> 
                          <p class="card-text">${level.minScore} - ${level.score}</p>
                          <a class="btn btn-primary" role="button" href="${ADMIN}/member/level/edit?id=${level.id}">编辑</a>
                      </div>
                  </div>
              </div>
              </c:forEach>
          </div>
      </div>
  </body>
</html>