<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>统计信息概览</title>
    <style>.env-info dt,.env-info dd{margin:0;padding:0;display:inline-block}.env-info dt{width:30%}.env-info dd{width:70%}</style>
  </head>
  <body>
      <div class="row">
          <div class="col-md-12">
              <div class="card">
                  <div class="card-header pb-3">最近七日会员活跃统计</div>
                  <div class="card-body" data-raw='${rs}' id="sevenDayChart" style="height: 350px;">
                  </div>
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-12 col-lg-4">
              <div class="card">
                  <div class="card-header card-header-divider pb-3">设备</div>
                  <div class="card-body pt-5 progressBarChart" style="height: 250px;text-align:center" data-handler="${ADMIN}/recent/stats/device/json">
                  </div>
              </div>
          </div>
          <div class="col-12 col-lg-4">
              <div class="card">
                   <div class="card-header card-header-divider pb-3">ISP通信商</div>
                   <div class="card-body pt-5 statsDonutChart" style="height: 250px;text-align:center" data-handler="${ADMIN}/recent/stats/isp/json">
                   </div>
              </div>
          </div>
          <div class="col-12 col-lg-4">
              <div class="card">
                  <div class="card-header card-header-divider pb-3">区域</div>
                  <div class="card-body pt-5 statsBarChart" style="height: 250px;text-align:center" data-handler="${ADMIN}/recent/stats/province/json">
                  </div>
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-12 col-lg-4">
              <div class="card">
                <div class="card-header card-header-divider pb-3">VM运行时内存</div>
                <div class="card-body pt-5">
                    <div id="vm-gauge-chart" data-val="${hmp}"></div>
                </div>
              </div>
          </div>
          <div class="col-12 col-lg-4">
              <div class="card">
                  <div class="card-header card-header-divider pb-3">Java环境</div>
                  <div class="card-body pt-5">
                      <div class="row">
                          <div class="col-lg-4"><img src="${BASE}/static/img/java.png" style="width:158px"/></div>
                          <div class="col-lg-8 env-info">
                              <dl><dt>虚拟机</dt><dd>${javaEnv["vm"]}</dd></dl>
                              <dl><dt>供应商</dt><dd>${javaEnv["vm.vendor"]}</dd></dl>
                              <dl><dt>模式</dt><dd>${javaEnv["vm.mode"]}</dd></dl>
                              <dl><dt>Java版本 </dt><dd>${javaEnv["version"]}</dd></dl>
                              <dl><dt>规范版本 </dt><dd>${javaEnv["specification"]}</dd></dl>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-12 col-lg-4">
              <div class="card">
                  <div class="card-header card-header-divider pb-3">系统环境</div>
                  <div class="card-body pt-5">
                      <div class="row">
                          <div class="col-lg-4"><img src="${BASE}/static/img/linux.png" style="width:158px"/></div>
                          <div class="col-lg-8 env-info">
                              <dl><dt>操作系统</dt><dd>${osEnv["name"]}</dd></dl>
                              <dl><dt>系统版本</dt><dd>${osEnv["version"]}</dd></dl>
                              <dl><dt>系统架构</dt><dd>${osEnv["arch"]}</dd></dl>
                              <dl><dt>CPU Endian</dt><dd>${osEnv["cpu.endian"]}</dd></dl>
                              <dl><dt>CPU Processors</dt><dd>${osEnv["cpu.processor"]}</dd></dl>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>