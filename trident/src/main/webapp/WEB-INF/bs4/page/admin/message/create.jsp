<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>新消息</title>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">新消息</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item active">表单</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <div class="col-md-12">
                  <div class="card card-border-color card-border-color-primary">
                      <div class="card-header card-header-divider">新消息</div>
                      <div class="card-body">
                          <c:if test="${not empty errors}">
                          <div role="alert" class="alert alert-danger alert-dismissible">
                              <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
                              <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                              <div class="message"><strong>oOps!</strong> ${errors} </div>
                          </div>
                          </c:if>
                          <form method="post" action="${ADMIN}/message/create">
                          <div class="form-group row">
                              <label for="uid" class="col-12 col-sm-3 col-form-label text-sm-right">收件人/UID</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="uid" id="suggest-uid" type="text" class="form-control" value="${form.uid}" placeholder="以u开头后跟会员的ID" data-handler="${ADMIN}/member/detection/uid" required="required" tabindex="4"/>
                                  <span class="form-text text-muted">以u开头后跟会员的ID,称之为uid; 例如:u1001. 不填写目标会员即为公告所有在线会员</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="label" class="col-12 col-sm-3 col-form-label text-sm-right">类型</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <select class="apo_select form-control" name="label" data-active="${form.label}" data-handler="${ADMIN}/message/label/json" required="required" tabindex="1">
                                      <option value="0">选择消息类型</option>
                                  </select>
                                  <span class="form-text text-muted">若类型为公告.只在当日可见</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="title" class="col-12 col-sm-3 col-form-label text-sm-right">主题</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="title" type="text" class="form-control" value="${form.title}" required="required" tabindex="2"/>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="content" class="col-12 col-sm-3 col-form-label text-sm-right">内容</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <textarea id="content" name="content" rows="5" class="form-control" required="required" tabindex="3">${form.content}</textarea>
                              </div>
                          </div>
                          <div class="form-group row text-right">
                              <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
                                  <input type="hidden" name="token" value="${form.token}" />
                                  <button type="submit" class="btn btn-space btn-primary" tabindex="6">发送</button>
                                  <button class="btn btn-space btn-secondary">取消</button>
                              </div>
                          </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>