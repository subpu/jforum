<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>编辑话题</title>
    <style>
    .label-info {background-color: #4285f4;}
    .label {
        display: inline;
        padding: .2em .6em .3em;
        line-height: 1;
        color: #fff;
        text-align: center;
        white-space: nowrap;
        vertical-align: baseline;
        border-radius: .25em;
    }
    .nav-tabs{border-style:none}
    #default li{width:50px;height:50px;text-align:center;line-height:50px;}
    #smiley-pic-collection img{cursor:pointer}
    .tools {position: absolute;right: 1px;top: 1px;width: 25px;line-height: 25px;text-align: center;background-color: #f1f1f1;}
    #smiley-pic-collection{position: absolute;bottom: 29px;border: 1px solid #ddd;background-color: #F8f8f8;display:none;opacity: 0.8;}
    .list-inline-item:hover{background-color:#666;cursor:pointer}
    </style>
  </head>
  <body>
      <div class="email-head">
          <c:if test="${not empty errors}">
          <div class="email-head-title">${errors}<span class="icon mdi mdi-alert-polygon"></span></div>
          </c:if>
          <c:if test="${empty errors}">
          <div class="email-head-title">编辑话题<span class="icon mdi mdi-edit"></span></div>
          </c:if>
      </div>
      <form action="${ADMIN}/topic/edit" method="post">
      <div class="email-compose-fields">
          <div class="subject">
              <div class="form-group row pt-2">
                  <label class="col-md-1 control-label">主题</label>
                  <div class="col-md-11">
                      <input name="title" type="text" class="form-control" value="${form.title}" tabindex="1" required="required" />
                  </div>
              </div>
          </div>
          <div class="to">
              <div class="form-group row pt-0">
                  <label class="col-md-1 control-label">关键词:</label>
                  <div class="col-md-11">
                      <input name="words" type="text" class="form-control" data-role="tagsinput" tabindex="2" value="${form.words}"/>
                  </div>
              </div>
          </div>
      </div>
      <div class="email editor">
            <div id="content-editor"><textarea id="content" name="content" rows="5" tabindex="3" required="required" class="richeditor" data-query="height:450px,width:100%,upload:/upload/ckeditor">${form.content}</textarea></div>
            <div class="row">
                <div class="smiley-anchor col-md-8 col-sm-9" style="margin-top: 15px;" id="smiley-collection" data-handler="${ADMIN}/smiley/pic/list.json">
                    <ul class="nav nav-tabs" role="tablist" id="smiley-theme-set">
                        <c:forEach items="${smileyThemes}" var="smileyTheme">
                        <li class="nav-item"><a class="nav-link" id="${smileyTheme.key}-anchor" data-toggle="tab" role="tab" aria-controls="${smileyTheme.key}" href="javascript:;">${smileyTheme.value}</a></li>
                        </c:forEach>
                    </ul>
                    <div class="tab-content" id="smiley-pic-collection">
                        <div class="tools"><a href="javascript:;"><span class="icon mdi mdi-close"></span></a></div>
                        <c:forEach items="${smileyThemes}" var="st">
                        <div class="tab-pane" id="${st.key}" role="tabpanel" aria-labelledby="${st.key}-anchor"><img src="${FRONT}/static/img/loading.gif" class="load-image"/></div>
                        </c:forEach>
                    </div>
                </div>
                <div class="form-group col-md-4 col-sm-3">
                    <input type="hidden" name="record" value="${form.record}" />
                    <input type="hidden" name="token" value="${form.token}" />
                    <button type="submit" class="btn btn-primary btn-space" tabindex="4"><i class="icon s7-mail"></i> 提交</button>
                    <button type="button" class="btn btn-secondary btn-space"><i class="icon s7-close"></i> 取消</button>
                </div>
            </div>
      </div>
      </form>
  </body>
</html>