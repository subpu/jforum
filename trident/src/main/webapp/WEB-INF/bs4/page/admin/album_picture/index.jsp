<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>话题图片列表</title>
    <style>.card-header .tools{font-size:0.8em}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">话题图片列表</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/topic/${param.topic}.xhtml">话题</a></li>
              <li class="breadcrumb-item active">图片列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!--Responsive table-->
              <div class="col-sm-12">
                  <div class="card">
                      <div class="card-header">
                          <h4 id="page-title">话题图片列表</h4>
                          <div class="tools">
                              <a href="${ADMIN}/topic/album/picture/add?topic=${param.topic}&album=${album.id}" title="上传话题图片"><i class="icon mdi mdi-plus"></i>新增</a>
                          </div>
                      </div>
                      <div class="card-body">
                          <div class="gallery-container">
                              <c:forEach items="${rs}" var="albumPicture">
                              <div class="item">
                                  <div class="photo">
                                      <div class="img">
                                          <img src="<forum:thumb path="${albumPicture.link}"/>" alt="Gallery Image" />
                                          <div class="over">
                                              <div class="info-wrapper">
                                                  <div class="info">
                                                      <div class="title">状态: <forum:print value="${albumPicture.status}" trueTitle="可用" falseTitle="删除"/></div>
                                                      <div class="date">封面: <forum:print value="${albumPicture.cover}" trueTitle="是" falseTitle="否"/></div>
                                                      <div class="description">${albumPicture.caption}</div>
                                                      <div class="func"><a href="javascript:;" title="设置为封面" class="action-cmd" data-handler="${ADMIN}/topic/album/cover" data-query="cover:${albumPicture.id},album:${albumPicture.album.id}"><i class="icon mdi mdi-local-see"></i></a></div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              </c:forEach>
                          </div>
                          <!-- 分页区 -->
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>