<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>${form.actionTitle}子栏目</title>
    <style>#upload-section .custom-checkbox{padding-left:0!important}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">${form.actionTitle}子栏目</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/section/">栏目</a></li>
              <li class="breadcrumb-item active">表单</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <div class="col-md-12">
                  <div class="card card-border-color card-border-color-primary">
                      <div class="card-header card-header-divider">${form.actionTitle}子栏目</div>
                      <div class="card-body">
                          <c:if test="${not empty errors}">
                          <div role="alert" class="alert alert-danger alert-dismissible">
                              <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
                              <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                              <div class="message"><strong>oOps!</strong> ${errors} </div>
                          </div>
                          </c:if>
                          <form method="post" action="${ADMIN}/section/term/edit" class="parsley-form" data-parsley-validate>
                          <div class="form-group row">
                              <label for="title" class="col-12 col-sm-3 col-form-label text-sm-right">名称</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="title" type="text" class="form-control" value="${form.title}" data-parsley-required tabindex="1"/>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="direct" class="col-12 col-sm-3 col-form-label text-sm-right">目录名称</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="direct" type="text" class="form-control" value="${form.direct}" placeholder="需要唯一;第少4个字符,最多19个字符" data-parsley-required data-parsley-type="alphanum" data-parsley-length="[4, 20]" data-parsley-remote="${ADMIN}/section/term/direct/unique?record=${form.record}" data-parsley-remote-message="目录名已经存在" tabindex="2"/>
                                  <span class="form-text text-muted">只可以使用26个英文字母或数字组合.需要唯一</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="section" class="col-12 col-sm-3 col-form-label text-sm-right">父栏目</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <select class="apo_select form-control" name="section" data-active="${form.section}" data-handler="${ADMIN}/section/list/json" data-parsley-required tabindex="3">
                                      <option value="0">选择父栏目</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="description" class="col-12 col-sm-3 col-form-label text-sm-right">描述</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <textarea name="description" class="form-control" data-parsley-required tabindex="4">${form.description}</textarea>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="entityStatus" class="col-12 col-sm-3 col-form-label text-sm-right">状态</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <c:forEach items="${boardStatusData}" var="boardStatus">
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="entityStatus"<c:if test="${boardStatus.key eq form.entityStatus}"> checked="checked"</c:if> class="custom-control-input" value="${boardStatus.key}"><span class="custom-control-label">${boardStatus.value}</span>
                                  </label>
                                  </c:forEach>
                              </div>
                          </div>
                          <div class="form-group row text-right">
                              <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
                                  <input type="hidden" name="record" value="${form.record}" />
                                  <input type="hidden" name="token" value="${form.token}" />
                                  <button type="submit" class="btn btn-space btn-primary" tabindex="5">提交</button>
                                  <button class="btn btn-space btn-secondary">取消</button>
                              </div>
                          </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>