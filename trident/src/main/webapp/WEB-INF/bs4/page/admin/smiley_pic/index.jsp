<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>表情图片</title>
    <style>.card-header .tools{font-size:0.8em}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">表情图片</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/smiley/theme/">表情风格</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <div class="col-12">
                  <div class="card">
                      <div class="card-header card-header-divider">${theme.title}风格<span class="card-subtitle">表情图片列表</span></div>
                      <div class="card-body mt-3">
                          <div class="row">
                              <!-- foreach -->
                              <c:forEach items="${rs}" var="smileyPictureNotes">
                              <div class="col-sm-6 col-md-2 col-lg-2">
                                  <div class="icon-container">
                                      <div class="icon" style="width:auto;height:auto">
                                          <img src="${smileyPictureNotes.url}" />
                                      </div>
                                      <span class="icon-class"><a href="javascript:;" data-name="${smileyPictureNotes.fileName}" data-theme="${theme.id}" class="edit_smiley_pic">${smileyPictureNotes.note}</a></span>
                                  </div>
                              </div>
                              </c:forEach>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>