<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>查看回复内容</title>
  </head>
  <body>
      <div class="email-head">
          <div class="email-head-subject">
              <div class="title">
                <a href="javascript:;" class="active">
                    <span class="icon mdi <mda:print value='${posts.reply}' trueTitle='mdi-comment-outline' falseTitle='mdi-account'/> "></span>
                </a> 
                <span>查看回复内容</span> 
                <div class="icons">
                    <c:if test="${posts.status}">
                    <a href="javascript:;" title="删除" class="action-cmd" data-handler="${ADMIN}/topic/posts/remove" data-query="id:${posts.id}"><span class="icon mdi mdi-delete"></span></a>
                    </c:if>
                </div>
              </div>
          </div>
          <div class="email-head-sender">
              <div class="date"><span class="icon mdi mdi-hourglass-alt"></span><forum:date value="${posts.modifyDateTime}"/></div>
              <div class="sender">${posts.floorNumber}楼 &nbsp; | &nbsp;${posts.memberNickname} &nbsp; <forum:date value="${posts.entryDateTime}"/></div>
          </div>
      </div>
      <div class="email-body" style="min-height:760px">${posts.content}</div>
  </body>
</html>