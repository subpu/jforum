<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>轮播图的幻灯片</title>
    <style>.card-header .tools{font-size:0.8em}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">轮播图的幻灯片列表</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/topic/carousel/">轮播图</a></li>
              <li class="breadcrumb-item active">幻灯片列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!--Responsive table-->
              <div class="col-sm-12">
                  <div class="card">
                      <div class="card-header">
                          <h4 id="page-title">幻灯片列表</h4>
                          <div class="tools">
                              <a href="${ADMIN}/topic/carousel/slide/edit?carousel=${param.carousel}" title="上传幻灯片图片"><i class="icon mdi mdi-plus"></i>新增</a>
                          </div>
                      </div>
                      <div class="card-body">
                          <div class="gallery-container">
                              <c:forEach items="${rs}" var="topicCarouselSlide">
                              <div class="item">
                                  <div class="photo">
                                      <div class="img">
                                          <img src="<forum:thumb path="${topicCarouselSlide.imageAddr}"/>" alt="Gallery Image" />
                                          <div class="over">
                                              <div class="info-wrapper">
                                                  <div class="info">
                                                      <div class="title">${topicCarouselSlide.title}</div>
                                                      <div class="date">状态: <forum:print value="${topicCarouselSlide.status}" trueTitle="可用" falseTitle="删除"/></div>
                                                      
                                                      <div class="func">
                                                          <a href="javascript:;" title="删除幻灯片" class="action-cmd" data-handler="${ADMIN}/topic/carousel/slide/remove" data-query="id:${topicCarouselSlide.id},carousel:${topicCarouselSlide.topicCarouselId}"><i class="icon mdi mdi-delete"></i></a>
                                                          <a href="${ADMIN}/topic/carousel/slide/edit?id=${topicCarouselSlide.id}" title="编辑幻灯片"><i class="icon mdi mdi-edit"></i></a>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              </c:forEach>
                          </div>
                          <!-- 分页区 -->
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>