<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>话题标签</title>
    <style>.card-header .tools{font-size:0.8em}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">话题标签</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!--Responsive table-->
              <div class="col-sm-12">
                  <div class="card card-table">
                      <div class="card-header">
                          <h4 id="page-title" data-handler="${ADMIN}/topic/title" data-query="id:${param.topic}" data-format="#%T%#的标签列表">loading</h4>
                          <div class="tools">
                              
                          </div>
                      </div>
                      <div class="card-body">
                          <div class="table-responsive noSwipe">
                              <table class="table table-striped table-hover">
                                  <thead>
                                      <tr>
                                          <th style="width:5%;">
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
                                              </label>
                                          </th>
                                          <th style="width:20%;">词语</th>
                                          <th style="width:20%;">词频</th>
                                          <th style="width:10%;">状态</th>
                                          <th class="actions"></th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <!-- foreach tr -->
                                      <c:forEach items="${rs}" var="tagObj">
                                      <tr>
                                          <td>
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" name="id" class="custom-control-input" value="${tagObj.id}"><span class="custom-control-label"></span>
                                              </label>
                                          </td>
                                          <td>${tagObj.names}</td>
                                          <td>${tagObj.rates}</td>
                                          <td><forum:print value="${tagObj.status}" trueTitle="可用" falseTitle="删除"/></td>
                                          <td class="actions">
                                              <a class="btn btn-primary" role="button" href="${ADMIN}/topic/tag/create?topic=${param.topic}">增加</a>
                                              <a class="btn btn-secondary action-cmd" role="button" href="javascript:;" data-handler="${ADMIN}/topic/tag/delete" data-query="id:${tagObj.id},topic:${param.topic}">删除</a>
                                          </td>
                                      </tr>
                                      </c:forEach>
                                  </tbody>
                              </table>
                          </div>
                          <!-- 分页区 -->
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>