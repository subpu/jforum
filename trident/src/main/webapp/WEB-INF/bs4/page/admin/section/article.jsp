<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>最近的文章</title>
    <style>.card-header .tools{font-size:0.8em}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">最近的文章</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <!--Responsive table-->
              <div class="col-sm-12">
                  <div class="card card-table">
                      <div class="card-header">最近的文章
                          <div class="tools">
                              <a href="${ADMIN}/section/article/publish"><i class="icon mdi mdi-plus"></i>新增</a>
                          </div>
                      </div>
                      <div class="card-body">
                          <div class="table-responsive noSwipe">
                              <table class="table table-striped table-hover">
                                  <thead>
                                      <tr>
                                          <th style="width:5%;">
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" class="custom-control-input"><span class="custom-control-label"></span>
                                              </label>
                                          </th>
                                          <th style="width:5%;">类别</th>
                                          <th style="width:10%;">子栏目</th>
                                          <th style="width:30%;">主题</th>
                                          <th style="width:10%;">作者</th>
                                          <th style="width:10%;">创建日期</th>
                                          <th style="width:10%;">状态</th>
                                          <th class="actions"></th>
                                      </tr>
                                  </thead>
                                  <tbody class="load-board-box" data-board-source="${ADMIN}/section/term/list/json">
                                      <!-- foreach tr -->
                                      <c:forEach items="${rs}" var="article">
                                      <tr>
                                          <td>
                                              <label class="custom-control custom-control-sm custom-checkbox">
                                                  <input type="checkbox" name="id" class="custom-control-input" value="${article.id}"><span class="custom-control-label"></span>
                                              </label>
                                          </td>
                                          <td><forum:outlet value="${article.topicCategoryName}" optional="普通"/></td>
                                          <td class="loadBoard board-${article.boardId}" data-board="${article.boardId}">loading</td>
                                          <td><a href="${ADMIN}/topic/${article.id}.xhtml" target="_blank">${article.title}</a></td>
                                          <td><a href="${ADMIN}/member/${article.memberId}.xhtml">${article.memberNickname}</a></td>
                                          <td><forum:date value="${article.entryDateTime}"/></td>
                                          <td>${article.status.title}</td>
                                          <td class="actions">
                                              <a class="btn btn-primary" role="button" href="${ADMIN}/section/article/modify?id=${article.id}">编辑</a>
                                          </td>
                                      </tr>
                                      </c:forEach>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>