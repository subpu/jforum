<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>欢迎您使用JForum</title>
  </head>
  <body>
      <div class="row invoice">
          <div class="col-sm-12">
              <div>
                  <h1>JForum <p class="lead">一个小论坛, 一个Java单体应用, 一个SSJ项目, 一个模块化项目</p></h1>
              </div>
              <div class="row">
                  <div class="col-sm-7">
                      <div class="">
                          <div class="card-header">环境需求</div>
                          <div class="card-body">
                              <ul class="list-unstyled">
                                  <li>JDK or JVM 1.8.0_?+</li>
                                  <li>Tomcat 8.5.37+</li>
                                  <li>Nginx 1.17.9+</li>
                                  <li>Mysql 5.5+</li>
                              </ul>
                          </div>
                      </div>
                      <div class="">
                          <div class="card-header">Tomcat 布署</div>
                          <div class="card-body">
                              <ul class="list-unstyled">
                                  <li><a href="https://gitee.com/apobates/jforum/wikis/tomcat%208+%E7%9A%84%E9%85%8D%E7%BD%AE%E6%96%87%E4%BB%B6?sort_id=2075924">tomcat 8+的配置文件</a></li>
                                  <li><a href="https://gitee.com/apobates/jforum/wikis/Nginx+Tomcat%208+%E7%BB%84%E5%90%88%E7%9A%84%E5%B8%83%E7%BD%B2%E9%85%8D%E7%BD%AE?sort_id=2075953">Nginx+Tomcat 8+组合的布署配置</a></li>
                              </ul>
                          </div>
                      </div>
                      <div class="">
                          <div class="card-header">二次开发</div>
                          <div class="card-body">
                              <ul class="list-unstyled">
                                  <li><a href="https://gitee.com/apobates/jforum/wikis/%E5%A6%82%E4%BD%95%E5%9C%A8eclipse%E4%B8%AD%E8%BF%98%E5%8E%9F%E9%A1%B9%E7%9B%AE?sort_id=2160164">如何在eclipse中还原项目</a></li>
                                  <li><a href="https://gitee.com/apobates/jforum/wikis/%E5%A6%82%E4%BD%95%E5%9C%A8NetBeans%20%E4%B8%AD%E8%BF%98%E5%8E%9F%E9%A1%B9%E7%9B%AE?sort_id=2171476">如何在NetBeans 中还原项目</a></li>
                              </ul>
                          </div>
                      </div>
                      <div class="">
                          <div class="card-header">最后</div>
                          <div class="card-body">
                              <ul class="list-unstyled">
                                  <li>项目在开源中国的码云完全开源, 项目网址:<a href="https://gitee.com/apobates/jforum">xiaofanku / jforum</a></li>
                                  <li>若您使用的JDK基于下一个长期支持的版本可用使用基于JDK11的jforum2, 项目网址:<a href="https://gitee.com/apobates/jforum2"> xiaofanku / jforum2</a></li>
                              </ul>
                          </div>
                      </div>
                  </div>
                  <div class="col-sm-5">
                      <div class="">
                          <div class="card-header card-header-divider">初始化组件</div>
                          <div class="card-body">
                              <ul class="list-unstyled">
                                  <li class="p-1 list-inline">系统会记录下会员的每个操作并生成操作记录,通过<a href="${ADMIN}/score/role/">积分规则</a>为不同的操作记录打分</li>
                                  <li class="p-1 list-inline">通过<a href="${ADMIN}/member/level/">等级定义</a>生成不同的等级,以此来为会员的所有操作记录计算总分,得出会员当前的等级</li>
                                  <li class="p-1 list-inline"><a href="${ADMIN}/member/protect/">帐号保护</a>来限制会员注册时使用的登录帐号, 系统内置了一些帐号名称。这些帐号名称您不需要创建</li>
                              </ul>
                          </div>
                      </div>
                      
                      <div class="">
                          <div class="card-header card-header-divider">版块组/版块</div>
                          <div class="card-body ">
                              <ul class="list-unstyled">
                                  <li class="p-1 list-inline">系统的一级分类为:<a href="${ADMIN}/board/group/">版块组</a>, 系统内置了一个默认版块组。版块组包含系统的二级分类: <a href="${ADMIN}/board/">版块</a></li>
                                  <li class="p-1 list-inline"><a href="${ADMIN}/board/">版块</a>中包含了会员发布的 <a href="${ADMIN}/topic/">主题或话题</a>，每个话题关联一个<a href="${ADMIN}/topic/category/">类型</a>称为话题类型。系统内置的话题类型包括:意见反馈和投拆</li>
                                  <li class="p-1 list-inline">通过将话题类型绑定到版块中.可以控制哪个版块产生哪种类型的话题，需要将意见反馈和投拆绑定到某一版块上. 否则意见反馈和投拆将无法保存</li>
                                  <li class="p-1 list-inline"><a href="${ADMIN}/board/">版块</a>关联一个版块配置文件,用以控制版块的显示, 话题的相关设置</li>
                              </ul>
                          </div>
                      </div>
                      
                      <div class="">
                          <div class="card-header card-header-divider">话题/主题</div>
                          <div class="card-body">
                              <ul class="list-unstyled">
                                  <li class="p-1 list-inline">话题可以关联一个话题<a href="${ADMIN}/topic/album/">像册</a>,像册中有封面图片同时包含一至多张图片. 一般在话题发布时从内容中提取并生成. 也可以通过管理后台手动添加</li>
                                  <li class="p-1 list-inline">话题关联一至多个<a href="${ADMIN}/topic/tag/">标签</a>，搜索时通过标签计算相关性. 一般在话题发布时从内容中提取并生成. 也可以通过管理后台手动添加</li>
                                  <li class="p-1 list-inline">每个话题关联一个话题配置文件,用以控制话题的显示, 回复的相关设置</li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>