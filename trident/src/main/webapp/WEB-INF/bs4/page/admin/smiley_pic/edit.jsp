<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>${form.actionTitle}表情图片</title>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">${form.actionTitle}表情图片</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/smiley/theme/">表情风格</a></li>
              <li class="breadcrumb-item active">表单</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <div class="col-md-12">
                  <div class="card card-border-color card-border-color-primary">
                      <div class="card-header card-header-divider">${form.actionTitle}表情图片</div>
                      <div class="card-body">
                          <c:if test="${not empty errors}">
                          <div role="alert" class="alert alert-danger alert-dismissible">
                              <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
                              <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                              <div class="message"><strong>oOps!</strong> ${errors} </div>
                          </div>
                          </c:if>
                          <form method="post" action="${ADMIN}/smiley/pic/edit">
                          <div class="form-group row">
                              <label for="themeDirect" class="col-12 col-sm-3 col-form-label text-sm-right">表情风格</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <select class="apo_select" id="smiley_theme_select" name="themeDirect" tabindex="1" required="required" data-active="${form.themeDirect}" data-handler="${ADMIN}/smiley/theme/json" style="width:300px;">
                                      <option value="">选择表情风格</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="fileNames" class="col-12 col-sm-3 col-form-label text-sm-right">图片文件</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <select id="smiley_pic_jsonp_select" class="select2" name="fileNames" tabindex="2" required="required" data-active="${form.fileNames}" data-handler="${SBS}/smiley/pic/jsonp" data-function="smileyCallFun" style="width:300px;">
                                      <option value="">选择表情图片</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group row pt-3">
                              <label class="col-12 col-sm-3 col-form-label text-sm-right pt-4">图片预览</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <div class="form-check form-check-inline">
                                      <label class="custom-control custom-radio custom-control-inline">
                                          <img src="${BASE}/static/img/140x140.png" alt="Placeholder" id="smiley_pic_preview" class="rounded-circle mr-2 mb-2" data-front="${SBS}/${smileyPath}"/>
                                      </label>
                                  </div>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="description" class="col-12 col-sm-3 col-form-label text-sm-right">语义</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="description" type="text" class="form-control" value="${form.description}" required="required" tabindex="3"/>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="ranking" class="col-12 col-sm-3 col-form-label text-sm-right">显示排序</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="ranking" type="number" class="form-control" value="${form.ranking}" required="required" tabindex="4"/>
                              </div>
                          </div>

                          <div class="form-group row pt-1 pb-1">
                              <label for="status" class="col-12 col-sm-3 col-form-label text-sm-right">状态</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="status"<c:if test="${'1' eq form.status}"> checked="checked"</c:if> class="custom-control-input" value="1" tabindex="5"><span class="custom-control-label">可用</span>
                                  </label>
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="status"<c:if test="${'0' eq form.status}"> checked="checked"</c:if> class="custom-control-input" value="0" tabindex="6"><span class="custom-control-label">禁用</span>
                                  </label>
                              </div>
                          </div>
                          <div class="form-group row text-right">
                              <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
                                  <input type="hidden" name="record" value="${form.record}" />
                                  <input type="hidden" name="token" value="${form.token}" />
                                  <button type="submit" class="btn btn-space btn-primary" tabindex="7">提交</button>
                                  <button class="btn btn-space btn-secondary">取消</button>
                              </div>
                          </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>