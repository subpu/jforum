<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>${form.actionTitle}栏目</title>
    <style>#upload-section .custom-checkbox{padding-left:0!important}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">${form.actionTitle}栏目</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/section/">栏目</a></li>
              <li class="breadcrumb-item active">表单</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <div class="col-md-12">
                  <div class="card card-border-color card-border-color-primary">
                      <div class="card-header card-header-divider">${form.actionTitle}栏目</div>
                      <div class="card-body">
                          <c:if test="${not empty errors}">
                          <div role="alert" class="alert alert-danger alert-dismissible">
                              <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
                              <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                              <div class="message"><strong>oOps!</strong> ${errors} </div>
                          </div>
                          </c:if>
                          <form method="post" action="${ADMIN}/section/edit" class="parsley-form" data-parsley-validate>
                          <div class="form-group row">
                              <label for="title" class="col-12 col-sm-3 col-form-label text-sm-right">名称</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="title" type="text" class="form-control" value="${form.title}" data-parsley-required tabindex="1"/>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="direct" class="col-12 col-sm-3 col-form-label text-sm-right">目录名称</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="direct" type="text" class="form-control" value="${form.direct}" placeholder="需要唯一;第少4个字符,最多19个字符" data-parsley-required data-parsley-type="alphanum" data-parsley-length="[4, 20]" data-parsley-remote="${ADMIN}/section/direct/unique?record=${form.record}" data-parsley-remote-message="目录名已经存在" tabindex="2" />
                                  <span class="form-text text-muted">在模板目录中的文件名,不是根目录下的文件名.系统内置了help示例模板.只可以使用26个英文字母或数字组合.需要唯一</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="description" class="col-12 col-sm-3 col-form-label text-sm-right">描述</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <textarea name="description" class="form-control" data-parsley-required tabindex="3">${form.description}</textarea>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="status" class="col-12 col-sm-3 col-form-label text-sm-right">状态</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="status"<c:if test="${'1' eq form.status}"> checked="checked"</c:if> class="custom-control-input" value="1" tabindex="4"><span class="custom-control-label">可用</span>
                                  </label>
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="status"<c:if test="${'0' eq form.status}"> checked="checked"</c:if> class="custom-control-input" value="0" tabindex="5"><span class="custom-control-label">禁用</span>
                                  </label>
                              </div>
                          </div>
                          <div class="form-group row text-right">
                              <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
                                  <input type="hidden" name="record" value="${form.record}" />
                                  <input type="hidden" name="token" value="${form.token}" />
                                  <button type="submit" class="btn btn-space btn-primary" tabindex="6">提交</button>
                                  <button class="btn btn-space btn-secondary">取消</button>
                              </div>
                          </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>