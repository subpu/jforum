<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>新增社区经理</title>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">新增社区经理</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item active">表单</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <div class="col-md-12">
                  <div class="card card-border-color card-border-color-primary">
                      <div class="card-header card-header-divider">新增社区经理</div>
                      <div class="card-body">
                          <c:if test="${not empty errors}">
                          <div role="alert" class="alert alert-danger alert-dismissible">
                              <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
                              <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                              <div class="message"><strong>oOps!</strong> ${errors} </div>
                          </div>
                          </c:if>
                          <form method="post" action="${ADMIN}/member/create">
                          <div class="form-group row">
                              <label for="names" class="col-12 col-sm-3 col-form-label text-sm-right">帐号</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="names" type="text" class="form-control" value="${form.names}" required="required" tabindex="1"/>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="nickname" class="col-12 col-sm-3 col-form-label text-sm-right">昵称</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="nickname" type="text" class="form-control" value="${form.nickname}" tabindex="2"/>
                                  <span class="form-text text-muted">可选项,不填写将使用帐号的值</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="pswd" class="col-12 col-sm-3 col-form-label text-sm-right">密码</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="pswd" type="password" class="form-control" value="" required="required" tabindex="3"/>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="role" class="col-12 col-sm-3 col-form-label text-sm-right">角色</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <select class="apo_select form-control" name="role" tabindex="4" data-active="${form.role}" data-handler="${ADMIN}/member/role/json">
                                      <option value="0">选择角色</option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group row text-right">
                              <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
                                  <input type="hidden" name="record" value="${form.record}" />
                                  <input type="hidden" name="token" value="${form.token}" />
                                  <button type="submit" class="btn btn-space btn-primary" tabindex="5">提交</button>
                                  <button class="btn btn-space btn-secondary">取消</button>
                              </div>
                          </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>