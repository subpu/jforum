<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>栏目列表</title>
    <style>.h186{height:186px;background-color:#868e96;margin:0}</style>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">栏目列表
              <small class="tools float-right">
                  <a href="${ADMIN}/section/edit"><i class="icon mdi mdi-plus"></i>新增</a>
              </small>
          </h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item active">列表</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div role="alert" class="alert alert-contrast alert-primary alert-dismissible">
              <div class="icon"><span class="mdi mdi-info-outline"></span></div>
              <div class="message">
                  <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button><strong>说明:</strong> 栏目是一种特殊化的版块组.
              </div>
          </div>
          <div class="row">
              <!-- foreach card -->
              <c:forEach items="${rs}" var="sectionObj">
              <div class="col-md-3 col-sm-6">
                  <div class="card">
                      <div class="card-header h186">&nbsp;</div>
                      <div class="card-body">
                          <h5 class="card-title">${sectionObj.title}</h5>
                          <h6 class="card-subtitle mb-2 text-muted"><span class="text-warning mdi mdi-folder"></span> ${sectionObj.directoryNames}</h6> 
                          <p class="card-text">&nbsp;</p>
                          <a class="btn btn-primary" role="button" href="${ADMIN}/section/edit?section=${sectionObj.id}">编辑</a>
                          <a class="btn btn-secondary" role="button" href="${ADMIN}/section/term?s=${sectionObj.id}">子栏目</a>
                      </div>
                  </div>
              </div>
              </c:forEach>
          </div>
      </div>
  </body>
</html>