<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="forum" uri="/WEB-INF/forum.tld"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>${article.title}</title>
  </head>
  <body>
      <div class="email-head">
          <div class="email-head-subject">
              <div class="title">
                <a href="javascript:;" class="active">
                    <span class="icon mdi <forum:status value="${article.status.symbol}"/>"></span>
                </a> 
                <span>${article.title}</span> 
                <div class="icons tools dropdown">
                    <a href="${ADMIN}/section/article/modify?id=${article.id}" title="编辑"><span class="icon mdi mdi-edit"></span></a>
                    <a href="javascript:;" class="action-cmd" data-handler="${ADMIN}/section/article/remove" data-query="id:${article.id}"><span class="icon mdi mdi-delete"></span></a>
                </div>
              </div>
          </div>
          <div class="email-head-sender">
              <div class="date">阅读 &nbsp; ${article.stats.displaies} &nbsp; &nbsp;赞 &nbsp; ${article.stats.likes}</div>
              <div class="sender">
                  <a id="page-title" href="${ADMIN}/section/article/list/${article.boardId}.xhtml" title="版块话题列表" data-handler="${ADMIN}/board/title" data-query="id:${article.boardId}" data-format="%T%">loading</a> &nbsp; | &nbsp;
                  <forum:outlet value="${article.topicCategoryName}" optional="普通"/> &nbsp; | &nbsp;
                  <a href="${ADMIN}/member/${article.memberId}.xhtml">${article.memberNickname}</a> &nbsp; <forum:date value="${article.entryDateTime}"/>
              </div>
          </div>
      </div>
      <div class="email-body" style="min-height:760px">${article.content.content}</div>
  </body>
</html>