<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>${form.actionTitle}话题设置</title>
  </head>
  <body>
      <div class="page-head">
          <h2 class="page-head-title">${form.actionTitle}版块设置</h2>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb page-head-nav">
              <li class="breadcrumb-item"><a href="${ADMIN}/client">主页</a></li>
              <li class="breadcrumb-item"><a href="${ADMIN}/topic/${form.topicId}.xhtml">话题</a></li>
              <li class="breadcrumb-item active">配置表单</li>
            </ol>
          </nav>
      </div>
      <div class="main-content container-fluid">
          <div class="row">
              <div class="col-md-12">
                  <div class="card card-border-color card-border-color-primary">
                      <div class="card-header card-header-divider">${form.actionTitle}话题设置</div>
                      <div class="card-body">
                          <c:if test="${not empty errors}">
                          <div role="alert" class="alert alert-danger alert-dismissible">
                              <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="mdi mdi-close"></span></button>
                              <div class="icon"> <span class="mdi mdi-close-circle-o"></span></div>
                              <div class="message"><strong>oOps!</strong> ${errors} </div>
                          </div>
                          </c:if>
                          <form method="post" action="${ADMIN}/topic/config">
                          <div class="form-group row">
                              <label for="privacy" class="col-12 col-sm-3 col-form-label text-sm-right">话题公开</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="privacy"<c:if test="${'1' eq form.privacy}"> checked="checked"</c:if> class="custom-control-input" value="1"><span class="custom-control-label">否</span>
                                  </label>
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="privacy"<c:if test="${'0' eq form.privacy}"> checked="checked"</c:if> class="custom-control-input" value="0"><span class="custom-control-label">是</span>
                                  </label>
                                  <span class="form-text text-muted">所有人可见表示为公开(是),仅自已或朋友可见表示不公开(否)</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="reply" class="col-12 col-sm-3 col-form-label text-sm-right">回复</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="reply"<c:if test="${'1' eq form.reply}"> checked="checked"</c:if> class="custom-control-input" value="1"><span class="custom-control-label">开启</span>
                                  </label>
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="reply"<c:if test="${'0' eq form.reply}"> checked="checked"</c:if> class="custom-control-input" value="0"><span class="custom-control-label">关闭</span>
                                  </label>
                                  <span class="form-text text-muted">选择关闭后,任何人都不可以回复</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="notify" class="col-12 col-sm-3 col-form-label text-sm-right">通知</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="notify"<c:if test="${'1' eq form.notify}"> checked="checked"</c:if> class="custom-control-input" value="1"><span class="custom-control-label">开启</span>
                                  </label>
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="notify"<c:if test="${'0' eq form.notify}"> checked="checked"</c:if> class="custom-control-input" value="0"><span class="custom-control-label">关闭</span>
                                  </label>
                                  <span class="form-text text-muted">如果回复选择:开启,通知选择:开启;当有人回复时会收到回复通知消息</span>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="readMinScore" class="col-12 col-sm-3 col-form-label text-sm-right">只读积分需求</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="readMinScore" type="number" class="form-control" value="${form.readMinScore}" required="required" />
                                  <span class="form-text text-muted">只读模式下会员积分的最低要求,单位:积分数</span>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="readLowMemberGroup" class="col-12 col-sm-3 col-form-label text-sm-right">只读会员组需求</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <select class="select2" name="readLowMemberGroup" style="width:300px;">
                                      <c:forEach items="${memberGroupData}" var="memberGroup">
                                      <option value="${memberGroup.key}"<c:if test="${form.readLowMemberGroup eq memberGroup.key }"> selected="selected"</c:if>>${memberGroup.value }</option>
                                      </c:forEach>
                                  </select>
                                  <span class="form-text text-muted">只读模式下会员组的最低要求,单位:会员组枚举</span>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="readLowMemberRole" class="col-12 col-sm-3 col-form-label text-sm-right">只读会员角色需求</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <select class="select2" name="readLowMemberRole" style="width:300px;">
                                      <c:forEach items="${memberRoleData}" var="memberRole">
                                      <option value="${memberRole.key}"<c:if test="${form.readLowMemberRole eq memberRole.key }"> selected="selected"</c:if>>${memberRole.value }</option>
                                      </c:forEach>
                                  </select>
                                  <span class="form-text text-muted">只读模式下会员角色的最低要求,单位:会员角色枚举</span>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="readLowMemberLevel" class="col-12 col-sm-3 col-form-label text-sm-right">只读等级需求</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="readLowMemberLevel" type="number" class="form-control" value="${form.readLowMemberLevel}" required="required" />
                                  <span class="form-text text-muted">只读模式下会员等级的最低要求,单位:等级数</span>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="writeMinScore" class="col-12 col-sm-3 col-form-label text-sm-right">读写积分需求</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="writeMinScore" type="number" class="form-control" value="${form.writeMinScore}" required="required" />
                                  <span class="form-text text-muted">读写模式下会员积分的最低要求,单位:积分数</span>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="writeLowMemberGroup" class="col-12 col-sm-3 col-form-label text-sm-right">读写会员组需求</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <select class="select2" name="writeLowMemberGroup" style="width:300px;">
                                      <c:forEach items="${memberGroupData}" var="memberGroup">
                                      <option value="${memberGroup.key}"<c:if test="${form.writeLowMemberGroup eq memberGroup.key }"> selected="selected"</c:if>>${memberGroup.value }</option>
                                      </c:forEach>
                                  </select>
                                  <span class="form-text text-muted">读写模式下会员组的最低要求,单位:会员组枚举</span>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="writeLowMemberRole" class="col-12 col-sm-3 col-form-label text-sm-right">读写会员角色需求</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <select class="select2" name="writeLowMemberRole" style="width:300px;">
                                      <c:forEach items="${memberRoleData}" var="memberRole">
                                      <option value="${memberRole.key}"<c:if test="${form.writeLowMemberRole eq memberRole.key }"> selected="selected"</c:if>>${memberRole.value }</option>
                                      </c:forEach>
                                  </select>
                                  <span class="form-text text-muted">读写模式下会员角色的最低要求,单位:会员角色枚举</span>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="writeLowMemberLevel" class="col-12 col-sm-3 col-form-label text-sm-right">读写等级需求</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="writeLowMemberLevel" type="number" class="form-control" value="${form.writeLowMemberLevel}" required="required"/>
                                  <span class="form-text text-muted">读写模式下会员等级的最低要求,单位:等级数</span>
                              </div>
                          </div>
                          <div class="form-group row pt-1 pb-1">
                              <label for="writeMinInterrupt" class="col-12 col-sm-3 col-form-label text-sm-right">连续操作次数</label>
                              <div class="col-12 col-sm-8 col-lg-6">
                                  <input name="writeMinInterrupt" type="number" class="form-control" value="${form.writeMinInterrupt}" required="required"/>
                                  <span class="form-text text-muted">读写模式下同一会员连续回贴的次数限制.例:3,同一会员只能连续进行3次回复.防止恶意注水</span>
                              </div>
                          </div>
                          <div class="form-group row">
                              <label for="atomPoster" class="col-12 col-sm-3 col-form-label text-sm-right">原子回复</label>
                              <div class="col-12 col-sm-8 col-lg-6 form-check mt-2">
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="atomPoster"<c:if test="${'1' eq form.atomPoster}"> checked="checked"</c:if> class="custom-control-input" value="1"><span class="custom-control-label">开启</span>
                                  </label>
                                  <label class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" name="atomPoster"<c:if test="${'0' eq form.atomPoster}"> checked="checked"</c:if> class="custom-control-input" value="0"><span class="custom-control-label">关闭</span>
                                  </label>
                                  <span class="form-text text-muted">原子回复:开启,每一位会员只允许回复一次,不可以重复回复,楼主可以重复回复</span>
                              </div>
                          </div>
                          <div class="form-group row text-right">
                              <div class="col col-sm-10 col-lg-9 offset-sm-1 offset-lg-0">
                                  <input type="hidden" name="record" value="${form.record}" />
                                  <input type="hidden" name="token" value="${form.token}" />
                                  <input type="hidden" name="topicId" value="${form.topicId}" />
                                  <button type="submit" class="btn btn-space btn-primary">提交</button>
                                  <button class="btn btn-space btn-secondary">取消</button>
                              </div>
                          </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </body>
</html>