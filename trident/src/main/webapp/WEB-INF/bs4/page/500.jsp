<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
  <head>
    <title>服务器内部错误 - 500</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
      body{margin:0;padding:0;font-size:14px;font-family: -apple-system,BlinkMacSystemFont,Helvetica Neue,Helvetica,Arial,PingFang SC,Hiragino Sans GB,Microsoft YaHei,sans-serif;}
      .full-page{width:100%;height:600px;background-color:#f2f2f2;margin:0;padding:0;float:left}
      .center-box{width:900px;margin:0 auto;}
      .box-content{margin:0px;padding-left:350px;padding-top:340px;height:260px;position:relative;background-color:transparent;z-index:10}
      .content-left-side{position:absolute;bottom:-40px;left:-80px;z-index:1;background-color:transparent;opacity:1}
      .code-ele {font-size: 1.1em;color: #c7254e;}
      .err-code{ padding: 2px 4px;font-size: 90%;color: #fff;background-color: #333;border-radius: 3px;-webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,.25);box-shadow: inset 0 -1px 0 rgba(0,0,0,.25);display:inline-block;}
      li p{display:inline-block;}a{display:inline-block;margin-left:10px;text-decoration: none;outline: none;color:#333;padding:3px 5px}a:hover{color:#fff;background-color: #337ab7;}h3{font-size: 24px;}ul{list-style:none;margin:0;padding:0}li{margin:0;padding:0;list-style:none}
    </style>
  </head>
  <body>
    <div class="full-page">
      <div class="center-box">
        <div class="box-content">
          <div class="content-left-side"><img src ="${BASE}/static/img/error.png" width="50%" height="50%"/></div>
          <h3>服务器内部错误</h3>
          <p class="code-ele">${errors}</p>
          <div style="position:relative;z-index:100">
            <ul>
              <li>代码: &nbsp;<span class="err-code">500</span></li>
              <li>来源: &nbsp;<c:out value="${referer}" default="未捕获到"/></li>
              <li>您可以:
                  <p><a href="${BASE}">首页</a></p>
                  <p><a href="${BASE}/board/home">版块</a></p>
                  <p><a href="${BASE}/member/home/">个人中心</a></p>
                  <p><a href="javascript:;" onClick="javascript:history.back(-1);">返回上一页</a></p>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>