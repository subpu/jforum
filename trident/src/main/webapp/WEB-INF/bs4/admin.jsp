<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="BASE" scope="application" value="${pageContext.request.contextPath}" />
<c:set var="THEME" scope="application"><spring:eval expression="@environment.getProperty('site.admin.theme')"/></c:set>
<c:set var="APP" scope="application"><spring:eval expression="@environment.getProperty('site.appname')"/></c:set>
<c:set var="LOGO" scope="application"><spring:eval expression="@environment.getProperty('site.logo')"/></c:set>
<c:set var="FRONT" scope="application"><spring:eval expression="@environment.getProperty('site.domain')"/></c:set>
<c:set var="ADMIN" scope="application"><spring:eval expression="@environment.getProperty('site.admin.path')"/></c:set>
<c:set var="IMGSTORE" scope="application"><spring:eval expression="@environment.getProperty('img.bucket.domain')"/></c:set>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><sitemesh:write property='title'/> - ${APP}</title>
    <link rel="icon" href="${BASE}/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="${BASE}/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="${BASE}/static/lib/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="${BASE}/static/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="${BASE}/static/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
    <link rel="stylesheet" type="text/css" href="${BASE}/static/lib/select2/css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="${BASE}/static/lib/jquery.niftymodals/dist/jquery.niftymodals.css"/>
    <link rel="stylesheet" type="text/css" href="${BASE}/static/lib/lightbox/dist/ekko-lightbox.css"/>
    <link rel="stylesheet" type="text/css" href="${BASE}/static/lib/poshytip/src/tip-yellowsimple/tip-yellowsimple.css"/>
    <link rel="stylesheet" type="text/css" href="${BASE}/static/lib/jquery-editable/css/jquery-editable.css"/>
    <link rel="stylesheet" type="text/css" href="${BASE}/static/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="${BASE}/static/lib/bootstrap-tagsinput/dist/bootstrap-tagsinput.css"/>
    <link rel="stylesheet" type="text/css" href="${BASE}/static/lib/morrisjs/morris.css"/>
    <link rel="stylesheet" type="text/css" href="${BASE}/static/lib/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="${BASE}/static/lib/bootstrap-fileinput/css/fileinput.min.css" media="all"/>
    <!-- if using RTL (Right-To-Left) orientation, load the RTL CSS file after fileinput.css by uncommenting below -->
    <!-- link rel="stylesheet" type="text/css" href="${BASE}/static/lib/bootstrap-fileinput/css/fileinput-rtl.min.css" media="all" /-->
    <link rel="stylesheet" href="${BASE}/static/css/app.css" type="text/css"/>
    <sitemesh:write property='head'/>
  </head>
  <body>
    <div class="be-wrapper">
      <!-- 顶部导航条开始 -->
      <nav class="navbar navbar-expand fixed-top be-top-header">
        <div class="container-fluid">
          <div class="be-navbar-header" style="text-align:center"><a href="${FRONT}" style="width:inherit"><img src="${BASE}/static/img/logo.svg" style="width:75%;height:75%" /></a>
          </div>
          <div class="be-right-navbar">
            <!-- 右上用户面板开始-->
            <jsp:include page="${THEME}include/index.jsp" />
            <!-- 右上用户面板结束-->
            <div class="page-title"><span>&nbsp;</span></div>
          </div>
        </div>
      </nav>
      <!-- 顶部导航条结束 -->
      <!-- 左部菜单开始 -->
      <div class="be-left-sidebar">
        <div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle" id="page-explor-title">Blank Page</a>
          <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
              <div class="left-sidebar-content">
                <ul class="sidebar-elements">
                  <li class="divider">菜单</li>
                  <li><a href="${ADMIN}/client"><i class="icon mdi mdi-home"></i><span>主面板</span></a></li>
                  <li class="parent"><a href="javascript:;"><i class="icon mdi mdi-settings"></i><span>参数功能</span></a>
                    <ul class="sub-menu">
                      <li><a href="${ADMIN}/smiley/theme/">表情定义</a></li>
                      <li><a href="${ADMIN}/score/role/">积分规则</a></li>
                      <li><a href="${ADMIN}/member/level/">等级定义</a></li>
                      <li><a href="${ADMIN}/member/protect/">帐号保护</a></li>
                      <li><a href="${ADMIN}/ipaddr/rule/">IP过滤</a></li>
                    </ul>
                  </li>
                  <li class="parent"><a href="javascript:;"><i class="icon mdi mdi-account"></i><span>会员</span></a>
                    <ul class="sub-menu">
                      <li><a href="${ADMIN}/member/">最近注册会员</a></li>
                      <li><a href="${ADMIN}/member/penalize/">惩罚记录</a></li>
                      <li><a href="${ADMIN}/member/invitecode/">注册邀请码</a></li>
                      <li><a href="${ADMIN}/member/exchange/">VIP会员</a></li>
                      <li><a href="${ADMIN}/member/action/">动作</a></li>
                    </ul>
                  </li>
                  <li class="parent"><a href="javascript:;"><i class="icon mdi mdi-view-dashboard"></i><span>版块</span></a>
                    <ul class="sub-menu">
                      <li><a href="${ADMIN}/board/group/">版块组</a></li>
                      <li><a href="${ADMIN}/board/">版块</a></li>
                      <li><a href="${ADMIN}/board/moderator/">版主</a></li>
                      <li><a href="${ADMIN}/board/action/">动作</a></li>
                    </ul>
                  </li>
                  <li class="parent"><a href="${ADMIN}/topic/stats/"><i class="icon mdi mdi-layers"></i><span>话题</span></a>
                    <ul class="sub-menu">
                      <li><a href="${ADMIN}/topic/">最近话题</a></li>
                      <li><a href="${ADMIN}/topic/album/">像册</a></li>
                      <li><a href="${ADMIN}/topic/action/">动作</a></li>
                      <li><a href="${ADMIN}/topic/category/">类型</a></li>
                      <li><a href="${ADMIN}/topic/tag/">标签</a></li>
                      <li><a href="${ADMIN}/topic/carousel/">轮播图</a></li>
                    </ul>
                  </li>
                  <li class="parent"><a href="javascript:;"><i class="icon mdi mdi-notifications"></i><span>消息</span></a>
                    <ul class="sub-menu">
                        <li><a href="${ADMIN}/message/create">新消息</a></li>
                        <li><a href="${ADMIN}/message/">收件箱</a></li>
                        <li><a href="${ADMIN}/message/sent">发件箱</a></li>
                    </ul>
                  </li><!-- mdi-input-power -->
                  <li class="parent"><a href="javascript:;"><i class="icon mdi mdi-folder"></i><span>文章</span></a>
                    <ul class="sub-menu">
                        <li><a href="${ADMIN}/section/">栏目</a></li>
                        <li><a href="${ADMIN}/section/term">子栏目</a></li>
                        <li><a href="${ADMIN}/section/article">最近文章</a></li>
                    </ul>
                  </li>
                  <li class="parent"><a href="#"><i class="icon mdi mdi-chart-donut"></i><span>统计信息</span></a>
                    <ul class="sub-menu">
                        <li><a href="${ADMIN}/recent/home">概览</a></li>
                        <li><a href="${ADMIN}/member/stats/">会员</a></li>
                        <li><a href="${ADMIN}/topic/stats/">话题</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- 左部菜单结束 -->
      <!-- 页面内容区开始 -->
      <div class="be-content">
        <div class="main-content container-fluid">
            <sitemesh:write property='body'/>
        </div>
      </div>
      <!-- 页面内容区结束 -->
    </div>
    <div id="niftymodals" class="md-overlay"></div>
    <script>var ADMIN="${ADMIN}";var BASE="${FRONT}";var IMGSTORE="${IMGSTORE}";</script>
    <script src="${BASE}/static/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="${BASE}/static/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="${BASE}/static/lib/popper.js/dist/umd/popper.min.js"></script>
    <script src="${BASE}/static/lib/bootstrap4/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="${BASE}/static/lib/fastclick/fastclick.min.js" type="text/javascript"></script>
    <script src="${BASE}/static/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="${BASE}/static/lib/moment.js/min/moment.min.js" type="text/javascript"></script>
    <script src="${BASE}/static/lib/jquery.niftymodals/dist/jquery.niftymodals.min.js" type="text/javascript"></script>
    <script src="${BASE}/static/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <!-- auto complete-->
    <script src="${BASE}/static/lib/jquery.autocomplete/dist/jquery.autocomplete.min.js"></script>
    <script src="${BASE}/static/lib/lightbox/dist/ekko-lightbox.min.js"></script>
    <script src="${BASE}/static/lib/masonry-layout/masonry.pkgd.min.js" type="text/javascript"></script>
    <script src="${BASE}/static/lib/poshytip/src/jquery.poshytip.min.js" type="text/javascript"></script>
    <script src="${BASE}/static/lib/jquery-editable/js/jquery-editable-poshytip.min.js" type="text/javascript"></script>
    <script src="${BASE}/static/lib/ckeditor4/ckeditor.js" type="text/javascript"></script>
    <script src="${BASE}/static/lib/ckeditor4/lang/zh-cn.js"></script>
    <script src="${BASE}/static/lib/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    <script src="${BASE}/static/lib/mustache.js/mustache.min.js"></script>
    <script src="${BASE}/static/lib/raphael/raphael.min.js"></script>
    <script src="${BASE}/static/lib/morrisjs/morris.min.js"></script>
    <script src="${BASE}/static/lib/justgage/dist/justgage.min.js"></script>
    <script src="${BASE}/static/lib/lazyload/jquery.lazyload.min.js"></script>
    <script src="${BASE}/static/lib/lazyload/jquery.scrollstop.min.js"></script>
    <script src="${BASE}/static/lib/bootstrap-fileinput/js/plugins/piexif.min.js" type="text/javascript"></script>
    <script src="${BASE}/static/lib/bootstrap-fileinput/js/plugins/sortable.min.js" type="text/javascript"></script>
    <script src="${BASE}/static/lib/bootstrap-fileinput/js/plugins/purify.min.js" type="text/javascript"></script>
    <script src="${BASE}/static/lib/bootstrap-fileinput/js/fileinput.min.js"></script>
    <script src="${BASE}/static/lib/bootstrap-fileinput/themes/fa/theme.js"></script>
    <script src="${BASE}/static/lib/bootstrap-fileinput/js/locales/zh.js"></script>
    <script src="${BASE}/static/lib/parsley/parsley.min.js" type="text/javascript"></script>
    <script src="${BASE}/static/lib/parsley/i18n/zh_cn.js" type="text/javascript"></script>
    <script src="${BASE}/static/js/app.js" type="text/javascript"></script>
    <script src="${BASE}/static/js/app-page-gallery.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(document).ready(function(){
          //initialize the javascript
          App.init();
      });
      //图片流式布局
      $(window).on('load',function(){
          App.pageGallery();
      });
    </script>
  </body>
</html>