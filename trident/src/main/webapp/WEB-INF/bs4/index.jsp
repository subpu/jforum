<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="BASE" scope="application" value="${pageContext.request.contextPath}" />
<c:set var="THEME" scope="application"><spring:eval expression="@environment.getProperty('site.theme')"/></c:set>
<c:set var="APP" scope="application"><spring:eval expression="@environment.getProperty('site.appname')"/></c:set>
<c:set var="DOMAIN" scope="application"><spring:eval expression="@environment.getProperty('site.domain')"/></c:set>
<c:set var="LOGO" scope="application"><spring:eval expression="@environment.getProperty('site.logo')"/></c:set>
<c:set var="IMGSTORE" scope="application"><spring:eval expression="@environment.getProperty('img.bucket.domain')"/></c:set>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <title><sitemesh:write property='title'/> - ${APP}</title>
    <link rel="icon" href="${BASE}/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="${BASE}/favicon.ico" type="image/x-icon" />
    <link href="${BASE}/static/lib/bootstrap4/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="${BASE}/static/lib/material-design-icons/css/material-design-iconic-font.min.css" rel="stylesheet" type="text/css" />
    <link href="${BASE}/static/lib/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="${BASE}/static/lib/select2-bootstrap4-theme/dist/select2-bootstrap4.min.css" rel="stylesheet" type="text/css" >
    <link href="${BASE}/static/lib/bootstrap-suggest/bootstrap-suggest.css" rel="stylesheet" type="text/css"/>
    <link href="${BASE}/static/lib/jquery-confirm/jquery-confirm.min.css" rel="stylesheet" />
    <link href="${BASE}/static/lib/magic-check/css/magic-check.min.css" rel="stylesheet" type="text/css"/>
    <link href="${BASE}/static/lib/lightbox/dist/ekko-lightbox.css" rel="stylesheet" type="text/css"/>
    <link href="${BASE}/static/lib/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
    <link href="${BASE}/static/lib/owo-emoji/dist/OwO.min.css" rel="stylesheet" type="text/css"/>
    <link href="${BASE}/static/img/core.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" media="screen and (max-width: 1200px)" href="${BASE}/static/img/core.device.css" />
    <sitemesh:write property='head'/>
  </head>
  <body class="<c:out value="${cookie.theme.value}" default="snowhite"/>">
    <!--<div class="position-absolute w-100 d-flex flex-column p-4" id="toast-collect"></div>-->
    <!-- 顶部导航条开始 -->
    <header class="container-fluid hl80" id="sitehead">
      <div class="container">
        <div class="row">
            <div id="leftmenu" class="col-md-9">
                <div id="logo"><a href="<c:url value="/"/>"><img src="${DOMAIN}/${LOGO}" style="width:23%"/></a></div>
            </div>
            <div id="topmenu" class="col-md-3 text-right" data-socket-uri="${BASE}/sock/message">
                <div class="btn-group" role="group" id="header_member_panel" data-handler="${DOMAIN}/member/panel">
                    <a role="button" class="btn btn-light forum_member_defat_link" href="${DOMAIN}/member/register">注册</a>
                    <a role="button" class="btn btn-primary forum_member_defat_link" href="${DOMAIN}/member/login">登陆</a>
                </div>
            </div>
        </div>
      </div>
    </header>
    <!-- 顶部导航条结束 -->
    <!-- 页面内容区开始 -->
    <sitemesh:write property='body'/>
    <!-- 页面内容区结束 -->
    <script src="${BASE}/static/lib/jquery/jquery.min.js"></script>
    <script>var BASE="${DOMAIN}";var IMGSTORE="${IMGSTORE}";</script>
    <script src="${BASE}/static/lib/store.js/dist/store.legacy.min.js"></script>
    <script src="${BASE}/static/lib/popper.js/dist/umd/popper.min.js"></script>
    <script src="${BASE}/static/lib/bootstrap4/js/bootstrap.min.js"></script>
    <script src="${BASE}/static/lib/jquery-confirm/jquery-confirm.min.js"></script>
    <script src="${BASE}/static/lib/purl.js"></script>
    <script src="${BASE}/static/lib/moment.js/min/moment.min.js"></script>
    <script src="${BASE}/static/lib/mustache.min.js"></script>
    <script src="${BASE}/static/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="${BASE}/static/lib/bootstrap-suggest/bootstrap-suggest.js"></script>
    <script src="${BASE}/static/lib/sockjs-client/dist/sockjs.min.js"></script>
    <script src="${BASE}/static/lib/ckeditor4/ckeditor.js"></script>
    <script src="${BASE}/static/lib/ckeditor4/lang/zh-cn.js"></script>
    <script src="${BASE}/static/lib/spin.js/dist/spin.min.js"></script>
    <script src="${BASE}/static/lib/spin.tool.js"></script>
    <script src="${BASE}/static/lib/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script src="${BASE}/static/lib/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
    <script src="${BASE}/static/lib/parsley/parsley.min.js" type="text/javascript"></script>
    <script src="${BASE}/static/lib/parsley/i18n/zh_cn.js" type="text/javascript"></script>
    <script src="${BASE}/static/lib/lazyload/jquery.lazyload.min.js"></script>
    <script src="${BASE}/static/lib/lazyload/jquery.scrollstop.min.js"></script>
    <script src="${BASE}/static/lib/lightbox/dist/ekko-lightbox.min.js"></script>
    <script src="${BASE}/static/lib/jquery.fileDownload.js"></script>
    <script src="${BASE}/static/lib/js.cookie.min.js"></script>
    <script src="${BASE}/static/lib/owo-emoji/dist/OwO.min.js"></script>
    <script src="${BASE}/static/lib/jquery.scrollPagination.plug.js"></script>
    <script src="${BASE}/static/lib/jquery.drawer.plug.js"></script>
    <script src="${BASE}/static/js/forum.notice.letter.js"></script>
    <script src="${BASE}/static/js/forum.notice.topic.js"></script>
    <script src="${BASE}/static/lib/jquery.pagination.plug.js"></script>
    <script src="${BASE}/static/js/forum.side.js"></script>
    <script src="${BASE}/static/js/forum.core.js"></script>
    <script src="${BASE}/static/js/forum.device.js"></script>
  </body>
</html>