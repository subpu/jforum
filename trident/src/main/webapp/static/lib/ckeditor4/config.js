/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// https://ckeditor.com/docs/ckeditor4/latest/api/CKEDITOR_config.html

	/* The toolbar groups arrangement, optimized for two toolbar rows.*/
	config.toolbarGroups = [
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph', groups: [ 'list', 'blocks', 'align', 'paragraph' ] },
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'links', groups: [ 'links' ] },
		{ name: 'insert', groups: [ 'image', 'table', 'insert', 'emoji' ] }
	];
	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	config.removeButtons = 'Subscript,Superscript,HorizontalRule,SpecialChar';
	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';
	// Simplify the dialog windows.
	config.removeDialogTabs = 'link:advanced;link:upload;image:advanced;image:link';
	//
	config.extraPlugins="uploadimage",
	config.uploadUrl="/action/blog/ckeditor_img_upload",
	config.filebrowserUploadUrl="/action/blog/ckeditor_dialog_img_upload",
	config.defaultLanguage="zh-cn",
	config.language = 'zh-cn'
};
