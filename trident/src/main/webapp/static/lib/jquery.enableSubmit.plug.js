/*
 * Copyright (c) 2017 xiaofanku
 * mail:xiaofanku@live.cn
 
 * build on jQuery 1.8.1
 * Versions
 * 0.0.1 - 20170517
 *
*/
(function($) {
    'use strict';
    //是否有输入项为空
    var isEmpty = function(formEle) {
        var data=false;
        var radioArr=[];
        formEle.find('input, textarea, select').not('input:hidden,input:submit,input:button').each(function(i, field) {
         //console.log('name:'+field.name+',type:'+field.type+',value:'+field.value);
         //radio,选中的值是否为undefined
         if(field.type==='radio' && typeof($(':radio[name='+field.name+']:checked').val())==="undefined"){
             data=true;
             return;
         }
         //checkbox选中几个
         if(field.type==='checkbox' && $(':checkbox[name='+field.name+']:checked').length == 0){
             data=true;
             return;
         }
         //不是radio,checkbox,select
         if(field.value===''){
             data=true;
             return;
         }
        });
        
        return data;
    };
    var action = {
        light: function(config) {
            var btn=$(config.ele).find(config.active).addClass(config.css);
            //是否使用disabled
            if(config.disable){
                //jquery 1.6+
                if(typeof(btn.prop)=='function'){
                    btn.prop('disabled',false);
                }else{
                    btn.attr('disabled',false);
                }
            }
        },
        dark: function(config) {
            var btn=$(config.ele).find(config.active).removeClass(config.css);
            if(config.disable){
                if(typeof(btn.prop)=='function'){
                    btn.prop('disabled',true);
                }else{
                    btn.attr('disabled',true);
                }
            }
        }
    };
    $.fn.enableSubmit = function(option) {
        var _self=this;
        if(!_self.is('form')){ return this;}
        var config=$.extend({active:'input:submit',css:'focus',disable:true},option);
        config.ele=_self;
        
        //默认值
        if(!isEmpty(_self)){
            action.light(config);
        }
        //侦听变化
        _self.bind('keyup change', 'input, select, textarea', function(){
            if(!isEmpty(_self)){
                //添加定义:可用
                action.light(config);
            }else{
                //移除定义:禁用
                action.dark(config);
            }
        });
        return this;
    };
})(jQuery);