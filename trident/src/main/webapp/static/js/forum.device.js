//会员中心的话题[NW][4][平板]
function drawDynamicTabletTopic(json, element){
    if(jQuery.isEmptyObject(json)){
        return;
    }
    $('#forumlist').removeClass('sectionbox');
    var data ={};
    data.result = json;
    var T='{#result}'
        +'<div class="embed_topic_item main-color">'
        +'  <div class="embed_topic_item_head">'
        +'    <div class="topic_item_avatar">'
        +'      <a href="{APP}/member/{author}.xhtml" title="{authorNames}">'
        +'        <img class="img-circle" src="{APP}/member/avatar/{author}.png" style="width: 64px; height: 64px;">'
        +'      </a>'
        +'    </div>'
        +'    <div class="topic_item_author">'
        +'      <h6>'
        +'        <a href="{APP}{link}" class="topic_item_title">{title}</a>'
        +'      </h6>'
        +'      <p><a href="{APP}/member/{author}.xhtml" title="会员主页">{authorNames}</a>&nbsp;&#x40;&nbsp;{date}&nbsp;&nbsp;&#187;&nbsp;&nbsp;<a href="{APP}{boardLink}">{boardTitle}</a></p>'
        +'    </div><div class="topic_item_tools">&nbsp;</div>'
        +'  </div>'
        +'  <div class="embed_topic_item_body">'
        +'      <p class="topic_item_summary">{&description}</p>'
        +'  </div>'
        +'  <div class="embed_topic_item_foot"></div>'
        +'</div>{/result}';
    Mustache.parse(T, ['{', '}']);
    $(element).append(Mustache.render(T, $.extend(data, {"APP":BASE})));
};
//版块内容页的置顶话题列表[NW][7][平板]
function drawBoardTopSectionTabletTopic(data, element){
    var currentId = "dynamic-section-"+randomAlphaNumeric(8);
    $(element).attr('id', currentId);
    $.each(data, function(index, item){
        drawUpdatedTabletTopics(item, currentId, false);
    });
};
//最新类推送回调
function drawUpdatedTabletTopics(json, jqEle, isPushed){
    var topicItemCS = isPushed?'dynamic-topic-record fadeout-anim':'updated-rows';
    var UT='' 
          +'<li class="board_item '+topicItemCS+'">' 
          +'  <dl class="icon topic_{status}">' 
          +'    <dt>' 
          +'      <div class="board_item_title">' 
          +'        <article class="forum-article-header">' 
          +'          {#top}<span class="label" title="置顶话题">&#x1F4CC</span> {/top}' 
          +'          {#best}<span class="label" title="精华话题">&#x1F48E</span> {/best}' 
          +'          {#image}<span class="label" title="图片话题">&#x1F304</span> {/image}' 
          +'          {#hot}<span class="label" title="很火噢">&#x1F525</span> {/hot}'
          +'          <a href="{APP}{boardLink}?category={categoryKey}">[{category}]</a> '
          +'          <a href="{APP}{link}" class="forumtitle">{title}</a>' 
          +'        </article>' 
          +'        <a class="txtline" href="{APP}/member/{author}.xhtml">{authorNames}</a> &#187; {date}' 
          +'      </div>' 
          +'    </dt>' 
          +'    <dd class="latest">{#recent}' 
          +'      <span class="last-posts-avatar"><img src ="{APP}/member/avatar/{recentAuthor}.png" width="32px" height="32px"/></span>' 
          +'      <span class="last-posts"><a href="{APP}/member/{recentAuthor}.xhtml" class="latest-member-color">{recentAuthorNames}</a> ' 
          +'      <br/>{recentDate}</span>{/recent}' 
          +'    </dd>' 
          +'  </dl>' 
          +'</li>';
    var UE='' 
          +'<li class="board_item '+topicItemCS+'">' 
          +'  <dl class="icon topic_{status}">' 
          +'    <dt>' 
          +'      <div class="board_item_title">' 
          +'        <article class="forum-article-header">' 
          +'          {#top}<span class="label" title="置顶话题">&#x1F4CC</span> {/top}' 
          +'          {#best}<span class="label" title="精华话题">&#x1F48E</span> {/best}' 
          +'          {#image}<span class="label" title="图片话题">&#x1F304</span> {/image}' 
          +'          {#hot}<span class="label" title="很火噢">&#x1F525</span> {/hot}'
          +'          <a href="{APP}{boardLink}?category={categoryKey}">[{category}]</a> '
          +'          <a href="{APP}{link}" class="forumtitle">{title}</a>' 
          +'        </article>' 
          +'        <a class="txtline" href="{APP}/member/{author}.xhtml">{authorNames}</a> &#187; {date}' 
          +'      </div>' 
          +'    </dt>' 
          +'    <dd class="latest"></dd>' 
          +'  </dl>' 
          +'</li>';
    var extRS={
            recent : function(){
                return parseInt(this.recentAuthor) > 0;
            },
            APP : BASE
    };
    var T = (json.recentAuthorNames !== '0.0.0.*')?UT:UE;
    Mustache.parse(T, ['{', '}']);
    var rs = $.extend(json, extRS);
    //
    $('#'+jqEle).prepend(Mustache.render(T, rs));
};
//版块话题的动态加载回调函数[平板]
function drawTabletThreads(jsonArray){
    var targetEleSelector = $('#board_topic_collect');
    var obj = {};
        obj.result = jsonArray;
    var T='{#result}'
        +'<li class="board_item dynamic-topic-record">'
        +'  <dl class="icon topic_{status}">'
        +'    <dt>'
        +'      <div class="board_item_title">'
        +'        <article class="forum-article-header">'
        +'            {#top}<span class="label" title="置顶话题">&#x1F4CC</span> {/top}'
        +'            {#best}<span class="label" title="精华话题">&#x1F48E</span> {/best}'
        +'            {#image}<span class="label" title="图片话题">&#x1F304</span> {/image}'
        +'            {#hot}<span class="label" title="很火噢">&#x1F525</span> {/hot}'
        +'            <a href="{APP}{boardLink}?category={categoryKey}">[{category}]</a> '
        +'            <a href="{APP}{link}" class="forumtitle">{title}</a>'
        +'        </article>'
        +'        <a class="txtline" href="{APP}/member/{author}.xhtml">{authorNames}</a> &#187; {date}'
        +'      </div>'
        +'    </dt>'
        +'    <dd class="latest">{#recent}'
        +'      <span class="last-posts-avatar">'
        +'        <img src ="{APP}/member/avatar/{recentAuthor}.png" width="32px" height="32px"/>'
        +'      </span>'
        +'      <span class="last-posts">'
        +'        <a href="{APP}/member/{recentAuthor}.xhtml" class="latest-member-color">{recentAuthorNames}</a>' 
        +'        <br/>'
        +'        {recentDate}'
        +'      </span>{/recent}'
        +'    </dd>'
        +'  </dl>'
        +'</li>{/result}';
    var extRS={
        recent : function(){
            return parseInt(this.recentAuthor) > 0;
        },
        APP : BASE
    };
    Mustache.parse(T, ['{', '}']);
    var rs = $.extend(obj, extRS);
    targetEleSelector.append(Mustache.render(T, rs));
};
//话题回复的动态加载回调函数[平板]
//需要注意scale的值@20200506
function drawTabletReplier(jsonArray){
    var targetEleSelector = $('#topic_replier_collect');
    var obj = {};
        obj.result = jsonArray;
        var T='{#result}'
            +'<div class="embed_topic_item main-color">'
            +'  <div class="embed_topic_item_head">'
            +'    <div class="topic_item_avatar">'
            +'      <a href="{APP}/member/{author}.xhtml" title="{authorNames}">'
            +'        <img class="img-circle" src="{APP}/member/avatar/{author}.png" style="width: 64px; height: 64px;">'
            +'      </a>'
            +'    </div>'
            +'    <div class="topic_item_author">'
            +'      <h6>'
            +'        <a href="{APP}/member/{author}.xhtml" title="会员主页">{authorNames}</a><small class="badge member-bg-{authorStyle} mgs">{authorGroup}</small>'
            +'      </h6>'
            +'      <p>{#master}楼主&nbsp;&nbsp;&#187;&nbsp;&nbsp;{/master}{date}</p>'
            +'    </div>'
            +'    <div class="topic_item_tools">'
            +'      <p><a href="javascript:;" role="button" name="#posts-{id}">{floor}<sup>#</sup></a><a href="javascript:;" role="button" class="btn reply-drawer-menu"><i class="ico-sm mdi mdi-more-vert" aria-hidden="true"></i></a></p>'
            +'    </div>'
            +'  </div>'
            +'  <div class="embed_topic_item_body" id="posts-{id}">'
            +'    {#block}<p class="alert alert-danger" role="alert"><strong>提示:</strong> 作者被禁止发言或内容自动屏蔽</p>{/block}'
            +'    {^block}'
            +'      {&content}{&modify}'
            +'    {/block}'
            +'  </div>'
            +'  <div class="embed_topic_replier_tools">'
            +'    <ul class="list-inline row mood-section" data-mood="{id}">'
            +'      <li class="col-xs-4 col-sm-4 text-center"><a href="javascript:;" title="不赞同" role="button" class="btn-default btn-sm mood-action" data-action="2" id="{id}-hates"><i class="ico-sm mdi mdi-thumb-down" aria-hidden="true"></i>&nbsp;不赞同</a></li>'
            +'      <li class="col-xs-4 col-sm-4 text-center"><a href="javascript:;" title="赞" role="button" class="btn-default btn-sm mood-action" data-action="1" id="{id}-likes"><i class="ico-sm mdi mdi-thumb-up" aria-hidden="true"></i>&nbsp;赞</a></li>'
            +'      <li class="col-xs-4 col-sm-4 text-center"><a href="{APP}/posts/create?qfloor={floor}&qreplier={id}&scale=640x360" title="回复作者" role="button" class="btn-default btn-sm"><i class="ico-sm mdi mdi-mail-reply"></i>&nbsp;回复</a></li>'
            +'    </ul>'
            +'    </div>'
            +'</div>{/result}';
    var extRS={
        modify : function(){
            return this.modifyDate.length == 0?'':'<p class="alert alert-warning" role="alert">回复最近由 '+this.modifyer+' 于 '+this.modifyDate+' 编辑</p>';
        },
        APP : BASE
    };
    Mustache.parse(T, ['{', '}']);
    var rs = $.extend(obj, extRS);
    targetEleSelector.append(Mustache.render(T, rs));
    //图片的懒加载
    targetEleSelector.find('img.lazyload').lazyload();
};
function gatherTabletMood(){
    var postsIdStr = $('.embed_topic_item').find('.embed_topic_item_body').map(function(){
        return this.id.replace('posts-', '');
    }).get().join(",");
    if(postsIdStr.length == 0){
        return;
    }
    var requestURI = '/posts/mood/list/jsonp';
    $.ajax({
        url: requestURI,
        jsonpCallback: 'drawMoodResult',
        dataType: 'jsonp',
        data: 'ids='+postsIdStr
    });
};
//加载版块的统计[平板]
function loadBoardStatsTabletResult(json, element){
    if(jQuery.isEmptyObject(json.result)){
        return;
    }
    $.each(json.result, function(index, item){
        var tb = $('[data-board="'+item.board+'"]');
        if(parseInt(item.todayTopics)>0){
            tb.find('.today-counter').addClass('badge-info').html(item.todayTopics);
        }
    });
};
//会员中心的话题|回复[平板]
function drawTabletMHomeRecord(jsonArray){
    var targetEleSelector = $('#member-home-records');
    var obj = {};
        obj.result = jsonArray;
    var T ='{#result}'
          +' <div class="home-record-item">'
          +'   <div class="record-item-body">'
          +'     <h6 class="record-item-heading"><a href="{APP}{link}">{title}</a></h6>'
          +'     <span>楼主: <a href="{APP}{publisherURI}">{publisher}</a></span>'
          +'     <span>发布日期: {publishDate}</span>'
          +'     {#reply}'
          +'       <span>最近回复: <a href="{APP}{replierURI}">{replier}</a></span>'
          +'       <span>&#187; {replyDate}</span>'
          +'     {/reply}'
          +'   </div>'
          +' </div>{/result}';
    Mustache.parse(T, ['{', '}']);
    var rs = $.extend(obj, {"APP":BASE});
    targetEleSelector.append(Mustache.render(T, rs));
};
//会员中心的点赞|收藏[平板]
function drawTabletMHomeAction(jsonArray){
    var targetEleSelector = $('#member-home-actions');
    var obj = {};
        obj.result = jsonArray;
    var T ='{#result}'
          +' <div class="home-record-item">'
          +'   <div class="record-item-body">'
          +'     <h6 class="record-item-heading"><a href="{APP}{link}">{title}</a></h6>'
          +'     <span>操作日期: {actionDate}</span>'
          +'   </div>'
          +' </div>{/result}';
    Mustache.parse(T, ['{', '}']);
    var rs = $.extend(obj, {"APP":BASE});
    targetEleSelector.append(Mustache.render(T, rs));
};
//会员中心的历史记录[平板]
function drawTabletActionHistory(jsonArray){
    var targetEleSelector = $('#member-home-actions');
    var obj = {};
        obj.result = jsonArray;
    var T ='{#result}'
          +' <div class="home-record-item">'
          +'   <div class="record-item-body">'
          +'     <h6 class="record-item-heading"><a href="{APP}{link}">{title}</a></h6>'
          +'     <span>{actionTitle}&nbsp;&#187;&nbsp;{actionDate}</span>'
          +'   </div>'
          +' </div>{/result}';
    Mustache.parse(T, ['{', '}']);
    var rs = $.extend(obj, {"APP":BASE});
    targetEleSelector.append(Mustache.render(T, rs));
};
//回复菜单的关闭函数
function replyDrawerMenuClose(){
    $(this).find('.post-action-remove').removeAttr('data-query');
    $(this).find('.post-action-report').removeAttr('data-query');
};
//首页的话题举报回调函数@R1
//话题内容页的回复举报回调函数@R2
function drawerReportMenuClose(){
  $(this).find('input[name=id]').val('');
  $(this).find('textarea[name=reason]').val('');
  $(this).find('.focus-current').removeClass('focus-current');
  $(this).find('.focus-item').last().addClass('focus-current');
};
//话题举报抽屉菜单的初始化@I1
function topicDrawerMenuInit(jqEle, drawerDiv, dtd){
    var ele = jqEle.parents('.embed_topic_item');
    if(!ele.attr('id')){
       return;
    }
    var eleId=ele.attr('id').replace('topic-', ''); 
    var drawerRecordInput = $(drawerDiv).find('input[name=id]');
    if(parseInt(eleId)>0 && drawerRecordInput.length == 1){
        drawerRecordInput.val(eleId);
        dtd.resolve();
    }else{
        dtd.reject();
    }
    return $.when(dtd);
};
//回复举报抽屉菜单的初始化@I2
function replierDrawerMenuInit(jqEle, drawerDiv, dtd){
    var self = jqEle;
    if(!self.attr('data-query')){
        dtd.reject(); 
        return $.when(dtd);
    }
    var eleId=self.attr('data-query').replace('id:', '');
    var drawerRecordInput = $(drawerDiv).find('input[name=id]');
    if(parseInt(eleId)>0 && drawerRecordInput.length == 1){
        drawerRecordInput.val(eleId);
        dtd.resolve();
    }else{
        dtd.reject();
    }
    return $.when(dtd);
};
//a.action-cmd的抽屉菜单回调
function cmdDrawerMenuClose(jqEle){
    jqEle.parents('.embed_drawer_menu').drawer('close');
};
//个人中心消息收件箱回调[平板]
function memberTabletInboxTemplate(jsonArray){
    var targetEleSelector = $('#member-message-records');
    var obj = {};
        obj.result = jsonArray;
    var T = '{#result}'
          +'<div class="embed_topic_item main-color">'
          +'  <div class="embed_topic_item_head">'
          +'    <div class="topic_item_avatar">'
          +'        <a href="{APP}{senderURI}" title="{senderNickname}">'
          +'        <img class="img-circle" src="{APP}/member/avatar/{sender}.png" style="width: 64px; height: 64px;">'
          +'        </a>'
          +'    </div>'
          +'    <div class="topic_item_author">'
          +'        <h6><a href="{APP}/message/view?sender={sender}" data-content="最近的消息主题">{subject}</a></h6>'
          +'        <p>{datetime}&nbsp;&nbsp;&#187;&nbsp;&nbsp;<a href="{APP}{senderURI}" title="会员主页">{senderNickname}</a></p>'
          +'    </div>'
          +'    <div class="topic_item_tools">'
          +'      <span id="notice_{sender}" class="float-right badge">0</span>'
          +'    </div>'
          +'  </div>'
          +'  <div class="embed_topic_item_foot"></div>'
          +'</div>{/result}';
    Mustache.parse(T, ['{', '}']);
    var rs = $.extend(obj, {"APP":BASE});
    targetEleSelector.append(Mustache.render(T, rs));
};
//个人中心消息发件箱回调[平板]
function memberTabletOutboxTemplate(jsonArray){
    var targetEleSelector = $('#member-message-records');
    var obj = {};
        obj.result = jsonArray;
    var T = '{#result}'
          +'<div class="embed_topic_item main-color">'
          +'  <div class="embed_topic_item_head">'
          +'    <div class="topic_item_avatar">'
          +'        {#everyone}<img style="width: 64px; height: 64px;" class="img-circle media-object" src="{APP}/member/avatar/0.png" title="message robot">{/everyone}'
          +'        {^everyone}<a href="{APP}{receiverURI}"><img style="width: 64px; height: 64px;" class="img-circle media-object" src="{APP}/member/avatar/{receiver}.png" title="{receiverNickname}"></a>{/everyone}'
          +'    </div>'
          +'    <div class="topic_item_author">'
          +'        <h6>{&reces} &nbsp;<small>[{category}]</small></h6>'
          +'        <p>{&content}</p>'
          +'    </div>'
          +'    <div class="topic_item_tools">'
          +'      <span class="floatl-right badge badge-pill badge-info"><i class="ico-sm mdi mdi-time"></i>&nbsp;{datetime}</span>'
          +'    </div>'
          +'  </div>'
          +'  <div class="embed_topic_item_foot"></div>'
          +'</div>{/result}';
    var extRS={
            reces : function(){
                  var tmp=''; var rs = this.receives;
                  for (var i = 0, len = rs.length; i < len; ++i) {
                      var rmid = rs[i].id;
                      var rmnm = rs[i].nickname;
                      tmp += '<a href="'+BASE+'/member/'+rmid+'.xhtml">'+rmnm+'</a>';
                      if(i< len -1){
                         tmp +'&nbsp;,&nbsp;';
                      }
                  }
                  return tmp;
            },
            APP : BASE
    };
    Mustache.parse(T, ['{', '}']);
    var rs = $.extend(obj, extRS);
    targetEleSelector.append(Mustache.render(T, rs));
};
function tabletMemberMessageFun(response, jqEle){
    cmdDrawerMenuClose(jqEle);
    if(response.hasOwnProperty('id') && parseInt(response.id) > 0){
        jqEle.parents('form').find('.form-control').val('');
        jqEle.removeClass('disabled');
        return;
    }
    errorMessage('消息发送失败');
};
function buildTabletRichEditor(){
    var emojiData = $('.h5Editor').attr('data-emoji-json');
    new OwO({
        logo: '😀',
        container: document.getElementById('emoji-picker'),
        target: document.getElementsByClassName('emoji-textarea')[0],
        api: emojiData,
        position: 'down',
        width: '100%',
        maxHeight: '250px',
    });
}
jQuery(function($){
    //滚动分页加载数据
    $('.page_scroll_pagination').on('initDataEvent', function(){
        var self = $(this);
        var rawdata = {};
            rawdata.uri = self.attr('data-handler');
            rawdata.data = parseQueryJson(self.attr('data-query'));
            rawdata.callFun = self.attr('data-function');
            rawdata.defFun = self.attr('data-deferred') || null;
        if(!$.isEmptyObject(rawdata.data)){
            rawdata.uri += '?'+$.param(rawdata.data); 
        }
        //Deferred
        var def = $.Deferred();
        self.scrollPagination({
            url: rawdata.uri, 
            jsonCallback: function(jsonArray){
                if(jQuery.isEmptyObject(jsonArray)){
                    return;
                }
                var fn = window[rawdata.callFun];
                if (typeof fn === "function"){
                    fn.call(this, jsonArray);
                    def.resolve();
                }
            }
        });
        //
        $.when(def).done(function(){
            var fn = window[rawdata.defFun];
            if (typeof fn === "function"){
                window[rawdata.defFun].call(this);
            }
        }).fail(function(){
            console.log('[page_scroll_pagination] def fail');
        });
    }).trigger('initDataEvent');
    $('body').on('DOMNodeInserted', '#notification', function(){
        var n = $(this).html();
        if(parseInt(n) > 0){
            $('#primary-panel-notice').find('.device-notice-ele').html(n);
        }
    });
    //移动端的action-cmd
    $('body').on('click', '.action-ddm', function(){
        var self=$(this);
        var rawdata = {};
            rawdata.uri=self.attr('data-handler');
            rawdata.data=parseQueryJson(self.attr('data-query'));
        
        $.ajax({
            type: 'POST',
            url: rawdata.uri,
            data: rawdata.data,
            dataType: 'json'
        }).done(function (response) {
            response.refresh = false;
            tipDialog(response);
        }).fail(function(){
            errorMessage('当前操作因错误而异外中止');
        }).always(function(){
            cmdDrawerMenuClose(self);
        });
        return false;
    });
    //抽屉菜单触点连接
    $('body').on('click', '.drawer', function(){
        var de = $(this).attr('data-drawer'); var dtd = $.Deferred();
        var option = {};
            option.opened = $(this).attr('data-draweropen-fun');
            option.closed = $(this).attr('data-drawerclose-fun');
        if($(this).attr('data-drawer-init')){
            var initDE = $(this).attr('data-drawer-init');
            var fn = window[initDE];
            if (typeof fn === "function"){
                dtd = fn.call(this, $(this), de, dtd);
            }
        }
        dtd.done($(de).drawer(option));
    });
    //话题内容页中回复的管理
    $('body').on('click', '.reply-drawer-menu', function(){
        var ele = $(this).parents('.embed_topic_item').find('.embed_topic_item_body');
        if(!ele.attr('id')){
           return;
        }
        var drawerMenu = $('#page-reply-drawer');var eleId=ele.attr('id').replace('posts-', '');
        drawerMenu.find('.replier-report-anchor').attr('data-query', 'id:'+eleId);
        drawerMenu.find('.post-action-remove').attr('data-query', 'id:'+eleId);
        drawerMenu.drawer({closed: 'replyDrawerMenuClose'});
    });
    //(话题|回复)举报抽屉菜单的提交[Action]
    $('.drawer-report-action').click(function(e){
        var self = $(this);
        var form = self.parents('form');
        //
        var reason = form.find('textarea[name=reason]').val();
        if(isBlank(reason)){
            form.find('textarea[name=reason]').focus();
            return false;
        }
        var reportRecord = form.find('input[name=id]').val();
        if(isBlank(reportRecord)){
            errorMessage('重要参数丢失');
            return false;
        }
        var requestParam = {};
            requestParam.id=reportRecord;
            requestParam.reason=reason;
            requestParam.token=form.find('input[name=token]').val();
            requestParam.type=form.find(".report-type").find('a.focus-current').attr('data-type') || '6';
        
        $.ajax({
            url: form.attr('action'),
            dataType: 'json',
            method: 'post',
            data: requestParam
        }).done(function (response) {
            response.refresh = false;
            tipDialog(response);
        }).fail(function(){
            errorMessage('当前操作因错误而异外中止');
        }).always(function(){
            cmdDrawerMenuClose(self);
        });
    });
    //标签的单切换(只能选一个)
    $('body').on('click', '.focus-item', function(e){
        $(this).toggleClass('focus-current');
        var self = $(this);
        self.parents('.focus-item-parent').find('.focus-current').not(self).removeClass('focus-current');
    });
    //话题内容页的快速回复
    $('body').on('click', '.embed-quickreply-btn', function(){
        var self = $(this).parents('form'); 
        //
        var requestData={};
            requestData.token = self.find('input[name=token]').val();
            requestData.content=self.find('textarea[name=content]').val();
        
        if(isBlank(requestData.content)){
            tipDialog('请输入回复的内容');
            return false;
        }
        // 同步操作
        $.ajax({
            type: 'POST',
            url: self.attr('action'),
            data: requestData,
            dataType: 'json'
        }).done(function (response) {
            if(response.level === 'err' && !isBlank(response.message)){
                //出错了
                response.refresh = false;
                tipDialog(response);
            }else{
                var arr = [];
                    arr[0] = response;
                drawTabletReplier(arr);
                //清空原来的内容
                self.find('textarea[name=content]').val('');
            }
        }).fail(function(){
            errorMessage('当前操作因错误而异外中止');
        }).always(function(){
            cmdDrawerMenuClose(self);
        });
        return false;
    });
    //聊天消息页中的发送动作[平板]
    $('.drawer-message-send').on('click', function(e){
        var self = $(this);
        //
        var requestParam = parseQueryJson(self.attr('data-query'));
            requestParam.content = self.parents('form').find('.form-control').val();
        //
        if(isBlank(requestParam.content)){
            self.parents('form').find('.form-control').focus();
            return false;
        }
        self.addClass('disabled');
        $.ajax({
            url: self.attr('data-handler'),
            dataType: 'json',
            method: 'post',
            data: requestParam
        }).done(function (response) {
            window['drawMessageItem'].call(this, response, self);
        }).fail(function(){
            errorMessage('当前操作因错误而异外中止');
        }).always(function(){
            cmdDrawerMenuClose(self);
            self.removeClass('disabled');
        });
        return false;
    });
});