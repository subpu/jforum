(function($) {
    'use strict';
    var TOOLBAR = {};
        TOOLBAR.feedback = {
            title : '我要反馈',
            ico : 'ico mdi mdi-comment-alt',
            action : function(){
                var requestURI = BASE+'/feedback';
                $.confirm({
                    title: '意见反馈',
                    closeIcon: true,
                    closeIconClass: 'ico-sm mdi mdi-close',
                    content: ''+
                         '<form action="">' +
                         '<div class="form-group">' +
                             '<input class="form-control" name="subject" required="required" type="text" placeholder="输入反馈的主题"/>' +
                         '</div>' +
                         '<div class="form-group">' +
                             '<textarea class="form-control" name="content" required="required" rows="3" placeholder="输入反馈的内容"></textarea>' +
                         '</div>' +
                         '</form>',
                    buttons: {
                        formSubmit: {
                            text: '提交',
                            btnClass: 'btn-blue',
                            action: function () {
                                var content = this.$content.find('textarea[name=content]').val();
                                if(isBlank(content)){
                                    this.$content.find('textarea[name=content]').focus();
                                    return false;
                                }
                                var requestParam = {};
                                    requestParam.content=content;
                                    requestParam.title=this.$content.find("input[name=subject]").val();
                                $.ajax({
                                    url: requestURI,
                                    dataType: 'json',
                                    method: 'post',
                                    data: requestParam
                                }).done(function (response) {
                                    response.refresh = false;
                                    tipDialog(response);
                                }).fail(function(){
                                    errorMessage('当前操作因错误而异外中止');
                                });
                            }
                        }
                    },
                    onContentReady: function () {
                        var jc = this;
                        this.$content.find('form').on('submit', function (e) {
                            e.preventDefault();
                            jc.$$formSubmit.trigger('click');
                        });
                    }
                });
                return false;
            }
        };
        TOOLBAR.theme = {
            title : '切换模式',
            ico : 'ico mdi mdi-lamp',
            action : function(e){
                var cacheTheme = Cookies.get('theme');
                if(isBlank(cacheTheme)){ //默认为snowhite
                    Cookies.set('theme', 'jetblack');
                    $('body').removeClass('snowhite').addClass('jetblack');
                }else{
                    Cookies.remove('theme');
                    $('body').removeClass('jetblack').addClass('snowhite');
                }
            }
        };
        TOOLBAR.publish = {
            title : '发布主题',
            ico : 'ico mdi mdi-edit',
            action : function(e){
                $(this).attr('href', '/topic/create');
            }
        };
        TOOLBAR.poster = {
            title : '分享话题',
            ico : 'ico mdi mdi-link',
            action : function(e){
                $(this).attr('href', '/topic/poster');
            }
        };
        TOOLBAR.toTop = {
            title : '返回顶部',
            ico : 'ico mdi mdi-chevron-up',
            action : function(e){
                $('html, body').animate({ scrollTop: 0 }, 500);
            }
        };
    $.fn.sideToolBar = function() {
        var _self=this;
        var baseTool = _self.attr('data-tools');
        var plugTool = $('#sideToolPlug').attr('data-tool-plug') || null;
        //
        if(plugTool !== null){
            baseTool = plugTool + ',' + baseTool;
        }
        var baseToolArray = (baseTool+',toTop').split(',');
        console.log('[FSB]tool string: '+baseToolArray);
        //渲染
        var eleBox =$('<ul></ul>');
        for(var index in baseToolArray){
            var tbar = baseToolArray[index];
            console.log('[FSB]bar: '+tbar);
            //
            var tobj = TOOLBAR[tbar];
            if(!tobj){
                continue;
            }
            var tmp = $('<a href="javascript:;" title="'+tobj.title+'"><i class="'+tobj.ico+'"/></a>'); var tmpParent=$('<li></li>');
                tmp.on('click', tobj.action);
            if(tbar === 'toTop'){
                tmpParent.attr('id', 'topTopBar').hide();
            }
            eleBox.append(tmpParent.append(tmp));
        }
        _self.append(eleBox);
        return this;
    };
    $(window).on('scroll',function(e) {
        if ($(this).scrollTop() > 200) {
            $('#topTopBar').fadeIn(500);
        } else {
            $('#topTopBar').fadeOut(500);
        }
    });
})(jQuery);
