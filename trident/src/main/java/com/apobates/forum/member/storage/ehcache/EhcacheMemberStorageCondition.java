package com.apobates.forum.member.storage.ehcache;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * OnlineMemberEhcacheStorage条件
 *
 * @author xiaofanku
 * @since 20201001
 */
public class EhcacheMemberStorageCondition implements Condition {

    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        Environment env = context.getEnvironment();
        return null != env && "true".equalsIgnoreCase(env.getProperty("site.member.ehcache"));
    }
}
