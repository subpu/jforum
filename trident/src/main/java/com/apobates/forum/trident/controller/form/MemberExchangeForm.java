package com.apobates.forum.trident.controller.form;

import com.apobates.forum.member.entity.ForumCalendarUnitEnum;
import com.apobates.forum.utils.lang.EnumArchitecture;

/**
 *
 * @author xiaofanku
 * @since 20200921
 */
public class MemberExchangeForm extends ActionForm{
    private long memberId;
    private String names;
    private String limit;
    //单位:天
    private String unit;
    private String serial;

    public long getMemberId() {
        return memberId;
    }

    public void setMemberId(long memberId) {
        this.memberId = memberId;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public int getIntegerLimit() {
        return covertStringToInteger(getLimit(), 1);
    }

    public ForumCalendarUnitEnum getEnumUnit() {
        int unitEnumSymbol = covertStringToInteger(getUnit(), 0);
        return EnumArchitecture.getInstance(unitEnumSymbol, ForumCalendarUnitEnum.class).orElse(ForumCalendarUnitEnum.MONTH);
    }
}
