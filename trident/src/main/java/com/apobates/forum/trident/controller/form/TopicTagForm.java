package com.apobates.forum.trident.controller.form;

public class TopicTagForm extends ActionForm{
	//话题的ID
	private long topic;
	//话题的标题
	private String title;
	//标签名
	private String names;
	
	public long getTopic() {
		return topic;
	}
	public void setTopic(long topic) {
		this.topic = topic;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getNames() {
		return names;
	}
	public void setNames(String names) {
		this.names = names;
	}
}
