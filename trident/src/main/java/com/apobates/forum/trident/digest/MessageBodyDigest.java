package com.apobates.forum.trident.digest;

import java.io.Serializable;

import com.apobates.forum.letterbox.entity.ForumLetter;
import com.apobates.forum.utils.DateTimeUtils;
/**
 * 消息的内容摘要
 * 
 * @author xiaofanku
 * @since 20190704
 */
public final class MessageBodyDigest implements Serializable{
	private static final long serialVersionUID = 2098412359584964594L;
	private final long id;
	private final String subject;
	private final String body;
	private final String datetime;
	//发件人/会员名字
	private final String senderNickname;
	//发件人/会员id
	private final long sender;
	//收件人/会员id
	private final long receiver;
	//在聊天内容中是否居右,是否是主人
	private final boolean master;
	
	/**
	 * 
	 * @param letter   
	 * @param receiver 收件人
	 * @param isMaster 是否是主人
	 */
	public MessageBodyDigest(ForumLetter letter, long receiverMember, boolean isMaster){
		this.id = letter.getId();
		this.subject = letter.getTitle();
		this.body = letter.getContent();
		this.datetime = DateTimeUtils.formatClock(letter.getEntryDateTime());
		this.senderNickname = letter.getNickname();
		this.sender = letter.getAuthor();
		this.receiver = receiverMember;
		this.master = isMaster;
	}
	
	public MessageBodyDigest(ForumLetter letter, long receiverMember){
		this.id = letter.getId();
		this.subject = letter.getTitle();
		this.body = letter.getContent();
		this.datetime = DateTimeUtils.formatClock(letter.getEntryDateTime());
		this.senderNickname = letter.getNickname();
		this.sender = letter.getAuthor();
		this.receiver = receiverMember;
		this.master = true;
	}
	
	public MessageBodyDigest(long sender, String senderNickname, long receiver, String subject, String body){
		this.id=0;
		this.subject = subject;
		this.body = body;
		this.datetime = "刚刚";
		this.senderNickname = senderNickname;
		this.sender = sender;
		this.receiver = receiver;
		this.master = true;
	}
	
	public static MessageBodyDigest ofEmpty(String errorMessage){
		return new MessageBodyDigest(-1L, "NUL", 0L, "一条错误消息", errorMessage);
	}
	
	public String getBody() {
		return body;
	}
	public String getDatetime() {
		return datetime;
	}
	public String getSenderNickname() {
		return senderNickname;
	}
	public long getSender() {
		return sender;
	}
	public long getReceiver() {
		return receiver;
	}
	public long getId() {
		return id;
	}
	public boolean isMaster() {
		return master;
	}
	public String getSubject() {
		return subject;
	}
}
