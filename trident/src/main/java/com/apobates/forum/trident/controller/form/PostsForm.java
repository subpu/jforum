package com.apobates.forum.trident.controller.form;

import com.apobates.forum.utils.Commons;

public class PostsForm extends ActionForm{
	private String volumes;
	private String board;
	private String topic;
	private String content;
	//20200313新增,只在移动设备中使用
	private long quoteId;
	private long quoteFloor;
	private String quoteScale;
	
	public String getBoard() {
		return board;
	}
	public long getLongBoard(){
		return covertStringToLong(getBoard(), 0L);
	}
	public void setBoard(String board) {
		this.board = board;
	}
	public String getTopic() {
		return topic;
	}
	public long getLongTopic(){
		return covertStringToLong(getTopic(), 0L);
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getContent() {
		return Commons.isNotBlank(content)?content.trim():"";
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getVolumes() {
		return volumes;
	}
	public int getIntegerVolumes(){
		return covertStringToInteger(getVolumes(), -1);
	}
	public void setVolumes(String volumes) {
		this.volumes = volumes;
	}
	public long getQuoteId() {
		return quoteId;
	}
	public void setQuoteId(long quoteId) {
		this.quoteId = quoteId;
	}
	public long getQuoteFloor() {
		return quoteFloor;
	}
	public void setQuoteFloor(long quoteFloor) {
		this.quoteFloor = quoteFloor;
	}
	public String getQuoteScale() {
		return quoteScale;
	}
	public void setQuoteScale(String quoteScale) {
		this.quoteScale = quoteScale;
	}
}
