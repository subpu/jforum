package com.apobates.forum.trident.controller.form;

public class MemberProfileForm extends ActionForm{
	private String nickname;
	private String signature;
	
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
}
