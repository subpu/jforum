package com.apobates.forum.trident.rss;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.web.servlet.view.feed.AbstractRssFeedView;
import com.apobates.forum.utils.DateTimeUtils;
import com.rometools.rome.feed.rss.Channel;
import com.rometools.rome.feed.rss.Content;
import com.rometools.rome.feed.rss.Item;
/**
 * RSS控制器视图
 * 
 * @author xiaofanku
 * @since 20190713
 */
public class TopicRssView extends AbstractRssFeedView{
	public TopicRssView(){
		setContentType(MediaType.APPLICATION_XML_VALUE);
	}
	
	@Override
	protected List<Item> buildFeedItems(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		List<Item> items = new ArrayList<>();
		TopicRssResult data = (TopicRssResult) model.get("feeds");
		
		for (TopicRssItem tri : data.getItems()) {
			Item item = new Item();
			item.setTitle(tri.getTitle());
			item.setLink(tri.getLink());
			item.setPubDate(DateTimeUtils.toDate(tri.getDate()));
			
			Content content = new Content();
			content.setValue(tri.getContent());
			item.setContent(content);
			
			items.add(item);
		}
		return items;
	}

	@Override
	protected void buildFeedMetadata(Map<String, Object> model, Channel feed, HttpServletRequest request) {
		TopicRssResult data = (TopicRssResult) model.get("feeds");
		feed.setTitle(data.getTitle());
		feed.setLink(data.getLink());
		feed.setDescription(data.getDesc());
		super.buildFeedMetadata(model, feed, request);
	}
}
