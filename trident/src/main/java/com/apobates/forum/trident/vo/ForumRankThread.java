package com.apobates.forum.trident.vo;

import java.io.Serializable;
import java.util.Objects;
import com.apobates.forum.core.entity.Topic;
import com.apobates.forum.core.entity.TopicStats;
import com.apobates.forum.utils.DateTimeUtils;
/**
 * 话题排名VO
 * @author xiaofanku
 * @since 20200411
 */
public class ForumRankThread implements Serializable, Comparable<ForumRankThread>{
	private static final long serialVersionUID = 357519189152599681L;
	private final String title;
	private final String link;
	private final String boardName;
	private final String boardURI;
	private final String publisher;
	private final String publisherURI;
	private final String publishDate;
	//回复数
	private final long postses;
	//查看数
	private final long displaies;
	//收藏数
	private final long favorites;
	//点赞的数
	private final long likes;
	//排名
	private final int ranking;
	
	public ForumRankThread(Topic topic, TopicStats topicStats, int ranking){
		Objects.requireNonNull(topic);
		
		this.title = topic.getTitle();
		this.link = String.format("/topic/%s.xhtml", topic.getConnect());
		this.publisher = topic.getMemberNickname();
		this.publisherURI = String.format("/member/%s.xhtml", topic.getMemberId()+"");
		this.publishDate = DateTimeUtils.formatClock(topic.getEntryDateTime());
		if(null != topic.getBoard()){
			this.boardName = topic.getBoard().getTitle();
			this.boardURI = String.format("/board/%s.xhtml", topic.getBoard().getConnect());
		}else{
			this.boardName = "-";
			this.boardURI = "javascript:;";
		}
		if(null != topicStats){
			this.postses = topicStats.getPostses();
			this.displaies = topicStats.getDisplaies();
			this.favorites = topicStats.getFavorites();
			this.likes = topicStats.getLikes();
		}else{
			this.postses = 0L;
			this.displaies = 0L;
			this.favorites = 0L;
			this.likes = 0L;
		}
		this.ranking = ranking;
	}

	public ForumRankThread(Topic topic, int ranking) {
		Objects.requireNonNull(topic);
		this.title = topic.getTitle();
		this.link = String.format("/topic/%s.xhtml", topic.getConnect());
		this.publisher = topic.getMemberNickname();
		this.publisherURI = String.format("/member/%s.xhtml", topic.getMemberId() + "");
		this.publishDate = DateTimeUtils.formatClock(topic.getEntryDateTime());
		if (null != topic.getBoard()) {
			this.boardName = topic.getBoard().getTitle();
			this.boardURI = String.format("/board/%s.xhtml", topic.getBoard().getConnect());
		} else {
			this.boardName = "-";
			this.boardURI = "javascript:;";
		}
		if (null != topic.getStats()) {
			this.postses = topic.getStats().getPostses();
			this.displaies = topic.getStats().getDisplaies();
			this.favorites = topic.getStats().getFavorites();
			this.likes = topic.getStats().getLikes();
		} else {
			this.postses = 0L;
			this.displaies = 0L;
			this.favorites = 0L;
			this.likes = 0L;
		}
		this.ranking = ranking;
	}
	public String getTitle() {
		return title;
	}

	public String getLink() {
		return link;
	}

	public String getBoardName() {
		return boardName;
	}

	public String getBoardURI() {
		return boardURI;
	}

	public String getPublisher() {
		return publisher;
	}

	public String getPublisherURI() {
		return publisherURI;
	}

	public String getPublishDate() {
		return publishDate;
	}

	public long getPostses() {
		return postses;
	}

	public long getDisplaies() {
		return displaies;
	}

	public long getFavorites() {
		return favorites;
	}

	public long getLikes() {
		return likes;
	}

	public int getRanking() {
		return ranking;
	}
	
	@Override
	public int compareTo(ForumRankThread o) {
		return ranking - o.getRanking();
	}
	
}
