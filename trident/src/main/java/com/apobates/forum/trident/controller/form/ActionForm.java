package com.apobates.forum.trident.controller.form;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apobates.forum.attention.core.ImagePathCoverter;
import com.apobates.forum.trident.fileupload.ImageStorageExecutor;
import com.apobates.forum.utils.Commons;

public abstract class ActionForm implements Serializable{
	private String record = "0";
	private String token; // (message="错误代码:1051;抽像的操作标识丢失")
	private String status = "0";
	// 来源地址
	// 只允许是本站地址,例:/product
	private String refer;
	private final static Logger logger = LoggerFactory.getLogger(ActionForm.class);
	
	public String getRecord() {
		return record;
	}

	public long getLongRecord() {
		return Long.valueOf(record);
	}

	public int getIntegerRecord() {
		return Integer.valueOf(record);
	}

	public void setRecord(String record) {
		this.record = record;
	}

	public void setRecord(long record) {
		this.record = record + "";
	}

	public void setRecord(int record) {
		this.record = record + "";
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		setStatus(covertBooleanToString(status));
	}
	public Boolean getBooleanStatus() {
		return covertStringToBoolean(getStatus());
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getRefer() {
		return refer;
	}

	public void setRefer(HttpServletRequest request) {
		this.refer = getPrevRequestRefer(request);
	}

	public static String getPrevRequestRefer(HttpServletRequest request) {
		String httpReferer = request.getHeader("referer");
		if (null==httpReferer) {
			return "";
		}
		// 包含http://
		// 是否是本站地址
		ResourceBundle gisResource = ResourceBundle.getBundle("global");
		String domain = gisResource.getString("site.domain");
		if (null==domain) {
			return "";
		}
		if (!httpReferer.contains(domain)) {
			return "";
		}
		return httpReferer.substring(domain.length() - 1);
	}

	/**
	 * 是添加还是编辑
	 * 
	 * @return
	 */
	public boolean isUpdate() {
		return Long.valueOf(record) > 0;
	}

	protected String covertBooleanToString(boolean booleanValue) {
		return booleanValue ? "1" : "0";
	}

	protected boolean covertStringToBoolean(String value) {
		return Commons.isNotBlank(value) && "1".equals(value);
	}
	
	protected int covertStringToInteger(String value, int defaultValue){
		return Commons.stringToInteger(value, defaultValue);
	}
	
	protected long covertStringToLong(String value, long defaultValue){
		return Commons.stringToLong(value, defaultValue);
	}
	
	public String getActionTitle() {
		return isUpdate()?"编辑":"新增";
	}
	
	public String getRequestIpAddr(HttpServletRequest request){
		return Commons.getRequestIp(request);
	}
	/*
	 * 
	 * 
	 * @param file     上传的文件
	 * @param executor 上传的执行器
	 * @exception      FileUploadFailException 上传产生的错误
	 * @return 若上传完成获取文件名失败:新增返回默认值,编辑返回null
	 *
	protected String uploadAndEncodeFile(MultipartFile file, ImageStorageExecutor executor)throws FileUploadFailException{
		Result<String> data = Result.empty();
		try{
			data = executor.store(file);
		}catch(IOException e){
			//只关心上传产生的错误
			throw new FileUploadFailException(e.getMessage()); //只关心上传产生的错误
		}
		if(data.isFailure()){ //上传过程中的错误
			throw new FileUploadFailException(data.failureValue().getMessage());
		}
		//
		String defaultValue = "image://defat/ico/default_icon.png";
		if(data.isEmpty()){ 
			//新增返回默认值
			//编辑返回null
			return (isUpdate())?null:defaultValue;
		}
		//编码
		ImagePathCoverter ipc = new ImagePathCoverter(data.successValue());
		return ipc.encodeUploadImageFilePath(executor.imageBucketDomain(), executor.uploadImageDirectName()).getOrElse(defaultValue);
	}*/
	/**
	 * 
	 * @param fileurl  上传成功后图片的访问地址
	 * @param executor 上传的执行器
	 * @return
	 */
	protected String uploadAndEncodeFile(String fileurl, ImageStorageExecutor executor){
		//
		String defaultValue = "image://defat/ico/default_icon.png";
		if(!Commons.isNotBlank(fileurl)){ 
			//新增返回默认值
			//编辑返回null
			return (isUpdate())?null:defaultValue;
		}
		//编码
		ImagePathCoverter ipc = new ImagePathCoverter(fileurl);
		return ipc.encodeUploadImageFilePath(executor.imageBucketDomain(), executor.uploadImageDirectName()).orElse(defaultValue);
	}
}
