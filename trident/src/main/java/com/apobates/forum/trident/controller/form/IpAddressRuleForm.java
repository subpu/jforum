package com.apobates.forum.trident.controller.form;

public class IpAddressRuleForm extends ActionForm{
	private String expression;

	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}
}
