package com.apobates.forum.trident.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.apobates.forum.core.MoodCollectResult;
import com.apobates.forum.core.ad.ActiveDirectoryConnectorFactory;
import com.apobates.forum.core.api.service.PostsMoodRecordsService;
import com.apobates.forum.core.entity.Topic;
import com.apobates.forum.event.elderly.ActionEventCulpritor;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.member.storage.core.MemberSessionBean;
import com.apobates.forum.strategy.Strategy;
import com.apobates.forum.strategy.StrategyEntityParam;
import com.apobates.forum.trident.OnlineDescriptor;
import com.apobates.forum.utils.Commons;
import com.apobates.forum.utils.TipMessage;
import com.google.gson.Gson;
/**
 * 回复喜好控制器
 * 
 * @author xiaofanku
 * @since 20191201
 */
@Controller
@RequestMapping(value = "/posts/mood")
public class PostsMoodController {
	@Autowired
	private PostsMoodRecordsService postsMoodRecordsService;
	@Value("${site.domain}")
	private String siteDomain;
	
	@ModelAttribute("topicArg")
	public Topic getTopic(HttpServletRequest request){
		return ActiveDirectoryConnectorFactory.parseRefererToTopic(request.getHeader("referer"), siteDomain).orElse(null);
	}
	
	// 回复点赞
	@PostMapping(path = "/like", produces = "application/json;charset=UTF-8")
	@ResponseBody
	@OnlineDescriptor(action=ForumActionEnum.POSTS_LIKED, isAjax=true)
	@Strategy(action=ForumActionEnum.POSTS_LIKED, param=StrategyEntityParam.QUERY_STR, paramId="id")
	public TipMessage likePostsAction(
			@RequestParam("id") long postsId, 
			@RequestParam("mood") int moodStatus, 
			@RequestParam(value = "token", required = false, defaultValue = "0")String token, 
			@ModelAttribute("topicArg")Topic tpObj, 
			MemberSessionBean mbean, 
			HttpServletRequest request, 
			Model model){
		//----------------------------------从Http Referer中获取需要的参数
		if(null==tpObj || tpObj.getId()<1){
			return TipMessage.ofError("操作参数解析失败");
		}
		//----------------------------------
		ActionEventCulpritor aec = ActionEventCulpritor.getInstance(mbean.getMid(), mbean.getNickname(), request, token);
		//----------------------------------不限制角色
		//  迁至StrategyInterceptorAdapter
		//----------------------------------
		boolean liked = (moodStatus==1);
		return TipMessage.Builder.of(()->postsMoodRecordsService.toggleMoodRecord(postsId, tpObj.getId(), liked, aec)>0).success("回复点赞成功").error("点赞失败");
	}
	
	//查看点赞的计数
	@GetMapping(path = "/list/jsonp", produces = "application/javascript;charset=UTF-8")
	@ResponseBody
	public String getAllBoardForJsonp(
			@RequestParam("ids")String postsIdString, 
			@RequestParam("callback") String callBackFun, 
			@ModelAttribute("topicArg")Topic tpObj, 
			HttpServletRequest request, 
			Model model){
		//-------------------------------------------------从Http Referer中获取需要的参数
		if(null==tpObj || tpObj.getId()<1){
			return callBackFun + "({});";
		}
		//-------------------------------------------------
		Set<Long> postsIdSet = Commons.toLongSet(postsIdString);
		if(null==postsIdSet || postsIdSet.isEmpty()){
			return callBackFun + "({});";
		}
		List<MoodCollectResult> data = postsMoodRecordsService.getByPosts(tpObj.getId(), postsIdSet);
		if (data.isEmpty()) {
			return callBackFun + "({});";
		}
		Map<String, Object> result = new HashMap<>();
		result.put("topic", tpObj.getId());
		result.put("result", data);
		Gson gson = new Gson();
		return callBackFun + "(" + gson.toJson(result) + ");";
	}
}
