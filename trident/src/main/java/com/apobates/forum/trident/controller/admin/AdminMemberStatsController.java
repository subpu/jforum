package com.apobates.forum.trident.controller.admin;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.apobates.forum.member.api.service.MemberOnlineService;
import com.apobates.forum.member.api.service.MemberService;
import com.apobates.forum.member.entity.MemberGroupEnum;
import com.apobates.forum.member.entity.MemberRoleEnum;
import com.apobates.forum.member.entity.MemberStatusEnum;
import com.apobates.forum.trident.vo.CommonMember;
import com.apobates.forum.utils.CommonBean;
import com.apobates.forum.utils.DateTimeUtils;
import com.google.gson.Gson;
/**
 * 会员统计控制器
 * 
 * @author xiaofanku
 * @since 20190829
 */
@Controller
@RequestMapping(value = "/admin/member/stats")
public class AdminMemberStatsController {
	@Autowired
	private MemberService memberService;
	@Autowired
	private MemberOnlineService memberOnlineService;
	private final static Logger logger = LoggerFactory.getLogger(AdminMemberStatsController.class);
	
	@GetMapping(path="/")
	public String homePage(
			HttpServletRequest request, 
			Model model){
		//近七天会员的活跃情况
		LocalDateTime finish = LocalDateTime.now();
		LocalDateTime start = DateTimeUtils.beforeDayForDate(finish, 7);
		//没有数据填上0
		TreeMap<String, Long> data = memberService.groupMemberForBirthDate(start, finish);
		logger.info("[MS] DB Query Size: "+data.size());
		if(data == null || data.size()<7){
			data = DateTimeUtils.fillEmptyResult(start, finish, data);
		}
		logger.info("[MS] Fill Result Size: "+data.size());
		List<CommonBean> rs = new ArrayList<>();
		for(Entry<String,Long> entry : data.entrySet()){
			logger.info("[MS] loop day: "+entry.getKey()+", size: "+entry.getValue());
			rs.add(new CommonBean(entry.getValue(), entry.getKey()));
		}
		model.addAttribute("rs", new Gson().toJson(rs));
		return "admin/member/stats";
	}
	
	//最近注册的会员
	@GetMapping(path="/recent/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public List<CommonMember> recentRegisteMember(HttpServletRequest request, Model model){
		return memberService.getRecent(10).map(m -> new CommonMember(m)).collect(Collectors.toList());
	}
	
	//当前在线的会员
	@GetMapping(path="/online/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public List<CommonMember> getOnlineMember(HttpServletRequest request, Model model){
		return memberOnlineService.getForNow().map(mo -> new CommonMember(mo.getMid(), mo.getMemberNickname(), mo.getActiveDateTime())).collect(Collectors.toList());
	}
	
	//用户组的占比
	@GetMapping(path="/donut/group", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public Map<String,Long> groupMemberForGroup(HttpServletRequest request, Model model){
		Map<MemberGroupEnum,Long> rs = memberService.groupMemberForGroup();
		Map<String,Long> data = new HashMap<>();
		for(Entry<MemberGroupEnum,Long> entry : rs.entrySet()){
			data.put(entry.getKey().getTitle(), entry.getValue());
		}
		return data;
	}

	//用户角色的占比
	@GetMapping(path="/donut/role", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public Map<String,Long> groupMemberForRole(HttpServletRequest request, Model model){
		Map<MemberRoleEnum,Long> rs = memberService.groupMemberForRole();
		Map<String,Long> data = new HashMap<>();
		for(Entry<MemberRoleEnum,Long> entry : rs.entrySet()){
			data.put(entry.getKey().getTitle(), entry.getValue());
		}
		return data;
	}
	
	//用户状态的占比
	@GetMapping(path="/donut/status", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public Map<String,Long> groupMemberForStatus(HttpServletRequest request, Model model){
		Map<MemberStatusEnum,Long> rs = memberService.groupMemberForStatus();
		Map<String,Long> data = new HashMap<>();
		for(Entry<MemberStatusEnum,Long> entry : rs.entrySet()){
			data.put(entry.getKey().getTitle(), entry.getValue());
		}
		return data;
	}
}
