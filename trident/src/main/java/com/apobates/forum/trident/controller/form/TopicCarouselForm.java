package com.apobates.forum.trident.controller.form;

public class TopicCarouselForm extends ActionForm{
	private String title;
	private String summary;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
}
