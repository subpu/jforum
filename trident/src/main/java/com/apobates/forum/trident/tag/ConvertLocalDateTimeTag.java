package com.apobates.forum.trident.tag;

import java.io.IOException;
import java.time.LocalDateTime;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import com.apobates.forum.utils.DateTimeUtils;
/**
 * 格式化日期 一分钟以内(刚刚),一天以内(HH:mm),一月以内(MM-dd),其它(yyyy-MM-dd)
 * 
 * @author xiaofanku
 * @since 20190714
 */
public class ConvertLocalDateTimeTag extends SimpleTagSupport{
	private LocalDateTime value;

	public ConvertLocalDateTimeTag() {
		super();
	}

	public void setValue(LocalDateTime value) {
		this.value = value;
	}

	@Override
	public void doTag() throws JspException, IOException {
		String sb = null;
		//
		if (null!=value) {
			sb = DateTimeUtils.formatClock(value); // DateTimeUtils.getRFC3339(value);
		} else {
			sb = "-";
		}
		getJspContext().getOut().print(sb);
	}
}
