package com.apobates.forum.trident.exception;

public class RequestLostTokenException extends RuntimeException {
	private static final long serialVersionUID = 7154763977346070668L;

	public RequestLostTokenException() {
		super();
	}

	public RequestLostTokenException(String s) {
		super(s);
	}
}
