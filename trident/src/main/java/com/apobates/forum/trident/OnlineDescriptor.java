package com.apobates.forum.trident;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import com.apobates.forum.event.elderly.ForumActionEnum;
/**
 * 在线追踪描述符
 * 
 * @author xiaofanku
 * @since 20190625
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OnlineDescriptor {
	/**
	 * 返回当前的操作枚举
	 * 
	 * @return
	 */
	public ForumActionEnum action();
	
	/**
	 * 当前操作是否是ajax操作.是的话需要验证Token
	 * 
	 * @return
	 */
	public boolean isAjax()default false;
}
