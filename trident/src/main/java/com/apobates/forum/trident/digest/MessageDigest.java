package com.apobates.forum.trident.digest;

import java.io.Serializable;
import java.time.LocalDateTime;
/**
 * 未读的消息ajax响应
 * 
 * @author xiaofanku
 * @since 20190701
 */
public class MessageDigest implements Serializable{
	private static final long serialVersionUID = 90424861919651822L;
	private final long member;
	private final String sender;
	private final String title;
	private final LocalDateTime date;
	
	public MessageDigest(long member, String sender, String title, LocalDateTime date) {
		super();
		this.member = member;
		this.sender = sender;
		this.title = title;
		this.date = date;
	}

	public long getMember() {
		return member;
	}

	public String getSender() {
		return sender;
	}

	public String getTitle() {
		return title;
	}

	public LocalDateTime getDate() {
		return date;
	}
}
