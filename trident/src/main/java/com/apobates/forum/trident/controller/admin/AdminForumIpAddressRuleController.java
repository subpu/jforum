package com.apobates.forum.trident.controller.admin;

import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.apobates.forum.trident.controller.form.IpAddressRuleForm;
import com.apobates.forum.member.api.service.ForumIpAddressRuleService;
import com.apobates.forum.member.entity.ForumIpAddressRule;
import com.apobates.forum.member.storage.core.MemberSessionBean;
import com.apobates.forum.utils.Commons;
import com.apobates.forum.utils.TipMessage;
/**
 * ip地址过滤规则控制器
 * 
 * @author xiaofanku
 * @since 201900810
 */
@Controller
@RequestMapping(value = "/admin/ipaddr/rule")
public class AdminForumIpAddressRuleController {
	@Autowired
	private ForumIpAddressRuleService forumIpAddressRuleService;
	
	@GetMapping(path="/")
	public String listPage(HttpServletRequest request, Model model) {
		model.addAttribute("rs", forumIpAddressRuleService.getAll().collect(Collectors.toList()));
		return "admin/ipaddr_rule/index";
	}
	
	@GetMapping(path="/edit")
	public String editForm(@RequestParam(name="id", required=false, defaultValue="0")int id, HttpServletRequest request, Model model){
		IpAddressRuleForm form = new IpAddressRuleForm();
		ForumIpAddressRule iar = forumIpAddressRuleService.get(id).orElse(new ForumIpAddressRule());
		form.setRecord(iar.getId());
		form.setExpression(iar.getRuleExpress());
		form.setStatus(iar.isStatus());
		//
		form.setToken(Commons.randomAlphaNumeric(8));
		model.addAttribute("form", form);
		return "admin/ipaddr_rule/edit";
	}
	
	@PostMapping(path="/edit")
	public String editAction(
			@ModelAttribute("form")IpAddressRuleForm form,
			HttpServletRequest request,
			Model model){
		ForumIpAddressRule iar = new ForumIpAddressRule();
		iar.setRuleExpress(form.getExpression());
		iar.setStatus(form.getBooleanStatus());
		
		boolean symbol = false;String errMsg=null;
		try{
			if(form.isUpdate()) {
				symbol = forumIpAddressRuleService.edit(form.getIntegerRecord(), iar).orElse(false);
			}else{
				symbol = forumIpAddressRuleService.create(iar.getRuleExpress(), iar.isStatus()).isPresent();
			}
		}catch(IllegalStateException e){
			errMsg=e.getMessage();
		}
		if(symbol) {
			return "redirect:/admin/ipaddr/rule/";
		}
		model.addAttribute("errors", Commons.optional(errMsg, form.getActionTitle()+"Ip地址过滤规则操作失败"));
		//
		form.setToken(Commons.randomAlphaNumeric(8));
		model.addAttribute("form", form);
		return "admin/ipaddr_rule/edit";
	}
	
	//删除
	@PostMapping(path="/remove", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public TipMessage removeAction(
			@RequestParam("id")int id,
			MemberSessionBean mbean,
			HttpServletRequest request,
			Model model){
		return TipMessage.Builder.take(()->forumIpAddressRuleService.editStatus(id, false)).success("Ip地址过滤规则删除成功").error("操作失败");
	}
}
