package com.apobates.forum.trident.tag;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import com.apobates.forum.utils.Commons;

/**
 * 输出字行串集,如果值等于null或空字符串使用默认值 只适用字符串,不适于其它对象
 * 
 * @author xiaofanku@live.cn
 * @since 20181104
 */
public class PrintStringValue extends SimpleTagSupport {
	private String value;
	private String optional = "";

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public void doTag() throws JspException, IOException {
		getJspContext().getOut().print(Commons.optional(value, optional));
	}

	public void setOptional(String optional) {
		this.optional = optional;
	}
}
