package com.apobates.forum.trident.rss;

import java.io.Serializable;
import java.time.LocalDateTime;
/**
 * RSS条目
 * 
 * @author xiaofanku
 * @since 20190713
 */
public class TopicRssItem implements Serializable{
	private static final long serialVersionUID = -4083466236332330584L;
	private final String title;
	private final String link;
	private final String content;
	private final LocalDateTime date;
	
	public TopicRssItem(String title, String link, String content, LocalDateTime date) {
		super();
		this.title = title;
		this.link = link;
		this.content = content;
		this.date = date;
	}
	
	public String getTitle() {
		return title;
	}
	public String getLink() {
		return link;
	}
	public String getContent() {
		return content;
	}
	public LocalDateTime getDate() {
		return date;
	}
}
