package com.apobates.forum.trident.tag;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ResourceBundle;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apobates.forum.core.entity.Posts;
import com.apobates.forum.decorater.Posts.ForumPostsDecorator;
/**
 * forum:decorate标签
 * 
 * @author xiaofanku
 *
 */
public class PostsDecorateTag extends SimpleTagSupport{
	// 标签属性
	private Posts posts;
	//图片的尺寸
	private String scale="auto";
	// 内置子标签:PostsBlockDecorateTag
	private String blockTip;
	// 内置子标签:PostsModifyDecorateTag
	private String modifyTip;
	//
	private final String imageBucketDomain;
	private final String smileyBaseDirect;
	private final String uploadImageDirectName;
	private final String sensitiveDictionary;
	private final static Logger logger = LoggerFactory.getLogger(PostsDecorateTag.class);
	
	public PostsDecorateTag() {
		ResourceBundle gisResource = ResourceBundle.getBundle("global");
		this.imageBucketDomain = gisResource.getString("img.bucket.domain");
		this.smileyBaseDirect = gisResource.getString("img.bucket.smile.direct");
		this.uploadImageDirectName = gisResource.getString("img.bucket.upload.direct");
		this.sensitiveDictionary = gisResource.getString("site.sensitive.dictionary");
	}

	public void setBlockTip(String blockTip) {
		this.blockTip = blockTip;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public void setModifyTip(String modifyTip) {
		this.modifyTip = modifyTip;
	}

	public void setPosts(Posts posts) {
		this.posts = posts;
	}

	public Posts getPosts() {
		return posts;
	}
	//202001162205
	@Override
	public void doTag() throws JspException, IOException {
		// 执行嵌套的子标签
		getJspBody().invoke(null);
		//
		// 输出
		final StringWriter result = new StringWriter();
		StringBuffer sb = result.getBuffer();
		//
		PageContext ctx = (PageContext)getJspContext();
		String dictionaryFilePath = ctx.getServletContext().getRealPath("/WEB-INF/"+sensitiveDictionary);
		// 若回复中含有图片进行懒加载
		logger.info("[SCALE][PostsDecorateTag]image scale: "+scale);
		String outPostsContent = ForumPostsDecorator.init(posts)
												.block(blockTip)
												.sensitiveWord(dictionaryFilePath)
												.decorateSmileImage(imageBucketDomain, smileyBaseDirect)
												.decorateUploadImage(imageBucketDomain, uploadImageDirectName, true, scale)
												.modify(modifyTip)
												.getContent();
		sb.append(outPostsContent);
		//
		getJspContext().getOut().print(sb);
	}
}
