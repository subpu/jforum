package com.apobates.forum.trident.controller.form;

import com.apobates.forum.core.entity.ForumEntityStatusEnum;
import com.apobates.forum.trident.fileupload.ImageStorageExecutor;
import com.apobates.forum.utils.lang.EnumArchitecture;

public class BoardForm extends ActionForm{
	private String title;
	private String description;
	private String imageAddr;
	//上传后的图片地址
	private String fileurl;
	private String ranking="1";
	//ForumEntityStatusEnum
	private String entityStatus="4";
	private String volumesId;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		if (null != title && title.equalsIgnoreCase("NUL")) {
			this.title = "";
			return;
		}
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImageAddr() {
		return imageAddr;
	}
	public void setImageAddr(String imageAddr) {
		this.imageAddr = imageAddr;
	}
	public int getIntegerRanking() {
		return covertStringToInteger(getRanking(), 1);
	}
	public String getRanking() {
		return ranking;
	}
	public void setRanking(String ranking) {
		this.ranking = ranking;
	}
	public ForumEntityStatusEnum getEnumEntityStatus(){
		Integer d = covertStringToInteger(getEntityStatus(), 4);
		return EnumArchitecture.getInstance(d, ForumEntityStatusEnum.class).orElse(ForumEntityStatusEnum.ACTIVE);
	}
	public String getEntityStatus() {
		return entityStatus;
	}
	public void setEntityStatus(String entityStatus) {
		this.entityStatus = entityStatus;
	}
	public void setEntityStatus(ForumEntityStatusEnum status) {
		setEntityStatus(status.getSymbol()+"");
	}
	public int getIntegerVolumesId() {
		return covertStringToInteger(getVolumesId(), 0); //没有选择时为默认版块组
	}
	public String getVolumesId() {
		return volumesId;
	}
	public void setVolumesId(String volumesId) {
		this.volumesId = volumesId;
	}
	public String getFileurl() {
		return fileurl;
	}
	public void setFileurl(String fileurl) {
		this.fileurl = fileurl;
	}
	public String getEncodeIcoAddr(ImageStorageExecutor executor){
		return super.uploadAndEncodeFile(getFileurl(), executor);
	}
}
