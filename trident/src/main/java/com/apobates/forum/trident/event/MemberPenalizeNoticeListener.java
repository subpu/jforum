package com.apobates.forum.trident.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import com.apobates.forum.letterbox.api.service.ForumLetterService;
import com.apobates.forum.letterbox.entity.ForumLetter;
import com.apobates.forum.member.entity.MemberStatusEnum;
import com.apobates.forum.member.impl.event.MemberPenalizeEvent;
import org.springframework.stereotype.Component;

/**
 * 会员惩罚通知
 * @author xiaofanku
 * @since 20190804
 */
@Component
public class MemberPenalizeNoticeListener implements ApplicationListener<MemberPenalizeEvent>{
	@Autowired
	private ForumLetterService forumLetterService;
	private final static Logger logger = LoggerFactory.getLogger(MemberPenalizeNoticeListener.class);
	
	@Override
	public void onApplicationEvent(MemberPenalizeEvent event) {
		logger.info("[Member][PenalizeEvent][2]惩罚记录通知开始");
		forumLetterService.create(getPenalizeNotic(event.getMember(), event.getNames(), event.getReason(), event.getArrive(), event.getDuration()));
		logger.info("[Member][PenalizeEvent][2]惩罚记录通知结束");
		
	}
	
	private ForumLetter getPenalizeNotic(long member, String names, String reason, MemberStatusEnum arrive, String duration){
		return new ForumLetter(
				"会员惩罚通知", 
				String.format("理由: <blockquote>%s</blockquote> 惩罚状态: %s, 惩罚时长: %s, 如果存在异议您可以联系在线管理员", reason, arrive.getTitle(), duration), 
				member, 
				names);
	}
}
