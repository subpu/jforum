package com.apobates.forum.trident.controller.helper;

import com.apobates.forum.core.ad.ActiveDirectoryConnectorFactory;
import com.apobates.forum.core.entity.BoardTopicCategoryIndex;
import com.apobates.forum.core.entity.ForumReportTypeEnum;
import com.apobates.forum.core.entity.Posts;
import com.apobates.forum.core.entity.TopicCategory;
import com.apobates.forum.core.entity.TopicConfig;
import com.apobates.forum.core.plug.AbstractPlugTopic;
import com.apobates.forum.decorater.ForumEncoder;
import com.apobates.forum.event.elderly.ActionEventCulpritor;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.member.entity.MemberGroupEnum;
import com.apobates.forum.member.entity.MemberRoleEnum;
import com.apobates.forum.utils.Commons;
import com.apobates.forum.utils.lang.EnumArchitecture;
/**
 * 举报话题构造扩展
 *
 * @author xiaofanku
 * @since 20200530
 */
public class ReportPlugTopic extends AbstractPlugTopic {
	private final Posts posts;
	private final String reportReason;
	private final ForumReportTypeEnum reportType;
	private long topicBoard;
	private int topicVolumes;

	/**
	 *
	 * @param action
	 * @param culpritor
	 *            操作的肇事信息
	 * @param reportPosts
	 *            举报的回复,回复需要加载话题实例
	 * @param reportTypeEnumSymbol
	 *            举报的类型
	 * @param reportReason
	 *            举报的理由
	 * @param targetBoardIds
	 *            关联的版块
	 */
	public ReportPlugTopic(ForumActionEnum action, ActionEventCulpritor culpritor, Posts reportPosts, int reportTypeEnumSymbol, String reportReason, BoardTopicCategoryIndex targetBoardIds) {
		super(action, culpritor);
		this.posts = reportPosts;
		this.reportReason = reportReason;
		this.reportType = EnumArchitecture.getInstance(reportTypeEnumSymbol, ForumReportTypeEnum.class).orElse(ForumReportTypeEnum.ETC);
		try {
			topicBoard = targetBoardIds.getBoardId();
			topicVolumes = targetBoardIds.getVolumesId();
		} catch (Exception e) {
			topicBoard = 1L;
			topicVolumes = 0;
		}
	}

	@Override
	public String getTitle() {
		return String.format("%s投诉 #%s", reportType.getTitle(), Commons.randomAlphaNumeric(8)); // 不加随机性可能主键冲突
	}

	@Override
	public String getContent() {
		// 无HTML tag,Emoji安全
		final String encodePostsContent = new ForumEncoder(reportReason).noneHtmlTag().parseEmoji().getContent();
		String postsTitle = (null != posts.getTopic()) ? String.format("回复: %s", posts.getTopic().getTitle()) 	: String.format("%d楼%s的回复", posts.getFloorNumber(), posts.getMemberNickname());
		String floorLink = "#";
		if (null != posts.getTopic()) {
			floorLink = String.format("/topic/%s.xhtml#posts-%d",ActiveDirectoryConnectorFactory.generateConnectString(topicVolumes, topicBoard, posts.getTopic().getId()), posts.getId());
		}
		String sourceContent = String.format(
				"<div class=\"quote\"><blockquote><strong>引用 %d楼 %s 的回复</strong><p>%s</p><a class=\"embed-link\" href=\"%s\">%s</a></blockquote></div><br/>",
				posts.getFloorNumber(), posts.getMemberNickname(), Commons.htmlPurifier(posts.getContent()), floorLink,
				postsTitle);
		return sourceContent + "<strong>举报理由:</strong><p>" + encodePostsContent + "</p>";
	}

	@Override
	public TopicCategory getCategory() {
		return TopicCategory.report();
	}

	@Override
	public int getVolumes() {
		return topicVolumes;
	}

	@Override
	public long getBoard() {
		return topicBoard;
	}

	@Override
	public TopicConfig getConfig() {
		TopicConfig config = new TopicConfig();
		// config.setTopicId(topicId);
		config.setPrivacy(false);
		config.setReply(true);
		config.setNotify(false);
		config.setAtomPoster(false);

		config.setReadMinScore(0);
		config.setReadLowMemberGroup(MemberGroupEnum.CARD);
		config.setReadLowMemberRole(MemberRoleEnum.BM);
		config.setReadLowMemberLevel(0);

		config.setWriteMinScore(1);
		config.setWriteLowMemberGroup(MemberGroupEnum.CARD);
		config.setWriteLowMemberRole(MemberRoleEnum.BM);
		config.setWriteLowMemberLevel(1);

		return config;
	}
}
