package com.apobates.forum.trident.tag;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
/**
 * 版块,话题状态的图标
 * @author xiaofanku
 * @since 20190819
 */
public class ForumStatusIcoTag extends SimpleTagSupport{
	private int value;
	private final Map<Integer, String> icoMap;

	public ForumStatusIcoTag() {
		super();
		Map<Integer, String> data = new HashMap<>();
		data.put(0, "mdi-delete");
		data.put(1, "mdi-lock-outline");
		data.put(2, "mdi-eye");
		data.put(3, "mdi-quote");
		data.put(4, "mdi-star");
		this.icoMap = data;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public void doTag() throws JspException, IOException {
		getJspContext().getOut().print(icoMap.get(value));
	}
}
