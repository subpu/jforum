package com.apobates.forum.trident.controller.form;

public class MemberSocialInfoForm extends ActionForm{
	private String alibaba;
	private String weibo;
	private String weixin;
	private String qq;
	private String email;
	
	public String getAlibaba() {
		return alibaba;
	}
	public void setAlibaba(String alibaba) {
		this.alibaba = alibaba;
	}
	public String getWeibo() {
		return weibo;
	}
	public void setWeibo(String weibo) {
		this.weibo = weibo;
	}
	public String getWeixin() {
		return weixin;
	}
	public void setWeixin(String weixin) {
		this.weixin = weixin;
	}
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
