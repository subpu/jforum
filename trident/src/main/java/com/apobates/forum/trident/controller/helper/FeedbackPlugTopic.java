package com.apobates.forum.trident.controller.helper;

import com.apobates.forum.core.entity.BoardTopicCategoryIndex;
import com.apobates.forum.core.entity.TopicCategory;
import com.apobates.forum.core.entity.TopicConfig;
import com.apobates.forum.core.plug.AbstractPlugTopic;
import com.apobates.forum.decorater.ForumEncoder;
import com.apobates.forum.event.elderly.ActionEventCulpritor;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.member.entity.MemberGroupEnum;
import com.apobates.forum.member.entity.MemberRoleEnum;

/**
 * 意见反馈话题构造扩展
 *
 * @author xiaofanku
 * @since 20200530
 */
public class FeedbackPlugTopic extends AbstractPlugTopic {
	private final String title;
	private final String content;
	private long topicBoard;
	private int topicVolumes;

	/**
	 *
	 * @param culpritor
	 *            操作的肇事信息
	 * @param title
	 *            反馈的主题
	 * @param content
	 *            反馈的内容
	 * @param targetBoardIds
	 *            关联的版块
	 */
	public FeedbackPlugTopic(ActionEventCulpritor culpritor, String title, String content, BoardTopicCategoryIndex targetBoardIds) {
		super(ForumActionEnum.COMMON_FEEDBACK, culpritor);
		this.title = title;
		this.content = content;
		try {
			topicBoard = targetBoardIds.getBoardId();
			topicVolumes = targetBoardIds.getVolumesId();
		} catch (Exception e) {
			topicBoard = 1L;
			topicVolumes = 0;
		}
	}

	@Override
	public String getTitle() {
		// 无HTML tag,Emoji安全
		return new ForumEncoder(title).noneHtmlTag().parseEmoji().getContent();
	}

	@Override
	public String getContent() {
		// 无HTML tag,Emoji安全
		return new ForumEncoder(content).noneHtmlTag().parseEmoji().getContent();
	}

	@Override
	public TopicCategory getCategory() {
		return TopicCategory.feedback();
	}

	@Override
	public int getVolumes() {
		return topicVolumes;
	}

	@Override
	public long getBoard() {
		return topicBoard;
	}

	@Override
	public TopicConfig getConfig() {
		TopicConfig config = new TopicConfig();
		// config.setTopicId(topicId);
		config.setPrivacy(false);
		config.setReply(true);
		config.setNotify(false);
		config.setAtomPoster(false);

		config.setReadMinScore(0);
		config.setReadLowMemberGroup(MemberGroupEnum.CARD);
		config.setReadLowMemberRole(MemberRoleEnum.ADMIN);
		config.setReadLowMemberLevel(0);

		config.setWriteMinScore(1);
		config.setWriteLowMemberGroup(MemberGroupEnum.CARD);
		config.setWriteLowMemberRole(MemberRoleEnum.ADMIN);
		config.setWriteLowMemberLevel(1);

		return config;
	}
}
