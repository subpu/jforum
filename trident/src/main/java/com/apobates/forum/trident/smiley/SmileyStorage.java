package com.apobates.forum.trident.smiley;

import java.util.List;
/**
 * 表情图片查询接口
 * 
 * @author xiaofanku
 * @since 20191118
 */
public interface SmileyStorage {
	/**
	 * 返回所有表情风格的目录名称
	 * 
	 * @return
	 */
	List<String> getThemeDirectNames();
	
	/**
	 * 返回指定表情风格下的所有表情图片
	 * 
	 * @param themeDirectName 表情风格目录名称
	 * @return
	 */
	List<String> getThemePictures(String themeDirectName);
}
