package com.apobates.forum.trident.controller.form;

public class MemberPassworResetForm extends ActionForm{
	private String newpswd;
	private String confirmpswd;
	//数字签名
	private String disi;
	
	public String getNewpswd() {
		return newpswd;
	}
	public void setNewpswd(String newpswd) {
		this.newpswd = newpswd;
	}
	public String getConfirmpswd() {
		return confirmpswd;
	}
	public void setConfirmpswd(String confirmpswd) {
		this.confirmpswd = confirmpswd;
	}
	public String getDisi() {
		return disi;
	}
	public void setDisi(String disi) {
		this.disi = disi;
	}
}
