package com.apobates.forum.trident.controller.form;

public class SmileyThemeForm extends ActionForm{
	//中文名称,例:QQ表情
	private String title;
	//英文名称,例:qq
	private String label;
	//目录名称(相对于表情根目录),例:qq
	private String directNames;
	//显示顺序
	//int
	private String ranking;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getDirectNames() {
		return directNames;
	}
	public void setDirectNames(String directNames) {
		this.directNames = directNames;
	}
	public int getIntegerRanking() {
		return covertStringToInteger(getRanking(), 1);
	}
	public String getRanking() {
		return ranking;
	}
	public void setRanking(String ranking) {
		this.ranking = ranking;
	}
}
