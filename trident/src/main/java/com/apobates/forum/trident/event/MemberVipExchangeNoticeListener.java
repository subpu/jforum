package com.apobates.forum.trident.event;

import com.apobates.forum.letterbox.api.service.ForumLetterService;
import com.apobates.forum.letterbox.entity.ForumLetter;
import com.apobates.forum.member.entity.ForumCalendarUnitEnum;
import com.apobates.forum.member.entity.MemberVipExchangeRecords;
import com.apobates.forum.member.impl.event.MemberVipExchangeEvent;
import com.apobates.forum.utils.DateTimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import java.time.LocalDateTime;

@Component
public class MemberVipExchangeNoticeListener implements ApplicationListener<MemberVipExchangeEvent> {
    @Autowired
    private ForumLetterService forumLetterService;
    private final static Logger logger = LoggerFactory.getLogger(MemberVipExchangeNoticeListener.class);

    @Override
    public void onApplicationEvent(MemberVipExchangeEvent event) {
        logger.info("[Member][VipExchangeEvent][2]VIP会员开通通知开始发送");
        MemberVipExchangeRecords ver = event.getRecord();
        forumLetterService.create(getVipGroupNotic(ver.getMemberId(), ver.getMemberNickname(), ver.getActiveDateTime(), ver.getDuration(), ver.getDurationUnit()));
        logger.info("[Member][VipExchangeEvent][2]VIP会员开通通知发送结束");
    }

    private ForumLetter getVipGroupNotic(long memberId, String memberNickname, LocalDateTime activeDateTime, int duration, ForumCalendarUnitEnum durationUnit) {
        return new ForumLetter(
                "VIP会员开通通知",
                String.format("恭喜您: %s, 您的VIP身份从%s开始，有效期: %d(%s)! 如果存在异议您可以联系在线管理员",
                        memberNickname, DateTimeUtils.getRFC3339(activeDateTime), duration, durationUnit.getTitle()),
                memberId,
                memberNickname);
    }
}
