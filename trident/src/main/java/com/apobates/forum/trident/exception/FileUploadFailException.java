package com.apobates.forum.trident.exception;

/**
 * 上传产生的IO异常
 * @author xiaofanku
 * @since 20191025
 */
public class FileUploadFailException extends IllegalStateException{
	private static final long serialVersionUID = 791526507628752377L;
	public FileUploadFailException(String message){
		super(message);
	}
	public FileUploadFailException(String message, Throwable cause){
		super(message, cause);
	}
}
