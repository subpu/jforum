package com.apobates.forum.trident;

import javax.servlet.Filter;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.FrameworkServlet;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
/**
 * Spring 项目启动类
 * 
 * @author xiaofanku
 * @since 20200303
 */
public class TridentFrontInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class<?>[] {TridentAppConfig.class };
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[] { TridentFrontConfig.class };
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}
	
	@Override
	protected Filter[] getServletFilters() {
		//Filter siteMeshFilter = new org.sitemesh.config.ConfigurableSiteMeshFilter();
		DelegatingFilterProxy filterProxy = new DelegatingFilterProxy();
		filterProxy.setTargetBeanName("tokenParamFilter");
		return new Filter[]{
				new org.springframework.web.filter.CharacterEncodingFilter("UTF-8", true), 
				filterProxy, 
				new org.sitemesh.config.ConfigurableSiteMeshFilter()};
	}

	@Override
	protected FrameworkServlet createDispatcherServlet(WebApplicationContext servletAppContext) {
		DispatcherServlet ds = new DispatcherServlet(servletAppContext);
		ds.setThrowExceptionIfNoHandlerFound(true);
		return ds;
	}
}
