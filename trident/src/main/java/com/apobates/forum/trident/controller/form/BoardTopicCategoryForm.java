package com.apobates.forum.trident.controller.form;

public class BoardTopicCategoryForm extends ActionForm{
	private String board;
	private String volumes;
	private String[] category;
	
	public String getBoard() {
		return board;
	}
	public void setBoard(String board) {
		this.board = board;
	}
	public String getVolumes() {
		return volumes;
	}
	public void setVolumes(String volumes) {
		this.volumes = volumes;
	}
	public String[] getCategory() {
		return category;
	}
	public void setCategory(String[] category) {
		this.category = category;
	}
	
	public int getIntegerVolumesId() {
		return covertStringToInteger(getVolumes(), 0);
	}
	public long getLongBoardId() {
		return covertStringToLong(getBoard(), 0L);
	}
}
