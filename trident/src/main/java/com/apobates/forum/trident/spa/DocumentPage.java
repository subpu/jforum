package com.apobates.forum.trident.spa;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import com.apobates.forum.core.entity.Board;
import com.apobates.forum.core.entity.BoardGroup;
import com.apobates.forum.core.entity.Topic;

public class DocumentPage implements Serializable{
	private static final long serialVersionUID = -5189261953475542347L;
	private long id;
	//标题
	private String title;
	//简述
	private String summary;
	//正文
	private String body;
	//栏目/版块
	private String category;
	//列表项
	private Set<EntryItem> entries = new TreeSet<>();
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Set<EntryItem> getEntries() {
		return entries;
	}
	public void setEntries(Set<EntryItem> entries) {
		this.entries = entries;
	}
	/*
	public void setEntries(Map<String, Topic> prevnext) {
		if(prevnext == null || prevnext.isEmpty()){
			return;
		}
		for(Entry<String,Topic> entry:prevnext.entrySet()){
			Topic tmp = entry.getValue();
			getEntries().add(new EntryItem(tmp.getTitle(), "/spa/page?bd="+getCategory()+"&id="+tmp.getId()));
		}
	}*/
	public static DocumentPage buildHomePage(BoardGroup group, List<Board> boards){
		DocumentPage dp = new DocumentPage();
		dp.setId(group.getId());
		dp.setTitle(group.getTitle());
		dp.setSummary(group.getDescription());
		dp.setBody(group.getDescription());
		dp.setCategory(group.getDirectoryNames());
		//
		for(Board b:boards){
			dp.getEntries().add(new EntryItem(b.getTitle(), "/article/page?bd="+b.getDirectoryNames(), b.getDescription()));
		}
		return dp;
	}
	
	public static DocumentPage buildViewPage(Topic topic, Board board){
		DocumentPage dp = new DocumentPage();
		dp.setId(topic.getId());
		dp.setTitle(topic.getTitle());
		dp.setBody(topic.getContent().getContent());
		dp.setCategory(board.getDirectoryNames());
		return dp;
	}
}
