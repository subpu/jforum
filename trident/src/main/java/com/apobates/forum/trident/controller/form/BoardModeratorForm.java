package com.apobates.forum.trident.controller.form;

import com.apobates.forum.core.entity.ModeratorLevelEnum;
import com.apobates.forum.utils.lang.EnumArchitecture;

public class BoardModeratorForm extends ActionForm{
	private String volumesId;
	private String boardId;
	//ModeratorLevelEnum.symbol
	private String level="0";
	private String memberNames;
	private String memberId;
	//
	public int getIntegerVolumes(){
		return covertStringToInteger(getVolumesId(), 0);
	}
	public String getVolumesId() {
		return volumesId;
	}
	public void setVolumesId(String volumesId) {
		this.volumesId = volumesId;
	}
	public long getLongBoard(){
		return covertStringToLong(getBoardId(), 0L);
	}
	public String getBoardId() {
		return boardId;
	}
	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}
	public ModeratorLevelEnum getEnumLevel(){
		Integer d = covertStringToInteger(getLevel(), 0);
		return EnumArchitecture.getInstance(d, ModeratorLevelEnum.class).orElse(ModeratorLevelEnum.INTERN);
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(ModeratorLevelEnum level){
		setLevel(level.getSymbol()+"");
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getMemberNames() {
		return memberNames;
	}
	public void setMemberNames(String memberNames) {
		this.memberNames = memberNames;
	}
	public long getLongMember(){
		return covertStringToLong(getMemberId(), 0L);
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
}
