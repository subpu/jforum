package com.apobates.forum.trident.event;

import com.apobates.forum.core.api.service.BoardGroupService;
import com.apobates.forum.core.api.service.BoardService;
import com.apobates.forum.core.entity.Board;
import com.apobates.forum.core.entity.BoardGroup;
import com.apobates.forum.core.entity.BoardModerator;
import com.apobates.forum.core.entity.ModeratorLevelEnum;
import com.apobates.forum.core.impl.event.ModeratorBornEvent;
import com.apobates.forum.letterbox.api.service.ForumLetterService;
import com.apobates.forum.letterbox.entity.ForumLetter;
import com.apobates.forum.member.entity.MemberRoleEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ModeratorBornNoticeListener implements ApplicationListener<ModeratorBornEvent> {
    @Autowired
    private BoardService boardService;
    @Autowired
    private BoardGroupService boardGroupService;
    @Autowired
    private ForumLetterService forumLetterService;
    private final static Logger logger = LoggerFactory.getLogger(ModeratorBornNoticeListener.class);

    @Override
    public void onApplicationEvent(ModeratorBornEvent event) {
        logger.info("[Moderator][BornEvent][2]版主任命通知开始发送");
        BoardModerator bm = event.getModerator();
        MemberRoleEnum currentRole = bm.isVolumesMaster()?MemberRoleEnum.MASTER:MemberRoleEnum.BM;

        forumLetterService.create(getModeratorBornNotice(bm.getMemberId(), bm.getMemberNickname(), bm.getLevel(), currentRole, boardGroupService.get(bm.getVolumesId()), boardService.get(bm.getBoardId())));
        logger.info("[Moderator][BornEvent][2]版主任命通知发送结束");
    }
    //生成任命通知
    private ForumLetter getModeratorBornNotice(long memberId, String names, ModeratorLevelEnum level, MemberRoleEnum currentRole, Optional<BoardGroup> boardGroup, Optional<Board> board) {
        String section = "";
        if(MemberRoleEnum.MASTER == currentRole && boardGroup.isPresent()){
            BoardGroup bgr = boardGroup.get();
            section=String.format("<a class=\"embed-link\" href=\"/board/volumes/%s.xhtml\">%s</a>的", bgr.getConnect(), bgr.getTitle());
        }

        if(MemberRoleEnum.BM == currentRole && boardGroup.isPresent() && board.isPresent()){
            BoardGroup bgr = boardGroup.get();
            Board br = board.get();
            section=String.format("<a class=\"embed-link\" href=\"/board/volumes/%s.xhtml\">%s</a> &#187; <a class=\"embed-link\" href=\"/board/%s.xhtml\">%s</a>的",
                    bgr.getConnect(), bgr.getTitle(),
                    br.getConnect(), br.getTitle());
        }
        String content = String.format("恭喜您加入社区管理团队, 您现在是%s%s%s", section, level.getTitle(), currentRole.getTitle());
        //(String title, String content, long author, String authorNames, ForumLetterTypeEnum type)
        return new ForumLetter("版主任命通知", content, memberId, names);
    }
}
