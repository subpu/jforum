package com.apobates.forum.trident.controller.admin;

import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.apobates.forum.trident.controller.form.RegisterInvitecodeForm;
import com.apobates.forum.member.api.service.RegisteInviteCodeService;
import com.apobates.forum.member.entity.RegisteInviteCode;
import com.apobates.forum.member.storage.core.MemberSessionBean;
import com.apobates.forum.utils.Commons;
import com.apobates.forum.utils.FrontPageURL;
import com.apobates.forum.utils.TipMessage;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.PageRequest;
import com.apobates.forum.utils.persistence.Pageable;
/**
 * 会员注册邀请码控制器
 * 
 * @author xiaofanku@live.cn
 * @since 20190718
 */
@Controller
@RequestMapping(value = "/admin/member/invitecode")
public class AdminRegisteInviteCodeController {
	@Autowired
	private RegisteInviteCodeService registeInviteCodeService;
	@Value("${site.pageSize}")
	private int pageSize;
	
	@GetMapping(path="/")
	public String listPage(
			@RequestParam(value = "p", required = false, defaultValue = "1") int page, 
			HttpServletRequest request, 
			Model model) {
		FrontPageURL fpbuild = new FrontPageURL(request.getContextPath() + "/admin/member/invitecode/").addPageSize("number", pageSize);
		Pageable pr = new PageRequest(page, fpbuild.getPageSize());
		Page<RegisteInviteCode> rs = registeInviteCodeService.get(pr);
		model.addAttribute("rs", rs.getResult().collect(Collectors.toList()));
		model.addAttribute("pageData", pr.toData(fpbuild, rs.getTotalElements()));
		return "admin/member_invitecode/index";
	}
	//创建
	@GetMapping(path="/create")
	public String createForm(
			HttpServletRequest request,
			Model model){
		RegisterInvitecodeForm form = new RegisterInvitecodeForm();
		form.setCode(Commons.randomAlphaNumeric(10));
		//
		form.setToken(Commons.randomAlphaNumeric(8));
		model.addAttribute("form", form);
		return "admin/member_invitecode/create";
	}
	@PostMapping(path="/create")
	public String createAction(
			@ModelAttribute("form") RegisterInvitecodeForm form, 
			MemberSessionBean mbean, 
			HttpServletRequest request,
			Model model){
		long primaryKey = registeInviteCodeService.create(mbean.getNickname(), form.getCode());
		if(primaryKey>0){
			return "redirect:/admin/member/invitecode/";
		}
		form.setToken(Commons.randomAlphaNumeric(8));
		model.addAttribute("form", form);
		return "admin/member_invitecode/create";
	}
	//禁用
	@PostMapping(path="/remove", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public TipMessage removeInvitecodeAction(
			@RequestParam("id")long id,
			MemberSessionBean mbean,
			HttpServletRequest request,
			Model model){
		return TipMessage.Builder.take(()->registeInviteCodeService.remove(id)).success("邀请码删除成功").error("操作失败");
	}
}
