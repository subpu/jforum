package com.apobates.forum.trident;


public class JdbcProperties {
	private String driverClassName; 
	private String url;
	private String username;  
	private String password; 
	private int poolInit;  
	private int poolMinIdle; 
	private int poolMaxActive;  
	private String poolTestSql;
	private String jpaUnitName;
	
	public String getDriverClassName() {
		return driverClassName;
	}
	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getPoolInit() {
		return poolInit;
	}
	public void setPoolInit(int poolInit) {
		this.poolInit = poolInit;
	}
	public int getPoolMinIdle() {
		return poolMinIdle;
	}
	public void setPoolMinIdle(int poolMinIdle) {
		this.poolMinIdle = poolMinIdle;
	}
	public int getPoolMaxActive() {
		return poolMaxActive;
	}
	public void setPoolMaxActive(int poolMaxActive) {
		this.poolMaxActive = poolMaxActive;
	}
	public String getPoolTestSql() {
		return poolTestSql;
	}
	public void setPoolTestSql(String poolTestSql) {
		this.poolTestSql = poolTestSql;
	}
	public String getJpaUnitName() {
		return jpaUnitName;
	}
	public void setJpaUnitName(String jpaUnitName) {
		this.jpaUnitName = jpaUnitName;
	}
}
