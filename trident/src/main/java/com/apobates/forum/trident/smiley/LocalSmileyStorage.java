package com.apobates.forum.trident.smiley;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apobates.forum.core.api.ImageIOMetaConfig;
import com.apobates.forum.utils.Commons;
/**
 * 站内存储表情图片
 * @deprecated 改用Emoji而停止使用
 * 
 * @author xiaofanku
 * @since 20191118
 */
public class LocalSmileyStorage implements SmileyStorage{
	//servletContext.getRealPath("/")
	private final String localRootPath;
	private final ImageIOMetaConfig imageIOMeta;
	private final static Logger logger = LoggerFactory.getLogger(LocalSmileyStorage.class);
	
	/**
	 * 
	 * @param localRootPath servletContext.getRealPath("/")的结果
	 */
	public LocalSmileyStorage(String localRootPath, ImageIOMetaConfig imageIOMeta) {
		super();
		this.localRootPath = localRootPath;
		this.imageIOMeta = imageIOMeta;
		if(logger.isDebugEnabled()){
			logger.debug("[SmileyStorage]现在是本地存储");
		}
	}

	@Override
	public List<String> getThemeDirectNames() {
		String smileyBasePath = localRootPath + imageIOMeta.getSmileyDirectName() + "/";
		return Commons.queryFolderOfDirectoryName(smileyBasePath);
	}

	@Override
	public List<String> getThemePictures(final String themeDirectName) {
		//Objects.requireNonNull(themeDirectName);
		if(!Commons.isNotBlank(themeDirectName)){
			return Collections.EMPTY_LIST;
		}
		String smileyThemePath = localRootPath + imageIOMeta.getSmileyDirectName() + "/" + themeDirectName + "/";
		List<String> data = Commons.queryFolderOfFileName(smileyThemePath, ".gif");
		//
		Function<String, String> covertPath = path -> imageIOMeta.getImageBucketDomain() + "/" + imageIOMeta.getSmileyDirectName() + "/" + themeDirectName + "/" + path;
		return data.stream().map(covertPath).collect(Collectors.toList());
	}

}
