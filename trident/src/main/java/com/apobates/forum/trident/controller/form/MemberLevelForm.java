package com.apobates.forum.trident.controller.form;

import com.apobates.forum.trident.fileupload.ImageStorageExecutor;
import com.apobates.forum.utils.Commons;

public class MemberLevelForm extends ActionForm{
	/**
	 * 等级名称
	 */
	private String names;
	/**
	 * 最少积分/double
	 */
	private String minScore;
	/**
	 * 上限积分/double
	 */
	private String score;
	/**
	 * 等级图标
	 */
	private String imageAddr;
	//上传后的图片地址
	private String fileurl;
	
	public String getNames() {
		return names;
	}

	public void setNames(String names) {
		if (null != names && names.equalsIgnoreCase("NUL")) {
			this.names = "";
			return;
		}
		this.names = names;
	}
	public double getDoubleMinScore(){
		return Commons.stringToDouble(()->getMinScore(), 1.0D);
	}
	public String getMinScore() {
		return minScore;
	}

	public void setMinScore(String minScore) {
		this.minScore = minScore;
	}
	public double getDoubleScore(){
		return Commons.stringToDouble(()->getScore(), 1.0D);
	}
	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getImageAddr() {
		return imageAddr;
	}

	public void setImageAddr(String imageAddr) {
		this.imageAddr = imageAddr;
	}
	public String getFileurl() {
		return fileurl;
	}
	public void setFileurl(String fileurl) {
		this.fileurl = fileurl;
	}
	public String getEncodeIcoAddr(ImageStorageExecutor executor){
		return super.uploadAndEncodeFile(getFileurl(), executor);
	}
}
