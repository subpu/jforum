package com.apobates.forum.trident.controller.admin;

import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.apobates.forum.core.api.service.BoardActionCollectionService;
import com.apobates.forum.core.entity.BoardActionCollection;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.utils.FrontPageURL;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.PageRequest;
import com.apobates.forum.utils.persistence.Pageable;
/**
 * 版块操作控制器
 * 
 * @author xiaofanku
 * @since 20190407
 */
@Controller
@RequestMapping(value = "/admin/board/action")
public class AdminBoardActionCollectionController {
	@Autowired
	private BoardActionCollectionService boardActionCollectionService;
	@Value("${site.pageSize}")
	private int pageSize;
	
	@GetMapping(path="/")
	public String listPage(
			@RequestParam(value = "p", required = false, defaultValue = "1") int page, 
			@RequestParam(value = "id", required = false, defaultValue = "0") long boardId, 
			HttpServletRequest request, 
			Model model) {
		FrontPageURL fpbuild = new FrontPageURL(request.getContextPath() + "/admin/board/action/").addPageSize("number", pageSize);
		if(boardId>0){
			fpbuild = fpbuild.addParameter("id", boardId);
		}
		Pageable pr = new PageRequest(page, fpbuild.getPageSize());
		Page<BoardActionCollection> rs = (boardId>0)?boardActionCollectionService.getByBoard(boardId, pr):boardActionCollectionService.getAll(pr);
		model.addAttribute("rs", rs.getResult().collect(Collectors.toList()));
		model.addAttribute("pageData", pr.toData(fpbuild, rs.getTotalElements()));
		model.addAttribute("paramBoard", boardId);
		return "admin/board/action";
	}
	//版块收藏者
	@GetMapping(path="/star")
	public String getBoardFavoriters(
			@RequestParam("id")long boardId, 
			@RequestParam(value = "p", required = false, defaultValue = "1") int page, 
			HttpServletRequest request, 
			Model model){
		FrontPageURL fpbuild = new FrontPageURL(request.getContextPath() + "/admin/board/action/star").addParameter("id", boardId).addPageSize("number", pageSize);
		Pageable pr = new PageRequest(page, fpbuild.getPageSize());
		Page<BoardActionCollection> rs = boardActionCollectionService.getByBoard(boardId, ForumActionEnum.BOARD_FAVORITE, pr);
		model.addAttribute("rs", rs.getResult().collect(Collectors.toList()));
		model.addAttribute("pageData", pr.toData(fpbuild, rs.getTotalElements()));
		return "admin/board/star";
	}
}
