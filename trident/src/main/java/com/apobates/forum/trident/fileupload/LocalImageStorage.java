package com.apobates.forum.trident.fileupload;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import javax.servlet.ServletException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;
import com.apobates.forum.utils.DateTimeUtils;
import com.apobates.forum.utils.fileupload.CommonInitParamers;
import com.apobates.forum.utils.fileupload.ckeditor.CKEditorHightHandler;

/**
 * 站内存储上传的图片
 * 
 * @author xiaofanku
 * @since 20191024
 */
public class LocalImageStorage implements ImageStorageExecutor{
	private final String imageBucketDomain;
	private final String uploadImageDirectName;
	//servletContext.getRealPath("/")
	private final String localRootPath;
	private final long uploadMaxByte;
	private final static Logger logger = LoggerFactory.getLogger(LocalImageStorage.class);
	
	/**
	 * 
	 * @param imageBucketDomain     本站的域名,例:http://x.com
	 * @param uploadImageDirectName 存储图片的目录名称
	 * @param localRootPath         servletContext.getRealPath("/")的结果
	 */
	public LocalImageStorage(String imageBucketDomain, String uploadImageDirectName, String localRootPath, long uploadMaxByte) {
		if(logger.isDebugEnabled()){
			logger.debug("[ImageStorageExecutor]现在是本地存储");
		}
		this.imageBucketDomain = imageBucketDomain;
		this.uploadImageDirectName = uploadImageDirectName;
		this.localRootPath = localRootPath;
		this.uploadMaxByte = uploadMaxByte;
	}
	
	/**
	 * 如果FormBean不使用MultipartFile此方法不建议调用
	 */
	@Override
	public Optional<String> store(MultipartFile file) throws IOException {
		//空白
		if(null==file || file.isEmpty()){
			return Optional.empty(); //("文件不存在或不可用");
		}
		//子目录
		String childDirect = DateTimeUtils.getYMD();
		// 项目路径
		final String realPath = localRootPath + File.separator + uploadImageDirectName + File.separator +childDirect;
		// 前台访问路径
		final String frontVisitPath = imageBucketDomain + "/" + uploadImageDirectName + "/" + childDirect + "/";
		CommonInitParamers cip = new CommonInitParamers() {
			@Override
			public String getFileSaveDir() {
				return realPath;
			}
			@Override
			public String getCallbackFunctionName() {
				// 没有回调函数
				return null;
			}
		};
		
		// 使用fileupload
		SpringFileUploadConnector connector = new SpringFileUploadConnector(new CKEditorHightHandler(frontVisitPath, uploadMaxByte));
		try{
			return Optional.ofNullable(connector.upload(cip, file)); //返回文件的访问地址
		} catch(ServletException e){
			if(logger.isDebugEnabled()){
				logger.debug("[ServletException]异常: "+e.getMessage());
			}
		} catch(MultipartException e){
			if(logger.isDebugEnabled()){
				logger.debug("[MultipartException]异常: "+e.getMessage());
			}
		}
		return Optional.empty();
	}

	@Override
	public String imageBucketDomain() {
		return imageBucketDomain;
	}

	@Override
	public String uploadImageDirectName() {
		return uploadImageDirectName;
	}

	public long getUploadMaxByte() {
		return uploadMaxByte;
	}
}
