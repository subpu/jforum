package com.apobates.forum.trident.controller.form;

public class MemberPasswordLostForm extends ActionForm{
	private String email;
	private String names;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNames() {
		return names;
	}
	public void setNames(String names) {
		this.names = names;
	}
}
