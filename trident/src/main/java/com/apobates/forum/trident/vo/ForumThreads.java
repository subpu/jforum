package com.apobates.forum.trident.vo;

import java.io.Serializable;
import com.apobates.forum.core.entity.Topic;
import com.apobates.forum.core.entity.TopicStats;
import com.apobates.forum.utils.Commons;
import com.apobates.forum.utils.DateTimeUtils;
/**
 * 封装(避免对外暴露主类)版块的最新主题[Topic.VO.2]
 * 
 * @author xiaofanku
 * @since 20190417
 */
public final class ForumThreads implements Serializable{
	private static final long serialVersionUID = 1L;
	//主题
	private final String title;
	//主题的连接
	private final String link;
	//作者ID
	private final long author;
	//作者的昵称
	private final String authorNames;
	//发布日期
	private final String date;
	//查看数
	private final long views;
	//回复数
	private final long replies;
	//最近回复者ID
	private final long recentAuthor;
	//最近回复者的昵称
	private final String recentAuthorNames;
	//最近回复日期
	private final String recentDate;
	//最近回复ID
	private final long replier;
	//置顶
	private final boolean top;
	//图片
	private final boolean image;
	//精华
	private final boolean best;
	//火贴
	private final boolean hot;
	//分类(中文)
	private final String category;
	//分类(英文)
	private final String categoryKey;
	//话题的状态
	private final String status;
	//版块的连接
	private final String boardLink;
	//封面图?
	private final long album;
	
	public String getTitle() {
		return title;
	}
	public long getViews() {
		return views;
	}
	public long getReplies() {
		return replies;
	}
	public long getReplier() {
		return replier;
	}
	public boolean isTop() {
		return top;
	}
	public boolean isImage() {
		return image;
	}
	public boolean isBest() {
		return best;
	}
	public boolean isHot() {
		return hot;
	}
	public String getCategory() {
		return category;
	}
	public String getCategoryKey() {
		return categoryKey;
	}
	public String getLink() {
		return link;
	}
	public long getAuthor() {
		return author;
	}
	public String getAuthorNames() {
		return authorNames;
	}
	public String getDate() {
		return date;
	}
	public long getRecentAuthor() {
		return recentAuthor;
	}
	public String getRecentAuthorNames() {
		return recentAuthorNames;
	}
	public String getRecentDate() {
		return recentDate;
	}
	public String getStatus() {
		return status;
	}
	public String getBoardLink() {
		return boardLink;
	}
	public long getAlbum() {
		return album;
	}
	
	public ForumThreads(Topic topic) {
		this(topic, topic.getStatsNullOfInstance());
	}
	
	private ForumThreads(Topic topic, TopicStats topicStats) {
		super();
		this.title = topic.getTitle();
		this.link = String.format("/topic/%s.xhtml", topic.getConnect());
		this.boardLink = String.format("/board/%s.xhtml", topic.getLazyBoard().getConnect());
		this.author = topic.getMemberId();
		this.authorNames = topic.getMemberNickname();
		this.date = DateTimeUtils.formatClock(topic.getEntryDateTime());
		this.views = topicStats.getDisplaies();
		this.replies = topicStats.getPostses();
		this.recentAuthor = topicStats.getRecentPostsMemberId();
		this.recentAuthorNames = Commons.optional(topicStats.getRecentPostsMemberNickname(), "");
		this.recentDate = DateTimeUtils.formatClock(topicStats.getRecentPostsDate());
		this.replier = topicStats.getRecentPostsId();
		this.top = topic.isTops();
		this.image = topic.isImage();
		this.best = topic.isGoods();
		this.hot = topic.isHots();
		this.category = topic.getTopicCategoryName();
		this.categoryKey = topic.getTopicCategoryValue();
		this.status = topic.getIcoStatus();
		this.album = topic.getAlbumId();
	}
}
