package com.apobates.forum.trident.controller.helper;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * 策略开启条件
 * 
 * @author xiaofanku
 * @since 20200829
 */
public class StrategyCondition implements Condition{
    //https://stackoverflow.com/questions/29844271/conditional-spring-bean-creation
    @Override
    public boolean matches(ConditionContext cc, AnnotatedTypeMetadata atm) {
        Environment env = cc.getEnvironment();
        return null != env && "true".equalsIgnoreCase(env.getProperty("site.strategy"));
    }
}
