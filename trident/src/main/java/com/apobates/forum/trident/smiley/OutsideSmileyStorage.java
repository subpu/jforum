package com.apobates.forum.trident.smiley;

import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import com.apobates.forum.core.api.ImageIOMetaConfig;
import com.apobates.forum.utils.Commons;
/**
 * 站外存储表情图片
 * @deprecated 改用Emoji而停止使用
 * @author xiaofanku
 * @since 20191118
 */
public class OutsideSmileyStorage implements SmileyStorage{
	private final ImageIOMetaConfig imageIOMeta;
	private final static Logger logger = LoggerFactory.getLogger(OutsideSmileyStorage.class);
	/**
	 * 
	 * @param imageIOInfoBean 图片存储信息
	 */
	public OutsideSmileyStorage(ImageIOMetaConfig imageIOMeta) {
		super();
		this.imageIOMeta = imageIOMeta;
		if(logger.isDebugEnabled()){
			logger.debug("[SmileyStorage]现在是站外存储");
		}
	}

	@Override
	public List<String> getThemeDirectNames() {
		final String requestURI = imageIOMeta.getImageBucketDomain()+"/smiley/theme";
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		try{
			ResponseEntity<List<String>> response = restTemplate.exchange(
				requestURI,
				HttpMethod.GET,
				new HttpEntity<String>(headers),
				new ParameterizedTypeReference<List<String>>(){});
			if(HttpStatus.OK == response.getStatusCode()){
				return response.getBody();
			}
		}catch(RestClientException e){
			if(logger.isDebugEnabled()){
				logger.debug("[SmileyTheme]远程通信失败", e);
			}
		}
		return Collections.EMPTY_LIST;
	}

	@Override
	public List<String> getThemePictures(final String themeDirectName) {
		if(!Commons.isNotBlank(themeDirectName)){
			return Collections.EMPTY_LIST;
		}
		final String requestURI = imageIOMeta.getImageBucketDomain()+"/smiley/theme/picture?theme="+themeDirectName;
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		try{
			ResponseEntity<List<String>> response = restTemplate.exchange(
				requestURI,
				HttpMethod.GET,
				new HttpEntity<String>(headers),
				new ParameterizedTypeReference<List<String>>(){});
			if(HttpStatus.OK == response.getStatusCode()){
				return response.getBody();
			}
		}catch(RestClientException e){
			if(logger.isDebugEnabled()){
				logger.debug("[SmileyThemePicture]远程通信失败", e);
			}
		}
		return Collections.EMPTY_LIST;
	}

}
