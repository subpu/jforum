package com.apobates.forum.trident.controller.admin;

import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.apobates.forum.trident.exception.ResourceNotFoundException;
import com.apobates.forum.core.api.ImageIOMeta;
import com.apobates.forum.core.api.service.PostsMoodRecordsService;
import com.apobates.forum.core.api.service.PostsService;
import com.apobates.forum.core.entity.Posts;
import com.apobates.forum.core.entity.PostsMoodRecords;
import com.apobates.forum.event.elderly.ActionEventCulpritor;
import com.apobates.forum.member.storage.core.MemberSessionBean;
import com.apobates.forum.utils.FrontPageURL;
import com.apobates.forum.utils.TipMessage;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.PageRequest;
import com.apobates.forum.utils.persistence.Pageable;
/**
 * 话题回复控制器
 * 
 * @author xiaofanku
 * @since 20190407
 */
@Controller
@RequestMapping(value = "/admin/topic/posts")
public class AdminPostsController {
	@Autowired
	private PostsService postsService;
	@Autowired
	private PostsMoodRecordsService postsMoodRecordsService;
	@Autowired
	private ImageIOMeta imageIOConfig;
	@Value("${site.pageSize}")
	private int pageSize;
	@Value("${site.sensitive.dictionary}")
	private String sensitiveDictionary;
	
	//指定话题的回复
	@GetMapping(path="/")
	public String listPage(
			@RequestParam("topic")long topicId, 
			@RequestParam(value = "p", required = false, defaultValue = "1") int page, 
			HttpServletRequest request, 
			Model model) {
		FrontPageURL fpbuild = new FrontPageURL(request.getContextPath() + "/admin/topic/posts/").addParameter("topic", topicId).addPageSize("number", pageSize);
		Pageable pr = new PageRequest(page, fpbuild.getPageSize());
		Page<Posts> rs = postsService.getAll(topicId, pr);
		model.addAttribute("rs", rs.getResult().collect(Collectors.toList()));
		model.addAttribute("pageData", pr.toData(fpbuild, rs.getTotalElements()));
		return "admin/posts/index";
	}
	
	//查看回复内容
	@GetMapping(path="/view")
	public String editForm(@RequestParam("id")long id, HttpServletRequest request, Model model) {
		String dictionaryFilePath = request.getServletContext().getRealPath("/WEB-INF/"+sensitiveDictionary);
		Posts posts = postsService.getPostsContentForQuote(id, imageIOConfig, true, "auto", dictionaryFilePath).orElseThrow(()->new ResourceNotFoundException("回复内容不存在或暂时无法访问"));
		model.addAttribute("posts", posts);
		return "admin/posts/view";
	}
	
	//删除回复
	@PostMapping(path="/remove", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public TipMessage removePostsAction(
			@RequestParam("id")long postsId,
			MemberSessionBean mbean,
			HttpServletRequest request,
			Model model){
		ActionEventCulpritor aec = ActionEventCulpritor.getInstance(mbean.getMid(), mbean.getNickname(), request, "");
		return TipMessage.Builder.take(()->postsService.remove(postsId, aec)).success("回复删除成功").error("操作失败");
	}
	
	//指定回复的喜好记录
	@GetMapping(path="/mood")
	public String getPostsMoodes(
			@RequestParam("posts")long postsId, 
			@RequestParam(value = "p", required = false, defaultValue = "1") int page, 
			MemberSessionBean mbean, 
			HttpServletRequest request, 
			Model model){
		Posts posts = postsService.get(postsId).orElseThrow(()->new ResourceNotFoundException("回复内容不存在或暂时无法访问"));
		model.addAttribute("posts", posts);
		
		FrontPageURL fpbuild = new FrontPageURL(request.getContextPath() + "/admin/topic/posts/mood").addParameter("posts", postsId).addPageSize("number", pageSize);
		Pageable pr = new PageRequest(page, fpbuild.getPageSize());
		Page<PostsMoodRecords> rs = postsMoodRecordsService.getAllByPosts(postsId, pr);
		model.addAttribute("rs", rs.getResult().collect(Collectors.toList()));
		model.addAttribute("pageData", pr.toData(fpbuild, rs.getTotalElements()));
		return "admin/posts/mood";
	}
}
