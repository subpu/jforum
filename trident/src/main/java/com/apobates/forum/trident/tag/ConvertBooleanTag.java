package com.apobates.forum.trident.tag;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class ConvertBooleanTag extends SimpleTagSupport{
	private boolean value;
	private String trueTitle;
	private String falseTitle = "";

	public void setValue(boolean value) {
		this.value = value;
	}

	@Override
	public void doTag() throws JspException, IOException {
		getJspContext().getOut().print((value) ? trueTitle : falseTitle);
	}

	public void setTrueTitle(String trueTitle) {
		this.trueTitle = trueTitle;
	}

	public void setFalseTitle(String falseTitle) {
		this.falseTitle = falseTitle;
	}
}
