package com.apobates.forum.trident.vo;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.apobates.forum.member.entity.Member;
import com.apobates.forum.utils.DateTimeUtils;
/**
 * 会员统计的VO(AdminMemberStatsController)
 * 
 * @author xiaofanku
 * @since 20190901
 *
 */
public final class CommonMember implements Serializable{
	private static final long serialVersionUID = 7009253130599060667L;
	//会员ID
	private final long id;
	//登录帐号
	private final String names;
	//注册日期
	private final String date;
	
	public CommonMember(long id, String names, LocalDateTime registeDateTime) {
		super();
		this.id = id;
		this.names = names;
		this.date = DateTimeUtils.formatClock(registeDateTime);
	}

	public CommonMember(Member member){
		this.id = member.getId();
		this.names = member.getNames();
		this.date = DateTimeUtils.formatClock(member.getRegisteDateTime());
	}
	
	public long getId() {
		return id;
	}

	public String getNames() {
		return names;
	}

	public String getDate() {
		return date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((names == null) ? 0 : names.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommonMember other = (CommonMember) obj;
		if (names == null) {
			if (other.names != null)
				return false;
		} else if (!names.equals(other.names))
			return false;
		return true;
	}
}
