package com.apobates.forum.trident.event;

import com.apobates.forum.core.api.service.BoardGroupService;
import com.apobates.forum.core.api.service.BoardService;
import com.apobates.forum.core.entity.Board;
import com.apobates.forum.core.entity.BoardGroup;
import com.apobates.forum.core.entity.BoardModeratorRoleHistory;
import com.apobates.forum.core.impl.event.ModeratorRecallEvent;
import com.apobates.forum.letterbox.api.service.ForumLetterService;
import com.apobates.forum.letterbox.entity.ForumLetter;
import com.apobates.forum.member.api.service.MemberService;
import com.apobates.forum.member.entity.Member;
import com.apobates.forum.member.entity.MemberRoleEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ModeratorRecallNoticeListener implements ApplicationListener<ModeratorRecallEvent> {
    @Autowired
    private BoardService boardService;
    @Autowired
    private BoardGroupService boardGroupService;
    @Autowired
    private MemberService memberSerivce;
    @Autowired
    private ForumLetterService forumLetterService;
    private final static Logger logger = LoggerFactory.getLogger(ModeratorRecallNoticeListener.class);

    @Override
    public void onApplicationEvent(ModeratorRecallEvent event) {
        logger.info("[Moderator][RecallEvent][2]版主卸任通知开始发送");
        final BoardModeratorRoleHistory mrh = event.getRemoveRoleHistory();
        final MemberRoleEnum currentRole = event.getUpdateRole();
        Optional<Member> member = memberSerivce.get(mrh.getMemberId());
        member.ifPresent(m -> {
            forumLetterService.create(getModeratorRecallNotice(m.getId(), m.getNickname(), boardGroupService.get(mrh.getVolumesId()), boardService.get(mrh.getBoardId()), currentRole));
        });
        logger.info("[Moderator][RecallEvent][2]版主卸任通知发送结束");
    }
    //生成卸任通知
    private ForumLetter getModeratorRecallNotice(long memberId, String names, Optional<BoardGroup> boardGroup, Optional<Board> board, MemberRoleEnum currentRole) {
        String section = "";
        if(MemberRoleEnum.MASTER == currentRole && boardGroup.isPresent()){
            BoardGroup bgr = boardGroup.get();
            section=String.format("<a class=\"embed-link\" href=\"/board/volumes/%s.xhtml\">%s</a>的大", bgr.getConnect(), bgr.getTitle());
        }

        if(MemberRoleEnum.BM == currentRole && boardGroup.isPresent() && board.isPresent()){
            BoardGroup bgr = boardGroup.get();
            Board br = board.get();
            section=String.format("<a class=\"embed-link\" href=\"/board/volumes/%s.xhtml\">%s</a> &#187; <a class=\"embed-link\" href=\"/board/%s.xhtml\">%s</a>的",
                    bgr.getConnect(), bgr.getTitle(),
                    br.getConnect(), br.getTitle());
        }
        String content = String.format("您现在已经辞去%s版主! 感谢您对社区孜孜不倦的工作付出, 您当前的会员角色: %s", section, currentRole.getTitle());
        return new ForumLetter("版主卸任通知", content, memberId, names);
    }
}
