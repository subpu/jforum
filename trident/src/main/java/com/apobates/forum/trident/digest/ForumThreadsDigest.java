package com.apobates.forum.trident.digest;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.apobates.forum.core.entity.Topic;
import com.apobates.forum.utils.Commons;
import com.apobates.forum.utils.DateTimeUtils;
/**
 * 右侧的话题摘要[Topic.VO.3]
 * 
 * @author xiaofanku
 * @since 20190422
 */
public class ForumThreadsDigest implements Serializable{
	private static final long serialVersionUID = 301467155856214156L;
	//话题的标题
	private final String title;
	//话题的连接
	private final String link;
	//话题的作者
	private final long author;
	//话题的作者
	private final String authorNames;
	//发布的日期
	private final String date;
	
	public ForumThreadsDigest(Topic topic) {
		super();
		this.title = topic.getTitle();
		this.link = "/topic/"+topic.getConnect()+".xhtml";
		this.author = topic.getMemberId();
		this.authorNames = topic.getMemberNickname();
		this.date = DateTimeUtils.formatClock(topic.getEntryDateTime());
	}

	public String getTitle() {
		return title;
	}
	
	public String getDate() {
		return date;
	}
	
	public String getLink() {
		return link;
	}
	
	public long getAuthor() {
		return author;
	}
	
	public String getAuthorNames() {
		return authorNames;
	}
	
	public Map<String, String> toMap(){
		Map<String,String> data = new HashMap<>();
		data.put("title", getTitle());
		data.put("link", getLink());
		data.put("author", getAuthor()+"");
		data.put("authorNames", getAuthorNames());
		data.put("date", getDate());
		return data;
	}

	/**
	 * 若extInfoMap中的key与toMap的key冲突,以toMap为准
	 * 
	 * @param extInfoMap
	 *            扩展的会员信息
	 * @return
	 */
	public Map<String, String> toMergeMap(Map<String, String> extInfoMap) {
		return Commons.mergeMap(toMap(), extInfoMap, (oldValue, newValue) -> oldValue);
	}
}
