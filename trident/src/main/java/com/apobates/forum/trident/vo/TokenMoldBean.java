package com.apobates.forum.trident.vo;

import java.io.Serializable;
/**
 * token参数塑造Bean(RequestTokenParameterFilter)
 * 
 * @author xiaofanku
 * @since 20191130
 */
public class TokenMoldBean implements Serializable{
	private static final long serialVersionUID = 6475137066651914655L;
	//Token值
	private final String token;
	//是否继承上一个页的,init=false(是继承的),true不是
	private final boolean inherit;
	
	/**
	 * 初始化
	 * @param token
	 * @param inherit 是否是继承上一个页的token. true是,false不是继承上一页的
	 */
	public TokenMoldBean(String token, boolean inherit) {
		super();
		this.token = token;
		this.inherit = inherit;
	}
	
	/**
	 * 初始化新的token
	 * @param token
	 */
	public TokenMoldBean(String token) {
		super();
		this.token = token;
		this.inherit = false; //是新生成的
	}
	
	public String getToken() {
		return token;
	}

	public boolean isInherit() {
		return inherit;
	}

}
