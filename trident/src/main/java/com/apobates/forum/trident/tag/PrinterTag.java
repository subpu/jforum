package com.apobates.forum.trident.tag;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import com.apobates.forum.utils.Commons;

/**
 * c:out的改进版
 * 
 * @author xiaofanku
 * @since 20190712
 */
public class PrinterTag extends SimpleTagSupport {
	private String value;
	private String defValue;

	@Override
	public void doTag() throws JspException, IOException {
		getJspContext().getOut().print(Commons.isNotBlank(value) ? value : defValue);
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setDefValue(String defValue) {
		this.defValue = defValue;
	}
}
