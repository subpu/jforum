package com.apobates.forum.trident.tag;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apobates.forum.utils.Commons;
import com.apobates.forum.utils.Pagination;
/**
 * 带有页码区段的分页票签:{上一页 | 页码区段(1,2,3,4,...) | 下一页 }
 * @author xiaofanku
 * @since 20190731
 */
public class PaginationTag extends SimpleTagSupport{
	// 总数量(必需)
	private int total;
	// 不代分页参数的地址(必需)
	private String url;
	// 每页显示的记录数(必需)
	private int size;
	// 页码参数(可选)
	// private String pageParam="p";
	private final static String NEWLINE = Commons.NEWLINE;
	private final static Logger logger = LoggerFactory.getLogger(PaginationTag.class);
	private final Map<String, String> notPageNumberTitle;

	public PaginationTag() {
		notPageNumberTitle = new HashMap<>();
		notPageNumberTitle.put(Pagination.PAGE_PREV, "<span aria-hidden=\"true\">&laquo;</span>");
		notPageNumberTitle.put(Pagination.PAGE_NEXT, "<span aria-hidden=\"true\">&raquo;</span>");
	}

	@Override
	public void doTag() throws JspException, IOException {
		PageContext pc = (PageContext) getJspContext();
		HttpServletRequest request = (HttpServletRequest) pc.getRequest();

		int page = 1;
		try {
			page = Integer.valueOf(request.getParameter("p"));
		} catch (NumberFormatException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("[PT]转化动态分页页码数失败", e);
			}
		}

		if (logger.isDebugEnabled()) {
			String descrip = "[PaginationTag]" + NEWLINE;
					descrip += "/*----------------------------------------------------------------------*/" + NEWLINE;
					descrip += " request url: " + url + NEWLINE;
					descrip += " page number: " + page + NEWLINE;
					descrip += " page size: " + size + NEWLINE;
					descrip += " record total: " + total + NEWLINE;
					descrip += "/*----------------------------------------------------------------------*/" + NEWLINE;
			logger.debug(descrip);
		}
		//无记录不输出分页标签
		if(total == 0){
			// 输出标签体
			getJspContext().getOut().print("");
			return;
		}
		Pagination frontPage = new Pagination(url, page, size, total);
		// 有序Map
		Map<String, String> struct = frontPage.draw(false, 10, notPageNumberTitle);
		// 输出
		StringWriter result = new StringWriter();
		StringBuffer sb = result.getBuffer();
		Set<String> anchors = struct.keySet();
		for (String anchor : anchors) {
			// 连接的描点
			getJspContext().setAttribute("anchor", anchor);
			// 连接的地址
			getJspContext().setAttribute("uri", struct.get(anchor));
			getJspBody().invoke(null);
		}
		getJspContext().getOut().print(sb);
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setSize(int size) {
		this.size = size;
	}
}
