package com.apobates.forum.trident.controller.form;

import com.apobates.forum.letterbox.entity.ForumLetterTypeEnum;
import com.apobates.forum.utils.lang.EnumArchitecture;
/**
 * 后台的消息创建表单
 * @author xiaofanku
 * 
 */
public class ForumMessageFullForm extends ActionForm{
	//以u开头后跟Member.id
	private String uid;
	//仅供显示用的(可以是会员的登录帐号或称)
	private String snames;
	//enum
	private String label;
	private String title;
	private String content;
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getSnames() {
		return snames;
	}
	public void setSnames(String snames) {
		this.snames = snames;
	}
	public long getMemberId(){
		return covertStringToLong(getUid().substring(1), 0L);
	}
	private int getIntegerLabel(){
		return covertStringToInteger(getLabel(), -1);
	}
	public ForumLetterTypeEnum getEnumLabel(){
		return EnumArchitecture.getInstance(getIntegerLabel(), ForumLetterTypeEnum.class).orElse(ForumLetterTypeEnum.LETTER);
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
}
