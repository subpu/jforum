package com.apobates.forum.trident.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.apobates.forum.trident.controller.form.SmileyThemeForm;
import com.apobates.forum.trident.smiley.SmileyStorage;
import com.apobates.forum.core.api.service.SmileyThemeService;
import com.apobates.forum.core.entity.SmileyTheme;
import com.apobates.forum.utils.Commons;
import com.google.gson.Gson;
/**
 * 表情风格控制器
 * 
 * @author xiaofanku@live.cn
 * @since 20190531
 */
@Controller
@RequestMapping(value = "/admin/smiley/theme")
public class AdminSmileyThemeController {
	@Autowired
	private SmileyThemeService smileyThemeService;
	@Autowired
	private SmileyStorage smileyStorage;
	
	@GetMapping(path="/")
	public String listPage(HttpServletRequest request, Model model) {
		Stream<SmileyTheme> rs = smileyThemeService.getAll();
		model.addAttribute("rs", rs.collect(Collectors.toList()));
		return "admin/smiley_theme/index";
	}
	
	@GetMapping(path="/edit")
	public String themeForm(@RequestParam(name="id", required=false, defaultValue="0") int id, HttpServletRequest request, Model model) {
		SmileyThemeForm form = new SmileyThemeForm();
		SmileyTheme st = smileyThemeService.get(id).orElse(SmileyTheme.empty(id));
		//
		form.setDirectNames(st.getDirectNames());
		form.setLabel(st.getLabel());
		form.setTitle(st.getTitle());
		form.setRanking(st.getRanking()+"");
		form.setRecord(id);
		form.setStatus(st.isStatus());
		//
		form.setToken(Commons.randomAlphaNumeric(8));
		model.addAttribute("form", form);
		return "admin/smiley_theme/edit";
	}
	
	@PostMapping(path="/edit")
	public String themeAction(@ModelAttribute("form") SmileyThemeForm form, HttpServletRequest request, Model model) {
		SmileyTheme st = new SmileyTheme(form.getTitle(), form.getLabel(), form.getDirectNames(), form.getBooleanStatus(), form.getIntegerRanking());
		//
		boolean symbol = false;String errMsg=null;
		try{
			if(form.isUpdate()) {
				symbol = smileyThemeService.edit(form.getIntegerRecord(), st).orElse(false);
			}else { 
				symbol = smileyThemeService.create(st.getTitle(), st.getLabel(), st.getDirectNames(), st.isStatus(), st.getRanking()).isPresent();
			}
		}catch(IllegalStateException e){
			errMsg=e.getMessage();
		}
		if(symbol) {
			return "redirect:/admin/smiley/theme/";
		}
		model.addAttribute("errors", Commons.optional(errMsg, form.getActionTitle()+"表情风格操作失败"));
		//
		form.setToken(Commons.randomAlphaNumeric(8));
		model.addAttribute("form", form);
		return "admin/smiley_theme/edit";
	}
	//所有表情风格
	@GetMapping(path="/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public Map<String,String> getAllForJson(HttpServletRequest request, Model model){
		return smileyThemeService.getAll().collect(Collectors.toMap(SmileyTheme::getDirectNames, SmileyTheme::getTitle));
	}
	
	//所有的表情风格目录名
	@GetMapping(path = "/direct.jsonp", produces = "application/javascript;charset=UTF-8")
	@ResponseBody
	public String querySmileyThemes(
			@RequestParam("callback") String callBackFun,
			@RequestParam(value = "box", required = false, defaultValue = "null") String boxEle, 
			HttpServletRequest request, 
			Model model) {
		List<String> data = smileyStorage.getThemeDirectNames();
		if (data.isEmpty()) {
			return callBackFun + "({});";
		}
		Map<String, Object> result = new HashMap<>();
		result.put("element", boxEle);
		result.put("result", data);
		Gson gson = new Gson();
		return callBackFun + "(" + gson.toJson(result) + ");";
	}

}
