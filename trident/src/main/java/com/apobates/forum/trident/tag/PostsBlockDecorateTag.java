package com.apobates.forum.trident.tag;

import java.io.IOException;
import java.io.StringWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
/**
 * forum:decorate 标签内的子标签:forum:block
 * 
 * @author xiaofanku
 *
 */
public class PostsBlockDecorateTag extends SimpleTagSupport{
	@Override
	public void doTag() throws JspException, IOException {
		// 获得标签内容体
		final StringWriter stringWriter = new StringWriter();
		getJspBody().invoke(stringWriter);

		// 父标签:PostsDecorateTag
		PostsDecorateTag parentTag = (PostsDecorateTag) getParent();
		if (null==parentTag) {
			throw new JspTagException("标签与父标签:PostsDecorateTag,配合使用");
		}
		parentTag.setBlockTip(stringWriter.toString());
	}
}
