package com.apobates.forum.trident.controller.admin;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.apobates.forum.core.api.service.TopicService;
import com.apobates.forum.core.entity.Board;
import com.apobates.forum.core.entity.ForumEntityStatusEnum;
import com.apobates.forum.core.entity.Topic;
import com.apobates.forum.trident.vo.CommonThreads;
import com.apobates.forum.utils.CommonBean;
import com.apobates.forum.utils.DateTimeUtils;
import com.google.gson.Gson;
/**
 * 话题统计控制器
 * 
 * @author xiaofanku
 * @since 20190829
 */
@Controller
@RequestMapping(value = "/admin/topic/stats")
public class AdminTopicStatsController {
	@Autowired
	private TopicService topicService;
	private final static Logger logger = LoggerFactory.getLogger(AdminTopicStatsController.class);
	
	@GetMapping(path="/")
	public String homePage(
			HttpServletRequest request, 
			Model model){
		//近七天的话题
		LocalDateTime finish = LocalDateTime.now();
		LocalDateTime start = DateTimeUtils.beforeDayForDate(finish, 7);
		//没有数据填上0
		TreeMap<String, Long> data = topicService.groupTopicesForDate(start, finish);
		logger.info("[TS] DB Query Size: "+data.size());
		if(data == null || data.size()<7){
			data = DateTimeUtils.fillEmptyResult(start, finish, data);
		}
		logger.info("[TS] Fill Result Size: "+data.size());
		List<CommonBean> rs = new ArrayList<>();
		for(Entry<String,Long> entry : data.entrySet()){
			logger.info("[TS] loop day: "+entry.getKey()+", size: "+entry.getValue());
			rs.add(new CommonBean(entry.getValue(), entry.getKey()));
		}
		model.addAttribute("rs", new Gson().toJson(rs));
		return "admin/topic/stats";
	}
	
	//最近发布的话题
	@GetMapping(path="/recent/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public List<CommonThreads> recentTopic(HttpServletRequest request, Model model){
		return topicService.getRecentIgnoreCondition(10).map(t -> new CommonThreads(t)).collect(Collectors.toList());
	}
	
	//最近回复的话题
	@GetMapping(path="/reply/json", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public List<CommonThreads> recentReplyTopic(HttpServletRequest request, Model model){
		return topicService.getRecentReply(10).sorted(Comparator.comparing(Topic::getRankingDateTime).reversed()).map(t -> new CommonThreads(t)).collect(Collectors.toList());
	}
	
	//版块占比
	@GetMapping(path="/donut/board", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public Map<String,Long> groupTopicForBoard(HttpServletRequest request, Model model){
		Map<Board,Long> rs = topicService.groupTopicesForBoard();
		Map<String,Long> data = new HashMap<>();
		for(Entry<Board,Long> entry : rs.entrySet()){
			data.put(entry.getKey().getTitle(), entry.getValue());
		}
		return data;
	}
	
	//状态占比
	@GetMapping(path="/donut/status", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public Map<String,Long> groupTopicForStatus(HttpServletRequest request, Model model){
		Map<ForumEntityStatusEnum,Long> rs = topicService.groupTopicesForStatus();
		Map<String,Long> data = new HashMap<>();
		for(Entry<ForumEntityStatusEnum,Long> entry : rs.entrySet()){
			data.put(entry.getKey().getTitle(), entry.getValue());
		}
		return data;
	}
	
	//分类占比
	@GetMapping(path="/donut/category", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public Map<String,Long> groupTopicForCategory(HttpServletRequest request, Model model){
		return topicService.groupTopicesForCategory();
	}
}
