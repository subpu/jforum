package com.apobates.forum.trident.fileupload;

import java.io.IOException;
import java.util.Optional;
import org.springframework.web.multipart.MultipartFile;
/**
 * 图片存储的执行器
 * 
 * @author xiaofanku
 * @since 20191024
 */
public interface ImageStorageExecutor extends ImageStorage{
	/**
	 * 存储图片
	 * 
	 * @param file 图片文件
	 * @return 成功返回图片的访问连接
	 * @throws IOException
	 */
	Optional<String> store(MultipartFile file) throws IOException;
}
