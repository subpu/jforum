package com.apobates.forum.trident.exception;

/**
 * 新注册的会员在冰封期创建话题或回复时产生
 * 
 * @author xiaofanku
 * @since 20190710
 */
public class MemberFreezeException extends InterruptedException {
	private static final long serialVersionUID = -7680984341880871572L;

	public MemberFreezeException(String message) {
		super(message);
	}

	public MemberFreezeException() {
		super();
	}
}
