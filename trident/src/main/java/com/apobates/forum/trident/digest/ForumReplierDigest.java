package com.apobates.forum.trident.digest;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import com.apobates.forum.core.entity.Posts;
import com.apobates.forum.member.MemberBaseProfile;
import com.apobates.forum.utils.Commons;
import com.apobates.forum.utils.DateTimeUtils;
/**
 * 延迟加载话题的回复摘要
 * 
 * @author xiaofanku
 * @since 20191210
 */
public class ForumReplierDigest implements Serializable{
	private static final long serialVersionUID = 1969703219658846579L;
	private long id;
	//作者ID
	private final long author;
	//作者的昵称
	private final String authorNames;
	private final String authorGroup;
	//作者的风格(撒色的)
	private final String authorStyle;
	private final String authorSignature;
	//发布日期
	private final String date;
	//回复的内容
	private final String content;
	//回复的楼层
	private final long floor;
	//是否阻塞
	private final boolean block;
	//编辑日期
	private final String modifyDate;
	//编辑的会员昵称
	private final String modifyer;
	//是否是楼主
	private final boolean master;
	
	public ForumReplierDigest(
			long id, 
			long author, 
			String authorNames, 
			String authorGroup, 
			String authorStyle, 
			String authorSignature,
			LocalDateTime date, 
			String content, 
			long floor, 
			boolean isMaster, 
			boolean isBlock, 
			String modifyer, 
			LocalDateTime modifyDate) {
		super();
		this.id = id;
		this.author = author;
		this.authorNames = authorNames;
		this.authorGroup = authorGroup;
		this.authorStyle = authorStyle;
		this.authorSignature = authorSignature;
		this.date = DateTimeUtils.formatClock(date);
		this.content = (isBlock)?"*":content;
		this.floor = floor;
		this.master = isMaster;
		this.block = isBlock;
		this.modifyer = modifyer;
		this.modifyDate = (modifyDate!=null)?DateTimeUtils.formatClock(modifyDate):"";
	}
	
	public ForumReplierDigest(Posts posts, long oneFloorMaster){
		Objects.requireNonNull(posts.getMember());
		this.id = posts.getId();
		this.author = posts.getMemberId();
		this.authorNames = posts.getMemberNickname();
		this.authorGroup = posts.getMember().getMgroup().getTitle();
		this.authorStyle = MemberBaseProfile.getStyle(posts.getMember().getMgroup(), posts.getMember().getMrole());
		this.authorSignature = posts.getMember().getSignature();
		this.date = DateTimeUtils.formatClock(posts.getEntryDateTime());
		this.floor = posts.getFloorNumber();
		this.master = (oneFloorMaster == posts.getMemberId());
		this.block = (!posts.isNormal());
		this.content = (this.block)?"*":posts.getContent();
		if(posts.getModifyMemberId() <=0){
			this.modifyer = "-1";
			this.modifyDate = "";
		}else{
			this.modifyer = posts.getModifyMemberNickname();
			this.modifyDate = DateTimeUtils.formatClock(posts.getModifyDateTime());
		}
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getAuthor() {
		return author;
	}

	public String getAuthorNames() {
		return authorNames;
	}

	public String getAuthorStyle() {
		return authorStyle;
	}

	public String getAuthorSignature() {
		return authorSignature;
	}

	public String getDate() {
		return date;
	}

	public String getContent() {
		return content;
	}

	public long getFloor() {
		return floor;
	}

	public boolean isBlock() {
		return block;
	}

	public String getModifyDate() {
		return modifyDate;
	}

	public String getModifyer() {
		return modifyer;
	}

	public boolean isMaster() {
		return master;
	}

	public String getAuthorGroup() {
		return authorGroup;
	}
	
	public String toJsonString(){
		return Commons.toJson(this);
	}
}
