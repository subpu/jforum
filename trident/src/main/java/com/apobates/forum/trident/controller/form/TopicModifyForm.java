package com.apobates.forum.trident.controller.form;

import com.apobates.forum.utils.Commons;

public class TopicModifyForm extends ActionForm{
	private String title;
	private String content;
	private String words;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getWords() {
		return words;
	}
	public void setWords(String words) {
		this.words = words;
	}
	
	public String[] getWordsArray(){
		if(Commons.isNotBlank(getWords())){
			return getWords().split(",");
		}
		return new String[]{};
	}
}
