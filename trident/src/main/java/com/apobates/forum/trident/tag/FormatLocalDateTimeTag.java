package com.apobates.forum.trident.tag;

import java.io.IOException;
import java.time.LocalDateTime;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import com.apobates.forum.utils.DateTimeUtils;
/**
 * RFC3339格式化日期
 * 
 * @author xiaofanku
 * @since 20190714
 */
public class FormatLocalDateTimeTag extends SimpleTagSupport{
	private LocalDateTime value;
	
	@Override
	public void doTag() throws JspException, IOException {
		String sb = null;
		// 
		if (value != null) {
			sb = DateTimeUtils.getRFC3339(value);
		} else {
			sb = "-";
		}
		getJspContext().getOut().print(sb);
	}
	
	public void setValue(LocalDateTime value) {
		this.value = value;
	}
	
}
