package com.apobates.forum.trident.vo;

import java.io.Serializable;
import java.time.LocalDateTime;
import com.apobates.forum.core.entity.Topic;
import com.apobates.forum.utils.DateTimeUtils;
/**
 * 话题统计的VO(AdminTopicStatsController)
 * 
 * @author xiaofanku
 * @since 20190831
 */
public final class CommonThreads implements Serializable{
	private static final long serialVersionUID = -8166100915524345533L;
	//话题ID
	private final long id;
	//话题主题
	private final String title;
	//发布的日期
	private final String date;
	//作者ID
	private final long member;
	//作者使用的昵称
	private final String author;
	
	public CommonThreads(long id, String title, LocalDateTime date, long member, String author) {
		super();
		this.id = id;
		this.title = title;
		this.date = DateTimeUtils.formatClock(date);
		this.member = member;
		this.author = author;
	}

	public CommonThreads(Topic topic){
		this.id = topic.getId();
		this.title = topic.getTitle();
		this.date = DateTimeUtils.formatClock(topic.getRankingDateTime());
		this.member = topic.getMemberId();
		this.author = topic.getMemberNickname();
	}
	
	public long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getDate() {
		return date;
	}

	public long getMember() {
		return member;
	}

	public String getAuthor() {
		return author;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommonThreads other = (CommonThreads) obj;
		if (id != other.id)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
}
