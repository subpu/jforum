package com.apobates.forum.trident.controller.form;

import com.apobates.forum.core.entity.ForumEntityStatusEnum;
import com.apobates.forum.utils.lang.EnumArchitecture;

public class SectionTermForm extends ActionForm{
	private String title;
	private String direct;
	private String description;
	//int|boardGroupId
	private String section;
	//ForumEntityStatusEnum
	private String entityStatus="4";
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDirect() {
		return direct;
	}
	public void setDirect(String direct) {
		this.direct = direct;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getIntegerSection() {
		return covertStringToInteger(getSection(), 0);
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public ForumEntityStatusEnum getEnumEntityStatus(){
		int d = covertStringToInteger(getEntityStatus(), 4);
		return EnumArchitecture.getInstance(d, ForumEntityStatusEnum.class).orElse(ForumEntityStatusEnum.ACTIVE);
	}
	public String getEntityStatus() {
		return entityStatus;
	}
	public void setEntityStatus(String entityStatus) {
		this.entityStatus = entityStatus;
	}
	public void setEntityStatus(ForumEntityStatusEnum status) {
		setEntityStatus(status.getSymbol()+"");
	}
}
