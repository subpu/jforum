package com.apobates.forum.trident.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import com.apobates.forum.core.entity.BoardModeratorRoleHistory;
import com.apobates.forum.core.impl.event.ModeratorRecallEvent;
import com.apobates.forum.member.api.dao.MemberDao;
import org.springframework.stereotype.Component;

/**
 * 版主卸任事件的角色变更监听器
 * 
 * @author xiaofanku
 * @since 20200831
 */
@Component
public class ModeratorRecallChangeRoleListener implements ApplicationListener<ModeratorRecallEvent>{
    @Autowired
    private MemberDao memberDao;
    private final static Logger logger = LoggerFactory.getLogger(ModeratorRecallChangeRoleListener.class);
    //从BoardModeratorDaoImpl.deleteModerator中取消MemberDao
    @Override
    public void onApplicationEvent(ModeratorRecallEvent e) {
        logger.info("[Moderator][RecallEvent][1]版主卸任角色变更开始");
        BoardModeratorRoleHistory bmrh = e.getRemoveRoleHistory();
        memberDao.editMemberRole(bmrh.getMemberId(), e.getUpdateRole());
        logger.info("[Moderator][RecallEvent][1]版主卸任角色变更结束");
    }
}
