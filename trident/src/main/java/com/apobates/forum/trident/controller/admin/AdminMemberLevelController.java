package com.apobates.forum.trident.controller.admin;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.apobates.forum.trident.controller.form.MemberLevelForm;
import com.apobates.forum.trident.fileupload.ImageStorageExecutor;
import com.apobates.forum.member.api.service.MemberLevelService;
import com.apobates.forum.member.entity.MemberLevel;
import com.apobates.forum.utils.Commons;
/**
 * 会员等级定义控制器
 * 
 * @author xiaofanku@live.cn
 * @since 20190323
 */
@Controller
@RequestMapping(value = "/admin/member/level")
public class AdminMemberLevelController {
	@Autowired
	private MemberLevelService memberLevelService;
	@Autowired
	private ImageStorageExecutor imageStorageExecutor;
	private final static Logger logger = LoggerFactory.getLogger(AdminMemberLevelController.class);
	
	@GetMapping(path="/")
	public String listPage(HttpServletRequest request, Model model) {
		Stream<MemberLevel> rs = memberLevelService.getAll();
		model.addAttribute("rs", rs.collect(Collectors.toList()));
		return "admin/member_level/index";
	}
	@GetMapping(path="/edit")
	public String editLevelForm(@RequestParam(name="id", required=false, defaultValue="0")int id, HttpServletRequest request, Model model) {
		MemberLevelForm form = new MemberLevelForm();
		MemberLevel ml = memberLevelService.get(id).orElse(MemberLevel.empty());
		form.setRecord(id);
		form.setStatus(ml.isStatus());
		form.setNames(ml.getNames());
		form.setMinScore(ml.getMinScore()+"");
		form.setScore(ml.getScore()+"");
		form.setImageAddr(ml.getImageAddr()); //编码后的图标图片地址|解码由标签负责
		//
		form.setToken(Commons.randomAlphaNumeric(8));
		model.addAttribute("form", form);
		return "admin/member_level/edit";
	}
	@PostMapping(path="/edit")
	public String editLevelAction(@ModelAttribute("form")MemberLevelForm form, HttpServletRequest request, Model model) {
		MemberLevel ml = new MemberLevel();
		ml.setNames(form.getNames());
		ml.setStatus(form.getBooleanStatus());
		ml.setScore(form.getDoubleScore());
		ml.setMinScore(form.getDoubleMinScore());
		//------------------------------------------------------------编码图标图片的地址
		String icoImageAddr = form.getEncodeIcoAddr(imageStorageExecutor);
		if(icoImageAddr != null){
			ml.setImageAddr(icoImageAddr);
		}
		//------------------------------------------------------------
		Optional<MemberLevel> data = Optional.empty();
		String errMsg=null;
		try{
			if(form.isUpdate()) {
				data = memberLevelService.edit(form.getIntegerRecord(), ml.getNames(), ml.getMinScore(), ml.getScore(), ml.getImageAddr(), ml.isStatus());
			}else {
				data = memberLevelService.create(ml.getNames(), ml.getMinScore(), ml.getScore(), ml.getImageAddr(), ml.isStatus());
			}
		}catch(IllegalStateException e){
			errMsg=e.getMessage();
		}
		if(data.isPresent()) {
			return "redirect:/admin/member/level/";
		}
		model.addAttribute("errors", Commons.optional(errMsg, form.getActionTitle()+"会员等级定义操作失败"));
		//
		form.setToken(Commons.randomAlphaNumeric(8));
		model.addAttribute("form", form);
		return "admin/member_level/edit";
	}
}
