package com.apobates.forum.trident.vo;

import java.io.Serializable;
import com.apobates.forum.core.entity.BoardStats;
import com.apobates.forum.core.entity.Topic;
import com.apobates.forum.utils.DateTimeUtils;
/**
 * 版块统计(推送信息)
 * 
 * @author xiaofanku
 * @since 20191022
 */
public class ForumBoardStatsRecord implements Serializable{
	private static final long serialVersionUID = 1278961971387410186L;
	//版块ID
	private final long board;
	//版块的回复数
	private final long postses;
	//版块的主题数
	private final long topices;
	//今日主题数
	private final long todayTopics;
	//版块最近的话题信息
	//最近话题主题
	private final String recentTitle;
	//最近话题连接
	private final String recentLink;
	//话题发布日期
	private final String recentDate;
	//话题的作者/会员ID
	private final long recentAuthor;
	//话题的作者/会员昵称/Member.nickname
	private final String recentAuthorNames;

	public ForumBoardStatsRecord(BoardStats boardStats) {
		super();
		this.postses = boardStats.getPostses();
		this.topices = boardStats.getTopices();
		this.todayTopics = boardStats.getTodayTopices();
		this.board = boardStats.getBoardId();
		
		if(boardStats.getRecentTopicId() > 0){
			Topic t = Topic.empty(boardStats.getVolumesId(), boardStats.getBoardId());
			t.setId(boardStats.getRecentTopicId());
			t.setTitle(boardStats.getRecentTopicTitle());
			
			this.recentLink = String.format("/topic/%s.xhtml", t.getConnect());
			this.recentTitle = t.getTitle();
			this.recentAuthor = boardStats.getRecentTopicMemberId();
			this.recentAuthorNames = boardStats.getRecentTopicMemberNickname();
			this.recentDate = DateTimeUtils.formatClock(boardStats.getUpdateDate());
		}else{
			this.recentLink ="#";
			this.recentTitle = "";
			this.recentDate = "-";
			this.recentAuthor = 0;
			this.recentAuthorNames = "*";
		}
	}
	public long getPostses() {
		return postses;
	}
	public long getTopices() {
		return topices;
	}
	public String getRecentTitle() {
		return recentTitle;
	}
	public String getRecentDate() {
		return recentDate;
	}
	public long getTodayTopics() {
		return todayTopics;
	}
	public String getRecentLink() {
		return recentLink;
	}
	public long getRecentAuthor() {
		return recentAuthor;
	}
	public String getRecentAuthorNames() {
		return recentAuthorNames;
	}
	public long getBoard() {
		return board;
	}
	
}
