package com.apobates.forum.trident.exception;

public class ResourceNotFoundException extends RuntimeException {
	private static final long serialVersionUID = -5557180634674606186L;

	public ResourceNotFoundException(String message) {
		super(message);
	}

	public ResourceNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
