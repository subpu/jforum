package com.apobates.forum.trident.controller.admin;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.apobates.forum.trident.exception.ResourceNotFoundException;
import com.apobates.forum.trident.smiley.SmileyStorage;
import com.apobates.forum.core.SmileyPictureNotes;
import com.apobates.forum.core.api.service.SmileyThemePictureService;
import com.apobates.forum.core.api.service.SmileyThemeService;
import com.apobates.forum.core.entity.SmileyTheme;
import com.apobates.forum.core.entity.SmileyThemePicture;
import com.apobates.forum.utils.TipMessage;
/**
 * 表情图片控制器
 * 
 * @author xiaofanku@live.cn
 * @since 20190531
 */
@Controller
@RequestMapping(value = "/admin/smiley/pic")
public class AdminSmileyThemePictureController {
	@Autowired
	private SmileyThemePictureService smileyThemePictureService;
	@Autowired
	private SmileyThemeService smileyThemeService;
	@Autowired
	private SmileyStorage smileyStorage;
	private final static Logger logger = LoggerFactory.getLogger(AdminSmileyThemePictureController.class);
	
	@GetMapping(path="/")
	public String listPage(@RequestParam("theme")int themeId, HttpServletRequest request, Model model) {
		//表情风格
		SmileyTheme smileyTheme = smileyThemeService.get(themeId).orElseThrow(()->new ResourceNotFoundException("表情风格不存在或暂时无法访问"));
		model.addAttribute("theme", smileyTheme);
		//表情风格下的表情图片
		List<String> themePices = smileyStorage.getThemePictures(smileyTheme.getDirectNames());
		if(themePices==null || themePices.isEmpty()){
			throw new ResourceNotFoundException("表情图片获取失败");
		}
		Set<SmileyPictureNotes> spns = themePices.stream().map(SmileyPictureNotes::new).collect(Collectors.toSet());
		//已经注解的表情图片
		//key = 文件名, value = 文件的语义
		Map<String, String> data = smileyThemePictureService.getAllByTheme(themeId).collect(Collectors.toMap(SmileyThemePicture::getFileNames, SmileyThemePicture::getDescription));
		//合并结果
		Set<SmileyPictureNotes> result = new HashSet<>();
		for(SmileyPictureNotes spn : spns){
			String tmpNotes = null; 
			try{
				tmpNotes = data.get(spn.getFileName());
			}catch(NullPointerException e){}
			if(tmpNotes!=null){
				result.add(new SmileyPictureNotes(spn.getUrl(), spn.getFileName(), tmpNotes));
			}else{
				result.add(spn);
			}
		}
		model.addAttribute("rs", result);
		return "admin/smiley_pic/index";
	}
	//更新表情图片的注释
	@PostMapping(path="/edit/dynamic", produces="application/json;charset=UTF-8")
	@ResponseBody
	public TipMessage modifyThemePic(
			@RequestParam("name")String fileName, 
			@RequestParam("value")String fileSemantic, 
			@RequestParam("theme")int themeId, 
			HttpServletRequest request, 
			Model model){
		try{
			Optional<Boolean> rs = smileyThemePictureService.modify(fileName, fileSemantic, themeId);
			if(rs.orElse(false)){
				return (rs.get())?TipMessage.ofSuccess("语义定义成功"):TipMessage.ofSuccess("语义定义失败");
			}
		}catch(IllegalStateException e){
			return TipMessage.ofError(e.getMessage());
		}
		return TipMessage.ofError("操作异常中断");
	}
	//发布话题时查看指定表情风格下的图片
	@GetMapping(path="/list.json", produces="application/json;charset=UTF-8")
	@ResponseBody
	public List<String> querySmileyDirectPices(@RequestParam("direct")String themeDirectName, HttpServletRequest request, Model model){
		return smileyStorage.getThemePictures(themeDirectName);
	}
}
