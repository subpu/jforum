package com.apobates.forum.trident.tag;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import com.apobates.forum.utils.Commons;
/**
 * 格式化数字标签
 * @author xiaofanku
 * @since 20191023
 */
public class LongNumbericFormatTag extends SimpleTagSupport{
	private long value;
	
	public void setValue(long value) {
		this.value = value;
	}

	@Override
	public void doTag() throws JspException, IOException {
		getJspContext().getOut().print(Commons.longNumbericFormat(value));
	}
}
