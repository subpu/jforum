package com.apobates.forum.trident.vo;

import java.io.Serializable;
/**
 * MemberController中用到的会员抽像
 * 
 * @author xiaofanku
 *
 */
public final class MemberModel implements Serializable{
	private static final long serialVersionUID = -8468326225050124476L;
	private final long id;
	private final String title;
	private final long value;
	
	public long getId() {
		return id;
	}
	
	public String getTitle() {
		return title;
	}

	public long getValue() {
		return value;
	}

	public MemberModel(long id, String title, long value) {
		super();
		this.id = id;
		this.title = title;
		this.value = value;
	}
}
