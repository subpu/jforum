package com.apobates.forum.trident.controller.form;

public class BoardModeratorPermissionForm extends ActionForm{
	private String moderatorId;
	private String[] actions;
	
	public String getModeratorId() {
		return moderatorId;
	}
	public void setModeratorId(String moderatorId) {
		this.moderatorId = moderatorId;
	}
	public String[] getActions() {
		return actions;
	}
	public void setActions(String[] actions) {
		this.actions = actions;
	}
}
