package com.apobates.forum.trident.vo;

import java.io.Serializable;

import com.apobates.forum.core.entity.AlbumPicture;
/**
 * ekko-lightbox的结果集
 * 
 * @author xiaofanku
 * @since 20191218
 */
public class ThreadsAlbumPicture implements Serializable, Comparable<ThreadsAlbumPicture>{
	private static final long serialVersionUID = -298601538672267112L;
	private final long album;
	private final String caption;
	private final String link;
	private final String thumbnail;
	private final long id;
	private final int ranking;
	
	public long getAlbum() {
		return album;
	}
	public String getCaption() {
		return caption;
	}
	public String getLink() {
		return link;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public long getId() {
		return id;
	}
	public int getRanking() {
		return ranking;
	}
	
	public ThreadsAlbumPicture(long album, long id, String link, String caption, int ranking, String thumbnail) {
		super();
		this.album = album;
		this.id = id;
		this.caption = caption;
		this.link = link;
		this.ranking = ranking;
		this.thumbnail = thumbnail;
	}
	
	public ThreadsAlbumPicture(AlbumPicture albumPicture){
		this.album = albumPicture.getAlbumId();
		this.id = albumPicture.getId();
		this.caption = albumPicture.getCaption();
		this.link = albumPicture.getLink();
		this.ranking = albumPicture.getRanking();
		this.thumbnail = albumPicture.getThumb();
	}
	@Override
	public int compareTo(ThreadsAlbumPicture o) {
		return getRanking() - o.getRanking();
	}
}
