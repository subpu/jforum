package com.apobates.forum.trident.controller.form;

public class MemberPassportForm extends ActionForm{
	private String oldpswd;
	private String newpswd;
	private String confimpswd;
	
	public String getOldpswd() {
		return oldpswd;
	}
	public void setOldpswd(String oldpswd) {
		this.oldpswd = oldpswd;
	}
	public String getNewpswd() {
		return newpswd;
	}
	public void setNewpswd(String newpswd) {
		this.newpswd = newpswd;
	}
	public String getConfimpswd() {
		return confimpswd;
	}
	public void setConfimpswd(String confimpswd) {
		this.confimpswd = confimpswd;
	}
}
