package com.apobates.forum.trident.tag;

import java.io.IOException;
import java.util.regex.PatternSyntaxException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import com.apobates.forum.utils.Commons;

/**
 * 将参数格式成<datalist>标签形式
 * 
 * @author xiaofanku
 * @since 20190915
 */
public class DateListTag extends SimpleTagSupport {
	private String value;

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public void doTag() throws JspException, IOException {
		String[] dataArray = {"-"};
		try{
			dataArray = value.split(",");
		}catch(PatternSyntaxException e){}
		//
		if (dataArray.length == 1) {
			getJspContext().getOut().print(value);
			return;
		}
		//
		StringBuffer sb = new StringBuffer();
		String tmp = String.format("<datalist id=\"%s\" data-size=\"%d\">", Commons.randomNumeric(6), dataArray.length);
		sb.append(tmp);
		for (String str : dataArray) {
			sb.append(String.format("<option value=\"%s\">", str));
		}
		sb.append("</datalist>");
		getJspContext().getOut().print(sb.toString());
	}
}
