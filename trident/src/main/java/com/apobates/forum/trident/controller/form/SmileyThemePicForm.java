package com.apobates.forum.trident.controller.form;

public class SmileyThemePicForm extends ActionForm{
	//图片文件名,例:03.gif
	private String fileNames;
	//说明,例:哈哈
	private String description;
	//显示顺序
	//int
	private String ranking;
	//表情风格的目录名
	private String themeDirect;
	
	public String getFileNames() {
		return fileNames;
	}
	public void setFileNames(String fileNames) {
		this.fileNames = fileNames;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getIntegerRanking() {
		return covertStringToInteger(getRanking(), 1);
	}
	public String getRanking() {
		return ranking;
	}
	public void setRanking(String ranking) {
		this.ranking = ranking;
	}
	public String getThemeDirect() {
		return themeDirect;
	}
	public void setThemeDirect(String themeDirect) {
		this.themeDirect = themeDirect;
	}
}
