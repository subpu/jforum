package com.apobates.forum.trident.controller.form;

public class TopicMoveForm extends ActionForm{
	//要移动的话题ID
	private String topicId;
	private String topicTitle;
	//要移动的话题现在所在的版块ID
	private String boardId;
	private String boardTitle;
	//要移动的话题现在所在的版块组ID
	private String volumesId;
	//话题移到的版块ID
	private String targetBoardId;
	
	public String getTopicId() {
		return topicId;
	}
	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}
	public String getTopicTitle() {
		return topicTitle;
	}
	public void setTopicTitle(String topicTitle) {
		this.topicTitle = topicTitle;
	}
	public String getBoardId() {
		return boardId;
	}
	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}
	public String getBoardTitle() {
		return boardTitle;
	}
	public void setBoardTitle(String boardTitle) {
		this.boardTitle = boardTitle;
	}
	public String getVolumesId() {
		return volumesId;
	}
	public void setVolumesId(String volumesId) {
		this.volumesId = volumesId;
	}
	public String getTargetBoardId() {
		return targetBoardId;
	}
	public void setTargetBoardId(String targetBoardId) {
		this.targetBoardId = targetBoardId;
	}
	public long getLongTopicId(){
		return covertStringToLong(getTopicId(), 0L);
	}
	public long getLongBoardId() {
		return covertStringToLong(getBoardId(), 0L);
	}
	public int getIntegerVolumesId() {
		return covertStringToInteger(getVolumesId(), -1);
	}
	public long getLongTargetBoardId() {
		return covertStringToLong(getTargetBoardId(), 0L);
	}
}
