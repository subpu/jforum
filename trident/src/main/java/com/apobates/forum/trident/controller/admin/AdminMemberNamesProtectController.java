package com.apobates.forum.trident.controller.admin;

import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.apobates.forum.trident.controller.form.MemberNamesProtectForm;
import com.apobates.forum.member.api.service.MemberNamesProtectService;
import com.apobates.forum.member.entity.MemberNamesProtect;
import com.apobates.forum.member.storage.core.MemberSessionBean;
import com.apobates.forum.utils.Commons;
import com.apobates.forum.utils.TipMessage;
/**
 * 会员帐号保护控制器
 * 
 * @author xiaofanku
 * @since 201900809
 */
@Controller
@RequestMapping(value = "/admin/member/protect")
public class AdminMemberNamesProtectController {
	@Autowired
	private MemberNamesProtectService memberNamesProtectService;
	
	@GetMapping(path="/")
	public String listPage(
			HttpServletRequest request, 
			Model model){
		model.addAttribute("rs", memberNamesProtectService.getAll().collect(Collectors.toList()));
		return "admin/member_protect/index";
	}
	
	@GetMapping(path="/edit")
	public String editForm(
			@RequestParam(name="id", required=false, defaultValue="0")int id,
			HttpServletRequest request,
			Model model){
		MemberNamesProtectForm form = new MemberNamesProtectForm();
		MemberNamesProtect mnp = memberNamesProtectService.get(id).orElse(new MemberNamesProtect());
		form.setRecord(mnp.getId());
		form.setStatus((mnp.getId()==0)?true:mnp.isStatus());
		form.setNames(mnp.getMemberNames());
		//
		form.setToken(Commons.randomAlphaNumeric(8));
		model.addAttribute("form", form);
		// 内置的提示
		model.addAttribute("nativeNames", String.join(",", MemberNamesProtect.getNativeProtect()));
		return "admin/member_protect/edit";
	}
	
	@PostMapping(path="/edit")
	public String editAction(
			@ModelAttribute("form") MemberNamesProtectForm form, 
			HttpServletRequest request,
			Model model){
		MemberNamesProtect mnp = new MemberNamesProtect();
		mnp.setMemberNames(form.getNames());
		mnp.setStatus(form.getBooleanStatus());
		
		boolean symbol = false;
		String errMsg=null;
		try{
			if(form.isUpdate()) {
				symbol = memberNamesProtectService.edit(form.getIntegerRecord(), mnp).orElse(false);
			}else {
				symbol = memberNamesProtectService.create(mnp.getMemberNames(), mnp.isStatus()).isPresent();
			}
		}catch(IllegalStateException e){
			errMsg=e.getMessage();
		}
		if(symbol) {
			return "redirect:/admin/member/protect/";
		}
		model.addAttribute("errors", Commons.optional(errMsg, form.getActionTitle()+"会员帐户保护操作失败"));
		//
		form.setToken(Commons.randomAlphaNumeric(8));
		model.addAttribute("form", form);
		// 内置的提示
		model.addAttribute("nativeNames", String.join(",", MemberNamesProtect.getNativeProtect()));
		return "admin/member_protect/edit";
	}
	
	//删除
	@PostMapping(path="/remove", produces = "application/json;charset=UTF-8")
	@ResponseBody
	public TipMessage removeAction(
			@RequestParam("id")int id,
			MemberSessionBean mbean,
			HttpServletRequest request,
			Model model){
		return TipMessage.Builder.take(()->memberNamesProtectService.editStatus(id, false)).success("会员帐户保护删除成功").error("操作失败");
	}
}
