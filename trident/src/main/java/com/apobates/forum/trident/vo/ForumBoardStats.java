package com.apobates.forum.trident.vo;

import java.io.Serializable;

import com.apobates.forum.core.entity.Board;
/**
 * 版块汇总统计(右侧的版块统计)
 * 
 * @author xiaofanku
 * @since 20190614
 */
public final class ForumBoardStats implements Serializable{
	private static final long serialVersionUID = 1265034484256240L;
	//版块名称
	private final String title;
	//版块的连接
	private final String link;
	//回复数
	private final long postses;
	//主题数
	private final long topices;
	//版块的ID
	private final long id;
	//缓存需要的
	private final String connect;
	
	public ForumBoardStats(Board board, long postses, long topices) {
		super();
		this.title = board.getTitle();
		this.link = String.format("/board/%s.xhtml", board.getConnect());
		this.postses = postses;
		this.topices = topices;
		this.id = board.getId();
		this.connect = board.getConnect();
	}
	
	public ForumBoardStats(Board board) {
		this.title = board.getTitle();
		this.link = String.format("/board/%s.xhtml", board.getConnect());
		this.postses = 0L;
		this.topices = 0L;
		this.id = board.getId();
		this.connect = board.getConnect();
	}
	
	public String getTitle() {
		return title;
	}

	public long getPostses() {
		return postses;
	}

	public long getTopices() {
		return topices;
	}

	public String getLink() {
		return link;
	}

	public long getId() {
		return id;
	}

	public String getConnect() {
		return connect;
	}
}
