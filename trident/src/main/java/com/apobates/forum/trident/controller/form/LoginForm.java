package com.apobates.forum.trident.controller.form;

import com.apobates.forum.utils.Commons;

public class LoginForm extends ActionForm{
	private String names;
	private String pswd;
	//内部地址
	private String redirect;
	private int tries = 0;
	//随机的词调(?验证码)
	private String lexical;
	//客户端缓存的cookie标记.以减少查询会员状态的请求@20200502
	private String trace;
	
	public String getNames() {
		return Commons.isNotBlank(names)?names.trim():"";
	}

	public void setNames(String names) {
		this.names = names;
	}

	public String getPswd() {
		return Commons.isNotBlank(pswd)?pswd.trim():"";
	}

	public void setPswd(String pswd) {
		this.pswd = pswd;
	}

	public String getRedirect() {
		return redirect;
	}

	public void setRedirect(String redirect) {
		this.redirect = redirect;
	}

	public int getTries() {
		return tries;
	}

	public void setTries(int tries) {
		this.tries = tries;
	}

	public String getLexical() {
		return lexical;
	}

	public void setLexical(String lexical) {
		this.lexical = lexical;
	}

	public String getTrace() {
		return trace;
	}

	public void setTrace(String trace) {
		this.trace = trace;
	}
}
