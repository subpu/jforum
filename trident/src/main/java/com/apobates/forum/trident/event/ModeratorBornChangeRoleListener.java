package com.apobates.forum.trident.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import com.apobates.forum.core.entity.BoardModerator;
import com.apobates.forum.core.impl.event.ModeratorBornEvent;
import com.apobates.forum.member.api.dao.MemberDao;
import com.apobates.forum.member.entity.Member;
import com.apobates.forum.member.entity.MemberRoleEnum;
import org.springframework.stereotype.Component;

/**
 * 版主新生事件的角色变更监听器
 * 
 * @author xiaofanku
 * @since 20200831
 */
@Component
public class ModeratorBornChangeRoleListener implements ApplicationListener<ModeratorBornEvent>{
    @Autowired
    private MemberDao memberDao;
    private final static Logger logger = LoggerFactory.getLogger(ModeratorBornChangeRoleListener.class);
    //从BoardModeratorDaoImpl.pushModerator中取消MemberDao
    @Override
    public void onApplicationEvent(ModeratorBornEvent e) {
        logger.info("[Moderator][BornEvent][1]版主新生角色变更开始");
        BoardModerator moderator = e.getModerator();
        Member member = e.getMember();
        MemberRoleEnum currentRole = moderator.isVolumesMaster()?MemberRoleEnum.MASTER:MemberRoleEnum.BM;
        if (currentRole.getSymbol() > member.getMrole().getSymbol()) {
            //更新会员的角色|这是升职了
            memberDao.editMemberRole(member.getId(), currentRole);
        }
        logger.info("[Moderator][BornEvent][1]版主新生角色变更结束");
    }
}
