package com.apobates.forum.trident.tag;

import java.io.IOException;
import java.util.ResourceBundle;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import com.apobates.forum.attention.core.ImagePathCoverter;
import com.apobates.forum.utils.Commons;
/**
 * 塑造/解码像册的图片路径
 * @author xiaofanku
 * @since 20190831
 */
public class MoldImagePathTag extends SimpleTagSupport{
	private String path;
	//
	private final String imageBucketDomain;
	private final String uploadImageDirectName;
	
	public MoldImagePathTag(){
		ResourceBundle gisResource = ResourceBundle.getBundle("global");
		this.imageBucketDomain = gisResource.getString("img.bucket.domain");
		this.uploadImageDirectName=gisResource.getString("img.bucket.upload.direct");
	}
	
	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public void doTag() throws JspException, IOException {
		if(!Commons.isNotBlank(path)){
			getJspContext().getOut().print("/static/img/140x140.png");
			return;
		}
		ImagePathCoverter ipc = new ImagePathCoverter(path);
		String imagePath = ipc.decodeUploadImageFilePath(imageBucketDomain, uploadImageDirectName).orElse("/static/img/140x140.png");
		//
		getJspContext().getOut().print(imagePath);
	}
}
