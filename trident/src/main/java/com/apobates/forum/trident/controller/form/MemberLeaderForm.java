package com.apobates.forum.trident.controller.form;

import com.apobates.forum.member.entity.MemberRoleEnum;
import com.apobates.forum.utils.lang.EnumArchitecture;

/**
 * 社区经理创建表单
 * @author xiaofanku
 * @since 20190719
 */
public class MemberLeaderForm extends ActionForm {
	private String names;
	private String nickname;
	private String pswd;
	// enum
	private String role;

	public String getNames() {
		return names;
	}

	public void setNames(String names) {
		this.names = names;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPswd() {
		return pswd;
	}

	public void setPswd(String newPswd) {
		this.pswd = newPswd;
	}
	public MemberRoleEnum getEnumRole(){
		int roleEnumSymbol=covertStringToInteger(getRole(), 0);
		return EnumArchitecture.getInstance(roleEnumSymbol, MemberRoleEnum.class).orElse(MemberRoleEnum.NO);
	}
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
