package com.apobates.forum.trident.controller.form;

public class RegisterInvitecodeForm extends ActionForm{
	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
