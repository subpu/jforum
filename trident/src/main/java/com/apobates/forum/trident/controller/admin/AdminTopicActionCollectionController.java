package com.apobates.forum.trident.controller.admin;

import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.apobates.forum.core.api.service.TopicActionCollectionService;
import com.apobates.forum.core.entity.TopicActionCollection;
import com.apobates.forum.utils.FrontPageURL;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.PageRequest;
import com.apobates.forum.utils.persistence.Pageable;
/**
 * 话题操作控制器
 * 
 * @author xiaofanku
 * @since 20190407
 */
@Controller
@RequestMapping(value = "/admin/topic/action")
public class AdminTopicActionCollectionController {
	@Autowired
	private TopicActionCollectionService topicActionCollectionService;
	@Value("${site.pageSize}")
	private int pageSize;
	
	@GetMapping(path="/")
	public String listPage(
			@RequestParam(value = "p", required = false, defaultValue = "1") int page, 
			@RequestParam(value = "id", required = false, defaultValue = "0") long topicId, 
			HttpServletRequest request, 
			Model model) {
		FrontPageURL fpbuild = new FrontPageURL(request.getContextPath() + "/admin/topic/action/").addPageSize("number", pageSize);
		if(topicId>0){
			fpbuild = fpbuild.addParameter("id", topicId);
		}
		Pageable pr = new PageRequest(page, fpbuild.getPageSize());
		Page<TopicActionCollection> rs = (topicId>0)?topicActionCollectionService.getByTopic(topicId, pr):topicActionCollectionService.getAll(pr);
		model.addAttribute("rs", rs.getResult().collect(Collectors.toList()));
		model.addAttribute("pageData", pr.toData(fpbuild, rs.getTotalElements()));
		model.addAttribute("paramTopic", topicId);
		return "admin/topic/action";
	}
}
