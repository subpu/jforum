package com.apobates.forum.trident.fileupload;

/**
 * 图片存储的信息
 * 
 * @author xiaofanku
 * @since 20191024
 */
public interface ImageStorage {
	/**
	 * 图片存储的域名,例:http://x.com
	 * 
	 * @return
	 */
	String imageBucketDomain();
	
	/**
	 * 图片存储的目录名
	 * 
	 * @return
	 */
	String uploadImageDirectName();
}
