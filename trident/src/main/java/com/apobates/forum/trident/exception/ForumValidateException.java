package com.apobates.forum.trident.exception;

public class ForumValidateException extends IllegalStateException{
	private static final long serialVersionUID = 2809200508550325106L;

	public ForumValidateException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ForumValidateException(String s) {
		super(s);
		// TODO Auto-generated constructor stub
	}
}
