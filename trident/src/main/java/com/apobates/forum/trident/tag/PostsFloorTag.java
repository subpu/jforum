package com.apobates.forum.trident.tag;

import java.io.IOException;
import java.util.Optional;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import com.apobates.forum.core.entity.PostsFloorAliasEnum;
import com.apobates.forum.utils.lang.EnumArchitecture;

public class PostsFloorTag extends SimpleTagSupport {
	// 楼层数值
	private int value;

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public void doTag() throws JspException, IOException {
		String sb = value + "";
		Optional<PostsFloorAliasEnum> ea = EnumArchitecture.getInstance(value, PostsFloorAliasEnum.class);
		if (ea.isPresent()) {
			PostsFloorAliasEnum pfa = ea.get();
			sb = pfa.getTitle();
		}
		getJspContext().getOut().print(sb);
	}
}
