package com.apobates.forum.trident.controller.form;

import com.apobates.forum.trident.fileupload.ImageStorageExecutor;

public class AlbumPictureForum extends ActionForm{
	private String caption;
	private String topicId;
	private String albumId;
	//上传后的图片地址
	private String fileurl;
	private String ranking;
	
	public String getCaption() {
		return caption;
	}
	public void setCaption(String caption) {
		this.caption = caption;
	}
	public String getTopicId() {
		return topicId;
	}
	public long getLongTopicId(){
		return covertStringToLong(getTopicId(), 0L);
	}
	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}
	public String getAlbumId() {
		return albumId;
	}
	public long getLongAlbumId(){
		return covertStringToLong(getAlbumId(), 0L);
	}
	public void setAlbumId(String albumId) {
		this.albumId = albumId;
	}
	public String getFileurl() {
		return fileurl;
	}
	public void setFileurl(String fileurl) {
		this.fileurl = fileurl;
	}
	public String getRanking() {
		return ranking;
	}
	public void setRanking(String ranking) {
		this.ranking = ranking;
	}
	public int getIntegerRanking() {
		return covertStringToInteger(getRanking(), 1);
	}
	public String getEncodePictureAddr(ImageStorageExecutor executor){
		return super.uploadAndEncodeFile(getFileurl(), executor);
	}
}
