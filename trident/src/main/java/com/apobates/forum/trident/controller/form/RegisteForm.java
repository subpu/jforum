package com.apobates.forum.trident.controller.form;

import com.apobates.forum.utils.Commons;

public class RegisteForm extends ActionForm {
	private String names;
	private String nickname;
	private String newPswd;
	private String pswdConfirm;

	public String getNames() {
		return Commons.isNotBlank(names)?names.trim():"";
	}

	public void setNames(String names) {
		this.names = names;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	public String getNewPswd() {
		return Commons.isNotBlank(newPswd)?newPswd.trim():"";
	}

	public void setNewPswd(String newPswd) {
		this.newPswd = newPswd;
	}

	public String getPswdConfirm() {
		return Commons.isNotBlank(pswdConfirm)?pswdConfirm.trim():"";
	}

	public void setPswdConfirm(String pswdConfirm) {
		this.pswdConfirm = pswdConfirm;
	}
}
