package com.apobates.forum.trident.controller.form;

import com.apobates.forum.trident.fileupload.ImageStorageExecutor;

public class BoardGroupForm extends ActionForm{
	private String title;
	private String description;
	private String imageAddr;
	//上传后的图片地址
	private String fileurl;
	private String ranking="1";
	//private boolean status;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImageAddr() {
		return imageAddr;
	}
	public void setImageAddr(String imageAddr) {
		this.imageAddr = imageAddr;
	}
	public int getIntegerRanking() {
		return covertStringToInteger(getRanking(), 1);
	}
	public String getRanking() {
		return ranking;
	}
	public void setRanking(String ranking) {
		this.ranking = ranking;
	}
	public String getFileurl() {
		return fileurl;
	}
	public void setFileurl(String fileurl) {
		this.fileurl = fileurl;
	}
	public String getEncodeIcoAddr(ImageStorageExecutor executor){
		return super.uploadAndEncodeFile(getFileurl(), executor);
	}
}
