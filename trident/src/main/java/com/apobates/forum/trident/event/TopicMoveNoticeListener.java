package com.apobates.forum.trident.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import com.apobates.forum.core.entity.Topic;
import com.apobates.forum.core.impl.event.TopicMoveEvent;
import com.apobates.forum.letterbox.api.service.ForumLetterService;
import com.apobates.forum.letterbox.entity.ForumLetter;
import com.apobates.forum.utils.DateTimeUtils;
import org.springframework.stereotype.Component;

/**
 * 话题移动通知侦听器
 * 
 * @author xiaofanku
 * @since 20191117
 */
@Component
public class TopicMoveNoticeListener implements ApplicationListener<TopicMoveEvent>{
	@Autowired
	private ForumLetterService forumLetterService;
	private final static Logger logger = LoggerFactory.getLogger(PostsMoodNoticeListener.class);
	
	@Override
	public void onApplicationEvent(TopicMoveEvent event) {
		logger.info("[Topic][MoveEvent][1]话题移动通知处理开始");
		Topic topic = event.getTopic();
		String content = String.format("%s在%s将话题: #%s# ,从%s版块移至%s版块, 如果存在异议您可以联系在线管理员", 
				event.getManagerNickname(), 
				DateTimeUtils.getRFC3339(event.getDateTime()), 
				topic.getTitle(), 
				event.getProtoBoard().getTitle(), 
				event.getTargetBoard().getTitle());
		forumLetterService.create(new ForumLetter("您发布的话题已经被管理员移至新版块", content, topic.getMemberId(), topic.getMemberNickname()));
		logger.info("[Topic][MoveEvent][1]话题移动通知处理结束");
	}
}
