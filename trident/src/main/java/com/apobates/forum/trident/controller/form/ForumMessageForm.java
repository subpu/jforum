package com.apobates.forum.trident.controller.form;

import com.apobates.forum.utils.Commons;
/**
 * 前台消息创建的表单
 * @author xiaofanku
 * 
 */
public class ForumMessageForm extends ActionForm{
	private String title;
	private String content;
	//收件人/会员ID
	//private String receiver;
	//以u开头后跟Member.id
	private String uid;
	//收件人名字
	//private String names;
	//仅供显示用的(可以是会员的登录帐号或称)
	private String snames;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return  Commons.isNotBlank(content)?content.trim():"";
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getSnames() {
		return snames;
	}
	public void setSnames(String snames) {
		this.snames = snames;
	}
	public long getMemberId(){
		return covertStringToLong(getUid().substring(1), 0L);
	}
}
