package com.apobates.forum.trident.controller.form;

public class BoardConfigForm extends ActionForm{
	private String volumesId;
	private String boardId;
	//true表示可读可写,false表示只读
	//boolean
	private String readWrite="1";
	// 写
	// 作者可编辑的时间有效期(分钟)
	//int
	private String editMinute="3";
	// 不间断的操作次数
	//int
	private String writeMinInterrupt="3";
	// 积分需求
	//int
	private String writeMinScore="0";
	// 会员组需求
	//MemberGroupEnum
	private String writeLowMemberGroup="1";
	//会员角色需求
	//MemberRoleEnum
	private String writeLowMemberRole="1";
	// 会员等级需求
	//int
	private String writeLowMemberLevel="0";
	// 读
	// 积分需求
	//int
	private String readMinScore="0";
	// 会员组需求
	//MemberGroupEnum
	private String readLowMemberGroup="0";
	//会员角色需求
	//MemberRoleEnum
	private String readLowMemberRole="1";
	// 会员等级需求
	//int
	private String readLowMemberLevel="0";
	//不分读写操作设置
	// ip地址过滤 boolean
	private String ipFilter="1";
	
	public String getVolumesId() {
		return volumesId;
	}
	public void setVolumesId(String volumesId) {
		this.volumesId = volumesId;
	}

	public String getBoardId() {
		return boardId;
	}
	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}

	public String getReadWrite() {
		return readWrite;
	}
	public void setReadWrite(boolean readWrite) {
		setReadWrite(covertBooleanToString(readWrite));
	}
	public void setReadWrite(String readWrite) {
		this.readWrite = readWrite;
	}

	public String getEditMinute() {
		return editMinute;
	}
	public void setEditMinute(String editMinute) {
		this.editMinute = editMinute;
	}

	public String getWriteMinInterrupt() {
		return writeMinInterrupt;
	}
	public void setWriteMinInterrupt(String writeMinInterrupt) {
		this.writeMinInterrupt = writeMinInterrupt;
	}

	public String getWriteMinScore() {
		return writeMinScore;
	}
	public void setWriteMinScore(String writeMinScore) {
		this.writeMinScore = writeMinScore;
	}

	public String getWriteLowMemberGroup() {
		return writeLowMemberGroup;
	}
	public void setWriteLowMemberGroup(String writeLowMemberGroup) {
		this.writeLowMemberGroup = writeLowMemberGroup;
	}

	public String getWriteLowMemberLevel() {
		return writeLowMemberLevel;
	}
	public void setWriteLowMemberLevel(String writeLowMemberLevel) {
		this.writeLowMemberLevel = writeLowMemberLevel;
	}

	public String getReadMinScore() {
		return readMinScore;
	}
	public void setReadMinScore(String readMinScore) {
		this.readMinScore = readMinScore;
	}

	public String getReadLowMemberGroup() {
		return readLowMemberGroup;
	}
	public void setReadLowMemberGroup(String readLowMemberGroup) {
		this.readLowMemberGroup = readLowMemberGroup;
	}

	public String getReadLowMemberLevel() {
		return readLowMemberLevel;
	}

	public void setReadLowMemberLevel(String readLowMemberLevel) {
		this.readLowMemberLevel = readLowMemberLevel;
	}
	public String getIpFilter() {
		return ipFilter;
	}
	public void setIpFilter(boolean ipFilter) {
		setIpFilter(covertBooleanToString(ipFilter));
	}
	public void setIpFilter(String ipFilter) {
		this.ipFilter = ipFilter;
	}
	public String getWriteLowMemberRole() {
		return writeLowMemberRole;
	}
	public void setWriteLowMemberRole(String writeLowMemberRole) {
		this.writeLowMemberRole = writeLowMemberRole;
	}

	public String getReadLowMemberRole() {
		return readLowMemberRole;
	}
	public void setReadLowMemberRole(String readLowMemberRole) {
		this.readLowMemberRole = readLowMemberRole;
	}
	//
	public long getLongBoardId(){
		return covertStringToLong(getBoardId(), 0L);
	}
	public int getIntegerVolumesId(){
		return covertStringToInteger(getVolumesId(), 0);
	}
	//只读|读写模式
	public Boolean getBooleanReadWrite() {
		return covertStringToBoolean(getReadWrite());
	}
	//读
	public int getIntegerReadMinScore(){
		return covertStringToInteger(getReadMinScore(), 0);
	}
	public int getIntegerReadLowMemberLevel() {
		return covertStringToInteger(getReadLowMemberLevel(), 0);
	}
	public int getIntegerReadLowMemberGroup() {
		return covertStringToInteger(getReadLowMemberGroup(), 0); //游客
	}
	public int getIntegerReadLowMemberRole() {
		return covertStringToInteger(getReadLowMemberRole(), 1); //无
	}
	//写
	public int getIntegerWriteLowMemberGroup() {
		return covertStringToInteger(getWriteLowMemberGroup(), 1); //会员
	}
	public int getIntegerWriteLowMemberRole() {
		return covertStringToInteger(getWriteLowMemberRole(), 1);
	}
	public int getIntegerWriteMinScore() {
		return covertStringToInteger(getWriteMinScore(), 0);
	}
	public int getIntegerWriteLowMemberLevel() {
		return covertStringToInteger(getWriteLowMemberLevel(), 0);
	}
	//扩展
	public Boolean getBooleanIpFileter() {
		return covertStringToBoolean(getIpFilter());
	}
	public int getIntegerWriteMinInterrupt(){
		return covertStringToInteger(getWriteMinInterrupt(), 3);
	}
	public int getIntegerEditMinute() {
		return covertStringToInteger(getEditMinute(), 3);
	}
}
