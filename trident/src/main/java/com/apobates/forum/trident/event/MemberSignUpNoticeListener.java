package com.apobates.forum.trident.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import com.apobates.forum.letterbox.api.service.ForumLetterService;
import com.apobates.forum.letterbox.entity.ForumLetter;
import com.apobates.forum.member.entity.Member;
import com.apobates.forum.member.impl.event.MemberSignUpEvent;
import org.springframework.stereotype.Component;

/**
 * 会员注册事件侦听器,会员注册后的发送问候通知
 * @author xiaofanku
 * @since 20190703
 */
@Component
public class MemberSignUpNoticeListener implements ApplicationListener<MemberSignUpEvent>{
	@Autowired
	private ForumLetterService forumLetterService;
	@Value("${site.appname}")
	private String appName;
	private final static Logger logger = LoggerFactory.getLogger(MemberSignUpNoticeListener.class);
	
	@Override
	public void onApplicationEvent(MemberSignUpEvent event) {
		logger.info("[Member][SignUpEvent][3]开始发送注册欢迎词");
		Member m = event.getMember();
		forumLetterService.create(getRegisterNotice(appName, m.getId(), m.getNames()));
		logger.info("[Member][SignUpEvent][3]注册欢迎词发送结束");
	}
	
	//注册
	private ForumLetter getRegisterNotice(String siteNames, long memberId, String names){
		String title = "欢迎您加入"+siteNames+"大家庭!";
		String content = "恭喜, 您已经完成了会员帐户注册, 请牢记您的登录信息. 在这里可以跟小伙伴们一起分享您的知识、您的快乐. 如果您发现任何问题, 请您及时向我们提出反馈或建议.";
		return new ForumLetter(title, content, memberId, names);
	}
}
