package com.apobates.forum.trident.exception;

/**
 * 权限丢失异常
 * @author xiaofanku
 * @since 20190730
 */
public class PermissionLostException extends InterruptedException{
	private static final long serialVersionUID = -3380292936369422129L;

	public PermissionLostException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PermissionLostException(String s) {
		super(s);
		// TODO Auto-generated constructor stub
	}
}
