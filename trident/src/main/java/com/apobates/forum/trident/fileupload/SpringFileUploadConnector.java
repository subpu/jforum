package com.apobates.forum.trident.fileupload;

import java.io.File;
import java.io.IOException;
import javax.servlet.ServletException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import com.apobates.forum.utils.fileupload.ApacheFileUploadConnector;
import com.apobates.forum.utils.fileupload.CommonInitParamers;
import com.apobates.forum.utils.fileupload.UploadHandler;
/**
 * 使用Apache Fileupload
 * 
 * @author xiaofanku@live.cn
 * @since 20171216
 */
public class SpringFileUploadConnector extends ApacheFileUploadConnector{
	
    public SpringFileUploadConnector(UploadHandler handler) {
        super(handler);
    }
    
    public String upload(CommonInitParamers params, MultipartFile file) throws IOException, ServletException{
        if(!(file instanceof CommonsMultipartFile)){
            throw new IOException("MultipartFile不是CommonsMultipartFile实例");
        }
        CommonsMultipartFile cmf=(CommonsMultipartFile)file;
        //保存的路径
        File uploadDir=new File(params.getFileSaveDir());
        if (!uploadDir.exists()) {
            uploadDir.mkdirs();
        }
        
        String callbackFun=params.getCallbackFunctionName();
        return execute(cmf.getFileItem(), uploadDir, callbackFun);
    }
}
