package com.apobates.forum.trident.event;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import com.apobates.forum.core.api.dao.PostsDao;
import com.apobates.forum.core.api.dao.TopicDao;
import com.apobates.forum.core.entity.Posts;
import com.apobates.forum.core.entity.Topic;
import com.apobates.forum.core.impl.event.PostsPublishEvent;
import com.apobates.forum.letterbox.api.service.ForumLetterService;
import com.apobates.forum.letterbox.entity.ForumLetter;
import org.springframework.stereotype.Component;

/**
 * 回复的引用通知
 * 
 * @author xiaofanku
 * @since 20200301
 */
@Component
public class PostsQuoteNoticeListener implements ApplicationListener<PostsPublishEvent>{
	@Autowired
	private TopicDao topicDao;
	@Autowired
	private PostsDao postsDao;
	@Autowired
	private ForumLetterService forumLetterService;
	private final static Logger logger = LoggerFactory.getLogger(PostsQuoteNoticeListener.class);
	
	@Override
	public void onApplicationEvent(PostsPublishEvent event) {
		logger.info("[Posts][PublishEvent][3]回复动作引用处理开始");
		Posts posts = event.getPosts();
		//回复中是否能提取引用的cite
		long quotePostsId = parseBlockquoteTag(posts.getContent());
		if(quotePostsId<=0){
			logger.info("[Posts][PublishEvent][3]回复中无引用记录");
			return;
		}
		logger.info("[Posts][PublishEvent][3]引用的回复ID: " + quotePostsId);
		//被引用的回复
		Posts quotePosts = postsDao.findOneForIndex(quotePostsId);
		Topic topic = topicDao.findOneForIndex(posts.getTopicId());
		if(null != quotePosts && null != topic){
			try{
				forumLetterService.create(getQuoteNotice(topic, posts, quotePosts.getMemberId(), quotePosts.getMemberNickname(), quotePosts.getFloorNumber()));
			}catch(Exception e){}
		}
		logger.info("[Posts][PublishEvent][3]回复动作引用处理结束");
	}
	//生成引用通知
	private ForumLetter getQuoteNotice(Topic topic, Posts posts, long authorId, String authorNickname, long authorReplyFloor){
		String floorLink=String.format("/topic/%s.xhtml#posts-%d", topic.getConnect(), posts.getId());
		return new ForumLetter(
				posts.getMemberNickname()+"引用了您的回复", 
				String.format(
						"<p><a href=\"/member/%d.xhtml\">%s</a>引用了您在<a href=\"%s\">#%s#</a>%d楼发布的回复内容</p>", 
						posts.getMemberId(),
						posts.getMemberNickname(),
						floorLink, 
						topic.getTitle(), 
						authorReplyFloor), 
				authorId, 
				authorNickname);
	}
	//提取blockquote属性的cite
	private long parseBlockquoteTag(String postsContent){
		Document doc = Jsoup.parse(postsContent);
		Element quoteTag = doc.selectFirst("blockquote");
		if(null == quoteTag){
			return -1L;
		}
		Element quoteAnchor = quoteTag.selectFirst("a");
		if(null == quoteAnchor){
			return 0L;
		}
		try{
			String quoteAnchorHref = quoteAnchor.attr("href");
			return Long.valueOf(quoteAnchorHref.substring(quoteAnchorHref.lastIndexOf("#posts-")+7));
		}catch(Exception e){
			return -2L;
		}
	}
}
