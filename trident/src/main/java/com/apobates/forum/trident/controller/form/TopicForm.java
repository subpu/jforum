package com.apobates.forum.trident.controller.form;

import com.apobates.forum.utils.Commons;

public class TopicForm extends ActionForm{
	private String volumes;
	private String board;
	private String title;
	private String content;
	//TopicCategory.id
	private String category;
	
	public String getBoard() {
		return board;
	}
	public long getLongBoard(){
		return covertStringToLong(getBoard(), 0L);
	}
	public void setBoard(String board) {
		this.board = board;
	}
	public String getTitle() {
		return Commons.isNotBlank(title)?Commons.htmlPurifier(title.trim()):"";
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return Commons.isNotBlank(content)?content.trim():"";
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getVolumes() {
		return volumes;
	}
	public int getIntegerVolumes(){
		return covertStringToInteger(getVolumes(), -1);
	}
	public void setVolumes(String volumes) {
		this.volumes = volumes;
	}
	public boolean isEmptyParameter(){
		return getVolumes().equals("-1") && getBoard().equals("0");
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public int getIntegerCategory(){
		return covertStringToInteger(getCategory(), 0);
	}
}
