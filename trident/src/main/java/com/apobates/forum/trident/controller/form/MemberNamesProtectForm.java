package com.apobates.forum.trident.controller.form;

public class MemberNamesProtectForm extends ActionForm{
	private String names;

	public String getNames() {
		return names;
	}

	public void setNames(String names) {
		this.names = names;
	}
}
