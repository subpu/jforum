package com.apobates.forum.trident.controller.form;


public class TopicConfigForm extends ActionForm{
	private String topicId;
	//是否是私人的
	//false表示公开,谁都可以看/true只有我的圈可以看
	private String privacy="0";
	//是否允许回复|是否允话评论
	private String reply="1";
	//开启回复通知
	private String notify="1";
	//A读
	// 积分需求
	private String readMinScore="0";
	// 会员组需求
	private String readLowMemberGroup="0";
	// 会员角色需求
	private String readLowMemberRole="1";
	// 会员等级需求
	private String readLowMemberLevel="0";
	
	//B写
	// 积分需求
	private String writeMinScore="1";
	// 会员组需求
	private String writeLowMemberGroup="1";
	// 会员角色需求
	private String writeLowMemberRole="1";
	// 会员等级需求
	private String writeLowMemberLevel="1";
	//b.1是否每人只能回一次|福利只能领一次
	private String atomPoster="0";
	// 不间断的操作次数
	//int
	private String writeMinInterrupt="3";
	//此列在实体中不存在
	private String boardId;
	private String volumesId;
	
	public String getVolumesId() {
		return volumesId;
	}
	public void setVolumesId(String volumesId) {
		this.volumesId = volumesId;
	}
	public String getBoardId() {
		return boardId;
	}
	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}
	public String getTopicId() {
		return topicId;
	}
	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}

	public String getPrivacy() {
		return privacy;
	}
	public void setPrivacy(boolean privacy) {
		setPrivacy(covertBooleanToString(privacy));
	}
	public void setPrivacy(String privacy) {
		this.privacy = privacy;
	}

	public String getReply() {
		return reply;
	}
	public void setReply(boolean reply) {
		setReply(covertBooleanToString(reply));
	}
	public void setReply(String reply) {
		this.reply = reply;
	}

	public String getNotify() {
		return notify;
	}
	public void setNotify(boolean notify) {
		setNotify(covertBooleanToString(notify));
	}
	public void setNotify(String notify) {
		this.notify = notify;
	}
	public String getReadMinScore() {
		return readMinScore;
	}
	public void setReadMinScore(String readMinScore) {
		this.readMinScore = readMinScore;
	}
	public String getReadLowMemberGroup() {
		return readLowMemberGroup;
	}
	public void setReadLowMemberGroup(String readLowMemberGroup) {
		this.readLowMemberGroup = readLowMemberGroup;
	}
	public String getReadLowMemberLevel() {
		return readLowMemberLevel;
	}
	public void setReadLowMemberLevel(String readLowMemberLevel) {
		this.readLowMemberLevel = readLowMemberLevel;
	}
	public String getWriteMinScore() {
		return writeMinScore;
	}
	public void setWriteMinScore(String writeMinScore) {
		this.writeMinScore = writeMinScore;
	}
	public String getWriteLowMemberGroup() {
		return writeLowMemberGroup;
	}
	public void setWriteLowMemberGroup(String writeLowMemberGroup) {
		this.writeLowMemberGroup = writeLowMemberGroup;
	}
	public String getWriteLowMemberLevel() {
		return writeLowMemberLevel;
	}
	public void setWriteLowMemberLevel(String writeLowMemberLevel) {
		this.writeLowMemberLevel = writeLowMemberLevel;
	}

	public String getAtomPoster() {
		return atomPoster;
	}
	public void setAtomPoster(boolean atomPoster) {
		setAtomPoster(covertBooleanToString(atomPoster));
	}
	public void setAtomPoster(String atomPoster) {
		this.atomPoster = atomPoster;
	}
	public String getReadLowMemberRole() {
		return readLowMemberRole;
	}
	public void setReadLowMemberRole(String readLowMemberRole) {
		this.readLowMemberRole = readLowMemberRole;
	}
	public String getWriteLowMemberRole() {
		return writeLowMemberRole;
	}
	public void setWriteLowMemberRole(String writeLowMemberRole) {
		this.writeLowMemberRole = writeLowMemberRole;
	}
	public String getWriteMinInterrupt() {
		return writeMinInterrupt;
	}
	public void setWriteMinInterrupt(String writeMinInterrupt) {
		this.writeMinInterrupt = writeMinInterrupt;
	}
	//
	public long getLongTopicId(){
		return covertStringToLong(getTopicId(), 0L);
	}
	public long getLongBoardId(){
		return covertStringToLong(getBoardId(), 0L);
	}
	public int getIntegerVolumesId(){
		return covertStringToInteger(getVolumesId(), 0);
	}
	//只读|读写模式
	
	//读
	public int getIntegerReadMinScore(){
		return covertStringToInteger(getReadMinScore(), 0);
	}
	public int getIntegerReadLowMemberLevel(){
		return covertStringToInteger(getReadLowMemberLevel(), 0);
	}
	public int getIntegerReadLowMemberGroup(){
		return covertStringToInteger(getReadLowMemberGroup(), 0); //游客
	}
	public int getIntegerReadLowMemberRole(){
		return covertStringToInteger(getReadLowMemberRole(), 1); //无
	}
	//写
	public int getIntegerWriteMinScore(){
		return covertStringToInteger(getWriteMinScore(), 1);
	}
	public int getIntegerWriteLowMemberLevel(){
		return covertStringToInteger(getWriteLowMemberLevel(), 1);
	}
	public int getIntegerWriteLowMemberGroup(){
		return covertStringToInteger(getWriteLowMemberGroup(), 1);
	}
	public int getIntegerWriteLowMemberRole(){
		return covertStringToInteger(getWriteLowMemberRole(), 1);
	}
	//扩展
	public int getIntegerWriteMinInterrupt(){
		return covertStringToInteger(getWriteMinInterrupt(), 3);
	}
	public boolean getBooleanAtomPoster() {
		return covertStringToBoolean(getAtomPoster());
	}
	public boolean getBooleanNotify() {
		return covertStringToBoolean(getNotify());
	}
	public boolean getBooleanReply() {
		return covertStringToBoolean(getReply());
	}
	public boolean getBooleanPrivacy() {
		return covertStringToBoolean(getPrivacy());
	}
}
