package com.apobates.forum.trident.rss;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
/**
 * RSS条目结果
 * 
 * @author xiaofanku
 * @since 20190713
 */
public class TopicRssResult implements Serializable{
	private static final long serialVersionUID = 5100432114588848735L;
	private final String title;
	private final String link;
	private final String desc;
	private final List<TopicRssItem> items;
	
	public TopicRssResult(String title, String link, String desc, List<TopicRssItem> items) {
		super();
		this.title = title;
		this.link = link;
		this.desc = desc;
		this.items = new CopyOnWriteArrayList<>(items);
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getLink() {
		return link;
	}
	
	public String getDesc() {
		return desc;
	}
	
	public List<TopicRssItem> getItems() {
		return items;
	}
}
