package com.apobates.forum.trident.vo;

import java.io.Serializable;
import com.apobates.forum.utils.Commons;
/**
 * 消息的内容摘要的集合类
 * 
 * @author xiaofanku
 * @since 20191015
 */
public final class MutualLetter implements Serializable{
	private static final long serialVersionUID = -7340152449468960695L;
	//第几页
	private final int page;
	//每页显示几条记录
	private final int pageSize;
	//发件人/message/view/more的请求参数
	private final long sender;
	//收件人/message/view/more的请求参数
	private final long receiver;
	//是否还有下一页
	private final boolean more;
	
	public MutualLetter(long sender, long receiver, int page, int pageSize, boolean more) {
		super();
		this.page = page;
		this.pageSize = pageSize;
		this.sender = sender;
		this.receiver = receiver;
		this.more = more;
	}
	
	public int getPage() {
		return page;
	}
	public int getPageSize() {
		return pageSize;
	}
	public long getSender() {
		return sender;
	}
	public long getReceiver() {
		return receiver;
	}
	public boolean isMore() {
		return more;
	}
	
	public String toJson(){
		return Commons.toJson(this);
	}
}
