package com.apobates.forum.trident.tag;

import java.util.Arrays;

/**
 * 数组中搜索
 * 
 * @author xiaofanku@live.cn
 * @since 20180507
 */
public class ArraryContainsTag {
	public static boolean contains(String[] array, String str) {
		if (null==array || array.length == 0) {
			return false;
		}
		return Arrays.asList(array).contains(str);
	}
}
