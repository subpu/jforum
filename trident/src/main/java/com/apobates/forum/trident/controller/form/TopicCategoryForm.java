package com.apobates.forum.trident.controller.form;

public class TopicCategoryForm extends ActionForm{
	//名称
	private String names;
	//英文值
	private String envalue;
	
	public String getNames() {
		return names;
	}
	public void setNames(String names) {
		this.names = names;
	}
	public String getEnvalue() {
		return envalue;
	}
	public void setEnvalue(String envalue) {
		this.envalue = envalue;
	}
}
