package com.apobates.forum.trident.controller.admin;

import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.apobates.forum.member.api.service.MemberActiveRecordsService;
import com.apobates.forum.member.entity.MemberActiveRecords;
import com.apobates.forum.utils.FrontPageURL;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.PageRequest;
import com.apobates.forum.utils.persistence.Pageable;
/**
 * 会员操作日志控制器
 * 
 * @author xiaofanku
 * @since 20190717
 */
@Controller
@RequestMapping(value = "/admin/member/action")
public class AdminMemberActiveRecordsController {
	@Autowired
	private MemberActiveRecordsService memberActiveRecordsService;
	@Value("${site.pageSize}")
	private int pageSize;
	
	@GetMapping(path="/")
	public String listPage(
			@RequestParam(value = "p", required = false, defaultValue = "1") int page, 
			@RequestParam(value = "id", required = false, defaultValue = "0") long memberId, 
			HttpServletRequest request, 
			Model model) {
		FrontPageURL fpbuild = new FrontPageURL(request.getContextPath() + "/admin/member/action/").addPageSize("number", pageSize);
		if(memberId>0){
			fpbuild = fpbuild.addParameter("id", memberId);
		}
		Pageable pr = new PageRequest(page, fpbuild.getPageSize());
		Page<MemberActiveRecords> rs = (memberId>0)?memberActiveRecordsService.getByMember(memberId, pr):memberActiveRecordsService.getAll(pr);
		model.addAttribute("rs", rs.getResult().collect(Collectors.toList()));
		model.addAttribute("pageData", pr.toData(fpbuild, rs.getTotalElements()));
		return "admin/member/action";
	}
}
