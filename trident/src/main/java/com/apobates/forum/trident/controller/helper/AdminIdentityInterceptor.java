package com.apobates.forum.trident.controller.helper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import com.apobates.forum.member.entity.MemberRoleEnum;
import com.apobates.forum.member.storage.OnlineMemberStorage;
import com.apobates.forum.member.storage.core.MemberSessionBean;
/**
 * /admin用户的身份检查
 * @author xiaofanku
 * @since 20190826
 */
public class AdminIdentityInterceptor extends HandlerInterceptorAdapter{
	@Value("${site.sessionSymbol}")
	private String sessionSymbol;
	@Autowired
	private OnlineMemberStorage onlineMemberStorage;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
		// Cookie
		MemberSessionBean mbean = onlineMemberStorage.getInstance(request, "AdminIdentityInterceptor").orElse(null);
		if (null!=mbean && mbean.isOnline() && MemberRoleEnum.ADMIN == mbean.getRole()) {
			//SESSION有没有，没有放进去
			if(request.getSession().getAttribute(sessionSymbol) == null){
				request.getSession().setAttribute(sessionSymbol, mbean);
			}
			return true;
		}
		response.sendRedirect(request.getContextPath() + "/");
		return false;
	}
}
