package com.apobates.forum.trident.controller.form;

import com.apobates.forum.utils.Commons;

public class TopicPublishForm extends ActionForm{
	private String volumes;
	private String board;
	//TopicCategory.value
	private String category;
	
	private String title;
	private String content;
	private String words;
	
	public String getVolumes() {
		return volumes;
	}
	public int getIntegerVolumes(){
		return covertStringToInteger(getVolumes(), -1);
	}
	public void setVolumes(String volumes) {
		this.volumes = volumes;
	}
	public String getBoard() {
		return board;
	}
	public long getLongBoard(){
		return covertStringToLong(getBoard(), 0L);
	}
	public void setBoard(String board) {
		this.board = board;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getWords() {
		return words;
	}
	public String[] getWordsArray(){
		if(Commons.isNotBlank(getWords())){
			return getWords().split(",");
		}
		return new String[]{};
	}
	public void setWords(String words) {
		this.words = words;
	}
}
