package com.apobates.forum.trident.vo;

import java.io.Serializable;
/**
 * 会员操作日志(AdminMemberController.java)
 * 
 * @author xiaofanku
 * @since 20190829
 */
public final class MemberActionBean implements Serializable{
	private static final long serialVersionUID = 4521390974926510086L;
	//操作名称
	private final String action;
	//日期
	private final String dateTime;
	private final String ipAddr;
	//状态结果
	private final String status;
	
	public MemberActionBean(String action, String dateTime, String ipAddr, String status) {
		super();
		this.action = action;
		this.dateTime = dateTime;
		this.ipAddr = ipAddr;
		this.status = status;
	}

	public String getAction() {
		return action;
	}

	public String getDateTime() {
		return dateTime;
	}

	public String getIpAddr() {
		return ipAddr;
	}

	public String getStatus() {
		return status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((action == null) ? 0 : action.hashCode());
		result = prime * result + ((dateTime == null) ? 0 : dateTime.hashCode());
		result = prime * result + ((ipAddr == null) ? 0 : ipAddr.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MemberActionBean other = (MemberActionBean) obj;
		if (action == null) {
			if (other.action != null)
				return false;
		} else if (!action.equals(other.action))
			return false;
		if (dateTime == null) {
			if (other.dateTime != null)
				return false;
		} else if (!dateTime.equals(other.dateTime))
			return false;
		if (ipAddr == null) {
			if (other.ipAddr != null)
				return false;
		} else if (!ipAddr.equals(other.ipAddr))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	
}
