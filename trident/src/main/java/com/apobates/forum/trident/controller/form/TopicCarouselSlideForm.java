package com.apobates.forum.trident.controller.form;

import com.apobates.forum.trident.fileupload.ImageStorageExecutor;

public class TopicCarouselSlideForm extends ActionForm{
	//int,轮播图ID
	private String carousel;
	private String title;
	private String link;
	private String ranking;
	//上传后的图片地址
	private String fileurl;
	//数据库中保存的编码后的图片地址
	private String imageAddr;
	
	public String getCarousel() {
		return carousel;
	}
	public void setCarousel(String carousel) {
		this.carousel = carousel;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getRanking() {
		return ranking;
	}
	public void setRanking(String ranking) {
		this.ranking = ranking;
	}
	public int getIntegerRanking(){
		return covertStringToInteger(getRanking(), 1);
	}
	public int getIntegerCarousel(){
		return covertStringToInteger(getCarousel(), 0);
	}
	public String getFileurl() {
		return fileurl;
	}
	public void setFileurl(String fileurl) {
		this.fileurl = fileurl;
	}
	public String getEncodePictureAddr(ImageStorageExecutor executor){
		return super.uploadAndEncodeFile(getFileurl(), executor);
	}
	public String getImageAddr() {
		return imageAddr;
	}
	public void setImageAddr(String imageAddr) {
		this.imageAddr = imageAddr;
	}
	
}
