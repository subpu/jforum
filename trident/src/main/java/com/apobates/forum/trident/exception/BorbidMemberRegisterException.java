package com.apobates.forum.trident.exception;
/**
 * 禁止注册会员
 * 
 * @author xiaofanku
 * @since 20190807
 */
public class BorbidMemberRegisterException extends InterruptedException{
	private static final long serialVersionUID = -7745679113640804874L;

	public BorbidMemberRegisterException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BorbidMemberRegisterException(String s) {
		super(s);
		// TODO Auto-generated constructor stub
	}
}
