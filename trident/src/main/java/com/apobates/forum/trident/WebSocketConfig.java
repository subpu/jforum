package com.apobates.forum.trident;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import com.apobates.forum.trident.controller.helper.MessagePushHandler;
import com.apobates.forum.trident.controller.helper.TopicUpdatedHandler;
/**
 * WebSocket配置文件类
 * 
 * @author xiaofanku
 * @since 20200303
 */
@Configuration
@EnableWebSocket
@PropertySource(value="classpath:global.properties", ignoreResourceNotFound=true, encoding="UTF-8")
public class WebSocketConfig implements WebSocketConfigurer{
	@Value("${site.domain}")
	private String siteDomain;
	
	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		registry.addHandler(topicUpdatedHandler(), "/sock/updated").setAllowedOrigins(siteDomain).withSockJS();
		registry.addHandler(messageUpdatedHandler(), "/sock/message").setAllowedOrigins(siteDomain).withSockJS();
	}
	//WebSocket
	@Bean(name="topicUpdatedHandler")
	public TopicUpdatedHandler topicUpdatedHandler(){
		return new com.apobates.forum.trident.controller.helper.TopicUpdatedHandler();
	}
	@Bean(name="messageUpdatedHandler")
	public MessagePushHandler messageUpdatedHandler(){
		return new com.apobates.forum.trident.controller.helper.MessagePushHandler();
	}
}
