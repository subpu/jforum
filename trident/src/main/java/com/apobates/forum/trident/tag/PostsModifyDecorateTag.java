package com.apobates.forum.trident.tag;

import java.io.IOException;
import java.io.StringWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import com.apobates.forum.utils.DateTimeUtils;
/**
 * forum:decorate 标签内的子标签:forum:modify
 * 
 * @author xiaofanku
 *
 */
public class PostsModifyDecorateTag extends SimpleTagSupport{
	@Override
	public void doTag() throws JspException, IOException {
		// 父标签:PostsDecorateTag
		PostsDecorateTag parentTag = (PostsDecorateTag) getParent();
		if (null==parentTag) {
			throw new JspTagException("标签与父标签:PostsDecorateTag,配合使用");
		}
		// 获得标签内容体
		final StringWriter stringWriter = new StringWriter();
		String modifyer = "Anno";
		String modifyDate = "-";
		//
		try {
			modifyer = parentTag.getPosts().getModifyMemberNickname();
		} catch (Exception e) {
		}
		getJspContext().setAttribute("modifyer", modifyer);
		try {
			modifyDate = DateTimeUtils.getRFC3339(parentTag.getPosts().getModifyDateTime());
		} catch (Exception e) {
		}
		getJspContext().setAttribute("modifyDate", modifyDate);
		//
		getJspBody().invoke(stringWriter);
		//
		parentTag.setModifyTip(stringWriter.toString());
	}
}
