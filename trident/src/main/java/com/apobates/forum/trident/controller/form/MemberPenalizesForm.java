package com.apobates.forum.trident.controller.form;

public class MemberPenalizesForm extends ActionForm{
	private long memberId;
	private String names;
	//MemberStatusEnum.symbol
	//惩罚:禁足
	private String arrive;
	//开始日期
	//private String date;
	//时长:1
	private String limit;
	//单位:天
	private String unit;
	//理由
	private String reason;
	//证据
	private String evidences;
	
	public long getMemberId() {
		return memberId;
	}
	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}
	public String getNames() {
		return names;
	}
	public void setNames(String names) {
		this.names = names;
	}
	public String getArrive() {
		return arrive;
	}
	public void setArrive(String arrive) {
		this.arrive = arrive;
	}
	public String getLimit() {
		return limit;
	}
	public void setLimit(String limit) {
		this.limit = limit;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getEvidences() {
		return evidences;
	}
	public void setEvidences(String evidences) {
		this.evidences = evidences;
	}
}
