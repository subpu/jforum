package com.apobates.forum.trident.vo;

import java.io.Serializable;
import com.apobates.forum.core.entity.TopicCarouselSlide;
/**
 * 幻灯片前端显示
 * 
 * @author xiaofanku
 * @since 20191103
 */
public class CarouselSlideItem implements Serializable{
	private static final long serialVersionUID = -8803015347244404208L;
	private final int id;
	private final String caption;
	private final String link;
	private final String imageAddr;
	private final boolean first;
	
	public CarouselSlideItem(int id, String caption, String link, String imageAddr) {
		super();
		this.id = id;
		this.caption = caption;
		this.link = link;
		this.imageAddr = imageAddr;
		this.first = (id==1);
	}
	
	public CarouselSlideItem(TopicCarouselSlide carouselSlide){
		this.id = carouselSlide.getRanking();
		this.caption = carouselSlide.getTitle();
		this.link = carouselSlide.getLink();
		this.imageAddr = carouselSlide.getImageAddr();
		this.first = (carouselSlide.getRanking()==1);
	}
	
	public int getId() {
		return id;
	}

	public String getCaption() {
		return caption;
	}

	public String getLink() {
		return link;
	}

	public String getImageAddr() {
		return imageAddr;
	}

	public boolean isFirst() {
		return first;
	}
	
}
