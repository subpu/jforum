package com.apobates.forum.trident.tag;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import com.apobates.forum.utils.Commons;
/**
 * 清理html
 * @author xiaofanku
 * @since 20190804
 */
public class HtmlPurifierTag extends SimpleTagSupport{
	private String value;
	private int length=0;
	
	public void setValue(String value) {
		this.value = value;
	}
	public void setLength(int length) {
		this.length = length;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		String tmpPc = Commons.htmlPurifier(value);
		if(length>0){
			try{
				tmpPc = tmpPc.substring(0, length)+" ...";
			}catch(StringIndexOutOfBoundsException e){
				//不到指定的数量
			}
		}
		getJspContext().getOut().print(tmpPc);
	}
}
