package com.apobates.forum.trident.spa;

import java.io.Serializable;

public class EntryItem implements Serializable, Comparable<EntryItem>{
	private static final long serialVersionUID = 5913290068003616760L;
	private final String title;
	private final String link;
	private final String summary;
	
	public EntryItem(String title, String link) {
		super();
		this.title = title;
		this.link = link;
		this.summary = "";
	}
	
	public EntryItem(String title, String link, String summary) {
		super();
		this.title = title;
		this.link = link;
		this.summary = summary;
	}
	
	public String getTitle() {
		return title;
	}

	public String getLink() {
		return link;
	}

	public String getSummary() {
		return summary;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((link == null) ? 0 : link.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EntryItem other = (EntryItem) obj;
		if (link == null) {
			if (other.link != null)
				return false;
		} else if (!link.equals(other.link))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(EntryItem o) {
		EntryItem other = o;
		return this.getLink().compareTo(other.getLink());
	}
}
