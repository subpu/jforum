package com.apobates.forum.trident.controller.form;

import java.util.Optional;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.utils.Commons;
import com.apobates.forum.utils.lang.EnumArchitecture;
/**
 * 
 * @author xiaofanku
 *
 */
public class ForumScoreRoleForm extends ActionForm{
	//ForumActionEnum.symbol
	private String action;
	//频次/int
	private String degree;
	//等级/int
	private String level;
	//得分/double
	private String score;
	
	public ForumActionEnum getEnumAction(){
		int d = covertStringToInteger(getAction(), -1);
		Optional<ForumActionEnum> a = EnumArchitecture.getInstance(d, ForumActionEnum.class);
		ForumActionEnum action = null;
		if(a.isPresent()) {
			action = a.get();
		}
		return action;
	}
	public String getAction() {
		return action;
	}
	public void setAction(ForumActionEnum action){
		setAction(action.getSymbol()+"");
	}
	public void setAction(String action) {
		this.action = action;
	}
	public int getIntegerDegree(){
		return Commons.stringToInteger(getDegree(), 1);
	}
	public String getDegree() {
		return degree;
	}
	public void setDegree(String degree) {
		this.degree = degree;
	}
	public int getIntegerLevel(){
		return Commons.stringToInteger(getLevel(), 1);
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public double getDoubleScore(){
		return Commons.stringToDouble(()->getScore(), 1.0D);
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
}
