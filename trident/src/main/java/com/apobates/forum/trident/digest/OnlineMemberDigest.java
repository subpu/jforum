package com.apobates.forum.trident.digest;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import com.apobates.forum.member.entity.MemberOnline;
import com.apobates.forum.utils.Commons;
import com.apobates.forum.utils.DateTimeUtils;
/**
 * 在线会员摘要/输出的在线信息
 * 
 * @author xiaofanku
 * @since 20190630
 */
public class OnlineMemberDigest implements Serializable{
	private static final long serialVersionUID = -5779752118545061174L;
	//会员ID
	private final long id;
	//是否在线,true:在线
	private final boolean result;
	//活跃的日期
	private final LocalDateTime active;
	
	private OnlineMemberDigest(long id, boolean result, LocalDateTime active) {
		super();
		this.id = id;
		this.result = result;
		this.active = active;
	}

	public long getId() {
		return id;
	}

	public boolean isResult() {
		return result;
	}

	public LocalDateTime getActive() {
		return active;
	}
	
	public Map<String,String> toMap(){
		Map<String, String> data = new HashMap<>();
		data.put("id", id+"");
		data.put("result", result?"1":"0");
		data.put("active", DateTimeUtils.getRFC3339(active));
		return data;
	}
	
	public String toJson(){
		return Commons.toJson(toMap());
	}
	
	private static boolean isOnline(LocalDateTime activeDateTime){
		//30分钟前 == > 在线
		//[middle] ==> 离开
		//1天前  == > 离线
		return DateTimeUtils.diffMinutes(LocalDateTime.now(), activeDateTime)<30;
	}
	
	public static OnlineMemberDigest getInstance(MemberOnline mo){
		return new OnlineMemberDigest(mo.getMid(), isOnline(mo.getActiveDateTime()), mo.getActiveDateTime());
	}
	
	public static OnlineMemberDigest getInstance(long memberId, LocalDateTime lastActiveDateTime){
		return new OnlineMemberDigest(memberId, isOnline(lastActiveDateTime), lastActiveDateTime);
	}
}
