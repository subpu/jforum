package com.apobates.forum.trident.controller.form;

import java.time.LocalDateTime;
import com.apobates.forum.member.entity.ForumCalendarUnitEnum;
import com.apobates.forum.utils.lang.EnumArchitecture;

public class BoardCarouselForm extends ActionForm{
	private String volumesId;
	private String boardId;
	private String carouselId;
	//yyyy-MM-dd
	//private String expireDate;
	//时长
	private String limit;
	//单位
	private String unit; 
	public String getVolumesId() {
		return volumesId;
	}
	public int getIntegerVolumes(){
		return covertStringToInteger(getVolumesId(), -1);
	}
	public void setVolumesId(String volumesId) {
		this.volumesId = volumesId;
	}
	public String getBoardId() {
		return boardId;
	}
	public long getLongBoard(){
		return covertStringToLong(getBoardId(), 0L);
	}
	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}
	public String getCarouselId() {
		return carouselId;
	}
	public int getIntegerCarousel(){
		return covertStringToInteger(getCarouselId(), 0);
	}
	public void setCarouselId(String carouselId) {
		this.carouselId = carouselId;
	}
	public int getIntegerLimit(){
		return covertStringToInteger(getLimit(), 1);
	}
	public String getLimit() {
		return limit;
	}
	public void setLimit(String limit) {
		this.limit = limit;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public ForumCalendarUnitEnum getEnumUnit(){
		Integer unitEnumSymbol = covertStringToInteger(getUnit(), 0);
		return EnumArchitecture.getInstance(unitEnumSymbol, ForumCalendarUnitEnum.class).orElse(ForumCalendarUnitEnum.DAY);
	}
	public LocalDateTime getDateTimeExpireDate() {
		Integer limit = getIntegerLimit();
		return getEnumUnit().plusDateTime(limit, null);
	}
}
