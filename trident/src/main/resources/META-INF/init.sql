use jforum;
-- member_protect
INSERT INTO `apo_member_protect` (`ID`,`MEMBERNAMES`,`STATUS`) VALUES (1,'admin',1);
INSERT INTO `apo_member_protect` (`ID`,`MEMBERNAMES`,`STATUS`) VALUES (2,'administrator',1);
INSERT INTO `apo_member_protect` (`ID`,`MEMBERNAMES`,`STATUS`) VALUES (3,'root',1);
INSERT INTO `apo_member_protect` (`ID`,`MEMBERNAMES`,`STATUS`) VALUES (4,'guest',1);
INSERT INTO `apo_member_protect` (`ID`,`MEMBERNAMES`,`STATUS`) VALUES (5,'robot',1);
INSERT INTO `apo_member_protect` (`ID`,`MEMBERNAMES`,`STATUS`) VALUES (6,'moderator',1);
INSERT INTO `apo_member_protect` (`ID`,`MEMBERNAMES`,`STATUS`) VALUES (7,'master',1);
INSERT INTO `apo_member_protect` (`ID`,`MEMBERNAMES`,`STATUS`) VALUES (8,'webmaster',1);
INSERT INTO `apo_member_protect` (`ID`,`MEMBERNAMES`,`STATUS`) VALUES (9,'sitemaster',1);
INSERT INTO `apo_member_protect` (`ID`,`MEMBERNAMES`,`STATUS`) VALUES (10,'manager',1);
INSERT INTO `apo_member_protect` (`ID`,`MEMBERNAMES`,`STATUS`) VALUES (11,'super',1);
-- member
-- INSERT INTO `apo_member` (`ID`,`AVATARURI`,`MGROUP`,`MROLE`,`NAMES`,`NICKNAME`,`PSWD`,`REGISTEDATETIME`,`SALT`,`SIGNATURE`,`STATUS`) VALUES (1,NULL,'CARD','ADMIN','xiaofanku','xiaofanku','3ec8044f03c16044c930a2789d98930c2f45cdceb9391c92f04f9ecbe4a01db1','2019-07-31 16:59:25','XmYfj4','','ACTIVE');
-- member_level(Music)
INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (1,'image://defat/ico/default_icon.png',1,'192K',4,1);
INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (2,'image://defat/ico/default_icon.png',5,'256K',14,1);
INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (3,'image://defat/ico/default_icon.png',15,'320K',29,1);
INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (4,'image://defat/ico/default_icon.png',30,'16/44.1K',49,1);
INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (5,'image://defat/ico/default_icon.png',50,'24/44.1K',99,1);
INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (6,'image://defat/ico/default_icon.png',100,'24/48K',199,1);
INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (7,'image://defat/ico/default_icon.png',200,'24/96K',499,1);
INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (8,'image://defat/ico/default_icon.png',500,'DSD64',999,1);
INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (9,'image://defat/ico/default_icon.png',1000,'32/96K',1999,1);
INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (10,'image://defat/ico/default_icon.png',2000,'24/176K',2999,1);
-- member_level(Diablo3)
-- INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (1,'image://defat/ico/default_icon.png',1,'正义陨落',4,1);
-- INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (2,'image://defat/ico/default_icon.png',5,'初识莉亚',14,1);
-- INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (3,'image://defat/ico/default_icon.png',15,'狂君浩劫',29,1);
-- INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (4,'image://defat/ico/default_icon.png',30,'追寻神教',49,1);
-- INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (5,'image://defat/ico/default_icon.png',50,'沙漠之影',99,1);
-- INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (6,'image://defat/ico/default_icon.png',100,'谎言疑虑',199,1);
-- INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (7,'image://defat/ico/default_icon.png',200,'血染黄沙',499,1);
-- INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (8,'image://defat/ico/default_icon.png',500,'戍卫要塞',999,1);
-- INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (9,'image://defat/ico/default_icon.png',1000,'罪恶之核',1999,1);
-- INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (10,'image://defat/ico/default_icon.png',2000,'莉亚之泣',2999,1);
-- INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (11,'image://defat/ico/default_icon.png',3000,'万恶之源',5999,1);
-- INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (12,'image://defat/ico/default_icon.png',6000,'亡者之魂',9999,1);
-- INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (13,'image://defat/ico/default_icon.png',10000,'殁天使',17999,1);

-- INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (14,'image://defat/ico/default_icon.png',18000,'殁天使',29999,1);
-- INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (15,'image://defat/ico/default_icon.png',30000,'殁天使',59999,1);
-- INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (16,'image://defat/ico/default_icon.png',60000,'殁天使',99999,1);
-- INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (17,'image://defat/ico/default_icon.png',100000,'殁天使',299999,1);
-- INSERT INTO `apo_member_level` (`ID`,`IMAGEADDR`,`MINSCORE`,`NAMES`,`SCORE`,`STATUS`) VALUES (18,'image://defat/ico/default_icon.png',300000,'殁天使',599999,1);
-- score_role
INSERT INTO `apo_score_role` (`ID`,`ACTION`,`DEGREE`,`LEVEL`,`SCORE`,`STATUS`) VALUES (1,'TOPIC_PUBLISH',2,1,1,1);
INSERT INTO `apo_score_role` (`ID`,`ACTION`,`DEGREE`,`LEVEL`,`SCORE`,`STATUS`) VALUES (2,'POSTS_REPLY',4,1,1,1);
INSERT INTO `apo_score_role` (`ID`,`ACTION`,`DEGREE`,`LEVEL`,`SCORE`,`STATUS`) VALUES (3,'TOPIC_BEST',1,1,2,1);
INSERT INTO `apo_score_role` (`ID`,`ACTION`,`DEGREE`,`LEVEL`,`SCORE`,`STATUS`) VALUES (4,'TOPIC_TOP',1,1,4,1);
INSERT INTO `apo_score_role` (`ID`,`ACTION`,`DEGREE`,`LEVEL`,`SCORE`,`STATUS`) VALUES (5,'TOPIC_DEL',1,1,-2,1);
INSERT INTO `apo_score_role` (`ID`,`ACTION`,`DEGREE`,`LEVEL`,`SCORE`,`STATUS`) VALUES (6,'POSTS_DEL',1,1,-1,1);
-- smiley_theme
INSERT INTO `apo_smiley_theme` (`ID`,`DIRECTNAMES`,`ENTRYDATETIME`,`LABEL`,`RANKING`,`STATUS`,`TITLE`) VALUES (1,'default','2019-08-26 16:35:22','default',1,1,'QQ表情');
INSERT INTO `apo_smiley_theme` (`ID`,`DIRECTNAMES`,`ENTRYDATETIME`,`LABEL`,`RANKING`,`STATUS`,`TITLE`) VALUES (2,'onion','2019-08-26 17:03:01','onion',2,1,'洋葱头');
INSERT INTO `apo_smiley_theme` (`ID`,`DIRECTNAMES`,`ENTRYDATETIME`,`LABEL`,`RANKING`,`STATUS`,`TITLE`) VALUES (3,'yoci','2019-08-28 11:57:10','yoci',3,1,'悠嘻猴');
-- board_group

-- board
-- INSERT INTO `apo_board` (`ID`,`DESCRIPTION`,`ENTRYDATETIME`,`IMAGEADDR`,`RANKING`,`STATUS`,`TITLE`,`VOLUMESID`) VALUES (1,'有话好好说,有事再商言商','2019-07-31 17:03:48','image://defat/ico/default_icon.png',1,'ACTIVE','站务专区',0);

-- member

