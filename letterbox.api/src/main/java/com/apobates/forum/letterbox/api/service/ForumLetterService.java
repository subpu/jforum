package com.apobates.forum.letterbox.api.service;

import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.letterbox.entity.ForumLetter;
import com.apobates.forum.letterbox.entity.ForumLetterTypeEnum;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;

public interface ForumLetterService {
	/**
	 * 创建一条私信(ForumLetterTypeEnum.LETTER)信件
	 * 
	 * @param title            主题
	 * @param content          内容
	 * @param receiver         收件人
	 * @param receiverNickname 收件人帐号
	 * @param sender           发件人
	 * @param senderNickname   发件人帐号
	 * @return
	 */
	long create(String title, String content, long receiver, String receiverNickname, long sender, String senderNickname)throws IllegalStateException;
	
	/**
	 * 创建一条信件
	 * 
	 * @param label            类型
	 * @param title            主题
	 * @param content          内容
	 * @param receiver         收件人
	 * @param receiverNickname 收件人帐号
	 * @param sender           发件人
	 * @param senderNickname   发件人帐号
	 * @return
	 */
	long create(ForumLetterTypeEnum label, String title, String content, long receiver, String receiverNickname, long sender, String senderNickname)throws IllegalStateException;
	
	/**
	 * 创建一条公告(ForumLetterTypeEnum.CLAIM)信件
	 * 
	 * @param title          主题
	 * @param content        内容
	 * @param sender         发件人
	 * @param senderNickname 发件人帐号
	 * @return
	 */
	long create(String title, String content, long sender, String senderNickname)throws IllegalStateException;
	
	/**
	 * 创建一封信件,未对内容进行HTML过滤,调用时请注意
	 * 
	 * @param letter 信件对象实例
	 * @return
	 */
	Optional<ForumLetter> create(ForumLetter letter)throws IllegalStateException;
	
	/**
	 * 查看收件人和发件人之间往来的所有信件
	 * 
	 * @param sender   发件人
	 * @param receiver 收件人
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<ForumLetter> getMutualLetter(long sender, long receiver, Pageable pageable);

	
	/**
	 * 查看收件人和发件人之间往来的所有信件
	 * 
	 * @param sender   发件人
	 * @param receiver 收件人
	 * @return
	 */
	Stream<ForumLetter> getMutualLetter(long sender, long receiver);
	
	/**
	 * 查看今日的公告类信件
	 * 
	 * @return
	 */
	Stream<ForumLetter> getTodayClaim();
	
	/**
	 * 创建一条私信(ForumLetterTypeEnum.LETTER)信件
	 * 
	 * @param title            主题
	 * @param content          内容
	 * @param receiver         收件人
	 * @param receiverNickname 收件人帐号
	 * @param sender           发件人
	 * @param senderNickname   发件人帐号
	 * @return
	 */
	Optional<ForumLetter> build(String title, String content, long receiver, String receiverNickname, long sender, String senderNickname);
	
	/**
	 * 查看指定的信件
	 * 
	 * @param id 信件ID
	 * @return
	 */
	Optional<ForumLetter> get(long id);
	
	/**
	 * 查看指定的信件,同时加载信件的收件人信息
	 * 
	 * @param id 信件ID
	 * @return
	 */
	Optional<ForumLetter> getOneLazyReceiver(long id);
}
