package com.apobates.forum.letterbox.api.dao;

import java.util.stream.Stream;
import com.apobates.forum.letterbox.entity.ForumLetter;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;
import com.apobates.forum.utils.persistence.PagingAndSortingRepository;

public interface ForumLetterDao extends PagingAndSortingRepository<ForumLetter, Long>{
	/**
	 * 查看指定会员之间往来的所有信件
	 * 
	 * @param sender   发件人/会员ID
	 * @param receiver 收件人/会员ID
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<ForumLetter> findAll(long sender, long receiver, Pageable pageable);
	
	/**
	 * 查看指定会员之间往来的所有信件
	 * 
	 * @param sender   发件人/会员ID
	 * @param receiver 收件人/会员ID
	 * @return
	 */
	Stream<ForumLetter> findAll(long sender, long receiver);
	
	/**
	 * 查看今日的公告类信件
	 * 
	 * @return
	 */
	Stream<ForumLetter> getTodayClaim();
}
