package com.apobates.forum.letterbox.api.dao;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Stream;
import com.apobates.forum.letterbox.entity.ForumLetter;
import com.apobates.forum.letterbox.entity.ForumLetterTypeEnum;
import com.apobates.forum.letterbox.entity.Outbox;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;
import com.apobates.forum.utils.persistence.PagingAndSortingRepository;

public interface OutboxDao extends PagingAndSortingRepository<ForumLetter,Long>{
	/**
	 * 查看指定会员发送的信件
	 * 
	 * @param memberId 发件人/会员ID
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<ForumLetter> findAllBySender(long memberId, Pageable pageable);
	long countBySender(long memberId);
	
	/**
	 * 查看指定会员发送的信件
	 * 
	 * @param memberId 发件人/会员ID
	 * @param typed    消息类型
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<ForumLetter> findAllBySenderAndType(long memberId, ForumLetterTypeEnum typed, Pageable pageable);
	long countBySenderAndType(long memberId, ForumLetterTypeEnum typed);
	
	/**
	 * 查看指定信件的收件人记录
	 * 
	 * @param letterId 信件ID
	 * @return
	 */
	Stream<Outbox> findAllByLetter(long letterId);
	
	/**
	 * 查看指定信件的发送记录
	 * 
	 * @param letterIdSet 信件记录ID列表ForumLetter.id(!= Inbox.id)
	 * @return
	 */
	Stream<Outbox> findAllByLetter(Collection<Long> letterIdSet);
	
	/**
	 * 批量保存多条信件发送记录
	 * 
	 * @param letterSendRecords 信件发送记录
	 * @return
	 */
	int batchSave(Set<Outbox> letterSendRecords);
}
