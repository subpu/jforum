package com.apobates.forum.letterbox.api.service;

import com.apobates.forum.letterbox.entity.ForumLetter;
import com.apobates.forum.letterbox.entity.ForumLetterTypeEnum;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;

public interface OutboxService {
	/**
	 * 查看指定会员的发件箱,级联加载信件的收件人
	 * 
	 * @param memberId 会员ID/信件的发件人
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<ForumLetter> getSent(long memberId, Pageable pageable);
	long countSentByMember(long memberId);
	
	/**
	 * 查看指定会员的发件箱,级联加载信件的收件人
	 * 
	 * @param memberId 会员ID/信件的发件人
	 * @param label    信件类型
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<ForumLetter> getSent(long memberId, ForumLetterTypeEnum label, Pageable pageable);
	long countSentByMember(long memberId, ForumLetterTypeEnum label);
}
