package com.apobates.forum.core.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
/**
 * 像册
 * 
 * @author xiaofanku
 * @since 20190301
 */
@Entity
@Table(name = "apo_album_picture")
public class AlbumPicture implements Serializable,Comparable<AlbumPicture>{
	private static final long serialVersionUID = -984105104691136873L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	//像册的ID
	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
	@JoinColumn(name="albumId")
	private Album album;
	//图片的连接地址
	private String link;
	//图片的alt内容
	private String caption;
	//状态
	private boolean status;
	//缩略图
	@Transient
	private String thumb;
	//是否是封面
	private boolean cover;
	//显示的顺序
	private int ranking;
	
	//empty constructor for JPA instantiation
	public AlbumPicture() {super();}
	
	public AlbumPicture(Album album, String link, String caption, boolean status, int ranking, boolean cover) {
		super();
		this.album = album;
		this.link = link;
		this.caption = caption;
		this.status = status;
		this.ranking = ranking;
		this.cover = cover;
	}
	public AlbumPicture(Album album, String link, String caption, int ranking, boolean status) {
		this(album, link, caption, status, ranking, false);
	}
	public AlbumPicture(Album album, String link, String caption, int ranking) {
		this(album, link, caption, true, ranking, false);
	}
	public AlbumPicture(String link, String caption, int ranking) {
		super();
		this.link = link;
		this.caption = caption;
		this.ranking = ranking;
		this.status = true;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Album getAlbum() {
		return album;
	}
	public void setAlbum(Album album) {
		this.album = album;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getCaption() {
		return caption;
	}
	public void setCaption(String caption) {
		this.caption = caption;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public boolean isCover() {
		return cover;
	}
	public void setCover(boolean cover) {
		this.cover = cover;
	}
	public String getThumb() {
		return thumb;
	}
	public void setThumb(String thumb) {
		this.thumb = thumb;
	}
	public int getRanking() {
		return ranking;
	}
	public void setRanking(int ranking) {
		this.ranking = ranking;
	}

	@Override
	public int compareTo(AlbumPicture o) {
		return getRanking() - o.getRanking();
	}

	@Transient
	public long getAlbumId(){
		if(getAlbum()!=null){
			return getAlbum().getId();
		}
		return 0L;
	}
}
