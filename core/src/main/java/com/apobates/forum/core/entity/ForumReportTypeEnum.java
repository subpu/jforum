package com.apobates.forum.core.entity;

import com.apobates.forum.utils.lang.EnumArchitecture;
/**
 * 回复/话题举报类型
 * 
 * @author xiaofanku
 * @since 20190329
 */
public enum ForumReportTypeEnum implements EnumArchitecture {
	AD(1, "广告"),
	SEXY(2, "色情"),
	POLITIC(3, "政治"),
	VIOLENCE(4, "暴恐"),

	SCAM(7, "诈骗"),
	GAMBLE(8, "赌博"),
	VULGAR(9, "低俗"),
	DRM(10, "侵权"),

	ETC(6, "其它");


	private final int symbol;
	private final String title;

	private ForumReportTypeEnum(int symbol, String title) {
		this.symbol = symbol;
		this.title = title;
	}

	@Override
	public int getSymbol() {
		return symbol;
	}

	@Override
	public String getTitle() {
		return title;
	}
}
