package com.apobates.forum.core.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
/**
 * 版块关联话题类型后的中间表
 * 
 * @author xiaofanku
 * @since 20190910
 */
@Entity
@Table(name = "apo_board_topic_category_index", uniqueConstraints={@UniqueConstraint(columnNames={"volumesId", "boardId","categoryId"})})
public class BoardTopicCategoryIndex implements Serializable{
	private static final long serialVersionUID = 4704482572456585419L;
	//卷的ID
	@Id
	private int volumesId;
	//若关联到大版块boardId=0, 大版块下的所有版块都可以用
	@Id
	private long boardId;
	@Id
	private int categoryId;
	
	//empty constructor for JPA instantiation
	public BoardTopicCategoryIndex() {}
	
	public BoardTopicCategoryIndex(int volumesId, long boardId, int categoryId) {
		this.volumesId = volumesId;
		this.boardId = boardId;
		this.categoryId = categoryId;
	}
	
	public long getBoardId() {
		return boardId;
	}
	
	public void setBoardId(long boardId) {
		this.boardId = boardId;
	}
	
	public int getCategoryId() {
		return categoryId;
	}
	
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	
	public int getVolumesId() {
		return volumesId;
	}

	public void setVolumesId(int volumesId) {
		this.volumesId = volumesId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (boardId ^ (boardId >>> 32));
		result = prime * result + categoryId;
		result = prime * result + volumesId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BoardTopicCategoryIndex other = (BoardTopicCategoryIndex) obj;
		if (boardId != other.boardId)
			return false;
		if (categoryId != other.categoryId)
			return false;
		if (volumesId != other.volumesId)
			return false;
		return true;
	}

}
