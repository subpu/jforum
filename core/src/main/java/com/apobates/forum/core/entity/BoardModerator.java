package com.apobates.forum.core.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import com.apobates.forum.event.elderly.ForumActionEnum;
/**
 * 版块的管理员(班猪)
 * 
 * @author xiaofanku
 * @since 20190303
 */
@Entity
@Table(name = "apo_board_moderator", uniqueConstraints={@UniqueConstraint(columnNames={"volumesId", "boardId", "memberId", "volumesMaster"})})
public class BoardModerator implements Serializable{
	private static final long serialVersionUID = 2207510275870216006L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	//卷
	private int volumesId;
	//版块|大版主的版块是: 0
	private long boardId;
	//等级
    @Basic
    @Enumerated(EnumType.STRING)
	private ModeratorLevelEnum level;
	//会员ID
	private long memberId;
	//会员的登录帐号
	private String memberNickname;
	//任命日期
	private LocalDateTime takeDateTime;
	//状态
	private boolean status;
	//是否是大版主,管辖所有版块
	private boolean volumesMaster;
	//版主权限容器
	@Transient
	private List<ForumActionEnum> actions;
	@Transient
	private BoardGroup volumes;
	@Transient
	private Board board;
	
	//empty constructor for JPA instantiation
	public BoardModerator() {}
	public BoardModerator(
			int volumesId, 
			long boardId, 
			ModeratorLevelEnum level, 
			long memberId, 
			String memberNickname) {
		this.volumesId = volumesId; 
		this.boardId = boardId;
		this.level = level;
		this.memberId = memberId;
		this.memberNickname=memberNickname;
		this.takeDateTime = LocalDateTime.now();
		this.status = true;
		this.volumesMaster=false;
	}
	
	public BoardModerator(
			int volumesId, 
			long boardId, 
			ModeratorLevelEnum level, 
			long memberId, 
			String memberNickname, 
			boolean volumesMaster) {
		this.volumesId = volumesId; 
		this.boardId = boardId;
		this.level = level;
		this.memberId = memberId;
		this.memberNickname=memberNickname;
		this.takeDateTime = LocalDateTime.now();
		this.status = true;
		this.volumesMaster=volumesMaster;
	}
	
	public long getId() {
		return id;
	}

	public int getVolumesId() {
		return volumesId;
	}

	public long getBoardId() {
		return boardId;
	}

	public ModeratorLevelEnum getLevel() {
		return level;
	}

	public long getMemberId() {
		return memberId;
	}

	public LocalDateTime getTakeDateTime() {
		return takeDateTime;
	}

	public boolean isStatus() {
		return status;
	}
	public List<ForumActionEnum> getActions() {
		return actions;
	}
	public String getMemberNickname() {
		return memberNickname;
	}
	public boolean isVolumesMaster() {
		return volumesMaster;
	}
	public void setVolumesMaster(boolean volumesMaster) {
		this.volumesMaster = volumesMaster;
	}
	//--------------------------------------------SET
	public void setId(long id) {
		this.id = id;
	}
	public void setVolumesId(int volumesId) {
		this.volumesId = volumesId;
	}
	public void setBoardId(long boardId) {
		this.boardId = boardId;
	}
	public void setLevel(ModeratorLevelEnum level) {
		this.level = level;
	}
	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}
	public void setTakeDateTime(LocalDateTime takeDateTime) {
		this.takeDateTime = takeDateTime;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public void setActions(List<ForumActionEnum> actions) {
		this.actions = actions;
	}
	public void setMemberNickname(String memberNickname) {
		this.memberNickname = memberNickname;
	}
	@Transient
	public static BoardModerator empty() {
		return new BoardModerator(0, 0L, ModeratorLevelEnum.INTERN, 0L, ""); 
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (boardId ^ (boardId >>> 32));
		result = prime * result + (int) (memberId ^ (memberId >>> 32));
		result = prime * result + volumesId;
		result = prime * result + (volumesMaster ? 1231 : 1237);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BoardModerator other = (BoardModerator) obj;
		if (boardId != other.boardId)
			return false;
		if (memberId != other.memberId)
			return false;
		if (volumesId != other.volumesId)
			return false;
		if (volumesMaster != other.volumesMaster)
			return false;
		return true;
	}
	public BoardGroup getVolumes() {
		return volumes;
	}
	public void setVolumes(BoardGroup volumes) {
		this.volumes = volumes;
	}
	public Board getBoard() {
		return board;
	}
	public void setBoard(Board board) {
		this.board = board;
	}
	
	@Transient
	public String getAssumeBoards(){
		if(isVolumesMaster()){ //是大版主吗
			return String.format("%s大版主", (null == getVolumes())?"":getVolumes().getTitle());
		}
		return String.format("%s版主", (null == getBoard())?"":getBoard().getTitle());
	}
}
