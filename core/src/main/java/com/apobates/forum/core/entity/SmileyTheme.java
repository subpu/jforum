package com.apobates.forum.core.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
/**
 * 表情风格
 * 
 * @author xiaofanku
 * @since 20190529
 */
@Entity
@Table(name = "apo_smiley_theme", uniqueConstraints={@UniqueConstraint(columnNames={"directNames"})})
public class SmileyTheme implements Serializable,Comparable<SmileyTheme>{
	private static final long serialVersionUID = 9093693730321710203L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	//中文名称,例:QQ表情
	private String title;
	//英文名称,例:qq
	private String label;
	//目录名称(相对于表情根目录),例:qq
	private String directNames;
	//状态
	private boolean status;
	//显示顺序:普通话题是1,置顶的依次++
	private int ranking;
	//创建日期
	private LocalDateTime entryDateTime;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getDirectNames() {
		return directNames;
	}
	public void setDirectNames(String directNames) {
		this.directNames = directNames;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public int getRanking() {
		return ranking;
	}
	public void setRanking(int ranking) {
		this.ranking = ranking;
	}
	public LocalDateTime getEntryDateTime() {
		return entryDateTime;
	}
	public void setEntryDateTime(LocalDateTime entryDateTime) {
		this.entryDateTime = entryDateTime;
	}
	//empty constructor for JPA instantiation
	public SmileyTheme() {
		super();
	}
	
	public SmileyTheme(String title, String label, String directNames, boolean status, int ranking) {
		super();
		this.title = title;
		this.label = label;
		this.directNames = directNames;
		this.status = status;
		this.ranking = ranking;
		this.entryDateTime=LocalDateTime.now();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((directNames == null) ? 0 : directNames.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SmileyTheme other = (SmileyTheme) obj;
		if (directNames == null) {
			if (other.directNames != null)
				return false;
		} else if (!directNames.equals(other.directNames))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(SmileyTheme o) {
		return ranking - o.getRanking();
	}
	@Transient
	public static SmileyTheme empty(int id) {
		SmileyTheme st = new SmileyTheme("NUL", "-", "-", true, 1);
		st.setId(id);
		return st;
	}
}
