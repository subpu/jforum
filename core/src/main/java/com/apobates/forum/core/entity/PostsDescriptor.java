package com.apobates.forum.core.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * 回复的附加信息
 * 
 * @author xiaofanku
 * @since 20191112
 */
@Entity
@Table(name = "apo_topic_posts_dictum")
public class PostsDescriptor implements Serializable{
	private static final long serialVersionUID = -4407691068279885922L;
	//会员ID
	private long memberId;
	//登录帐号
	private String memberNames;
	//会员昵称
	private String memberNickname;
	//回复ID
	@Id
	private long postsId;
	//使用的ip
	private String ipAddr;
	//设备
	private String device;
	//
	private String agent;
	//供应商:联通|电信
	private String isp;
	//地域:省
	private String province;
	//地域:市
	private String city;
	//地域:区
	private String district;
	
	//empty constructor for JPA instantiation
	public PostsDescriptor() {}
	
	public long getMemberId() {
		return memberId;
	}
	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}
	public String getMemberNames() {
		return memberNames;
	}
	public void setMemberNames(String memberNames) {
		this.memberNames = memberNames;
	}
	public String getMemberNickname() {
		return memberNickname;
	}
	public void setMemberNickname(String memberNickname) {
		this.memberNickname = memberNickname;
	}
	public long getPostsId() {
		return postsId;
	}
	public void setPostsId(long postsId) {
		this.postsId = postsId;
	}
	public String getIpAddr() {
		return ipAddr;
	}
	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getAgent() {
		return agent;
	}
	public void setAgent(String agent) {
		this.agent = agent;
	}
	public String getIsp() {
		return isp;
	}
	public void setIsp(String isp) {
		this.isp = isp;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
}
