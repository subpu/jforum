package com.apobates.forum.core;

import java.io.Serializable;
import com.apobates.forum.core.entity.Topic;
import com.apobates.forum.utils.DateTimeUtils;
/**
 * 抽像的话题[Topic.VO.1]
 * 
 * @author xiaofanku
 * @since 20190403
 */
public class CommonTopic implements Serializable {
	private static final long serialVersionUID = -7653993604999486750L;
	//话题主题
	private final String title;
	//话题的连接
	private final String link;
	//话题的作者ID
	private final long author;
	//话题的作者
	private final String authorNames;
	//话题的发布日期
	private final String date;
	//版块的连接
	private final String boardLink;
	//话题所在版块名称
	private final String boardTitle;
	//话题内容简述
	private final String description;

	public CommonTopic(Topic topic) {
		super();
		this.title = topic.getTitle();
		this.link = "/topic/"+topic.getConnect()+".xhtml";
		this.author = topic.getMemberId();
		this.authorNames = topic.getMemberNickname();
		this.date = DateTimeUtils.formatClock(topic.getEntryDateTime());
		this.boardLink = "/board/"+topic.getBoard().getConnect()+".xhtml";
		this.boardTitle = topic.getBoard().getTitle();
		this.description = topic.getSummary();
	}

	public String getTitle() {
		return title;
	}

	public long getAuthor() {
		return author;
	}

	public String getAuthorNames() {
		return authorNames;
	}

	public String getDate() {
		return date;
	}

	public String getBoardTitle() {
		return boardTitle;
	}

	public String getDescription() {
		return description;
	}

	public String getLink() {
		return link;
	}

	public String getBoardLink() {
		return boardLink;
	}

}
