package com.apobates.forum.core.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
/**
 * 版块与轮播图关联
 * 
 * @author xiaofanku
 * @since 20191109
 */
@Entity
@Table(name = "apo_topic_carousel_board_index", uniqueConstraints={@UniqueConstraint(columnNames={"volumesId", "boardId","carouselId"})})
public class BoardCarouselIndex implements Serializable{
	private static final long serialVersionUID = -3704975653505624938L;
	@Id
	private int carouselId;
	//卷的ID
	@Id
	private int volumesId;
	//版块
	@Id
	private long boardId;
	//发布日期
	private LocalDateTime entryDateTime;
	//到期的日期
	private LocalDateTime expireDateTime;
	@Transient
	private Board board;
	@Transient
	private BoardGroup volumes;
	
	public int getCarouselId() {
		return carouselId;
	}
	public void setCarouselId(int carouselId) {
		this.carouselId = carouselId;
	}
	public int getVolumesId() {
		return volumesId;
	}
	public void setVolumesId(int volumesId) {
		this.volumesId = volumesId;
	}
	public long getBoardId() {
		return boardId;
	}
	public void setBoardId(long boardId) {
		this.boardId = boardId;
	}
	public LocalDateTime getEntryDateTime() {
		return entryDateTime;
	}
	public void setEntryDateTime(LocalDateTime entryDateTime) {
		this.entryDateTime = entryDateTime;
	}
	public LocalDateTime getExpireDateTime() {
		return expireDateTime;
	}
	public void setExpireDateTime(LocalDateTime expireDateTime) {
		this.expireDateTime = expireDateTime;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (boardId ^ (boardId >>> 32));
		result = prime * result + carouselId;
		result = prime * result + volumesId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BoardCarouselIndex other = (BoardCarouselIndex) obj;
		if (boardId != other.boardId)
			return false;
		if (carouselId != other.carouselId)
			return false;
		if (volumesId != other.volumesId)
			return false;
		return true;
	}
	public Board getBoard() {
		return board;
	}
	public void setBoard(Board board) {
		this.board = board;
	}
	public BoardGroup getVolumes() {
		return volumes;
	}
	public void setVolumes(BoardGroup volumes) {
		this.volumes = volumes;
	}
}
