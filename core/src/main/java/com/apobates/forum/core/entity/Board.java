package com.apobates.forum.core.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.regex.PatternSyntaxException;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import com.apobates.forum.core.ad.ActiveDirectoryConnector;
import com.apobates.forum.utils.Commons;
/**
 * 版块
 * 
 * @author xiaofanku
 * @since 20190301
 */
@Entity
@Table(name = "apo_board", uniqueConstraints={@UniqueConstraint(columnNames={"title","volumesId"})})
public class Board implements Serializable, Comparable<Board>, ActiveDirectoryConnector<Board>{
	private static final long serialVersionUID = 52825921502430557L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	//名称
	private String title;
	//描述
	private String description;
	//创建日期
	private LocalDateTime entryDateTime;
	//图标
	private String imageAddr;
	//卷的ID
	private int volumesId=0;
	//状态
	@Basic
	@Enumerated(EnumType.STRING)
	private ForumEntityStatusEnum status;
	//显示顺序
	private int ranking;
	// 是否原生,多数情况都是站长创建的.默认是false
	// 原生不能在会员可以访问的地方出现.只供系统使用
	private boolean origin;
	// 配合原生一起使用的目录名
	private String directoryNames;
	//版主容器
	@Transient
	private List<BoardModerator> moderatores;//策略模式需要
	@Transient
	private BoardConfig configure; //策略模式需要
	@Transient
	private BoardStats stats; 
	@Transient
	private BoardGroup volumes;
	
	//empty constructor for JPA instantiation
	public Board() {super();}
	//从前端来的
	public Board(String title, String description, String imageAddr, int volumesId, ForumEntityStatusEnum status,int ranking) {
		this.id = 0L;
		this.title = title;
		this.description = description;
		this.entryDateTime = LocalDateTime.now();
		this.imageAddr = imageAddr;
		this.volumesId = volumesId;
		this.status = status;
		this.ranking = ranking;
		this.origin = false;
		this.directoryNames = null;
	}
	public LocalDateTime getEntryDateTime() {
		return entryDateTime;
	}

	public long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}
	
	public String getImageAddr() {
		return imageAddr;
	}
	public ForumEntityStatusEnum getStatus() {
		return status;
	}

	public int getRanking() {
		return ranking;
	}

	public int getVolumesId() {
		return volumesId;
	}
	public BoardGroup getVolumes() {
		return volumes;
	}
	public BoardConfig getConfigure() {
		return configure;
	}
	public List<BoardModerator> getModeratores() {
		return moderatores;
	}
	public BoardStats getStats() {
		return stats;
	}
	//--------------------------------------------SET
	public void setId(long id) {
		this.id = id;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setEntryDateTime(LocalDateTime entryDateTime) {
		this.entryDateTime = entryDateTime;
	}
	public void setImageAddr(String imageAddr) {
		this.imageAddr = imageAddr;
	}
	public void setVolumesId(int volumesId) {
		this.volumesId = volumesId;
	}
	public void setStatus(ForumEntityStatusEnum status) {
		this.status = status;
	}
	public void setRanking(int ranking) {
		this.ranking = ranking;
	}
	public void setVolumes(BoardGroup volumes) {
		this.volumes = volumes;
	}
	public void setConfigure(BoardConfig configure) {
		this.configure = configure;
	}
	public void setModeratores(List<BoardModerator> moderatores) {
		this.moderatores = moderatores;
	}
	public void setStats(BoardStats stats) {
		this.stats = stats;
	}
	public boolean isOrigin() {
		return origin;
	}
	public void setOrigin(boolean origin) {
		this.origin = origin;
	}
	public String getDirectoryNames() {
		return directoryNames;
	}
	public void setDirectoryNames(String directoryNames) {
		this.directoryNames = directoryNames;
	}
	
	@Transient
	public static Board empty(long boardId) {
		Board board = new Board("NUL", "Do Not Allow blank", "image://defat/ico/default_icon.png", 0, ForumEntityStatusEnum.ACTIVE, 1);
		board.setId(boardId);
		return board;
	}
	
	@Transient
	public boolean isNormal() {
		return ForumEntityStatusEnum.ACTIVE == status;
	}
	
	@Transient
	public String getIcoStatus() {
		if(ForumEntityStatusEnum.LOCKED == getStatus()) {
			return "locked"; //锁定
		}
		if(ForumEntityStatusEnum.DELETE == getStatus()) {
			return "read"; //只读
		}
		return "normal"; //正常
	}
	
	@Override
	public int compareTo(Board o) {
		return ranking - o.getRanking();
	}
	
	@Override
	public String getConnect() {
		return getId()+"-"+getVolumesId();
	}
	
	@Override
	public Optional<Board> toConnector(String connectValue) throws IllegalStateException {
		String[] params = {};
		try{
			params = connectValue.split("-");
		}catch(NullPointerException | PatternSyntaxException e){
			throw new IllegalStateException("构造版块的ActiveDirectoryConnector实例参数不合法");
		}
		if(params.length == 0){
			throw new IllegalStateException("构造版块的ActiveDirectoryConnector实例失败");
		}
		//话题: x-x-x
		//版块: x-x
		//版块组: x
		if(params.length == 1){
			Board board = new Board();
			board.setVolumesId(Commons.stringToInteger(params[0], -1));
			return Optional.of(board);
		}
		if(params.length == 2){
			Board board = new Board();
			board.setId(Commons.stringToLong(params[0], 0L));
			board.setVolumesId(Commons.stringToInteger(params[1], -1));
			return Optional.of(board);
		}
		if(params.length == 3){
			Board board = new Board();
			board.setId(Commons.stringToLong(params[1], 0L));
			board.setVolumesId(Commons.stringToInteger(params[2], -1));
			return Optional.of(board);
		}
		return Optional.empty();
	}
}
