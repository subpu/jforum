package com.apobates.forum.core.plug;

import com.apobates.forum.event.elderly.ActionEventCulpritor;
import com.apobates.forum.event.elderly.ForumActionEnum;

/**
 * 插件话题的抽像基类
 * 对反馈和话题(,回复)投拆的抽像
 * @author xiaofanku
 * @since 20200530
 */
public abstract class AbstractPlugTopic implements PlugTopic{
    private final ActionEventCulpritor culpritor;
    private final ForumActionEnum action;
    
    /**
     *
     * @param action 操作动作
     * @param culpritor 操作的肇事信息
     */
    public AbstractPlugTopic(ForumActionEnum action, ActionEventCulpritor culpritor) {
        super();
        this.action = action;
        this.culpritor = culpritor;
    }
    
    public ActionEventCulpritor getCulpritor() {
        return culpritor;
    }
    
    public ForumActionEnum getAction() {
        return action;
    }
    
    @Override
    public boolean isSuggest() {
        return false;
    }
}
