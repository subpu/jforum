package com.apobates.forum.core.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.apobates.forum.event.elderly.ForumActionEnum;
/**
 * 版主的操作权限
 * 
 * @author xiaofanku
 * @since 20190316
 */
@Entity
@Table(name = "apo_board_moderator_action_index", uniqueConstraints={@UniqueConstraint(columnNames={"moderatorId","action"})})
public class BoardModeratorActionIndex implements Serializable{
	private static final long serialVersionUID = 8661064108317743410L;
	@Id
	private long moderatorId;
	@Id
	@Basic
	@Enumerated(EnumType.STRING)
	private ForumActionEnum action;
	
	//empty constructor for JPA instantiation
	public BoardModeratorActionIndex() {}
	
	public BoardModeratorActionIndex(long moderatorId, ForumActionEnum action) {
		super();
		this.moderatorId = moderatorId;
		this.action = action;
	}
	
	public long getModeratorId() {
		return moderatorId;
	}
	
	public void setModeratorId(long moderatorId) {
		this.moderatorId = moderatorId;
	}
	
	public ForumActionEnum getAction() {
		return action;
	}
	
	public void setAction(ForumActionEnum action) {
		this.action = action;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((action == null) ? 0 : action.hashCode());
		result = prime * result + (int) (moderatorId ^ (moderatorId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BoardModeratorActionIndex other = (BoardModeratorActionIndex) obj;
		if (action != other.action)
			return false;
		if (moderatorId != other.moderatorId)
			return false;
		return true;
	}
}
