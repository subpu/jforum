package com.apobates.forum.core.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.PatternSyntaxException;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import com.apobates.forum.core.ad.ActiveDirectoryConnector;
/**
 * 版块组(卷), 版块的一种分组方式
 * 
 * @author xiaofanku
 * @since 20190301
 */
@Entity
@Table(name = "apo_board_group", uniqueConstraints = { @UniqueConstraint(columnNames = { "title" }) })
public class BoardGroup implements Serializable, Comparable<BoardGroup>, ActiveDirectoryConnector<BoardGroup> {
	private static final long serialVersionUID = 1687800278070662479L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	//名称
	private String title;
	//描述
	private String description;
	//创建日期
	private LocalDateTime entryDateTime;
	//图标
	private String imageAddr;
	//显示顺序
	private int ranking;
	// 状态
	private boolean status;
	// 是否原生,多数情况都是站长创建的.默认是false
	// 原生不能在会员可以访问的地方出现.只供系统使用
	private boolean origin;
	// 配合原生一起使用的目录名
	private String directoryNames;
	//组下的版块
	@Transient
	private TreeSet<Board> boardes = new TreeSet<>();
	
	// empty constructor for JPA instantiation
	public BoardGroup() {
		super();
	}
	//从前端来的
	public BoardGroup(String title, String description, String imageAddr, boolean status, int ranking) {
		this.id = 0;
		this.title = title;
		this.description = description;
		this.entryDateTime = LocalDateTime.now();
		this.imageAddr = imageAddr;
		this.status = status;
		this.ranking = ranking;
		this.origin = false;
		this.directoryNames = null;
	}
	public LocalDateTime getEntryDateTime() {
		return entryDateTime;
	}

	public int getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}
	
	public String getImageAddr() {
		return imageAddr;
	}
	public boolean isStatus() {
		return status;
	}
	@Transient
	public boolean isNormal() {
		return status;
	}

	public int getRanking() {
		return ranking;
	}
	// --------------------------------------------SET
	public void setId(int id) {
		this.id = id;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setEntryDateTime(LocalDateTime entryDateTime) {
		this.entryDateTime = entryDateTime;
	}
	public void setImageAddr(String imageAddr) {
		this.imageAddr = imageAddr;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public Set<Board> getBoardes() {
		return boardes;
	}
	public void setBoardes(TreeSet<Board> boardes) {
		this.boardes = boardes;
	}
	public void setRanking(int ranking) {
		this.ranking = ranking;
	}
	public boolean isOrigin() {
		return origin;
	}
	public void setOrigin(boolean origin) {
		this.origin = origin;
	}
	public String getDirectoryNames() {
		return directoryNames;
	}
	public void setDirectoryNames(String directoryNames) {
		this.directoryNames = directoryNames;
	}
	//空版块组(卷),用于创建
	@Transient
	public static BoardGroup empty() {
		return new BoardGroup("", "Do Not Allow blank", "image://defat/ico/blank_icon.png", true, 1);
	}
	@Transient
	public static BoardGroup empty(int id) {
		BoardGroup bg = new BoardGroup("版块组/卷", "Do Not Allow blank", "image://defat/ico/blank_icon.png", true, 1);
		bg.setId(id);
		if(id==0){
			bg.setTitle("默认版块组");
		}
		return bg;
	}
	//默认版块组(卷),用于分组
	@Transient
	public static BoardGroup getDefault() {
		return getDefault(0);
	}

	@Transient
	public static BoardGroup getDefault(int ranking) { 
		BoardGroup bg = new BoardGroup("默认版块组", "no description", null, true, ranking);
		return bg;
	}
	@Override
	public int compareTo(BoardGroup o) {
		return ranking - o.getRanking();
	}
	
	@Override
	public String getConnect() {
		return getId()+"";
	}
	
	@Override
	public Optional<BoardGroup> toConnector(String connectValue) throws IllegalStateException {
		String[] params = {};
		try{
			params = connectValue.split("-");
		}catch(NullPointerException | PatternSyntaxException e){
			throw new IllegalStateException("构造版块组的ActiveDirectoryConnector实例参数不合法");
		}
		if(params.length != 1){
			throw new IllegalStateException("构造版块组的ActiveDirectoryConnector实例失败");
		}
		try{
			return Optional.of(BoardGroup.empty(Integer.valueOf(params[0])));
		}catch(java.lang.NumberFormatException e){
			return Optional.empty();
		}
	}
	
}
