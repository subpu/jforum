package com.apobates.forum.core.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.apobates.forum.member.entity.Member;
/**
 * 回复
 * 
 * @author xiaofanku
 * @since 20190301
 */
@Entity
@Table(name = "apo_topic_posts")
public class Posts implements Serializable{
	private static final long serialVersionUID = 1850170862308478672L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	//内容
	@Lob
	@Column(columnDefinition="TEXT") 
	private String content;
	//回复日期
	private LocalDateTime entryDateTime;
	//作者:会员的ID
	private long memberId;
	private String memberNickname;
	private String ipAddr;
	//话题的ID
	private long topicId;
	//卷的ID
	private int volumesId;
	//版块的ID
	private long boardId;
	//楼层|ranking
	private long floorNumber;
	//ETC
	//是否是回复,true表示回复,false表示是楼主贴子的内容
	private boolean reply;
	//状态
	private boolean status;
	//编辑日期
	private LocalDateTime modifyDateTime;
	private long modifyMemberId;
	private String modifyMemberNickname;
	//@Transient
	//private PostsAlbum album;
	@Transient
	private Topic topic;
	@Transient
	private Board board; //策略需要+版主+配置文件
	@Transient
	private BoardGroup volumes;
	@Transient
	private Member member;
	@Transient
	private PostsDescriptor descriptor;
	
	//empty constructor for JPA instantiation
	public Posts() {super();}
	//话题的第一条回复
	public Posts(
			String content, 
			long memberId, 
			String memberNickname, 
			long topicId,
			int volumesId, 
			long boardId, 
			String ipAddr, 
			boolean reply) {
		this.id = 0L;
		this.content = content;
		this.entryDateTime = LocalDateTime.now();
		this.memberId = memberId;
		this.memberNickname = memberNickname;
		this.ipAddr = ipAddr;
		this.topicId = topicId;
		this.volumesId = volumesId;
		this.boardId = boardId;
		this.reply = reply;
		this.status = true;
		//
		this.modifyDateTime = null;
		this.modifyMemberId = 0;
		this.modifyMemberNickname = null;
		this.floorNumber = 1;
	}
	//回复话题的贴子
	public Posts(
			String content, 
			long memberId, 
			String memberNickname, 
			long topicId,
			int volumesId, 
			long boardId,
			long floorNumber, 
			String ipAddr) {
		this.content = content;
		this.entryDateTime = LocalDateTime.now();
		this.memberId = memberId;
		this.memberNickname = memberNickname;
		this.ipAddr = ipAddr;
		this.topicId = topicId;
		this.volumesId = volumesId;
		this.boardId = boardId;
		this.floorNumber = floorNumber;
		this.reply = true;
		this.status = true;
		//
		this.modifyDateTime = null;
		this.modifyMemberId = 0;
		this.modifyMemberNickname = null;
	}
	
	public long getTopicId() {
		return topicId;
	}
	public long getBoardId() {
		return boardId;
	}
	public boolean isStatus() {
		return status;
	}
	public int getVolumesId() {
		return volumesId;
	}
	public long getId() {
		return id;
	}
	public String getContent() {
		return content;
	}
	public LocalDateTime getEntryDateTime() {
		return entryDateTime;
	}
	public long getMemberId() {
		return memberId;
	}
	public String getIpAddr() {
		return ipAddr;
	}
	public String getMemberNickname() {
		return memberNickname;
	}
	public void setMemberNickname(String memberNickname) {
		this.memberNickname = memberNickname;
	}
	public String getModifyMemberNickname() {
		return modifyMemberNickname;
	}
	public void setModifyMemberNickname(String modifyMemberNickname) {
		this.modifyMemberNickname = modifyMemberNickname;
	}
	public boolean isReply() {
		return reply;
	}
	public LocalDateTime getModifyDateTime() {
		return modifyDateTime;
	}
	public long getModifyMemberId() {
		return modifyMemberId;
	}
	public Topic getTopic() {
		return topic;
	}
	public Board getBoard() {
		return board;
	}
	public BoardGroup getVolumes() {
		return volumes;
	}
	//--------------------------------------------SET
	public void setId(long id) {
		this.id = id;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public void setEntryDateTime(LocalDateTime entryDateTime) {
		this.entryDateTime = entryDateTime;
	}
	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}
	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}
	public void setTopicId(long topicId) {
		this.topicId = topicId;
	}
	public void setVolumesId(int volumesId) {
		this.volumesId = volumesId;
	}
	public void setBoardId(long boardId) {
		this.boardId = boardId;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public void setModifyDateTime(LocalDateTime modifyDateTime) {
		this.modifyDateTime = modifyDateTime;
	}
	public void setModifyMemberId(long modifyMemberId) {
		this.modifyMemberId = modifyMemberId;
	}
	public void setReply(boolean reply) {
		this.reply = reply;
	}
	public void setTopic(Topic topic) {
		this.topic = topic;
	}
	public void setBoard(Board board) {
		this.board = board;
	}
	public void setVolumes(BoardGroup volumes) {
		this.volumes = volumes;
	}
	public Member getMember() {
		return member;
	}
	public void setMember(Member member) {
		this.member = member;
	}
	public long getFloorNumber() {
		return floorNumber;
	}
	public void setFloorNumber(long floorNumber) {
		this.floorNumber = floorNumber;
	}
	public PostsDescriptor getDescriptor() {
		return descriptor;
	}
	public void setDescriptor(PostsDescriptor descriptor) {
		this.descriptor = descriptor;
	}
	@Transient
	public boolean isNormal() {
		// TODO Auto-generated method stub
		return status;
	}
	
	@Transient
	public static Posts empty(String content){
		Posts p = new Posts();
		p.setContent(content);
		return p;
	}
	
	@Transient
	public static Posts empty(long id, String content){
		Posts p = new Posts();
		p.setId(id);
		p.setContent(content);
		return p;
	}
	
	@Transient
	public Topic getLazyTopic(){
		if(getTopic()!=null){
			return getTopic();
		}
		Topic t = new Topic();
		t.setId(getTopicId());
		t.setBoardId(getBoardId());
		t.setVolumesId(getVolumesId());
		return t;
	}
}
