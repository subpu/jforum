package com.apobates.forum.core;

import java.io.Serializable;
import org.apache.commons.io.FilenameUtils;
/**
 * 表情图片的注释
 * 
 * @author xiaofanku
 * @since 20191118
 */
public class SmileyPictureNotes implements Serializable{
	private static final long serialVersionUID = -8977517358968456202L;
	private final String url;
	private final String fileName;
	private final String note;
	
	public SmileyPictureNotes(String url, String fileName, String note) {
		super();
		this.url = url;
		this.fileName = fileName;
		this.note = note;
	}

	public SmileyPictureNotes(String url) {
		this.url = url;
		this.fileName = FilenameUtils.getName(url);
		this.note = "VA";
	}
	
	public String getUrl() {
		return url;
	}

	public String getFileName() {
		return fileName;
	}

	public String getNote() {
		return note;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SmileyPictureNotes other = (SmileyPictureNotes) obj;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}
}
