package com.apobates.forum.core.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * 话题标签搜索历史
 * 
 * @author xiaofanku
 * @since 20200115
 */
@Entity
@Table(name = "apo_topic_tag_history")
public class TopicTagSHistory implements Serializable{
	private static final long serialVersionUID = -4463575202086340953L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	//搜索的词
	private String word;
	//结果的数量
	private long affect;
	//作者:会员的ID
	private long memberId;
	private String memberNickname;
	//日期
	//发布日期
	private LocalDateTime entryDateTime;
	
	//empty constructor for JPA instantiation
	public TopicTagSHistory() {
		super();
	}
	public TopicTagSHistory(String word, long affect, long memberId, String memberNickname){
		this.word = word;
		this.affect = affect;
		this.memberId = memberId;
		this.memberNickname = memberNickname;
		this.entryDateTime = LocalDateTime.now();
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public long getAffect() {
		return affect;
	}
	public void setAffect(long affect) {
		this.affect = affect;
	}
	public long getMemberId() {
		return memberId;
	}
	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}
	public String getMemberNickname() {
		return memberNickname;
	}
	public void setMemberNickname(String memberNickname) {
		this.memberNickname = memberNickname;
	}
	public LocalDateTime getEntryDateTime() {
		return entryDateTime;
	}
	public void setEntryDateTime(LocalDateTime entryDateTime) {
		this.entryDateTime = entryDateTime;
	}
}
