package com.apobates.forum.core.entity;

import com.apobates.forum.utils.lang.EnumArchitecture;

public enum ModeratorLevelEnum implements EnumArchitecture{
	INTERN(0, "实习"),
	FORMAL(1, "正式"),
	SUPER(2, "超级"),
	HONOR(3, "荣誉"),
	ELDER(4, "长老");
	
	private final int symbol;
	private final String title;
	
	private ModeratorLevelEnum(int symbol, String title) {
		this.symbol = symbol;
		this.title = title;
	}
	
	@Override
	public int getSymbol() {
		return symbol;
	}
	
	@Override
	public String getTitle() {
		return title;
	}
	
}
