package com.apobates.forum.core.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.apobates.forum.member.entity.MemberRoleEnum;
/**
 * 版主的角色变化历史
 * 
 * @author xiaofanku
 * @since 20190627
 */
@Entity
@Table(name = "apo_board_moderator_role_history")
public class BoardModeratorRoleHistory  implements Serializable{
	private static final long serialVersionUID = -1238566487288767584L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	// 变化前的角色
	@Basic
	@Enumerated(EnumType.STRING)
	private MemberRoleEnum originalRole;
	// 赋予的角色
	@Basic
	@Enumerated(EnumType.STRING)
	private MemberRoleEnum investRole;
	// 卷
	private int volumesId;
	// 版块
	private long boardId;
	// 会员ID
	private long memberId;
	// 日期
	private LocalDateTime entryDateTime;
	//版主ID
	private long moderatorId;
	// 状态
	private boolean status;
	
	//empty constructor for JPA instantiation
	public BoardModeratorRoleHistory() {}
	//保存版主
	public BoardModeratorRoleHistory(long moderatorId, MemberRoleEnum originalRole, MemberRoleEnum investRole, int volumesId, long boardId, long memberId){
		this.id=0;
		this.moderatorId = moderatorId;
		this.originalRole = originalRole;
		this.investRole = investRole;
		this.volumesId = volumesId;
		this.boardId = boardId;
		this.memberId = memberId;
		this.entryDateTime = LocalDateTime.now();
		this.status = true;
	}
	//保存大版主
	public BoardModeratorRoleHistory(long moderatorId, MemberRoleEnum originalRole, MemberRoleEnum investRole, int volumesId, long memberId){
		this.id=0;
		this.moderatorId = moderatorId;
		this.originalRole = originalRole;
		this.investRole = investRole;
		this.volumesId = volumesId;
		this.boardId = 0;
		this.memberId = memberId;
		this.entryDateTime = LocalDateTime.now();
		this.status = true;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public MemberRoleEnum getOriginalRole() {
		return originalRole;
	}
	public void setOriginalRole(MemberRoleEnum originalRole) {
		this.originalRole = originalRole;
	}
	public MemberRoleEnum getInvestRole() {
		return investRole;
	}
	public void setInvestRole(MemberRoleEnum investRole) {
		this.investRole = investRole;
	}
	public int getVolumesId() {
		return volumesId;
	}
	public void setVolumesId(int volumesId) {
		this.volumesId = volumesId;
	}
	public long getBoardId() {
		return boardId;
	}
	public void setBoardId(long boardId) {
		this.boardId = boardId;
	}
	public long getMemberId() {
		return memberId;
	}
	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}
	public LocalDateTime getEntryDateTime() {
		return entryDateTime;
	}
	public void setEntryDateTime(LocalDateTime entryDateTime) {
		this.entryDateTime = entryDateTime;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public long getModeratorId() {
		return moderatorId;
	}
	public void setModeratorId(long moderatorId) {
		this.moderatorId = moderatorId;
	}
}
