package com.apobates.forum.core.entity;

import java.time.LocalDateTime;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import com.apobates.forum.event.elderly.ActionEventCulpritor;
import com.apobates.forum.event.elderly.ForumActionEnum;
/**
 * 话题和回复的操作集合
 * 
 * @author xiaofanku
 * @since 20190328
 */
@Entity
@DiscriminatorValue("T")
public class TopicActionCollection extends ActionEventDescriptor{
	private static final long serialVersionUID = -6298738156739185617L;
	//话题的标题
	private String topicTitle;
	//话题的ID
	private long topicId;
	//话题类操作时,postsId=0
	private long postsId;
	@Transient
	private Posts posts;
	@Transient
	private Topic topic;
	
	//empty constructor for JPA instantiation
	public TopicActionCollection() {super();}
	public TopicActionCollection(
			String memberNames, 
			long memberId, 
			ForumActionEnum action, 
			String topicTitle,
			long topicId, 
			long postsId,
			long victim, 
			String ipAddr, 
			String token) {
		super(memberNames, memberId, action, LocalDateTime.now(), ipAddr, token, true, victim);
		this.topicTitle = topicTitle;
		this.topicId = topicId;
		this.postsId = postsId;
	}
	public TopicActionCollection(
			ForumActionEnum action, 
			String topicTitle, 
			long topicId, 
			long postsId, 
			boolean processed, 
			long victim, 
			ActionEventCulpritor culpritor) {
		super(culpritor.getMemberNickname(), culpritor.getMemberId(), action, LocalDateTime.now(), culpritor.getIpAddr(), culpritor.getToken(), processed, victim);
		this.topicTitle = topicTitle;
		this.topicId = topicId;
		this.postsId = postsId;
	}

	public String getTopicTitle() {
		return topicTitle;
	}

	public void setTopicTitle(String topicTitle) {
		this.topicTitle = topicTitle;
	}

	public long getTopicId() {
		return topicId;
	}

	public void setTopicId(long topicId) {
		this.topicId = topicId;
	}

	public long getPostsId() {
		return postsId;
	}

	public void setPostsId(long postsId) {
		this.postsId = postsId;
	}
	
	public Posts getPosts() {
		return posts;
	}
	
	public void setPosts(Posts posts) {
		this.posts = posts;
	}
	
	public Topic getTopic() {
		return topic;
	}
	
	public void setTopic(Topic topic) {
		this.topic = topic;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (int) (postsId ^ (postsId >>> 32));
		result = prime * result + (int) (topicId ^ (topicId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TopicActionCollection other = (TopicActionCollection) obj;
		if (postsId != other.postsId) {
			return false;
		}
		return topicId == other.topicId;
	}
	
}
