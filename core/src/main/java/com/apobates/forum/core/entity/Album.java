package com.apobates.forum.core.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
/**
 * 像册
 * 
 * @author xiaofanku
 * @since 20190301
 */
@Entity
@Table(name = "apo_album", uniqueConstraints = { @UniqueConstraint(columnNames = {"topicId","memberId"})})
public class Album implements Serializable{
	private static final long serialVersionUID = 3507751881145040347L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private String title;
	//卷的ID
	private int volumesId;
	//版块的ID
	private long boardId;
	//话题的ID
	private long topicId;
	//回复的ID
	//private long postsId;
	//像册的封面连接地址
	private String coverLink;
	//发布日期
	private LocalDateTime entryDateTime;
	//作者:会员的ID
	private long memberId;
	private String memberNickname;
	//状态
	private boolean status;
	@OneToMany(mappedBy = "album", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
	private List<AlbumPicture> pictures=new ArrayList<>();
	//所属的版块
	@Transient
	private Board board;
	
	//empty constructor for JPA instantiation
	public Album() {super();}
	public Album(String title, int volumesId, long boardId, long topicId, long memberId, String memberNickname, List<AlbumPicture> pictures) {
		super();
		this.title = title;
		this.volumesId = volumesId;
		this.boardId = boardId;
		this.topicId = topicId;
		//this.postsId = postsId;
		this.memberId = memberId;
		this.memberNickname = memberNickname;
		this.pictures = pictures;
		//
		this.entryDateTime = LocalDateTime.now();
		this.status=true;
		AlbumPicture ap = pictures.get(0);
		ap.setCover(true);
		this.coverLink = ap.getLink();
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getVolumesId() {
		return volumesId;
	}
	public void setVolumesId(int volumesId) {
		this.volumesId = volumesId;
	}
	public long getBoardId() {
		return boardId;
	}
	public void setBoardId(long boardId) {
		this.boardId = boardId;
	}
	public long getTopicId() {
		return topicId;
	}
	public void setTopicId(long topicId) {
		this.topicId = topicId;
	}
	/*
	public long getPostsId() {
		return postsId;
	}
	public void setPostsId(long postsId) {
		this.postsId = postsId;
	}*/
	public String getCoverLink() {
		return coverLink;
	}
	public void setCoverLink(String coverLink) {
		this.coverLink = coverLink;
	}
	public LocalDateTime getEntryDateTime() {
		return entryDateTime;
	}
	public void setEntryDateTime(LocalDateTime entryDateTime) {
		this.entryDateTime = entryDateTime;
	}
	public long getMemberId() {
		return memberId;
	}
	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}
	public String getMemberNickname() {
		return memberNickname;
	}
	public void setMemberNickname(String memberNickname) {
		this.memberNickname = memberNickname;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (memberId ^ (memberId >>> 32));
		result = prime * result + (int) (topicId ^ (topicId >>> 32));
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Album other = (Album) obj;
		if (memberId != other.memberId)
			return false;
		if (topicId != other.topicId)
			return false;
		return true;
	}
	public List<AlbumPicture> getPictures() {
		return pictures;
	}
	public void setPictures(List<AlbumPicture> pictures) {
		this.pictures = pictures;
	}
	public Board getBoard() {
		return board;
	}
	public void setBoard(Board board) {
		this.board = board;
	}
	@Transient
	public static Optional<Album> createAlbum(List<AlbumPicture> pictures, Posts oneFloorPosts, long topicId, String topicTitle){
        if (pictures == null || pictures.isEmpty()) {
            return Optional.empty();
        }
        Album album = new Album(
                topicTitle,
                oneFloorPosts.getVolumesId(),
                oneFloorPosts.getBoardId(),
                topicId,
                oneFloorPosts.getMemberId(),
                oneFloorPosts.getMemberNickname(),
                pictures);
        for (AlbumPicture ap : pictures) {
            ap.setAlbum(album);
        }
        return Optional.of(album);
	}
	@Transient
	public Topic getTopic() {
		Topic topic = Topic.empty(getVolumesId(), getBoardId());
		topic.setTitle(getTitle());
		topic.setId(getTopicId());
		return topic;
	}
}
