package com.apobates.forum.core.entity;

import java.time.LocalDateTime;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import com.apobates.forum.event.elderly.ActionEventCulpritor;
import com.apobates.forum.event.elderly.ForumActionEnum;
/**
 * 版块的操作集合
 * 
 * @author xiaofanku
 * @since 20190329
 */
@Entity
@DiscriminatorValue("B")
public class BoardActionCollection extends ActionEventDescriptor{
	private static final long serialVersionUID = -5014714885324936267L;
	private long boardId;
	private String boardTitle;
	
	//empty constructor for JPA instantiation
	public BoardActionCollection() {super();}
	public BoardActionCollection(
			String memberNames, 
			long memberId, 
			ForumActionEnum action, 
			String boardTitle,
			long boardId, 
			String ipAddr, 
			String token, 
			long victim) {
		super(memberNames, memberId, action, LocalDateTime.now(), ipAddr, token, true, victim);
		this.boardTitle = boardTitle;
		this.boardId = boardId;
	}
	public BoardActionCollection(
			ForumActionEnum action, 
			String boardTitle, 
			long boardId, 
			boolean processed, 
			long victim, 
			ActionEventCulpritor culpritor) {
		super(culpritor.getMemberNickname(), culpritor.getMemberId(), action, LocalDateTime.now(), culpritor.getIpAddr(), culpritor.getToken(), processed, victim);
		this.boardTitle = boardTitle;
		this.boardId = boardId;
	}
	
	public long getBoardId() {
		return boardId;
	}
	public void setBoardId(long boardId) {
		this.boardId = boardId;
	}
	public String getBoardTitle() {
		return boardTitle;
	}
	public void setBoardTitle(String boardTitle) {
		this.boardTitle = boardTitle;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (int) (boardId ^ (boardId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BoardActionCollection other = (BoardActionCollection) obj;
		return boardId == other.boardId;
	}
	
}
