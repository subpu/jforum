package com.apobates.forum.core.entity;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
/**
 * 轮播图
 * 
 * @author xiaofanku
 * @since 20191012
 */
@Entity
@Table(name = "apo_topic_carousel", uniqueConstraints = { @UniqueConstraint(columnNames = {"title"})})
public class TopicCarousel implements Serializable{
	private static final long serialVersionUID = -1581434329680631553L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	//建议用英文
	private String title;
	//描述
	private String summary;
	//true可用,false禁用
	private boolean status;
	//幻灯片集合
	@Transient
	private Set<TopicCarouselSlide> slides = new TreeSet<>();
	
	//empty constructor for JPA instantiation
	public TopicCarousel() {super();}
	public TopicCarousel(String title, String summary, boolean status) {
		this.id=0;
		this.title=title;
		this.summary=summary;
		this.status=status;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public Set<TopicCarouselSlide> getSlides() {
		return slides;
	}
	public void setSlides(Set<TopicCarouselSlide> slides) {
		this.slides = slides;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TopicCarousel other = (TopicCarousel) obj;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
	@Transient
	public static TopicCarousel empty(){
		return new TopicCarousel("", "", true);
	}
}
