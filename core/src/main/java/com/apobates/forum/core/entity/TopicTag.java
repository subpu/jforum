package com.apobates.forum.core.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
/**
 * 话题标签
 * 
 * @author xiaofanku
 * @since 20190301
 */
@Entity
@Table(name = "apo_topic_tag", uniqueConstraints = { @UniqueConstraint(columnNames = {"topicId", "names"})})
public class TopicTag implements Serializable{
	private static final long serialVersionUID = 5621571097419610637L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	//话题ID
	private long topicId;
	//标签名/词语
	private String names;
	//词频
	private int rates;
	//是否删除(1=true可用, 0=false删除)
	private boolean status;
	
	//empty constructor for JPA instantiation
	public TopicTag() { super();}
	public TopicTag(String names, int rates, long topicId) {
		super();
		this.names = names;
		this.status = true;
		this.rates = rates;
		this.topicId = topicId;
	}
	
	public long getId() {
		return id;
	}
	public String getNames() {
		return names;
	}
	public long getTopicId() {
		return topicId;
	}
	public int getRates() {
		return rates;
	}
	public boolean isStatus() {
		return status;
	}
	//--------------------------------------------SET
	public void setId(long id) {
		this.id = id;
	}
	public void setNames(String names) {
		this.names = names;
	}
	public void setTopicId(long topicId) {
		this.topicId = topicId;
	}
	public void setRates(int rates) {
		this.rates = rates;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((names == null) ? 0 : names.hashCode());
		result = prime * result + (int) (topicId ^ (topicId >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TopicTag other = (TopicTag) obj;
		if (names == null) {
			if (other.names != null)
				return false;
		} else if (!names.equals(other.names))
			return false;
		if (topicId != other.topicId)
			return false;
		return true;
	}
	
}
