package com.apobates.forum.core.plug;

import com.apobates.forum.core.entity.TopicCategory;
import com.apobates.forum.core.entity.TopicConfig;

/**
 * 插件话题
 *
 * @author xiaofanku
 * @since 20200530
 */
public interface PlugTopic {
    /**
     * 话题的主题
     *
     * @return
     */
    String getTitle();
    
    /**
     * 话题的内容
     *
     * @return
     */
    String getContent();
    
    /**
     * 话题的类型
     *
     * @return
     */
    TopicCategory getCategory();
    
    /**
     * 话题安装到的版块组/卷
     *
     * @return
     */
    int getVolumes();
    
    /**
     * 话题安装到的版块
     *
     * @return
     */
    long getBoard();
    
    /**
     * 话题的配置实例
     *
     * @return
     */
    TopicConfig getConfig();
    
    /**
     * 是否对外公开,false不公开,true公开
     *
     * @return
     */
    boolean isSuggest();
}
