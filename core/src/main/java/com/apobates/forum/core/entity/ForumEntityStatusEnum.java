package com.apobates.forum.core.entity;

import com.apobates.forum.utils.lang.EnumArchitecture;
/**
 * 论坛的状态
 * 适用于:版块,话题
 * 
 * @author xiaofanku
 * @since 20190302
 */
public enum ForumEntityStatusEnum implements EnumArchitecture{
	//看不到
	DELETE(0, "删除"),
	//看到但打不开
	LOCKED(1, "锁定"),
	//--------------------以下两个跟配置文件模式对冲
	//能看,能打开,不能发贴,不能回复
	//SHOWED(2, "只读"),
	//能看,能打开,能回复,不能发贴
	//REPLYED(3, "回复"),
	//--------------------以上两个跟配置文件模式对冲
	ACTIVE(4, "正常");
	
	private final int symbol;
	private final String title;
	
	private ForumEntityStatusEnum(int symbol, String title) {
		this.symbol = symbol;
		this.title = title;
	}
	@Override
	public int getSymbol() {
		return symbol;
	}
	@Override
	public String getTitle() {
		return title;
	}
	
	public static ForumEntityStatusEnum getInstance(String name){
		ForumEntityStatusEnum data=null;
		for(ForumEntityStatusEnum ese : ForumEntityStatusEnum.values()){
			if(ese.name().toLowerCase().equals(name)){
				data = ese;
				break;
			}
		}
		return data;
	}
}
