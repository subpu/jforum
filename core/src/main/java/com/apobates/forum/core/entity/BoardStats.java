package com.apobates.forum.core.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "apo_board_stats", uniqueConstraints={@UniqueConstraint(columnNames={"boardId"})})
public class BoardStats implements Serializable{
	private static final long serialVersionUID = 8660609463251710032L;
	@Id
	private long boardId;
	//卷的ID
	private int volumesId;
	//话题数
	private long topices;
	//回复数
	private long postses;
	//收藏数
	private long favorites;
	//最近发布的话题标题
	private String recentTopicTitle;
	//最近发布的话题ID
	private long recentTopicId;
	//最近发布的话题时间
	private LocalDateTime recentTopicDate;
	//最近发布的话题作者ID
	private long recentTopicMemberId;
	//最近发布的话题作者帐号
	private String recentTopicMemberNickname;
	//统计更新的时间
	private LocalDateTime updateDate;
	//今天的话题数|数据库中不存在
	@Transient
	private long todayTopices=0L;
	
	//empty constructor for JPA instantiation
	public BoardStats() {super();}
	//创建版块时
	public BoardStats(long boardId, int volumesId) {
		this.boardId = boardId;
		this.volumesId = volumesId;
		//
		this.topices = 0;
		this.postses = 0;
		this.favorites = 0;
		
		this.recentTopicTitle=null;
		this.recentTopicId=0L;
		this.recentTopicDate = null;
		this.recentTopicMemberId = 0L;
		this.recentTopicMemberNickname = null;
		this.updateDate = LocalDateTime.now();
	}
	public long getBoardId() {
		return boardId;
	}
	public void setBoardId(long boardId) {
		this.boardId = boardId;
	}
	public long getTopices() {
		return topices;
	}
	public void setTopices(long topices) {
		this.topices = topices;
	}
	public long getPostses() {
		return postses;
	}
	public void setPostses(long postses) {
		this.postses = postses;
	}
	public String getRecentTopicTitle() {
		return recentTopicTitle;
	}
	public void setRecentTopicTitle(String recentTopicTitle) {
		this.recentTopicTitle = recentTopicTitle;
	}
	public long getRecentTopicId() {
		return recentTopicId;
	}
	public void setRecentTopicId(long recentTopicId) {
		this.recentTopicId = recentTopicId;
	}
	public LocalDateTime getRecentTopicDate() {
		return recentTopicDate;
	}
	public void setRecentTopicDate(LocalDateTime recentTopicDate) {
		this.recentTopicDate = recentTopicDate;
	}
	public long getRecentTopicMemberId() {
		return recentTopicMemberId;
	}
	public void setRecentTopicMemberId(long recentTopicMemberId) {
		this.recentTopicMemberId = recentTopicMemberId;
	}
	public String getRecentTopicMemberNickname() {
		return recentTopicMemberNickname;
	}
	public void setRecentTopicMemberNickname(String recentTopicMemberNickname) {
		this.recentTopicMemberNickname = recentTopicMemberNickname;
	}
	public LocalDateTime getUpdateDate() {
		if(getTopices() == 0){
			return null;
		}
		return updateDate;
	}
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}
	public long getFavorites() {
		return favorites;
	}
	public void setFavorites(long favorites) {
		this.favorites = favorites;
	}
	public int getVolumesId() {
		return volumesId;
	}
	public void setVolumesId(int volumesId) {
		this.volumesId = volumesId;
	}
	public long getTodayTopices() {
		return todayTopices;
	}
	public void setTodayTopices(long todayTopices) {
		this.todayTopices = todayTopices;
	}
	@Transient
	public Map<String,Long> statsMap(){
		Map<String,Long> data = new HashMap<>();
		data.put("threads", getTopices());
		data.put("replies", getPostses());
		data.put("favorites", getFavorites());
		return data;
	}
}
