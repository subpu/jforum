package com.apobates.forum.core.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
/**
 * 话题子分类
 * 
 * @author xiaofanku
 * @since 20190303
 */
@Entity
@Table(name = "apo_topic_category_item")
public class TopicCategoryItem implements Serializable{
	private static final long serialVersionUID = -4667065219969047447L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	//参数值,category.value=year, categoryItem.value=1980
	private String value;
	//参数名,category.names=年代, categoryItem.value=80年代
	private String names;
	//父分类的ID
	private int topicCategoryId;
	
	//empty constructor for JPA instantiation
	public TopicCategoryItem() {super();}
	public TopicCategoryItem(String value, String names, int topicCategoryId) {
		super();
		this.value = value;
		this.names = names;
		this.topicCategoryId = topicCategoryId;
	}

	public long getId() {
		return id;
	}
	public String getValue() {
		return value;
	}
	public String getNames() {
		return names;
	}
	public int getTopicCategoryId() {
		return topicCategoryId;
	}
	//--------------------------------------------------------------SET
	public void setId(long id) {
		this.id = id;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public void setNames(String names) {
		this.names = names;
	}
	public void setTopicCategoryId(int topicCategoryId) {
		this.topicCategoryId = topicCategoryId;
	}
	@Transient
	public static TopicCategoryItem empty() {
		return new TopicCategoryItem("NUL","NUL", 0);
	}
}
