package com.apobates.forum.core.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
/**
 * 回复情绪记录
 * 
 * @author xiaofanku
 * @since 20191112
 */
@Entity
@Table(name = "apo_topic_posts_mood", uniqueConstraints={@UniqueConstraint(columnNames={"postsId","memberId"})})
public class PostsMoodRecords implements Serializable{
	private static final long serialVersionUID = 925846941706385841L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	//回复ID
	private long postsId;
	//话题ID
	private long topicId;
	//作者:会员的ID
	private long memberId;
	private String memberNickname;
	//喜欢|true,false讨厌
	private boolean liked;
	//回复日期
	private LocalDateTime entryDateTime;
	
	//empty constructor for JPA instantiation
	public PostsMoodRecords() {super();}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getPostsId() {
		return postsId;
	}
	public void setPostsId(long postsId) {
		this.postsId = postsId;
	}
	public long getMemberId() {
		return memberId;
	}
	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}
	public String getMemberNickname() {
		return memberNickname;
	}
	public void setMemberNickname(String memberNickname) {
		this.memberNickname = memberNickname;
	}
	public boolean isLiked() {
		return liked;
	}
	public void setLiked(boolean liked) {
		this.liked = liked;
	}
	public LocalDateTime getEntryDateTime() {
		return entryDateTime;
	}
	public void setEntryDateTime(LocalDateTime entryDateTime) {
		this.entryDateTime = entryDateTime;
	}
	public long getTopicId() {
		return topicId;
	}
	public void setTopicId(long topicId) {
		this.topicId = topicId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (memberId ^ (memberId >>> 32));
		result = prime * result + (int) (postsId ^ (postsId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PostsMoodRecords other = (PostsMoodRecords) obj;
		if (memberId != other.memberId)
			return false;
		if (postsId != other.postsId)
			return false;
		return true;
	}
	
}
