package com.apobates.forum.core.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Basic;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import com.apobates.forum.event.elderly.ForumActionEnum;
import javax.persistence.DiscriminatorType;

@Entity
@Table(name = "apo_action_event_set")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="TYPE", discriminatorType=DiscriminatorType.STRING, length=20)
@DiscriminatorValue("P")
public class ActionEventDescriptor implements Serializable{
	private static final long serialVersionUID = 7428831598004482750L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	protected long id;
	//会员的昵称
	protected String memberNickname;
	//会员的ID
	protected long memberId;
	//动作
	@Basic
	@Enumerated(EnumType.STRING)
	protected ForumActionEnum action;
	//创建日期
	protected LocalDateTime entryDateTime;
	//IP
	protected String ipAddr;
	protected String token;
	//对于有的记录需要后续处理
	protected boolean processed;
	//被害人/受害人/原告人
	//某些事件的原告人=memberId/被告人
	protected long victim;
	
	//empty constructor for JPA instantiation
	public ActionEventDescriptor() {super();}
	public ActionEventDescriptor(
			String memberNickname, 
			long memberId, 
			ForumActionEnum action,
			LocalDateTime entryDateTime, 
			String ipAddr, 
			String token, 
			boolean processed, 
			long victim) {
		super();
		this.memberNickname = memberNickname;
		this.memberId = memberId;
		this.action = action;
		this.entryDateTime = entryDateTime;
		this.ipAddr = ipAddr;
		this.token = token;
		this.processed = processed;
		this.victim = victim;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getMemberNickname() {
		return memberNickname;
	}
	public void setMemberNickname(String memberNickname) {
		this.memberNickname = memberNickname;
	}
	public long getMemberId() {
		return memberId;
	}
	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}
	public ForumActionEnum getAction() {
		return action;
	}
	public void setAction(ForumActionEnum action) {
		this.action = action;
	}
	public LocalDateTime getEntryDateTime() {
		return entryDateTime;
	}
	public void setEntryDateTime(LocalDateTime entryDateTime) {
		this.entryDateTime = entryDateTime;
	}
	public String getIpAddr() {
		return ipAddr;
	}
	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public boolean isProcessed() {
		return processed;
	}
	public void setProcessed(boolean processed) {
		this.processed = processed;
	}
	/*
	@Transient
	public String getModule() {
		return "";
	}

	@Transient
	public String getTitle() {
		return "";
	}
	@Transient
	public long getRecordId() {
		return 0L;
	}*/
	public long getVictim() {
		return victim;
	}
	public void setVictim(long victim) {
		this.victim = victim;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((action == null) ? 0 : action.hashCode());
		result = prime * result + ((entryDateTime == null) ? 0 : entryDateTime.hashCode());
		result = prime * result + (int) (memberId ^ (memberId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ActionEventDescriptor other = (ActionEventDescriptor) obj;
		if (action != other.action) {
			return false;
		}
		if (entryDateTime == null) {
			if (other.entryDateTime != null) {
				return false;
			}
		} else if (!entryDateTime.equals(other.entryDateTime)) {
			return false;
		}
		return memberId == other.memberId;
	}
}
