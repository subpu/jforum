package com.apobates.forum.core.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "apo_topic_stats", uniqueConstraints = { @UniqueConstraint(columnNames = {"topicId"}) })
public class TopicStats implements Serializable{
	private static final long serialVersionUID = -8368272385187201003L;
	@Id
	private long topicId;
	private int volumesId;
	private long boardId;
	//回复数
	private long postses;
	//查看数
	private long displaies;
	//收藏数
	private long favorites;
	//点赞的数
	private long likes;
	//最近回复的作者ID
	private long recentPostsMemberId;
	//最近回复的作者帐号
	private String recentPostsMemberNickname;
	//最近回复的日期
	private LocalDateTime recentPostsDate;
	//统计更新的时间
	private LocalDateTime updateDate;
	//最近回复的回复ID
	private long recentPostsId;
	
	//empty constructor for JPA instantiation
	public TopicStats() {super();}
	//创建话题时
	public TopicStats(long topicId, int volumesId, long boardId) {
		this.topicId = topicId;
		this.volumesId = volumesId;
		this.boardId = boardId;
		//
		this.postses = 0;
		this.displaies = 0;
		this.favorites = 0;
		this.likes = 0;
		this.recentPostsMemberId = 0L;
		this.recentPostsMemberNickname=null;
		this.recentPostsDate=LocalDateTime.now();
		this.updateDate = LocalDateTime.now();
		this.recentPostsId = 0L;
	}
	public long getTopicId() {
		return topicId;
	}
	public void setTopicId(long topicId) {
		this.topicId = topicId;
	}
	public long getPostses() {
		return postses;
	}
	public void setPostses(long postses) {
		this.postses = postses;
	}
	public long getDisplaies() {
		return displaies;
	}
	public void setDisplaies(long displaies) {
		this.displaies = displaies;
	}
	public long getFavorites() {
		return favorites;
	}
	public void setFavorites(long favorites) {
		this.favorites = favorites;
	}
	public long getRecentPostsMemberId() {
		return recentPostsMemberId;
	}
	public void setRecentPostsMemberId(long recentPostsMemberId) {
		this.recentPostsMemberId = recentPostsMemberId;
	}
	public String getRecentPostsMemberNames() {
		if(getPostses()==0 && recentPostsMemberNickname == null){
			return "-";
		}
		return recentPostsMemberNickname;
	}
	public LocalDateTime getRecentPostsDate() {
		return recentPostsDate;
	}
	public void setRecentPostsDate(LocalDateTime recentPostsDate) {
		this.recentPostsDate = recentPostsDate;
	}
	public LocalDateTime getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(LocalDateTime updateDate) {
		this.updateDate = updateDate;
	}
	public long getRecentPostsId() {
		return recentPostsId;
	}
	public void setRecentPostsId(long recentPostsId) {
		this.recentPostsId = recentPostsId;
	}
	public int getVolumesId() {
		return volumesId;
	}
	public void setVolumesId(int volumesId) {
		this.volumesId = volumesId;
	}
	public long getBoardId() {
		return boardId;
	}
	public void setBoardId(long boardId) {
		this.boardId = boardId;
	}
	public long getLikes() {
		return likes;
	}
	public void setLikes(long likes) {
		this.likes = likes;
	}
	public String getRecentPostsMemberNickname() {
		return recentPostsMemberNickname;
	}
	public void setRecentPostsMemberNickname(String recentPostsMemberNickname) {
		this.recentPostsMemberNickname = recentPostsMemberNickname;
	}
	@Transient
	public long getTopicPageSize(int pageSize){
		long currentPosts = getPostses();
		if(currentPosts <= pageSize){
			return 1;
		}
		// 整除
		long localTotalpage = currentPosts / pageSize;
		// 不整除
		double a = currentPosts % pageSize;
		if (a != 0) {
			localTotalpage += 1;
		}
		return localTotalpage;
	}
	@Transient
	public static TopicStats empty(long topicId, long topicMember, String topicNames){
		TopicStats ts = new TopicStats(topicId, 0, 0);
		ts.setRecentPostsMemberId(topicMember);
		ts.setRecentPostsMemberNickname(topicNames);
		return ts;
	}
	@Transient
	public Map<String,Long> statsMap(){
		Map<String,Long> data = new HashMap<>();
		data.put("views", getDisplaies());
		data.put("replies", getPostses());
		data.put("likes", getLikes());
		data.put("favorites", getFavorites());
		return data;
	}
}
