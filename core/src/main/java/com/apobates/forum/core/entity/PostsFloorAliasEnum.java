package com.apobates.forum.core.entity;

import com.apobates.forum.utils.lang.EnumArchitecture;
/**
 * 回复楼层的别名
 * 
 * @author xiaofanku
 * @since 20190325
 */
public enum PostsFloorAliasEnum implements EnumArchitecture{
	LORD(1, "楼主"),
	SOFA(2, "沙发"),
	CHAIR(3, "藤椅"),
	STOOL(4, "板凳"),
	PAPER(5, "报纸"),
	DECK(6, "地板");
	
	private final int symbol;
	private final String title;
	
	private PostsFloorAliasEnum(int symbol, String title) {
		this.symbol = symbol;
		this.title = title;
	}
	@Override
	public int getSymbol() {
		return symbol;
	}
	@Override
	public String getTitle() {
		return title;
	}
}
