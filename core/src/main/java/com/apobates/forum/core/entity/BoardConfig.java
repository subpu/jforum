package com.apobates.forum.core.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import com.apobates.forum.member.entity.MemberGroupEnum;
import com.apobates.forum.member.entity.MemberRoleEnum;
/**
 * 版块配置
 * 
 * @author xiaofanku@live.cn
 * @since 20171007|20190303
 */
@Entity
@Table(name = "apo_board_config", uniqueConstraints = { @UniqueConstraint(columnNames = { "boardId" }) })
public class BoardConfig implements Serializable {
	private static final long serialVersionUID = 6586242804914613696L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private long boardId;
	//true表示可读可写,false表示只读
	private boolean readWrite;
	// 写
	// 作者可编辑内容的时间有效期(分钟).-1表示不限制
	private int editMinute;
	// 同一会员不间断的发贴的数量
	private int writeMinInterrupt;
	// 积分需求
	private int writeMinScore;
	// 会员组需求
	@Basic
	@Enumerated(EnumType.STRING)
	private MemberGroupEnum writeLowMemberGroup;
	// 会员角色需求
	@Basic
	@Enumerated(EnumType.STRING)
	private MemberRoleEnum writeLowMemberRole;
	// 会员等级需求
	private int writeLowMemberLevel;
	// 读
	// 积分需求
	private int readMinScore;
	// 会员组需求
	@Basic
	@Enumerated(EnumType.STRING)
	private MemberGroupEnum readLowMemberGroup;
	// 会员角色需求
	@Basic
	@Enumerated(EnumType.STRING)
	private MemberRoleEnum readLowMemberRole;
	// 会员等级需求
	private int readLowMemberLevel;
	//不分读写操作设置
	// ip地址过滤
	private boolean ipFilter;
	// 启用AdditionalStrategy扩展策略,false全部AdditionalStrategy扩展策略不可用
	//private boolean supply;

	// empty constructor for JPA instantiation
	public BoardConfig() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getBoardId() {
		return boardId;
	}

	public void setBoardId(long boardId) {
		this.boardId = boardId;
	}

	public int getEditMinute() {
		return editMinute;
	}

	public void setEditMinute(int editMinute) {
		this.editMinute = editMinute;
	}

	public boolean isReadWrite() {
		return readWrite;
	}

	public void setReadWrite(boolean readWrite) {
		this.readWrite = readWrite;
	}

	public int getWriteMinInterrupt() {
		return writeMinInterrupt;
	}

	public void setWriteMinInterrupt(int writeMinInterrupt) {
		this.writeMinInterrupt = writeMinInterrupt;
	}

	public int getWriteMinScore() {
		return writeMinScore;
	}

	public void setWriteMinScore(int writeMinScore) {
		this.writeMinScore = writeMinScore;
	}

	public MemberGroupEnum getWriteLowMemberGroup() {
		return writeLowMemberGroup;
	}

	public void setWriteLowMemberGroup(MemberGroupEnum writeLowMemberGroup) {
		this.writeLowMemberGroup = writeLowMemberGroup;
	}

	public int getWriteLowMemberLevel() {
		return writeLowMemberLevel;
	}

	public void setWriteLowMemberLevel(int writeLowMemberLevel) {
		this.writeLowMemberLevel = writeLowMemberLevel;
	}

	public int getReadMinScore() {
		return readMinScore;
	}

	public void setReadMinScore(int readMinScore) {
		this.readMinScore = readMinScore;
	}

	public MemberGroupEnum getReadLowMemberGroup() {
		return readLowMemberGroup;
	}

	public void setReadLowMemberGroup(MemberGroupEnum readLowMemberGroup) {
		this.readLowMemberGroup = readLowMemberGroup;
	}

	public int getReadLowMemberLevel() {
		return readLowMemberLevel;
	}

	public void setReadLowMemberLevel(int readLowMemberLevel) {
		this.readLowMemberLevel = readLowMemberLevel;
	}

	public boolean isIpFilter() {
		return ipFilter;
	}

	public void setIpFilter(boolean ipFilter) {
		this.ipFilter = ipFilter;
	}
	
	public MemberRoleEnum getWriteLowMemberRole() {
		return writeLowMemberRole;
	}

	public void setWriteLowMemberRole(MemberRoleEnum writeLowMemberRole) {
		this.writeLowMemberRole = writeLowMemberRole;
	}

	public MemberRoleEnum getReadLowMemberRole() {
		return readLowMemberRole;
	}

	public void setReadLowMemberRole(MemberRoleEnum readLowMemberRole) {
		this.readLowMemberRole = readLowMemberRole;
	}

	@Transient
	public static BoardConfig empty() {
		return new BoardConfig();
	}
	@Transient
	public static BoardConfig defaultConfig(long boardId) {
		BoardConfig config = new BoardConfig();
		config.setReadWrite(true);
		config.setEditMinute(3);
		config.setWriteMinInterrupt(3);
		config.setIpFilter(true);
		
		config.setWriteMinScore(0);
		config.setWriteLowMemberGroup(MemberGroupEnum.CARD);
		config.setWriteLowMemberRole(MemberRoleEnum.NO);
		config.setWriteLowMemberLevel(0);
		
		config.setReadMinScore(0);
		config.setReadLowMemberGroup(MemberGroupEnum.GUEST);
		config.setReadLowMemberRole(MemberRoleEnum.NO);
		config.setReadLowMemberLevel(0);
		
		
		config.setId(0);
		config.setBoardId(boardId);
		return config;
	}
	
	@Transient
	public static BoardConfig originConfig(long boardId){
		BoardConfig config = new BoardConfig();
		config.setReadWrite(false);
		config.setEditMinute(999);
		config.setWriteMinInterrupt(999);
		
		config.setWriteMinScore(999);
		config.setWriteLowMemberGroup(MemberGroupEnum.LEADER);
		config.setWriteLowMemberRole(MemberRoleEnum.ADMIN);
		config.setWriteLowMemberLevel(999);
		
		config.setReadMinScore(0);
		config.setReadLowMemberGroup(MemberGroupEnum.GUEST);
		config.setReadLowMemberRole(MemberRoleEnum.NO);
		config.setReadLowMemberLevel(0);
		
		config.setIpFilter(false);
		config.setId(0);
		config.setBoardId(boardId);
		return config;
	}
}
