package com.apobates.forum.core;

import java.io.Serializable;
/**
 * 回复喜好统计记录
 * 
 * @author xiaofanku
 * @since 20191115
 */
public class MoodCollectResult implements Serializable{
	private static final long serialVersionUID = 2334695589177353013L;
	//哪个话题
	private final long topic;
	//哪个回复
	private final long posts;
	//点赞的数量
	private final long likes;
	//点讨厌的数量
	private final long hates;
	
	public MoodCollectResult(long topic, long posts, long likes, long hates) {
		super();
		this.topic = topic;
		this.posts = posts;
		this.likes = likes;
		this.hates = hates;
	}

	public long getTopic() {
		return topic;
	}

	public long getPosts() {
		return posts;
	}

	public long getLikes() {
		return likes;
	}

	public long getHates() {
		return hates;
	}
}
