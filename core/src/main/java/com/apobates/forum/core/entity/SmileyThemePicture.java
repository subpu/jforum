package com.apobates.forum.core.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
/**
 * 表情图片
 * 
 * @author xiaofanku
 * @since 20190529
 */
@Entity
@Table(name = "apo_smiley_pic", uniqueConstraints={@UniqueConstraint(columnNames={"fileNames", "smileyThemeId"})})
public class SmileyThemePicture implements Serializable{
	private static final long serialVersionUID = 5803173182520158975L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	//图片文件名,例:03.gif
	private String fileNames;
	//说明,例:哈哈
	private String description;
	//显示顺序:普通话题是1,置顶的依次++
	private int ranking;
	//状态
	private boolean status;
	//创建日期
	private LocalDateTime entryDateTime;
	//所属的风格
	private int smileyThemeId;
	//所属风格的目录名
	private String smileyThemeDirectNames;
	@Transient
	private SmileyTheme smileyTheme;
	
	public SmileyThemePicture(String fileNames, String description, int ranking, boolean status, int smileyThemeId, SmileyTheme smileyTheme) {
		super();
		this.fileNames = fileNames;
		this.description = description;
		this.ranking = ranking;
		this.status = status;
		this.entryDateTime=LocalDateTime.now();
		this.smileyThemeId = smileyThemeId;
		if(smileyTheme!=null){
			this.smileyThemeDirectNames = smileyTheme.getDirectNames();
		}
		this.smileyTheme = smileyTheme;
	}
	//empty constructor for JPA instantiation
	public SmileyThemePicture() {
		super();
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFileNames() {
		return fileNames;
	}
	public void setFileNames(String fileNames) {
		this.fileNames = fileNames;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getRanking() {
		return ranking;
	}
	public void setRanking(int ranking) {
		this.ranking = ranking;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public int getSmileyThemeId() {
		return smileyThemeId;
	}
	public void setSmileyThemeId(int smileyThemeId) {
		this.smileyThemeId = smileyThemeId;
	}
	public SmileyTheme getSmileyTheme() {
		return smileyTheme;
	}
	public void setSmileyTheme(SmileyTheme smileyTheme) {
		this.smileyTheme = smileyTheme;
	}
	public LocalDateTime getEntryDateTime() {
		return entryDateTime;
	}
	public void setEntryDateTime(LocalDateTime entryDateTime) {
		this.entryDateTime = entryDateTime;
	}
	public String getSmileyThemeDirectNames() {
		return smileyThemeDirectNames;
	}
	public void setSmileyThemeDirectNames(String smileyThemeDirectNames) {
		this.smileyThemeDirectNames = smileyThemeDirectNames;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fileNames == null) ? 0 : fileNames.hashCode());
		result = prime * result + smileyThemeId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SmileyThemePicture other = (SmileyThemePicture) obj;
		if (fileNames == null) {
			if (other.fileNames != null)
				return false;
		} else if (!fileNames.equals(other.fileNames))
			return false;
		if (smileyThemeId != other.smileyThemeId)
			return false;
		return true;
	}
	@Transient
	public static SmileyThemePicture empty(long id, int themeId) {
		SmileyThemePicture stp = new SmileyThemePicture("-", "Do Not Allow blank", 1, true, themeId, null);
		stp.setId(id);
		return stp;
	}
}
