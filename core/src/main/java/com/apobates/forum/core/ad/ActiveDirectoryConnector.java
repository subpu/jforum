package com.apobates.forum.core.ad;

import java.util.Optional;

/**
 * 目录服务连接器,已知实现类:BoardGroup, Board, Topic
 * 
 * @author xiaofanku
 * @since 20191104
 * @param <T>
 */
public interface ActiveDirectoryConnector<T> {
	/**
	 * 返回名称标识
	 * 
	 * @return
	 */
	String getConnect();
	
	/**
	 * getConnect的逆方法,使用getConnect方法的值返回一个实现类的实例
	 * 
	 * @param connectValue 名称标识
	 * @return
	 * @throws IllegalStateException
	 */
	Optional<T> toConnector(String connectValue)throws IllegalStateException;
}
