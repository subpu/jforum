package com.apobates.forum.core.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
/**
 * 轮播图的幻灯片
 * 
 * @author xiaofanku
 * @since 20191012
 */
@Entity
@Table(name = "apo_topic_carousel_slide", uniqueConstraints = { @UniqueConstraint(columnNames = {"link","imageAddr"})})
public class TopicCarouselSlide implements Serializable,Comparable<TopicCarouselSlide>{
	private static final long serialVersionUID = 4230786860004000283L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	//标题
	private String title;
	//跳转后的地址
	private String link;
	//图片的[编码|解码]地址
	private String imageAddr;
	//显示顺序
	private int ranking;
	//所属的轮播图
	private int topicCarouselId; 
	//true可用,false禁用
	private boolean status;
	
	//empty constructor for JPA instantiation
	public TopicCarouselSlide() {super();}
	public TopicCarouselSlide(int topicCarouselId, String title, String link, String imageAddr, int ranking, boolean status) {
		this.id=0;
		this.title=title;
		this.link=link;
		this.imageAddr=imageAddr;
		this.ranking=ranking;
		this.status=status;
		this.topicCarouselId = topicCarouselId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getImageAddr() {
		return imageAddr;
	}
	public void setImageAddr(String imageAddr) {
		this.imageAddr = imageAddr;
	}
	public int getRanking() {
		return ranking;
	}
	public void setRanking(int ranking) {
		this.ranking = ranking;
	}
	public int getTopicCarouselId() {
		return topicCarouselId;
	}
	public void setTopicCarouselId(int topicCarouselId) {
		this.topicCarouselId = topicCarouselId;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((imageAddr == null) ? 0 : imageAddr.hashCode());
		result = prime * result + ((link == null) ? 0 : link.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TopicCarouselSlide other = (TopicCarouselSlide) obj;
		if (imageAddr == null) {
			if (other.imageAddr != null)
				return false;
		} else if (!imageAddr.equals(other.imageAddr))
			return false;
		if (link == null) {
			if (other.link != null)
				return false;
		} else if (!link.equals(other.link))
			return false;
		return true;
	}
	

	@Override
	public int compareTo(TopicCarouselSlide o) {
		return ranking - o.getRanking();
	}
	
	@Transient
	public static TopicCarouselSlide empty(int topicCarouselId){
		return new TopicCarouselSlide(topicCarouselId, "", "", null, 1, true);
	}
}
