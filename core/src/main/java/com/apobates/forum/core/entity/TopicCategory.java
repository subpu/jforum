package com.apobates.forum.core.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
/**
 * 话题类型
 * 
 * @author xiaofanku
 * @since 20190301
 */
@Entity
@Table(name = "apo_topic_category", uniqueConstraints={@UniqueConstraint(columnNames={"value"})})
public class TopicCategory implements Serializable, Comparable<TopicCategory>{
	private static final long serialVersionUID = 8469618124496693022L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	//参数值,year
	private String value;
	//参数名,年代
	private String names;
	//是否删除(1=true可用, 0=false删除)
	private boolean status;
	//是否内置分类(1=true是内置)
	//private boolean natives;
	
	//empty constructor for JPA instantiation
	public TopicCategory() {
		super();
		this.value = "";
		this.names = "";
		this.status = true;
	}
	public TopicCategory(String value, String names) {
		super();
		this.value = value;
		this.names = names;
		this.status = true;
		//this.natives = false;
	}

	public int getId() {
		return id;
	}
	public String getNames() {
		return names;
	}
	public String getValue() {
		return value;
	}
	//--------------------------------------------------------------SET
	public void setId(int id) {
		this.id = id;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public void setNames(String names) {
		this.names = names;
	}
	
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	@Transient
	public static TopicCategory empty() {
		TopicCategory tc = new TopicCategory("normal", "普通");
		tc.setId(0);
		return tc;
	}
	
	@Transient
	public static TopicCategory feedback(){
		TopicCategory tc = new TopicCategory("feedback", "意见反馈");
		tc.setId(-1);
		return tc;
	}
	
	@Transient
	public static TopicCategory report(){
		TopicCategory tc = new TopicCategory("report", "举报");
		tc.setId(-2);
		return tc;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TopicCategory other = (TopicCategory) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	
	@Transient
	public static boolean isKeywords(String value){
		return Arrays.asList("normal","feedback","report").contains(value);
	}
	
	@Transient
	public static List<String> isForbidCategoryValue(){
		return Arrays.asList("feedback","report");
	}
	
	@Override
	public int compareTo(TopicCategory o) {
		TopicCategory other = o;
		return this.getId() > other.getId() ? +1 : this.getId() < other.getId() ? -1 : 0;
	}
	
}
