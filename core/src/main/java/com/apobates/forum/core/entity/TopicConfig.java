package com.apobates.forum.core.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import com.apobates.forum.member.entity.MemberGroupEnum;
import com.apobates.forum.member.entity.MemberRoleEnum;
/**
 * 话题配置
 * 
 * @author xiaofanku
 * @since 20190325
 */
@Entity
@Table(name = "apo_topic_config", uniqueConstraints = { @UniqueConstraint(columnNames = { "topicId" }) })
public class TopicConfig  implements Serializable{
	private static final long serialVersionUID = 8022335852544958725L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	//话题
	private long topicId;
	//是否是私人的
	//false表示公开,谁都可以看/true只有我的圈可以看
	private boolean privacy;
	//是否允许回复|是否允话评论,true允许
	private boolean reply;
	//开启回复通知,true开启
	private boolean notify;
	//A读
	// 积分需求
	private int readMinScore;
	// 会员组需求
	@Basic
	@Enumerated(EnumType.STRING)
	private MemberGroupEnum readLowMemberGroup;
	// 会员角色需求
	@Basic
	@Enumerated(EnumType.STRING)
	private MemberRoleEnum readLowMemberRole;
	// 角色会员等级需求
	private int readLowMemberLevel;
	
	//B写
	// 积分需求
	private int writeMinScore;
	// 会员组需求
	@Basic
	@Enumerated(EnumType.STRING)
	private MemberGroupEnum writeLowMemberGroup;
	// 会员组需求
	@Basic
	@Enumerated(EnumType.STRING)
	private MemberRoleEnum writeLowMemberRole;
	// 角色会员等级需求
	private int writeLowMemberLevel;
	//b.1是否每人只能回一次|福利只能领一次,true只一次,false不限次数
	private boolean atomPoster;
	//b.2回复的最大楼层次|不间断由某个人回复[不是话题的作者能决定]
	//b.3回复的时间上限|超过N天不允许再回复了[不是话题的作者能决定]
	// 同一会员不间断的回贴的数量
	private int writeMinInterrupt=3;
	
	public long getId() {
		return id;
	}
	public long getTopicId() {
		return topicId;
	}
	public boolean isPrivacy() {
		return privacy;
	}
	public boolean isReply() {
		return reply;
	}
	public boolean isNotify() {
		return notify;
	}
	public int getReadMinScore() {
		return readMinScore;
	}
	public MemberGroupEnum getReadLowMemberGroup() {
		return readLowMemberGroup;
	}
	public int getReadLowMemberLevel() {
		return readLowMemberLevel;
	}
	public int getWriteMinScore() {
		return writeMinScore;
	}
	public MemberGroupEnum getWriteLowMemberGroup() {
		return writeLowMemberGroup;
	}
	public int getWriteLowMemberLevel() {
		return writeLowMemberLevel;
	}
	public boolean isAtomPoster() {
		return atomPoster;
	}
	public MemberRoleEnum getReadLowMemberRole() {
		return readLowMemberRole;
	}
	public MemberRoleEnum getWriteLowMemberRole() {
		return writeLowMemberRole;
	}
	//--------------------------------------------------------------SET
	public void setId(long id) {
		this.id = id;
	}
	public void setTopicId(long topicId) {
		this.topicId = topicId;
	}
	public void setPrivacy(boolean privacy) {
		this.privacy = privacy;
	}
	public void setReply(boolean reply) {
		this.reply = reply;
	}
	public void setNotify(boolean notify) {
		this.notify = notify;
	}
	public void setReadMinScore(int readMinScore) {
		this.readMinScore = readMinScore;
	}
	public void setReadLowMemberGroup(MemberGroupEnum readLowMemberGroup) {
		this.readLowMemberGroup = readLowMemberGroup;
	}
	public void setReadLowMemberLevel(int readLowMemberLevel) {
		this.readLowMemberLevel = readLowMemberLevel;
	}
	public void setWriteMinScore(int writeMinScore) {
		this.writeMinScore = writeMinScore;
	}
	public void setWriteLowMemberGroup(MemberGroupEnum writeLowMemberGroup) {
		this.writeLowMemberGroup = writeLowMemberGroup;
	}
	public void setWriteLowMemberLevel(int writeLowMemberLevel) {
		this.writeLowMemberLevel = writeLowMemberLevel;
	}
	public void setAtomPoster(boolean atomPoster) {
		this.atomPoster = atomPoster;
	}
	public void setReadLowMemberRole(MemberRoleEnum readLowMemberRole) {
		this.readLowMemberRole = readLowMemberRole;
	}
	public void setWriteLowMemberRole(MemberRoleEnum writeLowMemberRole) {
		this.writeLowMemberRole = writeLowMemberRole;
	}
	public int getWriteMinInterrupt() {
		return writeMinInterrupt;
	}
	public void setWriteMinInterrupt(int writeMinInterrupt) {
		this.writeMinInterrupt = writeMinInterrupt;
	}
	@Transient
	public static TopicConfig empty() {
		return new TopicConfig();
	}
	@Transient
	public static TopicConfig defaultConfig(long topicId) {
		TopicConfig config = new TopicConfig();
		config.setTopicId(topicId);
		config.setPrivacy(false);
		config.setReply(true);
		config.setNotify(true);
		config.setAtomPoster(false);
		
		config.setReadMinScore(0);
		config.setReadLowMemberGroup(MemberGroupEnum.GUEST);
		config.setReadLowMemberRole(MemberRoleEnum.NO);
		config.setReadLowMemberLevel(0);
		
		config.setWriteMinScore(1);
		config.setWriteLowMemberGroup(MemberGroupEnum.CARD);
		config.setWriteLowMemberRole(MemberRoleEnum.NO);
		config.setWriteLowMemberLevel(1);
		
		return config;
	}
	
	@Transient
	public static TopicConfig originConfig(long topicId){
		TopicConfig config = new TopicConfig();
		config.setTopicId(topicId);
		config.setPrivacy(false);
		config.setReply(false);
		config.setNotify(false);
		config.setAtomPoster(false);
		
		config.setReadMinScore(0);
		config.setReadLowMemberGroup(MemberGroupEnum.GUEST);
		config.setReadLowMemberRole(MemberRoleEnum.NO);
		config.setReadLowMemberLevel(0);
		
		config.setWriteMinScore(999);
		config.setWriteLowMemberGroup(MemberGroupEnum.LEADER);
		config.setWriteLowMemberRole(MemberRoleEnum.ADMIN);
		config.setWriteLowMemberLevel(999);
		
		return config;
	}
}
