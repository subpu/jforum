package com.apobates.forum.core.tag.nlp;
/**
 * 分词中的单词词性枚举
 * 
 * @author xiaofanku
 * @since 20200109
 */
public enum WordNatureEnum {
	Noun("n", "名词"), Name("nr", "人名"), Region("ns", "地理名词"), Organization("nt", "机构名词"), EFB("nn" ,"职业名词"), Academic("g", "学术名称"), Pronouns("r", "代词");
	private final String nature;
	private final String title;
	
	private WordNatureEnum(String nature, String title) {
		this.nature = nature;
		this.title = title;
	}
	
	public String getNature() {
		return nature;
	}
	
	public String getTitle() {
		return title;
	}
}
