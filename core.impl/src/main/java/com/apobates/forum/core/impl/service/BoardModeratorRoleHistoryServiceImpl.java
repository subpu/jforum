package com.apobates.forum.core.impl.service;

import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.apobates.forum.core.api.dao.BoardModeratorRoleHistoryDao;
import com.apobates.forum.core.api.service.BoardModeratorRoleHistoryService;
import com.apobates.forum.core.entity.BoardModeratorRoleHistory;

@Service
public class BoardModeratorRoleHistoryServiceImpl implements BoardModeratorRoleHistoryService{
	@Autowired
	private BoardModeratorRoleHistoryDao boardModeratorRoleHistoryDao;
	
	@Override
	public Stream<BoardModeratorRoleHistory> getByMember(long memberId) {
		return boardModeratorRoleHistoryDao.findAllByMember(memberId);
	}

}
