package com.apobates.forum.core.impl.service;

import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.apobates.forum.core.api.dao.TopicTagSHistoryDao;
import com.apobates.forum.core.api.service.TopicTagSHistoryService;
import com.apobates.forum.core.entity.TopicTagSHistory;
import com.apobates.forum.event.elderly.ActionDescriptor;
import com.apobates.forum.event.elderly.ActionEventCulpritor;
import com.apobates.forum.event.elderly.ForumActionEnum;

@Service
public class TopicTagSHistoryServiceImpl implements TopicTagSHistoryService{
	@Autowired
	private TopicTagSHistoryDao topicTagSHistoryDao;
	
	@ActionDescriptor(action=ForumActionEnum.TOPIC_SEARCH)
	@Override
	public long create(String word, long affect, ActionEventCulpritor culpritor) {
		TopicTagSHistory tth = new TopicTagSHistory(word, affect, culpritor.getMemberId(), culpritor.getMemberNickname());
		topicTagSHistoryDao.save(tth);
		if(tth.getId()>0){
			return tth.getId();
		}
		return 0;
	}

	@Override
	public Stream<TopicTagSHistory> getAllForRelate(String word, int size) {
		return topicTagSHistoryDao.findAllRelate(word, size);
	}

}
