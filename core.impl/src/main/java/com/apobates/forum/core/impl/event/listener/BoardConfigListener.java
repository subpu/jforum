package com.apobates.forum.core.impl.event.listener;

import java.util.Optional;

import com.apobates.forum.core.impl.event.BoardCreateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import com.apobates.forum.core.api.dao.BoardConfigDao;
import com.apobates.forum.core.entity.Board;
import com.apobates.forum.core.entity.BoardConfig;
/**
 * 版块配置侦听器,创建版块的配置文件
 * @author xiaofanku
 * @since 20190703
 */
@Component
public class BoardConfigListener implements ApplicationListener<BoardCreateEvent>{
	@Autowired
	private BoardConfigDao boardConfigDao;
	private final static Logger logger = LoggerFactory.getLogger(BoardConfigListener.class);
	
	@Override
	public void onApplicationEvent(BoardCreateEvent event) {
		logger.info("[Board][CreateEvent][1]版块配置文件开始执行");
		Board board = event.getBoard();
		//版块配置
		BoardConfig boardConfig = Optional.ofNullable(board.getConfigure()).orElse(BoardConfig.defaultConfig(board.getId()));
		boardConfigDao.save(boardConfig);
		logger.info("[Board][CreateEvent][1]版块配置文件执行结束");
	}
}
