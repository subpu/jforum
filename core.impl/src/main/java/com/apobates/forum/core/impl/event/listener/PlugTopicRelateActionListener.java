package com.apobates.forum.core.impl.event.listener;

import com.apobates.forum.core.api.dao.TopicActionCollectionDao;
import com.apobates.forum.core.entity.Topic;
import com.apobates.forum.core.entity.TopicActionCollection;
import com.apobates.forum.core.impl.event.PlugTopicPublishEvent;
import com.apobates.forum.event.elderly.ActionEventCulpritor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * 插件话题发布事件侦听器.创建相关的动作记录
 * @author xiaofanku
 * @since 20200530
 */
@Component
public class PlugTopicRelateActionListener implements ApplicationListener<PlugTopicPublishEvent>{
    @Autowired
    private TopicActionCollectionDao topicActionCollectionDao;
    private final static Logger logger = LoggerFactory.getLogger(PlugTopicRelateActionListener.class);
    
    @Override
    public void onApplicationEvent(PlugTopicPublishEvent e) {
        logger.info("[PlugTopic][PublishEvent][2]插件话题关联动作开始执行");
        Topic tr = e.getPlugTopic();
        ActionEventCulpritor culpritor = e.getCulpritor();
        long topicId = tr.getId();
        long postsId = tr.getContent().getId();
        TopicActionCollection tac = new TopicActionCollection(e.getAction(), tr.getTitle(), topicId, postsId, true, -1, culpritor);
        topicActionCollectionDao.save(tac);
        logger.info("[PlugTopic][PublishEvent][2]插件话题关联动作执行结束");
    }
}
