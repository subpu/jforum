package com.apobates.forum.core.impl.dao;

import java.util.Optional;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.apobates.forum.core.api.dao.BoardModeratorRoleHistoryDao;
import com.apobates.forum.core.entity.BoardModeratorRoleHistory;

@Repository
public class BoardModeratorRoleHistoryDaoImpl implements BoardModeratorRoleHistoryDao{
	@PersistenceContext
	private EntityManager entityManager;
	
	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public void save(BoardModeratorRoleHistory entity) {
		entityManager.persist(entity);
	}

	@Override
	public Optional<BoardModeratorRoleHistory> findOne(Long primaryKey) {
		return Optional.ofNullable(entityManager.find(BoardModeratorRoleHistory.class, primaryKey));
	}

	@Override
	public Optional<Boolean> edit(BoardModeratorRoleHistory updateEntity) {
		return Optional.empty();
	}

	@Override
	public Stream<BoardModeratorRoleHistory> findAll() {
		return Stream.empty();
	}

	@Override
	public long count() {
		return 0L;
	}

	@Override
	public Stream<BoardModeratorRoleHistory> findAllByMember(long memberId) {
		return entityManager.createQuery("SELECT bmr FROM BoardModeratorRoleHistory bmr WHERE bmr.memberId = ?1 AND bmr.status = ?2", BoardModeratorRoleHistory.class)
				.setParameter(1, memberId)
				.setParameter(2, true)
				.getResultStream();
	}
}
