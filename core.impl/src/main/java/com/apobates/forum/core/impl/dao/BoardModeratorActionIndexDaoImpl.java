package com.apobates.forum.core.impl.dao;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.apobates.forum.core.api.dao.BoardModeratorActionIndexDao;
import com.apobates.forum.core.entity.BoardModeratorActionIndex;
import com.apobates.forum.event.elderly.ForumActionEnum;

@Repository
public class BoardModeratorActionIndexDaoImpl implements BoardModeratorActionIndexDao{
	@PersistenceContext
	private EntityManager entityManager;
	@Value("${jpa.batch.size}")
	private int batchSize;

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public void save(BoardModeratorActionIndex entity) {
		entityManager.persist(entity);
	}

	@Override
	public Optional<BoardModeratorActionIndex> findOne(Long primaryKey) {
		BoardModeratorActionIndex bma = entityManager.find(BoardModeratorActionIndex.class, primaryKey);
		return Optional.ofNullable(bma);
	}

	@Override
	public Optional<Boolean> edit(BoardModeratorActionIndex updateEntity) {
		return Optional.empty();
	}

	@Override
	public Stream<BoardModeratorActionIndex> findAll() {
		return Stream.empty();
	}

	@Override
	public long count() {
		return -1L;
	}

	@Override
	public Stream<BoardModeratorActionIndex> getAllByModerator(long moderatorId) {
		return entityManager.createQuery("SELECT bma FROM BoardModeratorActionIndex bma WHERE bma.moderatorId = ?1", BoardModeratorActionIndex.class).setParameter(1, moderatorId).getResultStream();
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public int batchSave(List<BoardModeratorActionIndex> bmActions) {
        int i = 0;
        for (BoardModeratorActionIndex bmai : bmActions) {
            entityManager.persist(bmai);
            i++;
            if (i % batchSize == 0) {
                // Flush a batch of inserts and release memory.
                entityManager.flush();
                entityManager.clear();
            }
        }
        return i;
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public int edit(long moderatorId, List<ForumActionEnum> actions) {
		//清空原来的
		entityManager.createQuery("DELETE FROM BoardModeratorActionIndex bma WHERE bma.moderatorId = ?1").setParameter(1, moderatorId).executeUpdate();
		//重新来插入
        int i = 0;
        for (ForumActionEnum fa : actions) {
            entityManager.persist(new BoardModeratorActionIndex(moderatorId, fa));
            i++;
            if (i % batchSize == 0) {
                // Flush a batch of inserts and release memory.
                entityManager.flush();
                entityManager.clear();
            }
        }
        return i;
	}
}
