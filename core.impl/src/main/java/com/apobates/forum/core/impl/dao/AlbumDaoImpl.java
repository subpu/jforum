package com.apobates.forum.core.impl.dao;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.apobates.forum.core.api.dao.AlbumDao;
import com.apobates.forum.core.entity.Album;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;

@Repository
public class AlbumDaoImpl implements AlbumDao{
	@PersistenceContext
	private EntityManager entityManager;
	private final static Logger logger = LoggerFactory.getLogger(AlbumDaoImpl.class);

	@Override
	public Page<Album> findAll(Pageable pageable) {
		final long total = count();
		if (total == 0) {
			return emptyResult();
		}
		TypedQuery<Album> query = entityManager.createQuery("SELECT a FROM Album a WHERE a.status = ?1 ORDER BY a.entryDateTime DESC",Album.class).setParameter(1, true);
		query.setFirstResult(pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());

		final Stream<Album> result = query.getResultStream();
		return new Page<Album>() {
			@Override
			public long getTotalElements() {
				return total;
			}

			@Override
			public Stream<Album> getResult() {
				return result;
			}
		};
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public void save(Album entity) {
		entityManager.persist(entity);
	}

	@Override
	public Optional<Album> findOne(Long primaryKey) {
		return Optional.ofNullable(entityManager.find(Album.class, primaryKey));
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public Optional<Boolean> edit(Album updateEntity) {
		try{
			entityManager.merge(updateEntity);
			return Optional.of(true);
		} catch (Exception e) {
			if(logger.isDebugEnabled()){
				logger.debug(e.getMessage(), e);
			}
		}
		return Optional.empty();
	}

	@Override
	public Stream<Album> findAll() {
		return Stream.empty();
	}

	@Override
	public long count() {
		try {
			return entityManager.createQuery("SELECT COUNT(a) FROM Album a WHERE a.status = ?1", Long.class).setParameter(1, true).getSingleResult();
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("[count][AlbumDao]", e);
			}
		}
		return 0L;
	}

	@Override
	public Page<Album> findAllByBoard(long boardId, Pageable pageable) {
		final long total = findAllByBoardCount(boardId);
		if (total == 0) {
			return emptyResult();
		}
		TypedQuery<Album> query = entityManager.createQuery("SELECT a FROM Album a WHERE a.status = ?1 AND a.boardId = ?2 ORDER BY a.entryDateTime DESC", Album.class).setParameter(1, true).setParameter(2, boardId);
		query.setFirstResult(pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());

		final Stream<Album> result = query.getResultStream();
		return new Page<Album>() {
			@Override
			public long getTotalElements() {
				return total;
			}

			@Override
			public Stream<Album> getResult() {
				return result;
			}
		};
	}
	
	private long findAllByBoardCount(long boardId){
		try {
			return entityManager.createQuery("SELECT COUNT(a) FROM Album a WHERE a.boardId = ?1 AND a.status = ?2", Long.class).setParameter(1, boardId).setParameter(2, true).getSingleResult();
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("[findAllByBoardCount][AlbumDao]", e);
			}
		}
		return 0L;
	}
	
	@Override
	public Page<Album> findAllByVolumes(int volumesId, Pageable pageable) {
		final long total = findAllByVolumeCount(volumesId);
		if (total == 0) {
			return emptyResult();
		}
		TypedQuery<Album> query = entityManager.createQuery("SELECT a FROM Album a WHERE a.status = ?1 AND a.volumesId = ?2 ORDER BY a.entryDateTime DESC", Album.class).setParameter(1, true).setParameter(2, volumesId);
		query.setFirstResult(pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());

		final Stream<Album> result = query.getResultStream();
		return new Page<Album>() {
			@Override
			public long getTotalElements() {
				return total;
			}

			@Override
			public Stream<Album> getResult() {
				return result;
			}
		};
	}
	private long findAllByVolumeCount(int volumesId){
		try {
			return entityManager.createQuery("SELECT COUNT(a) FROM Album a WHERE a.volumesId = ?1 AND a.status = ?2", Long.class).setParameter(1, volumesId).setParameter(2, true).getSingleResult();
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("[findAllByVolumeCount][AlbumDao]", e);
			}
		}
		return 0L;
	}

	@Override
	public Optional<Album> findOneByTopic(long topicId) {
		try{
			Album a = entityManager.createQuery("SELECT a FROM Album a WHERE a.topicId = ?1", Album.class).setParameter(1, topicId).getSingleResult();
			return Optional.ofNullable(a);
		}catch(javax.persistence.NoResultException e){
			if(logger.isDebugEnabled()){
				logger.debug(e.getMessage(), e);
			}
		}
		return Optional.empty();
	}

	@Override
	public Stream<Album> findAllRecent(int size) {
		return entityManager.createQuery("SELECT a FROM Album a WHERE a.status = ?1 ORDER BY a.entryDateTime DESC", Album.class).setParameter(1, true).setMaxResults(size).getResultStream();
	}

	@Override
	public Stream<Album> findAllByTopic(Collection<Long> topicIdSet) {
		if(topicIdSet == null || topicIdSet.isEmpty()){
			return Stream.empty();
		}
		return entityManager.createQuery("SELECT a FROM Album a WHERE a.status = ?1 AND a.topicId IN ?2", Album.class).setParameter(1, true).setParameter(2, topicIdSet).getResultStream();
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public Optional<Boolean> editCover(long id, String encodeCoverPictureLink, long coverPictureId) {
		int affect = 0;
		try{
			int updateRow = entityManager.createQuery("UPDATE AlbumPicture ap SET ap.cover = ?1 WHERE ap.album.id = ?2 AND ap.id = ?3").setParameter(1, true).setParameter(2, id).setParameter(3, coverPictureId).executeUpdate();
			if(updateRow == 1){
				//原来的封面取消
				entityManager.createQuery("UPDATE AlbumPicture ap SET ap.cover = ?1 WHERE ap.album.id = ?2 AND ap.id != ?3").setParameter(1, false).setParameter(2, id).setParameter(3, coverPictureId).executeUpdate();
				affect = entityManager.createQuery("UPDATE Album a SET a.coverLink = ?1 WHERE a.id = ?2").setParameter(1, encodeCoverPictureLink).setParameter(2, id).executeUpdate();
			}
		}catch(Exception e){
			if(logger.isDebugEnabled()){
				logger.debug(e.getMessage(), e);
			}
		}
		return affect == 1?Optional.of(true):Optional.empty();
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public int bindTopicAlbum(long id, long topicId) {
		int affect = entityManager.createQuery("UPDATE Topic t SET t.albumId = ?1 WHERE t.id = ?2 AND t.albumId = ?3").setParameter(1, id).setParameter(2, topicId).setParameter(3, 0).executeUpdate();
		return affect;
	}
	
}
