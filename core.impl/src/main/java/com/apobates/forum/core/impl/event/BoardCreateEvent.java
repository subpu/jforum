package com.apobates.forum.core.impl.event;

import org.springframework.context.ApplicationEvent;

import com.apobates.forum.core.entity.Board;
/**
 * 版块创建事件
 * 
 * @author xiaofanku
 * @since 20190703
 */
public class BoardCreateEvent extends ApplicationEvent{
	private static final long serialVersionUID = -2855321240838877119L;
	private final Board board;
	
	public BoardCreateEvent(Object source, Board board) {
		super(source);
		this.board = board;
	}

	public Board getBoard() {
		return board;
	}

}
