package com.apobates.forum.core.impl.event;

import com.apobates.forum.core.entity.BoardModerator;
import com.apobates.forum.core.entity.BoardModeratorRoleHistory;
import com.apobates.forum.member.entity.Member;
import org.springframework.context.ApplicationEvent;

/**
 * 版主新生事件
 * @author xiaofanku
 * @since 20200601
 */
public class ModeratorBornEvent extends ApplicationEvent{
    private static final long serialVersionUID = 1L;
    private final BoardModerator moderator;
    private final Member member;
    private final BoardModeratorRoleHistory memberRoleHistory;
    
    public ModeratorBornEvent(Object source, BoardModerator moderator, Member member, BoardModeratorRoleHistory memberRoleHistory) {
        super(source);
        this.moderator = moderator;
        this.member = member;
        this.memberRoleHistory = memberRoleHistory;
    }
    
    public BoardModerator getModerator() {
        return moderator;
    }
    
    public Member getMember() {
        return member;
    }
    
    public BoardModeratorRoleHistory getMemberRoleHistory() {
        return memberRoleHistory;
    }
}
