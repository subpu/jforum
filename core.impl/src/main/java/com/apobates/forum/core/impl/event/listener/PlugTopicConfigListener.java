package com.apobates.forum.core.impl.event.listener;

import com.apobates.forum.core.api.dao.TopicConfigDao;
import com.apobates.forum.core.entity.Topic;
import com.apobates.forum.core.impl.event.PlugTopicPublishEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
/**
 * 插件话题发布事件侦听器.创建话题的配置文件
 * @author xiaofanku
 * @since 20200530
 */
@Component
public class PlugTopicConfigListener implements ApplicationListener<PlugTopicPublishEvent>{
    @Autowired
    private TopicConfigDao topicConfigDao;
    private final static Logger logger = LoggerFactory.getLogger(TopicConfigListener.class);
    
    @Override
    public void onApplicationEvent(PlugTopicPublishEvent event) {
        logger.info("[PlugTopic][PublishEvent][1]插件话题配置文件开始执行");
        // 话题配置
        Topic topic = event.getPlugTopic();
        if(null != topic.getConfigure()){
            topicConfigDao.save(topic.getConfigure());
        }
        logger.info("[PlugTopic][PublishEvent][1]插件话题配置文件执行结束");
    }
}
