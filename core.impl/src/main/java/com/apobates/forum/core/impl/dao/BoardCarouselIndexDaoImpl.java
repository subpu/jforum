package com.apobates.forum.core.impl.dao;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.apobates.forum.core.api.dao.BoardCarouselIndexDao;
import com.apobates.forum.core.entity.BoardCarouselIndex;
import com.apobates.forum.core.entity.TopicCarousel;
import com.apobates.forum.core.entity.TopicCarouselSlide;
import com.apobates.forum.utils.DateTimeUtils;


@Repository
public class BoardCarouselIndexDaoImpl implements BoardCarouselIndexDao{
	@PersistenceContext
	private EntityManager entityManager;
	private final static Logger logger = LoggerFactory.getLogger(BoardCarouselIndexDaoImpl.class);
	
	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public void save(BoardCarouselIndex entity) {
		entityManager.persist(entity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Stream<TopicCarouselSlide> findAllUsedByBoard(int boardGroupId, long boardId) {
		final String SQL="SELECT tcs.* FROM apo_topic_carousel_slide AS tcs "
									+ "JOIN apo_topic_carousel_board_index AS tcb "
										+ "ON tcb.CAROUSELID = tcs.TOPICCAROUSELID "
									+ "WHERE tcs.STATUS = ?1 AND tcb.BOARDID = ?2 AND tcb.VOLUMESID = ?3 AND ?4 BETWEEN tcb.ENTRYDATETIME AND tcb.EXPIREDATETIME "
									+ "ORDER BY tcs.RANKING ASC";
		return entityManager.createNativeQuery(SQL, TopicCarouselSlide.class)
						.setParameter(1, true)
						.setParameter(2, boardId)
						.setParameter(3, boardGroupId)
						.setParameter(4, DateTimeUtils.getRFC3339(LocalDateTime.now()))
						.getResultStream();
	}

	@Override
	public Stream<TopicCarouselSlide> findAllUsedByBoard(int boardGroupId) {
		return findAllUsedByBoard(boardGroupId, 0);
	}

	@Override
	public Optional<TopicCarousel> findOneByBoard(int boardGroupId, long boardId) {
		final String SQL="SELECT tc.* FROM apo_topic_carousel AS tc "
				+ "RIGHT OUTER JOIN apo_topic_carousel_board_index AS tcb "
					+ "ON tcb.CAROUSELID = tc.ID "
				+ "WHERE tc.STATUS = ?1 AND tcb.BOARDID = ?2 AND tcb.VOLUMESID = ?3 AND ?4 BETWEEN tcb.ENTRYDATETIME AND tcb.EXPIREDATETIME "
				+ "ORDER BY tcb.ENTRYDATETIME DESC";
		TopicCarousel tc = null;
		try{
			tc = (TopicCarousel)entityManager.createNativeQuery(SQL, TopicCarousel.class)
							.setParameter(1, true)
							.setParameter(2, boardId)
							.setParameter(3, boardGroupId)
							.setParameter(4, DateTimeUtils.getRFC3339(LocalDateTime.now()))
							.getSingleResult();
		}catch(javax.persistence.NoResultException e) {
			if(logger.isDebugEnabled()){
				logger.debug(e.getMessage(), e);
			}
		}
		return Optional.ofNullable(tc);
	}

	@Override
	public Optional<TopicCarousel> findOneByBoard(int boardGroupId) {
		return findOneByBoard(boardGroupId, 0);
	}

	@Override
	public Stream<BoardCarouselIndex> findAllByCarousel(int topicCarouselId) {
		return entityManager.createQuery("SELECT bc FROM BoardCarouselIndex bc WHERE bc.carouselId = ?1", BoardCarouselIndex.class).setParameter(1, topicCarouselId).getResultStream();
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public Optional<Boolean> deleteBoardCarouselIndex(int topicCarouselId, int boardGroupId, long boardId) {
		int affect = 0;
		try{
			affect = entityManager.createQuery("DELETE FROM BoardCarouselIndex bci WHERE bci.carouselId = ?1 AND bci.volumesId = ?2 AND bci.boardId = ?3")
						.setParameter(1, topicCarouselId)
						.setParameter(2, boardGroupId)
						.setParameter(3, boardId)
						.executeUpdate();
		}catch(Exception e){
			if(logger.isDebugEnabled()){
				logger.debug(e.getMessage(), e);
			}
		}
		return affect == 1?Optional.of(true):Optional.empty();
	}

}
