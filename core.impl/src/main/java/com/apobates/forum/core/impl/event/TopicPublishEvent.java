package com.apobates.forum.core.impl.event;

import org.springframework.context.ApplicationEvent;
import com.apobates.forum.core.entity.Topic;
/**
 * 话题发布事件
 * 
 * @author xiaofanku
 * @since 20190703
 */
public class TopicPublishEvent extends ApplicationEvent{
	private static final long serialVersionUID = 5439566453466799789L;
	private final Topic topic;
	private final String userAgent;
	
	public TopicPublishEvent(Object source, Topic topic, String userAgent) {
		super(source);
		this.topic = topic;
		this.userAgent = userAgent;
	}

	public Topic getTopic(){
		return topic;
	}

	public String getUserAgent() {
		return userAgent;
	}
}
