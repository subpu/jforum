package com.apobates.forum.core.impl.service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.apobates.forum.attention.core.ImagePathCoverter;
import com.apobates.forum.core.api.ImageIOMeta;
import com.apobates.forum.core.api.dao.AlbumDao;
import com.apobates.forum.core.api.dao.AlbumPictureDao;
import com.apobates.forum.core.api.service.AlbumPictureService;
import com.apobates.forum.core.entity.Album;
import com.apobates.forum.core.entity.AlbumPicture;
import com.apobates.forum.utils.lang.BoundedTreeSet;

@Service
public class AlbumPictureServiceImpl implements AlbumPictureService{
	@Autowired
	private AlbumPictureDao albumPictureDao;
	@Autowired
	private AlbumDao albumDao;
	
	@Override
	public Stream<AlbumPicture> getAll(long albumId) {
		return albumPictureDao.findAllByAlbum(albumId);
	}

	@Override
	public Stream<AlbumPicture> getAll(Set<Long> albumIdSet, ImageIOMeta imageIO, String scale, String defaultPicture) {
		if(albumIdSet == null || albumIdSet.isEmpty()){
			return Stream.empty();
		}
		List<Long> albumIds = albumIdSet.stream().filter(albumId -> albumId > 0).collect(Collectors.toList());
		Consumer<AlbumPicture> action = ap->{
			//上传图片的路径转换
			String imagePath = new ImagePathCoverter(ap.getLink())
					.decodeUploadImageFilePath(imageIO.getImageBucketDomain(), imageIO.getUploadImageDirectName(), scale)
					.orElse(defaultPicture); //"/static/img/140x140.png"
			ap.setLink(imagePath);
		};
		
		return albumPictureDao.findAllByAlbum(albumIds).peek(action);
	}

	@Override
	public Map<Long, TreeSet<AlbumPicture>> getAll(Set<Long> albumIdSet, ImageIOMeta imageIO, String scale, String defaultPicture, int showSize) {
		if(albumIdSet == null || albumIdSet.isEmpty()){
			return Collections.emptyMap();
		}
		List<Long> albumIds = albumIdSet.stream().filter(albumId -> albumId > 0).collect(Collectors.toList());
		Consumer<AlbumPicture> action = (ap) -> {
			String imageThumbPath = new ImagePathCoverter(ap.getLink()).decodeUploadImageFilePath(imageIO.getImageBucketDomain(), imageIO.getUploadImageDirectName(), scale).orElse(defaultPicture); //"/static/img/140x140.png"
			ap.setThumb(imageThumbPath);
			String imageOriginalPath = new ImagePathCoverter(ap.getLink()).decodeUploadImageFilePath(imageIO.getImageBucketDomain(), imageIO.getUploadImageDirectName(), "auto").orElse(defaultPicture);
			ap.setLink(imageOriginalPath);
		};
		
		return albumPictureDao.findAllByAlbum(albumIds).peek(action).collect(Collectors.groupingBy(AlbumPicture::getAlbumId, Collectors.toCollection(()->new BoundedTreeSet<>(showSize))));//限制数量
	}

	@Override
	public Optional<AlbumPicture> get(long id) {
		return albumPictureDao.findOne(id);
	}

	@Override
	public int delete(long id) {
		return albumPictureDao.delete(Arrays.asList(id));
	}

	@Override
	public int delete(Set<Long> idSet) {
		if(idSet==null || idSet.isEmpty()){
			return -1;
		}
		return albumPictureDao.delete(idSet);
	}

	@Override
	public long create(long albumId, String encodeImageAddr, String caption, int ranking, boolean status, boolean cover)throws IllegalStateException {
		if(encodeImageAddr == null || encodeImageAddr.isEmpty()){
			throw new  IllegalStateException("图片地址不可用或不被接受");
		}
		Album data = albumDao.findOne(albumId).orElseThrow(()->new  IllegalStateException("像册不存在"));
		AlbumPicture ap = new AlbumPicture(data, encodeImageAddr, caption, status, ranking, cover);
		try{
			albumPictureDao.save(ap);
			if(ap.getId()>0){
				return ap.getId();
			}
		}catch(Exception e){
			throw new  IllegalStateException(e.getMessage());
		}
		throw new  IllegalStateException("为像册增加图片失败");
	}

	@Override
	public Optional<AlbumPicture> create(long albumId, String encodeImageAddr, String caption, int ranking, boolean status)throws IllegalStateException{
		if(encodeImageAddr == null || encodeImageAddr.isEmpty()){
			throw new  IllegalStateException("图片地址不可用或不被接受");
		}
		Album album = albumDao.findOne(albumId).orElseThrow(()->new  IllegalStateException("像册不存在"));
		AlbumPicture ap = new AlbumPicture(album, encodeImageAddr, caption, status, ranking, false);
		try{
			album.getPictures().add(ap);
			albumDao.edit(album);
			return Optional.of(ap);
		}catch(Exception e){
			throw new  IllegalStateException(e.getMessage());
		}
	}
}
