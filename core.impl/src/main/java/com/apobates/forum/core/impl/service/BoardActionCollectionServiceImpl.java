package com.apobates.forum.core.impl.service;

import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.apobates.forum.core.api.dao.BoardActionCollectionDao;
import com.apobates.forum.core.api.service.BoardActionCollectionService;
import com.apobates.forum.core.entity.BoardActionCollection;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;

@Service
public class BoardActionCollectionServiceImpl implements BoardActionCollectionService{
	@Autowired
	private BoardActionCollectionDao boardActionCollectionDao;
	
	@Override
	public Stream<BoardActionCollection> getRecentByMember(long memberId, int size) {
		return boardActionCollectionDao.findRecentByMember(memberId, size);
	}

	@Override
	public Page<BoardActionCollection> getByMember(long memberId, Pageable pageable) {
		return boardActionCollectionDao.findAllByMember(memberId, pageable);
	}

	@Override
	public Page<BoardActionCollection> getAll(Pageable pageable) {
		return boardActionCollectionDao.findAll(pageable);
	}

	@Override
	public Stream<BoardActionCollection> getMemberFavoriteBoard(long memberId, int size) {
		return boardActionCollectionDao.findAllMemberFavoriteBoard(memberId, size);
	}

	@Override
	public Page<BoardActionCollection> getByBoard(long boardId, Pageable pageable) {
		return boardActionCollectionDao.findAllByBoard(boardId, pageable);
	}

	@Override
	public Page<BoardActionCollection> getByBoard(long boardId, ForumActionEnum action, Pageable pageable) {
		return boardActionCollectionDao.findAllByBoard(boardId, action, pageable);
	}

}
