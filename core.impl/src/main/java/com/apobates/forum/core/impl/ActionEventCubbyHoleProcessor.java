package com.apobates.forum.core.impl;

import com.apobates.forum.core.api.dao.BoardActionCollectionDao;
import com.apobates.forum.core.api.dao.TopicActionCollectionDao;
import com.apobates.forum.core.entity.ActionEventDescriptor;
import com.apobates.forum.core.entity.BoardActionCollection;
import com.apobates.forum.core.entity.TopicActionCollection;
import com.apobates.forum.utils.cache.AbstractCubbyHole;
import com.apobates.forum.utils.cache.CubbyHoleProcessor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author xiaofanku
 */
public class ActionEventCubbyHoleProcessor implements CubbyHoleProcessor<ActionEventDescriptor>{
    @Autowired
    private BoardActionCollectionDao boardActionCollectionDao;
    @Autowired
    private TopicActionCollectionDao topicActionCollectionDao;
    
    @Override
    public Set<String> process(Collection<ActionEventDescriptor> action) {
        Supplier<Collection<ActionEventDescriptor>> s = ()->action;
        List<TopicActionCollection> tas = s.get().stream().filter(aed -> aed instanceof TopicActionCollection).map (ta -> (TopicActionCollection) ta).collect(Collectors.toList());
        List<BoardActionCollection> bas = s.get().stream().filter(aed -> aed instanceof BoardActionCollection).map (ta -> (BoardActionCollection) ta).collect(Collectors.toList());
        boardActionCollectionDao.batchSave(bas);
        topicActionCollectionDao.batchSave(tas);
        List<ActionEventDescriptor> data = new ArrayList<>(tas);
        data.addAll(bas);
        return data.stream().filter(ta->ta.getId()>0).map(AbstractCubbyHole::toChecksum).collect(Collectors.toSet());
    }
}
