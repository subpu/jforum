package com.apobates.forum.core.impl.dao;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.apobates.forum.core.api.dao.BoardDao;
import com.apobates.forum.core.entity.Board;
import com.apobates.forum.core.entity.BoardGroup;
import com.apobates.forum.core.entity.ForumEntityStatusEnum;
import com.apobates.forum.utils.Commons;

@Repository
public class BoardDaoImpl implements BoardDao{
	@PersistenceContext
	private EntityManager entityManager;
	private final static Logger logger = LoggerFactory.getLogger(BoardDaoImpl.class);
	
	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public void save(Board entity) {
		entityManager.persist(entity);
	}

	@Override
	public Optional<Board> findOne(Long primaryKey) {
		Board b = entityManager.find(Board.class, primaryKey);
		return Optional.ofNullable(b);
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public Optional<Boolean> edit(Board updateEntity) {
		try {
			entityManager.merge(updateEntity);
			return Optional.of(true);
		}catch(Exception e) {
			if(logger.isDebugEnabled()){
				logger.debug(e.getMessage(), e);
			}
		}
		return Optional.empty();
	}

	@Override
	public Stream<Board> findAll() {
		return entityManager.createQuery("SELECT b FROM Board b WHERE b.origin = ?1", Board.class).setParameter(1, false).getResultStream();
	}

	@Override
	public Stream<Board> findUsed() {
		return entityManager.createQuery("SELECT b FROM Board b WHERE b.status != ?1 AND b.origin = ?2", Board.class)
				.setParameter(1, ForumEntityStatusEnum.DELETE)
				.setParameter(2, false)
				.getResultStream();
	}

	@Override
	public long count() {
		return entityManager.createQuery("SELECT COUNT(b) FROM Board b WHERE b.origin = ?1", Long.class).setParameter(1, false).getSingleResult();
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public Optional<Boolean> edit(long id, ForumEntityStatusEnum status) {
		int affect=0;
		try {
			affect = entityManager.createQuery("UPDATE Board b SET b.status = ?1 WHERE b.id = ?2").setParameter(1, status).setParameter(2, id).executeUpdate();
		}catch(Exception e) {
			if(logger.isDebugEnabled()){
				logger.debug(e.getMessage(), e);
			}
		}
		return affect==1?Optional.of(true):Optional.empty();//.failure("更新失败");
	}

	@Override
	public Stream<Board> findAllByBoardGroup(int boardGroupId) {
		return entityManager.createQuery("SELECT b FROM Board b WHERE b.volumesId = ?1 AND b.origin = ?2", Board.class)
				.setParameter(1, boardGroupId)
				.setParameter(2, false)
				.getResultStream();
	}

	@Override
	public Stream<Board> findUsed(int boardGroupId) {
		return entityManager.createQuery("SELECT b FROM Board b WHERE b.volumesId = ?1 AND b.status != ?2 AND b.origin = ?3", Board.class)
				.setParameter(1, boardGroupId)
				.setParameter(2, ForumEntityStatusEnum.DELETE)
				.setParameter(3, false)
				.getResultStream();
	}

	@Override
	public Board findOneForIndex(long id) {
		Board b = entityManager.find(Board.class, id);
		if(b==null) {
			return null;
		}
		if(b.getVolumesId()>0) {
			try {
				BoardGroup bg = entityManager.createQuery("SELECT bg FROM BoardGroup bg WHERE bg.id = ?1", BoardGroup.class).setParameter(1, b.getVolumesId()).getSingleResult();
				if(bg!=null) {
					b.setVolumes(bg);
				}
			}catch(javax.persistence.NoResultException e) {
				if(logger.isDebugEnabled()){
					logger.debug(e.getMessage(), e);
				}
			}
		}else {
			b.setVolumes(BoardGroup.getDefault());
		}
		return b;
	}

	@Override
	public Stream<Board> findAllById(Collection<Long> ids) {
		if(ids == null || ids.isEmpty()){
			return Stream.empty();
		}
		return entityManager.createQuery("SELECT b FROM Board b WHERE b.origin = ?1 AND b.id IN ?2", Board.class).setParameter(1, false).setParameter(2, ids).getResultStream();
	}
	/*子栏目(原生的版块)*/
	@Override
	public List<Board> findAllTermByOriginId(int sectionId) {
		return entityManager.createQuery("SELECT b FROM Board b WHERE b.origin = ?1 AND b.volumesId = ?2 ORDER BY b.id ASC", Board.class).setParameter(1, true).setParameter(2, sectionId).getResultList();
	}

	@Override
	public List<Board> findAllTerm() {
		return entityManager.createQuery("SELECT b FROM Board b WHERE b.origin = ?1 ORDER BY b.id ASC", Board.class).setParameter(1, true).getResultList();
	}

	@Override
	public Optional<Board> findOneTermByDirectoryNames(String directoryNames) {
		Board b = null;
		try{
			b = entityManager.createQuery("SELECT b FROM Board b WHERE b.origin = ?1 AND b.directoryNames = ?2", Board.class).setParameter(1, true).setParameter(2, directoryNames).getSingleResult();
		}catch(javax.persistence.NoResultException e){
			if(logger.isDebugEnabled()){
				logger.debug(e.getMessage(), e);
			}
		}
		return Optional.ofNullable(b);
	}
	@Override
	public Board findOneTermAssSection(String directoryNames) {
		Board b = null;
		try{
			b = entityManager.createQuery("SELECT b FROM Board b WHERE b.origin = ?1 AND b.directoryNames = ?2", Board.class).setParameter(1, true).setParameter(2, directoryNames).getSingleResult();
		}catch(javax.persistence.NoResultException e){
			return null;
		}
		if(b.getVolumesId()>0){
			try {
				BoardGroup bg = entityManager.createQuery("SELECT bg FROM BoardGroup bg WHERE bg.origin =?1 AND bg.id = ?2", BoardGroup.class).setParameter(1, true).setParameter(2, b.getVolumesId()).getSingleResult();
				if(bg!=null) {
					b.setVolumes(bg);
				}
			}catch(javax.persistence.NoResultException e) {
				if(logger.isDebugEnabled()){
					logger.debug(e.getMessage(), e);
				}
			}
		}
		return b;
	}

	@Override
	public Stream<Board> findAllTermById(Collection<Long> ids) {
		if(ids == null || ids.isEmpty()){
			return Stream.empty();
		}
		return entityManager.createQuery("SELECT b FROM Board b WHERE b.origin = ?1 AND b.id IN ?2", Board.class).setParameter(1, true).setParameter(2, ids).getResultStream();
	}

	@Override
	public long existsTermDirectoryNames(String directoryNames) {
		if(!Commons.isNotBlank(directoryNames)){
			return -1L;
		}
		try {
			Long termId = entityManager.createQuery("SELECT b.id FROM Board b WHERE b.origin = ?1 AND b.directoryNames = ?2", Long.class)
					.setParameter(1, true)
					.setParameter(2, directoryNames)
					.getSingleResult();
			return termId;
		} catch (javax.persistence.NoResultException e) {
			return 0L;
		}
	}

	@Override
	public long existsTermDirectoryNames(String directoryNames, long termId) {
		if(!Commons.isNotBlank(directoryNames)){
			return -1L;
		}
		try{
			return entityManager.createQuery("SELECT COUNT(b) FROM Board b WHERE b.origin = ?1 AND b.directoryNames = ?2 AND b.id != ?3", Long.class)
					.setParameter(1, true)
					.setParameter(2, directoryNames)
					.setParameter(3, termId)
					.getSingleResult();
		}catch (Exception e) {
			return 0L;
		}
	}

	@Override
	public Optional<Board> findOneTermById(long termId) {
		Board term = entityManager.createQuery("SELECT b FROM Board b WHERE b.id = ?1 AND b.origin = ?2", Board.class).setParameter(1, termId).setParameter(2, true).getSingleResult();
		return Optional.ofNullable(term);
	}
	
}
