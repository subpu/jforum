package com.apobates.forum.core.impl.service;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.apobates.forum.core.api.dao.PostsDescriptorDao;
import com.apobates.forum.core.api.service.PostsDescriptorService;
import com.apobates.forum.core.entity.PostsDescriptor;

@Service
public class PostsDescriptorServiceImpl implements PostsDescriptorService{
	@Autowired
	private PostsDescriptorDao postsDescriptorDao;

	@Override
	public Optional<PostsDescriptor> get(long postsId) {
		return postsDescriptorDao.findOne(postsId);
	}

	@Override
	public Stream<PostsDescriptor> getAll(Set<Long> postsIdSet) {
		if (null == postsIdSet || postsIdSet.isEmpty()) {
			return Stream.empty();
		}
		return postsDescriptorDao.findAllByPosts(postsIdSet);
	}
}
