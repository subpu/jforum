package com.apobates.forum.core.impl.event.listener;

import java.util.Optional;

import com.apobates.forum.core.impl.event.TopicPublishEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import com.apobates.forum.core.api.dao.TopicConfigDao;
import com.apobates.forum.core.entity.Topic;
import com.apobates.forum.core.entity.TopicConfig;
/**
 * 话题创建事件侦听器.创建版块配置文件
 * 即使失败也可通过话题重塑
 * 
 * @author xiaofanku
 * @since 20190703
 */
@Component
public class TopicConfigListener implements ApplicationListener<TopicPublishEvent>{
	@Autowired
	private TopicConfigDao topicConfigDao;
	private final static Logger logger = LoggerFactory.getLogger(TopicConfigListener.class);
	
	@Override
	public void onApplicationEvent(TopicPublishEvent event) {
		logger.info("[Topic][PublishEvent][1]话题配置文件开始执行");
		// 话题配置
		Topic topic = event.getTopic();
		TopicConfig tc = Optional.ofNullable(topic.getConfigure()).orElse(TopicConfig.defaultConfig(topic.getId()));
		topicConfigDao.save(tc);
		logger.info("[Topic][PublishEvent][1]话题配置文件执行结束");
	}
}
