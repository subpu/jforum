package com.apobates.forum.core.impl.service;

import java.util.Optional;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.apobates.forum.core.api.dao.TopicCategoryItemDao;
import com.apobates.forum.core.api.service.TopicCategoryItemService;
import com.apobates.forum.core.entity.TopicCategoryItem;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;

@Service
public class TopicCategoryItemServiceImpl implements TopicCategoryItemService{
	@Autowired
	private TopicCategoryItemDao topicCategoryItemDao;

	@Override
	public Page<TopicCategoryItem> getAll(Pageable pageable) {
		return topicCategoryItemDao.findAll(pageable);
	}

	@Override
	public Optional<TopicCategoryItem> get(long id) {
		return topicCategoryItemDao.findOne(id);
	}

	@Override
	public Stream<TopicCategoryItem> getAll(int topicCategoryId) {
		return topicCategoryItemDao.findAllByCategory(topicCategoryId);
	}
}
