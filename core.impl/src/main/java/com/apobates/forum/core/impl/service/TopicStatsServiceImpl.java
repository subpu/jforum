package com.apobates.forum.core.impl.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.apobates.forum.core.api.dao.TopicStatsDao;
import com.apobates.forum.core.api.service.TopicStatsService;
import com.apobates.forum.core.entity.TopicStats;

@Service
public class TopicStatsServiceImpl implements TopicStatsService{
	@Autowired
	private TopicStatsDao topicStatsDao;
	
	@Override
	public Optional<TopicStats> getByTopic(long topicId) {
		return topicStatsDao.findOneByTopic(topicId);
	}

	@Override
	public Stream<TopicStats> getByTopicIdList(List<Long> topicIdList) {
		if(topicIdList == null || topicIdList.isEmpty()){
			return Stream.empty();
		}
		return topicStatsDao.findAllByTopicId(topicIdList);
	}
}
