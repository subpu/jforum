package com.apobates.forum.core.impl.event;

import org.springframework.context.ApplicationEvent;
import com.apobates.forum.core.entity.BoardModeratorRoleHistory;
import com.apobates.forum.member.entity.MemberRoleEnum;

/**
 * 版主卸任事件
 * 
 * @author xiaofanku
 * @since 20200831
 */
public class ModeratorRecallEvent extends ApplicationEvent{
    //卸任后会员的角色
    private final MemberRoleEnum updateRole;
    //卸任相关的角色变更历史(上任时生成的)
    private final BoardModeratorRoleHistory removeRoleHistory;
    
    public ModeratorRecallEvent(Object source, BoardModeratorRoleHistory removeRoleHistory, MemberRoleEnum updateRole) {
        super(source);
        this.updateRole = updateRole;
        this.removeRoleHistory = removeRoleHistory;
    }
    
    public MemberRoleEnum getUpdateRole() {
        return updateRole;
    }
    
    public BoardModeratorRoleHistory getRemoveRoleHistory() {
        return removeRoleHistory;
    }
}
