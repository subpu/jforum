package com.apobates.forum.core.impl;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import com.apobates.forum.core.entity.ActionEventDescriptor;
import com.apobates.forum.utils.cache.AbstractCubbyHole;
import com.apobates.forum.utils.cache.CubbyHoleLinkedDeque;
import com.apobates.forum.utils.cache.CubbyHoleProcessor;
import org.springframework.context.annotation.ComponentScan.Filter;

@Configuration
@EnableCaching
@ComponentScan(
		basePackages={"com.apobates.forum.core.impl"}, 
		useDefaultFilters=false, 
		includeFilters={
				@Filter(classes={org.springframework.stereotype.Service.class}),
				@Filter(classes={org.springframework.stereotype.Repository.class}),
				@Filter(classes={org.springframework.stereotype.Component.class})})
public class CoreAppConfig{

	@Bean
	public CubbyHoleProcessor<ActionEventDescriptor> getCubbyHoleProcessor() {
		return new ActionEventCubbyHoleProcessor();
	}

	@Bean("cubbyHole")
	public AbstractCubbyHole<ActionEventDescriptor> getCubbyHole(
			CubbyHoleProcessor<ActionEventDescriptor> cubbyHoleProcessor) {
		return new CubbyHoleLinkedDeque<>(cubbyHoleProcessor);
	}
}
