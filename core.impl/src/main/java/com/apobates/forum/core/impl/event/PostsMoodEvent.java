package com.apobates.forum.core.impl.event;

import org.springframework.context.ApplicationEvent;
import com.apobates.forum.core.entity.PostsMoodRecords;
/**
 * 回复喜好记录创建事件
 * 
 * @author xiaofanku
 * @since 20191116
 */
public class PostsMoodEvent extends ApplicationEvent{
	private static final long serialVersionUID = 4538344876689268099L;
	private final PostsMoodRecords records;
	
	public PostsMoodEvent(Object source, PostsMoodRecords records) {
		super(source);
		this.records = records;
	}

	public PostsMoodRecords getRecords() {
		return records;
	}
}
