package com.apobates.forum.core.impl.event.listener;

import com.apobates.forum.core.impl.event.PostsPublishEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import com.apobates.forum.core.api.dao.BoardStatsDao;
import com.apobates.forum.core.api.dao.TopicStatsDao;
import com.apobates.forum.core.entity.Posts;

/**
 * 回复创建侦听器,更新版块和话题的统计
 * @author xiaofanku
 * @since 20190703
 */
@Component
public class PostsStatsListener implements ApplicationListener<PostsPublishEvent>{
	@Autowired
	private TopicStatsDao topicStatsDao;
	@Autowired
	private BoardStatsDao boardStatsDao;
	private final static Logger logger = LoggerFactory.getLogger(PostsStatsListener.class);
	
	@Override
	public void onApplicationEvent(PostsPublishEvent event) {
		logger.info("[Posts][PublishEvent][1]回复创建后, 执行统计文件更新");
		Posts posts = event.getPosts();
		//统计{版块:回复数+1,}
		boardStatsDao.updatePosts(posts.getBoardId());
		//统计{话题: 回复数+1}
		//话题的最近回复{topic.rankingDateTime=}
		topicStatsDao.updatePosts(posts.getTopicId(), posts.getMemberId(), posts.getMemberNickname(), posts.getEntryDateTime(), posts.getId());
		logger.info("[Posts][PublishEvent][1]执行回复相关的统计更新结束");
	}
}
