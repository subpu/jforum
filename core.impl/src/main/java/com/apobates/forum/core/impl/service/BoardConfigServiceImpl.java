package com.apobates.forum.core.impl.service;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import com.apobates.forum.core.api.dao.BoardConfigDao;
import com.apobates.forum.core.api.service.BoardConfigService;
import com.apobates.forum.core.entity.BoardConfig;

@Service
public class BoardConfigServiceImpl implements BoardConfigService{
	@Autowired
	private BoardConfigDao boardConfigDao;
	
	@Cacheable(value="boardCache", key="'board_config_'+#boardId", unless="#result==null")
	@Override
	public Optional<BoardConfig> getByBoardId(long boardId) { //[BC]B2
		return boardConfigDao.findOneByBoard(boardId);
	}

	@Override
	public Optional<Boolean> create(long boardId, BoardConfig config)throws IllegalStateException {
		try {
			config.setBoardId(boardId);
			boardConfigDao.save(config);
			if(config.getId()>0){
				return Optional.of(true);
			}
			throw new IllegalStateException("版块配置文件创建失败");
		}catch(Exception e) {
			throw new IllegalStateException(e.getMessage());
		}
	}

}
