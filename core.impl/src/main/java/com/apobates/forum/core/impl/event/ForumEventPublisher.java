package com.apobates.forum.core.impl.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
/**
 * 论坛核心模块事件发布器
 * 
 * @author xiaofanku
 * @since 20190703
 */
@Component
public class ForumEventPublisher {
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;
    
    /**
     * 发布版块事件
     * @param boardCreateEvent
     */
    public void publishBoardEvent(BoardCreateEvent boardCreateEvent) {
        applicationEventPublisher.publishEvent(boardCreateEvent);
    }
    
    /**
     * 发布话题事件
     * @param topicPublishEvent
     */
    public void publishTopicEvent(TopicPublishEvent topicPublishEvent) {
        applicationEventPublisher.publishEvent(topicPublishEvent);
    }
    
    /**
     * 发布回复事件
     * @param postsPublishEvent
     */
    public void publishPostsEvent(PostsPublishEvent postsPublishEvent) {
        applicationEventPublisher.publishEvent(postsPublishEvent);
    }
    
    /**
     * 发布话题移动事件
     * @param moveTopicEvent
     */
    public void publishMoveTopicEvent(TopicMoveEvent moveTopicEvent){
        applicationEventPublisher.publishEvent(moveTopicEvent);
    }
    
    /**
     * 发布喜好创建事件
     * @param postsPublishEvent
     */
    public void publishPostsLikeEvent(PostsMoodEvent postsMoodEvent){
        applicationEventPublisher.publishEvent(postsMoodEvent);
    }
    
    /**
     * 插件类话题创建事件
     * @param plugTopicEvent
     */
    public void publishPlugTopicEvent(PlugTopicPublishEvent plugTopicEvent){
        applicationEventPublisher.publishEvent(plugTopicEvent);
    }
    
    /**
     * 发布版主新生事件
     * @param moderatorBornEvent
     */
    public void buildModeratorBornEvent(ModeratorBornEvent moderatorBornEvent){
        applicationEventPublisher.publishEvent(moderatorBornEvent);
    }
    
    /**
     * 发布版主卸任事件
     * @param moderatorRecallEvent 
     */
    public void buildModeratorRecallEvent(ModeratorRecallEvent moderatorRecallEvent){
        applicationEventPublisher.publishEvent(moderatorRecallEvent);
    }
}
