package com.apobates.forum.core.impl.event;

import org.springframework.context.ApplicationEvent;

import com.apobates.forum.core.entity.Posts;
/**
 * 回复创建事件
 * 
 * @author xiaofanku
 * @since 20190703
 */
public class PostsPublishEvent extends ApplicationEvent{
	private static final long serialVersionUID = -7693392703940046401L;
	private final Posts posts;
	private final String userAgent;
	
	public PostsPublishEvent(Object source, Posts posts, String userAgent) {
		super(source);
		this.posts = posts;
		this.userAgent = userAgent;
	}

	public Posts getPosts(){
		return posts;
	}

	public String getUserAgent() {
		return userAgent;
	}
	
}
