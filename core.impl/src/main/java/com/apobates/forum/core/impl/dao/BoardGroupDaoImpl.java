package com.apobates.forum.core.impl.dao;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.apobates.forum.core.api.dao.BoardGroupDao;
import com.apobates.forum.core.entity.BoardGroup;
import com.apobates.forum.utils.Commons;

@Repository
public class BoardGroupDaoImpl implements BoardGroupDao{
	@PersistenceContext
	private EntityManager entityManager;
	private final static Logger logger = LoggerFactory.getLogger(BoardGroupDaoImpl.class);

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public void save(BoardGroup entity) {
		entityManager.persist(entity);
	}

	@Override
	public Optional<BoardGroup> findOne(Integer primaryKey) {
		BoardGroup bg = entityManager.find(BoardGroup.class, primaryKey);
		return Optional.ofNullable(bg);
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public Optional<Boolean> edit(BoardGroup updateEntity) {
		try {
			entityManager.merge(updateEntity);
			return Optional.of(true);
		}catch(Exception e) {
			if(logger.isDebugEnabled()){
				logger.debug(e.getMessage(), e);
			}
		}
		return Optional.empty();
	}

	@Override
	public Stream<BoardGroup> findAll() {
		return entityManager.createQuery("SELECT bg FROM BoardGroup bg WHERE bg.origin = ?1", BoardGroup.class).setParameter(1, false).getResultStream();
	}

	@Override
	public List<BoardGroup> findUsed() {
		return entityManager.createQuery("SELECT bg FROM BoardGroup bg WHERE bg.status = ?1 AND bg.origin = ?2", BoardGroup.class).setParameter(1, true).setParameter(2, false).getResultList();
	}

	@Override
	public long count() {
		return -1L;
	}

	@Override
	public Optional<BoardGroup> findOneByBoard(long boardId) {
		try {
			BoardGroup bg = (BoardGroup) entityManager.createNativeQuery("SELECT bg.* FROM apo_board AS b JOIN apo_board_group AS bg ON b.VOLUMESID= bg.ID WHERE b.ID = ?1", BoardGroup.class).setParameter(1, boardId).getSingleResult();
			return Optional.ofNullable(bg);
		}catch(javax.persistence.NoResultException e) {
			if(logger.isDebugEnabled()){
				logger.debug(e.getMessage(), e);
			}
		}
		return Optional.empty();
	}

	@Override
	public Stream<BoardGroup> findAllById(Collection<Integer> idList) {
		if(idList == null || idList.isEmpty()){
			return Stream.empty();
		}
		return entityManager.createQuery("SELECT bg FROM BoardGroup bg WHERE bg.origin = ?1 AND bg.id IN ?2", BoardGroup.class).setParameter(1, false).setParameter(2, idList).getResultStream();
	}
	/*栏目(原生的版块组)*/
	@Override
	public Optional<BoardGroup> findOneOriginByDirectoryNames(String directoryNames) {
		BoardGroup bg = null;
		try{
			bg = entityManager.createQuery("SELECT bg FROM BoardGroup bg WHERE bg.origin = ?1 AND bg.directoryNames = ?2", BoardGroup.class).setParameter(1, true).setParameter(2, directoryNames).getSingleResult();
		}catch(javax.persistence.NoResultException e){
			if(logger.isDebugEnabled()){
				logger.debug(e.getMessage(), e);
			}
		}
		return Optional.ofNullable(bg);
	}

	@Override
	public Stream<BoardGroup> findAllOrigin() {
		return entityManager.createQuery("SELECT bg FROM BoardGroup bg WHERE bg.origin = ?1", BoardGroup.class).setParameter(1, true).getResultStream();
	}

	@Override
	public Stream<BoardGroup> findAllOriginById(Collection<Integer> idList) {
		if(idList == null || idList.isEmpty()){
			return Stream.empty();
		}
		return entityManager.createQuery("SELECT bg FROM BoardGroup bg WHERE bg.origin = ?1 AND bg.id IN ?2", BoardGroup.class).setParameter(1, true).setParameter(2, idList).getResultStream();
	}

	@Override
	public int existsOriginDirectoryNames(String directoryNames) {
		if(!Commons.isNotBlank(directoryNames)){
			return -1;
		}
		try {
			Integer originId = entityManager.createQuery("SELECT bg.id FROM BoardGroup bg WHERE bg.origin = ?1 AND bg.directoryNames = ?2", Integer.class)
					.setParameter(1, true)
					.setParameter(2, directoryNames)
					.getSingleResult();
			return originId;
		} catch (javax.persistence.NoResultException e) {
			return 0;
		}
	}
	
	@Override
	public long existsOriginDirectoryNames(String directoryNames, int sectionId) {
		if(!Commons.isNotBlank(directoryNames)){
			return -1L;
		}
		try {
			return entityManager.createQuery("SELECT COUNT(bg) FROM BoardGroup bg WHERE bg.origin = ?1 AND bg.directoryNames = ?2 AND bg.id != ?3", Long.class)
					.setParameter(1, true)
					.setParameter(2, directoryNames)
					.setParameter(3, sectionId)
					.getSingleResult();
		} catch (javax.persistence.NoResultException e) {
			return 0L;
		}
	}
	
	@Override
	public Optional<BoardGroup> findOneSectionById(int sectionId) {
		BoardGroup section = entityManager.createQuery("SELECT bg FROM BoardGroup bg WHERE bg.id = ?1 AND bg.origin = ?2", BoardGroup.class).setParameter(1, sectionId).setParameter(2, true).getSingleResult();
		return Optional.ofNullable(section);
	}
}
