package com.apobates.forum.core.impl.event.listener;

import com.apobates.forum.core.impl.event.TopicPublishEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import com.apobates.forum.core.api.dao.TopicTagDao;
import com.apobates.forum.core.entity.Topic;
/**
 * 保存话题的标签
 * 即使失败也可通过话题内容重塑
 * 
 * @author xiaofanku
 * @since 20190819
 */
@Component
public class TopicTagListener implements ApplicationListener<TopicPublishEvent>{
	@Autowired
	private TopicTagDao topicTagDao;
	private final static Logger logger = LoggerFactory.getLogger(TopicTagListener.class);
	
	@Override
	public void onApplicationEvent(TopicPublishEvent event) {
		logger.info("[Topic][PublishEvent][3]话题标签处理开始");
		Topic topic = event.getTopic();
		if(null == topic.getTages() || topic.getTages().isEmpty()){
			logger.info("[Topic][PublishEvent][3]话题标签是空");
			return;
		}
		//所有标签
		topicTagDao.batchSave(topic.getId(), topic.getTages());
		logger.info("[Topic][PublishEvent][3]话题标签处理结束");
	}
}
