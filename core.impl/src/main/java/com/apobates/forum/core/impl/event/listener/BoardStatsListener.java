package com.apobates.forum.core.impl.event.listener;

import com.apobates.forum.core.impl.event.BoardCreateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import com.apobates.forum.core.api.dao.BoardStatsDao;
import com.apobates.forum.core.entity.Board;
/**
 * 版块统计侦听器,创建版块的统计数据
 * @author xiaofanku
 * @since 20190703
 */
@Component
public class BoardStatsListener implements ApplicationListener<BoardCreateEvent>{
	@Autowired
	private BoardStatsDao boardStatsDao;
	private final static Logger logger = LoggerFactory.getLogger(BoardStatsListener.class);
	
	@Override
	public void onApplicationEvent(BoardCreateEvent event) {
		logger.info("[Board][CreateEvent][1]版块统计文件开始执行");
		Board board = event.getBoard();
		//版块统计
		boardStatsDao.createStats(board.getId(), board.getVolumesId());
		logger.info("[Board][CreateEvent][1]版块统计文件执行结束");
	}
}
