package com.apobates.forum.core.impl.event.listener;

import com.apobates.forum.core.impl.event.TopicPublishEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.apobates.forum.core.api.dao.BoardStatsDao;
import com.apobates.forum.core.api.dao.TopicStatsDao;
import com.apobates.forum.core.entity.Topic;
/**
 * 话题创建事件侦听器.创建话题统计文件并更新版块的统计
 * 即使失败也可通过话题重塑
 * 
 * @author xiaofanku
 * @since 20190703
 */
@Component
public class TopicStatsListener implements ApplicationListener<TopicPublishEvent>{
	@Autowired
	private TopicStatsDao topicStatsDao;
	@Autowired
	private BoardStatsDao boardStatsDao;
	private final static Logger logger = LoggerFactory.getLogger(TopicStatsListener.class);
	
	@Override
	public void onApplicationEvent(TopicPublishEvent event) {
		logger.info("[Topic][PublishEvent][2]话题统计和版块统计处理开始");
		//话题统计
		Topic topic = event.getTopic();
		topicStatsDao.createStats(topic.getId(), topic.getVolumesId(), topic.getBoardId());
		boardStatsDao.updateTopic(topic.getBoardId(), topic.getTitle(), topic.getId(), topic.getEntryDateTime(), topic.getMemberId(), topic.getMemberNickname());
		logger.info("[Topic][PublishEvent][2]话题统计和版块统计处理结束");
	}
}
