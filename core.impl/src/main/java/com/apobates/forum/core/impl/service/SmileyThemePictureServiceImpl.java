package com.apobates.forum.core.impl.service;

import java.util.Optional;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.apobates.forum.core.api.dao.SmileyThemeDao;
import com.apobates.forum.core.api.dao.SmileyThemePictureDao;
import com.apobates.forum.core.api.service.SmileyThemePictureService;
import com.apobates.forum.core.entity.SmileyTheme;
import com.apobates.forum.core.entity.SmileyThemePicture;

@Service
public class SmileyThemePictureServiceImpl implements SmileyThemePictureService{
	@Autowired
	private SmileyThemePictureDao smileyThemePictureDao;
	@Autowired
	private SmileyThemeDao smileyThemeDao;
	
	@Override
	public Optional<SmileyThemePicture> get(long id) {
		if(id>0) {
			Optional<SmileyThemePicture> obj = smileyThemePictureDao.findOne(id);
			if(!obj.isPresent()) {
				return Optional.empty();
			}
			SmileyThemePicture stp = obj.get();
			Optional<SmileyTheme> data = smileyThemeDao.findOne(stp.getSmileyThemeId());
			if(data.isPresent()) {
				stp.setSmileyTheme(data.get());
			}
			return Optional.of(stp);
		}
		return Optional.empty();
	}


	@Override
	public Optional<Boolean> edit(long id, SmileyThemePicture updateSmileyThemePicture) throws IllegalStateException{
		SmileyThemePicture stp = get(id).orElseThrow(()->new IllegalStateException("表情图片不存在"));
		stp.setDescription(updateSmileyThemePicture.getDescription());//哈哈
		stp.setFileNames(updateSmileyThemePicture.getFileNames());//03.gif
		stp.setRanking(updateSmileyThemePicture.getRanking());
		stp.setStatus(updateSmileyThemePicture.isStatus());
		
		return smileyThemePictureDao.edit(stp);
	}

	@Override
	public Stream<SmileyThemePicture> get() {
		return smileyThemePictureDao.findAll();
	}

	@Override
	public Stream<SmileyThemePicture> getAllByTheme(int smileyThemeId) {
		return smileyThemePictureDao.findAllByTheme(smileyThemeId);
	}

	@Override
	public Stream<SmileyThemePicture> getAllByTheme(String smileyThemeDirectNames) {
		return smileyThemePictureDao.findAllByTheme(smileyThemeDirectNames);
	}


	@Override
	public Optional<SmileyThemePicture> create(String fileNames, String description, int ranking, boolean status, SmileyTheme smileyTheme)throws IllegalStateException {
		if(smileyTheme == null || smileyTheme.getId() == 0) {
			throw new IllegalStateException("参数不合法或不被接受");
		}
		SmileyThemePicture stp = new SmileyThemePicture(fileNames, description, ranking, status, smileyTheme.getId(), smileyTheme);
		try {
			smileyThemePictureDao.save(stp);
			if(stp.getId()>0) {
				return Optional.of(stp);
			}
		}catch(Exception e) {
			throw new IllegalStateException(e.getMessage());
		}
		throw new IllegalStateException("表情图片语义定义失败");
	}


	@Override
	public Optional<Boolean> modify(String fileName, String fileSemantic, int themeId)throws IllegalStateException {
		SmileyTheme st = smileyThemeDao.findOne(themeId).orElseThrow(()->new IllegalStateException("表情风格不存在"));
		Optional<SmileyThemePicture> obj = smileyThemePictureDao.findOne(themeId, fileName);
		if(obj.isPresent()){
			//更新
			SmileyThemePicture tmp = obj.get();
			tmp.setDescription(fileSemantic);
			return smileyThemePictureDao.edit(tmp);
		}else{
			//创建
			SmileyThemePicture stp = new SmileyThemePicture(fileName, fileSemantic, 1, true, themeId, st);
			try{
				smileyThemePictureDao.save(stp);
				return Optional.of(true);
			}catch(Exception e){
				throw new IllegalStateException(e.getMessage());
			}
		}
	}
	
}
