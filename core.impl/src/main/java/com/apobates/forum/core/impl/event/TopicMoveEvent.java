package com.apobates.forum.core.impl.event;

import java.time.LocalDateTime;

import org.springframework.context.ApplicationEvent;
import com.apobates.forum.core.entity.Board;
import com.apobates.forum.core.entity.Topic;
/**
 * 话题移动事件
 * 
 * @author xiaofanku
 * @since 20191117
 */
public class TopicMoveEvent extends ApplicationEvent{
	private static final long serialVersionUID = -8368153055104486979L;
	//要移动的话题
	private final Topic topic;
	//话题原来的版块
	private final Board protoBoard;
	//移动至的新版块
	private final Board targetBoard;
	//管理员
	private final String managerNickname;
	//操作的日期
	private final LocalDateTime dateTime;
	
	public TopicMoveEvent(Object source, Topic moveTopic, Board protoBoard, Board targetBoard, String managerNickname) {
		super(source);
		this.topic = moveTopic;
		this.protoBoard = protoBoard;
		this.targetBoard = targetBoard;
		this.managerNickname = managerNickname;
		this.dateTime = LocalDateTime.now();
	}

	public Topic getTopic() {
		return topic;
	}

	public Board getProtoBoard() {
		return protoBoard;
	}

	public Board getTargetBoard() {
		return targetBoard;
	}

	public String getManagerNickname() {
		return managerNickname;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}
}
