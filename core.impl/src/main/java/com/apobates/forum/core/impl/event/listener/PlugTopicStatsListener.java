package com.apobates.forum.core.impl.event.listener;


import com.apobates.forum.core.api.dao.BoardStatsDao;
import com.apobates.forum.core.api.dao.TopicStatsDao;
import com.apobates.forum.core.entity.Topic;
import com.apobates.forum.core.impl.event.PlugTopicPublishEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * 插件话题创建事件侦听器.创建话题统计文件并更新版块的统计
 * @author xiaofanku
 * @since 20200531
 */
@Component
public class PlugTopicStatsListener implements ApplicationListener<PlugTopicPublishEvent>{
    @Autowired
    private TopicStatsDao topicStatsDao;
    @Autowired
    private BoardStatsDao boardStatsDao;
    private final static Logger logger = LoggerFactory.getLogger(PlugTopicStatsListener.class);
    
    @Override
    public void onApplicationEvent(PlugTopicPublishEvent event) {
        logger.info("[PlugTopic][PublishEvent][3]插件话题统计和版块统计处理开始");
        //话题统计
        Topic topic = event.getPlugTopic();
        topicStatsDao.createStats(topic.getId(), topic.getVolumesId(), topic.getBoardId());
        boardStatsDao.updateTopic(topic.getBoardId(), topic.getTitle(), topic.getId(), topic.getEntryDateTime(), topic.getMemberId(), topic.getMemberNickname());
        logger.info("[PlugTopic][PublishEvent][3]插件话题统计和版块统计处理结束");
    }
}
