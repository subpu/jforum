package com.apobates.forum.core.impl.event;

import org.springframework.context.ApplicationEvent;
import com.apobates.forum.core.entity.Topic;
import com.apobates.forum.event.elderly.ActionEventCulpritor;
import com.apobates.forum.event.elderly.ForumActionEnum;

/**
 * 插件类话题发布事件
 * 
 * @author xiaofanku
 * @since 20200530
 */
public class PlugTopicPublishEvent extends ApplicationEvent{
    private static final long serialVersionUID = 1L;
    private final Topic plugTopic;
    private final ActionEventCulpritor culpritor;
    //什么动作生成的插件话题
    //反馈,投拆话题(,回复)
    private final ForumActionEnum action;
    
    public PlugTopicPublishEvent(Object source, Topic plugTopic, ForumActionEnum action, ActionEventCulpritor culpritor) {
        super(source);
        this.plugTopic = plugTopic;
        this.action = action;
        this.culpritor = culpritor;
    }
    
    public Topic getPlugTopic() {
        return plugTopic;
    }
    
    public ForumActionEnum getAction() {
        return action;
    }
    
    public ActionEventCulpritor getCulpritor() {
        return culpritor;
    }
}
