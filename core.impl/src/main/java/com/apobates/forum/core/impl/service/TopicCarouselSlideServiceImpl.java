package com.apobates.forum.core.impl.service;

import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.apobates.forum.attention.core.ImagePathCoverter;
import com.apobates.forum.core.api.ImageIOMeta;
import com.apobates.forum.core.api.dao.BoardCarouselIndexDao;
import com.apobates.forum.core.api.dao.TopicCarouselSlideDao;
import com.apobates.forum.core.api.service.TopicCarouselSlideService;
import com.apobates.forum.core.entity.TopicCarouselSlide;

@Service
public class TopicCarouselSlideServiceImpl implements TopicCarouselSlideService{
	@Autowired
	private TopicCarouselSlideDao topicCarouselSlideDao;
	@Autowired
	private BoardCarouselIndexDao boardCarouselIndexDao;
	
	@Override
	public Stream<TopicCarouselSlide> getAll(int topicCarouselId) {
		return topicCarouselSlideDao.findAllUsed(topicCarouselId);
	}

	@Override
	public Stream<TopicCarouselSlide> getAll(final String topicCarouselTitle, final ImageIOMeta imageIOInfo) {
		Consumer<TopicCarouselSlide> action = (tcs)->{
			String imagePath =  new ImagePathCoverter(tcs.getImageAddr()).decodeUploadImageFilePath(imageIOInfo.getImageBucketDomain(), imageIOInfo.getUploadImageDirectName()).orElse("/static/img/140x140.png");
			tcs.setImageAddr(imagePath);
		};
		
		return topicCarouselSlideDao.findAllUsed(topicCarouselTitle).peek(action);
	}

	@Override
	public Stream<TopicCarouselSlide> getAllIgnoreStatus(int topicCarouselId) {
		return topicCarouselSlideDao.findAll(topicCarouselId);
	}

	@Override
	public Optional<Boolean> edit(int id, int ranking)throws IllegalStateException {
		return topicCarouselSlideDao.editRanking(id, ranking);
	}

	@Override
	public int edit(Map<Integer, Integer> rankingMap)throws IllegalStateException {
		return topicCarouselSlideDao.editRanking(rankingMap);
	}

	@Override
	public int create(int topicCarouselId, String title, String link, String encodeImageAddr, int ranking, boolean status)throws IllegalStateException {
		//编码图片地址
		TopicCarouselSlide tcs = new TopicCarouselSlide(topicCarouselId, title, link, encodeImageAddr, ranking, status);
		topicCarouselSlideDao.save(tcs);
		if(tcs.getId()<1) {
			throw new  IllegalStateException("幻灯片创建失败");
		}
		return tcs.getId();
	}

	@Override
	public Optional<Boolean> edit(int id, String title, String link, int ranking, boolean status)throws IllegalStateException {
		TopicCarouselSlide tcs = get(id).orElseThrow(()->new IllegalStateException("幻灯片不存在或无法访问"));
		tcs.setLink(link);
		tcs.setRanking(ranking);
		tcs.setTitle(title);
		tcs.setStatus(status);
		return topicCarouselSlideDao.edit(tcs);
	}

	@Override
	public Optional<TopicCarouselSlide> get(int id) {
		if(id>0){
			return topicCarouselSlideDao.findOne(id);
		}
		return Optional.empty();
	}

	@Override
	public Optional<Boolean> remove(int id, int topicCarouselId)throws IllegalStateException {
		return topicCarouselSlideDao.editStatus(id, topicCarouselId, false);
	}

	@Override
	public Stream<TopicCarouselSlide> getAll(int boardGroupId, final ImageIOMeta imageIOInfo) {
		Consumer<TopicCarouselSlide> action = (tcs)->{
			String imagePath =  new ImagePathCoverter(tcs.getImageAddr()).decodeUploadImageFilePath(imageIOInfo.getImageBucketDomain(), imageIOInfo.getUploadImageDirectName()).orElse("/static/img/140x140.png");
			tcs.setImageAddr(imagePath);
		};
		return boardCarouselIndexDao.findAllUsedByBoard(boardGroupId).peek(action);
	}

	@Override
	public Stream<TopicCarouselSlide> getAll(int boardGroupId, long boardId, ImageIOMeta imageIOInfo) {
		Consumer<TopicCarouselSlide> action = (tcs)->{
			String imagePath =  new ImagePathCoverter(tcs.getImageAddr()).decodeUploadImageFilePath(imageIOInfo.getImageBucketDomain(), imageIOInfo.getUploadImageDirectName()).orElse("/static/img/140x140.png");
			tcs.setImageAddr(imagePath);
		};
		return boardCarouselIndexDao.findAllUsedByBoard(boardGroupId, boardId).peek(action);
	}

}
