package com.apobates.forum.strategy.exposure.impl;

import com.apobates.forum.core.entity.Posts;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.strategy.exception.VerificaFailException;
import com.apobates.forum.strategy.exposure.AbstractDetectionStrategy;
import java.util.function.BiPredicate;
/**
 * 回复检测器
 * 
 * @author xiaofanku
 * @since 20200803
 */
public final class PostsDetectionStrategy extends AbstractDetectionStrategy<Posts>{
    
    @Override
    protected BiPredicate<Posts, ForumActionEnum> getEntityStrategy() throws VerificaFailException {
        return (Posts entity, ForumActionEnum forumAction) -> {
            if (null == entity || entity.getId() == 0) {
                throw new VerificaFailException("回复不存在或暂时无法访问");
            }
            if (!entity.isStatus()) {
                throw new VerificaFailException("回复当前处于:不可用状态");
            }
            if (!entity.isReply() && ForumActionEnum.POSTS_EDIT == forumAction) { //1楼的内容
                throw new VerificaFailException("不被支持的操作, 原因: 话题的内容编辑请使用话题编辑");
            }
            return true;
        };
    }
}