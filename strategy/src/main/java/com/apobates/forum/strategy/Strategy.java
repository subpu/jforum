package com.apobates.forum.strategy;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.member.entity.MemberRoleEnum;

/**
 * 策略注解
 * 
 * @author xiaofanku
 * @since 20200808
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Strategy {
    /**
     * 动作
     * @return 
     */
    public ForumActionEnum action();
    
    /**
     * 允许的角色,默认值:不限制角色
     * @return 
     */
    public MemberRoleEnum[] allowRoles()default {};
    
    /**
     * 实体参数,默认值:REFERER
     * REFERER 从请求的来源中取值
     * URI 从当前请求地址中取值
     * QUERY_STR 从请求参数中取值
     * @return 
     */
    public StrategyEntityParam param()default StrategyEntityParam.REFERER;
    
    /**
     * 当param=QUERY_STR时从请求参数中取此方法中填写的参数值
     * @return 
     */
    public String paramId()default "";
    
    /**
     * 配置文件的模式,默认值:其它模式
     * 非默认模式时会执行实体配置文件策略
     * @return 
     */
    public StrategyMode mode()default StrategyMode.ETC;
    
    /**
     * 处理器,默认值:由从请求的动作中分析得出
     * Void表示由当前类型的策略来处理,反之由特定的策略
     * @return 
     */
    public Class<?> handler()default Void.class;
}