package com.apobates.forum.strategy.exposure.impl;

import com.apobates.forum.core.entity.Board;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.strategy.exception.VerificaFailException;
import com.apobates.forum.strategy.exposure.AbstractDetectionStrategy;
import java.util.function.BiPredicate;
/**
 * 版块检测器
 * 
 * @author xiaofanku
 * @since 20200803
 */
public final class BoardDetectionStrategy extends AbstractDetectionStrategy<Board>{
    
    @Override
    protected BiPredicate<Board, ForumActionEnum> getEntityStrategy() throws VerificaFailException {
        return (Board entity, ForumActionEnum forumAction) -> {
            if (null == entity || entity.getId() == 0) {
                throw new VerificaFailException("版块不存在或暂时无法访问");
            }
            if (!entity.isNormal()) {
                throw new VerificaFailException(String.format("版块当前处于:%s状态", entity.getStatus().getTitle()));
            }
            //安装了反馈和投诉的版块对浏览的检查
            //ETC
            return true;
        };
    }
}