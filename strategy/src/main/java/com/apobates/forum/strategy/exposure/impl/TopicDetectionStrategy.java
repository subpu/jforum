package com.apobates.forum.strategy.exposure.impl;

import com.apobates.forum.core.entity.Topic;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.strategy.exception.VerificaFailException;
import com.apobates.forum.strategy.exposure.AbstractDetectionStrategy;
import java.util.function.BiPredicate;
/**
 * 话题检测器
 * 
 * @author xiaofanku
 * @since 20200803
 */
public final class TopicDetectionStrategy extends AbstractDetectionStrategy<Topic>{
    
    @Override
    protected BiPredicate<Topic, ForumActionEnum> getEntityStrategy() throws VerificaFailException {
        return (Topic entity, ForumActionEnum forumAction) -> {
            if (null == entity || entity.getId() == 0) {
                throw new VerificaFailException("话题不存在或暂时无法访问");
            }
            if (!entity.isNormal()) {
                throw new VerificaFailException(String.format("话题当前处于:%s状态", entity.getStatus().getTitle()));
            }
            return true;
        };
    }
}