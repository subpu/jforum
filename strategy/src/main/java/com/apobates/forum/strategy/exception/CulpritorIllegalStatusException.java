package com.apobates.forum.strategy.exception;
/**
 * 非法的操作者状态异常
 * @author xiaofanku
 * @since 20190419
 */
public class CulpritorIllegalStatusException extends StrategyException{
	private static final long serialVersionUID = 1L;
	
	public CulpritorIllegalStatusException(String message){
		super(message);
	}
	public CulpritorIllegalStatusException(String message, Throwable cause){
		super(message, cause);
	}
}
