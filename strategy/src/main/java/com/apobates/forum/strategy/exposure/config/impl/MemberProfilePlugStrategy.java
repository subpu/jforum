package com.apobates.forum.strategy.exposure.config.impl;

import com.apobates.forum.member.MemberProfileBean;
import com.apobates.forum.strategy.exception.StrategyException;
import java.util.function.Supplier;

/**
 * 实体配置文件针对会员等级信息的检查策略
 * 
 * @param <C> 实体配置文件
 * @author xiaofanku
 * @since 20200806
 */
public interface MemberProfilePlugStrategy<C> {
    /**
     * 写模式检查
     * @param entityConfig 实体配置文件
     * @param memberProfile 会员等级信息
     * @return
     * @throws StrategyException 
     */
    boolean writeMode(C entityConfig, Supplier<MemberProfileBean> memberProfile) throws StrategyException;
    
    /**
     * 读模式检查
     * @param entityConfig 实体配置文件
     * @param memberProfile 会员等级信息
     * @return
     * @throws StrategyException 
     */
    boolean readMode(C entityConfig, Supplier<MemberProfileBean> memberProfile) throws StrategyException;
}