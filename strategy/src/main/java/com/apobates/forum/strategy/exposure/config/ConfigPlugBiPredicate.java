package com.apobates.forum.strategy.exposure.config;

import java.util.function.BiPredicate;
import java.util.function.Supplier;
import com.apobates.forum.strategy.StrategyMode;

/**
 * 配置文件扩展策略接口
 * 
 * @param <T>
 * @param <U>
 * @author xiaofanku
 * @since 20200810
 */
public interface ConfigPlugBiPredicate<T, U>{
    /**
     * 扩展策略适用的模式
     * 
     * @return 
     */
    StrategyMode[] allowModes();
    /**
     * 扩展策略执行的条件
     * @return 
     */
    Supplier<BiPredicate<T, U>> condition();
}