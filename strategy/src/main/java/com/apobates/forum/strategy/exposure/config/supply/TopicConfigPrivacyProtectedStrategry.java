package com.apobates.forum.strategy.exposure.config.supply;

import com.apobates.forum.core.entity.Topic;
import com.apobates.forum.core.entity.TopicConfig;
import com.apobates.forum.member.entity.Member;
import com.apobates.forum.strategy.StrategyMode;
import com.apobates.forum.strategy.exception.PrivacyProtectedException;
import com.apobates.forum.strategy.exposure.config.ConfigPlugTriPredicate;
import com.apobates.forum.utils.lang.TriPredicate;
import java.util.function.Supplier;

/**
 * 话题隐私保护策略
 * 
 * @author xiaofanku
 * @since 20200806
 */
public final class TopicConfigPrivacyProtectedStrategry implements ConfigPlugTriPredicate<Topic, TopicConfig, Member>{
    
    @Override
    public StrategyMode[] allowModes() {
        return new StrategyMode[]{StrategyMode.READ, StrategyMode.WRITE};
    }
    
    @Override
    public Supplier<TriPredicate<Topic, TopicConfig, Member>> condition() {
        return ()->(Topic entity, TopicConfig entityConfig, Member culpritor)->{
            if (entityConfig.isPrivacy()) {
                if (culpritor.getId() != entity.getMemberId()) {
                    throw new PrivacyProtectedException("话题已开启隐私保护");
                }
            }
            return true;
        };
    }
}