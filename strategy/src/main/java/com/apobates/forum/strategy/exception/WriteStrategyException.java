package com.apobates.forum.strategy.exception;
/**
 * 写模式策略异常
 * @author xiaofanku
 * @since 20190418
 */
public class WriteStrategyException extends StrategyException{
	private static final long serialVersionUID = 1L;
	
	public WriteStrategyException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	public WriteStrategyException(String message,  Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}
}
