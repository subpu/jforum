package com.apobates.forum.strategy.exception;
/**
 * 操作者的个人信息丢失异常
 * @author xiaofanku
 * @since 20190425
 */
public class CulpritorProfileLostException extends StrategyException{
	private static final long serialVersionUID = 1L;
	
	public CulpritorProfileLostException(String message){
		super(message);
	}
	public CulpritorProfileLostException(String message, Throwable cause){
		super(message, cause);
	}
}
