package com.apobates.forum.strategy.exception;

/**
 * VerifyDescriptor相关的异常
 * 
 * @author xiaofanku
 * @since 20191107
 */
public class VerificaFailException extends RuntimeException{
	private static final long serialVersionUID = -8256239064694042035L;

	public VerificaFailException(String message) {
		super(message);
	}

	public VerificaFailException(String message, Throwable cause) {
		super(message, cause);
	}
}
