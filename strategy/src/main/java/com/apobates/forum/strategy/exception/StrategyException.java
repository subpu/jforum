package com.apobates.forum.strategy.exception;
/**
 * 策略异常的顶层类
 * @author xiaofanku
 * @since 20190418
 */
public class StrategyException extends RuntimeException{
	private static final long serialVersionUID = 1L;

	public StrategyException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public StrategyException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
}
