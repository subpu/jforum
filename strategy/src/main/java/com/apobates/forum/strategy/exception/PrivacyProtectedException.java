package com.apobates.forum.strategy.exception;

public class PrivacyProtectedException extends StrategyException{
	private static final long serialVersionUID = 1L;
	
	public PrivacyProtectedException(String message){
		super(message);
	}
	public PrivacyProtectedException(String message, Throwable cause){
		super(message, cause);
	}
}
