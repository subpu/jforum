package com.apobates.forum.strategy.exposure.config.impl;

import com.apobates.forum.core.entity.BoardModerator;
import com.apobates.forum.core.entity.TopicConfig;
import com.apobates.forum.member.MemberProfileBean;
import com.apobates.forum.strategy.exception.CulpritorProfileLostException;
import com.apobates.forum.strategy.exception.ReadStrategyException;
import com.apobates.forum.strategy.exception.StrategyException;
import com.apobates.forum.strategy.exception.WriteStrategyException;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * 话题配置文件针对会员等级信息的检查策略
 * 
 * @author xiaofanku
 * @since 20200806
 */
public final class TopicConfigMemberProfileStrategy implements MemberProfilePlugStrategy<TopicConfig>{
    private final boolean ignoreModerator;
    private final Supplier<Optional<BoardModerator>> managerModerator;
    public final static TopicConfigMemberProfileStrategy READ_INS = new TopicConfigMemberProfileStrategy(true, ()->Optional.empty());
    /**
     *
     * @param ignoreModerator 是否忽略对版主进行等级和积分检查
     * @param managerModerator 会员是否是版块的版主或版块所在组的大版主
     */
    public TopicConfigMemberProfileStrategy(boolean ignoreModerator, Supplier<Optional<BoardModerator>> managerModerator) {
        this.ignoreModerator = ignoreModerator;
        this.managerModerator = managerModerator;
    }
    
    @Override
    public boolean readMode(TopicConfig config, Supplier<MemberProfileBean> memberProfile) throws StrategyException {
        //是否存在profileBean
        MemberProfileBean mpb = memberProfile.get();
        if (null == mpb) {
            throw new CulpritorProfileLostException("未知的会员等级");
        }
        
        if (config.getReadLowMemberLevel() > 0) { //有等级要求
            //等级
            if (mpb.getLevelNo() < config.getReadLowMemberLevel()) {
                throw new ReadStrategyException("您当前的等级不满足话题阅读要求");
            }
        }
        
        if (config.getReadMinScore() > 0) { //有积分要求
            //积分
            if (mpb.getScore() < config.getReadMinScore()) {
                throw new ReadStrategyException("您当前的积分不满足话题阅读要求");
            }
        }
        return true;
    }
    
    @Override
    public boolean writeMode(TopicConfig config, Supplier<MemberProfileBean> memberProfile) throws StrategyException {
        //是否跳过版主的等级和积分检查
        if (ignoreModerator && managerModerator.get().isPresent()) {
            return true;
        }
        //是否存在profileBean
        MemberProfileBean mpb = memberProfile.get();
        if (null == mpb) {
            throw new CulpritorProfileLostException("未知的会员等级");
        }
        //是否存在profileBean
        if (config.getWriteLowMemberLevel() > 0) { //有等级要求
            //等级
            if (mpb.getLevelNo() < config.getWriteLowMemberLevel()) {
                throw new WriteStrategyException("您当前的等级不满足话题写入要求");
            }
        }
        
        if (config.getWriteMinScore() > 0) { //有积分要求
            //积分
            if (mpb.getScore() < config.getWriteMinScore()) {
                throw new WriteStrategyException("您当前的积分不满足话题写入要求");
            }
        }
        return true;
    }
}
