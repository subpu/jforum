package com.apobates.forum.strategy.exposure.config;

import com.apobates.forum.strategy.StrategyMode;
import com.apobates.forum.utils.lang.TriPredicate;
import java.util.function.Supplier;

/**
 * 配置文件扩展策略接口
 * 
 * @param <T>
 * @param <U>
 * @param <V>
 * @author xiaofanku
 * @since 20200810
 */
public interface ConfigPlugTriPredicate<T, U, V>{
    /**
     * 扩展策略适用的模式
     * 
     * @return 
     */
    StrategyMode[] allowModes();
    /**
     * 扩展策略执行的条件
     * @return 
     */
    Supplier<TriPredicate<T, U, V>> condition();
}