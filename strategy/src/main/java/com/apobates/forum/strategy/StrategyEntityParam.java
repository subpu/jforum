package com.apobates.forum.strategy;

/**
 * 实体主键的取值渠道
 * 
 * @author xiaofanku
 * @since 20200808
 */
public enum StrategyEntityParam {
    /**
     * 从请求地址中取主键
     */
    URI,
    /**
     * 从请求来源中取主键
     */
    REFERER,
    /**
     * 从请求参数中取主键
     */
    QUERY_STR;
}