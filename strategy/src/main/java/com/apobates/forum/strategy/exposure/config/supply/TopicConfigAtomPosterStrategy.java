package com.apobates.forum.strategy.exposure.config.supply;

import com.apobates.forum.core.entity.TopicConfig;
import com.apobates.forum.member.entity.Member;
import com.apobates.forum.strategy.StrategyMode;
import com.apobates.forum.strategy.exception.StrategyException;
import com.apobates.forum.strategy.exposure.config.ConfigPlugBiPredicate;
import java.util.function.BiPredicate;
import java.util.function.Supplier;

/**
 * 话题原子回复补充策略
 * 
 * @author xiaofanku
 * @since 20200806
 */
public final class TopicConfigAtomPosterStrategy implements ConfigPlugBiPredicate<TopicConfig, Member>{
    private final Supplier<Long> replySize; //culpritor已经回复的数量
    
    public TopicConfigAtomPosterStrategy(Supplier<Long> replySize) {
        this.replySize = replySize;
    }
    
    @Override
    public Supplier<BiPredicate<TopicConfig, Member>> condition() {
        return ()->(TopicConfig entityConfig, Member culpritor)->{
            if (entityConfig.isAtomPoster()) { //作原子检查
                if (replySize.get() != 0L) {
                    throw new StrategyException("话题已开启原子回复.禁止多次回复");
                }
            }
            return true; //不作原子检查
        };
    }
    
    @Override
    public StrategyMode[] allowModes(){
        return new StrategyMode[]{StrategyMode.WRITE};
    }
}