package com.apobates.forum.strategy.exception;
/**
 * 读模式策略异常
 * @author xiaofanku
 * @since 20190418
 */
public class ReadStrategyException extends StrategyException{
	private static final long serialVersionUID = 1L;

	public ReadStrategyException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ReadStrategyException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
