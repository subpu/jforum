package com.apobates.forum.strategy;

/**
 * 实体配置文件的模式
 * 
 * @author xiaofanku
 * @since 20200515
 */
public enum StrategyMode {
    /**
     * 只读
     */
    READ,
    /**
     * 写
     */
    WRITE,
    /**
     * 读写
     */
    MODIFY,
    /**
     * 其它,不使用实体配置文件策略时使用
     */
    ETC;
}