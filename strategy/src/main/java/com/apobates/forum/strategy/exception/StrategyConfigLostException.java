package com.apobates.forum.strategy.exception;
/**
 * 策略配置文件丢失
 * @author xiaofanku
 * @since 20190418
 */
public class StrategyConfigLostException extends StrategyException{
	private static final long serialVersionUID = 1L;
	
	public StrategyConfigLostException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public StrategyConfigLostException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}
	
}
