package com.apobates.forum.attention.core;
/**
 * 图片存储的路径
 * @author xiaofanku
 * @since 20190307
 */
public interface ImageStorePath{
	/**
	  *  返回默认的存储路径
	 * @return
	 */
	String getDefaultImagePath();
	/**
	  * 返回本地的存储路径
	 * @return
	 */
	String getLocalImagePath();
	/**
	  * 返回远程或第三方的存储路径
	 * @return
	 */
	String getRemoteImagePath();
	
}
