package com.apobates.forum.attention.core;

/**
  * 图片存储源
 * 
 * @author xiaofanku
 * @since 20190307
 */
public enum ImageDirectoryEnum {
	DEFAT(0, "内置"), LOCAL(1, "本地"), REMOT(2, "远程");

	private final int symbol;
	private final String title;

	private ImageDirectoryEnum(int symbol, String title) {
		this.symbol = symbol;
		this.title = title;
	}

	public int getSymbol() {
		return symbol;
	}

	public String getTitle() {
		return title;
	}
	/**
	 * 使用参数与enum的name作无区分大小匹配
	 * @param name
	 * @return 匹配失败返回null
	 */
	public static ImageDirectoryEnum getInstance(String name) {
		ImageDirectoryEnum ins = null;
		for (ImageDirectoryEnum ide : values()) {
			if (ide.name().equalsIgnoreCase(name)) {
				ins = ide;
				break;
			}
		}
		return ins;
	}
}
