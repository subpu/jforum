package com.apobates.forum.attention.core;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apobates.forum.utils.Commons;

/**
 * 图片路径转换器
 * @author xiaofanku
 * @since 20190307
 */
public class ImagePathCoverter {
	//要处理的内容
	//头像: Schema(avtar://)
	//表情: Schema(smile://)
	//上传: Schema(image://)
	private final String content;
	private final static Logger logger = LoggerFactory.getLogger(ImagePathCoverter.class);
	
	public ImagePathCoverter(String content) {
		super();
		this.content = content;
	}
	
	/**
	 * 解码默认头像的访问地址(http[s]://)
	 * 默认头像为系统提供的头像
	 * 
	 * @param storePath 图片的存储路径,例:avtar://defat[|local|remot]/目录名/文件名
	 * @return 成功返回
	 */
	public Optional<String> decodeAvatarPath(final ImageStorePath storePath) {
		if(!Commons.isNotBlank(getContent())) {
			return Optional.empty();
		}
		//0-5=模式(avtar),8-13=存储源(defat)
		ImageDirectoryEnum directoryEnum = null;
		try {
			directoryEnum=getDirectory(getContent().substring(8, 13));
		}catch(IndexOutOfBoundsException e) {}
		
		return new AvatarImagePathConvertHandler(storePath).decode(getContent(), directoryEnum, null);
	}
	
	/**
	 * 编码头像的路径
	 * 
	 * @param fileNamePath  以/开头+目录名+/+文件名
	 * @param directoryEnum 图片存储源
	 * @return
	 */
	public String encodeAvatarPath(ImageDirectoryEnum directoryEnum) {
		return new AvatarImagePathConvertHandler(null).encode(getContent(), directoryEnum).get();
	}
	
	/**
	 * 编码表情图片
	 * 
	 * @param schemaDomain     http[s]://domain
	 * @param smileyDirectName 表情的根目录
	 * @return
	 */
	public String encodeSmilePath(String schemaDomain, String smileyDirectName){
		return new SmileyImagePathConvertHandler(schemaDomain, smileyDirectName).encode(getContent(), ImageDirectoryEnum.DEFAT).get();
	}
	
	/**
	 * 解码表情图片
	 * 
	 * @param schemaDomain     http[s]://domain
	 * @param smileyDirectName 表情的根目录
	 * @return
	 */
	public Optional<String> decodeSmilePath(String schemaDomain, String smileyDirectName){
		if(!Commons.isNotBlank(getContent())) {
			return Optional.empty();
		}
		return new SmileyImagePathConvertHandler(schemaDomain, smileyDirectName).decode(getContent(), ImageDirectoryEnum.DEFAT, null);
	}
	
	/**
	 * 编码内容(回复)中包含上传的图片
	 * 
	 * @param imageDomain           图片存储的域名
	 * @param uploadImageDirectName 图片保存的目录名称
	 * @return
	 */
	public String encodeUploadImagePath(String imageDomain, String uploadImageDirectName){
		return new UploadImagePathConvertHandler(imageDomain, uploadImageDirectName).encode(getContent(), ImageDirectoryEnum.DEFAT).get();
	}
	
	/**
	 * 解码内容(回复)中包含上传的图片
	 * 
	 * @param imageDomain           图片存储的域名
	 * @param uploadImageDirectName 图片保存的目录名称
	 * @param lazyLoad              内容中的图片是否懒加载; true进行懒加载,false直接加载
	 * @param scale                 图片的尺寸,若为null表示自动
	 * @return
	 */
	public Optional<String> decodeUploadImagePath(String imageDomain, String uploadImageDirectName, boolean lazyLoad, String scale){
		if(!Commons.isNotBlank(getContent())) {
			return Optional.empty();
		}
		logger.info("[SCALE][ImagePathCoverter]image scale: "+scale);
		return new UploadImagePathConvertHandler(imageDomain, uploadImageDirectName, lazyLoad).decode(getContent(), ImageDirectoryEnum.DEFAT, scale);
	}
	
	/**
	 * 解码单个上传图片的路径
	 * 
	 * @param imageDomain           图片存储的域名
	 * @param uploadImageDirectName 图片保存的目录名称
	 * @return
	 */
	public Optional<String> decodeUploadImageFilePath(String imageDomain, String uploadImageDirectName){
		if(!Commons.isNotBlank(getContent())) {
			return Optional.empty();
		}
		return new UploadImagePathConvertHandler(imageDomain, uploadImageDirectName).filePathDecode(getContent(), ImageDirectoryEnum.DEFAT);
	}
	
	/**
	 * 解码单个上传图片的路径
	 * 
	 * @param imageDomain           图片存储的域名
	 * @param uploadImageDirectName 图片保存的目录名称
	 * @param lazyLoad              图片是否懒加载; true进行懒加载,false直接加载
	 * @param scale                 尺寸(auto|width<x>height);
	 * @return
	 */
	public Optional<String> decodeUploadImageFilePath(String imageDomain, String uploadImageDirectName, String scale){
		if(!Commons.isNotBlank(getContent())) {
			return Optional.empty();
		}
		return new UploadImagePathConvertHandler(imageDomain, uploadImageDirectName).filePathDecode(getContent(), ImageDirectoryEnum.DEFAT, scale);
	}
	/**
	 * 编码单个上传图片的路径
	 * 
	 * @param imageDomain           图片存储的域名
	 * @param uploadImageDirectName 图片保存的目录名称
	 * @return
	 */
	public Optional<String> encodeUploadImageFilePath(String imageDomain, String uploadImageDirectName){
		if(!Commons.isNotBlank(getContent()) || getContent().equals("500")) {
			return Optional.empty();
		}
		return new UploadImagePathConvertHandler(imageDomain, uploadImageDirectName).filePathEncode(getContent(), ImageDirectoryEnum.DEFAT);
	}
	
	public String getContent() {
		return content;
	}
	
	private ImageDirectoryEnum getDirectory(String name) {
		return ImageDirectoryEnum.getInstance(name);
	}
}
