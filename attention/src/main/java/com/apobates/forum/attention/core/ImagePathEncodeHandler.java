package com.apobates.forum.attention.core;

import java.util.Optional;

/**
 * 图片路径编码句柄
 * @author xiaofanku
 * @since 20190307
 */
public interface ImagePathEncodeHandler {
	/**
	 * 编码
	 * 
	 * @param content       待编码的内容
	 * @param directoryEnum 图片目录的存储形式(本地,远程/站外,内置)
	 * @return
	 */
	Optional<String> encode(String content, ImageDirectoryEnum directoryEnum);
}
