package com.apobates.forum.attention.core;

import java.util.Optional;

/**
 * 图片路径解码句柄
 * 将编码的图片路径还原为可访问的图片连接
 * 为什么要编码图片的路径?
 * 1:方便在不同的存储中迁移,例如:可以将本地存储的图片迁至第三方的OSS,此时不需要对DB中的图片路径作修改
 * 2:方便替换
 * @author xiaofanku
 * @since 20190307
 */
public interface ImagePathDecodeHandler {
	/**
	 * 解码
	 * @param content       待解码的内容
	 * @param directoryEnum 图片目录的存储形式(本地,远程/站外,内置)
	 * @param scale         图片的尺寸,若为null表示自动
	 * @return 
	 */
	Optional<String> decode(String content, ImageDirectoryEnum directoryEnum, String scale);
}