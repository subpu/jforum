package com.apobates.forum.letterbox.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
/**
 * 发件箱
 * @author xiaofanku
 * @since 20190914
 */
@Entity
@Table(name = "apo_letter_outbox", uniqueConstraints={@UniqueConstraint(columnNames={"member","receiver","letter"})})
public class Outbox implements Serializable{
	private static final long serialVersionUID = -6091096592336558464L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	//会员id
	private long member;
	//收件人/会员ID
	private long receiver;
	private String receiverNickname;
	//是否可用:false已删除,true可用
	private boolean status;
	//信件ID
	private long letter;
	
	public Outbox(){}
	public Outbox(long letterAuthor, long receiver, String receiverNickname, long letterId){
		this.member = letterAuthor;
		this.receiver = receiver;
		this.receiverNickname = receiverNickname;
		this.letter = letterId;
		this.status=true;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getMember() {
		return member;
	}
	public void setMember(long member) {
		this.member = member;
	}
	public long getReceiver() {
		return receiver;
	}
	public void setReceiver(long receiver) {
		this.receiver = receiver;
	}
	public String getReceiverNickname() {
		return receiverNickname;
	}
	public void setReceiverNickname(String receiverNickname) {
		this.receiverNickname = receiverNickname;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public long getLetter() {
		return letter;
	}
	public void setLetter(long letter) {
		this.letter = letter;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (letter ^ (letter >>> 32));
		result = prime * result + (int) (member ^ (member >>> 32));
		result = prime * result + (int) (receiver ^ (receiver >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Outbox other = (Outbox) obj;
		if (letter != other.letter)
			return false;
		if (member != other.member)
			return false;
		if (receiver != other.receiver)
			return false;
		return true;
	}
}
