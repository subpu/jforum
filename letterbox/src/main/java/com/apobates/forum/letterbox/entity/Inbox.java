package com.apobates.forum.letterbox.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
/**
 * 收件箱
 * @author xiaofanku
 * @since 20190914
 */
@Entity
@Table(name = "apo_letter_inbox", uniqueConstraints={@UniqueConstraint(columnNames={"member","sender","letter"})})
public class Inbox implements Serializable{
	private static final long serialVersionUID = 1151225407824658078L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	//会员id
	private long member;
	//发件人/会员ID
	private long sender;
	private String senderNickname;
	//是否已读:false已读,true未读
	private boolean readable;
	//是否回复:true已回复,false未回复
	private boolean reply;
	//是否删除:false已删除,true可用
	private boolean usable;
	//信件ID
	private long letter;
	
	public Inbox(){}
	public Inbox(long member, long letterAuthor, String letterAuthorNickname, long letterId){
		this.member = member;
		this.sender = letterAuthor;
		this.senderNickname = letterAuthorNickname;
		this.letter = letterId;
		this.readable = true;
		this.reply=false;
		this.usable=true;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getMember() {
		return member;
	}
	public void setMember(long member) {
		this.member = member;
	}
	public long getSender() {
		return sender;
	}
	public void setSender(long sender) {
		this.sender = sender;
	}
	public String getSenderNickname() {
		return senderNickname;
	}
	public void setSenderNickname(String senderNickname) {
		this.senderNickname = senderNickname;
	}
	public boolean isReadable() {
		return readable;
	}
	public void setReadable(boolean readable) {
		this.readable = readable;
	}
	public boolean isReply() {
		return reply;
	}
	public void setReply(boolean reply) {
		this.reply = reply;
	}
	public boolean isUsable() {
		return usable;
	}
	public void setUsable(boolean usable) {
		this.usable = usable;
	}
	public long getLetter() {
		return letter;
	}
	public void setLetter(long letter) {
		this.letter = letter;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (letter ^ (letter >>> 32));
		result = prime * result + (int) (member ^ (member >>> 32));
		result = prime * result + (int) (sender ^ (sender >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Inbox other = (Inbox) obj;
		if (letter != other.letter)
			return false;
		if (member != other.member)
			return false;
		if (sender != other.sender)
			return false;
		return true;
	}
}
