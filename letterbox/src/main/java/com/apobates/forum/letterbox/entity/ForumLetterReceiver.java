package com.apobates.forum.letterbox.entity;

import javax.persistence.Transient;
/**
 * ForumLetter的收件人
 * 
 * @author xiaofanku
 * @since 20190914
 */
public class ForumLetterReceiver {
	private final long member;
	private final String memberNickname;
	
	public ForumLetterReceiver(long member, String memberNickname) {
		super();
		this.member = member;
		this.memberNickname = memberNickname;
	}
	
	public long getMember() {
		return member;
	}

	public String getMemberNickname() {
		return memberNickname;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (member ^ (member >>> 32));
		result = prime * result + ((memberNickname == null) ? 0 : memberNickname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ForumLetterReceiver other = (ForumLetterReceiver) obj;
		if (member != other.member)
			return false;
		if (memberNickname == null) {
			if (other.memberNickname != null)
				return false;
		} else if (!memberNickname.equals(other.memberNickname))
			return false;
		return true;
	}
	@Transient
	public static ForumLetterReceiver allMemberes(){
		return new ForumLetterReceiver(0L, "所有");
	}
}
