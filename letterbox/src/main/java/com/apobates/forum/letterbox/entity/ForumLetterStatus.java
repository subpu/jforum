package com.apobates.forum.letterbox.entity;

public class ForumLetterStatus {
	//Inbox.id
	private final long id;
	private final boolean readable;
	private final boolean reply;
	private final boolean usable;
	
	public ForumLetterStatus(long boxId, boolean readable, boolean reply, boolean usable) {
		this.id = boxId;
		this.readable = readable;
		this.reply = reply;
		this.usable = usable;
	}
	public boolean isReadable() {
		return readable;
	}
	public boolean isReply() {
		return reply;
	}
	public boolean isUsable() {
		return usable;
	}
	public long getId() {
		return id;
	}
}
