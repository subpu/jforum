package com.apobates.forum.letterbox.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * 信件
 * @author xiaofanku
 * @since 20190914
 */
@Entity
@Table(name = "apo_letter")
public class ForumLetter implements Serializable{
	private static final long serialVersionUID = 591037265377031648L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	//标题
	private String title;
	//内容
	@Lob
	@Column(columnDefinition="TEXT")
	private String content;
	//创建日期
	private LocalDateTime entryDateTime;
	//消息类型
	@Basic
	@Enumerated(EnumType.STRING)
	private ForumLetterTypeEnum typed;
	//作者/发件人/会员ID
	private long author;
	//作者名称/会员昵称
	private String nickname;
	//是否需要回应(只有私信需要其它类型都不需要)
	private boolean responsive;
	//收件人
	@Transient
	private Set<ForumLetterReceiver> receivers = new HashSet<>();
	//Inbox的状态{readable|reply|usable}
	@Transient
	private ForumLetterStatus status;
	
	public ForumLetter() {}
	public ForumLetter(String title, String content, long author, String authorNickname, ForumLetterTypeEnum type) {
		this.title = title;
		this.content = content;
		this.entryDateTime = LocalDateTime.now();
		this.typed = type;
		this.author = author;
		this.nickname = authorNickname;
		this.responsive = (ForumLetterTypeEnum.LETTER == type);
	}
	/**
	 * 创建一条通知信件
	 * @param title
	 * @param content
	 * @param receiver
	 * @param receiverNames
	 */
	public ForumLetter(String title, String content, long receiver, String receiverNames) {
		this.title = title;
		this.content = content;
		this.entryDateTime = LocalDateTime.now();
		this.typed = ForumLetterTypeEnum.NOTICE;
		this.author = 0L;
		this.nickname = "机器人";
		this.responsive = false;
		this.receivers.add(new ForumLetterReceiver(receiver, receiverNames));
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public LocalDateTime getEntryDateTime() {
		return entryDateTime;
	}
	public void setEntryDateTime(LocalDateTime entryDateTime) {
		this.entryDateTime = entryDateTime;
	}
	public ForumLetterTypeEnum getTyped() {
		return typed;
	}
	public void setTyped(ForumLetterTypeEnum typed) {
		this.typed = typed;
	}
	public long getAuthor() {
		return author;
	}
	public void setAuthor(long author) {
		this.author = author;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public boolean isResponsive() {
		return responsive;
	}
	public void setResponsive(boolean responsive) {
		this.responsive = responsive;
	}
	public Set<ForumLetterReceiver> getReceivers() {
		return receivers;
	}
	public void setReceivers(Set<ForumLetterReceiver> receivers) {
		this.receivers = receivers;
	}
	@Transient
	public String getTargetReceiverNicknames(){
		if(getReceivers().isEmpty()){
			return ForumLetterTypeEnum.ALL.getTitle();
		}
		List<String> data = new ArrayList<>();
		for(ForumLetterReceiver r : getReceivers()){
			data.add(r.getMemberNickname());
		}
		return String.join(",", data);
	}
	public ForumLetterStatus getStatus() {
		return status;
	}
	public void setStatus(ForumLetterStatus status) {
		this.status = status;
	}
}
