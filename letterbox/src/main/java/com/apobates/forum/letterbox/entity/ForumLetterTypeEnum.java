package com.apobates.forum.letterbox.entity;

import com.apobates.forum.utils.lang.EnumArchitecture;
/**
 * 信件的类型
 * @author xiaofanku
 * @since 20190914
 */
public enum ForumLetterTypeEnum implements EnumArchitecture{
	ALL(0, "所有"),
	NOTICE(1, "通知"),
	LETTER(2, "私信"),
	CLAIM(3, "公告");
	
	private final int symbol;
	private final String title;
	
	private ForumLetterTypeEnum(int symbol, String title) {
		this.symbol = symbol;
		this.title = title;
	}
	
	@Override
	public int getSymbol() {
		return symbol;
	}
	@Override
	public String getTitle() {
		return title;
	}
}
