package com.apobates.forum.member.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

/**
 * 会员帐号保护
 * @author xiaofanku
 * @since 20190809
 */
@Entity
@Table(name = "apo_member_protect", uniqueConstraints={@UniqueConstraint(columnNames={"memberNames"})})
public class MemberNamesProtect implements Serializable{
	private static final long serialVersionUID = -2824461951137793643L;
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	//会员登录帐号名
	private String memberNames;
	//是否可用
	private boolean status;
	//empty constructor for JPA instantiation
	public MemberNamesProtect(){}
	public MemberNamesProtect(String memberNames){
		this.memberNames = memberNames;
		this.status = true;
	}
	public long getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMemberNames() {
		return memberNames;
	}
	public void setMemberNames(String memberNames) {
		this.memberNames = memberNames;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((memberNames == null) ? 0 : memberNames.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MemberNamesProtect other = (MemberNamesProtect) obj;
		if (memberNames == null) {
			if (other.memberNames != null)
				return false;
		} else if (!memberNames.equals(other.memberNames))
			return false;
		return true;
	}

	@Transient
	public static Set<String> getNativeProtect() {
		return Collections.unmodifiableSet(new HashSet<>(Arrays.asList("root", "guest", "admin", "robot", "administrator", "system", "systemclerk", "anony", "anonymous", "spider", "leader")));
	}
}
