package com.apobates.forum.member.entity;

import com.apobates.forum.utils.lang.EnumArchitecture;

/**
 * 会员的状态枚举
 * @author xiaofanku
 * @since 20190302
 */
public enum MemberStatusEnum implements EnumArchitecture{
	//登录不了
	DELETE(0, "失踪"),
	
	//能登录但话题和版块都打不开
	GROUND(1, "禁足"),
	
	//能看;不能回,发)
	READ(2, "禁言"),
	
	//能看;能回复;不能发贴
	POST(3, "附言"),
	//
	ACTIVE(4, "正常");
	
	private final int symbol;
	private final String title;
	
	private MemberStatusEnum(int symbol, String title) {
		this.symbol=symbol;
		this.title=title;
	}
	
	@Override
	public int getSymbol() {
		return symbol;
	}
	
	@Override
	public String getTitle() {
		return title;
	}
	
}
