package com.apobates.forum.member.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import com.apobates.forum.event.elderly.ForumActionEnum;
/**
 * 会员在线记录
 * @author xiaofanku
 * @since 20190630
 */
@Entity
@Table(name = "apo_member_online")
public class MemberOnline implements Serializable{
	private static final long serialVersionUID = 1L;
	/**
	 * 会员ID
	 */
	@Id
	private long mid;
	/**
	 * 会员昵称
	 */
	private String memberNickname;
	/**
	 * 最近的操作
	 */
	@Basic
	@Enumerated(EnumType.STRING)
	private ForumActionEnum action;
	/**
	 * 最近活跃日期
	 */
	private LocalDateTime activeDateTime;
	
    //empty constructor for JPA instantiation
	public MemberOnline() {}
	public MemberOnline(long memberId, String memberNickname, ForumActionEnum action) {
		this.action = action;
		this.mid = memberId;
		this.memberNickname = memberNickname;
		this.activeDateTime = LocalDateTime.now();
	}
	
	public long getMid() {
		return mid;
	}
	public void setMid(long mid) {
		this.mid = mid;
	}
	public String getMemberNickname() {
		return memberNickname;
	}
	public void setMemberNickname(String memberNickname) {
		this.memberNickname = memberNickname;
	}
	public ForumActionEnum getAction() {
		return action;
	}
	public void setAction(ForumActionEnum action) {
		this.action = action;
	}
	public LocalDateTime getActiveDateTime() {
		return activeDateTime;
	}
	public void setActiveDateTime(LocalDateTime activeDateTime) {
		this.activeDateTime = activeDateTime;
	}
}
