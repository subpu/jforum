package com.apobates.forum.member.exception;

public class MemberNamesExistException extends RuntimeException{

	public MemberNamesExistException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MemberNamesExistException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
