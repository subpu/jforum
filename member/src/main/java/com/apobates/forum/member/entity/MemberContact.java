package com.apobates.forum.member.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
/**
 * 会员联系方式
 * @author xiaofanku
 * @since 20190304
 */
@Entity
@Table(name = "apo_member_contact", uniqueConstraints={@UniqueConstraint(columnNames={"memberId"}),@UniqueConstraint(columnNames={"mobileCrc32"}),@UniqueConstraint(columnNames={"mobile"})})
public class MemberContact implements Serializable{
	private static final long serialVersionUID = 7909529678196416988L;
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private long memberId;
	//省
	private String province;
	//城市
	private String city;
	//区
	private String region;
	//街道
	private String street;
	//邮编
	private String postcode;
    /**
     * 手机
     * DEC加密
     */
    @Column(columnDefinition="VARCHAR(64) NOT NULL")
	private String mobile;
    /**
         * 手机的crc32值
     */
    @Column(columnDefinition="VARCHAR(64) NOT NULL")
	private String mobileCrc32;
    //empty constructor for JPA instantiation
	public MemberContact() {}

	public MemberContact(long id, long memberId, String province, String city, String region, String street,
			String postcode, String mobile, String mobileCrc32) {
		super();
		this.id = id;
		this.memberId = memberId;
		this.province = province;
		this.city = city;
		this.region = region;
		this.street = street;
		this.postcode = postcode;
		this.mobileCrc32 = mobileCrc32;
		this.mobile = mobile;
	}
	public MemberContact(long memberId, String province, String city, String region, String street,
			String postcode, String mobile, String mobileCrc32) {
		super();
		this.id = 0L;
		this.memberId = memberId;
		this.province = province;
		this.city = city;
		this.region = region;
		this.street = street;
		this.postcode = postcode;
		this.mobileCrc32 = mobileCrc32;
		this.mobile = mobile;
	}
	public long getId() {
		return id;
	}

	public long getMemberId() {
		return memberId;
	}

	public String getProvince() {
		return province;
	}

	public String getCity() {
		return city;
	}

	public String getRegion() {
		return region;
	}

	public String getStreet() {
		return street;
	}

	public String getPostcode() {
		return postcode;
	}

	public String getMobile() {
		return mobile;
	}

	public String getMobileCrc32() {
		return mobileCrc32;
	}
	//--------------------------------------------SET
	public void setId(long id) {
		this.id = id;
	}

	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public void setMobileCrc32(String mobileCrc32) {
		this.mobileCrc32 = mobileCrc32;
	}
	
}
