package com.apobates.forum.member.entity;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;
import com.apobates.forum.utils.lang.EnumArchitecture;
/**
 * 会员角色
 * @author xiaofanku
 * @since 20190617
 */
public enum MemberRoleEnum implements EnumArchitecture{
	//无
	NO(1, "无"),
	//版主
	BM(2, "版主"),
	//版块组(卷)的版主
	MASTER(3, "大版主"),
	//超级管理员
	ADMIN(4, "管理员");
	
	private final int symbol;
	private final String title;
	
	private MemberRoleEnum(int symbol, String title) {
		this.symbol = symbol;
		this.title = title;
	}

	@Override
	public int getSymbol() {
		return symbol;
	}

	@Override
	public String getTitle() {
		return title;
	}
	
	public static Set<MemberRoleEnum> getManagerRoles(){
		return Collections.unmodifiableSet(EnumSet.of(MemberRoleEnum.BM, MemberRoleEnum.MASTER, MemberRoleEnum.ADMIN));
	}
}
