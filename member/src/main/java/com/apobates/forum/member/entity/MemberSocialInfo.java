package com.apobates.forum.member.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
/**
 * 会员社会化信息
 * @author xiaofanku
 * @since 20190304
 */
@Entity
@Table(name = "apo_member_socialinfo", uniqueConstraints={@UniqueConstraint(columnNames={"memberId"}), @UniqueConstraint(columnNames={"memberNames"}), @UniqueConstraint(columnNames={"memberNames", "email"})})
public class MemberSocialInfo implements Serializable{
	private static final long serialVersionUID = 5862716054557875559L;
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private long memberId;
	//登录帐号,用于密码找回
	private String memberNames;
	private String alibaba;
	private String weibo;
	private String weixin;
	private String qq;
	//邮箱
	private String email;
    //empty constructor for JPA instantiation
	public MemberSocialInfo() {}
	
	public MemberSocialInfo(long id, long memberId, String memberNames, String alibaba, String weibo, String weixin, String qq,	String email) {
		super();
		this.id = id;
		this.memberId = memberId;
		this.memberNames = memberNames;
		this.alibaba = alibaba;
		this.weibo = weibo;
		this.weixin = weixin;
		this.qq = qq;
		this.email = email;
	}
	public MemberSocialInfo(long memberId, String memberNames, String alibaba, String weibo, String weixin, String qq,String email) {
		super();
		this.id = 0L;
		this.memberId = memberId;
		this.memberNames = memberNames;
		this.alibaba = alibaba;
		this.weibo = weibo;
		this.weixin = weixin;
		this.qq = qq;
		this.email = email;
	}
	public long getId() {
		return id;
	}
	public long getMemberId() {
		return memberId;
	}
	public String getAlibaba() {
		return alibaba;
	}
	public String getWeibo() {
		return weibo;
	}
	public String getWeixin() {
		return weixin;
	}
	public String getQq() {
		return qq;
	}
	public String getEmail() {
		return email;
	}

	//--------------------------------------------SET
	public void setId(long id) {
		this.id = id;
	}

	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}

	public void setAlibaba(String alibaba) {
		this.alibaba = alibaba;
	}

	public void setWeibo(String weibo) {
		this.weibo = weibo;
	}

	public void setWeixin(String weixin) {
		this.weixin = weixin;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMemberNames() {
		return memberNames;
	}

	public void setMemberNames(String memberNames) {
		this.memberNames = memberNames;
	}
}
