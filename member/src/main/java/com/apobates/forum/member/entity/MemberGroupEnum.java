package com.apobates.forum.member.entity;

import com.apobates.forum.utils.lang.EnumArchitecture;

/**
 * 会员组
 * @author xiaofanku
 * @since 20190301
 */
public enum MemberGroupEnum implements EnumArchitecture{
	SPIDER(-2, "网络蜘蛛"),
	ROBOT(-1, "机器人"),
	GUEST(0, "游客"),
	//注册会员
	CARD(1, "会员"),
	//VIP
	VIP(2, "超级会员"),
	//Support Team Member
	//Community Team Leader
	LEADER(3, "社区经理");
	
	private final int symbol;
	private final String title;
	
	private MemberGroupEnum(int symbol, String title) {
		this.symbol = symbol;
		this.title = title;
	}
	
	@Override
	public int getSymbol() {
		return symbol;
	}
	
	@Override
	public String getTitle() {
		return title;
	}
}
