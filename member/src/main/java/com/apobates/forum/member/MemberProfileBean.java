package com.apobates.forum.member;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.apobates.forum.member.entity.Member;
import com.apobates.forum.member.entity.MemberGroupEnum;
import com.apobates.forum.member.entity.MemberRoleEnum;
import com.apobates.forum.utils.Commons;

/**
 * 会员个人信息
 * @author xiaofanku
 * @since 20190411
 *
 */
public class MemberProfileBean implements Serializable{
	private static final long serialVersionUID = -4322979292162931992L;
	//会员ID
	private final long id;
	private final String nickname;
	//会员所在组名称
	private final String groupNames;
	//会员所有角名称
	private final String roleNames;
	//用以关联CSS样式定义:Member.getStyle
	private final String label;
	//主题数量
	private final long threads;
	//回复数
	private final long replies;
	//等级名称:小白
	private final String level;
	//等级数字:1级
	private final int levelNo;
	//根据主题的操作行为计算所得积分
	private final double score;
	//会员的签名
	private final String signature;
	
	public long getId() {
		return id;
	}
	
	public long getThreads() {
		return threads;
	}
	
	public long getReplies() {
		return replies;
	}
	
	public String getLevel() {
		return level;
	}
	
	public double getScore() {
		return score;
	}
	
	public String getSignature() {
		return signature;
	}

	public int getLevelNo() {
		return levelNo;
	}

	public String getRoleNames() {
		return roleNames;
	}

	public String getGroupNames() {
		return groupNames;
	}

	public String getLabel() {
		return label;
	}

	public String getNickname() {
		return nickname;
	}

	private MemberProfileBean(long id, String nickname, String groupNames, String roleNames, String label, long threads, long replies, String level, int levelNo, double score, String signature) {
		super();
		this.id = id;
		this.nickname = nickname;
		this.groupNames = groupNames;
		this.roleNames = roleNames;
		this.label = label;
		this.threads = threads;
		this.replies = replies;
		this.level = level;
		this.score = score;
		this.signature = signature;
		this.levelNo = levelNo;
	}
	/**
	 * 若extInfoMap中的key与toMap的key冲突,以toMap为准
	 * 
	 * @param extInfoMap
	 *            扩展的会员信息
	 * @return
	 */
	public Map<String, String> toMergeMap(Map<String, String> extInfoMap) {
		return Commons.mergeMap(toMap(), extInfoMap, (oldValue, newValue) -> oldValue);
	}
	public Map<String, String> toMap(){
		Map<String, String> data = new HashMap<>();
		data.put("id", getId()+"");
		data.put("nickname", getNickname());
		data.put("groupNames", getGroupNames());
		data.put("roleNames", getRoleNames());
		data.put("style", getLabel());
		data.put("level", getLevel());
		data.put("levelNo", getLevelNo()+"");
		data.put("threads", getThreads()+"");
		data.put("replies", getReplies()+"");
		data.put("score", getScore()+"");
		data.put("signature", getSignature());
		/*
        return Map.ofEntries(Map.entry("id", getId() + ""), 
                Map.entry("nickname", getNickname()), 
                Map.entry("groupNames", getGroupName()), 
                Map.entry("roleNames", getRoleName()), 
                Map.entry("style", getLabel()), 
                Map.entry("signature", getSignature()), 
                Map.entry("level", getLevel()), 
                Map.entry("levelNo", getLevelNo() + ""), 
                Map.entry("threads", getThreads() + ""), 
                Map.entry("replies", getReplies() + ""), 
                Map.entry("score", getScore() + ""));*/
		return data;
	}
	
	public static MemberProfileBean guest(){
		return new MemberProfileBean(-1L, Member.GUEST_NAMES, MemberGroupEnum.GUEST.getTitle(), MemberRoleEnum.NO.getTitle(), "guest", 0, 0, "-", 0, 0, "");
	}
	
	public static MemberProfileBean empty(long id, String names, String groupNames, String roleNames, String label, String signature){
		return new MemberProfileBean(id, names, groupNames, roleNames, label, 0, 0, "-", 0, 0, signature);
	}
	
	public static class MemberProfileBuilder{
		private final long id;
		private final String nickname;
		private final String groupNames; //组名
		private final String roleNames; //角色名
		private final String label; //用以关联CSS样式定义
		private String level="VA";
		private int levelNo=0;
		private long threads=0L;
		private long replies=0L;
		private double score=0;
		private String signature="No Signature";
		
		public MemberProfileBuilder(long id, String nickname, String groupNames, String roleNames, String label, String signature){
			this.id = id;
			this.nickname = nickname;
			this.groupNames = groupNames;
			this.roleNames = roleNames;
			this.label = label;
			if(Commons.isNotBlank(signature)){
				this.signature = signature;
			}
		}
		
		public MemberProfileBean build(){
			return new MemberProfileBean(getId(), getNickname(), getGroupNames(), getRoleNames(), getLabel(), getThreads(), getReplies(), getLevel(), getLevelNo(), getScore(), getSignature());
		}
		
		public MemberProfileBuilder setLevel(String level, int levelNo){
			this.level = level;
			this.levelNo = levelNo;
			return this;
		}
		
		public MemberProfileBuilder setThreads(long threads){
			this.threads = threads;
			return this;
		}
		
		public MemberProfileBuilder setReplies(long replies){
			this.replies = replies;
			return this;
		}
		
		public MemberProfileBuilder setScore(double score) {
			this.score = score;
			return this;
		}
		
		public long getId() {
			return id;
		}

		public String getLevel() {
			return level;
		}

		private long getThreads() {
			return threads;
		}

		private long getReplies() {
			return replies;
		}

		public double getScore() {
			return score;
		}

		public String getSignature() {
			return signature;
		}

		public int getLevelNo() {
			return levelNo;
		}

		public String getRoleNames() {
			return roleNames;
		}

		public String getGroupNames() {
			return groupNames;
		}

		public String getLabel() {
			return label;
		}

		public String getNickname() {
			return nickname;
		}
		
	}
}
