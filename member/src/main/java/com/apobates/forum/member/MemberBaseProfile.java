package com.apobates.forum.member;

import java.io.Serializable;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import com.apobates.forum.member.entity.Member;
import com.apobates.forum.member.entity.MemberGroupEnum;
import com.apobates.forum.member.entity.MemberRoleEnum;
import com.apobates.forum.utils.Commons;
/**
 * 会员实体中的公用信息
 * 
 * @author xiaofanku
 * @since 20190422
 */
public class MemberBaseProfile implements Serializable{
	private static final long serialVersionUID = 1L;
	//会员ID
	private final long id;
	//会员名称
	private final String nickname;
	//组名称
	private final String groupName; //中文名
	//角色名称
	private final String roleName;
	private final String label; //英文名
	//签名
	private final String signature;
	
	public MemberBaseProfile(
			long id, 
			String nickname, 
			MemberGroupEnum memberGroup, 
			MemberRoleEnum memberRole, 
			String label, 
			String signature) {
		super();
		this.id = id;
		this.nickname = nickname;
		this.groupName = memberGroup.getTitle();
		this.roleName = memberRole.getTitle();
		this.label = label;
		this.signature = signature;
	}

	public static MemberBaseProfile init(Member member) {
		return new MemberBaseProfile(member.getId(), member.getNickname(), member.getMgroup(), member.getMrole(), getStyle(member.getMgroup(), member.getMrole()), member.getSignature());
	}
	
	public long getId() {
		return id;
	}

	public String getNickname() {
		return nickname;
	}

	public String getGroupName() {
		return groupName;
	}


	public String getSignature() {
		return signature;
	}

	public String getRoleName() {
		return roleName;
	}

	public String getLabel() {
		return label;
	}
	/**
	 * 若extInfoMap中的key与toMap的key冲突,以toMap为准
	 * 
	 * @param extInfoMap
	 *            扩展的会员信息
	 * @return
	 */
	public Map<String, String> toMergeMap(Map<String, String> extInfoMap) {
		return Commons.mergeMap(toMap(), extInfoMap, (oldValue, newValue) -> oldValue);
	}

	/**
	 * 返回一个不可变的map,不可以在结果基础上put
	 * 
	 * @return
	 */
	public Map<String, String> toMap() {
		Map<String, String> data = new HashMap<>();
		data.put("id", getId() + "");
		data.put("nickname", getNickname());
		data.put("groupNames", getGroupName());
		data.put("roleNames", getRoleName());
		data.put("style", getLabel());
		data.put("signature", getSignature());
		return data;
	}

	/**
	 * 返回会员的色值样式
	 * 
	 * @param member
	 *            若参数为null返回空字符串
	 * @return
	 */
	public static String getStyle(Member member) {
		return Optional.ofNullable(member).map((Member m) -> MemberStyle.get(m.getMrole(), m.getMgroup())).orElse("");
	}

	/**
	 * 返回会员的色值样式
	 * 
	 * @param mg
	 *            会员的组枚举
	 * @param mr
	 *            会员的角色枚举
	 * @return
	 */
	public static String getStyle(MemberGroupEnum mg, MemberRoleEnum mr) {
		return MemberStyle.get(mr, mg);
	}

	private static class MemberStyle {
		private final static EnumMap<MemberRoleEnum, String> ROLE_STYLE = new EnumMap<>(MemberRoleEnum.class);
		private final static EnumMap<MemberGroupEnum, String> GROUP_STYLE = new EnumMap<>(MemberGroupEnum.class);

		static {
			ROLE_STYLE.put(MemberRoleEnum.BM, "moderator");
			ROLE_STYLE.put(MemberRoleEnum.MASTER, "master");
			ROLE_STYLE.put(MemberRoleEnum.ADMIN, "admin");
			//
			GROUP_STYLE.put(MemberGroupEnum.CARD, "general");
			GROUP_STYLE.put(MemberGroupEnum.VIP, "vip");
			GROUP_STYLE.put(MemberGroupEnum.GUEST, "guest");
			GROUP_STYLE.put(MemberGroupEnum.SPIDER, "spider");
			GROUP_STYLE.put(MemberGroupEnum.ROBOT, "robot");
			GROUP_STYLE.put(MemberGroupEnum.LEADER, "manager");
		}

		public static String get(MemberRoleEnum mrole, MemberGroupEnum mgroup) {
			Optional<String> data = Optional.ofNullable(ROLE_STYLE.get(mrole));
			if (!data.isPresent()) {
				data = Optional.ofNullable(GROUP_STYLE.get(mgroup));
			}
			return data.orElse("");
		}
	}
}
