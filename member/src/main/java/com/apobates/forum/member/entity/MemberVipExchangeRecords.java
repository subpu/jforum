package com.apobates.forum.member.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * VIP交易记录
 *
 * @author xiaofanku
 * @since 20200921
 */
@Entity
@Table(name = "apo_member_exchange", uniqueConstraints = {@UniqueConstraint(columnNames = {"memberId", "status"})})
public class MemberVipExchangeRecords implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;
    //会员
    private long memberId;
    private String memberNickname;
    // 记录日期
    private LocalDateTime entryDateTime;
    // 开始日期
    private LocalDateTime activeDateTime;
    // 有效日期单位,只支持:月,年
    @Basic
    @Enumerated(EnumType.STRING)
    private ForumCalendarUnitEnum durationUnit;
    // 有效日期时长
    private int duration;
    // 结束日期
    private LocalDateTime lapseDateTime;
    // 是否有效,true(1)有效,false(0)无效
    @Column(columnDefinition="tinyint(1) default 0")
    private boolean status;
    // 交易流水号
    private String serial;

    //empty constructor for JPA instantiation
    public MemberVipExchangeRecords() {
    }

    public MemberVipExchangeRecords(long memberId, String memberNickname, int duration, ForumCalendarUnitEnum unit) {
        this.id = 0L;
        this.memberId = memberId;
        this.memberNickname = memberNickname;
        this.durationUnit = unit;
        this.duration = duration;
        this.status = true;
        //
        LocalDateTime entry = LocalDateTime.now();
        this.activeDateTime = entry;
        this.entryDateTime = entry;
        this.lapseDateTime = unit.plug(duration, entry);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getMemberId() {
        return memberId;
    }

    public void setMemberId(long memberId) {
        this.memberId = memberId;
    }

    public String getMemberNickname() {
        return memberNickname;
    }

    public void setMemberNickname(String memberNickname) {
        this.memberNickname = memberNickname;
    }

    public LocalDateTime getEntryDateTime() {
        return entryDateTime;
    }

    public void setEntryDateTime(LocalDateTime entryDateTime) {
        this.entryDateTime = entryDateTime;
    }

    public LocalDateTime getActiveDateTime() {
        return activeDateTime;
    }

    public void setActiveDateTime(LocalDateTime activeDateTime) {
        this.activeDateTime = activeDateTime;
    }

    public ForumCalendarUnitEnum getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(ForumCalendarUnitEnum durationUnit) {
        this.durationUnit = durationUnit;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public LocalDateTime getLapseDateTime() {
        return lapseDateTime;
    }

    public void setLapseDateTime(LocalDateTime lapseDateTime) {
        this.lapseDateTime = lapseDateTime;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + (int) (this.memberId ^ (this.memberId >>> 32));
        hash = 31 * hash + (this.status ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MemberVipExchangeRecords other = (MemberVipExchangeRecords) obj;
        if (this.memberId != other.memberId) {
            return false;
        }
        if (this.status != other.status) {
            return false;
        }
        return true;
    }
}
