package com.apobates.forum.member.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import com.apobates.forum.member.MemberProfileBean;

/**
 * 会员
 * @author xiaofanku
 * @since 20190301
 */
@Entity
@Table(name = "apo_member", uniqueConstraints={@UniqueConstraint(columnNames={"names"})})
public class Member implements Serializable{
	private static final long serialVersionUID = 5320847116939560963L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	//登录的帐号名
	private String names;
	/**
	 * 密码 Sha256加密
	 */
	@Column(nullable = false, length = 65)
	private String pswd;
	/**
	 * 密码盐
	 */
	@Column(nullable = false, unique = true, length = 9)
	private String salt;
	//注册日期
	private LocalDateTime registeDateTime;
	//状态 
	@Basic
	@Enumerated(EnumType.STRING)
	private MemberStatusEnum status;
	//组
	@Basic
	@Enumerated(EnumType.STRING)
	private MemberGroupEnum mgroup;
	//角色
	@Basic
	@Enumerated(EnumType.STRING)
	private MemberRoleEnum mrole;
	//头像地址
	private String avatarURI;
	//签名
	private String signature;
	//昵称
	@Column(nullable=false, unique = true, length=15)
	private String nickname;
	//邀请码
	private String inviteCode;
	//邀请码ID
	private long inviteCodeId;
	//SSO/OAUTH
	//第三的域名
	private String tdparty;
	@Transient
	private MemberProfileBean profile; //策略需要的
	
	public final static String GUEST_AVATAR = "avtar://defat/empty.png";
	public final static String GUEST_NAMES = "Guest";
	//empty constructor for JPA instantiation
	public Member() {}
	//到DB去
	public Member(long id, String names, String pswd, String salt, MemberStatusEnum status, MemberGroupEnum mgroup, MemberRoleEnum mrole, String nickname) {
		this.id = id;
		this.names = names;
		this.pswd = pswd;
		this.salt = salt;
		this.registeDateTime = LocalDateTime.now();
		this.status = status;
		this.mgroup = mgroup;
		this.mrole = mrole;
		//
		this.avatarURI = null;
		this.signature = "";
		this.nickname = nickname;
		this.tdparty = null;
	}
	//到DB去
	public Member(long id, String names, String pswd, String salt,String nickname) {
		this(id, names, pswd, salt, MemberStatusEnum.ACTIVE, MemberGroupEnum.CARD, MemberRoleEnum.NO, nickname);
	}

	public long getId() {
		return id;
	}

	public String getNames() {
		return names;
	}

	public String getPswd() {
		return pswd;
	}

	public String getSalt() {
		return salt;
	}

	public LocalDateTime getRegisteDateTime() {
		return registeDateTime;
	}

	public MemberStatusEnum getStatus() {
		return status;
	}

	public MemberGroupEnum getMgroup() {
		return mgroup;
	}
	public String getAvatarURI() {
		return avatarURI == null?GUEST_AVATAR:avatarURI;
	}
	public String getSignature() {
		return signature;
	}
	public String getNickname() {
		return nickname;
	}
	public MemberProfileBean getProfile() {
		return profile;
	}
	//--------------------------------------------SET
	public void setId(long id) {
		this.id = id;
	}

	public void setNames(String names) {
		this.names = names;
	}

	public void setPswd(String pswd) {
		this.pswd = pswd;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public void setRegisteDateTime(LocalDateTime registeDateTime) {
		this.registeDateTime = registeDateTime;
	}

	public void setStatus(MemberStatusEnum status) {
		this.status = status;
	}

	public void setMgroup(MemberGroupEnum mgroup) {
		this.mgroup = mgroup;
	}
	@Transient
	public static Member robotMember() {
		Member robot = new Member(0L, "SystemClerk", "123456", "123456", MemberStatusEnum.ACTIVE, MemberGroupEnum.ROBOT, MemberRoleEnum.NO, "机器人");
		robot.setSignature("There is no gene for the human spirit");
		return robot;
	}
	@Transient
	public static Member guestMember(){
		Member guest = new Member(-1L, "Guest", "*", null, MemberStatusEnum.ACTIVE, MemberGroupEnum.GUEST, MemberRoleEnum.NO, "guest");
        guest.setSignature("I am a leaf on the wind");
        return guest;
	}
	@Transient
	public static Member empty(long id){
		if(id==0){
			return robotMember();
		}
		return new Member(id, "Member#"+id, "*", null, MemberStatusEnum.GROUND, MemberGroupEnum.CARD, MemberRoleEnum.NO, "Member#"+id);
	}
	public void setAvatarURI(String avatarURI) {
		this.avatarURI = avatarURI;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public void setProfile(MemberProfileBean profile) {
		this.profile = profile;
	}
	public MemberRoleEnum getMrole() {
		return mrole;
	}
	public void setMrole(MemberRoleEnum mrole) {
		this.mrole = mrole;
	}
	public String getInviteCode() {
		return inviteCode;
	}
	public void setInviteCode(String inviteCode) {
		this.inviteCode = inviteCode;
	}
	public long getInviteCodeId() {
		return inviteCodeId;
	}
	public void setInviteCodeId(long inviteCodeId) {
		this.inviteCodeId = inviteCodeId;
	}
	public String getTdparty() {
		return tdparty;
	}
	public void setTdparty(String tdparty) {
		this.tdparty = tdparty;
	}
	/**
	 * 是否来自第三方的渠道注册的会员
	 * @return 本站会员返回false
	 */
	@Transient
	public boolean isThirdpartySource(){
		return getTdparty()!=null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((names == null) ? 0 : names.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (obj == null){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		Member other = (Member) obj;
		if (names == null) {
			if (other.names != null){
				return false;
			}
		} else if (!names.equals(other.names)){
			return false;
		}
		return true;
	}
}
