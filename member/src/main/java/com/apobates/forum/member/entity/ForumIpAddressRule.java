package com.apobates.forum.member.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * ip过滤记录
 * @author xiaofanku
 * @since 20190810
 */
@Entity
@Table(name = "apo_ipaddr_rule", uniqueConstraints={@UniqueConstraint(columnNames={"addrole"})})
public class ForumIpAddressRule implements Serializable{
	private static final long serialVersionUID = 4144724877777901952L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	//过滤的ip规则表达式
	private String ruleExpress;
	//是否可用
	private boolean status;
	//
	public ForumIpAddressRule(){}
	public ForumIpAddressRule(String ruleExpress){
		this.ruleExpress = ruleExpress;
		this.status = true;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRuleExpress() {
		return ruleExpress;
	}
	public void setRuleExpress(String ruleExpress) {
		this.ruleExpress = ruleExpress;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ruleExpress == null) ? 0 : ruleExpress.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ForumIpAddressRule other = (ForumIpAddressRule) obj;
		if (ruleExpress == null) {
			if (other.ruleExpress != null)
				return false;
		} else if (!ruleExpress.equals(other.getRuleExpress()))
			return false;
		return true;
	}
	
	
}
