package com.apobates.forum.member.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
/**
 * 会员实名认证
 * @author xiaofanku
 * @since 20190304
 */
@Entity
@Table(name = "apo_member_realauthent", uniqueConstraints={@UniqueConstraint(columnNames={"memberId"}),@UniqueConstraint(columnNames={"identityCard"})})
public class MemberRealAuthentication implements Serializable{
	private static final long serialVersionUID = -2967898163980986087L;
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	private long memberId;
	//真实姓名
	private String realname;
	//生日
	//年
    @Column(nullable=false, length=4)
	private String birthYear;
    //月
    @Column(nullable=false, length=2)
	private String birthMonth;
    //日
    @Column(nullable=false, length=2)
	private String birthDay;
	//身份证号
	private String identityCard;
	//验证通过没
	private boolean passed;
    //empty constructor for JPA instantiation
	public MemberRealAuthentication() {}
	
	public MemberRealAuthentication(long id, long memberId, String realname, String birthYear, String birthMonth,
			String birthDay, String identityCard, boolean passed) {
		super();
		this.id = id;
		this.memberId = memberId;
		this.realname = realname;
		this.birthYear = birthYear;
		this.birthMonth = birthMonth;
		this.birthDay = birthDay;
		this.identityCard = identityCard;
		this.passed = passed;
	}
	
	public MemberRealAuthentication(long memberId, String realname, String birthYear, String birthMonth,
			String birthDay, String identityCard, boolean passed) {
		this.id = 0;
		this.memberId = memberId;
		this.realname = realname;
		this.birthYear = birthYear;
		this.birthMonth = birthMonth;
		this.birthDay = birthDay;
		this.identityCard = identityCard;
		this.passed = passed;
	}

	public long getId() {
		return id;
	}

	public long getMemberId() {
		return memberId;
	}

	public String getRealname() {
		return realname;
	}

	public String getBirthYear() {
		return birthYear;
	}

	public String getBirthMonth() {
		return birthMonth;
	}

	public String getBirthDay() {
		return birthDay;
	}

	public String getIdentityCard() {
		return identityCard;
	}

	public boolean isPassed() {
		return passed;
	}
	//--------------------------------------------SET
	public void setId(long id) {
		this.id = id;
	}

	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public void setBirthYear(String birthYear) {
		this.birthYear = birthYear;
	}

	public void setBirthMonth(String birthMonth) {
		this.birthMonth = birthMonth;
	}

	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}

	public void setIdentityCard(String identityCard) {
		this.identityCard = identityCard;
	}

	public void setPassed(boolean passed) {
		this.passed = passed;
	}
}
