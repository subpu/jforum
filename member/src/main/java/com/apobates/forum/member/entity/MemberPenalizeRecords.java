package com.apobates.forum.member.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

/**
 * 惩罚记录<br/>
 * 暂不支持预定日期的惩罚,只支持即可生效<br/>
 * 暂不支持惩罚的多重性,一个会员同时只能有一个惩罚<br/>
 * 
 * @author xiaofanku
 * @since 20190722
 */
@Entity
@Table(name = "apo_member_penalizes", uniqueConstraints={@UniqueConstraint(columnNames={"memberId", "arrive", "status"})})
public class MemberPenalizeRecords implements Serializable{
	private static final long serialVersionUID = 5569730578012369700L;
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	//被惩罚的会员
	private long memberId;
	private String memberNickname;
	/**
	 * 惩罚前的状态
	 */
	@Basic
	@Enumerated(EnumType.STRING)
	private MemberStatusEnum original;
	/**
	 * 惩罚降临的状态,给于何种惩罚
	 */
	@Basic
	@Enumerated(EnumType.STRING)
	private MemberStatusEnum arrive;
	// 记录日期
	private LocalDateTime entryDateTime;
	// 惩罚开始的日期
	private LocalDateTime rebornDateTime;
	// 惩罚的日期单位
	@Basic
	@Enumerated(EnumType.STRING)
	private ForumCalendarUnitEnum rebornUnit;
	// 惩罚的日期数量
	private int rebornumber;
	//惩罚的结束日期
	private LocalDateTime rebirthDateTime;
	// 理由
	private String reason;
	// 谁出具的惩罚
	private long judger;
	private String judgeNickname;
	//证据
	private String evidences;
	// 是否有效,true有效,false无效
	private boolean status;
    //empty constructor for JPA instantiation
	public MemberPenalizeRecords() {}
	public MemberPenalizeRecords(
			long memberId, 
			String memberNickname, 
			MemberStatusEnum original, 
			MemberStatusEnum arrive, 
			LocalDateTime reborn, 
			ForumCalendarUnitEnum rebornUnit, 
			int rebornLimit, 
			String reason, 
			String evidences, 
			long judger,
			String judgeNickname) {
		this.memberId = memberId;
		this.memberNickname = memberNickname;
		this.original = original;
		this.arrive = arrive;
		this.rebornDateTime = reborn;
		this.rebornUnit = rebornUnit;
		this.rebornumber = rebornLimit;
		this.rebirthDateTime = rebornUnit.plusDateTime(rebornLimit, rebornDateTime);
		this.reason = reason;
		this.evidences = evidences;
		
		this.judger = judger;
		this.judgeNickname = judgeNickname;
		
		this.status = true;
		this.entryDateTime = LocalDateTime.now();
		
	}
	public MemberPenalizeRecords(
			long memberId, 
			String memberNickname, 
			MemberStatusEnum original, 
			MemberStatusEnum arrive, 
			ForumCalendarUnitEnum rebornUnit, 
			int rebornLimit, 
			String reason, 
			String evidences, 
			long judger,
			String judgeNickname) {
		this.memberId = memberId;
		this.memberNickname = memberNickname;
		this.original = original;
		this.arrive = arrive;
		this.rebornDateTime = LocalDateTime.now();
		this.rebornUnit = rebornUnit;
		this.rebornumber = rebornLimit;
		this.rebirthDateTime = rebornUnit.plusDateTime(rebornLimit, rebornDateTime);
		this.reason = reason;
		this.evidences = evidences;
		
		this.judger = judger;
		this.judgeNickname = judgeNickname;
		
		this.status = true;
		this.entryDateTime = LocalDateTime.now();
		
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getMemberId() {
		return memberId;
	}
	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}
	public String getMemberNickname() {
		return memberNickname;
	}
	public void setMemberNickname(String memberNickname) {
		this.memberNickname = memberNickname;
	}
	public MemberStatusEnum getOriginal() {
		return original;
	}
	public void setOriginal(MemberStatusEnum original) {
		this.original = original;
	}
	public MemberStatusEnum getArrive() {
		return arrive;
	}
	public void setArrive(MemberStatusEnum arrive) {
		this.arrive = arrive;
	}
	public LocalDateTime getEntryDateTime() {
		return entryDateTime;
	}
	public void setEntryDateTime(LocalDateTime entryDateTime) {
		this.entryDateTime = entryDateTime;
	}
	public LocalDateTime getRebornDateTime() {
		return rebornDateTime;
	}
	public void setRebornDateTime(LocalDateTime rebornDateTime) {
		this.rebornDateTime = rebornDateTime;
	}
	public ForumCalendarUnitEnum getRebornUnit() {
		return rebornUnit;
	}
	public void setRebornUnit(ForumCalendarUnitEnum rebornUnit) {
		this.rebornUnit = rebornUnit;
	}
	public int getRebornumber() {
		return rebornumber;
	}
	public void setRebornumber(int rebornumber) {
		this.rebornumber = rebornumber;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public long getJudger() {
		return judger;
	}
	public void setJudger(long judger) {
		this.judger = judger;
	}
	public String getJudgeNickname() {
		return judgeNickname;
	}
	public void setJudgeNickname(String judgeNickname) {
		this.judgeNickname = judgeNickname;
	}
	public String getEvidences() {
		return evidences;
	}
	public void setEvidences(String evidences) {
		this.evidences = evidences;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	@Transient
	public String getDuration(){
		return getRebornumber()+getRebornUnit().getTitle();
	}
	public LocalDateTime getRebirthDateTime() {
		return rebirthDateTime;
	}
	public void setRebirthDateTime(LocalDateTime rebirthDateTime) {
		this.rebirthDateTime = rebirthDateTime;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((arrive == null) ? 0 : arrive.hashCode());
		result = prime * result + (int) (memberId ^ (memberId >>> 32));
		result = prime * result + (status ? 1231 : 1237);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MemberPenalizeRecords other = (MemberPenalizeRecords) obj;
		if (arrive != other.arrive)
			return false;
		if (memberId != other.memberId)
			return false;
		if (status != other.status)
			return false;
		return true;
	}
	
}
