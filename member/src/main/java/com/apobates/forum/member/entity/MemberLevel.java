package com.apobates.forum.member.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

/**
 * 会员等级定义
 * @author xiaofanku@live.cn
 * @since 20170512|20190303
 */
@Entity
@Table(name = "apo_member_level", uniqueConstraints={@UniqueConstraint(columnNames={"names"})})
public class MemberLevel implements Serializable{
	private static final long serialVersionUID = -7306285159772851576L;
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;
    /**
     * 等级名称
     */
    private String names;
    /**
     * 最少积分
     */
    @Column(precision=10, scale=2)
    private double minScore;
    /**
     * 上限积分
     */
    @Column(precision=10, scale=2)
    private double score;
    /**
     * 等级图标
     */
    private String imageAddr;
    /**
     * 状态,true表示可用,false表示删除
     */
    private boolean status;
    //empty constructor for JPA instantiation
	public MemberLevel() {}
	//从DB来的
	public MemberLevel(int id, String names, double minScore, double score, String imageAddr, boolean status) {
		super();
		this.id = id;
		this.names = names;
		this.minScore = minScore;
		this.score = score;
		this.imageAddr = imageAddr;
		this.status=status;
	}
	public MemberLevel(String names, double minScore, double score, String imageAddr, boolean status) {
		super();
		this.id = 0;
		this.names = names;
		this.minScore = minScore;
		this.score = score;
		this.imageAddr = imageAddr;
		this.status=status;
	}
	public int getId() {
		return id;
	}

	public String getNames() {
		return names;
	}

	public double getMinScore() {
		return minScore;
	}

	public double getScore() {
		return score;
	}

	public String getImageAddr() {
		return imageAddr;
	}
	
	public boolean isStatus() {
		return status;
	}
	//--------------------------------------------SET
	public void setId(int id) {
		this.id = id;
	}

	public void setNames(String names) {
		this.names = names;
	}

	public void setMinScore(double minScore) {
		this.minScore = minScore;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public void setImageAddr(String imageAddr) {
		this.imageAddr = imageAddr;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	@Transient
	public static MemberLevel empty() {
		return new MemberLevel("NUL", 0, 0, "-", true) ;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((names == null) ? 0 : names.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MemberLevel other = (MemberLevel) obj;
		if (names == null) {
			if (other.names != null)
				return false;
		} else if (!names.equals(other.names))
			return false;
		return true;
	}
	
}
