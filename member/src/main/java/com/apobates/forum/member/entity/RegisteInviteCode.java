package com.apobates.forum.member.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
/**
 * 会员注册邀请码
 * @author xiaofanku
 * @since 20190711
 */
@Entity
@Table(name = "apo_member_invite", uniqueConstraints={@UniqueConstraint(columnNames={"unicode"})})
public class RegisteInviteCode implements Serializable{
	private static final long serialVersionUID = -8245464080092495155L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	//邀请码
	@Column(nullable=false, unique = true, length=10)
	private String unicode;
	//谁发放的
	private String buildLeader;
	//是否有效
	private boolean status;
	//是否被激活
	private boolean active;
	//邀请码的生成日期
	private LocalDateTime buildDateTime;
	//激活日期
	private LocalDateTime activeDateTime;
	//激活的会员id
	private long memberId;
	//激活的会员登录名称
	private String memberNames;
	
    //empty constructor for JPA instantiation
	public RegisteInviteCode() {}
	public RegisteInviteCode(String leader, String code){
		this.unicode = code;
		this.buildLeader = leader;
		this.buildDateTime = LocalDateTime.now();
		this.status = true;
		this.active = false;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public LocalDateTime getBuildDateTime() {
		return buildDateTime;
	}
	public void setBuildDateTime(LocalDateTime buildDateTime) {
		this.buildDateTime = buildDateTime;
	}
	public LocalDateTime getActiveDateTime() {
		return activeDateTime;
	}
	public void setActiveDateTime(LocalDateTime activeDateTime) {
		this.activeDateTime = activeDateTime;
	}
	public long getMemberId() {
		return memberId;
	}
	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}
	public String getMemberNames() {
		return memberNames;
	}
	public void setMemberNames(String memberNames) {
		this.memberNames = memberNames;
	}
	public String getUnicode() {
		return unicode;
	}

	public void setUnicode(String unicode) {
		this.unicode = unicode;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 29 * hash + Objects.hashCode(this.unicode);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final RegisteInviteCode other = (RegisteInviteCode) obj;
		if (!Objects.equals(this.unicode, other.unicode)) {
			return false;
		}
		return true;
	}

	public String getBuildLeader() {
		return buildLeader;
	}

	public void setBuildLeader(String buildLeader) {
		this.buildLeader = buildLeader;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	@Transient
	public boolean isEmerge(){
		return isActive() && isStatus();
	}
}
