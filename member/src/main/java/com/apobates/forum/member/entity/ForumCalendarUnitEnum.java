package com.apobates.forum.member.entity;

import java.time.LocalDateTime;
import com.apobates.forum.utils.DateTimeUtils;
import com.apobates.forum.utils.lang.EnumArchitecture;

/**
 * 日期单位枚举
 * 
 * @author xiaofanku@live.cn
 * @since 20170604|20190303
 */
public enum ForumCalendarUnitEnum implements EnumArchitecture {
	MINUTE(1, "分钟"){
		@Override
		protected LocalDateTime plug(int calcStep, LocalDateTime startDateTime) {
			return DateTimeUtils.addMinuteForDate(startDateTime, calcStep);
		}
	}, 
	HOUR(2, "小时"){
		@Override
		protected LocalDateTime plug(int calcStep, LocalDateTime startDateTime) {
			return DateTimeUtils.addHourForDate(startDateTime, calcStep);
		}
	}, 
	DAY(3, "天"){
		@Override
		protected LocalDateTime plug(int calcStep, LocalDateTime startDateTime) {
			return DateTimeUtils.addDayForDate(startDateTime, calcStep);
		}
	}, 
	WEEK(4, "周"){
		@Override
		protected LocalDateTime plug(int calcStep, LocalDateTime startDateTime) {
			return startDateTime.plusWeeks(calcStep);
		}
	},
	MONTH(5, "月"){
		@Override
		protected LocalDateTime plug(int calcStep, LocalDateTime startDateTime) {
			return DateTimeUtils.addMonthForDate(startDateTime, calcStep);
		}
	}, 
	YEAR(6, "年"){
		@Override
		protected LocalDateTime plug(int calcStep, LocalDateTime startDateTime) {
			return DateTimeUtils.addYearForDate(startDateTime, calcStep);
		}
	};

	private final String title;
	private final int symbol;

	private ForumCalendarUnitEnum(int symbol, String title) {
		this.title = title;
		this.symbol = symbol;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public int getSymbol() {
		return symbol;
	}
	//
	public LocalDateTime plusDateTime(int number, LocalDateTime start) {
		int calcStep = (number<=0)?1:number;
		LocalDateTime startDateTime = (start == null)?LocalDateTime.now():start;
		//
		return this.plug(calcStep, startDateTime);
	}
	
	protected abstract LocalDateTime plug(int calcStep, LocalDateTime startDateTime);
}
