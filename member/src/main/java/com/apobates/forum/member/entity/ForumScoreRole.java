package com.apobates.forum.member.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.apobates.forum.event.elderly.ForumActionEnum;
/**
 * 积分规则
 * @author xiaofanku
 * @since 20190402
 */
@Entity
@Table(name = "apo_score_role")
public class ForumScoreRole implements Serializable{
	private static final long serialVersionUID = 3373778467637785034L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int id;
	//动作
	@Basic
	@Enumerated(EnumType.STRING)
	private ForumActionEnum action;
	//频次
	private int degree;
	//等级
	private int level;
	//得分
	@Column(precision=5, scale=2)
	private double score;
	//是否可用
	private boolean status;
	//empty constructor for JPA instantiation
	public ForumScoreRole() {}
	public ForumScoreRole(int id, ForumActionEnum action, int degree, int level, double score, boolean status) {
		super();
		this.id = id;
		this.action = action;
		this.degree = degree;
		this.level = level;
		this.score = score;
		this.status = status;
	}
	public ForumScoreRole(ForumActionEnum action, int degree, int level, double score, boolean status) {
		super();
		this.id = 0;
		this.action = action;
		this.degree = degree;
		this.level = level;
		this.score = score;
		this.status = status;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public ForumActionEnum getAction() {
		return action;
	}
	public void setAction(ForumActionEnum action) {
		this.action = action;
	}
	public int getDegree() {
		return degree;
	}
	public void setDegree(int degree) {
		this.degree = degree;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	@Transient
	public static ForumScoreRole empty() {
		return new ForumScoreRole(ForumActionEnum.TOPIC_PUBLISH, 1, 1, 0, true);
	}
}
