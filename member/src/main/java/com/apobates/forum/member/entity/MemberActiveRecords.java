package com.apobates.forum.member.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.apobates.forum.event.elderly.ForumActionEnum;
/**
 * 会员操作记录
 * @author xiaofanku
 * @since 20190304
 */
@Entity
@Table(name = "apo_member_activerecs")
public class MemberActiveRecords implements Serializable{
	private static final long serialVersionUID = -4908946487410402348L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	//成功了大于0
	private long memberId;
	//登录帐号
	private String memberNames;
	//是否成功了
	private boolean succeed;
	//活跃日期
	private LocalDateTime activeDateTime;
	//使用的ip
	private String ipAddr;
	//来源
	private String referrer;
	//操作的token
	private String token;
	//设备
	private String device;
	//
	private String agent;
	@Basic
	@Enumerated(EnumType.STRING)
	private ForumActionEnum action;
	//供应商:联通|电信
	private String isp;
	//地域:省
	private String province;
	//地域:市
	private String city;
	//地域:区
	private String district;
	//empty constructor for JPA instantiation
	public MemberActiveRecords() {}
	
	public MemberActiveRecords(ForumActionEnum action, long memberId, String memberNames, String ipAddr, String referrer, String token, String device, String agent) {
		super();
		this.action = action;
		this.id = 0L;
		this.memberId = memberId;
		this.memberNames = memberNames;
		this.succeed = (memberId>0)?true:false;
		this.activeDateTime = LocalDateTime.now();
		this.ipAddr = ipAddr;
		this.referrer = referrer;
		this.token = token;
		this.device = device;
		this.agent = agent;
	}
	public MemberActiveRecords(ForumActionEnum action, boolean status, long memberId, String memberNames, String ipAddr, String referrer, String token, String device, String agent) {
		super();
		this.action = action;
		this.id = 0L;
		this.memberId = memberId;
		this.memberNames = memberNames;
		this.succeed = status;
		this.activeDateTime = LocalDateTime.now();
		this.ipAddr = ipAddr;
		this.referrer = referrer;
		this.token = token;
		this.device = device;
		this.agent = agent;
	}

	public long getId() {
		return id;
	}

	public long getMemberId() {
		return memberId;
	}
	public String getMemberNames() {
		return memberNames;
	}

	public void setMemberNames(String memberNames) {
		this.memberNames = memberNames;
	}

	public boolean isSucceed() {
		return succeed;
	}

	public LocalDateTime getActiveDateTime() {
		return activeDateTime;
	}

	public String getIpAddr() {
		return ipAddr;
	}

	public String getReferrer() {
		return referrer;
	}

	public String getToken() {
		return token;
	}

	//--------------------------------------------SET
	public void setId(long id) {
		this.id = id;
	}

	public void setMemberId(long memberId) {
		this.memberId = memberId;
	}
	public void setSucceed(boolean succeed) {
		this.succeed = succeed;
	}

	public void setActiveDateTime(LocalDateTime activeDateTime) {
		this.activeDateTime = activeDateTime;
	}

	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}

	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public ForumActionEnum getAction() {
		return action;
	}

	public void setAction(ForumActionEnum action) {
		this.action = action;
	}

	public String getIsp() {
		return isp;
	}

	public void setIsp(String isp) {
		this.isp = isp;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}
	
	public static MemberActiveRecords empty(ForumActionEnum action, long memberId, String memberNames){
		return new MemberActiveRecords(action, false, memberId, memberNames, null, null, null, null, null);
		//(ForumActionEnum action, boolean status, long memberId, String memberNames, String ipAddr, String referrer, String token, String device, String agent)
	}
}
