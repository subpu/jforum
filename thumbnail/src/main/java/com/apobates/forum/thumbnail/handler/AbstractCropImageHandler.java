package com.apobates.forum.thumbnail.handler;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

import com.apobates.forum.utils.image.OverlayBox;
import com.apobates.forum.utils.image.OverlayBox.BoxPosition;
import com.apobates.forum.utils.image.OverlayElement;
import net.coobird.thumbnailator.geometry.Position;
import net.coobird.thumbnailator.geometry.Positions;
/**
 * 抽像的裁剪处理句柄实现类
 * @author xiaofanku
 * @since 20191022
 */
public abstract class AbstractCropImageHandler implements CropImageHandler{
	//被裁剪的图片文件
	private final File orginalImageFile;
	//遮盖元素/水印元素
	private final OverlayBox waterBox;
	/**
	 * 被裁剪的原始图片文件
	 * 
	 * @return
	 */
	protected File getOrginalImageFile() {
		return orginalImageFile;
	}
	/**
	 * 遮盖盒
	 * 
	 * @return
	 */
	protected OverlayBox getWaterBox() {
		return waterBox;
	}
	/**
	 * 初始化
	 * 
	 * @param orginalImageFile 原始图片文件
	 */
	public AbstractCropImageHandler(File orginalImageFile) {
		this.orginalImageFile = orginalImageFile;
		this.waterBox = null;
	}
	/**
	 * 初始化
	 * 
	 * @param orginalImageFile 原始图片文件
	 * @param overlayBox       遮盖盒
	 */
	public AbstractCropImageHandler(File orginalImageFile, OverlayBox waterBox) {
		this.orginalImageFile = orginalImageFile;
		this.waterBox = waterBox;
	}
	/**
	 * 返回保存图片的扩展名
	 * @see com.apobates.forum.utils.Commons.getFileExtension
	 * @param fileName 文件名
	 * @return
	 */
	protected String getFileExtension(String fileName) {
		//return fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
		if (fileName.indexOf('.') != -1 && fileName.lastIndexOf('.') != fileName.length() - 1) {
			int lastIndex = fileName.lastIndexOf('.');
			return fileName.substring(lastIndex + 1);
		}
		throw new IllegalStateException("from argument query File Extension fail");
	}
	
	/**
	 * 返回保存图片的文件名
	 * 
	 * @return
	 */
	protected String getFileName(File cropSaveFile) {
		//"c:\\home\\snapshot-20191022152122268.png"
		return cropSaveFile.getName(); //snapshot-20191022152122268.png
	}
	/**
	 * 返回水印的透明度,只可用于Thumbnails框架
	 * 
	 * @return
	 */
	protected float getWaterBoxOpacity(){
		if(null == waterBox){
			return 1.0f;
		}
		return waterBox.getConfig().getOpacity();
	}
	/**
	 * 返回水印在图片的位置,只可用于Thumbnails框架<br/>
	 * null位置                            等于Positions.CENTER<br/>
	 * 默认位置                             等于Positions.TOP_LEFT<br/>
	 * 随机位置                             等于Positions.TOP_RIGHT<br/>
	 * 
	 * @return
	 */
	protected Position getThumbnailPosition(){
		if(null == waterBox){
			return Positions.CENTER;
		}
		if(BoxPosition.MIDDLE == waterBox.getConfig().getPosition()){
			return Positions.CENTER;
		}
		if(BoxPosition.BOTTOM_RIGHT == waterBox.getConfig().getPosition()){
			return Positions.BOTTOM_RIGHT;
		}
		if(BoxPosition.RANDOM == waterBox.getConfig().getPosition()){
			return Positions.TOP_RIGHT;
		}
		return Positions.TOP_LEFT;
	}
	/**
	 * 返回水印盒元素的数据
	 * 
	 * @return
	 * @throws IOException
	 */
	protected BufferedImage getWatermark()throws IOException{
		Objects.requireNonNull(waterBox);
		OverlayElement oElement = waterBox.getOverlayElement();
		if(null == oElement){
			throw new IOException("overlay element argument is null");
		}
		if(!oElement.isUsable()){ //文件不存在
			throw new IOException("overlay element is not usable");
		}
		return oElement.getData();
	}
}
