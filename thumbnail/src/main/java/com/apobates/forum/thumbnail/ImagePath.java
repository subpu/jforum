package com.apobates.forum.thumbnail;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import com.apobates.forum.thumbnail.handler.ZoomFixedCropHandler;
import com.apobates.forum.utils.image.OverlayBox;
/**
 * 裁剪图片主类
 * 
 * @author xiaofanku
 * @since 20191022
 */
public class ImagePath {
	//接受:auto|width<x>height
	private final String scaleParameter;
	//图片的缩放比例(1-100)
	private final int imageScale;
	//裁剪保存的目录
	private final File thumbDirectory;
	//被裁剪的图片文件
	private final File sourceImageFile;
	//遮盖盒
	private OverlayBox overlayBox=null;
	
	public ImagePath(String scale, String sourceFilePath, int imageWidthSize, File thumbDir) {
		this.scaleParameter = scale;
		this.imageScale = getScaleNumber(imageWidthSize);
		this.thumbDirectory = thumbDir;
		this.sourceImageFile = new File(sourceFilePath);
	}
	
	/**
	 * 裁剪后的WEB访问地址
	 * 
	 * @param thumbDir
	 * @return
	 */
	public String getThumbWebHref(String thumbDir) {
		return ThumbConstant.WEB_FS + thumbDir + ThumbConstant.WEB_FS + getCropDirect() + ThumbConstant.WEB_FS + sourceImageFile.getName();
	}
	
	/**
	 * 裁剪后的物理地址
	 * 
	 * @return
	 */
	public File getImagePhysicalPath() {
		File ipp = new File(thumbDirectory, ThumbConstant.FS + getCropDirect() + ThumbConstant.FS);
		if (!ipp.exists()) {
			ipp.mkdirs();
		}
		return new File(ipp, ThumbConstant.FS + sourceImageFile.getName());
	}
	
	/**
	 * 参数中的宽度和高度信息
	 * 
	 * @return
	 */
	private Map<String, Integer> getImageWidthAndHeight() {
		Map<String, Integer> data = new HashMap<String, Integer>();
		String[] sas = scaleParameter.split(ThumbConstant.WIDTH_HEIGHT_SPLITER);
		if(sas.length != 2){
			return Collections.EMPTY_MAP;
		}
		data.put(ThumbConstant.IMAGE_WIDTH, Integer.valueOf(sas[0]));
		data.put(ThumbConstant.IMAGE_HEIGHT, Integer.valueOf(sas[1]));
		return data;
	}

	public void makeThumb() throws IOException {
		Map<String, Integer> d = getImageWidthAndHeight();
		int cropWidth=0, cropHeight=0;
		if(!d.isEmpty()){
			cropWidth = d.get(ThumbConstant.IMAGE_WIDTH);
			cropHeight = d.get(ThumbConstant.IMAGE_HEIGHT);
		}
		//
		//全缩模式
		//new ThumbnailsScaleHandler(sourceImageFile, imageScale).cropImage(getImagePhysicalPath(), cropWidth, cropHeight);
		//自适应模式(自动时缩放,固定时裁剪)
		new ZoomFixedCropHandler(sourceImageFile, overlayBox, imageScale).cropImage(getImagePhysicalPath(), cropWidth, cropHeight);
	}
	/**
	 * 设置遮盖盒, 需要在调用makeThumb之前调用
	 * 
	 * @param overlayBox 遮盖盒
	 */
	public void water(OverlayBox overlayBox){
		this.overlayBox = overlayBox;
	}
	/**
	 * 根据图片的宽度返回缩放的值
	 * 
	 * @param originalImageWidth 原始图片的宽度
	 * @return
	 */
	private int getScaleNumber(int originalImageWidth){
		if(originalImageWidth > 1440){
			return 25;
		}
		if(originalImageWidth > 992){
			return 50;
		}
		return 75;
	}
	
	/**
	 * 返回图片裁剪后的存储目录名
	 * 
	 * @return
	 */
	private String getCropDirect(){
		if(scaleParameter.indexOf(ThumbConstant.WIDTH_HEIGHT_SPLITER) == -1){
			return imageScale+"";
		}
		return scaleParameter; //width<x>height
	}
}
