package com.apobates.forum.thumbnail.handler;

import java.io.File;
import java.io.IOException;
/**
 * 裁剪图片处理句柄
 * @author xiaofanku
 * @since 20191022
 */
public interface CropImageHandler {
	/**
	 * 裁剪图片
	 * 
	 * @param cropSaveFile 裁剪后保存文件
	 * @param width        裁剪的宽度,若自动缩放设置0
	 * @param height       裁剪的高度,若自动缩放设置0
	 * @throws IOException
	 */
	void cropImage(File cropSaveFile, int width, int height) throws IOException;
}
