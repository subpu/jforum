package com.apobates.forum.thumbnail;

import java.io.File;

public class ThumbConstant {
	public static final String WIDTH_HEIGHT_SPLITER = "x";
	public static final String IMAGE_WIDTH = "width";
	public static final String IMAGE_HEIGHT = "height";
	public static final String FS = File.separator;
	public static final String WEB_FS = "/";
	public static final String MATCHING_NAME_REGEXP = ".+\\-[0-9]+x[0-9]+\\.(png|jpg|jpeg|gif)";
	public static final String NEWLINE = "\r\n";
}
