package com.apobates.forum.utils.selenium;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 使用selenium WebDriver的WebDriver截屏<br/>
 * 需要在系统的环境变量中设置:webdriver.gecko.driver<br/>
 * {@link https://firefox-source-docs.mozilla.org/testing/geckodriver/Support.html}
 * 
 * @author xiaofanku
 * @since 20191222
 */
public class WebDriverCaptureScreen {
	private final String url; 
	//屏幕的宽和高
	private final int screenWidth;
	private final int screenHeight;
	private final static Logger logger = LoggerFactory.getLogger(WebDriverCaptureScreen.class);
	//https://stackoverflow.com/questions/13832322/how-to-capture-the-screenshot-of-a-specific-element-rather-than-entire-page-usin
	//https://www.guru99.com/take-screenshot-selenium-webdriver.html
	public WebDriverCaptureScreen(String url) {
		super();
		this.url = url;
		this.screenHeight = 1200;
		this.screenWidth = 1920;
	}
	
	public WebDriverCaptureScreen(String url, int screenWidth, int screenHeight) {
		super();
		this.url = url;
		this.screenWidth = screenWidth;
		this.screenHeight = screenHeight;
	}
	
	public File shot()throws IOException{
		return shot(null);
	}
	
	public File shot(org.openqa.selenium.By selector) throws IOException{
		String gd = System.getenv("webdriver.gecko.driver");
		if(null == gd){
			throw new IOException("webdriver.gecko.driver file no find");
		}
		//
		System.setProperty("webdriver.gecko.driver", gd);
		FirefoxOptions options = new FirefoxOptions().setProfile(new FirefoxProfile());
		WebDriver driver = new FirefoxDriver(options);
		driver.manage().window().setSize(new Dimension(screenWidth,screenHeight)); 
		driver.get(url);
		// Get entire page screenshot
		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		if(null==selector){
			return screenshot;
		}
		//
		WebElement ele = driver.findElement(selector);
		Rectangle rect = ele.getRect();
		logger.info("coordinate x: "+rect.getX()+", coordinate y: "+rect.getY());
		logger.info("Rectangle width: "+rect.getWidth()+", Rectangle height: "+rect.getHeight());
		//
		BufferedImage fullImg = ImageIO.read(screenshot);
		logger.info("Full width: "+fullImg.getWidth()+", Height: "+fullImg.getHeight());
		// Crop the entire page screenshot to get only element screenshot
		SeleniumCoordinate sc = new SeleniumCoordinate(screenWidth, fullImg.getWidth(), rect.getWidth());
		//
		int resizeHeight = sc.getResizeHeightValue(rect.getHeight()); 
		int leftTopY = sc.getLeftTopY(rect.getY());
		logger.info("resize Height: "+resizeHeight+", left top Y: "+leftTopY);
		BufferedImage eleScreenshot= fullImg.getSubimage(sc.getLeftTopX(), leftTopY, sc.getWidth(), resizeHeight);
		//
		long nt = System.nanoTime();
		File posterShot = File.createTempFile(""+nt, "png");
		ImageIO.write(eleScreenshot, "png", posterShot);
		posterShot.deleteOnExit();
		driver.quit();
		return posterShot;
	}
}
