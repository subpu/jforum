# jforum

#### 简介
一个SpringMVC Spring JPA单体的小论坛, 这不是分布式项目, 没有使用全文搜索，不是一个前后端分离程序，没有集成MQ中间件。~~项目的示例网址: [Orion jForum](http://www.subpu.com)~~


#### 框架
项目基于JDK 8+, 主要使用: Spring 5, Spring MVC 5, JPA2.1(基于EclipseLink实现)实现一个基础的小论坛. 其它增加用户粘性的功能后期会以插件的方式接入


#### 应用说明

1.  基础模块: member, core, letter(内部消息), strategy(策略模块), utils(工具), decorater(装饰模块), event(事件定义), bucket(站外图片项目), thumbnail(图片裁剪), trident(WEB模块)

2.  项目(utils.selenium)的截屏采用webdriver.gecko.driver驱动, 需要设置它的系统变量，相关文章请参考: 
[可以使用selenium的WebDriver将指定元素截图吗?](https://blog.csdn.net/xiaofanku/article/details/103624135)
[geckodriver](https://firefox-source-docs.mozilla.org/testing/geckodriver/)

3.  若采用站外图片存储(非OSS),你可能需要单独再布署一个项目(bucket+thumbnail+utils), 若图片量不大也可以使用站内存储, 相关文章请参考: [聊聊WEB项目中的图片](https://blog.csdn.net/xiaofanku/article/details/102743469)

4.  话题的相关性(core.tag.nlp)根据标签使用Jaccard方法来显示相关性, 相关文章请参考: 
[推荐系统技术 - 文本相似性计算](https://segmentfault.com/a/1190000005270047)

5.  敏感词过滤采用hankcs 的AhoCorasickDoubleArrayTrie, 若不希望执行过滤可以直接删除(WEB模块:trident)WEB-INF目录下的脏词库(dictionary.txt). 相关文章请参考:
[DoubleArrayTrie和AhoCorasickDoubleArrayTrie的实用性对比](https://www.hankcs.com/program/algorithm/double-array-trie-vs-aho-corasick-double-array-trie.html), 

6.  图片的水印(thumbnail)支持图片和文字,存放位置固定在(bucket)项目的watermark目录中,图片名称为image.png,文字为text.properties. 资源文件中有相关设置(文字内容,字号,字体,颜色,是否粗体,宽和高在字体是系统内置时一般设置为0)

7.  WEB站点(trident, spring mvc项目), 所有配置信息都在resources下的global.properties文件中,默认的项目地址为: center.test.com, 图片站外存储的项目地址为: pic.test.com,也就是哪个bucket项目. UI使用Bootstrap加自定义样式. 管理端的路径为:center.test.com/admin/. 第一个注册的会员默认为管理员

8.  关于多设备适配使用spring-mobile-device 加自定义模板, PC模板的路径为: /WEB-INF/layout/page/default/, tablet(平板)模板的路径为: /WEB-INF/layout/page/tablet(进行中), mobile(手机)模板的路径为: /WEB-INF/layout/page/mobile(未开始)。 

9.  模板主要使用jsp配合自定义标签库(com.apobates.forum.trident.tag包)。更多信息: [模板的使用手册](https://gitee.com/apobates/jforum/wikis/%E6%A8%A1%E6%9D%BF%E7%9A%84%E4%BD%BF%E7%94%A8%E6%89%8B%E5%86%8C?sort_id=2136079)

10. 会员信息默认使用cookie存储(member.store模块), 客户端使用store.js来缓存. trident模块中的com.apobates.forum.member.storage.session.OnlineMemberSessionStorage是Session存储(不建议使用)。存储已完成抽像您也可以使用mongodb或redis等内存库来实现。实现完(或切换成session来存储)需要到TridentFrontConfig类中完成配置。

```
public class TridentFrontConfig implements WebMvcConfigurer{
        //ETC
	@Bean(name="onlineMemberStorage")
	public OnlineMemberStorage getMemberStorage(CookieMetaConfig cookieConfig){
		return new com.apobates.forum.member.storage.cookie.HttpCookieProvider(cookieConfig);
	}
}
```

11.  话题的修改和删除的提示消息内容

11.1 JSP(topic/view)使用自定义标签来实现:

```
<forum:decorate posts = "${topic.content}">
       <forum:blockTip><p class="alert-tip bg-danger"><strong>提示:</strong> 作者被禁止发言或内容自动屏蔽</p></forum:blockTip>
       <forum:editTip><p class="alert-tip bg-warning">回复最近由 ${modifyer} 于 ${modifyDate} 编辑</p></forum:editTip>
</forum:decorate>
```

11.2 若是JS异步加载, 项目在获得JSON后使用[Mustache.js](https://github.com/janl/mustache.js/)来回填数据. 例如:forum.core.js

```  
function drawReplier(jsonArray){
    var targetEleSelector = $('#topic_posts_collect');
        targetEleSelector.find('.dynamic-posts-record').remove();
    var obj = {};
        obj.result = jsonArray;
    
    var T='{#result}'
        +' //ETC '
        +'        <div class="posts-body-content">'
        +'          {#block}<p class="alert-tip bg-danger"><strong>提示:</strong> 作者被禁止发言或内容自动屏蔽</p>{/block}'
        +'          {^block}'
        +'            {&content}{&modify}'
        +'          {/block}'
        +' //ETC';
    var extRS={
        modify : function(){
            return this.modifyDate.length == 0?'':'<p class="alert-tip bg-warning">回复最近由 '+this.modifyer+' 于 '+this.modifyDate+' 编辑</p>';
        },
        APP : BASE
    };
    Mustache.parse(T, ['{', '}']);
    var rs = $.extend(obj, extRS);
    //ETC

}
```

block： true表示已经删除了, false表示正常


12.  日志文件默认存储到/home/test/logs目录中,有三个: jforum(trident模块), jforum-sql(jpa日志), bucket(bucket模块). 若需变更需要到trident的resources目录中的log4j2.xml, trident的resources目录中META-INF/persistence.xml, bucket的resources目录中的log4j2.xml. 以上说的都是在代码级别的路径

13.  策略检查{strategy(策略模块)}是针对实体(版块,话题)配置文件的检查, 它前接A: VerifySentinel<T>, 后复合B: GalaxSupplyStrategy

13.1 A可以作操作者检查, 被操作实体的检查. 例会员 张三收藏话题(xxx), 张三即是操作者, 话题(xxx)即为被操作的实体。包名: com.apobates.forum.core.verify

13.2 B是扩展策略的实现者， 可以在配置文件检查完后进行其它业务的检查。包名: com.apobates.forum.strategy.galax

13.3 版块的配置文件决定此版块下的话题的发布, 版块阅读的角色，组和等级要求， 话题的配置文件决定此话题下的回复, 话题阅读的角色，组和等级要求

14. 关于代码和布署的信息, 请访问项目的:[wiki](https://gitee.com/apobates/jforum/wikis/)
#### 感谢

1.  hankcs的分词HanLP和双数组Trie
2.  [ip2region](https://gitee.com/lionsoul/ip2region) IPV4地址库


