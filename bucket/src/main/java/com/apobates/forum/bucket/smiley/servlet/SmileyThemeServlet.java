package com.apobates.forum.bucket.smiley.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.apobates.forum.utils.Commons;
import com.google.gson.Gson;

//输出表情目录下的所有表情风格目录名
public class SmileyThemeServlet extends HttpServlet{
	private static final long serialVersionUID = -525068467128221277L;
	private String smileyDirectName; 
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String smileyBasePath = request.getServletContext().getRealPath(smileyDirectName + "/");
		List<String> data = Commons.queryFolderOfDirectoryName(smileyBasePath);
		//
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		try(PrintWriter out = response.getWriter()){
			out.write(new Gson().toJson(data));
			out.flush();
		}
	}
	
	/**
	 * 获得初始化值
	 */
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		this.smileyDirectName = config.getInitParameter("directName"); //表情保存的目录
	}
}
