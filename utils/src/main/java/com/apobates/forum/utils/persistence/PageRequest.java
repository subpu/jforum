package com.apobates.forum.utils.persistence;
/**
 * 分页查询参数类型的一个实现
 * Pageable pageable = new PageRequest(pages, pageSize);
  Page<T> page = TDao.findAll(pageable);
 * @author xiaofanku@live.cn
 */
public class PageRequest implements Pageable{
	private static final long serialVersionUID = 1L;
	private final int page;
    private final int pageViewRecords;
    
    public PageRequest(int page, int pageSize) {
        this.page = page;
        this.pageViewRecords = pageSize;
    }
    
    @Override
    public int getOffset() {
        return (page - 1) * pageViewRecords;
    }
    
    @Override
    public int getPageNumber() {
        return page;
    }
    
    @Override
    public int getPageSize() {
        return pageViewRecords;
    }
}
