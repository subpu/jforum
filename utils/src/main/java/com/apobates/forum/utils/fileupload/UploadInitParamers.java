package com.apobates.forum.utils.fileupload;

import javax.servlet.http.HttpServletRequest;

/**
 * 上传的参数接口
 * @author xiaofanku@live.cn
 * @since 20170419
 */
public interface UploadInitParamers extends CommonInitParamers{
    /**
     * 上传的输入项名字
     * @return 
     */
    String getUploadFileInputName();
    
    /**
     * http servlet request
     * @return 
     */
    HttpServletRequest getRequest();
}
