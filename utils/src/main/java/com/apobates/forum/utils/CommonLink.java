package com.apobates.forum.utils;

import java.io.Serializable;

public class CommonLink implements Serializable{
	private static final long serialVersionUID = -4949071263599956313L;
	private final String uri;
	private final boolean ajax;
	private final String anchor;
	
	private CommonLink(String uri, boolean ajax, String anchor) {
		super();
		this.uri = uri;
		this.ajax = ajax;
		this.anchor = anchor;
	}
	
	public String getUri() {
		return uri;
	}
	
	public boolean isAjax() {
		return ajax;
	}
	
	public String getAnchor() {
		return anchor;
	}
	
	public static class CommonLinkBuilder{
		private String url;
		private String anchor;
		private boolean ajax;
		
		private CommonLinkBuilder(String url, String anchor){
			this.url = url;
			this.anchor = anchor;
			this.ajax = false;
		}
		
		public static CommonLinkBuilder setAnchor(String anchor){
			return new CommonLinkBuilder("#", anchor);
		}
		
		public CommonLinkBuilder setLink(String link){
			this.url = link;
			return this;
		}
		
		public CommonLinkBuilder isAjax(boolean ajaxLink){
			this.ajax = ajaxLink;
			return this;
		}
		
		public CommonLink build(){
			return new CommonLink(this.url, this.ajax, this.anchor);
		}
	}
}
