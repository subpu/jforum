package com.apobates.forum.utils.image;

import java.awt.image.BufferedImage;
import java.io.IOException;
/**
 * 遮盖元素抽像接口,现可用实现:<br/>
 * OverlayImageElement 图片类型的遮盖元素<br/>
 * OverlayTextElement  文本类型的遮盖元素<br/>
 * 
 * @author xiaofanku
 * @since 20200329
 */
public interface OverlayElement {
	/**
	 * 返回元素的数据
	 * 
	 * @return
	 * @exception IOException
	 * @exception OverlayBoxException
	 */
	BufferedImage getData() throws IOException, OverlayBoxException;
	
	/**
	 * 是否可用,true可用
	 * 
	 * @return
	 */
	boolean isUsable();
}
