package com.apobates.forum.utils.persistence;

import java.io.Serializable;
/**
 * 标记接口
 * @author xiaofanku@live.cn
 * @param <T> 实例类型
 * @param <ID> 主键ID类型
 */
public interface Repository<T, ID extends Serializable> {

}
