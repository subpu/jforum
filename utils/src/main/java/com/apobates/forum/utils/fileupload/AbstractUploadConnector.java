package com.apobates.forum.utils.fileupload;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.io.FilenameUtils;

public abstract class AbstractUploadConnector {
	private final UploadHandler handler;
	protected final static String NEWLINE = "\r\n";

	public AbstractUploadConnector(UploadHandler handler) {
		super();
		this.handler = handler;
	}
	/**
	 * 检查文件的扩展名是否被允许上传
	 * 
	 * @param originalFileName 上传文件的原始名称
	 * @return
	 * @throws IOException
	 */
	protected boolean checkFileExtension(String originalFileName) throws IOException {
		// 检查上传允许的文件扩展名
		String fe = FilenameUtils.getExtension(originalFileName);
		List<String> allowFileExt = Arrays.asList(handler.allowFileExtension());
		if (!allowFileExt.contains(fe.toLowerCase())) {
			handler.forceException(new IOException("非法的文件类型, 目前只支持: " + String.join(",", allowFileExt) + "类型的文件"));
			return false;
		}
		return true;
	}
	
	/**
	 * 检查文件的大小是否超出了被允许上传的单文件最大容量
	 * 注意::SizeLimitExceededException异常::,一旦超出了设置的阀值请求会被拒绝.程序无法获取后续的操作权
	 * Part上传, 有MultiPartConfig设置(!=-1). 此方法可以不用调(容器会检查)
	 * ASF fileupload上传, 在FileUploadBase子类上有调用setSizeMax方法. 此方法可以不用调(FileUploadBase$FileItemIteratorImpl会检查)
	 * 
	 * @param originalFileByteSize 上传文件的大小,单位字节/Byte
	 * @return
	 * @throws IOException
	 */
	protected boolean checkFileByteSize(long originalFileByteSize) throws IOException {
		//检查允许的
		if (handler.allowMaxBytes() != -1 && originalFileByteSize > handler.allowMaxBytes()) {
			handler.forceException(new IOException("文件超出了允许的最大容量: " + handler.allowMaxBytes() + "(字节)"));
			return false;
		}
		return true;
	}

	public UploadHandler getHandler() {
		return handler;
	}

}
