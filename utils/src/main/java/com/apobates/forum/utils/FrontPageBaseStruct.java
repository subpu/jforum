package com.apobates.forum.utils;
/**
 * 前端分页
 * @author xiaofanku@live.cn
 * @since 20181119
 */
public interface FrontPageBaseStruct {
	/**
	 * 分页的页码
	 * 
	 * @return
	 */
	int getPage();

	/**
	 * 总页数
	 * 
	 * @return
	 */
	int getTotalPages();

	/**
	 * 第一页的连接地址
	 * 
	 * @return
	 */
	String getFirstLink();

	/**
	 * 最后一页的连接地址
	 * 
	 * @return
	 */
	String getLastLink();

	/**
	 * 上一页的连接地址 不可以为null,当不存在上一页时(第一页)建议使用#或javascript:;等安全的地址
	 * 
	 * @return
	 */
	String getPreviousLink();

	/**
	 * 下一页的连接地址 不可以为null,当不存在下一页时(最后一页)建议使用#或javascript:;等安全的地址
	 * 
	 * @return
	 */
	String getNextLink();
}
