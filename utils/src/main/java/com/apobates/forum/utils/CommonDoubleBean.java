package com.apobates.forum.utils;

import java.io.Serializable;

public class CommonDoubleBean implements Serializable{
	private static final long serialVersionUID = -8695062954072685003L;
	private final double value;
	private final String title;
	private final int id;
	
	public CommonDoubleBean(int id, String title, double value) {
		super();
		this.id = id;
		this.title = title;
		this.value = value;
	}

	public double getValue() {
		return value;
	}

	public String getTitle() {
		return title;
	}

	public int getId() {
		return id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(value);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommonDoubleBean other = (CommonDoubleBean) obj;
		if (Double.doubleToLongBits(value) != Double.doubleToLongBits(other.value))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
	
	public static CommonDoubleBean empty() {
		return new CommonDoubleBean(0, "VA", 0);
	}
}
