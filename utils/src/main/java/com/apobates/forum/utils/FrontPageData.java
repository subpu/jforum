package com.apobates.forum.utils;

import java.io.Serializable;

/**
 * 前端分页
 * @author xiaofanku@live.cn
 * @since 20180808
 */
public interface FrontPageData extends Serializable{
    /**
     * 分页的连接地址
     * @return 
     */
    String getPageURL();
    /**
     * 每页显示的记录数
     * @return 
     */
    int getPageSize();
    /**
     * 总记录数
     * @return 
     */
    long getRecords();
    /**
     * 分页的页码
     * @return 
     */
    int getPage();
}
