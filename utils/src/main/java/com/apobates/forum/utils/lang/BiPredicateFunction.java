package com.apobates.forum.utils.lang;

import java.util.function.BiPredicate;
import org.apache.commons.lang3.tuple.ImmutablePair;

/**
 * 双参数的执行函数
 * @deprecated
 * @param <T> 条件的第一个参数类型
 * @param <U> 条件的第二个参数类型
 * @author xiaofanku
 * @since 20200805
 */
public interface BiPredicateFunction<T, U> {
    /**
     * 返回函数的执行条件
     * @return 
     */
    public BiPredicate<T, U> getCondtion();
    
    /**
     * 返回函数的参数
     * @return 
     */
    public ImmutablePair<T, U> getArgument();
    
    /**
     * 为执行条件绑定参数并执行
     * @return 
     */
    default boolean execute(){
        return getCondtion().test(getArgument().getLeft(), getArgument().getRight());
    }
}
