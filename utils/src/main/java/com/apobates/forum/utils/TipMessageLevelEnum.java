package com.apobates.forum.utils;

public enum TipMessageLevelEnum {
	ACC("acc", 1), ERR("err", 0), IBB("ibb", 2);
	private final String level;
	private final int category;

	private TipMessageLevelEnum(String level, int category) {
		this.level = level;
		this.category = category;
	}

	// GET/SET
	public String getLevel() {
		return level;
	}

	public int getCategory() {
		return category;
	}
	
	public static TipMessageLevelEnum getInstance(boolean symbol){
		if(symbol){
			return TipMessageLevelEnum.ACC;
		}
		return TipMessageLevelEnum.ERR;
	}
	
	public static TipMessageLevelEnum getInstance(String level){
		TipMessageLevelEnum data = TipMessageLevelEnum.ERR;
		for(TipMessageLevelEnum tmp : values()){
			if(tmp.getLevel().toLowerCase().equals(level.toLowerCase())){
				data = tmp;
				break;
			}
		}
		return data;
	}
}
