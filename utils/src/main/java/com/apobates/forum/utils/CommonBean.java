package com.apobates.forum.utils;

import java.io.Serializable;
/**
 * 封装适合两个属性的bean
 * 
 * @author xiaofanku
 * @since 20190323
 */
public final class CommonBean implements Serializable{
	private static final long serialVersionUID = 1L;
	private final Long id;
	private final String title;
	
	public CommonBean(Long id, String title) {
		super();
		this.id = id;
		this.title = title;
	}
	public CommonBean(Integer id, String title) {
		this(id.longValue(), title);
	}
	public Long getId() {
		return id;
	}
	public String getTitle() {
		return title;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommonBean other = (CommonBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "{id:" + id + ", title:" + title + "}";
	}
	
	public String toJson(){
		return Commons.toJson(this);
	}
}
