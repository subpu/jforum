package com.apobates.forum.utils.persistence;

import java.io.Serializable;

import com.apobates.forum.utils.FrontPageData;
import com.apobates.forum.utils.FrontPageURL;
/**
 * DAORepository分页查询参数类型
 * @author xiaofanku@live.cn
 */
public interface Pageable extends Serializable {
	/**
	 * 记录的起始位(pageNumber-1) * pageSize
	 * 
	 * @return
	 */
	int getOffset();
	/**
	 * 当前页码值
	 * 
	 * @return
	 */
	int getPageNumber();
	/**
	 * 每页显示的数量
	 * 
	 * @return
	 */
	int getPageSize();
    /**
     * 输出前端分页需要的数据
     * @param url 分页的连接地址
     * @param records 总记录数
     * @return 
     */
    default FrontPageData toData(final FrontPageURL url, final long records){
        final int ps = getPageSize(); 
        final int p = getPageNumber();
        
        return new FrontPageData(){
            @Override
            public String getPageURL() {
                return url.toString();
            }
            @Override
            public int getPageSize() {
                return ps;
            }
            @Override
            public long getRecords() {
                return records;
            }
            @Override
            public int getPage() {
                return p;
            }
        };
    }
}
