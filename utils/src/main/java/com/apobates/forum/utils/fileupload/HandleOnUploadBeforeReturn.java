package com.apobates.forum.utils.fileupload;

import java.io.File;

/**
 * 上传结束后,结果返回前的处理句柄
 * @author xiaofanku@live.cn
 * @since 20170419
 */
public interface HandleOnUploadBeforeReturn {
	void otherProcess(File uploadDir, String fileName, String callBackFunction);
}
