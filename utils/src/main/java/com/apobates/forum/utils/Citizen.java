package com.apobates.forum.utils;

import java.util.Calendar;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

public class Citizen {
	/**
	 * 二代身份证号码
	 */
	private String id;
	/**
	 * 生日
	 */
	private String birthday;
	/**
	 * 出生地 要查询数据库
	 */
	private String birthplace;
	/**
	 * 6位的身份证地区编码
	 */
	private String areacode;
	/**
	 * 是否是未成年 true代表未成年,默认值,false代表成年
	 */
	private boolean isMinority = true;
	/**
	 * 是否是格式合法 false为不合法,默认值.true为合法
	 */
	private boolean isWell = false;

	public Citizen(String id) {
		super();
		this.id = id;
		parser();
	}

	private void parser() {

		if (id.length() == 18 && this.isWellStructured()) {
			this.areacode = StringUtils.substring(id, 0, 6);
			//this.parserZone();
			this.birthday = (StringUtils.substring(id, 6, 14));
			this.parserAge();
		}
	}

	// 是否是格式正确的
	private boolean isWellStructured() {
		boolean symbol = false;
		String idcard_base = StringUtils.substring(id, 0, 17);
		if (StringUtils.substring(id, 17, 18).equals(String.valueOf(this.getVerifyBit(idcard_base)))) {
			symbol = true;
		}
		return this.isWell = symbol;
	}

	private void parserAge() {
		Calendar now = Calendar.getInstance();
		// 当前月数
		String curMonth = DateFormatUtils.format(now, "Md");
		// 出生的月数
		String birMonth = StringUtils.right(birthday, 4);
		// 年龄
		int A = now.get(Calendar.YEAR) - Integer.parseInt(StringUtils.substring(birthday, 0, 4));
		if (Integer.parseInt(curMonth) >= Integer.parseInt(birMonth)) {
			A += 1;
		}
		if (A >= 18) {
			this.isMinority = false;
		}
	}
	/*
	private void parserZone() {
		// 根据地区编码查询数据库的归属地
	}
	*/
	private Character getVerifyBit(String base) {
		if (base.length() != 17) {
			return null;
		}
		// 加权因子
		int[] factor = { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 };
		// 校验码对应值
		char[] verify_number_list = { '1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2' };
		int checksum = 0;
		for (int i = 0; i < base.length(); i++) {
			checksum += Integer.parseInt(StringUtils.substring(base, i, i + 1)) * factor[i];
		}
		int mod = checksum % 11;
		return verify_number_list[mod];
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 查询身份证的出生年月日
	 * 
	 * @return 格式验证失败返回空
	 */
	public String getBirthday() {
		return birthday;
	}

	/**
	 * 查询身份证的户籍地,依赖地区编码数据库
	 * 
	 * @return 格式验证失败或编码数据库不可用返回空
	 */
	public String getBirthplace() {
		return birthplace;
	}

	/**
	 * 查询身份证的地区编码
	 * 
	 * @return 格式验证失败返回空
	 */
	public String getAreacode() {
		return areacode;
	}

	/**
	 * 查询身份证的主人是否是未成年人
	 * 
	 * @return true代表未成年,默认值,false代表成年,如果格式验证失败总是返回true
	 */
	public boolean isMinority() {
		return isMinority;
	}

	/**
	 * 身份证是否是格式合法
	 * 
	 * @return true为合法,false代表格式非法,如果格式验证失败总是返回false
	 */
	public boolean isWell() {
		return isWell;
	}
}
