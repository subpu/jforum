package com.apobates.forum.utils.lang;

/**
 * Predicate接受一个参数返回布尔类型 
 * BiPredicate接受两个参数返回布尔类型 
 * TriFunction接受三个参数返回布尔类型
 * 
 * @param <T>
 *            the type of the first argument to the function
 * @param <U>
 *            the type of the second argument to the function
 * @param <V>
 *            the type of the three argument to the function
 * @author xiaofanku
 * @since 20200805
 */
@FunctionalInterface
public interface TriPredicate<T, U, V> {
	/**
	 * Evaluates this predicate on the given arguments.
	 * 
	 * @param t
	 *            the first input argument
	 * @param u
	 *            the second input argument
	 * @param v
	 *            the three function argument
	 * @return true if the input arguments match the predicate, otherwise false
	 */
	boolean test​(T t, U u, V v);
}
