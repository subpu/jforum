package com.apobates.forum.utils.fileupload;
/**
 * 公共的上传初始化参数接口
 * @author xiaofanku
 */
public interface CommonInitParamers {
    /**
     * 文件的保存目录
     * @return 
     */
    String getFileSaveDir();
    
    /**
     * 回调函数的查询字符串
     * @return 
     */
    String getCallbackFunctionName();
}
