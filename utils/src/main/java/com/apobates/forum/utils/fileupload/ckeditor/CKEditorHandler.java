package com.apobates.forum.utils.fileupload.ckeditor;

import com.apobates.forum.utils.DateTimeUtils;
import com.apobates.forum.utils.fileupload.AbstractHandler;

/**
 * CKeditor V4.6.2上传的回调句柄
 * 
 * @author xiaofanku@live.cn
 * @since 20170419
 */
public class CKEditorHandler extends AbstractHandler {
	public CKEditorHandler(String frontVisitPath) {
		super(frontVisitPath);
	}

	@Override
	public String getResponse(String fileName, String callbackFun) {
		String body = "<script type=\"text/javascript\">";
				body += "window.parent.CKEDITOR.tools.callFunction(" + callbackFun + ", ";
				body += "'" + getFrontVisitPath() + fileName + "', ";
				body += "'上传成功'); ";
				body += "</script>";
		return body;
	}

	@Override
	public String generateFileName() {
		return "snapshot-" + DateTimeUtils.getDateFullFormatString() + ".png";
	}
}
