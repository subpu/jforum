package com.apobates.forum.utils.lang;

/**
 * Supplier不接受参数返回一个类型
 * Function接受一个参数返回一个类型 
 * BiFunction接受两个参数返回一个类型
 * TriFunction接受三个参数返回一个类型
 * 
 * @param <T>
 *            the type of the first argument to the function
 * @param <U>
 *            the type of the second argument to the function
 * @param <V>
 *            the type of the three argument to the function
 * @param <R>
 *            the type of the result of the function
 * @author xiaofanku
 * @since 20200708
 */
@FunctionalInterface
public interface TriFunction<T, U, V, R> {
	/**
	 * Applies this function to the given arguments.
	 *
	 * @param t
	 *            the first function argument
	 * @param u
	 *            the second function argument
	 * @param v
	 *            the three function argument
	 * @return the function result
	 */
	R apply(T t, U u, V v);
}
