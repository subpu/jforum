package com.apobates.forum.utils.persistence;

import java.io.Serializable;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * 通用的Dao接口
 * @author xiaofanku@live.cn
 * @param <T> 实例类型
 * @param <ID> 主键ID类型
 */
public interface DataRepository<T, ID extends Serializable> extends Repository<T, ID> {
	/**
	 * 保存实体
	 * 
	 * @param entity 实体
	 */
	void save(T entity);

	/**
	 * 查看实体
	 * 
	 * @param primaryKey 实体主键
	 * @return 成功返回Result.success(T)
	 */
	Optional<T> findOne(ID primaryKey);

	/**
	 * 编辑实体
	 * @param entity 实体
	 * @return 成功返回Result.success(true)失败返回Result.success(false)遇到异常返回Result.failure(e)
	 */
	Optional<Boolean> edit(T updateEntity);

	/**
	 * 实体所有记录
	 * 
	 * @return 无结果集返回一个空列表
	 */
	Stream<T> findAll();

	/**
	 * 实体的总数量
	 * 
	 * @return 空结果返回0
	 */
	long count();
}
