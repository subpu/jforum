package com.apobates.forum.utils.fileupload;

import java.io.File;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 抽像的处理句柄
 * @author xiaofanku@live.cn
 * @since 20170419
 */
public abstract class AbstractHandler implements UploadHandler{
    private final String frontVisitPath;
    private final static Logger logger=LoggerFactory.getLogger(AbstractHandler.class);
    
    public AbstractHandler(String frontVisitPath){
        this.frontVisitPath=frontVisitPath;
    }
    @Override
    public void forceException(IOException e) throws IOException {
        if(logger.isDebugEnabled()){
            logger.debug("[AHUpload]upload has exception: "+e.getMessage(), e);
        }
        throw e;
    }
    /**
     * 子类可以覆盖方法
     * 
     * @param uploadDir 文件的保存目录
     * @param fileName 文件名
     * @param callBackFunction 回调函数或其它参数的传递
     */
    @Override
    public void otherProcess(File uploadDir, String fileName, String callBackFunction) {
        
    }
    
    public String getFrontVisitPath() {
        return frontVisitPath;
    }
    
    /**
     * 
     * 如果想控制生成的文件名子类可以覆盖方法
     * 
     * @return 
     */
    @Override
    public String generateFileName(){
        return null;
    }
    
    /**
     * 上传允许的文件扩展名, 默认图片格式(jpg,jpeg,png,gif). 子类可以覆盖方法
     * 
     * @return
     */
    @Override
    public String[] allowFileExtension(){
        return new String[]{"jpg", "jpeg", "png", "gif"};
    }
    
    /**
     * 上传允许的最大字节数, 默认不限制. 子类可以覆盖方法
     * 
     * @return
     */
    @Override
    public long allowMaxBytes() {
        return -1;
    }
    
}
