package com.apobates.forum.utils.fileupload;
/**
 * 上传结束后的处理句柄
 * @author xiaofanku@live.cn
 * @since 20170419
 */
public interface HandleOnUploadComplete {
	String getResponse(String fileName, String callFunction);
}
