package com.apobates.forum.utils.fileupload;

import java.io.IOException;

/**
 * 上传过程中出现IO异常时的处理句柄
 * @author xiaofanku@live.cn
 * @since 20170419
 */
public interface HandleOnUploadException {
	void forceException(IOException e) throws IOException;
}
