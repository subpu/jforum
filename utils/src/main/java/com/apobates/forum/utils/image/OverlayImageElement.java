package com.apobates.forum.utils.image;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
/**
 * 图片类型的遮盖元素
 * 
 * @author xiaofanku
 * @since 20200329
 */
public class OverlayImageElement implements OverlayElement{
	//遮盖图片
	private final File overlayImage;
	
	public OverlayImageElement(File overlayImage) {
		super();
		this.overlayImage = overlayImage;
	}
	
	@Override
	public BufferedImage getData() throws IOException, OverlayBoxException{
		return ImageIO.read(overlayImage);
	}
	
	@Override
	public boolean isUsable() {
		return overlayImage.exists();
	}

}
