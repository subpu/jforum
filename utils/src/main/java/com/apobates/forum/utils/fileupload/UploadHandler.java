package com.apobates.forum.utils.fileupload;
/**
 * 整合的句柄接口
 * @author xiaofanku@live.cn
 * @since 20170419
 */
public interface UploadHandler extends HandleOnUploadComplete,HandleOnUploadException,HandleOnUploadBeforeReturn{
    /**
     * 保存的文件名
     * 
     * @return 
     */
    String generateFileName();
    
    /**
     * 允许使用的文件扩展名,必须都小写
     * 
     * @return 
     */
    String[] allowFileExtension();
    
    /**
     * 上传允许的最大字节数
     * 
     * @return
     */
    long allowMaxBytes();
}
