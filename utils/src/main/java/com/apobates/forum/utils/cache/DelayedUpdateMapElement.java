package com.apobates.forum.utils.cache;

import java.time.LocalDateTime;

/**
 * 延迟更新Map的元素
 * 
 * @author xiaofanku
 * @since 20200726
 */
public interface DelayedUpdateMapElement {
    /**
     * Key的值
     * @return 
     */
    String getUnionKey();
    
    /**
     * 活跃日期
     * @return 
     */
    LocalDateTime getActiveDateTime();
}
