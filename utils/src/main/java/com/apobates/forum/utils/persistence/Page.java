package com.apobates.forum.utils.persistence;

import java.util.stream.Stream;
/**
 * 分页查询返回类型
 * @author xiaofanku@live.cn
 * @param <T> 实例类型
 */
public interface Page<T> {
	/**
	 * 总记录数
	 * 
	 * @return
	 */
	long getTotalElements();

	/**
	 * 结果集
	 * 
	 * @return
	 */
	Stream<T> getResult();
	
	public static <T> Page<T> empty() {
		return new Page<T>() {
			@Override
			public long getTotalElements() {
				return 0L;
			}

			@Override
			public Stream<T> getResult() {
				return Stream.empty();
			}
		};
	}
}
