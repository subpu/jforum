package com.apobates.forum.utils.persistence;

import java.io.Serializable;
import java.util.stream.Stream;
/**
 * 分页查询DAO接口
 * @author xiaofanku@live.cn
 * @param <T> 实例类型
 * @param <ID> 主键ID类型
 */
public interface PagingAndSortingRepository<T, ID extends Serializable> extends DataRepository<T, ID> {
    /**
     * 分页查询所有记录
     * @param pageable
     * @return 空结果集返回一个空列表
     */
    Page<T> findAll(Pageable pageable);
    /**
     * 无数据时的结果模板
     * 当总记录数为0时直接调用这个方法
     * @param pageable
     * @return 
     */
    default Page<T> emptyResult(){
        return new Page<T>(){
            @Override
            public long getTotalElements() {
                return 0L;
            }
            
            @Override
            public Stream<T> getResult() {
                return Stream.empty();
            }
        };
    }
}
