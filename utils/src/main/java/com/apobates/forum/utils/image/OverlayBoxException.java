package com.apobates.forum.utils.image;
/**
 * 为图片增加遮盖元素失败的异常
 * 
 * @author xiaofanku
 * @since 20200328
 */
public class OverlayBoxException extends RuntimeException{
	private static final long serialVersionUID = -6052471183728551587L;

	public OverlayBoxException(String message, Throwable cause) {
		super(message, cause);
	}

	public OverlayBoxException(String message) {
		super(message);
	}
	
}
