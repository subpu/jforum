package com.apobates.forum.decorater.Posts;

import com.apobates.forum.attention.core.ImagePathCoverter;
/**
 * 解码回复内容中的表情图片
 * 
 * @author xiaofanku
 * @since 20190604
 */
public class PostsSmileyDecodeContentDecorator extends PostsContentDecorator{
	//http://center.test.com
	private final String siteDomain;
	//smiley
	private final String smileyDirectName;
	
	public PostsSmileyDecodeContentDecorator(Display display, String siteDomain, String smileyDirectNam) {
		super(display, false, null);
		this.siteDomain = siteDomain;
		this.smileyDirectName = smileyDirectNam;
	}

	@Override
	public String getDecoratorContent() {
		String rawContent = super.getDisplay().getContent();
		ImagePathCoverter ipc = new ImagePathCoverter(rawContent);
		return ipc.decodeSmilePath(siteDomain, smileyDirectName).orElse(rawContent);
	}

}
