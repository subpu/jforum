package com.apobates.forum.decorater;

import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;
import com.apobates.forum.attention.core.ImagePathCoverter;
import com.apobates.forum.utils.Commons;
import com.vdurmont.emoji.EmojiParser;
/**
 * 回复内容编码
 * 
 * @author xiaofanku
 * @since 20190610
 */
public class ForumEncoder {
	private final String content;
	
	public ForumEncoder(String content) {
		super();
		this.content = content;
	}
	
	/**
	 * 进行表情图片地址编码
	 * 
	 * @param siteDomain       表情的访问域名,http://center.test.com
	 * @param smileyDirectName 表情保存的目录名
	 * @return
	 */
	public ForumEncoder encodeSmile(String siteDomain, String smileyDirectName){
		ImagePathCoverter ipc = new ImagePathCoverter(getContent());
		String data = ipc.encodeSmilePath(siteDomain, smileyDirectName);
		return new ForumEncoder(data);
	}
	
	/**
	 * 进行上传图片地址编码
	 * 
	 * @param imageSiteDomain      表情的访问域名,
	 * @param uploadFileDirectName 上传图片保存的目录名
	 * @return
	 */
	public ForumEncoder encodeUploadImage(String imageSiteDomain, String uploadFileDirectName){
		ImagePathCoverter ipc = new ImagePathCoverter(getContent());
		String data = ipc.encodeUploadImagePath(imageSiteDomain, uploadFileDirectName);
		return new ForumEncoder(data);
	}
	
	/**
	 * 返回编码后的内容
	 * 
	 * @return
	 */
	public String getContent() {
		return content;
	}
	
	public String getContent(int length) {
		try{
			return content.substring(0, length) + "...";
		}catch(IndexOutOfBoundsException e){
			return content;
		}
	}
	/**
	 * 提取上传的图片编码地址
	 * 
	 * @return
	 */
	public List<CommonImageLinkDigest> extractUploadImageLink(){
		List<CommonImageLinkDigest> data = new ArrayList<>();
		//
		int ranking = 0;
		Document doc = Jsoup.parse(content);
		Elements imgTags = doc.select("img[src]");
		for(Element element : imgTags){
			String imgLink = element.attr("src");
			String imgCaption = Commons.optional(element.attr("alt"), "未填写图片描述");
			if(imgLink.startsWith("image://")){
				data.add(new CommonImageLinkDigest(imgCaption, imgLink, ranking));
				ranking += 1;
			}
		}
		return data;
	}
	
	/**
	 * 清理内容中的html标签,返回的内容只为纯文本
	 * 
	 * @return
	 */
	public ForumEncoder noneHtmlTag(){
		String data = Commons.htmlPurifier(getContent());
		return new ForumEncoder(data);
	}
	
	/**
	 * 编码内容中的emoji 表情符号. <br/>
	 * 一定要在取得getContent()方法前一步调用(若你需要的话),否则其它方法会导致结果回到起点. <br/>
	 * 这在编码为UTF8的数据库上是在灾难的. 你可以将数据库的编码转为UTF8MB4,来一劳永逸的解决. <br/>
	 * 
	 * @return
	 */
	public ForumEncoder parseEmoji(){
		String result = EmojiParser.parseToHtmlDecimal(getContent());
		return new ForumEncoder(result);
	}
	
	/**
	 * 替掉不安全的html标签,最大程度的保留格式标签.可用的标签见:Commons.htmlCleanUsedRelaxed方法签名
	 * 
	 * @return
	 */
	public ForumEncoder relaxedHtmlTag(){
		String data = Jsoup.clean(getContent(), Whitelist.relaxed().addProtocols("img", "src", "smile", "image", "avtar", "video"));
		return new ForumEncoder(data);
	}
	
}
