package com.apobates.forum.decorater.Posts;

/**
 * 装饰器的基础接口
 * 
 * @author xiaofanku@live.cn
 * @since 20171025
 */
public interface Display {
	
	/**
	 * 获取内容
	 * 
	 * @return
	 */
	String getContent();
}
