package com.apobates.forum.decorater.Posts;

import com.apobates.forum.attention.core.ImagePathCoverter;
/**
 * 解码回复内容中的上传图片
 * 
 * @author xiaofanku
 * @since 20190610
 */
public class PostsUploadImageDecodeContentDecorator extends PostsContentDecorator{
	//图片的访问域名
	//http://center.test.com
	private final String imageDomain;
	//imagestore
	private final String uploadDirectName;
	
	/**
	 * 
	 * @param display          装饰器的基础接口
	 * @param lazyLoad         内容中的图片是否懒加载; true进行懒加载,false直接加载
	 * @param imageDomain      图片存储的域名
	 * @param uploadDirectName 图片保存的目录名称
	 */
	public PostsUploadImageDecodeContentDecorator(Display display, boolean lazyLoad, String scale, String imageDomain, String uploadDirectName) {
		super(display, lazyLoad, scale);
		this.imageDomain = imageDomain;
		this.uploadDirectName = uploadDirectName;
	}
	
	@Override
	public String getDecoratorContent() {
		String rawContent = super.getDisplay().getContent();
		ImagePathCoverter ipc = new ImagePathCoverter(rawContent);
		return ipc.decodeUploadImagePath(imageDomain, uploadDirectName, isLazyLoad(), getScale()).orElse(rawContent);
	}
}
