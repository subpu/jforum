package com.apobates.forum.decorater.Posts;

import com.apobates.forum.core.entity.Posts;
/**
 * 装饰对象
 * @author xiaofanku@live.cn
 * @since 20171025
 */
public class PostsDisplay implements Display{
	private final Posts posts;
	
	public PostsDisplay(Posts posts) {
		super();
		this.posts = posts;
	}

	@Override
	public String getContent() {
		return posts.getContent();
	}

}
