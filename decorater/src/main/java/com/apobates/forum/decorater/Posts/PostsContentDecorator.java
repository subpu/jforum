package com.apobates.forum.decorater.Posts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 编辑装饰器
 * 
 * @author xiaofanku@live.cn
 * @since 20171025
 */
public abstract class PostsContentDecorator implements Display{
	private final Display display;
	private final boolean lazyLoad;
	private final String scale;
	private final static Logger logger = LoggerFactory.getLogger(PostsContentDecorator.class);
	
	/**
	 * 
	 * @param display  装饰器的基础接口
	 * @param lazyLoad 是否懒加载图片
	 */
	public PostsContentDecorator(Display display, boolean lazyLoad, String scale) {
		super();
		this.display = display;
		this.lazyLoad = lazyLoad;
		this.scale = scale;
	}
	
	public Display getDisplay() {
		return display;
	}

	public String getScale() {
		logger.info("[SCALE][PostsContentDecorator]image scale: "+scale);
		return scale;
	}

	@Override
	public String getContent() {
		return getDecoratorContent();
	}
	
	public boolean isLazyLoad() {
		return lazyLoad;
	}
	/**
	 * 装饰内容
	 * 
	 * @return
	 */
	public abstract String getDecoratorContent();
}
