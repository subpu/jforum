package com.apobates.forum.decorater;

import java.io.Serializable;
/**
 * 提出图片的公共URL
 * 
 * @author xiaofanku
 * @since 20191218
 */
public class CommonImageLinkDigest implements Serializable{
	private static final long serialVersionUID = 7086909396118845121L;
	private final String anchor;
	private final String url;
	//在内容中出现的顺序
	private final int ranking;
	
	public CommonImageLinkDigest(String anchor, String url, int ranking) {
		super();
		this.anchor = anchor;
		this.url = url;
		this.ranking = ranking;
	}
	
	public String getAnchor() {
		return anchor;
	}
	public String getUrl() {
		return url;
	}
	public int getRanking() {
		return ranking;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommonImageLinkDigest other = (CommonImageLinkDigest) obj;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}
	
}
