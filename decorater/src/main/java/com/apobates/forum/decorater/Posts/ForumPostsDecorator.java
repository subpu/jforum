package com.apobates.forum.decorater.Posts;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apobates.forum.core.entity.Posts;
import com.apobates.forum.utils.Commons;
import com.apobates.forum.utils.sensitive.SensitiveWordFilter;
/**
 * 话题回复装饰器
 * @author xiaofanku
 * @since 20190604
 */
public class ForumPostsDecorator {
  //是否显示
    //不显示或删除--->内容被删除(<div class="alert alert-danger" role="alert"><strong>提示:</strong> 作者被禁止发言或内容自动屏蔽</div>)
    //显示:
      //1):编辑日期(<div class="alert alert-warning" role="alert">该回复最后由 ${posts.getModifyer().getNickname()} 于 <forum:format date="${post.modifyDate}"/> 编辑</div>)
      //2):脏词过滤
      //3):表情解码
      //4):像册
	private final Posts posts;
	private final String content;
	private final static Logger logger = LoggerFactory.getLogger(ForumPostsDecorator.class);
	/**
	 * 初始装饰器
	 * 
	 * @param posts 回复实例
	 * @return
	 */
	public static ForumPostsDecorator init(Posts posts) {
		return new ForumPostsDecorator(posts, null);
	}
	
	private ForumPostsDecorator(Posts posts, String content) {
		this.posts = posts;
		this.content = content;
	}
	/**
	 * [优先调用]设置不显示内容(删除或阻塞)时的提示消息
	 * 
	 * @param blockTipMessage
	 * @return
	 */
	public ForumPostsDecorator block(String blockTipMessage){
		if(!getPosts().isNormal()){
			return new ForumPostsDecorator(null, blockTipMessage);
		}
		return this;
	}
	/**
	 * 解码回复内容中的表情
	 * 
	 * @param siteDomain       域名,http://center.test.com
	 * @param smileyDirectName 表情的目录
	 * @return
	 */
	public ForumPostsDecorator decorateSmileImage(String siteDomain, String smileyDirectName){
		if(null == getPosts()){
			return this;
		}
		if(null!=content){ // 前面处理过一次了?
			posts.setContent(content);
		}
		String rs = new PostsSmileyDecodeContentDecorator(new PostsDisplay(posts), siteDomain, smileyDirectName).getContent();
		return new ForumPostsDecorator(posts, rs);
	}
	/**
	 * 解码回复内容中的上传图片
	 * 
	 * @param imageSiteDomain      图片存储的域名,http://center.test.com
	 * @param uploadFileDirectName 图片保存的目录名称
	 * @param lazyLoad             内容中的图片是否懒加载; true进行懒加载,false直接加载
	 * @param scale                图片的尺寸,若为null表示自动
	 * @return
	 */
	public ForumPostsDecorator decorateUploadImage(String imageSiteDomain, String uploadFileDirectName, boolean lazyLoad, String scale){
		if(null == getPosts()){
			return this;
		}
		
		if(null!=content){ // 前面处理过一次了?
			posts.setContent(content);
		}
		logger.info("[SCALE][ForumPostsDecorator]image scale: "+scale);
		String rs = new PostsUploadImageDecodeContentDecorator(new PostsDisplay(posts), lazyLoad, scale, imageSiteDomain, uploadFileDirectName).getContent();
		return new ForumPostsDecorator(posts, rs);
	}
	/**
	 * [最后调用]设置编辑提示消息
	 * 
	 * @param modifyTipMessage
	 * @return
	 */
	public ForumPostsDecorator modify(String modifyTipMessage){
		if(null == getPosts()){
			return this;
		}
		
		if(null!=posts.getModifyDateTime() && Commons.isNotBlank(posts.getModifyMemberNickname())){
			String rs = (null == content)?posts.getContent():content; 
			return new ForumPostsDecorator(posts, rs +"<br/>"+ modifyTipMessage);
		}
		return this;
	}
	/**
	 * [优先调用+1]执行敏感词过滤
	 * 
	 * @param wordFilter
	 * @return
	 */
	public ForumPostsDecorator sensitiveWord(String dictionaryFilePath){
		if(null == getPosts()){
			return this;
		}
		
		String rs = (null == content)?posts.getContent():content; // 是第一次还是以前有处理过
		try{
			SensitiveWordFilter swf = SensitiveWordFilter.getInstance(dictionaryFilePath);
			String clearBadWordContent = swf.execute(rs);
			//对**套上<mark>标签
			clearBadWordContent = clearBadWordContent.replaceAll("(\\*{1,})", "<mark>$1</mark>");
			return new ForumPostsDecorator(posts, clearBadWordContent);
		}catch(IOException e){
			if(logger.isDebugEnabled()){
				logger.debug("[PDS]脏词词库无法查找");
			}
		}catch(NullPointerException e){
			if(logger.isDebugEnabled()){
				logger.debug("[PDS]内容不可用或脏词词库为空");
			}
		}
		
		return this;
	}
	private Posts getPosts() {
		return posts;
	}
	
	public String getContent() {
		return content;
	}
}
