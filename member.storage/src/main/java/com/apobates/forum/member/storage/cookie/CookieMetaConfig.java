package com.apobates.forum.member.storage.cookie;

import com.apobates.forum.member.storage.MetaConfig;
/**
 * Cookie存储方案的配置文件
 * 
 * @author xiaofanku
 * @since 20191231
 */
public class CookieMetaConfig extends MetaConfig{
	//cookie绑定的域名
	private final String domain;
	//cookie作用域
	private final String path;
	
	public CookieMetaConfig(String name, String domain, String path, String site) {
		super(name, site);
		this.domain = domain;
		this.path = path;
	}
	/**
	 * cookie绑定的域名
	 * 
	 * @return
	 */
	public String getDomain() {
		return domain;
	}
	
	/**
	 * cookie作用域
	 * 
	 * @return
	 */
	public String getPath() {
		return path;
	}

}
