package com.apobates.forum.member.storage.core;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import com.apobates.forum.utils.Commons;
import com.apobates.forum.utils.DateTimeUtils;

/**
 * MemberSessionBean与Map<String,String>的转换器
 * 
 * @author xiaofanku
 * @since 20200905
 */
public class MemberSessionBeanConverter {

    public static Map<String,String> toMap(MemberSessionBean msb){
        Map<String,String> data = new HashMap<>();
        data.put("mid", msb.getMid() + "");
        data.put("names", msb.getNames()); 
        data.put("nickname", msb.getNickname()); 
        data.put("signature", msb.getSignature()); 
        data.put("status", msb.getStatus().getSymbol()+""); 
        data.put("group", msb.getGroup().getSymbol()+"");
        data.put("role", msb.getRole().getSymbol()+"");
        data.put("avatar", msb.getAvatar());
        data.put("birth", DateTimeUtils.getRFC3339(msb.getRegisteDateTime()));
        return Collections.unmodifiableMap(data);
    }
    
    /**
     * 生成不可变的Map集合
     * @param msb
     * @param expireDateTime
     * @return 
     */
    public static Map<String,String> toMap(MemberSessionBean msb, LocalDateTime expireDateTime){
        Map<String,String> data = new HashMap<>();
        data.put("mid", msb.getMid() + "");
        data.put("names", msb.getNames());
        data.put("nickname", msb.getNickname()); 
        data.put("signature", msb.getSignature());
        data.put("status", msb.getStatus().getSymbol()+""); 
        data.put("group", msb.getGroup().getSymbol()+""); 
        data.put("role", msb.getRole().getSymbol()+"");
        data.put("avatar", msb.getAvatar());
        data.put("birth", DateTimeUtils.getRFC3339(msb.getRegisteDateTime())); 
        data.put("expire", DateTimeUtils.getRFC3339(expireDateTime));
        return Collections.unmodifiableMap(data);
    }
    
    public static Optional<MemberSessionBean> toInstance(Map<String,String> data, String ipAddr, String sentinel){
        if(null == data || data.isEmpty()){
            return Optional.empty();
        }
        MemberSessionBeanBuilder msbb = MemberSessionBeanBuilder.empty();
        //
        long mid = Commons.stringToLong(data.getOrDefault("mid", null), -1L);
        if (mid > 0) {
            msbb = msbb.setId(mid);
        }
        //
        msbb = msbb.setNames(data.getOrDefault("names", null));
        msbb = msbb.setNickname(data.getOrDefault("nickname", null));
        msbb = msbb.setSignature(data.getOrDefault("signature", null));
        //
        int statusEnumSymbol = Commons.stringToInteger(data.getOrDefault("status", null), -1);
        msbb = msbb.setStatus(statusEnumSymbol);
        //
        int groupEnumSymbol = Commons.stringToInteger(data.getOrDefault("group", null), -3);
        msbb = msbb.setGroup(groupEnumSymbol);
        //
        int roleEnumSymbol = Commons.stringToInteger(data.getOrDefault("role", null), 0);
        msbb = msbb.setRole(roleEnumSymbol);
        //
        msbb = msbb.setBirthday(data.getOrDefault("birth", null));
        msbb = msbb.setAvatar(data.getOrDefault("avatar", null));
        //
        return Optional.of(msbb.build(ipAddr, sentinel));
    }
}
