package com.apobates.forum.member.storage;

import com.apobates.forum.utils.Commons;
/**
 * 存储方案的配置文件
 * 
 * @author xiaofanku
 * @since 20200101
 */
public class MetaConfig {
	//名称
	private final String name;
	//站点的url,要求http打头
	private final String site;
	
	public MetaConfig(String name, String site) {
		super();
		this.name = name;
		this.site = site;
	}
	
	/**
	 * 存储方案使用的Key名称
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * 站点的url,要求http打头
	 * 
	 * @return
	 */
	public String getSite() {
		return site;
	}
	
	/**
	 * site是否启用了HTTPS,是返回true
	 * 
	 * @return
	 */
	public boolean isHttps(){
		return Commons.isHttpsProtocol(site);
	}
}
