package com.apobates.forum.member.storage;

import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.apobates.forum.member.entity.MemberGroupEnum;
import com.apobates.forum.member.entity.MemberRoleEnum;
import com.apobates.forum.member.entity.MemberStatusEnum;
import com.apobates.forum.member.storage.core.MemberSessionBean;
/**
 * 会员在线信息存储
 * 
 * @author xiaofanku
 * @since 20191231
 */
public interface OnlineMemberStorage {
	/**
	 * 保存
	 * 
	 * @param memberSessionBean
	 * @param request
	 * @param response
	 */
	void store(MemberSessionBean memberSessionBean, HttpServletRequest request, HttpServletResponse response);
	
	/**
	 * 销毁
	 * 
	 * @param request
	 * @param response
	 */
	void delete(HttpServletRequest request, HttpServletResponse response);
	
	/**
	 * 还原
	 * 
	 * @param request  
	 * @param sentinel 一个标识从哪事变的
	 * @return
	 */
	Optional<MemberSessionBean> getInstance(HttpServletRequest request, String sentinel);
	
	/**
	 * 刷新
	 * 
	 * @param request
	 * @param response
	 * @param status   新的状态
	 * @param group    新的管理组
	 * @param role     新的角色
	 */
	void refresh(HttpServletRequest request, HttpServletResponse response, MemberStatusEnum status, MemberGroupEnum group, MemberRoleEnum role);

	/**
	 * 是否支持还原功能
	 * 
	 * @return
	 */
	boolean isSupportRevival();
	
	/**
	 * 存储方案相关的配置文件
	 * 
	 * @return
	 */
	MetaConfig getMetaConfig();
}
