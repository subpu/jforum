package com.apobates.forum.member.storage.core;

import java.time.LocalDateTime;
import com.apobates.forum.member.entity.Member;
import com.apobates.forum.member.entity.MemberGroupEnum;
import com.apobates.forum.member.entity.MemberRoleEnum;
import com.apobates.forum.member.entity.MemberStatusEnum;
import com.apobates.forum.utils.DateTimeUtils;
import com.apobates.forum.utils.lang.EnumArchitecture;
/**
 * 会员身份构造器
 * @author xiaofanku
 * @since 20190623
 */
public class MemberSessionBeanBuilder {
	private long mid = -1L;
	private String avatar = Member.GUEST_AVATAR;
	private MemberStatusEnum status = MemberStatusEnum.ACTIVE; //MemberStatusEnum.GROUND; 不允许游客过多操作
	private MemberGroupEnum group = MemberGroupEnum.GUEST;
	private MemberRoleEnum role = MemberRoleEnum.NO;
	private String signature = "";
	private String nickname = Member.GUEST_NAMES;
	private LocalDateTime registeDateTime=null;
	private String names ="*";
	//
	private MemberSessionBeanBuilder(){}
	public static MemberSessionBeanBuilder empty(){
		return new MemberSessionBeanBuilder();
	}
	
	public MemberSessionBean build(String ipAddr, String sentinel){
		return new MemberSessionBean(
				mid, 
				names,
				avatar, 
				signature, 
				status,
				group, 
				role, 
				registeDateTime, 
				nickname, 
				ipAddr, 
				false, sentinel);
	}
	
	public MemberSessionBeanBuilder setId(long mid){
		this.mid = mid;
		return this;
	}
	public MemberSessionBeanBuilder setNames(String names){
		this.names = names;
		return this;
	}
	public MemberSessionBeanBuilder setNickname(String nickname){
		this.nickname = nickname;
		return this;
	}
	
	public MemberSessionBeanBuilder setSignature(String signature){
		this.signature = signature;
		return this;
	}
	
	public MemberSessionBeanBuilder setAvatar(String avatar){
		this.avatar = avatar;
		return this;
	}
	public MemberSessionBeanBuilder setBirthday(LocalDateTime registeDateTime){
		this.registeDateTime = registeDateTime;
		return this;
	}
	public MemberSessionBeanBuilder setBirthday(String registeDateTime){
		LocalDateTime tmp = LocalDateTime.now();
		try{
			tmp = DateTimeUtils.parseDate(registeDateTime);
		}catch(NullPointerException e){}
		this.registeDateTime = tmp;
		return this;
	}
	public MemberSessionBeanBuilder setStatus(int memberStatusEnumSymbol){
		this.status = EnumArchitecture.getInstance(memberStatusEnumSymbol, MemberStatusEnum.class).orElse(MemberStatusEnum.GROUND);
		return this;
	}
	
	public MemberSessionBeanBuilder setGroup(int memberGroupEnumSymbol){
		this.group = EnumArchitecture.getInstance(memberGroupEnumSymbol, MemberGroupEnum.class).orElse(MemberGroupEnum.GUEST);
		return this;
	}
	
	public MemberSessionBeanBuilder setRole(int memberRoleEnumSymbol){
		this.role = EnumArchitecture.getInstance(memberRoleEnumSymbol, MemberRoleEnum.class).orElse(MemberRoleEnum.NO);
		return this;
	}
}
