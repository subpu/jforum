package com.apobates.forum.member.storage.core;

import java.io.Serializable;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apobates.forum.member.MemberBaseProfile;
import com.apobates.forum.member.entity.Member;
import com.apobates.forum.member.entity.MemberGroupEnum;
import com.apobates.forum.member.entity.MemberRoleEnum;
import com.apobates.forum.member.entity.MemberStatusEnum;
import com.apobates.forum.utils.DateTimeUtils;
import com.google.gson.Gson;
/**
 * 会员身份信息 
 * 客户端在本地存储,服务器端session或内存数据库中
 * 
 * @author xiaofanku
 * @since 20190306
 */
public final class MemberSessionBean implements Serializable {
	private static final long serialVersionUID = -244863562785441889L;
	// 会员ID
	private final long mid;
	// 会员的登录帐号
	private final String names;
	// 昵称
	private final String nickname;
	// 头像
	private final String avatar;
	// 签名
	private final String signature;
	// 状态
	private final MemberStatusEnum status;
	// 组
	private final MemberGroupEnum group;
	// 角色
	private final MemberRoleEnum role;
	// ip
	private final String ipAddr;
	// 是重生(使用缓存或cookie中还原的)=true
	private final boolean rebirth;
	//注册日期
	private final LocalDateTime registeDateTime;
	private final String sentinel;
	private final static Logger logger = LoggerFactory.getLogger(MemberSessionBean.class);
	
	//Cookie中还原的
	public MemberSessionBean(
			long mid, 
			String names,
			String avatar, 
			String signature, 
			MemberStatusEnum status,
			MemberGroupEnum group, 
			MemberRoleEnum role, 
			LocalDateTime signUpDateTime, 
			String nickname, 
			String ipAddr, 
			boolean rebirth,
			String sentinel) {
		super();
		this.mid = mid;
		this.names = names;
		this.avatar = avatar;
		this.signature = signature;
		this.status = status;
		this.group = group;
		this.role = role;
		this.registeDateTime = signUpDateTime;
		this.nickname = nickname;
		this.ipAddr = ipAddr;
		this.rebirth = rebirth;
		this.sentinel = sentinel;
	}
	//登录的
	public MemberSessionBean(Member member, String ipAddr){
		this.mid = member.getId();
		this.names = member.getNames();
		this.avatar = member.getAvatarURI();
		this.signature = member.getSignature();
		this.status = member.getStatus();
		this.group = member.getMgroup();
		this.role = member.getMrole();
		this.nickname = member.getNickname();
		this.registeDateTime = member.getRegisteDateTime();
		this.ipAddr = ipAddr;
		this.rebirth = true;
		this.sentinel = "login";
	}
	public long getMid() {
		return mid;
	}
	public String getAvatar() {
		return avatar;
	}
	public String getSignature() {
		return signature;
	}
	public MemberStatusEnum getStatus() {
		return status;
	}
	public MemberGroupEnum getGroup() {
		return group;
	}
	public MemberRoleEnum getRole() {
		return role;
	}
	public String getIpAddr() {
		return ipAddr;
	}
	public boolean isRebirth() {
		return rebirth;
	}
	public LocalDateTime getRegisteDateTime() {
		return registeDateTime;
	}
	public String getNames() {
		return names;
	}
	public String getNickname() {
		logger.info(String.format("[MSB]%s -> %s", sentinel, nickname));
		return nickname;
	}
	public String getSentinel() {
		return sentinel;
	}
	/**
	 * 是否在线状态
	 * 
	 * @return true是
	 */
	public boolean isOnline() {
		return getMid() > 0;
	}
	
	/**
	 * 是否是管理员
	 * 
	 * @return
	 */
	public boolean isAdmin(){
		if(!isOnline()){
			return false;
		}
		return MemberRoleEnum.ADMIN == getRole();
	}
	
	/**
	 * 是否是管理者(版主,大版主,管理员)
	 * 
	 * @return
	 */
	public boolean isManager(){
		if(!isOnline()){
			return false;
		}
		if(isAdmin()){
			return true;
		}
		return MemberRoleEnum.BM == getRole() || MemberRoleEnum.MASTER == getRole();
	}
	
	//快速登录|前端显示用的
	public String toJson(){
		Map<String,String> data = new HashMap<>();
		data.put("id", getMid()+"");
		data.put("nickname", getNickname());
		data.put("status", getStatus().getTitle());
		data.put("group", getGroup().getTitle());
		data.put("role", getRole().getTitle());
		data.put("style", MemberBaseProfile.getStyle(getGroup(), getRole()));
		data.put("birth", DateTimeUtils.getRFC3339(getRegisteDateTime()));
		data.put("verify", hashCode()+"");
		return new Gson().toJson(data);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	/**
	 * 转换头像,只为输出使用,程序内部不保存(以http开头的公网访问地址)
	 * 会员面板(panel)
	 * 
	 * @param avatar 新的头像地址,以http开头的公网访问地址
	 * @return
	 */
	public MemberSessionBean clone(URL avatar) {
		return new MemberSessionBean(
				getMid(), 
				getNames(),
				avatar.toString(), 
				getSignature(), 
				getStatus(), 
				getGroup(),
				getRole(), 
				getRegisteDateTime(), 
				getNickname(),
				getIpAddr(), 
				isRebirth(), getSentinel());
	}
	
	//更新昵称和签名
	public MemberSessionBean refact(String nickname, String signature) {
		return new MemberSessionBean(
				getMid(), 
				getNames(),
				getAvatar(), 
				signature, 
				getStatus(), 
				getGroup(), 
				getRole(), 
				getRegisteDateTime(), 
				nickname, 
				getIpAddr(), 
				isRebirth(), getSentinel());
	}
	
	//更新头像
	public MemberSessionBean refact(String encodeAvtarFormatPath) {
		return new MemberSessionBean(
				getMid(), 
				getNames(),
				encodeAvtarFormatPath, 
				getSignature(), 
				getStatus(),
				getGroup(), 
				getRole(), 
				getRegisteDateTime(), 
				getNickname(), 
				getIpAddr(), 
				isRebirth(), getSentinel());
	}
	
	public MemberSessionBean refact(MemberGroupEnum group, MemberRoleEnum role, MemberStatusEnum status) {
		return new MemberSessionBean(
				getMid(), 
				getNames(),
				getAvatar(), 
				getSignature(), 
				status, 
				group, 
				role, 
				getRegisteDateTime(), 
				getNickname(), 
				getIpAddr(), 
				isRebirth(), getSentinel());
	}
	
	//策略需要的
	public Member toMember(){
		Member member = new Member(getMid(), "**", "*", "-", getStatus(), getGroup(), getRole(), getNickname());
		member.setRegisteDateTime(getRegisteDateTime());
		return member;
	}
}
