package com.apobates.forum.letterbox.impl;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ComponentScan.Filter;

@Configuration
@ComponentScan(
		basePackages={"com.apobates.forum.letterbox.impl"}, 
		useDefaultFilters=false, 
		includeFilters={
				@Filter(classes={org.springframework.stereotype.Service.class}),
				@Filter(classes={org.springframework.stereotype.Repository.class}),
				@Filter(classes={org.springframework.stereotype.Component.class})})
public class LetterAppConfig {

}
