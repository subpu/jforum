package com.apobates.forum.letterbox.impl.dao;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.apobates.forum.letterbox.api.dao.ForumLetterDao;
import com.apobates.forum.letterbox.entity.ForumLetter;
import com.apobates.forum.letterbox.entity.ForumLetterTypeEnum;
import com.apobates.forum.utils.DateTimeUtils;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;

@Repository
public class ForumLetterDaoImpl implements ForumLetterDao{
	@PersistenceContext
	private EntityManager entityManager;
	private final static Logger logger = LoggerFactory.getLogger(ForumLetterDaoImpl.class);
	
	@Override
	public Page<ForumLetter> findAll(Pageable pageable) {
		return emptyResult();
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public void save(ForumLetter entity) {
		entityManager.persist(entity);
	}

	@Override
	public Optional<ForumLetter> findOne(Long primaryKey) {
		return Optional.ofNullable(entityManager.find(ForumLetter.class, primaryKey));
	}

	@Override
	public Optional<Boolean> edit(ForumLetter updateEntity) {
		return Optional.empty();
	}

	@Override
	public Stream<ForumLetter> findAll() {
		return Stream.empty();
	}

	@Override
	public long count() {
		return 0L;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Stream<ForumLetter> findAll(long sender, long receiver) {
		String SQL="SELECT fl.* FROM apo_letter AS fl JOIN apo_letter_inbox AS ib ON fl.ID = ib.LETTER "
				+ "WHERE (ib.MEMBER = ?1 AND ib.USABLE = ?2 AND fl.AUTHOR = ?3) "
				+ "OR (ib.MEMBER = ?4 AND ib.USABLE = ?5 AND fl.AUTHOR = ?6) "
				+ "ORDER BY fl.ENTRYDATETIME ASC";
		return entityManager.createNativeQuery(SQL, ForumLetter.class)
							.setParameter(1, receiver)
							.setParameter(2, true)
							.setParameter(3, sender)
							.setParameter(4, sender)
							.setParameter(5, true)
							.setParameter(6, receiver)
							.getResultStream();
	}

	@Override
	public Page<ForumLetter> findAll(long sender, long receiver, Pageable pageable) {
		final long count=countAll(sender, receiver);
		if(count == 0){
			return emptyResult();
		}
		String SQL="SELECT fl.* FROM apo_letter AS fl JOIN apo_letter_inbox AS ib ON fl.ID = ib.LETTER "
				+ "WHERE (ib.MEMBER = ?1 AND ib.USABLE = ?2 AND fl.AUTHOR = ?3) "
				+ "OR (ib.MEMBER = ?4 AND ib.USABLE = ?5 AND fl.AUTHOR = ?6) "
				+ "ORDER BY fl.ENTRYDATETIME DESC";
		Query query = entityManager.createNativeQuery(SQL, ForumLetter.class)
								.setParameter(1, receiver)
								.setParameter(2, true)
								.setParameter(3, sender)
								.setParameter(4, sender)
								.setParameter(5, true)
								.setParameter(6, receiver);
		query.setFirstResult(pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());
		
		@SuppressWarnings("unchecked")
		final List<ForumLetter> result = query.getResultList();
		return new Page<ForumLetter>() {
			@Override
			public long getTotalElements() {
				return count;
			}

			@Override
			public Stream<ForumLetter> getResult() {
				return result.stream();
			}
		};
	}
	private long countAll(long sender, long receiver){
		String SQL="SELECT COUNT(*) FROM apo_letter AS fl JOIN apo_letter_inbox AS ib ON fl.ID = ib.LETTER "
				+ "WHERE (ib.MEMBER = ?1 AND ib.USABLE = ?2 AND fl.AUTHOR = ?3) "
				+ "OR (ib.MEMBER = ?4 AND ib.USABLE = ?5 AND fl.AUTHOR = ?6) ";
		try {
			Object obj = entityManager.createNativeQuery(SQL)
					.setParameter(1, receiver)
					.setParameter(2, true)
					.setParameter(3, sender)
					.setParameter(4, sender)
					.setParameter(5, true)
					.setParameter(6, receiver)
					.getSingleResult();
			try{
				return ((BigDecimal)obj).longValue();
			}catch(java.lang.ClassCastException e){
				return (Long)obj;
			}
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("[countAll][ForumLetterDao]", e);
			}
		}
		return 0L;
	}
	@Override
	public Stream<ForumLetter> getTodayClaim() {
		LocalDateTime start = DateTimeUtils.getTodayEarlyMorning();
		LocalDateTime finish = DateTimeUtils.getTodayMidnight();
		
		return entityManager.createQuery("SELECT fl FROM ForumLetter fl WHERE fl.typed = ?1 AND fl.entryDateTime BETWEEN ?2 AND ?3", ForumLetter.class)
				.setParameter(1, ForumLetterTypeEnum.CLAIM)
				.setParameter(2, start)
				.setParameter(3, finish)
				.getResultStream();
	}
}
