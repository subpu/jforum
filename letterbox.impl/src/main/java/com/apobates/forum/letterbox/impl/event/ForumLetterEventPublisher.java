package com.apobates.forum.letterbox.impl.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

/**
 * 信件模块事件发布器
 * @author xiaofanku
 * @since 20190703
 */
@Component
public class ForumLetterEventPublisher {
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;
    
    /**
     * 发布信件创建事件
     * @param boardCreateEvent
     */
    public void publishPostsEvent(ForumLetterPostEvent letterPostEvent) {
        applicationEventPublisher.publishEvent(letterPostEvent);
    }
}
