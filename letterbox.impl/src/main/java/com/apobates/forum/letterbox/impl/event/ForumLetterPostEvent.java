package com.apobates.forum.letterbox.impl.event;

import org.springframework.context.ApplicationEvent;

import com.apobates.forum.letterbox.entity.ForumLetter;
/**
 * 信件创建事件
 * @author xiaofanku
 * @since 20190914
 */
public class ForumLetterPostEvent extends ApplicationEvent{
	private static final long serialVersionUID = -4875399186422889558L;
	private final ForumLetter letter;
	
	public ForumLetterPostEvent(Object source, ForumLetter letter) {
		super(source);
		this.letter = letter;
	}

	public ForumLetter getLetter() {
		return letter;
	}

}
