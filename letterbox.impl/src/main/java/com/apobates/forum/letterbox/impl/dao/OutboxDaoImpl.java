package com.apobates.forum.letterbox.impl.dao;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.apobates.forum.letterbox.api.dao.OutboxDao;
import com.apobates.forum.letterbox.entity.ForumLetter;
import com.apobates.forum.letterbox.entity.ForumLetterTypeEnum;
import com.apobates.forum.letterbox.entity.Outbox;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;

@Repository
public class OutboxDaoImpl implements OutboxDao{
	@PersistenceContext
	private EntityManager entityManager;
	@Value("${jpa.batch.size}")
	private int batchSize;
	private final static Logger logger = LoggerFactory.getLogger(OutboxDaoImpl.class);
	
	@Override
	public Page<ForumLetter> findAllBySender(long memberId, Pageable pageable) {
		final long count = countBySender(memberId);
		if(count==0){
			return emptyResult();
		}
		
		TypedQuery<ForumLetter> query = entityManager.createQuery("SELECT fl FROM ForumLetter fl WHERE fl.author = ?1 ORDER BY fl.entryDateTime DESC", ForumLetter.class).setParameter(1, memberId);
		query.setFirstResult(pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());

		final Stream<ForumLetter> result = query.getResultStream();
		return new Page<ForumLetter>() {
			@Override
			public long getTotalElements() {
				return count;
			}

			@Override
			public Stream<ForumLetter> getResult() {
				return result;
			}
		};
	}
	@Override
	public long countBySender(long memberId){
		try {
			return entityManager.createQuery("SELECT COUNT(fl) FROM ForumLetter fl WHERE fl.author = ?1", Long.class)
					.setParameter(1, memberId)
					.getSingleResult();
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("[countBySender][OutboxDao]", e);
			}
		}
		return 0L;
	}
	
	@Override
	public Page<ForumLetter> findAllBySenderAndType(long memberId, ForumLetterTypeEnum typed, Pageable pageable) {
		final long count = countBySenderAndType(memberId, typed);
		if(count==0){
			return emptyResult();
		}
		TypedQuery<ForumLetter> query = entityManager.createQuery("SELECT fl FROM ForumLetter fl WHERE fl.author = ?1 AND fl.typed = ?2 ORDER BY fl.entryDateTime DESC", ForumLetter.class)
				.setParameter(1, memberId)
				.setParameter(2, typed);
		query.setFirstResult(pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());

		final Stream<ForumLetter> result = query.getResultStream();
		return new Page<ForumLetter>() {
			@Override
			public long getTotalElements() {
				return count;
			}

			@Override
			public Stream<ForumLetter> getResult() {
				return result;
			}
		};
	}
	@Override
	public long countBySenderAndType(long memberId, ForumLetterTypeEnum typed){
		try {
			return entityManager.createQuery("SELECT COUNT(fl) FROM ForumLetter fl WHERE fl.author = ?1 AND fl.typed = ?2", Long.class)
					.setParameter(1, memberId)
					.setParameter(2, typed)
					.getSingleResult();
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("[countBySenderAndType][OutboxDao]", e);
			}
		}
		return 0L;
	}
	
	@Override
	public Stream<Outbox> findAllByLetter(long letterId) {
		return entityManager.createQuery("SELECT ob FROM Outbox ob WHERE ob.letter = ?1", Outbox.class).setParameter(1, letterId).getResultStream();
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public int batchSave(Set<Outbox> letterSendRecords) {
		int i = 0;
		for (Outbox ob : letterSendRecords) {
			entityManager.persist(ob);
			i++;
			if (i % batchSize == 0) {
				// Flush a batch of inserts and release memory.
				entityManager.flush();
				entityManager.clear();
			}
		}
		return i;
	}

	@Override
	public Stream<Outbox> findAllByLetter(Collection<Long> letterIdSet) {
		return entityManager.createQuery("SELECT ob FROM Outbox ob WHERE ob.letter IN ?1", Outbox.class).setParameter(1, letterIdSet).getResultStream();
	}
	
	@Override
	public Page<ForumLetter> findAll(Pageable pageable) {
		return emptyResult();
	}

	@Override
	public void save(ForumLetter entity) {}

	@Override
	public Optional<ForumLetter> findOne(Long primaryKey) {
		return Optional.empty();
	}

	@Override
	public Optional<Boolean> edit(ForumLetter updateEntity) {
		return Optional.empty();
	}

	@Override
	public Stream<ForumLetter> findAll() {
		return Stream.empty();
	}

	@Override
	public long count() {
		return 0L;
	}
	
}
