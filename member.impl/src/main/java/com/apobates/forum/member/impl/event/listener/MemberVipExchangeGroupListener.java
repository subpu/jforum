package com.apobates.forum.member.impl.event.listener;

import com.apobates.forum.member.api.dao.MemberDao;
import com.apobates.forum.member.entity.MemberGroupEnum;
import com.apobates.forum.member.entity.MemberVipExchangeRecords;
import com.apobates.forum.member.impl.event.MemberVipExchangeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class MemberVipExchangeGroupListener implements ApplicationListener<MemberVipExchangeEvent> {
    @Autowired
    private MemberDao memberDao;
    private final static Logger logger = LoggerFactory.getLogger(MemberVipExchangeGroupListener.class);

    @Override
    public void onApplicationEvent(MemberVipExchangeEvent event) {
        logger.info("[Member][VipExchangeEvent][1]VIP会员的组变更开始");
        MemberVipExchangeRecords vee = event.getRecord();
        if (vee.getId() > 0) {
            memberDao.editMemberGroup(vee.getMemberId(), MemberGroupEnum.VIP);
        }
        logger.info("[Member][VipExchangeEvent][1]VIP会员的组结束变更");
    }
}
