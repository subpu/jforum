package com.apobates.forum.member.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class MemberTaskScheduleConfig {
	private final static Logger logger = LoggerFactory.getLogger(MemberTaskScheduleConfig.class);
	
	@Bean
	public MemberTask getTask() {
		logger.info("[Task][member]query member task");
		return new MemberTask();
	}
	// fixedDelay An interval-based trigger where the interval is measured from the completion time of the previous task. The time unit value is measured in milliseconds.
	//            即表示从上一个任务完成开始到下一个任务开始的间隔，单位是毫秒。
	// fixedRate  An interval-based trigger where the interval is measured from the start time of the previous task. The time unit value is measured in milliseconds.
	//            即从上一个任务开始到下一个任务开始的间隔，单位是毫秒。
	@Scheduled(fixedDelay = 60000) //1*60*1000
	public void penalizeScheduleJob() {
		logger.info("[Task][member]start penalize job");
		getTask().rebirthMemberJob();
		logger.info("[Task][member]penalize job end");
	}

	@Scheduled(fixedDelay = 60000) //1*60*1000
	public void vipExchangeResetMemberGroupScheduleJob() {
		logger.info("[Task][member]start Vip Exchange job");
		getTask().vipExchangeResetMemberGroupJob();
		logger.info("[Task][member]Vip Exchange job end");
	}
}
