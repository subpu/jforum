package com.apobates.forum.member.impl.dao;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.apobates.forum.member.api.dao.MemberPenalizeRecordsDao;
import com.apobates.forum.member.entity.MemberPenalizeRecords;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;

@Repository
public class MemberPenalizeRecordsDaoImpl implements MemberPenalizeRecordsDao{
	@PersistenceContext
	private EntityManager entityManager;
	private final static Logger logger = LoggerFactory.getLogger(MemberPenalizeRecordsDaoImpl.class);

	@Override
	public Page<MemberPenalizeRecords> findAll(Pageable pageable) {
		final long total = count();
		if (total == 0) {
			return emptyResult();
		}
		final String SQL = "SELECT mpr FROM MemberPenalizeRecords mpr ORDER BY mpr.entryDateTime DESC";
		TypedQuery<MemberPenalizeRecords> query = entityManager.createQuery(SQL, MemberPenalizeRecords.class);
		query.setFirstResult(pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());

		final Stream<MemberPenalizeRecords> result = query.getResultStream();
		return new Page<MemberPenalizeRecords>() {
			@Override
			public long getTotalElements() {
				return total;
			}

			@Override
			public Stream<MemberPenalizeRecords> getResult() {
				return result;
			}
		};
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public void save(MemberPenalizeRecords entity) {
		entityManager.persist(entity);
	}

	@Override
	public Optional<MemberPenalizeRecords> findOne(Long primaryKey) {
		return Optional.ofNullable(entityManager.find(MemberPenalizeRecords.class, primaryKey));
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public Optional<Boolean> edit(MemberPenalizeRecords updateEntity) {
		try{
			entityManager.merge(updateEntity);
			return Optional.of(true);
		}catch(Exception e){
			if(logger.isDebugEnabled()){
				logger.debug(e.getMessage(), e);
			}
		}
		return Optional.empty();
	}

	@Override
	public Stream<MemberPenalizeRecords> findAll() {
		return Stream.empty();
	}

	@Override
	public long count() {
		try {
			return entityManager.createQuery("SELECT COUNT(mpr) FROM MemberPenalizeRecords mpr", Long.class).getSingleResult();
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("[count][MemberPenalizeRecordsDao]", e);
			}
		}
		return 0L;
	}

	@Override
	public Stream<MemberPenalizeRecords> findAllByMember(long memberId) {
		return entityManager.createQuery("SELECT mpr FROM MemberPenalizeRecords mpr WHERE mpr.memberId = ?1 AND mpr.status = ?2", MemberPenalizeRecords.class)
				.setParameter(1, memberId)
				.setParameter(2, true)
				.getResultStream();
	}

	@Override
	public long countForMember(long memberId) {
		return entityManager.createQuery("SELECT COUNT(mpr) FROM MemberPenalizeRecords mpr WHERE mpr.memberId = ?1 AND mpr.status = ?2", Long.class)
				.setParameter(1, memberId)
				.setParameter(2, true)
				.getSingleResult();
	}

	@Override
	public Page<MemberPenalizeRecords> findAll(long buildMemberId, Pageable pageable) {
		final long total = count(buildMemberId);
		if (total == 0) {
			return emptyResult();
		}
		final String SQL = "SELECT mpr FROM MemberPenalizeRecords mpr WHERE mpr.judger = ?1 ORDER BY mpr.entryDateTime DESC";
		TypedQuery<MemberPenalizeRecords> query = entityManager.createQuery(SQL, MemberPenalizeRecords.class).setParameter(1, buildMemberId);
		query.setFirstResult(pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());

		final Stream<MemberPenalizeRecords> result = query.getResultStream();
		return new Page<MemberPenalizeRecords>() {
			@Override
			public long getTotalElements() {
				return total;
			}

			@Override
			public Stream<MemberPenalizeRecords> getResult() {
				return result;
			}
		};
	}

	private Long count(long buildMemberId){
		try {
			return entityManager.createQuery("SELECT COUNT(mpr) FROM MemberPenalizeRecords mpr WHERE mpr.judger = ?1", Long.class)
					.setParameter(1, buildMemberId)
					.getSingleResult();
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("[count][MemberPenalizeRecordsDao]", e);
			}
		}
		return 0L;
	}

	@Override
	public Stream<MemberPenalizeRecords> findAllByExpire(LocalDateTime start, LocalDateTime finish) {
		final String SQL="SELECT mpr FROM MemberPenalizeRecords mpr WHERE mpr.status = :sta AND mpr.rebirthDateTime BETWEEN :art AND :ish";
		return entityManager.createQuery(SQL, MemberPenalizeRecords.class)
				.setParameter("sta", true)
				.setParameter("art", start)
				.setParameter("ish", finish)
				.getResultStream();
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public Optional<Boolean> expired(long id) {
		int affect = entityManager.createQuery("UPDATE MemberPenalizeRecords mpr SET mpr.status = ?1 WHERE mpr.id = ?2 AND mpr.status = ?3")
				.setParameter(1, false)
				.setParameter(2, id)
				.setParameter(3, true)
				.executeUpdate();
		if(affect == 1){
			return Optional.of(true);
		}
		return Optional.empty();
	}
	
	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public int expired(Collection<Long> idList) {
		Integer affect = 0;
		Query query = entityManager.createQuery("UPDATE MemberPenalizeRecords mpr SET mpr.status = ?1 WHERE mpr.id = ?2 AND mpr.status = ?3");
		for (Long id : idList) {
			affect += query.setParameter(1, false).setParameter(2, id).setParameter(3, true).executeUpdate();
		}
		return affect;
	}
}
