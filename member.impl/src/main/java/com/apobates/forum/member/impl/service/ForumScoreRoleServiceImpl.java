package com.apobates.forum.member.impl.service;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.member.api.dao.ForumScoreRoleDao;
import com.apobates.forum.member.api.service.ForumScoreRoleService;
import com.apobates.forum.member.entity.ForumScoreRole;


@Service
public class ForumScoreRoleServiceImpl implements ForumScoreRoleService{
	@Autowired
	private ForumScoreRoleDao forumScoreRoleDao;
	
	@Cacheable(value = "memberCache", key="'score'")
	@Override
	public Stream<ForumScoreRole> getAllUsed() { //[MC]M3
		return forumScoreRoleDao.findAllUsed();
	}
	
	@Cacheable(value = "memberCache", key="'score_'+#id", unless="#result==null")
	@Override
	public Optional<ForumScoreRole> get(int id) { //[MC]M4
		if(id>0){
			return forumScoreRoleDao.findOne(id);
		}
		return Optional.empty();
	}
	
	@CacheEvict(value = "memberCache", key="'score_'+#id")
	@Override
	public Optional<Boolean> edit(int id, final ForumScoreRole updateEntity) { //[MC]M4-R1
		Optional<ForumScoreRole> opt = get(id);
		opt.ifPresent(fsr->{
			fsr.setDegree(updateEntity.getDegree());
			fsr.setLevel(updateEntity.getLevel());
			fsr.setScore(updateEntity.getScore());
			fsr.setStatus(updateEntity.isStatus());
		});
		if(opt.isPresent()){
			return forumScoreRoleDao.edit(opt.get());
		}
		return Optional.empty();
	}
	
	@CacheEvict(value = "memberCache", key="'score'")
	@Override
	public Optional<ForumScoreRole> create(ForumActionEnum action, int degree, int level, double score, boolean status)throws IllegalStateException { //[MC]M3-R1
		ForumScoreRole fsr = new ForumScoreRole(action,  degree, level, score, status);
		forumScoreRoleDao.save(fsr);
		if(fsr.getId()>0) {
			return Optional.of(fsr);
		}
		throw new IllegalStateException("积分规则创建失败");
	}
	
	@Override
	public Stream<ForumScoreRole> getAll() {
		return forumScoreRoleDao.findAll();
	}
	
	@Override
	public double getMemberScore(Map<ForumActionEnum, Long> statsActionSize) {
        if (null == statsActionSize || statsActionSize.isEmpty()) {
            return 0;
        }
        return getAllUsed().filter(Objects::nonNull).reduce(0.0, (partialResult,fsr)->{
            Long operateSize;
            try {
                operateSize = statsActionSize.get(fsr.getAction());
            } catch (Exception e) {
                operateSize = 0L;
            }
            //积分和次频
            if (null != operateSize && operateSize > 0 && fsr.getScore() != 0 && fsr.getDegree() >= 1) {
                double tmp = operateSize / fsr.getDegree() * fsr.getScore();
                return partialResult+tmp;
            }
            return partialResult;
        }, Double::sum);
		/*
           {action:发贴, 频次:1,积分:1}100/1*1
           {action:回复, 频次:4,积分:1}500/4*1
           {action:置顶, 频次:1,积分:2}10/1*2
           {action:加精, 频次:1,积分:3}5/1*3
           {action:删除的话题, 频次:1,积分:-1}100/1*1
           {action:删除的回复, 频次:4,积分:-1}500/4*1
		 * */
	}
}
