package com.apobates.forum.member.impl.event.listener;

import com.apobates.forum.member.impl.event.MemberSignUpEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.apobates.forum.member.api.dao.MemberDao;
import com.apobates.forum.member.entity.Member;
import com.apobates.forum.member.entity.MemberRoleEnum;

/**
 * 将1号员工提升为管理员
 * 
 * @author xiaofanku
 * @since 20190911
 */
@Component
public class MemberSignUpPromoteListener implements ApplicationListener<MemberSignUpEvent>{
	@Autowired
	private MemberDao memberDao;
	private final static Logger logger = LoggerFactory.getLogger(MemberSignUpPromoteListener.class);
	
	@Override
	public void onApplicationEvent(MemberSignUpEvent event) {
		logger.info("[Member][SignUpEvent][1]开始1号员工提升");
		Member m = event.getMember();
		if(m.getId() == 1){
			memberDao.editMemberRole(1L, MemberRoleEnum.ADMIN);
		}
		logger.info("[Member][SignUpEvent][1]1号员工提升结束");
	}
}
