package com.apobates.forum.member.impl.service;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.event.elderly.MemberAction;
import com.apobates.forum.event.elderly.MemberActionDescriptor;
import com.apobates.forum.member.api.dao.MemberContactDao;
import com.apobates.forum.member.api.service.MemberContactService;
import com.apobates.forum.member.entity.MemberContact;
import com.apobates.forum.utils.Commons;

@Service
public class MemberContactServiceImpl implements MemberContactService{
	@Autowired
	private MemberContactDao memberContactDao;
	private final static Logger logger = LoggerFactory.getLogger(MemberContactServiceImpl.class);

	@MemberAction(action = ForumActionEnum.MEMBER_PROFILE_CONTACT, argName = "memberId")
	@Override
	public Optional<MemberContact> create(long memberId, String province, String city, String region, String street,String postcode, String unEncryptMobileString, MemberActionDescriptor actionDescriptor) {
		String mobile="";
		String mobileCrc32 = "*";
		logger.info("[create][pre]mobile:"+unEncryptMobileString);
		if(Commons.isPhoneNumber(unEncryptMobileString)) {
			mobileCrc32 = Commons.crc32(unEncryptMobileString)+"";
			mobile = encryptMobile(unEncryptMobileString, mobileCrc32);
		}
		logger.info("[create][post]-->crc:"+mobileCrc32+", mobile:"+mobile);
		MemberContact mc = new MemberContact(memberId, province, city, region, street, postcode, mobile, mobileCrc32);
		try {
			memberContactDao.save(mc);
			return Optional.of(mc);
		}catch(Exception e) {
			throw new IllegalStateException(e.getMessage());
		}
	}

	@MemberAction(action = ForumActionEnum.MEMBER_PROFILE_CONTACT, editable=true, argName="memberId")
	@Override
	public Optional<MemberContact> edit(long id, long memberId, String province, String city, String region, String street,String postcode, String unEncryptMobileString, MemberActionDescriptor actionDescriptor) {
		Optional<MemberContact> obj = get(id);
		if(!obj.isPresent()) {
			throw new IllegalStateException("会员联系方式不存在");
		}
		MemberContact mc = obj.get();
		//
		if(Commons.isPhoneNumber(unEncryptMobileString)) {
			String mobileCrc32 = Commons.crc32(unEncryptMobileString)+"";
			String mobile = encryptMobile(unEncryptMobileString, mobileCrc32);
			mc.setMobileCrc32(mobileCrc32);
			mc.setMobile(mobile);
		}
		if(Commons.isNotBlank(province)) {
			mc.setProvince(province);
		}
		if(Commons.isNotBlank(city)) {
			mc.setCity(city);
		}
		if(Commons.isNotBlank(region)) {
			mc.setRegion(region);
		}
		if(Commons.isNotBlank(street)) {
			mc.setStreet(street);
		}
		if(Commons.isNotBlank(postcode)) {
			mc.setPostcode(postcode);
		}
		try {
			Optional<Boolean> d = memberContactDao.edit(mc);
			if(d.isPresent()) {
				return Optional.of(mc);
			}
		}catch(Exception e) {
			throw new IllegalStateException(e.getMessage());
		}
		throw new IllegalStateException("更新会员的联系方式操作失败");
	}

	@Override
	public Optional<MemberContact> get(long id) {
		return memberContactDao.findOne(id);
	}

	@Override
	public Optional<MemberContact> getByMember(long memberId) {
		return memberContactDao.findOneByMember(memberId);
	}
	
	/**
	 * 编码失败失败只包含一个字符(*)的字符串
	 * @param unEncryptIdentityCard
	 * @param memberId
	 * @return
	 */
	private String encryptMobile(String unEncryptMobileString, String mobileCrc32) {
		return Commons.desEncrypt(mobileCrc32, unEncryptMobileString, () -> "*");
	}
}
