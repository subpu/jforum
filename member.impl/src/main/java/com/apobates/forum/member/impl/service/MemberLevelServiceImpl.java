package com.apobates.forum.member.impl.service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import com.apobates.forum.member.api.dao.MemberLevelDao;
import com.apobates.forum.member.api.service.MemberLevelService;
import com.apobates.forum.member.entity.MemberLevel;
import com.apobates.forum.utils.CommonDoubleBean;
import com.apobates.forum.utils.Commons;
import com.apobates.forum.utils.DoubleBeanFormatter;


@Service
public class MemberLevelServiceImpl implements MemberLevelService{
	@Autowired
	private MemberLevelDao memberLevelDao;

	@CacheEvict(value="memberCache", key="'level_used'")
	@Override
	public Optional<MemberLevel> create(String names, double minScore, double score, String imageAddr, boolean status)throws IllegalStateException {
		MemberLevel ml = new MemberLevel(0, names, minScore, score, imageAddr, status);
		try {
			memberLevelDao.save(ml);
			return Optional.of(ml);
		}catch(Exception e) {
			throw new IllegalStateException(e.getMessage());
		}
	}
	
	@CacheEvict(value="memberCache", key="'level_used'")
	@Override
	public Optional<MemberLevel> edit(int id, String names, double minScore, double score, String imageAddr, boolean status)throws IllegalStateException {
		Optional<MemberLevel> obj = get(id);
		if(!obj.isPresent()) {
			throw new IllegalStateException("会员等级不存在");
		}
		MemberLevel ml = obj.get();
		if(Commons.isNotBlank(names)) {
			ml.setNames(names);
		}
		if(minScore != ml.getMinScore()) {
			ml.setMinScore(minScore);
		}
		if(score != ml.getScore()) {
			ml.setScore(score);
		}
		if(Commons.isNotBlank(imageAddr)) {
			ml.setImageAddr(imageAddr);
		}
		ml.setStatus(status);
		Optional<Boolean> d = memberLevelDao.edit(ml);
		if(d.isPresent()) {
			return Optional.of(ml);
		}
		throw new IllegalStateException("更新会员等级操作失败");
	}
	
	@Override
	public Optional<MemberLevel> get(int id) {
		if(id>0){
			return memberLevelDao.findOne(id);
		}
		return Optional.empty();
	}

	@Override
	public Stream<MemberLevel> getAll() {
		return memberLevelDao.findAll();
	}

	@Override
	public boolean editStatus(int id, boolean status) {
		return memberLevelDao.editStatus(id, status) == 1;
	}

	@Override
	public Optional<MemberLevel> getMemeberLevel(double scoreValue) {
		List<CommonDoubleBean> rs = getMemberLevelCommonBean(); 
		
		String levelName = new DoubleBeanFormatter(rs).format(scoreValue);
		//从定义中找到是几级
		Optional<CommonDoubleBean> result = rs.stream().filter(cdb -> cdb.getTitle().equals(levelName)).findAny();
		MemberLevel data = new MemberLevel();
		data.setId(result.orElse(CommonDoubleBean.empty()).getId());
		data.setNames(levelName);
		data.setScore(scoreValue);
		data.setStatus(true);
		return Optional.of(data);
	}

	@Override
	public Stream<MemberLevel> getAllUsed() {
		return memberLevelDao.findAllUsed();
	}
	@Cacheable(value="memberCache", key="'level_used'", unless="#result==null")
	@Override
	public List<CommonDoubleBean> getMemberLevelCommonBean() {
		AtomicInteger index = new AtomicInteger(1);
		Function<MemberLevel, CommonDoubleBean> mapper = ml -> {
			int rk = index.getAndIncrement();
			return new CommonDoubleBean(rk, ml.getNames(), ml.getMinScore());
		};
		return memberLevelDao.findAllUsed().sorted(Comparator.comparingDouble(MemberLevel::getMinScore)).map(mapper).collect(Collectors.toList());
	}
}
