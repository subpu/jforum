package com.apobates.forum.member.impl.event;

import org.springframework.context.ApplicationEvent;
import com.apobates.forum.member.entity.Member;
/**
 * 会员注销事件
 * 
 * @author xiaofanku
 * @since 20191230
 */
public class MemberSignOutEvent extends ApplicationEvent{
	private static final long serialVersionUID = -28020595031472116L;
	private final Member member;
	private final String refererURL;
	private final String ipAddr;
	
	public MemberSignOutEvent(Object source, Member member, String refererURL, String ipAddr) {
		super(source);
		this.member = member;
		this.refererURL = refererURL;
		this.ipAddr = ipAddr;
	}

	public Member getMember() {
		return member;
	}

	public String getRefererURL() {
		return refererURL;
	}

	public String getIpAddr() {
		return ipAddr;
	}
}
