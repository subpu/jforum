package com.apobates.forum.member.impl.service;

import java.util.Map;
import java.util.Optional;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.event.elderly.MemberAction;
import com.apobates.forum.event.elderly.MemberActionDescriptor;
import com.apobates.forum.member.api.dao.MemberRealAuthenticationDao;
import com.apobates.forum.member.api.service.MemberRealAuthenticationService;
import com.apobates.forum.member.entity.MemberRealAuthentication;
import com.apobates.forum.utils.Commons;

@Service
public class MemberRealAuthenticationServiceImpl implements MemberRealAuthenticationService{
	@Autowired
	private MemberRealAuthenticationDao memberRealAuthenticationDao;
	private final static Logger logger = LoggerFactory.getLogger(MemberRealAuthenticationServiceImpl.class);

	@MemberAction(action = ForumActionEnum.MEMBER_PROFILE_REALAUTH, argName = "memberId")
	@Override
	public Optional<MemberRealAuthentication> create(long memberId, String realname, String birthYear, String birthMonth,	String birthDay, String unEncryptIdentityCard, String globalDecSalt, MemberActionDescriptor actionDescriptor)throws IllegalStateException {
		boolean isVP=islegalCard(unEncryptIdentityCard, birthYear, birthMonth, birthDay); 
		String tmp="";
		if(isVP) {
			tmp = encryptIdentityCard(unEncryptIdentityCard, memberId, globalDecSalt);
		}
		MemberRealAuthentication mra = new MemberRealAuthentication(memberId, realname, birthYear, birthMonth, birthDay,  tmp, isVP);
		try {
			memberRealAuthenticationDao.save(mra);
			return Optional.of(mra);
		}catch(Exception e) {
			throw new IllegalStateException(e.getMessage());
		}
	}

	@MemberAction(action = ForumActionEnum.MEMBER_PROFILE_REALAUTH, editable=true, argName="memberId")
	@Override
	public Optional<MemberRealAuthentication> edit(long id, long memberId, String realname, String birthYear, String birthMonth, String birthDay, String unEncryptIdentityCard, String globalDecSalt, MemberActionDescriptor actionDescriptor)throws IllegalStateException {
		Optional<MemberRealAuthentication> obj = get(id);
		if(!obj.isPresent()) {
			throw new IllegalStateException("会员实名认证记录不存在");
		}
		//身份证加密
		MemberRealAuthentication mra = obj.get();
		if(mra.isPassed()) {
			throw new IllegalStateException("实名验证已通过, 不可以再编辑");
		}
		boolean isVP=islegalCard(unEncryptIdentityCard, birthYear, birthMonth, birthDay); 
		mra.setPassed(isVP);
		
		if(isVP) {
			String tmp = encryptIdentityCard(unEncryptIdentityCard, memberId, globalDecSalt);
			mra.setIdentityCard(tmp);
		}
		if(Commons.isNotBlank(realname)) {
			mra.setRealname(realname);
		}
		if(Commons.isNotBlank(birthYear)) {
			mra.setBirthYear(birthYear);
		}
		if(Commons.isNotBlank(birthMonth)) {
			mra.setBirthMonth(birthMonth);
		}
		if(Commons.isNotBlank(birthDay)) {
			mra.setBirthDay(birthDay);
		}
		try {
			Optional<Boolean> dd = memberRealAuthenticationDao.edit(mra);
			if(dd.isPresent()) {
				return Optional.of(mra);
			}
		}catch(Exception e) {
			throw new IllegalStateException(e.getMessage());
		}
		throw new IllegalStateException("会员实名认证更新失败");
	}

	@Override
	public Optional<MemberRealAuthentication> get(long id) {
		return memberRealAuthenticationDao.findOne(id);
	}

	@Override
	public Optional<MemberRealAuthentication> getByMember(long memberId) {
		return memberRealAuthenticationDao.findOneByMember(memberId);
	}
	private boolean islegalCard(String unEncryptIdentityCard, String birthYear, String birthMonth,	String birthDay) {
		Map<String,String> data = Commons.parseCitizenCard(unEncryptIdentityCard);
		if(data.isEmpty()) {
			return false;
		}
		return data.get(Commons.Y).equals(birthYear) &&  data.get(Commons.M).equals(birthMonth) && data.get(Commons.D).equals(birthDay);
	}
	/**
	 * 编码失败失败只包含一个字符(*)的字符串
	 * @param unEncryptIdentityCard
	 * @param memberId
	 * @return
	 */
	private String encryptIdentityCard(String unEncryptIdentityCard, long memberId, String globalDecSalt) {
		String secretString = String.format("%sv%s", globalDecSalt, memberId);
		return Commons.desEncrypt(secretString, unEncryptIdentityCard, () -> "*");
	}
}
