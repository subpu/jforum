package com.apobates.forum.member.impl;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCaching
@ComponentScan(
		basePackages={"com.apobates.forum.member.impl"}, 
		useDefaultFilters=false, 
		includeFilters={
				@Filter(classes={org.springframework.stereotype.Service.class}),
				@Filter(classes={org.springframework.stereotype.Repository.class}),
				@Filter(classes={org.springframework.stereotype.Component.class})})
public class MemberAppConfig {
    //@Import(value={MemberTaskScheduleConfig.class})
}
