package com.apobates.forum.member.impl.event;

import org.springframework.context.ApplicationEvent;
import com.apobates.forum.member.entity.Member;
/**
 * 会员注册事件
 * 
 * @author xiaofanku
 * @since 20190703
 */
public class MemberSignUpEvent extends ApplicationEvent{
	private static final long serialVersionUID = -1170897700743310079L;
	private final Member member;
	
	public MemberSignUpEvent(Object source, Member member){
		super(source);
		this.member = member;
	}

	public Member getMember() {
		return member;
	}
}
