package com.apobates.forum.member.impl.event.listener;

import com.apobates.forum.member.impl.event.MemberSignUpEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import com.apobates.forum.member.api.dao.RegisteInviteCodeDao;
import com.apobates.forum.member.entity.Member;
/**
 * 会员注册事件侦听器,会员注册后的激活邀请码
 * 
 * @author xiaofanku
 * @since 20191113
 */
@Component
public class MemberSignUpInviteCodeListener implements ApplicationListener<MemberSignUpEvent>{
	@Autowired
	private RegisteInviteCodeDao registeInviteCodeDao;
	private final static Logger logger = LoggerFactory.getLogger(MemberSignUpInviteCodeListener.class);
	
	@Override
	public void onApplicationEvent(MemberSignUpEvent event) {
		logger.info("[Member][SignUpEvent][2]开始激活邀请码");
		Member m = event.getMember();
		if(m.getInviteCodeId() > 0){
			registeInviteCodeDao.active(m.getInviteCodeId(), m.getId(), m.getNames());
		}
		logger.info("[Member][SignUpEvent][2]邀请码激活结束");
	}
}
