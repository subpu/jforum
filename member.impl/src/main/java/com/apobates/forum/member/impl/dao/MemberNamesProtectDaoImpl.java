package com.apobates.forum.member.impl.dao;

import java.util.Optional;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.apobates.forum.member.api.dao.MemberNamesProtectDao;
import com.apobates.forum.member.entity.MemberNamesProtect;


@Repository
public class MemberNamesProtectDaoImpl implements MemberNamesProtectDao {
	@PersistenceContext
	private EntityManager entityManager;
	private final static Logger logger = LoggerFactory.getLogger(MemberNamesProtectDaoImpl.class);

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public void save(MemberNamesProtect entity) {
		entityManager.persist(entity);
	}

	@Override
	public Optional<MemberNamesProtect> findOne(Integer primaryKey) {
		return Optional.ofNullable(entityManager.find(MemberNamesProtect.class, primaryKey));
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public Optional<Boolean> edit(MemberNamesProtect updateEntity) {
		try {
			entityManager.merge(updateEntity);
			return Optional.of(true);
		} catch (Exception e) {
			if(logger.isDebugEnabled()){
				logger.debug(e.getMessage(), e);
			}
		}
		return Optional.empty();
	}

	@Override
	public Stream<MemberNamesProtect> findAll() {
		return entityManager.createQuery("SELECT mnp FROM MemberNamesProtect mnp", MemberNamesProtect.class).getResultStream();
	}

	@Override
	public long count() {
		try {
			return entityManager.createQuery("SELECT COUNT(mnp) FROM MemberNamesProtectDao mnp", Long.class).getSingleResult();
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("[count][MemberNamesProtectDao]", e);
			}
		}
		return 0L;
	}

	@Override
	public Stream<MemberNamesProtect> findAll(boolean status) {
		return entityManager.createQuery("SELECT mnp FROM MemberNamesProtect mnp WHERE mnp.status = ?1", MemberNamesProtect.class).setParameter(1, status).getResultStream();
	}

	@Override
	public Optional<MemberNamesProtect> findOne(String memberNames) {
		try{
			MemberNamesProtect mnp = entityManager.createQuery("SELECT mnp FROM MemberNamesProtect mnp WHERE mnp.status = ?1 AND mnp.memberNames = ?2", MemberNamesProtect.class)
					.setParameter(1, true)
					.setParameter(2, memberNames)
					.getSingleResult();
			return Optional.ofNullable(mnp);
		}catch (javax.persistence.NoResultException e) {
			if(logger.isDebugEnabled()){
				logger.debug(e.getMessage(), e);
			}
		}
		return Optional.empty();
	}

	@Override
	public long count(boolean status) {
		try {
			return entityManager.createQuery("SELECT COUNT(mnp) FROM MemberNamesProtectDao mnp WHERE mnp.status = ?1", Long.class).setParameter(1, status).getSingleResult();
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("[count][MemberNamesProtectDao]", e);
			}
		}
		return 0L;
	}

}
