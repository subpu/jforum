package com.apobates.forum.member.impl.event;

import com.apobates.forum.member.entity.MemberVipExchangeRecords;
import org.springframework.context.ApplicationEvent;

public class MemberVipExchangeEvent extends ApplicationEvent {
    private final MemberVipExchangeRecords record;

    public MemberVipExchangeEvent(Object source, MemberVipExchangeRecords record) {
        super(source);
        this.record = record;
    }

    public MemberVipExchangeRecords getRecord() {
        return record;
    }
}