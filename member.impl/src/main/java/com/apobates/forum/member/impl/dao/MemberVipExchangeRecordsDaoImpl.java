package com.apobates.forum.member.impl.dao;

import com.apobates.forum.member.api.dao.MemberVipExchangeRecordsDao;
import com.apobates.forum.member.entity.MemberVipExchangeRecords;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;

/**
 *
 * @author xiaofanku
 * @since 20200921
 */
@Repository
public class MemberVipExchangeRecordsDaoImpl implements MemberVipExchangeRecordsDao {
    @PersistenceContext
    private EntityManager entityManager;
    private final static Logger logger = LoggerFactory.getLogger(MemberVipExchangeRecordsDaoImpl.class);

    @Override
    public Page<MemberVipExchangeRecords> findAllByMember(long memberId, Pageable pageable) {
        final long total = findAllByMemberCount(memberId);
        if (total == 0) {
            return emptyResult();
        }
        final String SQL = "SELECT ver FROM MemberVipExchangeRecords ver WHERE ver.memberId = ?1 ORDER BY ver.entryDateTime DESC";
        TypedQuery<MemberVipExchangeRecords> query = entityManager.createQuery(SQL, MemberVipExchangeRecords.class).setParameter(1, memberId);
        query.setFirstResult(pageable.getOffset());
        query.setMaxResults(pageable.getPageSize());

        final Stream<MemberVipExchangeRecords> result = query.getResultStream();
        return new Page<MemberVipExchangeRecords>() {
            @Override
            public long getTotalElements() {
                return total;
            }

            @Override
            public Stream<MemberVipExchangeRecords> getResult() {
                return result;
            }
        };
    }
    private long findAllByMemberCount(long memberId){
        try {
            return entityManager.createQuery("SELECT COUNT(ver) FROM MemberVipExchangeRecords ver WHERE ver.memberId = ?1", Long.class).setParameter(1, memberId).getSingleResult();
        } catch (Exception e) {
            if (logger.isDebugEnabled()) {
                logger.debug("[findAllByMemberCount][MemberVipExchangeRecords]", e);
            }
        }
        return 0L;
    }
    @Override
    public Page<MemberVipExchangeRecords> findAll(LocalDateTime start, LocalDateTime finish, Pageable pageable) {
        final long total = countByDateRange(start, finish);
        if (total == 0) {
            return emptyResult();
        }
        final String SQL = "SELECT ver FROM MemberVipExchangeRecords ver WHERE ver.activeDateTime >= ?1 AND ver.lapseDateTime <= ?2 ORDER BY ver.entryDateTime DESC";
        TypedQuery<MemberVipExchangeRecords> query = entityManager.createQuery(SQL, MemberVipExchangeRecords.class).setParameter(1, start).setParameter(2, finish);
        query.setFirstResult(pageable.getOffset());
        query.setMaxResults(pageable.getPageSize());

        final Stream<MemberVipExchangeRecords> result = query.getResultStream();
        return new Page<MemberVipExchangeRecords>() {
            @Override
            public long getTotalElements() {
                return total;
            }

            @Override
            public Stream<MemberVipExchangeRecords> getResult() {
                return result;
            }
        };
    }
    private long countByDateRange(LocalDateTime start, LocalDateTime finish){
        try {
            return entityManager.createQuery("SELECT COUNT(ver) FROM MemberVipExchangeRecords ver WHERE ver.activeDateTime >= ?1 AND ver.lapseDateTime <= ?2 ", Long.class)
                    .setParameter(1, start)
                    .setParameter(2, finish)
                    .getSingleResult();
        } catch (Exception e) {
            if (logger.isDebugEnabled()) {
                logger.debug("[countByDateRange][MemberVipExchangeRecords]", e);
            }
        }
        return 0L;
    }

    @Override
    public Stream<MemberVipExchangeRecords> findAllOfRecent(int size) {
        return entityManager.createQuery("SELECT ver FROM MemberVipExchangeRecords ver ORDER BY ver.entryDateTime DESC", MemberVipExchangeRecords.class).setMaxResults(size).getResultStream();
    }

    @Override
    public Page<MemberVipExchangeRecords> findAll(Pageable pageable) {
        final long total = count();
        if (total == 0) {
            return emptyResult();
        }
        final String SQL = "SELECT ver FROM MemberVipExchangeRecords ver ORDER BY ver.entryDateTime DESC";
        TypedQuery<MemberVipExchangeRecords> query = entityManager.createQuery(SQL, MemberVipExchangeRecords.class);
        query.setFirstResult(pageable.getOffset());
        query.setMaxResults(pageable.getPageSize());

        final Stream<MemberVipExchangeRecords> result = query.getResultStream();
        return new Page<MemberVipExchangeRecords>() {
            @Override
            public long getTotalElements() {
                return total;
            }

            @Override
            public Stream<MemberVipExchangeRecords> getResult() {
                return result;
            }
        };
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void save(MemberVipExchangeRecords entity) {
        entityManager.persist(entity);
    }

    @Override
    public Optional<MemberVipExchangeRecords> findOne(Long primaryKey) {
        return Optional.ofNullable(entityManager.find(MemberVipExchangeRecords.class, primaryKey));
    }

    @Override
    public Optional<Boolean> edit(MemberVipExchangeRecords updateEntity) {
        return Optional.empty();
    }

    @Override
    public Stream<MemberVipExchangeRecords> findAll() {
        return Stream.empty();
    }

    @Override
    public long count() {
        try {
            return entityManager.createQuery("SELECT COUNT(ver) FROM MemberVipExchangeRecords ver", Long.class).getSingleResult();
        } catch (Exception e) {
            if (logger.isDebugEnabled()) {
                logger.debug("[count][MemberVipExchangeRecords]", e);
            }
        }
        return 0L;
    }

    @Override
    public Stream<MemberVipExchangeRecords> findAllByExpire(LocalDateTime lapseDateStart, LocalDateTime lapseDateFinish) {
        return entityManager.createQuery("SELECT ver FROM MemberVipExchangeRecords ver WHERE ver.lapseDateTime BETWEEN ?1 AND ?2 ORDER BY ver.entryDateTime DESC", MemberVipExchangeRecords.class)
                .setParameter(1, lapseDateStart)
                .setParameter(2, lapseDateFinish)
                .getResultStream();
    }
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public int expired(Collection<Long> idList) {
        int affect = 0;
        Query query = entityManager.createQuery("UPDATE MemberVipExchangeRecords ver SET ver.status = ?1 WHERE ver.id = ?2 AND ver.status = ?3");
        for (Long id : idList) {
            affect += query.setParameter(1, false).setParameter(2, id).setParameter(3, true).executeUpdate();
        }
        return affect;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public Optional<Boolean> expired(long id) {
        int affect = entityManager.createQuery("UPDATE MemberVipExchangeRecords ver SET ver.status = ?1 WHERE ver.id = ?2 AND ver.status = ?3")
                .setParameter(1, false)
                .setParameter(2, id)
                .setParameter(3, true)
                .executeUpdate();
        if (affect == 1) {
            return Optional.of(true);
        }
        return Optional.empty();
    }
}
