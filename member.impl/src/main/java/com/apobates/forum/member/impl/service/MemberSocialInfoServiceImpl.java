package com.apobates.forum.member.impl.service;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.event.elderly.MemberAction;
import com.apobates.forum.event.elderly.MemberActionDescriptor;
import com.apobates.forum.member.api.dao.MemberSocialInfoDao;
import com.apobates.forum.member.api.service.MemberSocialInfoService;
import com.apobates.forum.member.entity.MemberSocialInfo;
import com.apobates.forum.utils.Commons;

@Service
public class MemberSocialInfoServiceImpl implements MemberSocialInfoService{
	@Autowired
	private MemberSocialInfoDao memberSocialInfoDao;
	@Value("${site.dec.salt}")
	private String globalDecSalt;
	private final static Logger logger = LoggerFactory.getLogger(MemberSocialInfoServiceImpl.class);

	@MemberAction(action = ForumActionEnum.MEMBER_PROFILE_SOCIAL, argName = "memberId")
	@Override
	public Optional<MemberSocialInfo> create(long memberId, String memberNames, String alibaba, String weibo, String weixin, String qq, String unEncryptEmailString, String globalDecSalt, MemberActionDescriptor actionDescriptor)throws IllegalStateException {
		//加密邮箱
		String email="";
		if(Commons.isMailString(unEncryptEmailString) && Commons.isNotBlank(globalDecSalt)) {
			email = encryptEmail(unEncryptEmailString, memberNames, globalDecSalt);
		}
		MemberSocialInfo msi = new MemberSocialInfo(memberId, memberNames, alibaba, weibo, weixin, qq, email);
		try {
			memberSocialInfoDao.save(msi);
			return Optional.of(msi);
		}catch(Exception e) {
			throw new IllegalStateException(e.getMessage());
		}
	}

	@MemberAction(action = ForumActionEnum.MEMBER_PROFILE_SOCIAL, editable=true, argName="memberId")
	@Override
	public Optional<MemberSocialInfo> edit(long id, long memberId, String memberNames, String alibaba, String weibo, String weixin, String qq, String unEncryptEmailString, String globalDecSalt, MemberActionDescriptor actionDescriptor)throws IllegalStateException {
		Optional<MemberSocialInfo> obj = get(id);
		if(!obj.isPresent()) {
			throw new IllegalStateException("会员社交信息不存在");
		}
		MemberSocialInfo msi = obj.get();
		//
		if(Commons.isMailString(unEncryptEmailString) && Commons.isNotBlank(globalDecSalt)) {
			String email = encryptEmail(unEncryptEmailString, memberNames, globalDecSalt);
			msi.setEmail(email);
		}
		if(Commons.isNotBlank(memberNames)) {
			msi.setMemberNames(memberNames);
		}
		if(Commons.isNotBlank(alibaba)) {
			msi.setAlibaba(alibaba);
		}
		if(Commons.isNotBlank(weibo)) {
			msi.setWeibo(weibo);
		}
		if(Commons.isNotBlank(weixin)) {
			msi.setWeixin(weixin);
		}
		if(Commons.isNotBlank(qq)) {
			msi.setQq(qq);
		}
		try {
			memberSocialInfoDao.edit(msi);
			return Optional.of(msi);
		}catch(Exception e) {
			throw new IllegalStateException(e.getMessage());
		}
	}

	@Override
	public Optional<MemberSocialInfo> get(long id) {
		return memberSocialInfoDao.findOne(id);
	}

	@Override
	public Optional<MemberSocialInfo> getByMember(long memberId) {
		return memberSocialInfoDao.findOneByMember(memberId);
	}

	@Override
	public Optional<MemberSocialInfo> getByMember(String memberNames, String unEncryptEmailString, String globalDecSalt)throws IllegalStateException {
		if (!Commons.isMailString(unEncryptEmailString)) {
			throw new IllegalStateException("邮箱地址校验失败");
		}
		if (!Commons.isNotBlank(globalDecSalt)) {
			throw new IllegalStateException("缺少重要的参数.操作中断");
		}
		try {
			String secretString = String.format("%s/%s", globalDecSalt, memberNames);
			String email = Commons.desEncrypt(secretString, unEncryptEmailString, () -> null);
			if (null == email) {
				throw new IllegalStateException("编码邮箱地址失败.操作中断");
			}
			return memberSocialInfoDao.findOneByMember(memberNames, email);
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("解码邮箱地址失败");
			}
			throw new IllegalStateException(e.getMessage());
		}
	}

	private String encryptEmail(String unEncryptEmailString, String memberNames, String globalDecSalt) {
		String secretString = String.format("%s/%s", globalDecSalt, memberNames);
		return Commons.desEncrypt(secretString, unEncryptEmailString, () -> "*");
	}
}
