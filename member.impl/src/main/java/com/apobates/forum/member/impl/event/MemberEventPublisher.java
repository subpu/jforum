package com.apobates.forum.member.impl.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
/**
 * 会员模块事件发布器
 * 
 * @author xiaofanku
 * @since 20190703
 */
@Component
public class MemberEventPublisher {
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;
    
    /**
     * 发布登录事件
     * @param boardCreateEvent
     */
    public void publishSignInEvent(MemberSignInEvent memberSignInEvent) {
        applicationEventPublisher.publishEvent(memberSignInEvent);
    }
    
    /**
     * 发布注册事件
     * @param topicPublishEvent
     */
    public void publishSignUpEvent(MemberSignUpEvent memberSignUpEvent) {
        applicationEventPublisher.publishEvent(memberSignUpEvent);
    }
    
    /**
     * 发布会员惩罚事件
     * @param memberPenalizeEvent
     */
    public void publishPenalizeEvent(MemberPenalizeEvent memberPenalizeEvent){
        applicationEventPublisher.publishEvent(memberPenalizeEvent);
    }
    
    /**
     * 发布会员注销事件
     * @param memberSignOutEvent
     */
    public void publishSignOutEvent(MemberSignOutEvent memberSignOutEvent){
        applicationEventPublisher.publishEvent(memberSignOutEvent);
    }

    /**
     * 发布会员变化事件
     * @param memberChangeEvent
     */
    public void publishChangeEvent(MemberChangeEvent memberChangeEvent){
        applicationEventPublisher.publishEvent(memberChangeEvent);
    }

    /**
     * 发布VIP交易记录
     * @param memberVipExchangeEvent
     */
    public void publishVipExchangeEvent(MemberVipExchangeEvent memberVipExchangeEvent){
        applicationEventPublisher.publishEvent(memberVipExchangeEvent);
    }
}
