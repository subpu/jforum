package com.apobates.forum.member.impl.service;

import java.util.Optional;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.apobates.forum.member.api.dao.MemberDao;
import com.apobates.forum.member.api.dao.MemberPenalizeRecordsDao;
import com.apobates.forum.member.api.service.MemberPenalizeRecordsService;
import com.apobates.forum.member.entity.MemberPenalizeRecords;
import com.apobates.forum.member.impl.event.MemberEventPublisher;
import com.apobates.forum.member.impl.event.MemberPenalizeEvent;
import com.apobates.forum.utils.DateTimeUtils;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;

@Service
public class MemberPenalizeRecordsServiceImpl implements MemberPenalizeRecordsService{
	@Autowired
	private MemberPenalizeRecordsDao memberPenalizeRecordsDao;
	@Autowired
	private MemberEventPublisher memberEventPublisher;
	@Autowired
	private MemberDao memberDao;
	
	@Override
	public Page<MemberPenalizeRecords> getAll(Pageable pageable) {
		return memberPenalizeRecordsDao.findAll(pageable);
	}

	@Override
	public Stream<MemberPenalizeRecords> getByMember(long memberId) {
		return memberPenalizeRecordsDao.findAllByMember(memberId);
	}

	@Override
	public long create(MemberPenalizeRecords entity)throws IllegalStateException {
		if(memberPenalizeRecordsDao.countForMember(entity.getMemberId())>0){
			throw new IllegalStateException("会员已经处于惩罚期");
		}
		
		try{
			memberPenalizeRecordsDao.save(entity);
			if(entity.getId()>0){
				// 应该在惩罚生效时发送,暂不支持预定日期的惩罚,只支持即可生效
				memberEventPublisher.publishPenalizeEvent(
						new MemberPenalizeEvent(this, 
												entity.getMemberId(), 
												entity.getMemberNickname(), 
												entity.getArrive(), 
												entity.getDuration(), 
												entity.getJudger(), 
												entity.getJudgeNickname(), 
												entity.getReason()));
				return entity.getId();
			}
		}catch(Exception e){
			throw new IllegalStateException(e.getMessage());
		}
		throw new IllegalStateException("创建会员惩罚记录失败");
	}

	@Override
	public Optional<MemberPenalizeRecords> get(long id) {
		return memberPenalizeRecordsDao.findOne(id);
	}
	//IS BAD
	@Override
	public Optional<Boolean> expired(long id) throws IllegalStateException{
		Optional<MemberPenalizeRecords> data = get(id);
		if(!data.isPresent()){
			throw new IllegalStateException("会员惩罚记录不存在");
		}
		MemberPenalizeRecords obj = data.get(); 
		boolean isChangeMember = obj.isStatus();
		//是否正处于惩罚期
		boolean isDoing = DateTimeUtils.isFeatureDate(obj.getRebirthDateTime()); 
		if(isDoing){
			throw new IllegalStateException("会员惩罚进行中,不可以删除");
		}
		obj.setStatus(false);
		Optional<Boolean> result = memberPenalizeRecordsDao.edit(obj);
		//编辑成功 && 惩罚进行时(会员的状态变了)
		if(result.isPresent() && isChangeMember){
			memberDao.editMemberStatus(obj.getMemberId(), obj.getOriginal());
		}
		return result;
	}

	@Override
	public Page<MemberPenalizeRecords> getAll(long buildMemberId, Pageable pageable) {
		return memberPenalizeRecordsDao.findAll(buildMemberId, pageable);
	}
}
