package com.apobates.forum.member.impl.event.listener;

import com.apobates.forum.member.impl.event.MemberPenalizeEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import com.apobates.forum.member.api.dao.MemberDao;
/**
 * 会员惩罚更新状态
 * 
 * @author xiaofanku
 * @since 20190804
 */
@Component
public class MemberPenalizeChangeStatusListener implements ApplicationListener<MemberPenalizeEvent>{
	@Autowired
	private MemberDao memberDao;
	private final static Logger logger = LoggerFactory.getLogger(MemberPenalizeChangeStatusListener.class);
	
	@Override
	public void onApplicationEvent(MemberPenalizeEvent event) {
		logger.info("[Member][PenalizeEvent][1]惩罚状态更新开始");
		//惩罚开始了吗?,暂不支持预定日期的惩罚,只支持即可生效
		memberDao.editMemberStatus(event.getMember(), event.getArrive());
		logger.info("[Member][PenalizeEvent][1]惩罚状态更新结束");
	}
}
