package com.apobates.forum.member.impl.event;

import org.springframework.context.ApplicationEvent;
import com.apobates.forum.member.entity.MemberStatusEnum;
/**
 * 会员惩罚事件
 * 
 * @author xiaofanku
 * @since 20190804
 */
public class MemberPenalizeEvent extends ApplicationEvent{
	private static final long serialVersionUID = -8131311352503255701L;
	//被惩罚的会员
	private final long member;
	private final String names;
	//谁出具的惩罚
	private final long judger;
	private final String judgeNames;
	//原因
	private final String reason;
	//时长:x(小时|分)
	private final String duration;
	//惩罚降临的状态
	private final MemberStatusEnum arrive;
	
	public MemberPenalizeEvent(Object source, long member, String names, MemberStatusEnum arrive, String duration, long judger, String judgeNames, String reason) {
		super(source);
		this.member = member;
		this.names = names;
		this.judger = judger;
		this.judgeNames = judgeNames;
		this.arrive = arrive;
		this.duration = duration;
		this.reason = reason;
	}

	public long getMember() {
		return member;
	}

	public long getJudger() {
		return judger;
	}

	public String getJudgeNames() {
		return judgeNames;
	}

	public String getReason() {
		return reason;
	}

	public MemberStatusEnum getArrive() {
		return arrive;
	}

	public String getNames() {
		return names;
	}

	public String getDuration() {
		return duration;
	}

}
