package com.apobates.forum.member.impl.dao;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.apobates.forum.member.api.dao.RegisteInviteCodeDao;
import com.apobates.forum.member.entity.RegisteInviteCode;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;

@Repository
public class RegisteInviteCodeDaoImpl implements RegisteInviteCodeDao{
	@PersistenceContext
	private EntityManager entityManager;
	private final static Logger logger = LoggerFactory.getLogger(RegisteInviteCodeDaoImpl.class);

	@Override
	public Page<RegisteInviteCode> findAll(Pageable pageable) {
		final long total = count();
		if (total == 0) {
			return emptyResult();
		}
		TypedQuery<RegisteInviteCode> query = entityManager.createQuery("SELECT ric FROM RegisteInviteCode ric ORDER BY ric.buildDateTime DESC", RegisteInviteCode.class);
		query.setFirstResult(pageable.getOffset());
		query.setMaxResults(pageable.getPageSize());

		final Stream<RegisteInviteCode> result = query.getResultStream();
		return new Page<RegisteInviteCode>() {
			@Override
			public long getTotalElements() {
				return total;
			}

			@Override
			public Stream<RegisteInviteCode> getResult() {
				return result;
			}
		};
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	@Override
	public void save(RegisteInviteCode entity) {
		entityManager.persist(entity);
	}

	@Override
	public Optional<RegisteInviteCode> findOne(Long primaryKey) {
		return Optional.ofNullable(entityManager.find(RegisteInviteCode.class, primaryKey));
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	@Override
	public Optional<Boolean> edit(RegisteInviteCode updateEntity) {
		try{
			entityManager.merge(updateEntity);
			return Optional.of(true);
		}catch(Exception e){
			if(logger.isDebugEnabled()){
				logger.debug(e.getMessage(), e);
			}
		}
		return Optional.empty();
	}

	@Override
	public Stream<RegisteInviteCode> findAll() {
		return Stream.empty();
	}

	@Override
	public long count() {
		try {
			return entityManager.createQuery("SELECT COUNT(ric) FROM RegisteInviteCode ric", Long.class).getSingleResult();
		} catch (Exception e) {
			if (logger.isDebugEnabled()) {
				logger.debug("[count][RegisteInviteCodeDao]", e);
			}
		}
		return 0L;
	}

	@Override
	public Optional<RegisteInviteCode> findOne(String code) {
		try {
			RegisteInviteCode ric = entityManager
					.createQuery("SELECT ric FROM RegisteInviteCode ric WHERE ric.unicode = ?1 AND ric.status = ?2 AND ric.active = ?3", RegisteInviteCode.class)
					.setParameter(1, code)
					.setParameter(2, true)
					.setParameter(3, false)
					.getSingleResult();
			return Optional.ofNullable(ric);
		} catch (javax.persistence.NoResultException e) {
			return Optional.empty();
		}
	}
	
	@Transactional(propagation=Propagation.REQUIRED)
	@Override
	public Optional<Boolean> active(long id, long memberId, String memberNames) throws IllegalStateException {
		int affect = entityManager.createQuery("UPDATE RegisteInviteCode ric SET ric.memberId = ?1, ric.memberNames = ?2, ric.activeDateTime = ?3, ric.active = ?4 WHERE ric.id = ?5 AND ric.active = ?6 AND ric.status = ?7")
				.setParameter(1, memberId)
				.setParameter(2, memberNames)
				.setParameter(3, LocalDateTime.now())
				.setParameter(4, true)
				.setParameter(5, id)
				.setParameter(6, false)
				.setParameter(7, true)
				.executeUpdate();
		return affect == 1 ? Optional.of(true) : Optional.empty();
	}

}
