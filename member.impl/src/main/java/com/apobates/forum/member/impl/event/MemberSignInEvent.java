package com.apobates.forum.member.impl.event;

import org.springframework.context.ApplicationEvent;
import com.apobates.forum.member.entity.Member;
/**
 * 会员登录事件
 * 
 * @author xiaofanku
 * @since 20190703
 */
public class MemberSignInEvent extends ApplicationEvent{
	private static final long serialVersionUID = 2594623388491543454L;
	private final Member member;
	private final String ipAddr;
	private final String refererURL;
	private final String device;
	
	public MemberSignInEvent(Object source, Member member, String ipAddr, String refererURL, String device) {
		super(source);
		this.member = member;
		this.ipAddr = ipAddr;
		this.refererURL = refererURL;
		this.device = device;
	}

	public Member getMember() {
		return member;
	}

	public String getIpAddr() {
		return ipAddr;
	}

	public String getRefererURL() {
		return refererURL;
	}

	public String getDevice() {
		return device;
	}
}
