package com.apobates.forum.event.elderly;

import javax.servlet.http.HttpServletRequest;

import com.apobates.forum.utils.Commons;

/**
 * 操作的肇事信息
 * @author xiaofanku
 * @since 20190328
 */
public interface ActionEventCulpritor {
	/**
	 * 返回肇事会员ID
	 * @return
	 */
	public long getMemberId();
	
	/**
	 * 返回肇事会员昵称
	 * @return
	 */
	public String getMemberNickname();
	
	/**
	 * 返回肇事会员使用的Ip
	 * @return
	 */
	public String getIpAddr();
	
	/**
	 * 返回肇事标识
	 * @return
	 */
	public String getToken();
	
	/**
	 * 返回HTTP请求的浏览器User-Agent
	 * @return
	 */
	public String getUserAgent();
	
	public static ActionEventCulpritor getInstance(final long memberId, final String memberNickname, final HttpServletRequest request, final String token) {
		return new ActionEventCulpritor() {
			@Override
			public long getMemberId() {
				return memberId;
			}
			
			@Override
			public String getMemberNickname() {
				return memberNickname;
			}
			
			@Override
			public String getIpAddr() {
				return Commons.getRemoteAddr(request);
			}
			
			@Override
			public String getToken() {
				return token;
			}
			
			@Override
			public String getUserAgent() {
				return request.getHeader("User-Agent");
			}
		};
	}
}
