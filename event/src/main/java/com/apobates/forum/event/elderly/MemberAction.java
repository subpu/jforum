package com.apobates.forum.event.elderly;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 会员操作注解
 * @author xiaofanku
 * @since 20190628
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MemberAction {
    /**
     * 行为
     * @return 
     */
    public ForumActionEnum action();
    
    /**
     * 是编辑吗?默认为false
     * @return
     */
    public boolean editable() default false;
    
    /**
     * 操作的主要参数名, 默认为会员的ID(id)
     * 如果是登录或注册使用:用户名(names)
     * 方法的返回值用以识别会员
     * 
     * @return
     */
    public String argName() default "id";
    
    /**
     * 确定argName的类型
     * 
     * @return
     */
    public Class<?> argType() default Long.class;
}
