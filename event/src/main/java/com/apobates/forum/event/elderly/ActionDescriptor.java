package com.apobates.forum.event.elderly;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
 * 操作注解,仅现注解前端使用的方法实现
 * 注意:
 * 1:创建方法使用create, 返回值是主键 
 *   非创建方法方法名不限,但必需存在id的参数名
 * 2.注解的方法最后一个参数是ActionEventCulpritor culpritor
 * @author xiaofanku
 * @since 20190329
 * 
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ActionDescriptor {

    /**
     * 行为
     * @return 
     */
    public ForumActionEnum action();
    /**
     * 注解的方法是否返回boolean值,
     * true的返回值是Optional/Boolean
     * false的返回值是主键
     * @return 
     */
    public boolean isRedress() default true;
    
    /**
     * 当isRedress=true时方法的参数中的主键是哪个参数名
     * @return 
     */
    public String keyName() default "id";
}
