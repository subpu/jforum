package com.apobates.forum.event.elderly;

import javax.servlet.http.HttpServletRequest;
import com.apobates.forum.utils.Commons;
import eu.bitwalker.useragentutils.UserAgent;
/**
 * 会员请求描述符
 * @author xiaofanku
 * @since 20190628
 */
public interface MemberActionDescriptor {
	/**
	 * 请求标识符
	 * @return
	 */
	public String getToken();
	
	/**
	 * IP地址
	 * @return
	 */
	public String getIpAddr();
	
	/**
	 * 来源
	 * @return
	 */
	public String getReferrer();
	
	/**
	 * 设备,Google Nexus 6
	 * @return
	 */
	public String getDevice();
	
	/**
	 * 代理,Chrome
	 * @return
	 */
	public String getAgent();
	
	public static MemberActionDescriptor getInstance(HttpServletRequest request, String token){
		final String referrer = request.getHeader("referer");
		final String ipAddr = Commons.getRequestIp(request);
		final String operateToken = Commons.isNotBlank(token)?token:"-";
		final String agent = request.getHeader("User-Agent");
		//
		return new MemberActionDescriptor(){
			@Override
			public String getToken() {
				return operateToken;
			}
			@Override
			public String getIpAddr() {
				return ipAddr;
			}
			@Override
			public String getReferrer() {
				return referrer;
			}
			@Override
			public String getDevice() {
				return UserAgent.parseUserAgentString(agent).getOperatingSystem().getName();
			}
			@Override
			public String getAgent() {
				return agent;
			}
		};
	}
}
