package com.apobates.forum.event.elderly;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.apobates.forum.utils.lang.EnumArchitecture;

/**
 * 操作枚举
 * @author xiaofanku@live.cn
 * @since 20170510|20190303
 */
public enum ForumActionEnum implements EnumArchitecture{
	//消息的操作
	
	//公共的操作
    COMMON_VISIT(100, "浏览", "common", true), //*:visit
    COMMON_REPORT(101, "举报", "common", false), //*:report
    COMMON_ATTACH(102, "上传附件", "common", false), //*:attach
    COMMON_FEEDBACK(103, "意见反馈", "common", false), 
    COMMON_CASUAL(104, "随便看看", "common", true), 
    //话题的操作
    TOPIC_PUBLISH(30, "发布话题", "topic", false), //topic:publish
    TOPIC_TOP(31, "话题置顶", "topic", false), //topic:top
    TOPIC_BEST(32, "话题加精", "topic", false), //topic:best
    TOPIC_EDIT(33, "编辑话题", "topic", false), //topic:edit
    TOPIC_DEL(34, "删除话题", "topic", false), //topic:del
    TOPIC_LOCK(35, "锁定话题", "topic", false), //topic:lock
    TOPIC_UNLOCK(36, "解锁话题", "topic", false), //topic:unlock
    TOPIC_MOVE(37, "移动话题", "topic", false), //topic:move
    TOPIC_CONFIG_EDIT(38, "话题配置文件", "topic", false), //topic:config.edit
    TOPIC_FAVORITE(39, "收藏话题", "topic", false), //topic:favorite
    TOPIC_BROWSE(40, "浏览话题", "topic", true), //topic:browse
    TOPIC_REPORT(41, "举报话题", "topic", false), 
    TOPIC_LIKED(42, "话题点赞", "topic", false),
    TOPIC_SEARCH(43, "话题搜索", "topic", false),
    TOPIC_TAG_DEL(44, "删除话题标签", "topic", false),
    TOPIC_TAG_ADD(45, "添加话题标签", "topic", false),
    TOPIC_SHARE(46, "话题分享", "topic", false),
    TOPIC_FAVORITE_CANCEL(47, "取消话题收藏", "topic", false),
    TOPIC_LIKED_CANCEL(48, "取消话题点赞", "topic", false),
    //回复的操作
    POSTS_REPLY(50, "回复话题", "posts", false), //posts:reply
    POSTS_EDIT(51, "编辑回复", "posts", false), //posts:edit
    POSTS_DEL(52, "删除回复", "posts", false), //posts:del
    POSTS_REPORT(53, "举报回复", "posts", false), 
    POSTS_LIKED(54, "回复点赞", "posts", false), 
    POSTS_QUOTE(55, "引用回复", "posts", false),
    POSTS_REPLY_FORM(56, "快速回复", "posts", false),
    //版块的操作
    BOARD_EDIT(20, "版块编辑", "board", false), //board:edit
    BOARD_UNLOCK(21, "解锁版块", "board", false), //board:unlock
    BOARD_LOCK(22, "锁定版块", "board", false), //board:lock
    BOARD_CONFIG_EDIT(23, "版块配置文件", "board", false), //board:config.edit
    BOARD_FAVORITE(24, "收藏版块", "board", false), //board:favorite
    BOARD_HOME(25, "版块主页", "board", true), 
    BOARD_BROWSE(26, "版块话题", "board", true), 
    BOARD_GROUP_BROWSE(27, "版块列表", "board", true), 
    BOARD_REMOVED(28, "删除版块", "board", false), 
    BOARD_FAVORITE_CANCEL(29, "取消版块收藏", "board", false),
    //权限上是基本权利,积分上可以设置分值
    //消息
    MESSAGE_BOX(70, "收件箱", "messages", false),
    MESSAGE_SENT(71, "发件箱", "messages", false),
    MESSAGE_CREATE(72, "创建消息", "messages", false),
    MESSAGE_BROWSE(73, "浏览消息", "messages", false),
    MESSAGE_ACTION_READ(74, "标记阅读", "messages", false),
    MESSAGE_ACTION_DEL(75, "标记删除", "messages", false),
    //会员的操作
    MEMBER_LOGIN(2, "登录", "member", true), //member:login
    MEMBER_REGISTER(1, "注册", "member", true), //member:register
    MEMBER_LOGOUT(10, "注销", "member", false),
    MEMBER_PROFILE_BASE(3, "帐户设置", "member", false), //member:profile.base
    MEMBER_PROFILE_SOCIAL(4, "社交信息", "member", false), //member:profile.social
    MEMBER_PROFILE_REALAUTH(5, "实名认证", "member", false), //member:profile.realauth
    MEMBER_PROFILE_CONTACT(6, "联系方式", "member",false), //member:profile.contact
    MEMBER_PROFILE_AVATAR(7, "更新头像", "member", false), //member:profile.avatar
    MEMBER_PASSPORT(8, "修改密码", "member", false), //member:passport
	MEMBER_PASSPORT_REST(9, "找回密码", "member", true),
	MEMBER_HISTORY(11, "历史记录", "member", false),
	MEMBER_HOME(12, "会员中心", "member", false);
	//重置密码|找回密码|注销
	
	private final int symbol;
	private final String title;
	private final String section;
	private final boolean supportGuest;
	
	private ForumActionEnum(int symbol, String title, String section, boolean isSupportGuest) {
		this.symbol = symbol;
		this.title = title;
		this.section = section;
		this.supportGuest = isSupportGuest;
	}
	@Override
	public int getSymbol() {
		return symbol;
	}
	@Override
	public String getTitle() {
		return title;
	}
	
	public String getSection() {
		return section;
	}
	
	public boolean isSupportGuest() {
		return supportGuest;
	}
	//外层key=getSection用于分组
	//List元素的Key=getSymbol
	public static Map<String, List<Map<Integer, String>>> getStructData(){
		Map<String, List<Map<Integer, String>>> data = new HashMap<>();
		for(ForumActionEnum ins : ForumActionEnum.values()) {
			String section = ins.getSection();
			if(!data.containsKey(section)){
				data.put(section, new ArrayList<>());
			}
			Map<Integer,String> innerV = new HashMap<>();
			innerV.put(ins.getSymbol(), ins.getTitle());
			data.get(section).add(innerV);
		}
		return data;
	}
	//管理员可以执行的操作
	public static Set<ForumActionEnum> getModeratorActions(){
		ForumActionEnum[] data = new ForumActionEnum[]{
			ForumActionEnum.TOPIC_TOP, ForumActionEnum.TOPIC_BEST, ForumActionEnum.TOPIC_EDIT, 
			ForumActionEnum.TOPIC_DEL, ForumActionEnum.TOPIC_LOCK, ForumActionEnum.TOPIC_UNLOCK, 
			ForumActionEnum.TOPIC_MOVE, ForumActionEnum.TOPIC_CONFIG_EDIT, ForumActionEnum.POSTS_EDIT,
			ForumActionEnum.POSTS_DEL, ForumActionEnum.BOARD_EDIT, ForumActionEnum.BOARD_UNLOCK, 
			ForumActionEnum.BOARD_LOCK, ForumActionEnum.BOARD_CONFIG_EDIT
		};
		return Collections.unmodifiableSet(EnumSet.copyOf(Arrays.asList(data)));
	}
	
	public String getInstanceMarker(){
		String section = this.name().toLowerCase().replace("_", ".");
		
		return section.replaceFirst("\\.", ":")+".token";
	}
}
