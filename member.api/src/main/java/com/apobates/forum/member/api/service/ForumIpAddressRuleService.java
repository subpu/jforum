package com.apobates.forum.member.api.service;

import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.member.entity.ForumIpAddressRule;

/**
 * ip地址过滤规则业务接口
 * @author xiaofanku
 * @since 20190810
 */
public interface ForumIpAddressRuleService {
	/**
	 * 查看所有ip地址过滤规则
	 * 
	 * @return
	 */
	Stream<ForumIpAddressRule> getAll();
	Stream<ForumIpAddressRule> getAll(boolean status);
	
	/**
	 * 保存一条ip地址过滤规则
	 * 
	 * @param addrole ip地址过滤规则
	 * @return
	 */
	Optional<ForumIpAddressRule> create(String addrole)throws IllegalStateException;
	
	/**
	 * 保存一条ip地址过滤规则
	 * 
	 * @param addrole ip地址过滤规则
	 * @param status  状态
	 * @return
	 */
	Optional<ForumIpAddressRule> create(String addrole, boolean status)throws IllegalStateException;
	
	/**
	 * 查看指定的ip地址过滤规则
	 * 
	 * @param id 规则ID
	 * @return
	 */
	Optional<ForumIpAddressRule> get(int id);
	
	/**
	 * 更新指定ip地址过滤规则的状态
	 * 
	 * @param id     规则ID
	 * @param status 状态
	 * @return
	 */
	Optional<Boolean> editStatus(int id, boolean status);
	
	/**
	 * 更新指定的ip地址过滤规则
	 * 
	 * @param id           规则ID
	 * @param updateEntity 更新的实例
	 * @return
	 */
	Optional<Boolean> edit(int id, ForumIpAddressRule updateEntity)throws IllegalStateException;
	
	/**
	 * 
	 * @param status
	 * @return
	 */
	long count(boolean status);
}
