package com.apobates.forum.member.api.service;

import com.apobates.forum.member.entity.MemberVipExchangeRecords;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.stream.Stream;

/**
 *
 * @author xiaofanku
 * @since 20200921
 */
public interface MemberVipExchangeRecordsService {
    /**
     * 查看指定日期范围的VIP交易记录
     *
     * @param start 开始日期
     * @param finish 结束日期
     * @param pageable 分页请求参数
     * @return
     */
    Page<MemberVipExchangeRecords> getAll(LocalDateTime start, LocalDateTime finish, Pageable pageable);

    /**
     * 查看指定会员的VIP交易记录
     *
     * @param memberId 会员ID
     * @param pageable 分页请求参数
     * @return
     */
    Page<MemberVipExchangeRecords> getAll(long memberId, Pageable pageable);

    /**
     * 查看所有的VIP交易记录
     *
     * @param pageable 分页请求参数
     * @return
     */
    Page<MemberVipExchangeRecords> getAll(Pageable pageable);

    /**
     * 最近的VIP交易记录
     *
     * @param size 显示的数量
     * @return
     */
    Stream<MemberVipExchangeRecords> getRecent(int size);

    /**
     * 手动结束VIP交易记录.必须是交易记录已经到期
     *
     * @param id VIP交易记录ID
     * @return
     */
    Optional<Boolean> expired(long id)throws IllegalArgumentException,IllegalStateException;
}
