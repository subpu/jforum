package com.apobates.forum.member.api.service;

import java.util.Optional;
import com.apobates.forum.event.elderly.MemberActionDescriptor;
import com.apobates.forum.member.entity.MemberSocialInfo;

/**
 * 会员社会化(社交)信息的业务接口
 * 
 * @author xiaofanku
 * @since 20190304
 */
public interface MemberSocialInfoService {
	/**
	 * 创建会员的社会化(社交)信息
	 * 
	 * @param memberId             会员ID
	 * @param memberNames          会员的登录帐号
	 * @param alibaba
	 * @param weibo                新浪微博
	 * @param weixin               腾讯微信号
	 * @param qq                   QQ帐号
	 * @param unEncryptEmailString 未加密的电子信箱
	 * @param actionDescriptor     会员操作描述符
	 * @return
	 */
	Optional<MemberSocialInfo> create(long memberId, String memberNames, String alibaba, String weibo, String weixin, String qq,String unEncryptEmailString, String globalDecSalt, MemberActionDescriptor actionDescriptor)throws IllegalStateException;

	/**
	 * 更新会员的社会化(社交)信息
	 * 
	 * @param id                   社会化(社交)信息ID
	 * @param memberId             会员ID
	 * @param memberNames          会员的登录帐号
	 * @param alibaba
	 * @param weibo                新浪微博
	 * @param weixin               腾讯微信号
	 * @param qq                   QQ帐号
	 * @param unEncryptEmailString 未加密的电子信箱
	 * @param actionDescriptor     会员操作描述符
	 * @return
	 */
	Optional<MemberSocialInfo> edit(long id, long memberId, String memberNames, String alibaba, String weibo, String weixin,String qq, String unEncryptEmailString, String globalDecSalt, MemberActionDescriptor actionDescriptor)throws IllegalStateException;

	/**
	 * 查看指定的会员的社会化(社交)信息
	 * 
	 * @param id 社会化(社交)信息ID
	 * @return
	 */
	Optional<MemberSocialInfo> get(long id);

	/**
	 * 查看指定的会员的社会化(社交)信息
	 * 
	 * @param memberId 会员ID
	 * @return
	 */
	Optional<MemberSocialInfo> getByMember(long memberId);

	/**
	 * 查看指定的会员的社会化(社交)信息 找回密码第一步
	 * 
	 * @param memberNames          会员的登录帐号
	 * @param unEncryptEmailString 未加密的电子信箱
	 * @return
	 */
	Optional<MemberSocialInfo> getByMember(String memberNames, String unEncryptEmailString, String globalDecSalt)throws IllegalStateException;
}
