package com.apobates.forum.member.api.dao;

import java.util.Optional;
import com.apobates.forum.member.entity.RegisteInviteCode;
import com.apobates.forum.utils.persistence.PagingAndSortingRepository;

/**
 * 会员注册邀请码持久接口
 * @author xiaofanku
 * @since 20190711
 */
public interface RegisteInviteCodeDao extends PagingAndSortingRepository<RegisteInviteCode, Long>{
	/**
	 * 查看指定的注册邀请码
	 * 
	 * @param code 邀请码
	 * @return
	 */
	Optional<RegisteInviteCode> findOne(String code);
	
	/**
	 * 激活指定的邀请码
	 * 
	 * @param id          邀请码Id
	 * @param memberId    会员ID
	 * @param memberNames 会员的登录帐号
	 * @return
	 */
	Optional<Boolean> active(long id, long memberId, String memberNames)throws IllegalStateException;
}
