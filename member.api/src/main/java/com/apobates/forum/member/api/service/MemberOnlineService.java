package com.apobates.forum.member.api.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.member.entity.MemberOnline;

/**
 * 会员在线业务接口
 * @author xiaofanku
 * @since 20190630
 */
public interface MemberOnlineService {
	/**
	 * 返回当前在线的会员
	 * 
	 * @return
	 */
	Stream<MemberOnline> getForNow();
	
	/**
	 * 查看指定的会员是否存在在线记录
	 * 
	 * @param memberIdList 会员ID列表
	 * @return
	 */
	Stream<MemberOnline> get(List<Long> memberIdList);
	
	/**
	 * 所有的会员在线记录
	 * 
	 * @return
	 */
	Stream<MemberOnline> get();
	
	/**
	 * 查看指定日期的在线记录
	 * 
	 * @param start  开始日期
	 * @param finish 结束日期
	 * @return
	 */
	Stream<MemberOnline> get(LocalDateTime start, LocalDateTime finish);
	
	/**
	 * 指定的会员是否存在在线记录
	 * 
	 * @param memberId 会员ID
	 * @return 
	 */
	Optional<MemberOnline> get(long memberId);
	
	/**
	 * 保存会员在线记录.如果会员存在过往的在线记录更新动作和活跃日期
	 * 
	 * @param memberOnline 会员在线记录
	 * @return
	 */
	long create(MemberOnline memberOnline)throws IllegalStateException;
	
	/**
	 * 统计指定日期在线的人数
	 * 
	 * @param start  开始日期
	 * @param finish 结束日期
	 * @return
	 */
	long count(LocalDateTime start, LocalDateTime finish);
}
