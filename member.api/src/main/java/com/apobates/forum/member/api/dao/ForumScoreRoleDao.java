package com.apobates.forum.member.api.dao;

import java.util.stream.Stream;
import com.apobates.forum.member.entity.ForumScoreRole;
import com.apobates.forum.utils.persistence.DataRepository;

/**
 * 积分规则的持久层接口
 * 
 * @author xiaofanku
 * @see 20190402
 */
public interface ForumScoreRoleDao extends DataRepository<ForumScoreRole, Integer> {
	/**
	 * 查看所有可用的积分规则
	 * 
	 * @return
	 */
	Stream<ForumScoreRole> findAllUsed();
}
