package com.apobates.forum.member.api.service;

import java.util.Optional;
import com.apobates.forum.member.entity.RegisteInviteCode;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;

/**
 * 会员注册邀请码业务接口
 * @author xiaofanku
 * @since 20190711
 */
public interface RegisteInviteCodeService {
	/**
	 * 所有邀请码
	 * 
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<RegisteInviteCode> get(Pageable pageable);
	
	/**
	 * 查看指定的邀请码
	 * 
	 * @param id 激活码Id
	 * @return
	 */
	Optional<RegisteInviteCode> get(long id);
	
	/**
	 * 查看指定的邀请码.只在当天有效
	 * 
	 * @param code 激活码
	 * @return
	 */
	Optional<RegisteInviteCode> get(String code);
	
	/**
	 * 销毁指定的邀请码
	 * 
	 * @param id 激活码Id
	 * @return
	 */
	Optional<Boolean> remove(long id)throws IllegalStateException;
	
	/**
	 * 激活指定的邀请码
	 * 
	 * @param id          激活码Id
	 * @param memberId    激活的会员Id
	 * @param memberNames 激活的会员登录帐号
	 * @return
	 */
	Optional<Boolean> activeCode(long id, long memberId, String memberNames)throws IllegalStateException;
	
	/**
	 * 创建邀请码
	 * 
	 * @param leader 创建邀请码的管理员
	 * @param code   邀请码
	 * @return
	 */
	long create(String leader, String code)throws IllegalStateException;
}
