package com.apobates.forum.member.api.service;

import java.util.Optional;
import com.apobates.forum.event.elderly.MemberActionDescriptor;
import com.apobates.forum.member.entity.MemberRealAuthentication;

/**
 * 会员实名认证的业务接口
 * 
 * @author xiaofanku
 * @since 20190304
 */
public interface MemberRealAuthenticationService {
	/**
	 * 创建会员的实名认证信息
	 * 
	 * @param memberId              会员ID
	 * @param realname              真实姓名
	 * @param birthYear             生日中的年
	 * @param birthMonth            生日中的月
	 * @param birthDay              生日中的日
	 * @param unEncryptIdentityCard 未加密的身份证号码
	 * @param actionDescriptor      会员操作描述符
	 * @return
	 */
	Optional<MemberRealAuthentication> create(long memberId, String realname, String birthYear, String birthMonth,String birthDay, String unEncryptIdentityCard, String globalDecSalt, MemberActionDescriptor actionDescriptor)throws IllegalStateException;

	/**
	 * 更新会员的实名认证信息
	 * 
	 * @param id                    实名认证信息ID
	 * @param memberId              会员ID
	 * @param realname              真实姓名
	 * @param birthYear             生日中的年
	 * @param birthMonth            生日中的月
	 * @param birthDay              生日中的日
	 * @param unEncryptIdentityCard 未加密的身份证号码
	 * @param actionDescriptor      会员操作描述符
	 * @return
	 */
	Optional<MemberRealAuthentication> edit(long id, long memberId, String realname, String birthYear, String birthMonth,String birthDay, String unEncryptIdentityCard, String globalDecSalt, MemberActionDescriptor actionDescriptor)throws IllegalStateException;

	/**
	 * 查看指定的会员实名认证信息
	 * 
	 * @param id 实名认证信息ID
	 * @return
	 */
	Optional<MemberRealAuthentication> get(long id);

	/**
	 * 查看指定会员的实名认证信息
	 * 
	 * @param memberId 会员ID
	 * @return
	 */
	Optional<MemberRealAuthentication> getByMember(long memberId);
}
