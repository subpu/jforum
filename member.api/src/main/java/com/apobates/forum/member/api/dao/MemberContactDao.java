package com.apobates.forum.member.api.dao;

import java.util.Optional;
import com.apobates.forum.member.entity.MemberContact;
import com.apobates.forum.utils.persistence.DataRepository;

/**
 * 会员联系方式的持久层接口
 * 
 * @author xiaofanku
 * @since 20190304
 */
public interface MemberContactDao extends DataRepository<MemberContact, Long> {
	/**
	 * 查看指定会员的联系方式
	 * 
	 * @param memberId 会员ID
	 * @return
	 */
	Optional<MemberContact> findOneByMember(long memberId);
}
