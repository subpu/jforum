package com.apobates.forum.member.api.dao;

import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.member.entity.MemberNamesProtect;
import com.apobates.forum.utils.persistence.DataRepository;
/**
 * 会员帐号保护持久层接口
 * @author xiaofanku
 * @since 20190809
 */
public interface MemberNamesProtectDao extends DataRepository<MemberNamesProtect, Integer>{
	/**
	 * 查看指定状态的帐号保护
	 * 
	 * @param status 状态
	 * @return
	 */
	Stream<MemberNamesProtect> findAll(boolean status);
	
	/**
	 * 查看指定的帐号保护
	 * 
	 * @param memberNames 会员帐号
	 * @return
	 */
	Optional<MemberNamesProtect> findOne(String memberNames);
	
	/**
	 * 统计指定状态的帐号保护数量
	 * 
	 * @param status 状态
	 * @return
	 */
	long count(boolean status);
}
