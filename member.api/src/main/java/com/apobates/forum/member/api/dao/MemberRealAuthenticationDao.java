package com.apobates.forum.member.api.dao;

import java.util.Optional;
import com.apobates.forum.member.entity.MemberRealAuthentication;
import com.apobates.forum.utils.persistence.DataRepository;

/**
 * 会员实名认证的持久层接口
 * 
 * @author xiaofanku
 * @since 20190304
 */
public interface MemberRealAuthenticationDao extends DataRepository<MemberRealAuthentication, Long> {
	/**
	 * 查看指定会员的实名认证信息
	 * 
	 * @param memberId 会员ID
	 * @return
	 */
	Optional<MemberRealAuthentication> findOneByMember(long memberId);
}
