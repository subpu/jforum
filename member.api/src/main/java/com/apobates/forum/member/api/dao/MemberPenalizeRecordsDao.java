package com.apobates.forum.member.api.dao;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.member.entity.MemberPenalizeRecords;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;
import com.apobates.forum.utils.persistence.PagingAndSortingRepository;

/**
 * 会员惩罚记录的持久层接口
 * @author xiaofanku
 * @since 20190722
 */
public interface MemberPenalizeRecordsDao extends PagingAndSortingRepository<MemberPenalizeRecords, Long>{
	/**
	 * 查看指定管理者开具的惩罚记录
	 * 
	 * @param buildMemberId 版主或管理员
	 * @param pageable      分页请求参数
	 * @return
	 */
	Page<MemberPenalizeRecords> findAll(long buildMemberId, Pageable pageable);
	
	/**
	 * 查看指定会员的可用惩罚记录
	 * 
	 * @param memberId 会员ID
	 * @return
	 */
	Stream<MemberPenalizeRecords> findAllByMember(long memberId);
	
	/**
	 * 查看指定日期范围内可用的惩罚记录,MemberPenalizeRecords.rebirthDateTime为参考
	 * 
	 * @param start  开始日期
	 * @param finish 结束日期
	 * @return
	 */
	Stream<MemberPenalizeRecords> findAllByExpire(LocalDateTime start, LocalDateTime finish);
	
	/**
	 * 更新惩罚记录的状态
	 * 
	 * @param id     惩罚记录ID
	 * @return
	 */
	Optional<Boolean> expired(long id);
	
	/**
	 * 更新惩罚记录的状态
	 * 
	 * @param idList 惩罚记录ID列表
	 * @return
	 */
	int expired(Collection<Long> idList);
	
	/**
	 * 统计指定会员的惩罚记录
	 * 
	 * @param memberId 会员ID
	 * @return
	 */
	long countForMember(long memberId);
}
