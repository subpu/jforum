package com.apobates.forum.member.api.service;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.member.entity.ForumScoreRole;

/**
 * 积分规则的业务层接口
 * 
 * @author xiaofanku
 * @see 20190402
 */
public interface ForumScoreRoleService {
	/**
	 * 所有可用的积分规则
	 * 
	 * @return
	 */
	Stream<ForumScoreRole> getAllUsed();

	/**
	 * 所有积分规则
	 * 
	 * @return
	 */
	Stream<ForumScoreRole> getAll();

	/**
	 * 查看指定的积分规则
	 * 
	 * @param id 积分规则ID
	 * @return
	 */
	Optional<ForumScoreRole> get(int id);

	/**
	 * 创建积分规则
	 * 
	 * @param action 动作类型
	 * @param degree 频次
	 * @param level  等级值
	 * @param score  积分
	 * @param status 状态
	 * @return
	 */
	Optional<ForumScoreRole> create(ForumActionEnum action, int degree, int level, double score, boolean status)throws IllegalStateException;

	/**
	 * 编辑指定的积分规则
	 * 
	 * @param id           积分规则ID
	 * @param updateEntity 更新的积分规则实例
	 * @return
	 */
	Optional<Boolean> edit(int id, ForumScoreRole updateEntity)throws IllegalStateException;
	/**
	 * 计算积分
	 * 
	 * @param statsActionSize key=操作, value=操作的次数
	 * @return
	 */
	double getMemberScore(Map<ForumActionEnum, Long> statsActionSize);
}
