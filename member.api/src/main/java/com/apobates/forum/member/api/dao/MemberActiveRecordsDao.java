package com.apobates.forum.member.api.dao;

import java.time.LocalDateTime;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Stream;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.member.entity.MemberActiveRecords;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;
import com.apobates.forum.utils.persistence.PagingAndSortingRepository;

/**
 * 会员操作记录的持久层接口
 * 
 * @author xiaofanku
 * @since 20190304
 */
public interface MemberActiveRecordsDao extends PagingAndSortingRepository<MemberActiveRecords, Long> {
	/**
	 * 查看指定日期的操作记录
	 * 
	 * @param start    开始日期
	 * @param finish   结束日期
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<MemberActiveRecords> findAll(LocalDateTime start, LocalDateTime finish, Pageable pageable);
	
	/**
	 * 查看批定会员的操作记录
	 * 
	 * @param memberId 会员ID
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<MemberActiveRecords> findAllByMember(long memberId, Pageable pageable);
	
	/**
	 * 查看指定会员登录记录
	 * 
	 * @param memberId    会员ID
	 * @param memberNames 登录帐号
	 * @param showSize    显示的数量
	 * @return
	 */
	Stream<MemberActiveRecords> findAllByMember(long memberId, String memberNames, int showSize);
	
	/**
	 * 查看指定会员帐号最近的登录记录
	 * 
	 * @param memberNames 登录帐号
	 * @param showSize    显示的数量
	 * @return
	 */
	Stream<MemberActiveRecords> findAllByMemberNames(String memberNames, int showSize);
	
	/**
	 * 查看指定会员最近的几次登录记录,只显示成功的
	 * 
	 * @param memberId    会员ID
	 * @param memberNames 登录帐号
	 * @param showSize    显示的数量
	 * @return
	 */
	Stream<MemberActiveRecords> findAllLoginAction(long memberId, String memberNames, int showSize);
	
	/**
	 * 统计会员的动作,包括登录,注册数
	 * 
	 * @param memberId 会员ID
	 * @return
	 */
	EnumMap<ForumActionEnum, Long> statsMemberAction(long memberId);
	
	/**
	 * 统计会员的动作数
	 * 
	 * @param memberId 会员ID
	 * @param actions  统计操作
	 * @return
	 */
	EnumMap<ForumActionEnum, Long> statsMemberAction(long memberId, List<ForumActionEnum> actions);
	
	/**
	 * 分组统计会员使用的设备
	 * 
	 * @return key=MemberActiveRecords.device
	 */
	Map<String, Long> groupForDevice();
	
	/**
	 * 分组统计会员使用的网络供应商
	 * 
	 * @return key=MemberActiveRecords.isp
	 */
	Map<String, Long> groupForISP();
	
	/**
	 * 分组统计会员所在的省份
	 * 
	 * @return key=MemberActiveRecords.province
	 */
	Map<String, Long> groupForProvince();
	
	/**
	 * 统计指定日期范围内会员活跃的数量
	 * 
	 * @param start  开始日期
	 * @param finish 结束日期
	 * @return Key = YYYY-MM-DD
	 */
	TreeMap<String, Long> groupMemberForActivity(LocalDateTime start, LocalDateTime finish);
}
