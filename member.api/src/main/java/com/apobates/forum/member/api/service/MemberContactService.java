package com.apobates.forum.member.api.service;

import java.util.Optional;
import com.apobates.forum.event.elderly.MemberActionDescriptor;
import com.apobates.forum.member.entity.MemberContact;
/**
 * 会员联系方式的业务接口
 * 
 * @author xiaofanku
 * @since 20190304
 */
public interface MemberContactService {
	/**
	 * 创建会员的联系方式信息
	 * 
	 * @param memberId              会员ID
	 * @param province              省份
	 * @param city                  城市
	 * @param region                区/县
	 * @param street                街道
	 * @param postcode              邮编
	 * @param unEncryptMobileString 未加密的手机号码
	 * @param actionDescriptor      会员操作描述符
	 * @return
	 */
	Optional<MemberContact> create(long memberId, String province, String city, String region, String street,String postcode, String unEncryptMobileString, MemberActionDescriptor actionDescriptor)throws IllegalStateException;

	/**
	 * 更新会员联系方式
	 * 
	 * @param id                    会员的联系方式ID
	 * @param memberId              会员ID
	 * @param province              省份
	 * @param city                  城市
	 * @param region                区/县
	 * @param street                街道
	 * @param postcode              邮编
	 * @param unEncryptMobileString 未加密的手机号码
	 * @param actionDescriptor      会员操作描述符
	 * @return
	 */
	Optional<MemberContact> edit(long id, long memberId, String province, String city, String region, String street,String postcode, String unEncryptMobileString, MemberActionDescriptor actionDescriptor)throws IllegalStateException;

	/**
	 * 查看指定的会员联系方式
	 * 
	 * @param id 会员的联系方式
	 * @return
	 */
	Optional<MemberContact> get(long id);

	/**
	 * 查看指定会员的联系方式
	 * 
	 * @param memberId 会员ID
	 * @return
	 */
	Optional<MemberContact> getByMember(long memberId);
}
