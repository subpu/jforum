package com.apobates.forum.member.api.dao;

import java.util.Optional;
import com.apobates.forum.member.entity.MemberSocialInfo;
import com.apobates.forum.utils.persistence.DataRepository;

/**
 * 会员社会化(社交)信息的持久层接口
 * 
 * @author xiaofanku
 * @since 20190304
 */
public interface MemberSocialInfoDao extends DataRepository<MemberSocialInfo, Long> {
	/**
	 * 查看指定会员的社会化(社交)信息
	 * 
	 * @param memberId 会员ID
	 * @return
	 */
	Optional<MemberSocialInfo> findOneByMember(long memberId);

	/**
	 * 查看指定会员的社会化(社交)信息 服务于密码重置功能
	 * 
	 * @param memberNames        会员登录帐号
	 * @param encryptEmailString 加密后的邮箱地址
	 * @return
	 */
	Optional<MemberSocialInfo> findOneByMember(String memberNames, String encryptEmailString);
}
