package com.apobates.forum.member.api.service;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Stream;
import com.apobates.forum.member.entity.MemberActiveRecords;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;

/**
 * 会员操作记录的业务接口
 * 
 * @author xiaofanku
 * @since 20190304
 */
public interface MemberActiveRecordsService {
	/**
	 * 查看指定会员的操作记录
	 * 
	 * @param memberId 会员ID
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<MemberActiveRecords> getByMember(long memberId, Pageable pageable);
	
	/**
	 * 查看会员的操作记录
	 * 
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<MemberActiveRecords> getAll(Pageable pageable);
	
	/**
	 * 查看指定会员登录记录
	 * 
	 * @param memberId    会员ID
	 * @param memberNames 登录帐号
	 * @param showSize    显示的数量
	 * @return
	 */
	Stream<MemberActiveRecords> getByMember(long memberId, String memberNames, int showSize);
	
	/**
	 * 查看指定的会员操作记录
	 * 
	 * @param id 会员活跃记录ID
	 * @return
	 */
	Optional<MemberActiveRecords> get(long id);
	
	/**
	 * 查看指定会员上次登录记录
	 * 
	 * @param memberId    会员ID
	 * @param memberNames 登录帐号
	 * @return
	 */
	Optional<MemberActiveRecords> getPreviousLoginRecord(long memberId, String memberNames);
	
	/**
	 * 保存会员操作记录
	 * 
	 * @param entity 会员的活跃记录
	 * @return
	 */
	long create(MemberActiveRecords entity)throws IllegalStateException;
	
	/**
	 * 分组统计会员使用的设备
	 * 
	 * @return key=MemberActiveRecords.device
	 */
	Map<String,Long> statsDevice();
	
	/**
	 * 分组统计会员使用的网络供应商
	 * 
	 * @return key=MemberActiveRecords.isp
	 */
	Map<String,Long> statsIsp();
	
	/**
	 * 分组统计会员所在的省份
	 * 
	 * @return key=MemberActiveRecords.province
	 */
	Map<String,Long> statsProvince();
	
	/**
	 * 统计指定日期范围内会员活跃的数量
	 * 
	 * @param start  开始日期
	 * @param finish 结束日期
	 * @return Key = YYYY-MM-DD
	 */
	TreeMap<String,Long> groupMemberForActivity(LocalDateTime start, LocalDateTime finish);
}
