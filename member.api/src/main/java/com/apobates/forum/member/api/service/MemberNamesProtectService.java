package com.apobates.forum.member.api.service;

import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.member.entity.MemberNamesProtect;

/**
 * 会员帐号保护业务层接口
 * @author xiaofanku
 * @since 20190809
 */
public interface MemberNamesProtectService {
	/**
	 * 查看保护的帐号
	 * 
	 * @return
	 */
	Stream<MemberNamesProtect> getAll();
	
	/**
	 * 查看保护的帐号
	 * 
	 * @param id 帐号保护ID
	 * @return
	 */
	Optional<MemberNamesProtect> get(int id);
	
	/**
	 * 增加一个帐号保护
	 * 
	 * @param memberNames 保护的帐号
	 * @return
	 */
	Optional<MemberNamesProtect> create(String memberNames)throws IllegalStateException;
	
	/**
	 * 增加一个帐号保护
	 * 
	 * @param memberNames 保护的帐号
	 * @param status      状态
	 * @return
	 */
	Optional<MemberNamesProtect> create(String memberNames, boolean status)throws IllegalStateException;
	
	/**
	 * 查看保护帐号的状态
	 * 
	 * @param id     帐号保护ID
	 * @param status 状态
	 * @return
	 */
	Optional<Boolean> editStatus(int id, boolean status);
	
	/**
	 * 编辑指定的帐户保护
	 * 
	 * @param id           帐号保护ID
	 * @param updateEntity 更新的实例
	 * @return
	 */
	Optional<Boolean> edit(int id, MemberNamesProtect updateEntity)throws IllegalStateException;
}
