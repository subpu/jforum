package com.apobates.forum.member.api.dao;

import com.apobates.forum.member.entity.MemberVipExchangeRecords;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;
import com.apobates.forum.utils.persistence.PagingAndSortingRepository;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;

/**
 *
 * @author xiaofanku
 * @since 20200921
 */
public interface MemberVipExchangeRecordsDao extends PagingAndSortingRepository<MemberVipExchangeRecords, Long> {
    /**
     * 查看指定会员的VIP交易记录
     *
     * @param memberId 会员ID
     * @param pageable 分页请求参数
     * @return
     */
    Page<MemberVipExchangeRecords> findAllByMember(long memberId, Pageable pageable);

    /**
     * 查看指定日期范围的VIP交易记录
     *
     * @param start 开始日期
     * @param finish 结束日期
     * @param pageable 分页请求参数
     * @return
     */
    Page<MemberVipExchangeRecords> findAll(LocalDateTime start, LocalDateTime finish, Pageable pageable);

    /**
     * 最近的VIP交易记录
     *
     * @param size 显示的数量
     * @return
     */
    Stream<MemberVipExchangeRecords> findAllOfRecent(int size);

    /**
     * 查看即将到期的VIP交易记录,以MemberVipExchangeRecords.lapseDateTime为参考
     *
     * @param lapseDateStart 到期日期的开始(凌晨)
     * @param lapseDateFinish 到期日期的结束(午夜)
     * @return
     */
    Stream<MemberVipExchangeRecords> findAllByExpire(LocalDateTime lapseDateStart, LocalDateTime lapseDateFinish);

    /**
     * 将指定的VIP交易记录作废
     *
     * @param idList 惩罚记录ID列表
     * @return
     */
    int expired(Collection<Long> idList);

    /**
     * 将指定的VIP交易记录作废
     *
     * @param id VIP交易记录ID
     * @return
     */
    Optional<Boolean> expired(long id);
}
