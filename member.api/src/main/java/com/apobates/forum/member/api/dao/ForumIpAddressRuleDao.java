package com.apobates.forum.member.api.dao;

import java.util.stream.Stream;
import com.apobates.forum.member.entity.ForumIpAddressRule;
import com.apobates.forum.utils.persistence.DataRepository;
/**
 * ip地址过滤规则持久接口
 * @author xiaofanku
 * @since 20190810
 */
public interface ForumIpAddressRuleDao extends DataRepository<ForumIpAddressRule, Integer> {
	/**
	 * 查看指定状态的ip地址过滤规则
	 * 
	 * @param status 状态
	 * @return
	 */
	Stream<ForumIpAddressRule> findAll(boolean status);
	
	/**
	 * 统计指定状态的ip地址过滤规则数量
	 * 
	 * @param status 状态
	 * @return
	 */
	long count(boolean status);
}
