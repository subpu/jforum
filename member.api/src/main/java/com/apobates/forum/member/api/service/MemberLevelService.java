package com.apobates.forum.member.api.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.member.entity.MemberLevel;
import com.apobates.forum.utils.CommonDoubleBean;

/**
 * 会员等级定义的业务接口
 * 
 * @author xiaofanku
 * @since 20190304
 */
public interface MemberLevelService {
	/**
	 * 所有可用的等级定义实例
	 * 
	 * @return
	 */
	Stream<MemberLevel> getAllUsed();
	
	/**
	 * 所有会员等级
	 * 
	 * @return
	 */
	Stream<MemberLevel> getAll();
	
	/**
	 * 创建新的会员等级
	 * 
	 * @param names     等级名称
	 * @param minScore  等级需要的最小积分
	 * @param score     等级需要的最大积分
	 * @param imageAddr 会员等级图片(经过编码后的地址)
	 */
	Optional<MemberLevel> create(String names, double minScore, double score, String imageAddr, boolean status)throws IllegalStateException;

	/**
	 * 更新会员等级
	 * 
	 * @param id        会员等级ID
	 * @param names     等级名称
	 * @param minScore  等级需要的最小积分
	 * @param score     等级需要的最大积分
	 * @param imageAddr 会员等级图片(经过编码后的地址)
	 */
	Optional<MemberLevel> edit(int id, String names, double minScore, double score, String imageAddr, boolean status)throws IllegalStateException;

	/**
	 * 查看指定的会员等级
	 * 
	 * @param id 会员等级ID
	 * @return
	 */
	Optional<MemberLevel> get(int id);
	
	/**
	 * 计算指定的积分是多少级
	 * 
	 * @param scoreValue 积分分值
	 * @return
	 */
	Optional<MemberLevel> getMemeberLevel(double scoreValue);

	/**
	 * 更新会员等级的状态
	 * 
	 * @param id     会员等级ID
	 * @param status 新的状态值
	 * @return
	 */
	boolean editStatus(int id, boolean status);
	
	/**
	 * 解决重复问题
	 * 
	 * @return
	 */
	List<CommonDoubleBean> getMemberLevelCommonBean();
}
