package com.apobates.forum.member.api.service;

import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.member.entity.MemberPenalizeRecords;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;

/**
 * 会员惩罚记录的业务层接口
 * @author xiaofanku
 * @since 20190722
 */
public interface MemberPenalizeRecordsService {
	/**
	 * 所有的惩罚记录
	 * 
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<MemberPenalizeRecords> getAll(Pageable pageable);
	
	/**
	 * 指定版主或管理员开具的惩罚记录
	 * 
	 * @param buildMemberId 版主或管理员
	 * @param pageable      分页请求参数
	 * @return
	 */
	Page<MemberPenalizeRecords> getAll(long buildMemberId, Pageable pageable);
	
	/**
	 * 查看指定会员可用的惩罚记录
	 * 
	 * @param memberId 会员ID
	 * @return
	 */
	Stream<MemberPenalizeRecords> getByMember(long memberId);
	
	/**
	 * 查看惩罚记录
	 * 
	 * @param id 罚记录ID
	 * @return
	 */
	Optional<MemberPenalizeRecords> get(long id);
	
	/**
	 * 手动结束惩罚记录.必须是惩罚期结束的记录
	 * 
	 * @param id 罚记录ID
	 * @return
	 */
	Optional<Boolean> expired(long id)throws IllegalStateException;
	
	/**
	 * 创建惩罚记录, 操作成功后会发送通知(MemberPenalizeEvent)
	 * 
	 * @param entity 惩罚记录
	 * @return
	 */
	long create(MemberPenalizeRecords entity)throws IllegalStateException;
}
