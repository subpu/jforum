package com.apobates.forum.member.api.dao;

import java.util.stream.Stream;
import com.apobates.forum.member.entity.MemberLevel;
import com.apobates.forum.utils.persistence.DataRepository;

/**
 * 会员等级定义的持久层接口
 * 
 * @author xiaofanku
 * @since 20190304
 */
public interface MemberLevelDao extends DataRepository<MemberLevel, Integer> {
	/**
	 * 所有可用的等级
	 * 
	 * @return
	 */
	Stream<MemberLevel> findAllUsed();
	
	/**
	 * 更新指定会员等级的状态
	 * 
	 * @param id     会员等级ID
	 * @param status 会员等级新的状态
	 * @return
	 */
	int editStatus(Integer id, boolean status);

}
