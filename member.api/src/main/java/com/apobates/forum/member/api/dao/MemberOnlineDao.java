package com.apobates.forum.member.api.dao;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Stream;
import com.apobates.forum.member.entity.MemberOnline;
import com.apobates.forum.utils.persistence.DataRepository;
/**
 * 会员在线持久接口
 * @author xiaofanku
 * @since 20190630
 */
public interface MemberOnlineDao  extends DataRepository<MemberOnline, Long>{
	/**
	 * 查看指定会员的在线记录
	 * 
	 * @param memberIdList 会员ID列表
	 * @return
	 */
	Stream<MemberOnline> findAll(List<Long> memberIdList);
	
	/**
	 * 查看指定日期范围之内的在线记录
	 * 
	 * @param start  开始日期
	 * @param finish 结束日期
	 * @return
	 */
	Stream<MemberOnline> findAll(LocalDateTime start, LocalDateTime finish);
	
	/**
	 * 统计指定日期范围之内的在线人数
	 * 
	 * @param start  开始日期
	 * @param finish 结束日期
	 * @return
	 */
	long countByDateTime(LocalDateTime start, LocalDateTime finish);
	
	/**
	 * 批量保存
	 * 
	 * @param entities 会员在线记录
	 * @return
	 */
	int batchSave(List<MemberOnline> entities);
}
