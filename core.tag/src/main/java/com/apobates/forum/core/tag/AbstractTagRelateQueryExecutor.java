package com.apobates.forum.core.tag;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;
/**
 * 标签相关查询实现类
 * 
 * @author xiaofanku
 * @since 20200111
 */
public abstract class AbstractTagRelateQueryExecutor implements TagRelateQuery{
    //目标文章的,K=词,V=词频
    private final Map<String, Integer> rawdata;
    
    public AbstractTagRelateQueryExecutor(Map<String, Integer> rawdata) {
        super();
        this.rawdata = Collections.unmodifiableMap(rawdata);
    }
    
    /**
     * 加载相关的标签源数据
     *
     * @param wordSource
     * @return 
     */
    public abstract AbstractTagRelateQueryExecutor load(Collection<RelateWordStats> wordSource);
    
    /**
     * 目标文章的标签结构, K=词语,V=词频
     *
     * @return
     */
    public Map<String, Integer> getRawdata() {
        return rawdata;
    }
    
    /**
     * 为结果集增加显示排序
     * 
     * @param src 要求方法调用前已完成排序
     * @return 
     */
    protected Stream<TagRelateResult> applyWithIndex(Stream<TagRelateResult> src){
        AtomicInteger index = new AtomicInteger(1);
        return src.map(trr->{
            int rk = index.getAndIncrement();
            return trr.replica(rk);
        });
    }
}
