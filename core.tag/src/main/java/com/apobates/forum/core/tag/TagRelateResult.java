package com.apobates.forum.core.tag;

import java.io.Serializable;

public class TagRelateResult implements Serializable, Comparable<TagRelateResult>{
	private static final long serialVersionUID = 6383091625086291323L;
	//话题
	private final long topic;
	//显示的顺序,小的先显示
	private final int ranking;
	//相似度
	private final double similarity;
	
	public TagRelateResult(long topic, int ranking, double similarity) {
		super();
		this.topic = topic;
		this.ranking = ranking;
		this.similarity = similarity;
	}
	/**
	 * 话题ID
	 * @return
	 */
	public long getTopic() {
		return topic;
	}
	/**
	 * 显示的顺序
	 * @return
	 */
	public int getRanking() {
		return ranking;
	}
	/**
	 * 相似度
	 * @return
	 */
	public double getSimilarity() {
		return similarity;
	}

	/**
	 * 为了设置排序值而复制对象
	 * 
	 * @param ranking
	 *            显示排序
	 * @return
	 */
	public TagRelateResult replica(final int ranking) {
		return new TagRelateResult(getTopic(), ranking, getSimilarity());
	}

	@Override
	public int compareTo(TagRelateResult o) {
		return ranking - o.getRanking();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (topic ^ (topic >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TagRelateResult other = (TagRelateResult) obj;
		if (topic != other.topic)
			return false;
		return true;
	}
	
}
