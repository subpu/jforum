package com.apobates.forum.core.tag;

import java.util.Map;
/**
 * 标签提取接口
 * 
 * @author xiaofanku
 * @since 20200110
 */
public interface TagExtraction {
	/**
	 * 提取标题和内容中的标签
	 * 
	 * @param title    标题
	 * @param content  内容
	 * @param keywords 提示词
	 * @return
	 */
	Map<String,Integer> extract(String title, String content, String... keywords);
	
	/**
	 * 行分隔符
	 * 
	 * @return
	 */
	String getLineSeparator();
}
