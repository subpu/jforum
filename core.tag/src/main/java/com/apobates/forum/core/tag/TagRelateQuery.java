package com.apobates.forum.core.tag;

import java.util.stream.Stream;
/**
 * 标签相关查询接口
 * 
 * @author xiaofanku
 * @since 20200111
 */
public interface TagRelateQuery {
	/**
	 * 标签相关查询
	 * 
	 * @param showSize 结果集的最大数量
	 * @return 
	 */
	Stream<TagRelateResult> getResult(int showSize);
}
