package com.apobates.forum.core.tag;
/**
 * 标签源数据
 * 
 * @author xiaofanku
 * @since 20200111
 */
public class RelateWordStats {
	private final long topic;
	//词
	private final String word;
	//词频
	private final int frequency;
	
	public RelateWordStats(long topic, String word, int frequency) {
		super();
		this.topic = topic;
		this.word = word;
		this.frequency = frequency;
	}
	/**
	 * 话题ID
	 * @return
	 */
	public long getTopic() {
		return topic;
	}
	/**
	 * 标签名/词语
	 * @return
	 */
	public String getWord() {
		return word;
	}
	/**
	 * 词频
	 * @return
	 */
	public int getFrequency() {
		return frequency;
	}
}
