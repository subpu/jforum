package com.apobates.forum.core.api.dao;

import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.BoardCarouselIndex;
import com.apobates.forum.core.entity.TopicCarousel;
import com.apobates.forum.core.entity.TopicCarouselSlide;
/**
 * 
 * @author xiaofanku
 * @since 20191109
 */
public interface BoardCarouselIndexDao {
	/**
	 * 创建版块与轮播图关联实例
	 * 
	 * @param entity 版块与轮播图关联
	 */
	void save(BoardCarouselIndex entity);
	
	/**
	 * 查看指定版块的最近轮播图的幻灯片
	 * 
	 * @param boardGroupId 版块组(卷)ID
	 * @param boardId      版块ID
	 * @return
	 */
	Stream<TopicCarouselSlide> findAllUsedByBoard(int boardGroupId, long boardId);
	
	/**
	 * 查看指定版块组(卷)的最近轮播图的幻灯片
	 * 
	 * @param boardGroupId 版块组(卷)ID
	 * @return
	 */
	Stream<TopicCarouselSlide> findAllUsedByBoard(int boardGroupId);
	
	/**
	 * 查看指定轮播图的关联记录
	 * 
	 * @param topicCarouselId 轮播图ID
	 * @return
	 */
	Stream<BoardCarouselIndex> findAllByCarousel(int topicCarouselId);
	
	/**
	 * 查看指定版块最近关联的轮播图
	 * 
	 * @param boardGroupId 版块组(卷)ID
	 * @param boardId      版块ID
	 * @return
	 */
	Optional<TopicCarousel> findOneByBoard(int boardGroupId, long boardId);
	
	/**
	 * 查看指定版块组(卷)最近关联的轮播图
	 * 
	 * @param boardGroupId 版块组(卷)ID
	 * @return
	 */
	Optional<TopicCarousel> findOneByBoard(int boardGroupId);
	
	/**
	 * 删除版块轮播图的关联记录
	 * 
	 * @param topicCarouselId 轮播图ID
	 * @param boardGroupId    版块组(卷)ID
	 * @param boardId         版块ID
	 * @return
	 */
	Optional<Boolean> deleteBoardCarouselIndex(int topicCarouselId, int boardGroupId, long boardId);
}
