package com.apobates.forum.core.api.service;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import com.apobates.forum.core.api.TopicStatsCollect;
import com.apobates.forum.core.entity.TopicActionCollection;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;

/**
 * 话题动作集合的业务层接口
 * 
 * @author xiaofanku
 * @since 20190328
 */
public interface TopicActionCollectionService {
	/**
	 * 指定会员的话题动作,同时加载话题对象
	 * 
	 * @param memberId 会员ID
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<TopicActionCollection> getByMember(long memberId, Pageable pageable);
	long countByMember(long memberId);
	
	/**
	 * 查看指定话题发生的动作
	 * 
	 * @param topicId  话题ID
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<TopicActionCollection> getByTopic(long topicId, Pageable pageable);
	
	/**
	 * 所有话题操作动作
	 * 
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<TopicActionCollection> getAll(Pageable pageable);
	
	/**
	 * 查看会员指定动作的操作记录,同时加载话题对象
	 * 
	 * @param memberId 会员ID
	 * @param action   操作动作
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<TopicActionCollection> getAllByMemberAction(long memberId, ForumActionEnum action, Pageable pageable);
	long countAllByMemberAction(long memberId, ForumActionEnum action);
	
	/**
	 * 查看会员指定动作的操作记录
	 * 
	 * @param memberId 会员ID
	 * @param actiones 操作动作列表
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<TopicActionCollection> getAllByMemberAction(long memberId, List<ForumActionEnum> actiones, Pageable pageable);
	
	/**
	 * 查看指定管理者(版主,大版主,管理员)删除的话题
	 * 
	 * @param memberId 会员ID
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<TopicActionCollection> getRecycleTopic(long memberId, Pageable pageable);
	
	/**
	 * 指定会员最近的话题动作
	 * 
	 * @param memberId 会员ID
	 * @param size     显示的数量
	 * @return
	 */
	Stream<TopicActionCollection> getRecentByMember(long memberId, int size);
	
	/**
	 * 统计会员的动作统计
	 * 
	 * @param memberId 会员ID
	 * @return
	 */
	TopicStatsCollect getStats(long memberId);
	
	/**
	 * 统计会员的话题动作,包括发贴数,回贴数
	 * 
	 * @param memberId 会员ID
	 * @return
	 */
	EnumMap<ForumActionEnum, Long> statsMemberTopicAction(long memberId);
	EnumMap<ForumActionEnum, Long> statsMemberAction(long memberId, List<ForumActionEnum> actions);

	/**
	 * 统计会员的话题动作,包括发贴数,回贴数,话题加精数量,置顶数量, 删除的主题数量, 删除的回复数量
	 * 
	 * @param memberId 会员ID
	 * @return
	 */
	EnumMap<ForumActionEnum, Long> groupMemberTopicAction(long memberId);
	
	/**
	 * 统计指定会员的话题动作,包括发贴数,回贴数,话题加精数量,置顶数量, 删除的主题数量, 删除的回复数量
	 * 
	 * @param memberIdList 会员ID列表
	 * @return
	 */
	Map<Long, EnumMap<ForumActionEnum, Long>> groupMemberTopicAction(List<Long> memberIdList);
	
	/**
	 * 统计话题总数(TOPIC_PUBLISH)和回复总数(POSTS_REPLY)
	 * 
	 * @return
	 */
	EnumMap<ForumActionEnum,Long> sumTopicAndPosts();
	
	/**
	 * 会员发贴的排行榜
	 * [names - > nickname]
	 * @param size 显示的数量
	 * @return
	 */
	Map<Long, Map<String, Long>> orderMemberTopices(int size);
}
