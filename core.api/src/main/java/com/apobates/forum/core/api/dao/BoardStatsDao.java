package com.apobates.forum.core.api.dao;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.EnumMap;
import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.BoardStats;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.utils.persistence.DataRepository;
/**
 * 版块统计持层接口
 * 
 * @author xiaofanku
 * @since 20190327
 */
public interface BoardStatsDao extends DataRepository<BoardStats, Long> {
	/**
	 * 版块创建时创建版块统计信息,发/回贴时只需要更新
	 * 
	 * @param boardId   版块ID
	 * @param volumesId 版块组(卷)ID
	 * @return
	 */
	Optional<Boolean> createStats(long boardId, int volumesId);

	/**
	 * [发布话题]更新版块的话题计数,最近的话题信息
	 * 
	 * @param boardId                   版块ID
	 * @param recentTopicTitle          版块最近的话题主题
	 * @param recentTopicId             版块最近的话题ID
	 * @param recentTopicDate           版块最近的话题发布日期
	 * @param recentTopicMemberId       版块最近的话题作者
	 * @param recentTopicMemberNickname 版块最近的话题作者登录帐号
	 * @return
	 */
	Optional<Boolean> updateTopic(long boardId, String recentTopicTitle, long recentTopicId,LocalDateTime recentTopicDate, long recentTopicMemberId, String recentTopicMemberNickname);

	/**
	 * [回复话题]更新版块的回复计数
	 * 
	 * @param boardId 版块ID
	 * @return
	 */
	Optional<Boolean> updatePosts(long boardId);

	/**
	 * [星标版块]更新版块的收藏计数(+1)
	 * 
	 * @param boardId  版块ID
	 * @param memberId 会员ID
	 * @return
	 */
	Optional<Boolean> plusFavorites(long boardId, long memberId);
	
	/**
	 * 更新版块的收藏计数(-1), 方法会删除会员的收藏记录
	 * 
	 * @param boardId  版块ID
	 * @param memberId 会员ID
	 * @return
	 */
	Optional<Boolean> negateFavorites(long boardId, long memberId);
	
	/**
	 * [移动话题]平衡目标版块与原版块的话题数和回复话
	 * 
	 * @param plusBoardId  目标版块ID
	 * @param minusBoardId 原版块ID
	 * @param postsSize    两个版块汲及的回复数量
	 * @return
	 */
	Optional<Boolean> balanceTopicPosts(long plusBoardId, long minusBoardId, long postsSize)throws IllegalStateException;
	
	/**
	 * 只更新版块的话题数,只适用于:站务专区(1号版)
	 * 
	 * @param boardId 版块ID
	 * @return
	 */
	Optional<Boolean> updateTopices(long boardId);
	
	/**
	 * 查看指定版本的统计
	 * 
	 * @param boardIdList  版块ID列表
	 * @return
	 */
	Stream<BoardStats> findAllByBoardId(Collection<Long> boardIdList);
	
	/**
	 * 查看指定版块组下的所有版块统计
	 * 
	 * @param volumesId 版块组(卷)ID
	 * @return
	 */
	Stream<BoardStats> findAllByBoardGroup(int volumesId);
	
	/**
	 * 查看所有非原生版块的统计
	 * 
	 * @return
	 */
	Stream<BoardStats> findAllForNotOriginBoard();
	
	/**
	 * 查看版块的统计信息
	 * 
	 * @param boardId 版块ID
	 * @return
	 */
	Optional<BoardStats> findOneByBoard(long boardId);
	
	/**
	 * 合计贴子和回复总数
	 * 
	 * @return
	 */
	EnumMap<ForumActionEnum,Long> sumTopicAndPosts();
}
