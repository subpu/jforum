package com.apobates.forum.core.api.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.event.elderly.ForumActionEnum;

/**
 * 版主权限的业务接口
 * 
 * @author xiaofanku
 * @since 20190316
 */
public interface BoardModeratorActionIndexService {
	/**
	 * 查看指定版主的操作权限
	 * 
	 * @param moderatorId 版主ID
	 * @return
	 */
	Stream<ForumActionEnum> getAllByBoardId(long moderatorId);

	/**
	 * 为指定的版主授于管理权限
	 * 
	 * @param moderatorId 版主ID
	 * @param actions     操作集合
	 * @return
	 */
	Optional<Boolean> grantModeratorActions(long moderatorId, List<ForumActionEnum> actions)throws IllegalStateException;

	/**
	 * 更新指定版主的管理权限
	 * 
	 * @param moderatorId 版主ID
	 * @param actions     现要的操作集合
	 * @return
	 */
	Optional<Boolean> edit(long moderatorId, List<ForumActionEnum> actions)throws IllegalStateException;
}
