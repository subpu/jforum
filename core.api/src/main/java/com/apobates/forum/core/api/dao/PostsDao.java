package com.apobates.forum.core.api.dao;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.Posts;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;
import com.apobates.forum.utils.persistence.PagingAndSortingRepository;
/**
 * 回复的持久层接口
 * 
 * @author xiaofanku
 * @since 20190326
 */
public interface PostsDao extends PagingAndSortingRepository<Posts, Long> {
	/**
	 * 更新回复的状态值
	 * 
	 * @param id     回复ID
	 * @param status 更新的状态值,true表示可用,false表示删除
	 * @return
	 */
	Optional<Boolean> editStatus(long id, boolean status);
	
	/**
	 * 更新回复的内容, 只能是回复(Posts.reply=true)的记录
	 * 
	 * @param id                 回复ID
	 * @param content            更新的内容
	 * @param editMemberId       更新的会员ID
	 * @param editMemberNickname 更新的会员登录帐号
	 * @return
	 */
	Optional<Boolean> edit(long id, String content, long editMemberId, String editMemberNickname);
	
	/**
	 * 编辑指定话题楼主的内容(1楼), 只能是话题(Posts.reply=false)的内容
	 * 
	 * @param topicId            话题ID
	 * @param content            更新的内容
	 * @param editMemberId       更新的会员ID
	 * @param editMemberNickname 更新的会员登录帐号
	 * @return
	 */
	Optional<Boolean> editTopicContent(long topicId, String content, long editMemberId, String editMemberNickname);
	
	/**
	 * 查看指定话题的回复,包括楼主的话题内容
	 * 
	 * @param topicId  话题ID
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<Posts> findAllByTopic(long topicId, Pageable pageable);

	/**
	 * 查看指定话题的回复,不包括楼主的话题内容
	 * 
	 * @param topicId  话题ID
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<Posts> findAllReplyByTopic(long topicId, Pageable pageable);
	
	/**
	 * 查看话题中指定会员的所有回复,不包括楼主的话题内容
	 * 
	 * @param topicId         话题ID
	 * @param filterMemeberId 回复者ID/过滤的会员ID
	 * @param pageable        分页请求参数
	 * @return
	 */
	Page<Posts> findAllReplyByTopic(long topicId, long filterMemeberId, Pageable pageable);
	
	/**
	 * 
	 * @param topicIdList
	 * @return
	 */
	Stream<Posts> findOneByOneFloor(List<Long> topicIdList);
	
	/**
	 * 自参考日期以后版块最近的回复
	 * 
	 * @param topicId  话题ID
	 * @param prevDate 参考日期
	 * @return
	 */
	Stream<Posts> findAllRecentByTopic(long topicId, LocalDateTime prevDate);
	
	/**
	 * 查看指定话题最近的回复,忽略状态;以entryDateTime倒序
	 * 
	 * @param topicId 话题ID
	 * @param size    显示的数量
	 * @return
	 */
	Stream<Posts> findAllRecentByTopicIgnoreStatus(long topicId, int size);
	
	/**
	 * 查看话题的回复,不包括楼主的回复
	 * 
	 * @param topicId 话题ID
	 * @param size    显示的数量
	 * @return
	 */
	Stream<Posts> findAllByTopic(long topicId, int size);
	
	/**
	 * 查看指定回复的记录,不包括楼主的回复.按楼层的升序排序
	 * 
	 * @param topicId 话题ID
	 * @param page    第几页
	 * @param size    每页显示的数量
	 * @return
	 */
	Stream<Posts> findAllByTopic(long topicId, int page, int size);
	
	/**
	 * 查看指定的回复
	 * 
	 * @param postsIdList 回复Id列表
	 * @return
	 */
	Stream<Posts> findAll(Set<Long> postsIdList);
	
	/**
	 * 查看话题的内容,即楼主的话题内容
	 * 
	 * @param topicId 话题ID
	 * @return
	 */
	Optional<Posts> findOneByOneFloor(long topicId);
	
	/**
	 * 会级联加载版块组(卷), 版块, 话题
	 * 
	 * @param id 回复ID
	 * @return
	 */
	Posts findOneForIndex(long id);
	
	/**
	 * 会级联加载话题
	 * 
	 * @param id 回复ID
	 * @return
	 */
	Posts findOneRelativeTopic(long id);
	
	/**
	 * 统计指定话题的回复数, 1楼不在统计之列.忽略状态值
	 * 
	 * @param topicId 话题ID
	 * @return
	 */
	long countTopicReply(long topicId);
	
	/**
	 * 查看话题最大的楼层
	 * 
	 * @param topicId 话题ID
	 * @return
	 */
	long maxFloor(long topicId);
	
	/**
	 * 统计指定回复者在话题中的回复次数.1楼不在统计之列
	 * 
	 * @param topicId  话题ID
	 * @param memberId 回复者/会员Id
	 * @return
	 */
	long countRepliesSize(long topicId, long memberId);
}
