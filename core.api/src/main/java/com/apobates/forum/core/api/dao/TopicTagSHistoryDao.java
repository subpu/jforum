package com.apobates.forum.core.api.dao;

import java.util.stream.Stream;
import com.apobates.forum.core.entity.TopicTagSHistory;
import com.apobates.forum.utils.persistence.PagingAndSortingRepository;
/**
 * 话题标签搜索历史的持久接口
 * 
 * @author xiaofanku
 * @since 20200115
 */
public interface TopicTagSHistoryDao extends PagingAndSortingRepository<TopicTagSHistory, Long>{
	/**
	 * 找到指定单词的搜索历史
	 * 
	 * @param word 查找的单词,不能是组合形式,例(词A 词B)
	 * @param size 显示的数量
	 * @return
	 */
	Stream<TopicTagSHistory> findAllRelate(String word, int size);
}
