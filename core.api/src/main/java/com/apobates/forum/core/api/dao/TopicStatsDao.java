package com.apobates.forum.core.api.dao;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.TopicStats;
import com.apobates.forum.utils.persistence.DataRepository;

/**
 * 话题统计持层接口
 * 
 * @author xiaofanku
 * @since 20190327
 */
public interface TopicStatsDao extends DataRepository<TopicStats, Long> {
	/**
	 * 创建话题时创建话题统计信息
	 * 
	 * @param topicId   话题ID
	 * @param volumesId 版块组(卷)ID
	 * @param boardId   版块ID
	 * @return
	 */
	Optional<Boolean> createStats(long topicId, int volumesId, long boardId);

	/**
	 * [回复话题]更新话题的回复数,最近回复信息
	 * 
	 * @param topicId                   话题ID
	 * @param recentPostsMemberId       话题最近回复的会员ID
	 * @param recentPostsMemberNickname 话题最近回复的会员登录帐号
	 * @param recentPostsDate           话题最近回复的日期
	 * @param recentPostsId             话题最近回复的ID
	 * @return
	 */
	Optional<Boolean> updatePosts(long topicId, long recentPostsMemberId, String recentPostsMemberNickname, LocalDateTime recentPostsDate, long recentPostsId);

	/**
	 * 更新话题的收藏计数(+1)
	 * 
	 * @param topicId  话题ID
	 * @param memberId 会员ID
	 * @return
	 */
	Optional<Boolean> plusFavorites(long topicId, long memberId);
	
	/**
	 * 更新话题的收藏计数(-1), 方法会删除会员的收藏记录
	 * 
	 * @param topicId  话题ID
	 * @param memberId 会员ID
	 * @return
	 */
	Optional<Boolean> negateFavorites(long topicId, long memberId);
	
	/**
	 * 更新话题的显示计数
	 * 
	 * @param topicId 话题ID
	 * @return
	 */
	Optional<Boolean> updateDisplaies(long topicId);
	
	/**
	 * 更新话题的点赞计数(+1)
	 * 
	 * @param topicId  话题ID
	 * @param memberId 会员ID
	 * @return
	 */
	Optional<Boolean> plusLikes(long topicId, long memberId);
	
	/**
	 * 更新话题的点赞计数(-1), 方法会删除会员的点赞记录
	 * 
	 * @param topicId  话题ID
	 * @param memberId 会员ID
	 * @return
	 */
	Optional<Boolean> negateLikes(long topicId, long memberId);
	
	/**
	 * 
	 * 
	 * @param topicIdList
	 * @return
	 */
	Stream<TopicStats> findAllByTopicId(Collection<Long> topicIdList);
	
	/**
	 * 按回复日期查看最近回复的话题统计
	 * 
	 * @param size 显示数量
	 * @return
	 */
	Stream<TopicStats> findAllByReplyDate(int size);
	
	/**
	 * 查看话题的统计信息
	 * 
	 * @param topicId 话题ID
	 * @return
	 */
	Optional<TopicStats> findOneByTopic(long topicId);
}
