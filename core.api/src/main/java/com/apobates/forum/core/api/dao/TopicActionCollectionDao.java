package com.apobates.forum.core.api.dao;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.TopicActionCollection;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;
import com.apobates.forum.utils.persistence.PagingAndSortingRepository;

/**
 * 话题动作集合的持久层接口
 * 
 * @author xiaofanku
 * @since 20190328
 */
public interface TopicActionCollectionDao extends PagingAndSortingRepository<TopicActionCollection, Long> {
	/**
	 * 指定会员的话题动作
	 * 
	 * @param memberId 会员ID
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<TopicActionCollection> findByMember(long memberId, Pageable pageable);
	long findByMemberCount(long memberId);
	
	/**
	 * 指定话题的动作
	 * 
	 * @param topicId  话题ID
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<TopicActionCollection> findByTopic(long topicId, Pageable pageable);
	
	/**
	 * 查看指定操作的话题动作
	 * 
	 * @param action   统计的操作
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<TopicActionCollection> findAllByAction(ForumActionEnum action, Pageable pageable);
	
	/**
	 * 查看指定会员的操作动作
	 * 
	 * @param action   操作枚举
	 * @param memberId 会员ID
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<TopicActionCollection> findAllByActionAndMember(ForumActionEnum action, long memberId, Pageable pageable);
	long findAllByActionAndMemberCount(ForumActionEnum action, long memberId);
	
	/**
	 * 查看指定会员的操作动作
	 * 
	 * @param actions  操作枚举列表
	 * @param memberId 会员ID
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<TopicActionCollection> findAllByActionAndMember(List<ForumActionEnum> actions, long memberId, Pageable pageable);
	
	/**
	 * 查看指定日期的操作记录
	 * 
	 * @param start    开始日期
	 * @param finish   结束日期
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<TopicActionCollection> findAll(LocalDateTime start, LocalDateTime finish, Pageable pageable);
	
	/**
	 * 指定会员最近的话题动作
	 * 
	 * @param memberId 会员ID
	 * @param size     显示的数量
	 * @return
	 */
	Stream<TopicActionCollection> findRecentByMember(long memberId, int size);
	
	/**
	 * 查看在话题中会员的操作记录(针对话题自身的操作)
	 * 
	 * @param memberId 会员ID
	 * @param topicId  话题ID
	 * @param action   操作枚举
	 * @return
	 */
	Stream<TopicActionCollection> findAllByMemberAction(long memberId, long topicId, ForumActionEnum action);
	
	/**
	 * 查看不同的操作枚举
	 * 
	 * @return
	 */
	Stream<ForumActionEnum> findAllAction();
	
	/**
	 * 批量保存
	 * 
	 * @param entities
	 * @return
	 */
	int batchSave(Collection<TopicActionCollection> entities);

	/**
	 * 查看指定会员是否有话题的收藏记录
	 * 
	 * @param memberId 会员ID
	 * @param topicId  话题ID
	 * @return
	 */
	Optional<TopicActionCollection> findOneFavoriteRecord(long memberId, long topicId);
	
	/**
	 * 查看指定会员是否有话题的点赞记录
	 * 
	 * @param memberId 会员ID
	 * @param topicId  话题ID
	 * @return
	 */
	Optional<TopicActionCollection> findOneLikeRecord(long memberId, long topicId);
	
	/**
	 * 统计会员的话题动作
	 * 
	 * @param memberId     会员ID
	 * @param allowActions 统计的操作列表
	 * @return
	 */
	EnumMap<ForumActionEnum, Long> groupMemberAction(long memberId, Collection<ForumActionEnum> allowActions);
	
	/**
	 * 统计指定的动作合计
	 * 
	 * @param allowActions 统计的操作列表
	 * @return
	 */
	EnumMap<ForumActionEnum, Long> groupAction(List<ForumActionEnum> allowActions);
	
	/**
	 * 统计指定会员的话题动作
	 * 
	 * @param memberIdList  会员ID列表
	 * @param allowActions  统计的操作列表
	 * @return Key=会员Id value=Map{Key=操作, Value=汇总数}
	 */
	Map<Long, EnumMap<ForumActionEnum, Long>> groupMemberAction(List<Long> memberIdList, Collection<ForumActionEnum> allowActions);
	
	/**
	 * 统计指定动作的会员统计信息
	 * [names - > nickname]
	 * @param action 统计的操作
	 * @param size   显示的数量
	 * @return Key=会员Id value=Map{Key=会员登录帐号, Value=汇总数}
	 */
	Map<Long, Map<String, Long>> groupMemberAction(ForumActionEnum action, int size);
	
	/**
	 * 统计在话题中会员的操作次数(针对话题自身的操作)
	 * 
	 * @param memberId 会员ID
	 * @param topicId  话题ID
	 * @param action   操作枚举
	 * @return
	 */
	long countMemberAction(long memberId, long topicId, ForumActionEnum action);
	
	/**
	 * 是否可以点赞, 可以返回true
	 * 
	 * @param topicId  话题ID
	 * @param memberId 会员ID
	 * @return
	 */
	Optional<Boolean> isLiked(long topicId, long memberId)throws IllegalStateException;
	
	/**
	 * 是否可以收藏, 可以返回true
	 * 
	 * @param topicId  话题ID
	 * @param memberId 会员ID
	 * @return
	 */
	Optional<Boolean> isFavorited(long topicId, long memberId)throws IllegalStateException;
}
