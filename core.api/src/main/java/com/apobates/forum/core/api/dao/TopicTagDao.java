package com.apobates.forum.core.api.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.TopicTag;
import com.apobates.forum.utils.persistence.PagingAndSortingRepository;
/**
 * 话题标签持久层接口
 * 
 * @author xiaofanku
 * @since 20190401
 */
public interface TopicTagDao extends PagingAndSortingRepository<TopicTag, Long> {
	/**
	 * 删除指定标签
	 * 
	 * @param id 标签ID
	 * @return
	 */
	Optional<Boolean> remove(long id);
	
	/**
	 * 删除指定标签
	 * 
	 * @param id      标签ID
	 * @param topicId 话题ID
	 * @return
	 */
	Optional<Boolean> remove(long id, long topicId);
	
	/**
	 * 删除指定话题的所有标签
	 * 
	 * @param topicId 话题ID
	 * @return
	 */
	int removeAll(long topicId);
	
	/**
	 * 批量保存话题标签
	 * 
	 * @param topicId 话题ID
	 * @param tages   话题标签实例列表
	 * @return
	 */
	int batchSave(long topicId, Collection<TopicTag> tages);
	
	/**
	 * 批量保存话题标签
	 * 
	 * @param topicId 话题ID
	 * @param tages   Key=标签,Value=词频
	 * @return
	 */
	int batchSave(long topicId, Map<String,Integer> tages);
	
	/**
	 * 查看指定话题中的标签
	 * 
	 * @param topicId 话题ID
	 * @return
	 */
	List<TopicTag> findAllByTopic(long topicId);
	
	/**
	 * 与指定话题标签相关的标签
	 * 
	 * @param topicId 话题ID
	 * @param size    结果集的数量
	 * @return
	 */
	Stream<TopicTag> findAllRelateTopic(long topicId, int size);
	
	/**
	 * 与指定话题标签相关的标签,结果集的数量等于tages.size x 10
	 * 
	 * @param topicId 话题ID
	 * @param tages   话题ID相关的标签
	 * @return
	 */
	Stream<TopicTag> findAllRelateTopic(long topicId, Collection<String> tages);
	
	/**
	 * 分组统计出现频率最高的标签名称(TopicTag.names)
	 * 
	 * @param size 从高到低选取的数量
	 * @return
	 */
	Map<String,Long> groupTagForNames(int size);
}
