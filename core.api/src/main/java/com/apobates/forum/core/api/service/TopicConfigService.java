package com.apobates.forum.core.api.service;

import java.util.Optional;
import com.apobates.forum.core.entity.TopicConfig;
/**
 * 话题配置的业务接口
 * 
 * @author xiaofanku
 * @since 20190326
 */
public interface TopicConfigService {
	/**
	 * 根据话题查看话题配置
	 * 
	 * @param topicId 话题ID
	 * @return
	 */
	Optional<TopicConfig> getByTopicId(long topicId);

	/**
	 * 创建话题配置
	 * 
	 * @param topicId 话题ID
	 * @param config  话题配置实例
	 * @return
	 */
	Optional<Boolean> create(long topicId, TopicConfig config)throws IllegalStateException;
}
