package com.apobates.forum.core.api.dao;

import java.util.stream.Stream;
import com.apobates.forum.core.entity.BoardModeratorRoleHistory;
import com.apobates.forum.utils.persistence.DataRepository;

/**
 * 版主的角色变化历史持久接口
 * @author xiaofanku
 * @since 20190627
 */
public interface BoardModeratorRoleHistoryDao  extends DataRepository<BoardModeratorRoleHistory, Long> {
	/**
	 * 返回会员可用的角色变化历史记录
	 * 
	 * @param memberId 会员ID
	 * @return
	 */
	Stream<BoardModeratorRoleHistory> findAllByMember(long memberId);
}
