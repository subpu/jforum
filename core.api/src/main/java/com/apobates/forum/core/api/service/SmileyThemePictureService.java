package com.apobates.forum.core.api.service;

import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.SmileyTheme;
import com.apobates.forum.core.entity.SmileyThemePicture;

/**
 * 表情图片业务接口
 * 
 * @author xiaofanku
 * @since 20190529
 */
public interface SmileyThemePictureService {
	/**
	 * 查看所以表情图片
	 * 
	 * @return
	 */
	Stream<SmileyThemePicture> get();

	/**
	 * 查看指定表情风格的表情图片
	 * 
	 * @param id
	 *            表情风格ID
	 * @return
	 */
	Stream<SmileyThemePicture> getAllByTheme(int smileyThemeId);

	/**
	 * 查看指定表情风格的表情图片
	 * 
	 * @param smileyThemeDirectNames
	 *            表情风格目录名
	 * @return
	 */
	Stream<SmileyThemePicture> getAllByTheme(String smileyThemeDirectNames);
	
	/**
	 * 查看指定的表情图片 及联加载表情风格
	 * 
	 * @param id
	 *            表情图片ID
	 * @return
	 */
	Optional<SmileyThemePicture> get(long id);

	/**
	 * 创建表情图片
	 * 
	 * @param fileNames
	 *            文件名
	 * @param description
	 *            图片描述
	 * @param ranking
	 *            显示顺序
	 * @param status
	 *            状态
	 * @param smileyTheme
	 *            表情风格
	 * @return
	 */
	Optional<SmileyThemePicture> create(String fileNames, String description, int ranking, boolean status, SmileyTheme smileyTheme)throws IllegalStateException;

	/**
	 * 更新指定的表情图片
	 * 
	 * @param id
	 *            表情图片ID
	 * @param updateSmileyThemePicture
	 *            更新的表情图片实例
	 * @return
	 */
	Optional<Boolean> edit(long id, SmileyThemePicture updateSmileyThemePicture)throws IllegalStateException;

	/**
	 * 如果以前未定义,将创建语义;反之更新语义
	 * 
	 * @param fileName
	 *            表情图片文件名
	 * @param fileSemantic
	 *            表情图片代表的语义
	 * @param themeId
	 *            表情风格Id
	 * @return
	 */
	Optional<Boolean> modify(String fileName, String fileSemantic, int themeId)throws IllegalStateException;
}
