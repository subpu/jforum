package com.apobates.forum.core.api.service;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import com.apobates.forum.core.api.ImageIOMeta;
import com.apobates.forum.core.entity.Album;
import com.apobates.forum.core.entity.Topic;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;
/**
 * 像册业务接口
 * 
 * @author xiaofanku
 * @since 20190609
 */
public interface AlbumService {
	/**
	 * 查看所有像册
	 * 
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<Album> getAll(Pageable pageable);
	
	/**
	 * 查看指定版块组(卷)的像册
	 * 
	 * @param volumesId 版块组(卷)ID
	 * @param pageable  分页请求参数
	 * @return
	 */
	Page<Album> getAll(int volumesId, Pageable pageable);
	
	/**
	 * 查看指定版块的像册
	 * 
	 * @param boardId  版块ID
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<Album> getAll(long boardId, Pageable pageable);
	
	/**
	 * 查看最近可用的像册
	 * 
	 * @param size 显示的数量
	 * @return
	 */
	Stream<Album> getRecent(int size);
	
	/**
	 * 查看指定话题的像册
	 * 
	 * @param topicIdSet     话题ID集合
	 * @param imageIO        图片存储信息
	 * @param scale          缩放比例. 支持: 宽x高|auto
	 * @param defaultPicture 解码失败时显示的默认图
	 * @return
	 */
	Stream<Album> getAll(Set<Long> topicIdSet, ImageIOMeta imageIO, String scale, String defaultPicture);
	
	/**
	 * 查看指定话题的像册
	 * 
	 * @param topicId 话题ID
	 * @return
	 */
	Optional<Album> getByTopic(long topicId);
	
	/**
	 * 查看指定的像册
	 * 
	 * @param id 像册ID
	 * @return
	 */
	Optional<Album> get(long id);
	
	/**
	 * 设置某图片为像册的封面图片
	 * 
	 * @param id        像册的ID
	 * @param pictureId 封面图片ID
	 * @return
	 */
	Optional<Boolean> editCover(long id, long pictureId)throws IllegalStateException;
	
	/**
	 * 创建一个只有封面的像册(当没有图片的话题增加图片时)
	 * 
	 * @param encodeImageAddr 图片地址(编码后的格式)
	 * @param caption         图片的文字说明
	 * @param status          图片的状态
	 * @param topic           话题对象实列(需要的属性:id,title,volumesId,boardId,memberId,memberNames)
	 * @return
	 */
	long create(String encodeImageAddr, String caption, boolean status, Topic topic)throws IllegalStateException;
}
