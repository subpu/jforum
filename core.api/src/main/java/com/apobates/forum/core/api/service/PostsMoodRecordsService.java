package com.apobates.forum.core.api.service;

import java.util.List;
import java.util.Set;
import java.util.stream.Stream;
import com.apobates.forum.core.MoodCollectResult;
import com.apobates.forum.core.entity.PostsMoodRecords;
import com.apobates.forum.event.elderly.ActionEventCulpritor;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;

public interface PostsMoodRecordsService {
	/**
	 * 查看指定回复的喜好记录
	 * 
	 * @param postsId  回复ID
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<PostsMoodRecords> getAllByPosts(long postsId, Pageable pageable);
	
	/**
	 * 查看指定回复的喜好记录
	 * 
	 * @param postsId 回复ID
	 * @param liked   是否为赞,true,false为讨厌
	 * @return
	 */
	Stream<PostsMoodRecords> getByPosts(long postsId, boolean liked);
	
	/**
	 * 查看指定话题的回复喜好汇总记录
	 * 
	 * @param topicId  话题ID
	 * @param page     最几页
	 * @param pageSize 每页显示的数量
	 * @return
	 */
	List<MoodCollectResult> getByTopic(long topicId, int page, int pageSize);
	
	/**
	 * 查看指定的回复喜好汇总记录
	 * 
	 * @param topicId    话题ID
	 * @param postsIdSet 回复ID集合
	 * @return
	 */
	List<MoodCollectResult> getByPosts(long topicId, Set<Long> postsIdSet);
	
	/**
	 * 创建喜好记录.若会员已对回复的喜好记录存在方法会删除并返回成功
	 * 
	 * @param id   回复ID
	 * @param topicId   话题ID
	 * @param like      是否为赞,true,false为讨厌
	 * @param culpritor 操作的肇事信息
	 * @return
	 */
	long toggleMoodRecord(long id, long topicId, boolean like, ActionEventCulpritor culpritor)throws IllegalStateException;
}
