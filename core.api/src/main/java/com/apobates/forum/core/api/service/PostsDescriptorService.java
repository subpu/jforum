package com.apobates.forum.core.api.service;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.PostsDescriptor;

public interface PostsDescriptorService {
	/**
	 * 查看指定回复的附加信息
	 * 
	 * @param postsIdSet 回复ID集合
	 * @return
	 */
	Stream<PostsDescriptor> getAll(Set<Long> postsIdSet);
	
	/**
	 * 查看指定回复的附加信息
	 * 
	 * @param postsId 回复ID
	 * @return
	 */
	Optional<PostsDescriptor> get(long postsId);
}
