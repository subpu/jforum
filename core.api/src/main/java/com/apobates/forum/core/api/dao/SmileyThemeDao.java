package com.apobates.forum.core.api.dao;

import java.util.Optional;
import com.apobates.forum.core.entity.SmileyTheme;
import com.apobates.forum.utils.persistence.DataRepository;

/**
 * 表情风格持久接口
 * @author xiaofanku
 * @since 20190529
 */
public interface SmileyThemeDao extends DataRepository<SmileyTheme, Integer>{
	/**
	 * 根据表情风格目录名查看
	 * @param directName 风格目录名
	 * @return
	 */
	Optional<SmileyTheme> findOneByDirectName(String directName);
}
