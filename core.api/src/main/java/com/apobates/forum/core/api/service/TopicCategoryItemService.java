package com.apobates.forum.core.api.service;

import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.TopicCategoryItem;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;

/**
 * 话题子分类的业务层接口
 * 
 * @author xiaofanku
 * @since 20190331
 */
public interface TopicCategoryItemService {
	/**
	 * 所有话题子分类
	 * 
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<TopicCategoryItem> getAll(Pageable pageable);
	
	/**
	 * 查看指定话题分类的子分类
	 * 
	 * @param topicCategoryId 话题分类ID
	 * @return
	 */
	Stream<TopicCategoryItem> getAll(int topicCategoryId);
	
	/**
	 * 指定的话题子分类
	 * 
	 * @param id 话题子分类ID
	 * @return
	 */
	Optional<TopicCategoryItem> get(long id);
}
