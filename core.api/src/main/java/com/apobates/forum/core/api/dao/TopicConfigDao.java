package com.apobates.forum.core.api.dao;

import java.util.Optional;
import com.apobates.forum.core.entity.TopicConfig;
import com.apobates.forum.utils.persistence.DataRepository;

/**
 * 话题配置的持久层接口
 * 
 * @author xiaofanku
 * @since 20190326
 */
public interface TopicConfigDao extends DataRepository<TopicConfig, Long> {
	/**
	 * 查看指定话题的配置
	 * 
	 * @param topicId 话题ID
	 * @return
	 */
	Optional<TopicConfig> findOneByTopic(long topicId);
}
