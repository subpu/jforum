package com.apobates.forum.core.api.dao;

import java.util.Optional;
import com.apobates.forum.core.entity.TopicCarousel;
import com.apobates.forum.utils.persistence.DataRepository;
/**
 * 轮播图的持久接口
 * 
 * @author xiaofanku
 * @since 20191012
 */
public interface TopicCarouselDao extends DataRepository<TopicCarousel, Integer>{
	/**
	 * 查看指定轮播图,状态要求可用
	 * 
	 * @param title 轮播图标题或标识
	 * @return
	 */
	Optional<TopicCarousel> findOne(String title);
	
	/**
	 * 更新轮播图的状态
	 * 
	 * @param id 轮播图ID
	 * @return
	 */
	Optional<Boolean> editStatus(int id, boolean status);
}
