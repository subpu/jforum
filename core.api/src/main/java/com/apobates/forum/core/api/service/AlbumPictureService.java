package com.apobates.forum.core.api.service;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Stream;
import com.apobates.forum.core.api.ImageIOMeta;
import com.apobates.forum.core.entity.AlbumPicture;
/**
 * 像册图片业务接口
 * 
 * @author xiaofanku
 * @since 20190609
 */
public interface AlbumPictureService {
	/**
	 * 查看指定像册下的图片
	 * 
	 * @param albumId 像册ID
	 * @return
	 */
	Stream<AlbumPicture> getAll(long albumId);
	
	/**
	 * 查看指定像册的图片
	 * 
	 * @param albumIdSet     像册ID集合
	 * @param imageIO        图片存储信息
	 * @param scale          缩放比例. 支持: 宽x高|auto
	 * @param defaultPicture 解码失败时显示的默认图
	 * @return 
	 */
	Stream<AlbumPicture> getAll(Set<Long> albumIdSet, ImageIOMeta imageIO, String scale, String defaultPicture);
	
	/**
	 * 查看指定的图片
	 * 
	 * @param id 图片ID
	 * @return
	 */
	Optional<AlbumPicture> get(long id);
	
	/**
	 * 为指定的像册添加新图片
	 * 
	 * @param albumId         像册的ID
	 * @param encodeImageAddr 图片地址(编码后的格式)
	 * @param caption         图片的文字说明
	 * @param ranking         图片的显示顺序
	 * @param status          状态,true可用,false禁用
	 * @return
	 */
	Optional<AlbumPicture> create(long albumId, String encodeImageAddr, String caption, int ranking, boolean status)throws IllegalStateException;
	
	/**
	 * 删除指定的图片
	 * 
	 * @param id 图片ID
	 * @return
	 */
	int delete(long id);
	
	/**
	 * 删除指定的图片
	 * 
	 * @param idSet 图片ID集合
	 * @return
	 */
	int delete(Set<Long> idSet);
	
	/**
	 * 为指定的像册添加新图片
	 * 
	 * @param albumId         像册的ID
	 * @param encodeImageAddr 图片地址(编码后的格式)
	 * @param caption         图片的文字说明
	 * @param ranking         图片的显示顺序
	 * @param status          状态,true可用,false禁用
	 * @param cover           是否是像册的封面,true是,false否
	 * @return
	 */
	long create(long albumId, String encodeImageAddr, String caption, int ranking, boolean status, boolean cover)throws IllegalStateException;
	
	/**
	 * 查看指定像册的图片
	 * 
	 * @param albumIdSet     像册ID集合
	 * @param imageIO        图片存储信息
	 * @param scale          缩放比例. 支持: 宽x高|auto
	 * @param defaultPicture 解码失败时显示的默认图
	 * @param showSize       每个像册显示的图片数量
	 * @return key = 像册ID, value = 图片
	 */
	Map<Long, TreeSet<AlbumPicture>> getAll(Set<Long> albumIdSet, ImageIOMeta imageIO, String scale, String defaultPicture, int showSize);
}
