package com.apobates.forum.core.api.dao;

import java.util.Optional;
import com.apobates.forum.core.entity.BoardConfig;
import com.apobates.forum.utils.persistence.DataRepository;

/**
 * 版块配置的持久层接口
 * 
 * @author xiaofanku
 * @since 20190316
 */
public interface BoardConfigDao extends DataRepository<BoardConfig, Long> {
	/**
	 * 根据版块查看版块配置
	 * 
	 * @param boardId 版块ID
	 * @return
	 */
	Optional<BoardConfig> findOneByBoard(long boardId);
}
