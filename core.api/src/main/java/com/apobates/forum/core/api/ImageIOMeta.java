package com.apobates.forum.core.api;
/**
 * 话题和回复 需要的图片IO信息
 * 
 * @author xiaofanku
 * @since 20190922
 */
public interface ImageIOMeta {
	/**
	 * 图片存储的站点URL
	 * @return
	 */
	String getImageBucketDomain();
	/**
	 * 图片存储的站点中表情的目录名称
	 * @return
	 */
	String getSmileyDirectName();
	/**
	 * 图片存储的站点中上传的目录名称
	 * @return
	 */
	String getUploadImageDirectName();
}
