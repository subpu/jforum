package com.apobates.forum.core.api.dao;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.PostsMoodRecords;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;
import com.apobates.forum.utils.persistence.PagingAndSortingRepository;

public interface PostsMoodRecordsDao extends PagingAndSortingRepository<PostsMoodRecords, Long>{
	/**
	 * 查看指定回复的喜好记录
	 * 
	 * @param postsId  回复ID
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<PostsMoodRecords> findAllByPosts(long postsId, Pageable pageable);
	
	/**
	 * 查看指定回复点赞的用户
	 * 
	 * @param postsId 回复ID
	 * @param liked   是否为赞,true,false为讨厌
	 * @return
	 */
	Stream<PostsMoodRecords> findAllByMood(long postsId, boolean liked);
	
	/**
	 * 删除指定会员对回复的喜欢操作记录
	 * 
	 * @param postsId  回复ID
	 * @param memberId 操作的会员
	 * @return
	 */
	Optional<Boolean> removeMoodRecord(long postsId, long memberId);
	
	/**
	 * 统计指定回复的点赞[讨厌]总数
	 * 
	 * @param postsId 回复ID
	 * @return
	 */
	Map<Boolean,Long> collectMembers(long postsId);
	
	/**
	 * 统计指定回复的点赞[讨厌]总数
	 * 
	 * @param postsIdSet 回复ID集合
	 * @return
	 */
	Map<Long, Map<Boolean,Long>> collectMembers(Collection<Long> postsIdSet);
}
