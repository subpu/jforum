package com.apobates.forum.core.api;

import java.io.Serializable;
import com.apobates.forum.core.entity.Topic;
/**
 * 话题相关记录结果封装
 * 
 * @author xiaofanku
 * @since 20200112
 */
public class TagRelateTopic implements Serializable, Comparable<TagRelateTopic>{
	private static final long serialVersionUID = 3101501005271044195L;
	private final Topic topic;
	//显示的顺序,小的先显示
	private final int ranking;
	//相似度
	private final double similarity;
	
	public TagRelateTopic(Topic topic, int ranking, double similarity) {
		super();
		this.topic = topic;
		this.ranking = ranking;
		this.similarity = similarity;
	}

	public Topic getTopic() {
		return topic;
	}

	public int getRanking() {
		return ranking;
	}

	public double getSimilarity() {
		return similarity;
	}

	@Override
	public int compareTo(TagRelateTopic o) {
		// TODO Auto-generated method stub
		return ranking - o.getRanking();
	}
}
