package com.apobates.forum.core.api.dao;

import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.TopicCategory;
import com.apobates.forum.utils.persistence.PagingAndSortingRepository;
/**
 * 话题类型的持久层接口
 * @author xiaofanku
 * @since 20190330
 */
public interface TopicCategoryDao extends PagingAndSortingRepository<TopicCategory,Integer>{

	/**
	 * 所有可用的话题类型
	 * 
	 * @return
	 */
	Stream<TopicCategory> findUsed();
	
	/**
	 * 根据分类属性值查看话题类型
	 * 
	 * @param value 分类属性值
	 * @return
	 */
	Optional<TopicCategory> findOneByValue(String value);
}
