package com.apobates.forum.core.api.service;

import java.util.stream.Stream;
import com.apobates.forum.core.entity.BoardModeratorRoleHistory;

/**
 * 版主的角色变更历史业务接口
 * @author xiaofanku
 * @since 20190627
 */
public interface BoardModeratorRoleHistoryService {
	/**
	 * 管理员的角色变更历史
	 * 
	 * @param memberId 管理员Id
	 * @return
	 */
	Stream<BoardModeratorRoleHistory> getByMember(long memberId);
}
