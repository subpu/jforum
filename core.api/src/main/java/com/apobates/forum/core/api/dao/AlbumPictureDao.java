package com.apobates.forum.core.api.dao;

import java.util.Collection;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.AlbumPicture;
import com.apobates.forum.utils.persistence.DataRepository;
/**
 * 像册图片持久接口
 * 
 * @author xiaofanku
 * @since 20190609
 */
public interface AlbumPictureDao extends DataRepository<AlbumPicture, Long>{
	/**
	 * 查看指定像册中的图片
	 * 
	 * @param albumId 像册ID
	 * @return
	 */
	Stream<AlbumPicture> findAllByAlbum(long albumId);
	
	/**
	 * 查看指定像册的图片
	 * 
	 * @param albumIdSet 像册ID集合
	 * @return
	 */
	Stream<AlbumPicture> findAllByAlbum(Collection<Long> albumIdSet);
	
	/**
	 * 删除指定的图片
	 * 
	 * @param idSet 图片ID集合
	 * @return
	 */
	int delete(Collection<Long> idSet);
}
