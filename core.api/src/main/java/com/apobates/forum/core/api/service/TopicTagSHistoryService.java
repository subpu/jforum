package com.apobates.forum.core.api.service;

import java.util.stream.Stream;
import com.apobates.forum.core.entity.TopicTagSHistory;
import com.apobates.forum.event.elderly.ActionEventCulpritor;
/**
 * 话题标签搜索历史的业务接口
 * 
 * @author xiaofanku
 * @since 20200115
 */
public interface TopicTagSHistoryService {
	/**
	 * 保存一次搜索记录
	 * 
	 * @param word      搜索的词组,可以是组合形式,例(词A 词B)
	 * @param affect    结果集的数量
	 * @param culpritor 操作的肇事信息
	 * @return
	 */
	long create(String word, long affect, ActionEventCulpritor culpritor);
	
	/**
	 * 找到指定单词的搜索历史
	 * 
	 * @param word 查找的单词,不能是组合形式,例(词A 词B)
	 * @param size 显示的数量
	 * @return
	 */
	Stream<TopicTagSHistory> getAllForRelate(String word, int size);
}
