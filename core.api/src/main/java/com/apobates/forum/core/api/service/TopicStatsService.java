package com.apobates.forum.core.api.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.TopicStats;

/**
 * 话题统计
 * 
 * @author xiaofanku
 * @since 20190327
 */
public interface TopicStatsService {
	/**
	 * 查看指定话题的统计
	 * 
	 * @param topicIdList 话题ID列表
	 * @return
	 */
	Stream<TopicStats> getByTopicIdList(List<Long> topicIdList);
	
	/**
	 * 查看话题的统计
	 * 
	 * @param topicId 话题ID
	 * @return
	 */
	Optional<TopicStats> getByTopic(long topicId);
}
