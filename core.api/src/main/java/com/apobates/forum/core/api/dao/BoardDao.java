package com.apobates.forum.core.api.dao;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.Board;
import com.apobates.forum.core.entity.ForumEntityStatusEnum;
import com.apobates.forum.utils.persistence.DataRepository;

/**
 * 版块的持久层接口
 * 
 * @author xiaofanku
 * @since 20190316
 */
public interface BoardDao extends DataRepository<Board, Long> {
	/**
	 * 更新版块的状态
	 * 
	 * @param id     版块ID
	 * @param status 版块新的状态
	 * @return
	 */
	Optional<Boolean> edit(long id, ForumEntityStatusEnum status);
	
	/**
	 * 查看指定版块组中的所有版块
	 * 
	 * @param boardGroupId 版块组(卷)ID
	 * @return
	 */
	Stream<Board> findAllByBoardGroup(int boardGroupId);
	
	/**
	 * 查看指定版块组中可用的版块
	 * 
	 * @param boardGroupId 版块组(卷)ID
	 * @return
	 */
	Stream<Board> findUsed(int boardGroupId);
	
	/**
	 * 所有可以显示的
	 * 
	 * @return
	 */
	Stream<Board> findUsed();
	
	/**
	 * 查询指定的版块集合
	 * 
	 * @param ids 版块ID集合
	 * @return
	 */
	Stream<Board> findAllById(Collection<Long> ids);
	
	/**
	 * 查看指定的子栏目
	 * @since 20200427
	 * @param ids 子栏目ID集合
	 * @return
	 */
	Stream<Board> findAllTermById(Collection<Long> ids);
	
	/**
	 * 查看栏目下的所有子栏目(版块组下的版块)
	 * @since 20200427
	 * @param sectionId 栏目ID
	 * @return
	 */
	List<Board> findAllTermByOriginId(int sectionId);
	
	/**
	 * 查看所有子栏目(原生的版块)
	 * @since 20200427
	 * @return
	 */
	List<Board> findAllTerm();
	
	/**
	 * 查看指定的子栏目(原生的版块)
	 * @since 20200427
	 * @param directoryNames 子栏目的目录名称
	 * @return
	 */
	Optional<Board> findOneTermByDirectoryNames(String directoryNames);
	
	/**
	 * 查看指定的子栏目(原生的版块)
	 * @since 20200427
	 * @param termId 子栏目ID
	 * @return
	 */
	Optional<Board> findOneTermById(long termId);
	
	/**
	 * 查看指定的子栏目(原生版块),同时加载所属的栏目(原生版块组)
	 * @since 20200427
	 * @param directoryNames 子栏目的目录名称
	 * @return
	 */
	Board findOneTermAssSection(String directoryNames);
	
	/**
	 * 会级联加载版块组(卷)
	 * 
	 * @param id 版块ID
	 * @return
	 */
	Board findOneForIndex(long id);
	
	/**
	 * 指定的子栏目名称是否存在
	 * @since 20200427
	 * @param directoryNames 子栏目的目录名称
	 * @return 返回的值在存在时是对象ID, 不存在返回0L
	 */
	long existsTermDirectoryNames(String directoryNames);
	
	/**
	 * 指定的子栏目名称是否存在
	 * @since 20200427
	 * @param directoryNames 子栏目的目录名称
	 * @param termId         排除的子栏目ID
	 * @return 返回的值表示统计的数量
	 */
	long existsTermDirectoryNames(String directoryNames, long termId);
}
