package com.apobates.forum.core.api.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.BoardTopicCategoryIndex;
import com.apobates.forum.core.entity.TopicCategory;

public interface BoardTopicCategoryIndexService {
	/**
	 * 删除指定版块关联的话题类型
	 * 
	 * @param boardId     版块ID
	 * @param categoryIds 话题类型ID集合
	 * @return
	 */
	int deleteForTopic(long boardId, List<Integer> categoryIds);
	
	/**
	 * 为指定的版块或版块组(卷)关联多个话题类型
	 * 
	 * @param boardGroundId 版块组(卷)ID
	 * @param boardId       版块ID
	 * @param categoryIds   话题类型ID集合
	 * @return
	 */
	int create(int boardGroundId, long boardId, List<Integer> categoryIds);
	
	/**
	 * 查看指定版块关联的话题类型列表.用于后台查看版块或所在的版块组关联了哪些话题类型<br/>
	 * 注意版块必须与版块组(卷)是包含关系
	 * 
	 * @param boardGroundId 版块组(卷)ID
	 * @param boardId       版块ID
	 * @return
	 */
	Stream<TopicCategory> getAllByBoard(int boardGroupId, long boardId);
	
	/**
	 * 查看版块话题可用的话题类型列表.<br/>
	 * 注意版块必须与版块组(卷)是包含关系
	 * 
	 * @param boardGroupId 版块组(卷)ID
	 * @param boardId      版块ID
	 * @return 返回值永远不会为空<br/>
	 *         若getAllByBoard返回Null或空, 此方法返回所有可用的话题类型<br/>
	 *         若getAllByBoard返回值不为空, 此方法返回getAllByBoard返回值
	 */
	Stream<TopicCategory> getAllByBoardTopicCategories(int boardGroupId, long boardId);
	
	/**
	 * 查看举报话题类型的关联记录
	 * 
	 * @return
	 */
	Optional<BoardTopicCategoryIndex> getReportRelativeRecord();
	
	/**
	 * 查看意见反馈话题类型的关联记录
	 * 
	 * @return
	 */
	Optional<BoardTopicCategoryIndex> getFeedbackRelativeRecord();
	
	/**
	 * 查看举报话题类型和意见反馈话题类型的关联记录
	 * 
	 * @return 返回关联的话题类型ID
	 */
	Set<Integer> getAllRelativeRecords();
}
