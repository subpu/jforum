package com.apobates.forum.core.api.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.BoardGroup;

/**
 * 版块组(卷)的业务接口
 * 
 * @author xiaofanku
 * @since 20190316
 */
public interface BoardGroupService {
	/**
	 * 更新指定的版块组(卷)
	 * 
	 * @param id           版块组(卷)ID
	 * @param updateEntity 
	 *                     更新的版块组(卷)属性实例<br/>
	 *                     若许更新版块组(卷)的图标需要对其进行编码<br/>
	 *                     不更新图标需要将其设置为null<br/>
	 * @return
	 */
	Optional<Boolean> edit(int id, BoardGroup updateEntity)throws IllegalStateException;
	
	/**
	 * 编辑栏目
	 * @since 20200427
	 * @param id             栏目的ID
	 * @param title          栏目的名称
	 * @param description    栏目的描述
	 * @param directoryNames 目录名称,需要唯一
	 * @param status         栏目的状态
	 * @return
	 */
	Optional<Boolean> editSection(int id, String title, String description, String directoryNames, boolean status)throws IllegalStateException;
	
	/**
	 * 创建新的栏目(原生的版块组)
	 * @since 20200427
	 * @param title          栏目的名称
	 * @param description    栏目的描述
	 * @param directoryNames 目录名称,需要唯一
	 * @param status         栏目的状态
	 * @return
	 */
	int buildOrigin(String title, String description, String directoryNames, boolean status)throws IllegalStateException;
	
	/**
	 * 所有的版块组(卷).包括所有状态的, 前端不要使用
	 * 
	 * @return
	 */
	Stream<BoardGroup> getAll();
	
	/**
	 * 所有的版块组(卷).包括所有状态的, 默认的; 前端不要使用
	 * 
	 * @return
	 */
	Stream<BoardGroup> getAllContainsDefault(); 
	
	/**
	 * 所有可用的版块组(卷)
	 * 
	 * @return
	 */
	List<BoardGroup> getAllUsed();
	
	/**
	 * 查看所有栏目(原生的版块组)
	 * @since 20200427
	 * @return
	 */
	Stream<BoardGroup> getAllOrigin();
	
	/**
	 * 查看指定的栏目
	 * @since 20200427
	 * @param idList 栏目ID列表
	 * @return
	 */
	Stream<BoardGroup> getAllOriginById(Collection<Integer> idList);
	
	/**
	 * 级联关联卷下的所有版块
	 * 
	 * @param containsDefault 是否包含默认版块.true包含,false不包含
	 * @return
	 */
	List<BoardGroup> getAllUsedAndBoard(boolean containsDefault);
	
	/**
	 * 查找指定版块所属的版块组(卷)
	 * 
	 * @param boardId 版块ID
	 * @return
	 */
	Optional<BoardGroup> getByBoardId(long boardId);
	
	/**
	 * 创建新的版块组(卷)
	 * 
	 * @param title           名称
	 * @param description     描述
	 * @param encodeImageAddr 图标地址(编码后的格式)
	 * @param status          状态
	 * @param ranking         显示顺序
	 * @return
	 */
	Optional<BoardGroup> create(String title, String description, String encodeImageAddr, boolean status, int ranking)throws IllegalStateException;
	
	/**
	 * 查看指定的版块组(卷)
	 * 
	 * @param id 版块组(卷)ID
	 * @return
	 */
	Optional<BoardGroup> get(int id);
	
	/**
	 * 查看指定栏目(原生版块组)
	 * @since 20200427
	 * @param directoryNames 栏目的目录名称
	 * @return
	 */
	Optional<BoardGroup> getOriginByDirectoryNames(String directoryNames);
	
	/**
	 * 查看指定栏目(原生版块组)
	 * @since 20200427
	 * @param sectionId 栏目ID
	 * @return
	 */
	Optional<BoardGroup> getOriginById(int sectionId);
	
	/**
	 * //编辑栏目时检查目录是否被使用
	 * @since 20200427
	 * @param directoryNames 栏目的目录名称
	 * @param sectionId      编辑的栏目ID
	 * @return
	 */
	Optional<Boolean> checkOriginDirectNameUnique(String directoryNames, int sectionId)throws IllegalStateException;
	
	/**
	 * 查看指定的版块组(卷)名称
	 * 
	 * @param idList 版块组(卷)ID列表
	 * @return key = 版块组(卷)ID, value=相应的名称
	 */
	Map<Integer, String> getAllById(Collection<Integer> idList);
}
