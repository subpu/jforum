package com.apobates.forum.core.api.service;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.api.ImageIOMeta;
import com.apobates.forum.core.entity.TopicCarouselSlide;
/**
 * 轮播图幻灯片的业务接口
 * @author xiaofanku
 * @since 20191012
 */
public interface TopicCarouselSlideService {
	/**
	 * 查看指定轮播图的幻灯片,要求幻灯片的状态可用
	 * 
	 * @param topicCarouselId 轮播图ID
	 * @return
	 */
	Stream<TopicCarouselSlide> getAll(int topicCarouselId);
	
	/**
	 * 查看指定轮播图的幻灯片,要求幻灯片的状态可用并解码幻灯片的图片地址
	 * 
	 * @param topicCarouselTitle 轮播图标题或标识
	 * @param imageIOInfo        图片IO信息
	 * @return
	 */
	Stream<TopicCarouselSlide> getAll(String topicCarouselTitle, ImageIOMeta imageIOInfo);
	
	/**
	 * 查看指定版块组(卷)的最近轮播图的幻灯片
	 * 
	 * @param boardGroupId 版块组(卷)ID
	 * @param imageIOInfo  图片IO信息
	 * @return
	 */
	Stream<TopicCarouselSlide> getAll(int boardGroupId, ImageIOMeta imageIOInfo);
	
	/**
	 * 查看指定版块的最近轮播图的幻灯片
	 * 
	 * @param boardGroupId 版块组(卷)ID
	 * @param boardId      版块ID
	 * @param imageIOInfo  图片IO信息
	 * @return
	 */
	Stream<TopicCarouselSlide> getAll(int boardGroupId, long boardId, ImageIOMeta imageIOInfo);
	
	/**
	 * 查看指定轮播图的幻灯片
	 * 
	 * @param topicCarouselId 轮播图ID
	 * @return
	 */
	Stream<TopicCarouselSlide> getAllIgnoreStatus(int topicCarouselId);
	
	/**
	 * 查看指定的幻灯片
	 * 
	 * @param id 幻灯片ID
	 * @return
	 */
	Optional<TopicCarouselSlide> get(int id);
	
	/**
	 * 更新幻灯片的显示顺序
	 * 
	 * @param id      幻灯片ID
	 * @param ranking 显示顺序
	 * @return
	 */
	Optional<Boolean> edit(int id, int ranking)throws IllegalStateException;
	
	/**
	 * 编辑指定的幻灯片属性值
	 * 
	 * @param id      幻灯片ID
	 * @param title   标题
	 * @param link    连接地址
	 * @param ranking 显示顺序
	 * @param status  状态
	 * @return
	 */
	Optional<Boolean> edit(int id, String title, String link, int ranking, boolean status)throws IllegalStateException;
	
	/**
	 * 将轮播图标记为删除
	 * 
	 * @param id              幻灯片ID
	 * @param topicCarouselId 轮播图ID
	 * @return
	 */
	Optional<Boolean> remove(int id, int topicCarouselId)throws IllegalStateException;
	
	/**
	 * 更新幻灯片的显示顺序
	 * 
	 * @param rankingMap Key=幻灯片ID, Value=显示顺序
	 * @return
	 */
	int edit(Map<Integer,Integer> rankingMap)throws IllegalStateException;
	
	/**
	 * 创建轮播图的的幻灯片
	 * 
	 * @param topicCarouselId 轮播图ID
	 * @param title           标题
	 * @param link            连接地址
	 * @param encodeImageAddr 编码后的图片地址
	 * @param ranking         显示顺序
	 * @param status          状态
	 * @return
	 */
	int create(int topicCarouselId, String title, String link, String encodeImageAddr, int ranking, boolean status)throws IllegalStateException;
}
