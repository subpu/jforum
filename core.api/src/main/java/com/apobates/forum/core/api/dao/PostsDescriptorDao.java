package com.apobates.forum.core.api.dao;

import java.util.Collection;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.PostsDescriptor;
import com.apobates.forum.utils.persistence.PagingAndSortingRepository;

public interface PostsDescriptorDao extends PagingAndSortingRepository<PostsDescriptor,Long>{
	/**
	 * 查看指定回复的附加信息
	 * 
	 * @param postsIdSet 回复ID集合
	 * @return
	 */
	Stream<PostsDescriptor> findAllByPosts(Collection<Long> postsIdSet);
}
