package com.apobates.forum.core.api.service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.api.ImageIOMeta;
import com.apobates.forum.core.entity.BoardCarouselIndex;
import com.apobates.forum.core.entity.TopicCarousel;
/**
 * 轮播图的业务接口
 * @author xiaofanku
 * @since 20191012
 */
public interface TopicCarouselService {
	/**
	 * 查看所有轮播图,忽视状态
	 * 
	 * @return
	 */
	Stream<TopicCarousel> getAll();
	
	/**
	 * 查看指定轮播图的关联记录
	 * 
	 * @param topicCarouselId 轮播图ID
	 * @return
	 */
	Stream<BoardCarouselIndex> getAllRelative(int topicCarouselId);
	
	/**
	 * 查看指定轮播图,忽视状态
	 * 
	 * @param id 轮播图ID
	 * @return
	 */
	Optional<TopicCarousel> get(int id);
	
	/**
	 * 创建新的轮播图
	 * 
	 * @param title   轮播图标题或标识
	 * @param summary 描述
	 * @param status  状态
	 * @return
	 */
	int create(String title, String summary, boolean status)throws IllegalStateException;
	
	/**
	 * 查看指定轮播图,状态要求可用.级联加载幻灯片
	 * 
	 * @param title 轮播图标题或标识
	 * @return
	 */
	Optional<TopicCarousel> get(String title);
	
	/**
	 * 查看指定轮播图,状态要求可用.级联加载解码后的幻灯片
	 * 
	 * @param title   轮播图标题或标识
	 * @param imageIO 图片IO信息
	 * @return
	 */
	Optional<TopicCarousel> get(String title, ImageIOMeta imageIO);
	
	/**
	 * 查看指定版块最近关联的轮播图
	 * 
	 * @param boardGroupId 版块组(卷)ID
	 * @param boardId      版块ID
	 * @param imageIO      图片IO信息
	 * @return
	 */
	Optional<TopicCarousel> get(int boardGroupId, long boardId, ImageIOMeta imageIO);
	
	/**
	 * 编辑指定的轮播图属性值
	 * 
	 * @param id      更新的轮播图ID
	 * @param title   轮播图标题或标识
	 * @param summary 描述
	 * @param status  状态
	 * @return
	 */
	Optional<Boolean> edit(int id, String title, String summary, boolean status)throws IllegalStateException;
	
	/**
	 * 将轮播图标记为删除
	 * 
	 * @param id 轮播图ID
	 * @return
	 */
	Optional<Boolean> remove(int id)throws IllegalStateException;
	
	/**
	 * 关联版块的轮播图
	 * 
	 * @param boardGroupId    版块组(卷)ID
	 * @param boardId         版块ID
	 * @param topicCarouselId 轮播图ID
	 * @param expireDateTime  结束日期
	 * @return
	 */
	Optional<Boolean> bindBoard(int boardGroupId, long boardId, int topicCarouselId, LocalDateTime expireDateTime)throws IllegalStateException;
	
	/**
	 * 删除指定版块关联的轮播图
	 * 
	 * @param topicCarouselId 轮播图ID
	 * @param boardGroupId    版块组(卷)ID
	 * @param boardId         版块ID
	 * @return
	 */
	Optional<Boolean> remove(int topicCarouselId, int boardGroupId, long boardId)throws IllegalStateException;
}
