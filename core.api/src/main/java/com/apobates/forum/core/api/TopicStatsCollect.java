package com.apobates.forum.core.api;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 针对会员的统计
 * @author xiaofanku
 * @since 20200313
 */
public class TopicStatsCollect implements Serializable{
	private static final long serialVersionUID = 5866832108338333856L;
	//会员ID
	private final long member;
	//话题数
	private final long threads;
	//回复数
	private final long replies;
	//点赞的话题数
	private final long threadsLikes;
	//收藏的话题数
	private final long threadsFavorites;
	
	public TopicStatsCollect(long member, long threads, long replies, long threadsLikes, long threadsFavorites) {
		super();
		this.member = member;
		this.threads = threads;
		this.replies = replies;
		this.threadsLikes = threadsLikes;
		this.threadsFavorites = threadsFavorites;
	}

	public long getMember() {
		return member;
	}

	public long getThreads() {
		return threads;
	}

	public long getReplies() {
		return replies;
	}

	public long getThreadsLikes() {
		return threadsLikes;
	}

	public long getThreadsFavorites() {
		return threadsFavorites;
	}
	
	public Map<String,String> toMap(){
		Map<String,String> data = new HashMap<>();
		data.put("id", getMember()+"");
		data.put("threads", getThreads()+"");
		data.put("replies", getReplies()+"");
		data.put("threads-likes", getThreadsLikes()+"");
		data.put("threads-favorites", getThreadsFavorites()+"");
		return data;
	}
	
	public static class TopicStatsCollectBuilder{
		//会员ID
		private final long member;
		//话题数
		private long threads=0L;
		//回复数
		private long replies=0L;
		//点赞的话题数
		private long threadsLikes=0L;
		//收藏的话题数
		private long threadsFavorites=0L;
		
		public TopicStatsCollectBuilder(long memberId) {
			this.member = memberId;
		}
		public TopicStatsCollectBuilder threads(long threads){
			this.threads = threads;
			return this;
		}
		public TopicStatsCollectBuilder replies(long replies){
			this.replies = replies;
			return this;
		}
		public TopicStatsCollectBuilder threadsLikes(long threadsLikes){
			this.threadsLikes = threadsLikes;
			return this;
		}
		public TopicStatsCollectBuilder threadsFavorites(long threadsFavorites){
			this.threadsFavorites = threadsFavorites;
			return this;
		}
		public TopicStatsCollect build() {
			return new TopicStatsCollect(member, threads, replies, threadsLikes, threadsFavorites);
		}
	}
}
