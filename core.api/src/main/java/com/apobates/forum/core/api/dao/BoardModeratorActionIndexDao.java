package com.apobates.forum.core.api.dao;

import java.util.List;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.BoardModeratorActionIndex;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.utils.persistence.DataRepository;

/**
 * 版主权限的持久层接口
 * 
 * @author xiaofanku
 * @since 20190316
 */
public interface BoardModeratorActionIndexDao extends DataRepository<BoardModeratorActionIndex, Long> {
	/**
	 * 查看指定版主的操作权限
	 * 
	 * @param moderatorId 版主ID
	 * @return
	 */
	Stream<BoardModeratorActionIndex> getAllByModerator(long moderatorId);
	
	/**
	 * 创建指定的版主操作权限
	 * 
	 * @param bmActions 版主操作枚举实例列表
	 */
	int batchSave(List<BoardModeratorActionIndex> bmActions);
	
	/**
	 * 更新指定版主的操作权限
	 * 
	 * @param moderatorId 版主ID
	 * @param actions     操作枚举
	 * @return
	 */
	int edit(long moderatorId, List<ForumActionEnum> actions);
}
