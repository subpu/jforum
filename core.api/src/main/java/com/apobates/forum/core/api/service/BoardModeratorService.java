package com.apobates.forum.core.api.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.BoardModerator;
import com.apobates.forum.core.entity.ModeratorLevelEnum;
import com.apobates.forum.member.entity.Member;

/**
 * 版主的业务接口
 * 
 * @author xiaofanku
 * @since 20190316
 */
public interface BoardModeratorService {
	/**
	 * 指定版块的所有版主,包括各种状态.版块所在版块组的大版主不会显示
	 * 
	 * @param boardId 版块ID
	 * @return
	 */
	Stream<BoardModerator> getAllByBoardId(long boardId);

	/**
	 * 指定版块所有在职的版主.版块所在版块组的大版主不会显示
	 * 
	 * @param boardId 版块ID
	 * @return
	 */
	List<BoardModerator> getAllUsedByBoardId(long boardId);
	
	/**
	 * 指定版块所有在职的版主.包括当前版块组的大版主
	 * 
	 * @param boardGroupId 版块组(卷)ID
	 * @param boardId      版块ID
	 * @return
	 */
	Stream<BoardModerator> getAllUsedByBoardId(int boardGroupId, long boardId);
	
	/**
	 * 所有版主
	 * 
	 * @return
	 */
	Stream<BoardModerator> getAll();
	
	/**
	 * 查看指定版块组下的所有在职的大版主
	 *  
	 * @param boardGroupId 版块组(卷)ID
	 * @return
	 */
	Stream<BoardModerator> getAllUsedByBoardGroupId(int boardGroupId);
	
	/**
	 * 会员是否是指定版块的版主或者是版块所在组的大版主
	 * 
	 * @param boardGroupId 版块组(卷)ID
	 * @param boardId      版块ID
	 * @param memberId     会员ID
	 * @return
	 */
	Optional<BoardModerator> get(int boardGroupId, long boardId, long memberId);
	
    /**
     * 任命版主
     *
     * @param volumesId 版块组(卷)ID
     * @param boardId 版块ID
     * @param member 会员,需要属性有:id,nickname,mrole
     * @param level 版主的等级
     * @exception IllegalArgumentException 若会员为null时产生
     * @return
     */
    Optional<BoardModerator> create(int volumesId, long boardId, Member member, ModeratorLevelEnum level)throws IllegalArgumentException;
    
    /**
     * 任命版块组(卷)大版主
     *
     * @param volumesId 版块组(卷)ID
     * @param member 会员,需要属性有:id,nickname,mrole
     * @param level 大版主的等级
     * @exception IllegalArgumentException 若会员为null时产生
     * @return
     */
    Optional<BoardModerator> create(int volumesId, Member member, ModeratorLevelEnum level)throws IllegalArgumentException;
	
	/**
	 * 查看指定的版主
	 * 
	 * @param id 版主ID
	 * @return
	 */
	Optional<BoardModerator> get(long id);
	
	/**
	 * 卸任会员的大版主
	 * 
	 * @param volumesId 版块组(卷)ID
	 * @param memberId  会员ID
	 * @return
	 */
	Optional<Boolean> remove(int volumesId, long memberId)throws IllegalStateException;
	
	/**
	 * 卸任会员的版主
	 * 
	 * @param volumesId 版块组(卷)ID
	 * @param boardId   版块ID
	 * @param memberId  会员ID
	 * @return
	 */
	Optional<Boolean> remove(int volumesId, long boardId, long memberId)throws IllegalStateException;
	
	/**
	 * 更新版主
	 * 
	 * @param id              版主ID
	 * @param updateModerator 更新的版主属性实例
	 * @return
	 */
	Optional<Boolean> edit(long id, BoardModerator updateModerator)throws IllegalStateException;
}
