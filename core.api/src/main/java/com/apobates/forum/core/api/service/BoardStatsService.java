package com.apobates.forum.core.api.service;

import java.util.EnumMap;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.BoardStats;
import com.apobates.forum.event.elderly.ForumActionEnum;
/**
 * 版块统计
 * 
 * @author xiaofanku
 * @since 20190327
 */
public interface BoardStatsService {
	/**
	 * 查看所有版块的统计信息
	 * 
	 * @return
	 */
	Stream<BoardStats> getAll();
	
	/**
	 * 查看指定版块的统计信息
	 * 
	 * @param boardIdList 版块ID列表
	 * @return
	 */
	Stream<BoardStats> getByBoardIdList(Set<Long> boardIdList);
	
	/**
	 * 指定版块组下的所有版块统计, 并填充今日话题的数量
	 * 
	 * @param boardGroupId 版块组ID
	 * @return
	 */
	Stream<BoardStats> getFillTodayTopices(int boardGroupId);
	
	/**
	 * 查看所有非原生版块的统计, 并填充今日话题的数量
	 * 
	 * @return
	 */
	Stream<BoardStats> getFillTodayTopicesForNotOrigin();
	
	/**
	 * 指定版块的统计, 并填充今日话题的数量
	 * 
	 * @param boardIdSet 版块ID集合
	 * @return
	 */
	Stream<BoardStats> getFillTodayTopices(Set<Long> boardIdSet); 
	
	/**
	 * 查看版块的统计
	 * 
	 * @param boardId 版块ID
	 * @return
	 */
	Optional<BoardStats> getByBoard(long boardId);
	
	/**
	 * 合计所有话题数和所有回复数
	 * 
	 * @return
	 */
	EnumMap<ForumActionEnum,Long> sumTopicAndPosts();
}
