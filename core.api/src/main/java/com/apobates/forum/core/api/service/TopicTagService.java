package com.apobates.forum.core.api.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import com.apobates.forum.core.entity.TopicTag;
import com.apobates.forum.event.elderly.ActionEventCulpritor;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;
/**
 * 话题标签的业务接口
 * 
 * @author xiaofanku
 * @since 20190326
 */
public interface TopicTagService {
	/**
	 * 查看所有标签
	 * 
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<TopicTag> getAll(Pageable pageable);
	
	/**
	 * 查看指定话题的标签
	 * 
	 * @param topicId 话题ID
	 * @return
	 */
	List<TopicTag> getAllBy(long topicId);
	
	/**
	 * 从话题中删除指定标签
	 * 
	 * @param topicId   话题ID
	 * @param tagId     标签ID
	 * @param culpritor 操作的肇事信息
	 * @return
	 */
	Optional<Boolean> deleteForTopic(long topicId, long tagId, ActionEventCulpritor culpritor)throws IllegalStateException;
	
	/**
	 * 为话题增加标签
	 * 
	 * @param topicId   话题ID
	 * @param tagData   Key=标签,Value=词频
	 * @param culpritor 操作的肇事信息
	 * @return
	 */
	Optional<Boolean> addForTopic(long topicId, Map<String,Integer> tagData, ActionEventCulpritor culpritor)throws IllegalStateException;
	
	/**
	 * 添加新标签
	 * 
	 * @param topicId   话题ID
	 * @param names     标签名称
	 * @param rates     词频
	 * @param culpritor 操作的肇事信息
	 * @return
	 */
	long create(long topicId, String names, int rates, ActionEventCulpritor culpritor)throws IllegalStateException;
	
	/**
	 * 分组统计出现频率最高的标签名称(TopicTag.names)
	 * 
	 * @param size 从高到低选取的数量
	 * @return
	 */
	Map<String,Long> groupTagForNames(int size);
}
