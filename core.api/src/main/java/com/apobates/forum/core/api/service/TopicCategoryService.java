package com.apobates.forum.core.api.service;

import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.TopicCategory;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;

/**
 * 话题类型的业务层接口
 * 
 * @author xiaofanku
 * @since 20190330
 */
public interface TopicCategoryService {
	/**
	 * 所有话题类型
	 * 
	 * @param pageable 分页请参数
	 * @return
	 */
	Page<TopicCategory> getAll(Pageable pageable);
	
	/**
	 * 所有可用的话题类型
	 * 
	 * @return
	 */
	Stream<TopicCategory> getAllUsed();
	
	/**
	 * 查看版块可以关联的话题类型
	 * 
	 * @return
	 */
	Stream<TopicCategory> getAssociates();
	
	/**
	 * 查看指定的话题类型
	 * 
	 * @param id 话题分类ID
	 * @return
	 */
	Optional<TopicCategory> get(int id);
	
	/**
	 * 创建话题类型
	 * 
	 * @param names  参数名称
	 * @param value  参数值
	 * @param status 状态
	 * @return
	 */
	Optional<TopicCategory> create(String names, String value, boolean status)throws IllegalStateException;
	
	/**
	 * 根据名称值查看话题类型
	 * 
	 * @param value 参数值
	 * @return
	 */
	Optional<TopicCategory> get(String value);
	
	/**
	 * 删除指定的话题类型
	 * 
	 * @param id 话题分类ID
	 * @return
	 */
	Optional<Boolean> remove(int id)throws IllegalStateException;
	
	/**
	 * 更新指定的话题类型
	 * 
	 * @param id             话题类型类ID
	 * @param updateCategory 更新的类型属性实例
	 * @return
	 */
	Optional<Boolean> edit(int id, TopicCategory updateCategory)throws IllegalStateException;
}
