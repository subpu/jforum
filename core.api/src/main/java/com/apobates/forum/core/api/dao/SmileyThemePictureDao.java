package com.apobates.forum.core.api.dao;

import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.SmileyThemePicture;
import com.apobates.forum.utils.persistence.DataRepository;
/**
 * 表情图片持久接口
 * @author xiaofanku
 * @since 20190529
 */
public interface SmileyThemePictureDao extends DataRepository<SmileyThemePicture, Long>{
	/**
	 * 
	 * @param smileyThemeId
	 * @return
	 */
	Stream<SmileyThemePicture> findAllByTheme(int smileyThemeId);
	/**
	 * 
	 * @param smileyThemeNames
	 * @return
	 */
	Stream<SmileyThemePicture> findAllByTheme(String smileyThemeNames);
	/**
	 * 
	 * @param themeId
	 * @param picFileNames
	 * @return
	 */
	Optional<SmileyThemePicture> findOne(int themeId, String picFileNames);
}
