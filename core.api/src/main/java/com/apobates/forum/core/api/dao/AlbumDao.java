package com.apobates.forum.core.api.dao;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.Album;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;
import com.apobates.forum.utils.persistence.PagingAndSortingRepository;
/**
 * 像册持久接口
 * 
 * @author xiaofanku
 * @since 20190609
 */
public interface AlbumDao extends PagingAndSortingRepository<Album, Long>{
	/**
	 * 查看指定版块下的所有像册
	 * 
	 * @param boardId  版块ID
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<Album> findAllByBoard(long boardId, Pageable pageable);
	
	/**
	 * 查看指定版块组(卷)下的所有像册
	 * 
	 * @param volumesId 版块组(卷)ID
	 * @param pageable  分页请求参数
	 * @return
	 */
	Page<Album> findAllByVolumes(int volumesId, Pageable pageable);
	
	/**
	 * 查看最近可用的像册
	 * 
	 * @param size 显示的数量
	 * @return
	 */
	Stream<Album> findAllRecent(int size);
	
	/**
	 * 查看指定话题的像册
	 * 
	 * @param topicIdSet 话题ID集合
	 * @return
	 */
	Stream<Album> findAllByTopic(Collection<Long> topicIdSet);
	
	/**
	 * 查看指定话题下的像册
	 * 
	 * @param topicId 话题ID
	 * @return
	 */
	Optional<Album> findOneByTopic(long topicId);
	
	/**
	 * 设置指定的图片为像册的封面图片
	 * 
	 * @param id                     像册ID
	 * @param encodeCoverPictureLink 编码后的封面图片地址
	 * @param coverPictureId         新封面图片ID
	 * @return
	 */
	Optional<Boolean> editCover(long id, String encodeCoverPictureLink, long coverPictureId);
	
	/**
	 * 将像册与话题绑定,前提是话题未绑定过像册
	 * 
	 * @param id      像册ID
	 * @param topicId 话题ID
	 * @return
	 */
	int bindTopicAlbum(long id, long topicId);
}
