package com.apobates.forum.core.api;

import java.io.File;
import java.util.Optional;

public interface TopicFileCacheHandler {
	/**
	 * 
	 * @return
	 */
	Optional<File> create();
}
