package com.apobates.forum.core.api.dao;

import java.util.stream.Stream;
import com.apobates.forum.core.entity.TopicCategoryItem;
import com.apobates.forum.utils.persistence.PagingAndSortingRepository;

/**
 * 话题子分类的持久层接口
 * 
 * @author xiaofanku
 * @since 20190331
 */
public interface TopicCategoryItemDao extends PagingAndSortingRepository<TopicCategoryItem, Long> {
	/**
	 * 查看指定话题分类的子分类
	 * 
	 * @param categoryId 话题分类ID
	 * @return
	 */
	Stream<TopicCategoryItem> findAllByCategory(int categoryId);
}
