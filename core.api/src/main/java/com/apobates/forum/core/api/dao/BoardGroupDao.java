package com.apobates.forum.core.api.dao;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.BoardGroup;
import com.apobates.forum.utils.persistence.DataRepository;

/**
 * 版块组(卷)的持久层接口
 * 
 * @author xiaofanku
 * @since 20190316
 */
public interface BoardGroupDao extends DataRepository<BoardGroup, Integer> {
	/**
	 * 所有可以显示的
	 * 
	 * @return
	 */
	List<BoardGroup> findUsed();
	
	/**
	 * 查看所有的栏目(原生的版块组)
	 * @since 20200427
	 * @return
	 */
	Stream<BoardGroup> findAllOrigin();
	
	/**
	 * 查看指定的栏目(原生的版块组)
	 * @since 20200427 
	 * @param idList 栏目ID列表
	 * @return
	 */
	Stream<BoardGroup> findAllOriginById(Collection<Integer> idList);
	
	/**
	 * 查看指定的版块组(卷)
	 * 
	 * @param idList 版块组(卷)ID列表
	 * @return
	 */
	Stream<BoardGroup> findAllById(Collection<Integer> idList);
	
	/**
	 * 查找版块所属的版块组(卷)
	 * 
	 * @param boardId 版块ID
	 * @return
	 */
	Optional<BoardGroup> findOneByBoard(long boardId);
	
	/**
	 * 查看指定的栏目(原生的版块组)
	 * @since 20200427
	 * @param directoryNames 栏目的目录名称
	 * @return
	 */
	Optional<BoardGroup> findOneOriginByDirectoryNames(String directoryNames);
	
	/**
	 * 查看指定的栏目(原生的版块组)
	 * @since 20200427
	 * @param sectionId 栏目ID
	 * @return
	 */
	Optional<BoardGroup> findOneSectionById(int sectionId);
	
	/**
	 * 指定的栏目名称是否存在
	 * @since 20200427
	 * @param directoryNames 栏目的目录名称
	 * @return 返回的值在存在时是对象ID, 不存在返回0L
	 */
	int existsOriginDirectoryNames(String directoryNames);
	
	/**
	 * 指定的栏目名称是否存在
	 * @since 20200427
	 * @param directoryNames 栏目的目录名称
	 * @param sectionId      排除的栏目ID
	 * @return 返回的值表示统计的数量
	 */
	long existsOriginDirectoryNames(String directoryNames, int sectionId);
}
