package com.apobates.forum.core.api.dao;

import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.BoardTopicCategoryIndex;
import com.apobates.forum.core.entity.TopicCategory;

public interface BoardTopicCategoryIndexDao {
	/**
	 * 批量保存版块话题类型索引
	 * 
	 * @param entities
	 * @return
	 */
	int batchSave(List<BoardTopicCategoryIndex> entities);
	
	/**
	 * 批量删除指定版块已关联的话题类型
	 * 
	 * @param boardId       版块ID
	 * @param categoryIdSet 话题类型ID集合
	 * @return
	 */
	int delete(long boardId, Collection<Integer> categoryIdSet);
	
	/**
	 * 指定版块关联的话题类型索引
	 * 
	 * @param boardId      版块ID
	 * @param boardGroupId 版块所在版块组(卷)的ID
	 * @return
	 */
	Stream<BoardTopicCategoryIndex> findAllByBoard(long boardId, int boardGroupId);
	
	/**
	 * 指定版块关联的话题类型
	 * 
	 * @param boardId      版块ID
	 * @param boardGroupId 版块所在版块组(卷)的ID
	 * @return
	 */
	Stream<TopicCategory> findAll(long boardId, int boardGroupId);
	
	/**
	 * 指定话题类型关联的话题类型索引
	 * 
	 * @param categoryId 话题类型ID
	 * @return
	 */
	Stream<BoardTopicCategoryIndex> findAllByCategory(int categoryId);
	
	/**
	 * 指定话题类型关联的话题类型索引
	 * 
	 * @param categoryIdList 话题类型ID
	 * @return
	 */
	Stream<BoardTopicCategoryIndex> findAllByCategory(List<Integer> categoryIdList);
}
