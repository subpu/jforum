package com.apobates.forum.core.api.service;

import java.util.Optional;
import com.apobates.forum.core.entity.BoardConfig;

/**
 * 版块配置的业务接口
 * 
 * @author xiaofanku
 * @since 20190316
 */
public interface BoardConfigService {
	/**
	 * 查看指定的版块配置
	 * 
	 * @param boardId 版块ID
	 * @return
	 */
	Optional<BoardConfig> getByBoardId(long boardId);

	/**
	 * 创建指定的版块配置实例
	 * 
	 * @param boardId 版块ID
	 * @param config  版块配置的属性实例
	 * @return
	 */
	Optional<Boolean> create(long boardId, BoardConfig config)throws IllegalStateException;
}
