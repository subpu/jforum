package com.apobates.forum.core.api.service;

import java.util.stream.Stream;
import com.apobates.forum.core.entity.BoardActionCollection;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;

/**
 * 版块动作集合的业务层接口
 * 
 * @author xiaofanku
 * @since 20190329
 */
public interface BoardActionCollectionService {
	/**
	 * 指定会员的版块动作
	 * 
	 * @param memberId 会员ID
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<BoardActionCollection> getByMember(long memberId, Pageable pageable);
	
	/**
	 * 查看指定版块发生的动作
	 * 
	 * @param boardId  版块ID
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<BoardActionCollection> getByBoard(long boardId, Pageable pageable);
	Page<BoardActionCollection> getByBoard(long boardId, ForumActionEnum action, Pageable pageable);
	
	/**
	 * 所有的版块操作动作
	 * 
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<BoardActionCollection> getAll(Pageable pageable);
	
	/**
	 * 指定会员最近的版块动作
	 * 
	 * @param memberId 会员ID
	 * @param size     显示的数量
	 * @return
	 */
	Stream<BoardActionCollection> getRecentByMember(long memberId, int size);
	
	/**
	 * 查看会员收藏/星标的版块
	 * 
	 * @param memberId 会员ID
	 * @param size     显示的数量
	 * @return
	 */
	Stream<BoardActionCollection> getMemberFavoriteBoard(long memberId, int size);

}
