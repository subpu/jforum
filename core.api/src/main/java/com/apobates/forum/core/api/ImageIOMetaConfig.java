package com.apobates.forum.core.api;
/**
 * 图片存储信息
 * 
 * @author xiaofanku
 * @since 20190922
 */
public class ImageIOMetaConfig implements ImageIOMeta{
	private final String imageBucketDomain;
	private final String uploadImageDirectName;
	private final String smileyDirectName;
	/**
	 * 
	 * @param imageBucketDomain     存储图片的域名,例:http://x.com
	 * @param uploadImageDirectName 存储图片的目录名称
	 * @param smileyDirectName      表情图片的目录名称
	 */
	public ImageIOMetaConfig(String imageBucketDomain, String uploadImageDirectName, String smileyDirectName) {
		super();
		this.imageBucketDomain = imageBucketDomain;
		this.uploadImageDirectName = uploadImageDirectName;
		this.smileyDirectName = smileyDirectName;
	}

	@Override
	public String getImageBucketDomain() {
		return imageBucketDomain;
	}

	@Override
	public String getSmileyDirectName() {
		return smileyDirectName;
	}

	@Override
	public String getUploadImageDirectName() {
		return uploadImageDirectName;
	}

}
