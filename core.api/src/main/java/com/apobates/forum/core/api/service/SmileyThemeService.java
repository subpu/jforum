package com.apobates.forum.core.api.service;

import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.SmileyTheme;

/**
 * 表情风格业务接口
 * 
 * @author xiaofanku
 * @since 20190529
 */
public interface SmileyThemeService {
	/**
	 * 查看所有的表情风格
	 * 
	 * @return
	 */
	Stream<SmileyTheme> getAll();
	
	/**
	 * 查看指定的表情风格
	 * 
	 * @param id 表情风格ID
	 * @return
	 */
	Optional<SmileyTheme> get(int id);
	
	/**
	 * 查看指定的表情风格
	 * @param themeDirectName 表情风格的目录名
	 * @return
	 */
	Optional<SmileyTheme> getByDirectName(String themeDirectName);
	
	/**
	 * 创建一个表情风格实例
	 * 
	 * @param title       中文名称
	 * @param label       英文文名称
	 * @param directNames 目录名
	 * @param status      状态
	 * @param ranking     显示顺序
	 * @return
	 */
	Optional<SmileyTheme> create(String title, String label, String directNames, boolean status, int ranking)throws IllegalStateException;

	/**
	 * 更新指定的表情风格
	 * 
	 * @param id               表情风格ID
	 * @param updateSmileTheme 更新的表情风格实例
	 * @return
	 */
	Optional<Boolean> edit(int id, SmileyTheme updateSmileTheme)throws IllegalStateException;
	
	/**
	 * getAll方法的toMap版本, Key=SmileyTheme::getDirectNames, Value=SmileyTheme::getTitle
	 * 
	 * @return
	 */
	TreeMap<String,String> getAllUsed();
}
