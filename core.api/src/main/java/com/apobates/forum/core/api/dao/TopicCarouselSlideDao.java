package com.apobates.forum.core.api.dao;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.TopicCarouselSlide;
import com.apobates.forum.utils.persistence.DataRepository;
/**
 * 轮播图幻灯片的持久接口
 * 
 * @author xiaofanku
 * @since 20191012
 */
public interface TopicCarouselSlideDao extends DataRepository<TopicCarouselSlide, Integer>{
	/**
	 * 查看指定轮播图的幻灯片,要求幻灯片的状态可用
	 * 
	 * @param topicCarouselId 轮播图ID
	 * @return
	 */
	Stream<TopicCarouselSlide> findAllUsed(int topicCarouselId);
	
	/**
	 * 查看指定轮播图的幻灯片
	 * 
	 * @param topicCarouselId 轮播图ID
	 * @return
	 */
	Stream<TopicCarouselSlide> findAll(int topicCarouselId);
	
	/**
	 * 查看指定轮播图的幻灯片,要求幻灯片的状态可用
	 * 
	 * @param topicCarouselTitle 轮播图.title
	 * @return
	 */
	Stream<TopicCarouselSlide> findAllUsed(String topicCarouselTitle);
	
	/**
	 * 更新指定幻灯片的显示顺序
	 * 
	 * @param id      幻灯片ID
	 * @param ranking 显示顺序
	 * @return
	 */
	Optional<Boolean> editRanking(int id, int ranking);
	
	/**
	 * 更新幻灯片的状态
	 * 
	 * @param id              幻灯片ID
	 * @param topicCarouselId 轮播图ID
	 * @return
	 */
	Optional<Boolean> editStatus(int id, int topicCarouselId, boolean status);
	
	/**
	 * 更新幻灯片的显示顺序
	 * 
	 * @param rankingMap Key=幻灯片ID, Value=显示顺序
	 * @return
	 */
	int editRanking(Map<Integer, Integer> rankingMap);
}
