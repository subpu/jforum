package com.apobates.forum.core.api.dao;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.BoardModerator;
import com.apobates.forum.core.entity.BoardModeratorRoleHistory;
import com.apobates.forum.utils.persistence.DataRepository;

/**
 * 版主的持久层接口
 * 
 * @author xiaofanku
 * @since 20190316
 */
public interface BoardModeratorDao extends DataRepository<BoardModerator, Long> {
	/**
	 * 指定版块的所有版主,包括禁用的
	 * 
	 * @param boardId 版块ID
	 * @return
	 */
	Stream<BoardModerator> findAllByBoardId(long boardId);

	/**
	 * 指定版块所有在职的版主
	 * 
	 * @param boardId 版块ID
	 * @return
	 */
	List<BoardModerator> findUsedByBoardId(long boardId);
	
	/**
	 * 查看指定版块组在职的大版主
	 * 
	 * @param boardGroupId 版块组(卷)ID
	 * @return
	 */
	Stream<BoardModerator> findAllUsedByBoardGroup(int boardGroupId);
	
	/**
	 * 指定的会员是否是指定版块的版主
	 * 
	 * @param boardId  版块ID
	 * @param memberId 会员ID
	 * @return
	 */
	Optional<BoardModerator> findOneByBoardAndMember(long boardId, long memberId);
	
	/**
	 * 指定的会员是否是指定版块组(卷)的大版主
	 * 
	 * @param boardGroupId 版块组(卷)ID
	 * @param memberId     会员ID
	 * @return
	 */
	Optional<BoardModerator> findOneByBoardGroupAndMember(int boardGroupId, long memberId);

	/**
	 * 保存版主
	 * 
	 * @param moderator
	 *            版主对象
	 * @param memberRoleHistory
	 *            会员的角色变化历史
	 * @return
	 */
	Optional<BoardModerator> pushModerator(BoardModerator moderator, BoardModeratorRoleHistory memberRoleHistory);

	/**
	 * 删除版主,pushModerator方法的倒转方法
	 * 
	 * @param history
	 *            删除的版主上任时的角色历史记录
	 * 
	 */
	void deleteModerator(BoardModeratorRoleHistory history);
}
