package com.apobates.forum.core.api.dao;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;
import com.apobates.forum.core.entity.BoardActionCollection;
import com.apobates.forum.event.elderly.ForumActionEnum;
import com.apobates.forum.utils.persistence.Page;
import com.apobates.forum.utils.persistence.Pageable;
import com.apobates.forum.utils.persistence.PagingAndSortingRepository;

/**
 * 版块动作集合的持久层接口
 * 
 * @author xiaofanku
 * @since 20190329
 */
public interface BoardActionCollectionDao extends PagingAndSortingRepository<BoardActionCollection, Long> {
	/**
	 * 指定会员的版块动作
	 * 
	 * @param memberId 会员ID
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<BoardActionCollection> findAllByMember(long memberId, Pageable pageable);
	
	/**
	 * 指定版块的动作
	 * 
	 * @param boardId  版块ID
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<BoardActionCollection> findAllByBoard(long boardId, Pageable pageable);
	Page<BoardActionCollection> findAllByBoard(long boardId, ForumActionEnum action, Pageable pageable);
	
	/**
	 * 查看指定日期的操作记录
	 * 
	 * @param start    开始日期
	 * @param finish   结束日期
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<BoardActionCollection> findAll(LocalDateTime start, LocalDateTime finish, Pageable pageable);
	
	/**
	 * 查看指定操作枚举的版块动作记录
	 * 
	 * @param action   操作枚举
	 * @param pageable 分页请求参数
	 * @return
	 */
	Page<BoardActionCollection> findAllByAction(ForumActionEnum action, Pageable pageable);

	/**
	 * 查看不同的操作枚举
	 * 
	 * @return
	 */
	Stream<ForumActionEnum> findAllAction();

	/**
	 * 批量保存
	 * 
	 * @param entities
	 * @return
	 */
	int batchSave(Collection<BoardActionCollection> entities);

	/**
	 * 指定会员最近的版块动作
	 * 
	 * @param memberId 会员ID
	 * @param size     显示的数量
	 * @return
	 */
	Stream<BoardActionCollection> findRecentByMember(long memberId, int size);

	/**
	 * 查看会员收藏/星标的版块
	 * 
	 * @param memberId 会员ID
	 * @param size     显示的数量
	 * @return
	 */
	Stream<BoardActionCollection> findAllMemberFavoriteBoard(long memberId, int size);
	
	/**
	 * 查看指定版块中会员的操作记录(针对版块自身的操作)
	 * 
	 * @param memberId 会员ID
	 * @param boardId  版块ID
	 * @param action   操作枚举
	 * @return
	 */
	Stream<BoardActionCollection> findAllMemberAction(long memberId, long boardId, ForumActionEnum action);
	
	/**
	 * 查看指定会员是否有版块的收藏记录
	 * 
	 * @param memberId 会员ID
	 * @param boardId  版块ID
	 * @return
	 */
	Optional<BoardActionCollection> findOneFavoriteRecord(long memberId, long boardId);
	
	/**
	 * 统计指定版块中会员的操作(针对版块自身的操作)
	 * 
	 * @param memberId 会员ID
	 * @param boardId  版块ID
	 * @param action   操作枚举
	 * @return
	 */
	long countMemberAction(long memberId, long boardId, ForumActionEnum action);
	
	/**
	 * 是否可以收藏, 可以返回true
	 * 
	 * @param boardId  版块ID
	 * @param memberId 会员ID
	 * @return
	 */
	Optional<Boolean> isFavorited(long boardId, long memberId)throws IllegalStateException;
}
